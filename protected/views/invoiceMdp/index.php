<?php
$this->breadcrumbs = array(
	'Invoice Mdps',
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' InvoiceMdp', 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' InvoiceMdp', 'url' => array('admin')),
);
?>

<h1>Invoice Mdps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 