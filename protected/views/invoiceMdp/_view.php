<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('invoice_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->invoice_id), array('view', 'id' => $data->invoice_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('doc_ref')); ?>:
	<?php echo GxHtml::encode($data->doc_ref); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tgl_cetak')); ?>:
	<?php echo GxHtml::encode($data->tgl_cetak); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('store_kode')); ?>:
	<?php echo GxHtml::encode($data->store_kode); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tgl_bayar')); ?>:
	<?php echo GxHtml::encode($data->tgl_bayar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tgl_periode')); ?>:
	<?php echo GxHtml::encode($data->tgl_periode); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('sub_total')); ?>:
	<?php echo GxHtml::encode($data->sub_total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('disc')); ?>:
	<?php echo GxHtml::encode($data->disc); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('disc_rp')); ?>:
	<?php echo GxHtml::encode($data->disc_rp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ppn')); ?>:
	<?php echo GxHtml::encode($data->ppn); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jml')); ?>:
	<?php echo GxHtml::encode($data->jml); ?>
	<br />
	*/ ?>

</div>