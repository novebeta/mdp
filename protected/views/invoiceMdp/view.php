<?php
$this->breadcrumbs = array(
	'Invoice Mdps' => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' InvoiceMdp', 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' InvoiceMdp', 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' InvoiceMdp', 'url'=>array('update', 'id' => $model->invoice_id)),
	array('label'=>Yii::t('app', 'Delete') . ' InvoiceMdp', 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->invoice_id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' InvoiceMdp', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View'); ?> InvoiceMdp #<?php echo GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'invoice_id',
'doc_ref',
'tgl_cetak',
'total',
'store_kode',
'tgl_bayar',
'tgl_periode',
'sub_total',
'disc',
'disc_rp',
'ppn',
'jml',
	),
        'itemTemplate' => "<tr class=\"{class}\"><td style=\"width: 120px\"><b>{label}</b></td><td>{value}</td></tr>\n",
        'htmlOptions' => array(
            'class' => 'table',
        ),
)); ?>

