<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'invoice-mdp-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="span-8 last">
		<?php echo $form->labelEx($model,'doc_ref'); ?>
		<?php echo $form->textField($model, 'doc_ref', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'doc_ref'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'tgl_cetak'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl_cetak',
			'value' => $model->tgl_cetak,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'tgl_cetak'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model, 'total', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'total'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'store_kode'); ?>
		<?php echo $form->textField($model, 'store_kode', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'store_kode'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'tgl_bayar'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl_bayar',
			'value' => $model->tgl_bayar,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'tgl_bayar'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'tgl_periode'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl_periode',
			'value' => $model->tgl_periode,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'tgl_periode'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'sub_total'); ?>
		<?php echo $form->textField($model, 'sub_total', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'sub_total'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'disc'); ?>
		<?php echo $form->textField($model, 'disc', array('maxlength' => 3)); ?>
		<?php echo $form->error($model,'disc'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'disc_rp'); ?>
		<?php echo $form->textField($model, 'disc_rp', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'disc_rp'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'ppn'); ?>
		<?php echo $form->textField($model, 'ppn', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'ppn'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'jml'); ?>
		<?php echo $form->textField($model, 'jml'); ?>
		<?php echo $form->error($model,'jml'); ?>
		</div><!-- row -->
<!-- june -->
<div class="row"></div>
<!-- june -->
		<!--label--><!--/label-->
		                
<?php
echo GxHtml::Button(Yii::t('app', 'Cancel'), array(
			'submit' => array('invoicemdp/admin')
		));
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->