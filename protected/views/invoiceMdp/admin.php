<?php
$this->breadcrumbs = array(
	'Invoice Mdps' => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' InvoiceMdp',
			'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' InvoiceMdp',
		'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('invoice-mdp-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage'); ?> Invoice Mdps</h1>

<p style="display:none">
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'invoice-mdp-grid',
	'dataProvider' => $model->search(),
        'itemsCssClass' => 'table',
	'filter' => $model,
	'columns' => array(
		'invoice_id',
		'doc_ref',
		'tgl_cetak',
		'total',
		'store_kode',
		'tgl_bayar',
		/*
		'tgl_periode',
		'sub_total',
		'disc',
		'disc_rp',
		'ppn',
		'jml',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>