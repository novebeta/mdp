<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="span-8 last">
		<?php echo $form->label($model, 'invoice_id'); ?>
		<?php echo $form->textField($model, 'invoice_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'doc_ref'); ?>
		<?php echo $form->textField($model, 'doc_ref', array('maxlength' => 50)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'tgl_cetak'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl_cetak',
			'value' => $model->tgl_cetak,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'total'); ?>
		<?php echo $form->textField($model, 'total', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'store_kode'); ?>
		<?php echo $form->textField($model, 'store_kode', array('maxlength' => 20)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'tgl_bayar'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl_bayar',
			'value' => $model->tgl_bayar,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'tgl_periode'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl_periode',
			'value' => $model->tgl_periode,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'sub_total'); ?>
		<?php echo $form->textField($model, 'sub_total', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'disc'); ?>
		<?php echo $form->textField($model, 'disc', array('maxlength' => 3)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'disc_rp'); ?>
		<?php echo $form->textField($model, 'disc_rp', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'ppn'); ?>
		<?php echo $form->textField($model, 'ppn', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'jml'); ?>
		<?php echo $form->textField($model, 'jml'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
