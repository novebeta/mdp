<?php
$this->breadcrumbs = array(
	'Invoice Mdps' => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'List') . ' InvoiceMdp', 'url' => array('index')),
	array('label'=>Yii::t('app', 'Manage') . ' InvoiceMdp', 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Create'); ?> InvoiceMdp</h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model,
		'buttons' => 'create'));
?>