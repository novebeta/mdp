<?php

class m190107_024204_add_visible_produksi extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `nscc_produksi` ADD COLUMN `visible`  tinyint(1) NULL DEFAULT 1 AFTER `doc_ref_produksi`;
		")->execute();
	}

	public function down()
	{
		echo "m190107_024204_add_visible_produksi does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}