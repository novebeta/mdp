<?php

class m180518_025733_create_sync_status_table extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand('CREATE TABLE IF NOT EXISTS `nscc_sync_status` (
                                      `sync_id` varchar(50) NOT NULL,
                                      `store_code` varchar(50) NOT NULL,
                                      `ip_address` varchar(50) DEFAULT NULL,
                                      `glt_pusat` int(11) DEFAULT \'0\', 
                                      `btr_pusat` int(11) DEFAULT \'0\',
                                      `glt_cabang` int(11) DEFAULT \'0\',
                                      `btr_cabang` int(11) DEFAULT \'0\',
                                      `diff_btr` int(11) DEFAULT \'0\',
                                      `diff_glt` int(11) DEFAULT \'0\',
                                      `updated_date` datetime DEFAULT NULL,
                                      `startscan` date DEFAULT NULL,
                                      `endscan` date DEFAULT NULL,
                                      `status_scan` varchar(20) DEFAULT \'0\',
                                      `sync_date` datetime DEFAULT NULL,
                                      `startsync` date DEFAULT NULL,
                                      `endsync` date DEFAULT NULL,
                                      `status_sync` varchar(20) DEFAULT \'0\',
                                      `note` text,
                                      PRIMARY KEY (`sync_id`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;')->execute();

	}

	public function down()
	{
		echo "m180518_025733_create_sync_status_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}