<?php

class m190128_084320_add_note_di_nscc_customers extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `nscc_customers` ADD COLUMN `note` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `photo_kanan`;
		")->execute();
	}

	public function down()
	{
		echo "m190128_084320_add_note_di_nscc_customers does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}