<?php

class m180704_033802_setting_price_override extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('ALTER TABLE `nscc_barang`
ADD COLUMN `price_override`  tinyint(4) NOT NULL DEFAULT 0 AFTER `barcode`;')->execute();
	}

	public function down()
	{
		echo "m180704_033802_setting_price_override does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}