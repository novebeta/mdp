<?php

class m180919_013612_resep_header extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		insert into nscc_sys_prefs (name_, store) VALUE('resep_header0', (select * from (select store from nscc_sys_prefs limit 1) as x));
		insert into nscc_sys_prefs (name_, store) VALUE('resep_header1', (select * from (select store from nscc_sys_prefs limit 1) as x));
		insert into nscc_sys_prefs (name_, store) VALUE('resep_header2', (select * from (select store from nscc_sys_prefs limit 1) as x));
		insert into nscc_sys_prefs (name_, store) VALUE('resep_header3', (select * from (select store from nscc_sys_prefs limit 1) as x));
		insert into nscc_sys_prefs (name_, store) VALUE('resep_header4', (select * from (select store from nscc_sys_prefs limit 1) as x));
		insert into nscc_sys_prefs (name_, store) VALUE('resep_header5', (select * from (select store from nscc_sys_prefs limit 1) as x));
		
		
		update nscc_sys_prefs set value_=(select * from (select value_ from nscc_sys_prefs WHERE name_='receipt_header0') as x ) 
		where name_='resep_header0';
		update nscc_sys_prefs set value_=(select * from (select value_ from nscc_sys_prefs WHERE name_='receipt_header1') as x ) 
		where name_='resep_header1';
		update nscc_sys_prefs set value_=(select * from (select value_ from nscc_sys_prefs WHERE name_='receipt_header2') as x ) 
		where name_='resep_header2';
		update nscc_sys_prefs set value_=(select * from (select value_ from nscc_sys_prefs WHERE name_='receipt_header3') as x ) 
		where name_='resep_header3';
		update nscc_sys_prefs set value_=(select * from (select value_ from nscc_sys_prefs WHERE name_='receipt_header4') as x ) 
		where name_='resep_header4';
		update nscc_sys_prefs set value_=(select * from (select value_ from nscc_sys_prefs WHERE name_='receipt_header5') as x ) 
		where name_='resep_header5';
		")->execute();
	}

	public function down()
	{
		echo "m180919_013612_resep_header does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}