<?php

class m190223_074602_add_panjang_cardnumber extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		ALTER TABLE `nscc_payment`
MODIFY COLUMN `card_number`  varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `payment_id`;
		")->execute();
	}

	public function down()
	{
		echo "m190223_074602_add_panjang_cardnumber does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}