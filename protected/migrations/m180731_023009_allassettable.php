<?php

class m180731_023009_allassettable extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand('DROP TABLE IF EXISTS `nscc_asset_category`;
CREATE TABLE IF NOT EXISTS `nscc_asset_category` (
  `category_id` varchar(50) NOT NULL,
  `category_code` varchar(50) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_desc` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `nscc_asset_group`;
CREATE TABLE IF NOT EXISTS `nscc_asset_group` (
  `asset_group_id` varchar(50) NOT NULL,
  `tariff` double DEFAULT NULL,
  `period` int(10) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `golongan` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`asset_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `nscc_asset`;
CREATE TABLE IF NOT EXISTS `nscc_asset` (
  `asset_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `asset_group_id` varchar(50) DEFAULT NULL,
  `barang_id` varchar(50) DEFAULT NULL,
  `category_id` varchar(50) DEFAULT NULL,
  `asset_name` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `doc_ref` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `branch` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `qty` varchar(15) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `date_acquisition` date DEFAULT NULL,
  `price_acquisition` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `new_price_acquisition` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `status` enum(`1`,`0`) DEFAULT `1`,
  `ati` varchar(50) DEFAULT NULL,
  `hide` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `nscc_asset_detail`;
CREATE TABLE IF NOT EXISTS `nscc_asset_detail` (
  `asset_trans_id` varchar(50) NOT NULL,
  `asset_id` varchar(50) DEFAULT NULL,
  `asset_group_id` varchar(50) DEFAULT NULL,
  `barang_id` varchar(50) DEFAULT NULL,
  `category_id` varchar(50) DEFAULT NULL,
  `docref` varchar(50) DEFAULT NULL,
  `docref_other` varchar(50) DEFAULT NULL,
  `asset_trans_name` varchar(100) DEFAULT NULL,
  `asset_trans_branch` varchar(100) DEFAULT NULL,
  `asset_trans_price` varchar(100) DEFAULT NULL,
  `asset_trans_new_price` varchar(50) DEFAULT NULL,
  `asset_trans_date` varchar(100) DEFAULT NULL,
  `description` tinytext,
  `class` varchar(50) DEFAULT NULL,
  `period` varchar(50) DEFAULT NULL,
  `tariff` varchar(50) DEFAULT NULL,
  `penyusutanperbulan` varchar(50) DEFAULT NULL,
  `penyusutanpertahun` varchar(50) DEFAULT NULL,
  `status` enum(`1`,`0`) DEFAULT NULL,
  `ati` varchar(50) DEFAULT NULL,
  `hide` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`asset_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `nscc_asset_periode`;
CREATE TABLE IF NOT EXISTS `nscc_asset_periode` (
  `asset_periode_id` varchar(50) NOT NULL,
  `asset_trans_id` varchar(50) NOT NULL,
  `asset_id` varchar(50) DEFAULT NULL,
  `asset_group_id` varchar(50) DEFAULT NULL,
  `barang_id` varchar(50) DEFAULT NULL,
  `docref` varchar(50) DEFAULT NULL,
  `docref_other` varchar(50) DEFAULT NULL,
  `asset_trans_name` varchar(100) DEFAULT NULL,
  `asset_trans_branch` varchar(100) DEFAULT NULL,
  `asset_trans_price` varchar(100) DEFAULT NULL,
  `asset_trans_new_price` varchar(50) DEFAULT NULL,
  `asset_trans_date` varchar(100) DEFAULT NULL,
  `description` tinytext,
  `class` varchar(50) DEFAULT NULL,
  `period` varchar(50) DEFAULT NULL,
  `tariff` varchar(50) DEFAULT NULL,
  `tglpenyusutan` varchar(50) DEFAULT NULL,
  `penyusutanperbulan` varchar(50) DEFAULT NULL,
  `penyusutanpertahun` varchar(50) DEFAULT NULL,
  `balance` varchar(50) DEFAULT NULL,
  `status` enum(`1`,`0`) DEFAULT NULL,
  `startdate` varchar(50) DEFAULT NULL,
  `enddate` varchar(50) DEFAULT NULL,
  `ati` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`asset_periode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `nscc_asset_detail_view`;
CREATE VIEW nscc_asset_detail_view as select a.*, ac.category_name as `category` from nscc_asset_detail a
join nscc_asset_category ac on a.category_id = ac.category_id
order by a.docref asc ;

DROP TABLE IF EXISTS `nscc_asset_view`;
CREATE VIEW nscc_asset_view as select a.*, ac.category_name as `category` from nscc_asset a
join nscc_asset_category ac on a.category_id = ac.category_id
order by a.doc_ref asc ;')->execute();
	}

	public function down()
	{
		echo "m180731_023009_allassettable does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}