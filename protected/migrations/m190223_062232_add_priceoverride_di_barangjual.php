<?php

class m190223_062232_add_priceoverride_di_barangjual extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
				ALTER 
			ALGORITHM=UNDEFINED 
			DEFINER=`root`@`127.0.0.1` 
			SQL SECURITY DEFINER 
			VIEW `nscc_barang_jual` AS 
			SELECT
			COALESCE (`nj`.`price`, 0) AS `price`,
			`nb`.`barang_id` AS `barang_id`,
			`nb`.`kode_barang` AS `kode_barang`,
			`nb`.`nama_barang` AS `nama_barang`,
			`nb`.`ket` AS `ket`,
			`nb`.`grup_id` AS `grup_id`,
			`nb`.`active` AS `active`,
			`nb`.`sat` AS `sat`,
			`nb`.`up` AS `up`,
			nb.price_override,
			`nb`.`tipe_barang_id` AS `tipe_barang_id`,
			`nj`.`store` AS `store`
			FROM
			(
			`nscc_barang` `nb`
				LEFT JOIN `nscc_jual` `nj` ON (
				(
				`nj`.`barang_id` = `nb`.`barang_id`
				)
			)
			) ;
		")->execute();

	}

	public function down()
	{
		echo "m190223_062232_add_priceoverride_di_barangjual does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}