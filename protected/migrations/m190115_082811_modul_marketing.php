<?php

class m190115_082811_modul_marketing extends CDbMigration
{
	public function up()
	{
		        Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS `nscc_divisi`;
CREATE TABLE `nscc_divisi` (
`divisi_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`name`  varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`up`  tinyint(1) NOT NULL DEFAULT 0 ,
`seq`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`hide`  tinyint(1) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`divisi_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;


DROP TABLE IF EXISTS `nscc_divisi_chart_types`;
CREATE TABLE `nscc_divisi_chart_types` (
`chart_types_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`name`  varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' ,
`up`  tinyint(1) NOT NULL DEFAULT 0 ,
`seq`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`hide`  tinyint(1) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`chart_types_id`),
INDEX `name` (`name`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;

DROP TABLE IF EXISTS `nscc_divisi_chart_master`;
CREATE TABLE `nscc_divisi_chart_master` (
`account_code`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
`divisi_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`chart_types_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`account_name`  varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`status`  tinyint(4) NOT NULL DEFAULT 1 ,
`description`  mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`header`  tinyint(4) NOT NULL DEFAULT 0 ,
`saldo_normal`  char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'D' ,
`tipe`  char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'L' ,
`up`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
PRIMARY KEY (`account_code`),
CONSTRAINT `fk1_divisi_id` FOREIGN KEY (`divisi_id`) REFERENCES `nscc_divisi` (`divisi_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
CONSTRAINT `fk2_chart_types_id` FOREIGN KEY (`chart_types_id`) REFERENCES `nscc_divisi_chart_types` (`chart_types_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
INDEX `fk1_divisi_id` (`divisi_id`) USING BTREE ,
INDEX `fk2_chart_types_id` (`chart_types_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;

DROP TABLE IF EXISTS `nscc_divisi_bank`;
CREATE TABLE `nscc_divisi_bank` (
`bank_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`nama_bank`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`ket`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`account_code`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`minimum`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`up`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`bank_id`),
CONSTRAINT `nscc_divisi_bank_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `nscc_divisi_chart_master` (`account_code`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `idx_nscc_bank` (`account_code`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;



DROP TABLE IF EXISTS `nscc_divisi_bank_trans`;
CREATE TABLE `nscc_divisi_bank_trans` (
`bank_trans_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`type_`  int(11) NOT NULL ,
`trans_no`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`ref`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`tgl`  date NOT NULL ,
`amount`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`id_user`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`tdate`  datetime NOT NULL ,
`bank_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`up`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`bank_trans_id`),
CONSTRAINT `nscc_divisi_bank_trans_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `nscc_divisi_bank` (`bank_id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `idx_nscc_bank_trans` (`bank_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;
  
 DROP TABLE IF EXISTS `nscc_divisi_gl_trans`; 
 CREATE TABLE `nscc_divisi_gl_trans` (
`counter`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`type`  smallint(6) NOT NULL DEFAULT 0 ,
`type_no`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`tran_date`  date NULL DEFAULT NULL ,
`memo_`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`amount`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`id_user`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`account_code`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`tdate`  datetime NOT NULL ,
`cf`  tinyint(4) NOT NULL DEFAULT 0 ,
`up`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`counter`),
CONSTRAINT `nscc_divisi_gl_trans_ibfk_1` FOREIGN KEY (`account_code`) REFERENCES `nscc_divisi_chart_master` (`account_code`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `idx_psn_gl_trans` (`id_user`) USING BTREE ,
INDEX `idx_psn_gl_trans_0` (`account_code`) USING BTREE ,
INDEX `tran_date` (`tran_date`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;

  
  
  
  
  DROP TABLE IF EXISTS `nscc_divisi_kas`;
  CREATE TABLE `nscc_divisi_kas` (
`kas_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`doc_ref`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`no_kwitansi`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`keperluan`  mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`total`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`bank_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`amount`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`tgl`  date NOT NULL ,
`user_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`tdate`  datetime NOT NULL ,
`type_`  tinyint(4) NOT NULL DEFAULT 0 ,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`up`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`arus`  tinyint(4) NOT NULL ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
`all_store`  tinyint(4) NOT NULL DEFAULT 0 ,
`jml_cabang`  int(11) NOT NULL DEFAULT 0 ,
`total_debit`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`total_kredit`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
PRIMARY KEY (`kas_id`),
CONSTRAINT `nscc_divisi_kas_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `nscc_divisi_bank` (`bank_id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `idx_nscc_kas` (`bank_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;

  
  
  
  
  
  DROP TABLE IF EXISTS `nscc_divisi_kas_detail`;
  CREATE TABLE `nscc_divisi_kas_detail` (
`kas_detail_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`item_name`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`total`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`kas_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`account_code`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`kas_detail_id`),
CONSTRAINT `nscc_divisi_kas_detail_ibfk_1` FOREIGN KEY (`kas_id`) REFERENCES `nscc_divisi_kas` (`kas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `nscc_divisi_kas_detail_ibfk_2` FOREIGN KEY (`account_code`) REFERENCES `nscc_divisi_chart_master` (`account_code`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `idx_nscc_kas_detail` (`kas_id`) USING BTREE ,
INDEX `idx_nscc_kas_detail_0` (`account_code`) USING BTREE ,
INDEX `nscc_kas_detail_fk3_idx` (`store`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;

  
  
  
  
  DROP TABLE IF EXISTS `nscc_divisi_kegiatan`;
  CREATE TABLE `nscc_divisi_kegiatan` (
`kegiatan_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`account_code`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
`bank_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`doc_ref`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`tgl`  date NOT NULL ,
`id_user`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`tdate`  datetime NOT NULL ,
`tgl_mulai_kegiatan`  date NOT NULL ,
`tgl_akhir_kegiatan`  date NOT NULL ,
`jml_pasien_baru`  int(11) NOT NULL ,
`jml_pasien_riil`  int(11) NULL DEFAULT NULL ,
`omset`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`budget`  decimal(30,2) NULL DEFAULT 0.00 ,
`total_biaya`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`total_biaya_real`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`arus`  tinyint(4) NOT NULL ,
`note`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
`up`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 ,
PRIMARY KEY (`kegiatan_id`),
CONSTRAINT `fk1` FOREIGN KEY (`account_code`) REFERENCES `nscc_divisi_chart_master` (`account_code`) ON DELETE RESTRICT ON UPDATE CASCADE,
CONSTRAINT `fk2` FOREIGN KEY (`bank_id`) REFERENCES `nscc_divisi_bank` (`bank_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
INDEX `fk1` (`account_code`) USING BTREE ,
INDEX `fk2` (`bank_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;



DROP TABLE IF EXISTS `nscc_divisi_kegiatan_detail`;
CREATE TABLE `nscc_divisi_kegiatan_detail` (
`kegiatan_detail_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`kegiatan_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`item_name`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`total`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`visible`  tinyint(4) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`kegiatan_detail_id`),
CONSTRAINT `fk_kegiatan_id` FOREIGN KEY (`kegiatan_id`) REFERENCES `nscc_divisi_kegiatan` (`kegiatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `fk_kegiatan_id` (`kegiatan_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;

				")->execute();
	}

	public function down()
	{
		echo "m190115_082811_modul_marketing does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}