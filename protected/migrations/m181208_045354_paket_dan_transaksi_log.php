<?php

class m181208_045354_paket_dan_transaksi_log extends CDbMigration
{
	public function up()
	{

		Yii::app()->db->createCommand("
		
-- ----------------------------
-- Table structure for nscc_paket_perawatan
                       -- ----------------------------
	-- DROP TABLE IF EXISTS `nscc_paket_perawatan`;
CREATE TABLE `nscc_paket_perawatan` (
	`paket_perawatan_id` varchar(36) NOT NULL,
  `barang_id` varchar(36) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `max_trans` int(2) DEFAULT NULL,
  `expired_day` int(3) DEFAULT NULL,
  `visible` tinyint(4) DEFAULT NULL COMMENT '1',
  PRIMARY KEY (`paket_perawatan_id`),
  UNIQUE KEY `barang_id` (`barang_id`) USING BTREE,
  CONSTRAINT `nscc_paket_perawatan_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nscc_paket_perawatan_details
                       -- ----------------------------
	-- DROP TABLE IF EXISTS `nscc_paket_perawatan_details`;
CREATE TABLE `nscc_paket_perawatan_details` (
	`paket_perawatan_details` varchar(36) NOT NULL,
  `paket_perawatan_id` varchar(36) DEFAULT NULL,
  `trans_ke` int(2) DEFAULT NULL,
  `payment_percent` int(3) DEFAULT NULL,
  `visible` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`paket_perawatan_details`),
  KEY `paket_perawatan_id` (`paket_perawatan_id`),
  CONSTRAINT `nscc_paket_perawatan_details_ibfk_1` FOREIGN KEY (`paket_perawatan_id`) REFERENCES `nscc_paket_perawatan` (`paket_perawatan_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nscc_paket_perawatan_log
                       -- ----------------------------
	-- DROP TABLE IF EXISTS `nscc_paket_perawatan_log`;
CREATE TABLE `nscc_paket_perawatan_log` (
	`paket_perawatan_log_id` varchar(36) NOT NULL,
  `customer_id` varchar(36) DEFAULT NULL,
  `barang_id` varchar(36) DEFAULT NULL,
  `jumlah_trans` int(11) DEFAULT NULL,
  `awal_trans` date DEFAULT NULL,
  `last_trans` date DEFAULT NULL,
  PRIMARY KEY (`paket_perawatan_log_id`),
  KEY `customer_id` (`customer_id`),
  KEY `barang_id` (`barang_id`),
  CONSTRAINT `nscc_paket_perawatan_log_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `nscc_customers` (`customer_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `nscc_paket_perawatan_log_ibfk_2` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nscc_paket_perawatan_log_trans
                       -- ----------------------------
	-- DROP TABLE IF EXISTS `nscc_paket_perawatan_log_trans`;
CREATE TABLE `nscc_paket_perawatan_log_trans` (
	`paket_perawatan_log_trans_id` varchar(36) NOT NULL,
  `paket_perawatan_log_id` varchar(36) DEFAULT NULL,
  `salestrans_id` varchar(36) NOT NULL,
  `return_salestrans_id` varchar(36) DEFAULT NULL,
  `salestrans_details` varchar(36) NOT NULL,
  `barang_id` varchar(36) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `disc` decimal(30,2) DEFAULT NULL,
  `discrp` decimal(30,2) DEFAULT NULL,
  `ketpot` varchar(255) DEFAULT NULL,
  `vat` decimal(30,2) DEFAULT NULL,
  `vatrp` decimal(30,2) DEFAULT NULL,
  `bruto` decimal(30,2) DEFAULT NULL,
  `total` decimal(30,2) DEFAULT NULL,
  `total_pot` decimal(30,2) DEFAULT NULL,
  `dokter_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`paket_perawatan_log_trans_id`),
  KEY `salestrans_details` (`salestrans_details`),
  KEY `barang_id` (`barang_id`),
  KEY `paket_perawatan_log_id` (`paket_perawatan_log_id`),
  CONSTRAINT `nscc_paket_perawatan_log_trans_ibfk_2` FOREIGN KEY (`salestrans_details`) REFERENCES `nscc_salestrans_details` (`salestrans_details`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `nscc_paket_perawatan_log_trans_ibfk_3` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `nscc_paket_perawatan_log_trans_ibfk_4` FOREIGN KEY (`paket_perawatan_log_id`) REFERENCES `nscc_paket_perawatan_log` (`paket_perawatan_log_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
DROP TRIGGER IF EXISTS `paket_perawatan_before_insert`;
DELIMITER ;;
CREATE TRIGGER `paket_perawatan_before_insert` BEFORE INSERT ON `nscc_paket_perawatan` FOR EACH ROW BEGIN
	IF NEW.paket_perawatan_id IS NULL OR LENGTH(NEW.paket_perawatan_id) = 0
    THEN
      SET NEW.paket_perawatan_id = UUID();
    END IF;
  END
  ;;
DELIMITER ;
DROP TRIGGER IF EXISTS `nscc_paket_perawtan_det_b4_insert`;
DELIMITER ;;
CREATE TRIGGER `nscc_paket_perawtan_det_b4_insert` BEFORE INSERT ON `nscc_paket_perawatan_details` FOR EACH ROW BEGIN
	IF NEW.paket_perawatan_details IS NULL OR LENGTH(NEW.paket_perawatan_details) = 0
    THEN
      SET NEW.paket_perawatan_details = UUID();
    END IF;
  END
  ;;
DELIMITER ;
DROP TRIGGER IF EXISTS `paket_perawatan_log_before_insert`;
DELIMITER ;;
CREATE TRIGGER `paket_perawatan_log_before_insert` BEFORE INSERT ON `nscc_paket_perawatan_log` FOR EACH ROW BEGIN
	IF NEW.paket_perawatan_log_id IS NULL OR LENGTH(NEW.paket_perawatan_log_id) = 0
    THEN
      SET NEW.paket_perawatan_log_id = UUID();
    END IF;
  END
  ;;
DELIMITER ;
DROP TRIGGER IF EXISTS `paket_perawatan_log_trans_before_insert`;
DELIMITER ;;
CREATE TRIGGER `paket_perawatan_log_trans_before_insert` BEFORE INSERT ON `nscc_paket_perawatan_log_trans` FOR EACH ROW BEGIN
	IF NEW.paket_perawatan_log_trans_id IS NULL OR LENGTH(NEW.paket_perawatan_log_trans_id) = 0
    THEN
      SET NEW.paket_perawatan_log_trans_id = UUID();
    END IF;
  END
  ;;
DELIMITER ;

		")->execute();
	}

	public function down()
	{
		echo "m181208_045354_paket_dan_transaksi_log does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}