<?php

class m190121_075051_tabel_master_bonus_dokter extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("DROP TABLE IF EXISTS `nscc_bonus_dokter_perawatan`;
CREATE TABLE `nscc_bonus_dokter_perawatan` (
  `barang_id` varchar(36) NOT NULL DEFAULT '',
  `kode` varchar(40) NOT NULL,
  `bonus` varchar(200) NOT NULL,
  PRIMARY KEY (`barang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")->execute();
	}

	public function down()
	{
		echo "m190121_075051_tabel_master_bonus_dokter does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}