<?php

class m181001_022243_transaksi_komen_di_receipt extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `nscc_resep_print`
			ADD COLUMN `tr_comment`  varchar(255) NULL AFTER `header5`;
		")->execute();

	}

	public function down()
	{
		echo "m181001_022243_transaksi_komen_di_receipt does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}