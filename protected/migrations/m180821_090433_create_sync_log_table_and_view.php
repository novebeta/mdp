<?php

class m180821_090433_create_sync_log_table_and_view extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand('
CREATE TABLE IF NOT EXISTS `nscc_sync_log` (
  `sync_id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `store` varchar(50) NOT NULL,
  `tdate` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT \'0\',
  `diff` int(11) NOT NULL,
  `note` text NOT NULL,
  `action` varchar(50) NOT NULL,
  `timestamps` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sync_id`)
) ENGINE=InnoDB AUTO_INCREMENT=654 DEFAULT CHARSET=latin1;

CREATE VIEW nscc_sync_log_view as
select user, store, tdate, max(timestamps) as timestamps, status, diff,  note, action from nscc_sync_log
group by store, tdate, action
order by store, tdate desc ;')->execute();
	}

	public function down()
	{
		echo "m180821_090433_create_sync_log_table_and_view does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}