<?php
class BodyPaintController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $is_new               = $_POST['mode'] == 0;
            $jasaDetils           = CJSON::decode($_POST['jasaDetils']);
            $partDetils           = CJSON::decode($_POST['partDetils']);
            app()->db->autoCommit = false;
            $transaction          = Yii::app()->db->beginTransaction();
            try {
                if (!$is_new) {
                    BpJasa::model()->deleteAll('body_paint_id = :body_paint_id', [':body_paint_id' => $_POST['id']]);
                    if (intval($_POST['mode']) < 3) {
                        BpParts::model()->deleteAll('body_paint_id = :body_paint_id', [':body_paint_id' => $_POST['id']]);
                    }
                }
                /** @var BodyPaint $model */
                $model = $is_new ? new BodyPaint : $this->loadModel($_POST['id'], 'BodyPaint');
                if ($is_new) {
                    $model->est_tgl = $_POST['est_tgl'];
                    $model->est_no  = new CDbExpression(
                        "fnc_ref_bp_est('" . $_POST['est_tgl'] . "')");
                    unset($_POST['est_tgl']);
                    unset($_POST['est_no']);
                } else {
                    unset($_POST['est_tgl']);
                    unset($_POST['est_no']);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['BodyPaint'][$k] = $v;
                }
                $model->attributes = $_POST['BodyPaint'];
                if ($_POST['mode'] == 2) {
                    $model->wo_no  = new CDbExpression('fnc_ref_bp_wo(NOW())');
                    $model->wo_tgl = new CDbExpression('NOW()');
                }
//                $model->wo_tgl     = null;
                $model->vat = floatval(yiiparam('ppn',10));
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'BodyPaint')) . CHtml::errorSummary($model));
                }
                foreach ($jasaDetils as $detil) {
                    $salesD                  = new BpJasa();
                    $salesD->body_paint_id   = $model->body_paint_id;
                    $salesD->jasa_bp_id      = $detil['jasa_bp_id'];
                    $salesD->tipe            = $detil['tipe'];
                    $salesD->note            = $detil['note'];
                    $salesD->hpp1            = get_number($detil['hpp1']);
                    $salesD->hpp2            = get_number($detil['hpp2']);
                    $salesD->harga           = get_number($detil['harga']);
                    $salesD->discrp          = get_number($detil['discrp']);
                    $salesD->disc            = get_number($detil['disc']);
                    $salesD->dpp             = get_number($detil['dpp']);
                    $salesD->ppn_ind         = get_number($detil['ppn_ind']);
                    $salesD->ppn_out         = get_number($detil['ppn_out']);
                    $salesD->ppn             = get_number($detil['ppn']);
                    $salesD->total_jasa_line = get_number($detil['total_jasa_line']);
                    if (!$salesD->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Jasa Detail')) . CHtml::errorSummary($salesD));
                    }
                }
                if ($_POST['mode'] < 3) {
                    foreach ($partDetils as $detil) {
                        $salesD                   = new BpParts();
                        $salesD->body_paint_id    = $model->body_paint_id;
                        $salesD->kode             = $detil['kode'];
                        $salesD->nama             = $detil['nama'];
                        $salesD->hpp              = get_number($detil['hpp']);
                        $salesD->harga            = get_number($detil['harga']);
                        $salesD->qty              = get_number($detil['qty']);
                        $salesD->total_parts_line = get_number($detil['total_parts_line']);
                        if (!$salesD->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Parts Detail')) . CHtml::errorSummary($salesD));
                        }
                    }
                }
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg    = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg'     => $msg, //,
                //'print' => $print
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'BodyPaint');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['BodyPaint'][$k] = $v;
            }
            $msg               = "Data gagal disimpan";
            $model->attributes = $_POST['BodyPaint'];
            if ($model->save()) {
                $status = true;
                $msg    = "Data berhasil di simpan dengan id " . $model->body_paint_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg'     => $msg,
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->body_paint_id));
            }
        }
    }
    public function actionCreateWO($id)
    {
        /** @var BodyPaint $model */
        $model         = $this->loadModel($id, 'BodyPaint');
        $msg           = "Data gagal disimpan";
        $model->wo_no  = new CDbExpression('fnc_ref_bp_wo(NOW())');
        $model->wo_tgl = new CDbExpression('NOW()');
        if ($model->save()) {
            $status = true;
            $msg    = "Data berhasil di buat WO ";
        } else {
            $msg .= " " . Chtml::errorSummary($model);
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg'     => $msg,
        ));
        Yii::app()->end();
    }
    public function actionCreateInv($id)
    {
        app()->db->autoCommit = false;
        $transaction          = Yii::app()->db->beginTransaction();
        try {
            /** @var BodyPaint $model */
            $model          = $this->loadModel($id, 'BodyPaint');
            $model->inv_no  = new CDbExpression('fnc_ref_bp_inv(NOW())');
            $model->inv_tgl = new CDbExpression('NOW()');
            if (!$model->save()) {
                throw new Exception(CHtml::errorSummary($model));
            }
            $model->refresh();
            # start gl total invoice
            $gl  = new GL();
            $ref = $model->inv_no;
            $gl->add_gl(BODYPAINT_CREATE_INV, $model->body_paint_id, $model->inv_tgl,
                $ref, '11-05-30', "Body Paint INV $ref",
                "Body Paint INV  $ref", $model->grand_total, 0, '');
            $gl->add_gl(BODYPAINT_CREATE_INV, $model->body_paint_id, $model->inv_tgl,
                $ref, '41-00-03', "Body Paint INV $ref",
                "Body Paint INV $ref", -$model->total, 0, '');
            $gl->add_gl(BODYPAINT_CREATE_INV, $model->body_paint_id, $model->inv_tgl,
                $ref, '21-02-03', "Body Paint INV $ref",
                "Body Paint INV $ref", -$model->vatrp, 0, '');
            $gl->validate();
            # end gl total invoice
            # posting all jasa
            foreach ($model->bpJasas as $rJasa) {
                $hpp = floatval($rJasa->hpp1) + floatval($rJasa->hpp2);
//                $pph23    = $hpp * 0.02;
                $hutang = $hpp;
                $ppn = 0;
                if($rJasa->ppn_out == 1){
                    $ppn    = $hpp * floatval(yiiparam('ppn',10)/100);
                    $hutang = $hpp + $ppn;
                }
                $gl  = new GL();
                $ref = $model->inv_no;
                /**  # jurnal versi lama #
                $gl->add_gl( BODYPAINT_JASA, $rJasa->bp_jasa_id, $model->inv_tgl,
                $ref, $rJasa->jasaBp->overhead, "Body Paint INV $ref " . $rJasa->jasaBp->nama,
                "Body Paint INV  $ref " . $rJasa->jasaBp->nama, $overhead, 0, '' );
                $gl->add_gl( BODYPAINT_JASA, $rJasa->bp_jasa_id, $model->inv_tgl,
                $ref, $rJasa->jasaBp->account_code, "Body Paint INV $ref " . $rJasa->jasaBp->nama,
                "Body Paint INV $ref " . $rJasa->jasaBp->nama, - $hpp, 0, '' );
                $gl->add_gl( BODYPAINT_JASA, $rJasa->bp_jasa_id, $model->inv_tgl,
                $ref, '21-02-04', "Body Paint INV $ref " . $rJasa->jasaBp->nama,
                "Body Paint INV $ref " . $rJasa->jasaBp->nama, - $pph23, 0, '' );
                 */

                $gl->add_gl(BODYPAINT_JASA, $rJasa->bp_jasa_id, $model->inv_tgl,
                    $ref, $rJasa->jasaBp->overhead, "Body Paint INV $ref " . $rJasa->jasaBp->nama,
                    "Body Paint INV  $ref " . $rJasa->jasaBp->nama, $hpp, 0, '');
                $gl->add_gl(BODYPAINT_JASA, $rJasa->bp_jasa_id, $model->inv_tgl,
                    $ref, '11-09-03', "Body Paint INV $ref " . $rJasa->jasaBp->nama,
                    "Body Paint INV $ref " . $rJasa->jasaBp->nama, $ppn, 0, '');
                $gl->add_gl(BODYPAINT_JASA, $rJasa->bp_jasa_id, $model->inv_tgl,
                    $ref, $rJasa->jasaBp->account_code, "Body Paint INV $ref " . $rJasa->jasaBp->nama,
                    "Body Paint INV $ref " . $rJasa->jasaBp->nama, -$hutang, 0, '');
                $gl->validate();
            }
            $msg = "Data berhasil di buat Invoice ";
            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg    = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg'     => $msg, //,
            //'print' => $print
        ));
        Yii::app()->end();
    }
    public function actionClose($id)
    {
        app()->db->autoCommit = false;
        $transaction          = Yii::app()->db->beginTransaction();
        try {
            /** @var BodyPaint $model */
            $model          = $this->loadModel($id, 'BodyPaint');
            $model->inv_no  = new CDbExpression('NULL');
            $model->inv_tgl = new CDbExpression('NULL');
            if (!$model->save()) {
                throw new Exception(CHtml::errorSummary($model));
            }
            $model->refresh();
            # start gl total invoice
            GlTrans::model()->deleteAllByAttributes([
                'type'    => BODYPAINT_CREATE_INV,
                'type_no' => $model->body_paint_id,
            ]);
            # posting all jasa
            foreach ($model->bpJasas as $rJasa) {
                GlTrans::model()->deleteAllByAttributes([
                    'type'    => BODYPAINT_JASA,
                    'type_no' => $rJasa->bp_jasa_id,
                ]);
            }
            $msg = "Data berhasil di buat Invoice ";
            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg    = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg'     => $msg, //,
            //'print' => $print
        ));
        Yii::app()->end();
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg                  = 'Data berhasil dihapus.';
            $status               = true;
            app()->db->autoCommit = false;
            $transaction          = Yii::app()->db->beginTransaction();
            try {
                /** @var BodyPaint $model */
                $model          = $this->loadModel($id, 'BodyPaint');
                $model->deleted = new CDbExpression('now()');
                if ($model->save()) {
                    $status = true;
                    $msg    = "Data berhasil di delete ";
                } else {
                    $msg .= " " . Chtml::errorSummary($model);
                    $status = false;
                }
                foreach ($model->bpParts as $part) {
                    if ($part->posting != null || $part->posting != '') {
                        GlTrans::model()->deleteAll("type_no = :type_no AND type = :type", [
                            ':type_no' => $part->bp_parts_id,
                            ':type'    => BODYPAINT_PARTS,
                        ]);
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $status = false;
                $msg    = $ex;
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg'     => $msg,
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('deleted is null');
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit  = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['no_pol']) && $_POST['no_pol'] != '') {
            $criteria->addCondition("m.no_pol = :nopol");
            $criteria->params[':nopol'] = $_POST['no_pol'];
        }
        $criteria->with = [
            'mobil' => [
                'alias'  => 'm',
                'select' => 'm.no_pol',
            ],
        ];
        $criteria->addCondition('wo_no is NULL');
        $model = BodyPaint::model()->findAll($criteria);
        $total = BodyPaint::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexWO()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('deleted is null');
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit  = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['no_pol']) && $_POST['no_pol'] != '') {
            $criteria->addCondition("m.no_pol = :nopol");
            $criteria->params[':nopol'] = $_POST['no_pol'];
        }
        $criteria->with = [
            'mobil' => [
                'alias'  => 'm',
                'select' => 'm.no_pol',
            ],
        ];
        $criteria->addCondition('wo_no is NOT NULL AND inv_no IS NULL');
        $model = BodyPaint::model()->findAll($criteria);
        $total = BodyPaint::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexInv()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('deleted is null');
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit  = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['no_pol']) && $_POST['no_pol'] != '') {
            $criteria->addCondition("m.no_pol = :nopol");
            $criteria->params[':nopol'] = $_POST['no_pol'];
        }
        if (isset($_POST['asuransi_bp_id']) && $_POST['asuransi_bp_id'] != '') {
            $criteria->addCondition("asuransi_bp_id = :asuransi_bp_id");
            $criteria->params[':asuransi_bp_id'] = $_POST['asuransi_bp_id'];
        }
        if (isset($_POST['status']) && $_POST['status'] != '') {
            if ($_POST['status'] == 'SUDAH') {
                $criteria->addCondition("lunas is not null");
            } else {
                $criteria->addCondition("lunas is null");
            }
        }
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $criteria->addCondition("inv_tgl >= :from AND inv_tgl <= :to");
            $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
            $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        }
        $criteria->with = [
            'mobil' => [
                'alias'  => 'm',
                'select' => 'm.no_pol',
            ],
        ];
        $criteria->addCondition('inv_no IS NOT NULL');
        $model = BodyPaint::model()->findAll($criteria);
        $total = BodyPaint::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexClose()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('deleted is null');
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit  = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['no_pol']) && $_POST['no_pol'] != '') {
            $criteria->addCondition("m.no_pol = :nopol");
            $criteria->params[':nopol'] = $_POST['no_pol'];
        }
        if (isset($_POST['asuransi_bp_id']) && $_POST['asuransi_bp_id'] != '') {
            $criteria->addCondition("asuransi_bp_id = :asuransi_bp_id");
            $criteria->params[':asuransi_bp_id'] = $_POST['asuransi_bp_id'];
        }
        $criteria->addCondition("lunas is null");
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $criteria->addCondition("inv_tgl >= :from AND inv_tgl <= :to");
            $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
            $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        }
        $criteria->with = [
            'mobil' => [
                'alias'  => 'm',
                'select' => 'm.no_pol',
            ],
        ];
        $criteria->addCondition('b1_tgl IS NULL AND b2_tgl IS NULL AND b3_tgl IS NULL');
        $model = BodyPaint::model()->findAll($criteria);
        $total = BodyPaint::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionBayar($id)
    {
        /** @var BodyPaint $model */
        $model = $this->loadModel($id, 'BodyPaint');
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction          = Yii::app()->db->beginTransaction();
            try {
                if ($model->lunas != null) {
                    throw new Exception("Sudah lunas.");
                }
                if ($_POST['modez'] == 1) {
                    $model->b1_tgl     = $_POST['b1_tgl'];
                    $model->b1_bank_id = $_POST['b1_bank_id'];
                    $model->b1_qty     = get_number($_POST['qty_kejadian']) + floatval($model->b1_qty);
                    $model->b1_total   = get_number($_POST['b1_total']) + floatval($model->b1_total);
                    $total_pembayaran  = floatval($model->b1_total) + floatval($model->b2_total) + floatval($model->b4_total);
                    if ($total_pembayaran > $model->grand_total) {
                        throw new Exception("Pembayaran tidak bisa lebih besar dari total.");
                    }
                    if ($total_pembayaran == $model->grand_total) {
                        $model->lunas = $model->b1_tgl;
                    }
                    $msg = "Data berhasil disimpan";
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                            ['{model}' => 'BodyPaint']) . CHtml::errorSummary($model));
                    }
                    $gl   = new GL();
                    $ref  = $model->inv_no;
                    $bank = Bank::model()->findByPk($model->b1_bank_id);
                    $gl->add_gl(BODYPAINT_BAYAR_1, $model->body_paint_id, $model->b1_tgl,
                        $ref, $bank->account_code, "BAYAR 1 INV $ref",
                        "BAYAR 1 INV $ref", $model->b1_total, 0, '');
                    $gl->add_bank_trans(BODYPAINT_BAYAR_1, $model->body_paint_id, $bank->bank_id,
                        $ref, $model->b1_tgl, $model->b1_total, Yii::app()->user->getId(), '');
                    $gl->add_gl(BODYPAINT_BAYAR_1, $model->body_paint_id, $model->b1_tgl,
                        $ref, '11-05-30', "BAYAR 1 INV $ref",
                        "BAYAR 1 INV $ref", -$model->b1_total, 1, '');
                    $gl->validate();
                } elseif ($_POST['modez'] == 4) {
                    $model->b4_tgl = $_POST['b1_tgl'];
//                    $model->b4_bank_id = $_POST[ 'b1_bank_id' ];
                    $model->b4_qty    = get_number($_POST['qty_kejadian']) + floatval($model->b4_qty);
                    $model->b4_total  = get_number($_POST['b1_total']) + floatval($model->b4_total);;
                    $total_pembayaran = floatval($model->b1_total) + floatval($model->b2_total) + floatval($model->b4_total);
                    if ($total_pembayaran > $model->grand_total) {
                        throw new Exception("Pembayaran tidak bisa lebih besar dari total.");
                    }
                    if ($total_pembayaran == $model->grand_total) {
                        $model->lunas = $model->b4_tgl;
                    }
                    $msg = "Data berhasil disimpan";
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                            ['{model}' => 'BodyPaint']) . CHtml::errorSummary($model));
                    }
                    $gl  = new GL();
                    $ref = $model->inv_no;
//                    $bank = Bank::model()->findByPk( $model->b1_bank_id );
                    $gl->add_gl(BODYPAINT_BAYAR_4, $model->body_paint_id, $model->b4_tgl,
                        $ref, '62-00-46', "BAYAR 4 INV $ref",
                        "BAYAR 4 INV $ref", $model->b4_total, 0, '');
                    $gl->add_gl(BODYPAINT_BAYAR_4, $model->body_paint_id, $model->b4_tgl,
                        $ref, '11-05-30', "BAYAR 4 INV $ref",
                        "BAYAR 4 INV $ref", -$model->b4_total, 0, '');
                    $gl->validate();
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg    = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg'     => $msg,
            ));
            Yii::app()->end();
        }
    }
    public function actionBayar2()
    {
        if (isset($_POST) && !empty($_POST)) {
            $detils               = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction          = Yii::app()->db->beginTransaction();
            try {
                $model                 = new BpBayar2();
                $model->ref            = new CDbExpression('fnc_ref_bp_bayar_2(:tgl)', [':tgl' => $_POST['tgl']]);
                $model->tgl            = $_POST['tgl'];
                $model->asuransi_bp_id = $_POST['asuransi_bp_id'];
                $model->total          = get_number($_POST['total']);
                $model->tagih          = intval(get_number($_POST['totalTagihan']));
                $model->bank_id        = $_POST['bank_id'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Woug')) . CHtml::errorSummary($model));
                }
                $model->refresh();
                foreach ($detils as $row) {
                    /** @var BodyPaint $sales */
                    $sales = BodyPaint::model()->findByPk($row['body_paint_id']);
                    if ($sales->lunas != null) {
                        throw new Exception("Sudah lunas.");
                    }
                    if ($sales->bp_bayar_id != null) {
                        throw new Exception("Sudah dibayar.");
                    }
                    $sales->b2_tgl      = $_POST['tgl'];
                    $sales->b2_bank_id  = $_POST['bank_id'];
                    $sales->b2_total    = floatval($row['total']);
                    $sales->bp_bayar_id = $model->bp_bayar_id;
                    if ((floatval($sales->b1_total) + floatval($sales->b2_total) + floatval($sales->b4_total)) > $sales->grand_total) {
                        throw new Exception("Pembayaran tidak bisa lebih besar dari total.");
                    }
                    if ((floatval($sales->b1_total) + floatval($sales->b2_total) + floatval($sales->b4_total)) == $sales->grand_total) {
                        $sales->lunas = $sales->b2_tgl;
                    }
                    if (!$sales->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'BodyPaint')) . CHtml::errorSummary($sales));
                    }

                }

                $gl    = new GL();
                $ref   = $model->ref;
                $bank  = Bank::model()->findByPk($model->bank_id);
                $pph23 = floatval($model->tagih) - floatval($model->total);
                if ($pph23 < 0) {
                    throw new Exception("Total tidak boleh lebih besar dari tagihan.");
                }
                $gl->add_gl(BODYPAINT_BAYAR_2, $model->bp_bayar_id, $model->tgl,
                    $ref, $bank->account_code, "BAYAR 2 INV $ref",
                    "BAYAR 2 INV $ref", $model->total, 0, '');
                $gl->add_bank_trans(BODYPAINT_BAYAR_2, $model->bp_bayar_id, $bank->bank_id,
                    $ref, $model->tgl, $model->total, Yii::app()->user->getId(), '');
                if ($pph23 > 0) {
                    $gl->add_gl(BODYPAINT_BAYAR_2, $model->bp_bayar_id, $model->tgl,
                        $ref, SysPrefs::get_val('PPH23'), "BAYAR 2 INV $ref",
                        "BAYAR 2 INV $ref", $pph23, 0, '');
                }
                $gl->add_gl(BODYPAINT_BAYAR_2, $model->bp_bayar_id, $model->tgl,
                    $ref, '11-05-30', "BAYAR 2 INV $ref",
                    "BAYAR 2 INV $ref", -$model->tagih, 1, '');
                $gl->validate();

                $msg = "Data berhasil disimpan";
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg    = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg'     => $msg,
            ));
            Yii::app()->end();
        }
    }
    protected function buktiTerima($nomor, $tgl, $dari, $total, $note, $bank_id, $items)
    {
        $this->layout = 'a5lanscape';
        $this->render('BuktiTerima', [
            'nomor'   => $nomor,
            'tgl'     => $tgl,
            'dari'    => $dari,
            'total'   => $total,
            'note'    => $note,
            'bank_id' => $bank_id,
            'items'   => $items,
        ]);
    }
    public function actionPrintBuktiTerimaPribadi($id)
    {
        /** @var BpBayar3 $model */
        $model = BpBayar3::model()->findByPk($id);
        /** @var BodyPaint $invoice */
        $invoice = BodyPaint::model()->findByPk($model->body_paint_id);
        $items[] = [
            'no_inv'        => $invoice->inv_no,
            'nama_customer' => $invoice->mobil->customer->nama_customer,
            'no_pol'        => $invoice->mobil->no_pol,
            'total'         => $model->total,
        ];
        $this->buktiTerima($invoice->inv_no, $model->tgl, $invoice->mobil->customer->nama_customer, $model->total,
            "TITIPAN PEMBAYARAN SERVICE BODY REPAIR (OWN RISK)", $model->bank_id, $items);
    }
    public function actionPrintBuktiTerima($id)
    {
        /** @var BpBayar2 $model */
        $model = BpBayar2::model()->findByPk($id);
        /** @var AsuransiBp $asuransi */
        $asuransi = AsuransiBp::model()->findByPk($model->asuransi_bp_id);
        /** @var BodyPaint[] $invoice */
        $invoice = BodyPaint::model()->findAllByAttributes([
            'bp_bayar_id' => $model->bp_bayar_id,
        ]);
        $items = [];
        foreach ($invoice as $item) {
            $items[] = [
                'no_inv'        => $item->inv_no,
                'nama_customer' => $item->mobil->customer->nama_customer,
                'no_pol'        => $item->mobil->no_pol,
                'total'         => $item->b2_total,
            ];
        }
        $this->buktiTerima($model->ref, $model->tgl, $asuransi->nama, $model->total,
            "PENERIMAAN FAKTUR SERVICE BODY REPAIR", $model->bank_id, $items);
    }
    public function actionPrintPass($id)
    {
        $this->layout = 'a5lanscape';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('PrintPass', ['model' => $model]);
    }
    public function actionPrintOwnRisk($id)
    {
        $this->layout = 'a5lanscape';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('PrintOwnRisk', ['model' => $model]);
    }
    public function actionPrintKwitansiPribadi($id)
    {
        $this->layout = 'a5lanscape';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('KwitansiPribadi', ['model' => $model]);
    }
    public function actionPrintKwitansiAsuransi($id)
    {
        $this->layout = 'a4';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('KwitansiAsuransi', ['model' => $model]);
    }
    public function actionPrintEstimasi($id)
    {
        $this->layout = 'a4';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('EstimasiBodyPaint', ['model' => $model]);
    }
    public function actionPrintWorkOrder($id)
    {
        $this->layout = 'a4';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('WorkOrderBodyPaint', ['model' => $model]);
    }
    public function actionPrintInvoice($id)
    {
        $this->layout = 'a4';
        $model        = $this->loadModel($id, 'BodyPaint');
        $this->render('InvoiceBodyPaint', ['model' => $model]);
    }
}
