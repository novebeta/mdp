<?php
class BpBayarJasaController extends GxController {
	public function actionCreate() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				$model               = new BpBayarJasa();
				$model->ref          = new CDbExpression(
					"fnc_ref_bp_bayar_jasa('" . $_POST['tgl'] . "')" );
				$model->tgl          = $_POST['tgl'];
				$model->total        = get_number( $_POST['total'] );
				$model->total_bayar  = get_number( $_POST['total_bayar'] );
				$model->bank_id      = $_POST['bank_id'];
				$model->account_code = $_POST['account_code'];
				$model->note         = $_POST['note'];
				$model->pph23         = ($model->total == $model->total_bayar) ? 0 : 1;
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'BpBayarJasa' ) ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				$gl   = new GL();
				$ref  = $model->ref;
				$bank = Bank::model()->findByPk( $model->bank_id );
				foreach ( $detils as $row ) {
					/** @var BpJasa $sales */
					$sales = BpJasa::model()->findByPk( $row['bp_jasa_id'] );
					if ( $sales->bp_bayar_jasa_id != null ) {
						throw new Exception( "Sudah dibayar." );
					}
					$sales->bp_bayar_jasa_id = $model->bp_bayar_jasa_id;
					if ( ! $sales->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'BpBayarJasa' ) ) . CHtml::errorSummary( $sales ) );
					}
					$hpp = floatval( $sales->hpp1 ) + floatval( $sales->hpp2 );
					$ppn = floatval( $sales->ppn_out );
					$gl->add_gl( BODYPAINT_BAYAR_SUPP_JASA, $model->bp_bayar_jasa_id, $model->tgl,
						$ref, $row['account_code'], "BAYAR JASA KE SUPPLIER - INV $ref",
						"BAYAR JASA KE SUPPLIER - INV $ref", ($hpp + ($hpp*floatval(yiiparam('ppn',10)/100)*$ppn) ), 1, '' );
//					$gl   = new GL();
//					$ref  = $sales->inv_no;
//					$bank = Bank::model()->findByPk( $sales->b2_bank_id );
//					$gl->add_gl( BODYPAINT_BAYAR_2, $sales->body_paint_id, $sales->b2_tgl,
//						$ref, $bank->account_code, "BAYAR 2 INV $ref",
//						"BAYAR 2 INV $ref", $sales->b2_total, 0, '' );
//					$gl->add_bank_trans( BODYPAINT_BAYAR_2, $sales->body_paint_id, $bank->bank_id,
//						$ref, $sales->b2_tgl, $sales->b2_total, Yii::app()->user->getId(), '' );
//					$gl->add_gl( BODYPAINT_BAYAR_2, $sales->body_paint_id, $sales->b2_tgl,
//						$ref, '11-05-30', "BAYAR 2 INV $ref",
//						"BAYAR 2 INV $ref", - $sales->b2_total, 1, '' );
//					$gl->validate();
				}
				$kas = $model->total_bayar;
				// if($model->getSupplierName() !== 'LAINNYA') {
				if($model->pph23 == 1) {
					// $tanpa_ppn = floatval( $model->total ) / 1.1;
					// $pph       = $tanpa_ppn * 0.02;
					// $kas       = ( $model->total ) - $pph;
					$pph       = $model->total - $model->total_bayar;
					$gl->add_gl( BODYPAINT_BAYAR_SUPP_JASA, $model->bp_bayar_jasa_id, $model->tgl,
						$ref, '21-02-01', "BAYAR JASA KE SUPPLIER - INV $ref ",
						"BAYAR JASA KE SUPPLIER - INV $ref", - $pph, 0, '' );
				}
				$gl->add_gl( BODYPAINT_BAYAR_SUPP_JASA, $model->bp_bayar_jasa_id, $model->tgl,
					$ref, $bank->account_code, "BAYAR JASA KE SUPPLIER - INV $ref",
					"BAYAR JASA KE SUPPLIER - INV $ref", - $kas, 0, '' );
				$gl->add_bank_trans( BODYPAINT_BAYAR_SUPP_JASA, $model->bp_bayar_jasa_id, $bank->bank_id,
					$ref, $model->tgl, - $kas, Yii::app()->user->getId(), '' );
				$gl->validate();
				$msg = "Data berhasil disimpan";
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'BpBayarJasa' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['BpBayarJasa'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['BpBayarJasa'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bp_bayar_jasa_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->bp_bayar_jasa_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'BpBayarJasa' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $criteria->addCondition("tgl >= :from AND tgl <= :to");
            $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
            $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        }
		$model = BpBayarJasa::model()->findAll( $criteria );
		$total = BpBayarJasa::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionList() {
		$bp_bayar_jasa_id = null;
		$account_code     = null;
		if ( isset( $_POST['bp_bayar_jasa_id'] ) ) {
			$bp_bayar_jasa_id = $_POST['bp_bayar_jasa_id'];
		}
		if ( isset( $_POST['account_code'] ) ) {
			$account_code = $_POST['account_code'];
		}
		$data = BpBayarJasa::getList( $account_code, $bp_bayar_jasa_id );
		$this->renderJsonArrWithTotal( $data, sizeof( $data ) );
	}

	public function actionPrint($id) {
		$this->layout = 'a5lanscape';
		$model        = $this->loadModel( $id, 'BpBayarJasa' );
		$this->render( 'BuktiKas', [ 'model' => $model ] );
	}
}