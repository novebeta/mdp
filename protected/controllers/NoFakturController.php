<?php

class NoFakturController extends GxController
{

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data berhasil disimpan.";
            $status = true;
            $jml = get_number($_POST['jml_nofaktur']);
            $awal = $_POST['no_faktur_awal'];
            $len = strlen($awal);
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $data = [];
                // $multi = new CDbMultiInsertCommand(NoFaktur::class);
                for ($i = 0; $i < $jml; $i++) {
                    $model = new NoFaktur();
                    $command = Yii::app()->db->createCommand("SELECT UUID();");
                    $uuid = $command->queryScalar();
                    $model->no_faktur_id = $uuid;
                    $model->no_faktur = str_pad(intval($awal) + $i, $len, '0', STR_PAD_LEFT);;
                    $model->mulai_berlaku = $_POST['mulai_berlaku'];
                    $model->created_at = time();
                    $model->created_user = Yii::app()->user->getId();
                    $data[] = $model->getAttributes();
                }
                Yii::app()->db->getCommandBuilder()
                    ->createMultipleInsertCommand('{{no_faktur}}', $data)
                    ->execute();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg'     => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'NoFaktur');

        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }

                $_POST['NoFaktur'][$k] = $v;
            }
            $msg               = "Data gagal disimpan";
            $model->attributes = $_POST['NoFaktur'];

            if ($model->save()) {

                $status = true;
                $msg    = "Data berhasil di simpan dengan id " . $model->no_faktur_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg'     => $msg,
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->no_faktur_id));
            }
        }
    }
    public function actionGenerate()
    {
        $status = false;
        $msg = 'Gagal generate faktur';
        $req = Yii::app()->request;
        $tipe = $req->getPost('tipe');
        $data = null;
        $criteria=new CDbCriteria;
        $tglAttr = $idAttr = '';
        if (!empty($tipe)) {
            switch ($tipe) {
                case 1: //flazz
                    $criteria->condition='lunas is NOT NULL AND no_faktur is null';
                    $criteria->order = 'tgl ASC';
                    $data = SalesFlazz::model()->findAll($criteria);
                    $tglAttr = 'tgl';
                    $idAttr = 'sales_flazz_id';
                    break;
                case 2: //bodypaint
                    $criteria->condition='lunas is NOT NULL AND deleted is null AND no_faktur is null';
                    $criteria->order = 'inv_tgl ASC';
                    $data = BodyPaint::model()->findAll($criteria);
                    $tglAttr = 'inv_tgl';
                    $idAttr = 'body_paint_id';
                    break;
            }
        }
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach($data as $inv){
                $tglInv = $inv->getAttribute($tglAttr);
                $idTrans = $inv->getAttribute($idAttr);
                $noFaktur = NoFaktur::getNoFaktur($tglInv);
                if(empty($noFaktur)) continue;
                $noFaktur->tipe_trans = $tipe;
                $noFaktur->id_trans = $idTrans;
                $noFaktur->modified_at = time();
                $noFaktur->modified_user = Yii::app()->user->getId();
                $noFaktur->save();
                $inv->no_faktur = $noFaktur->no_faktur;
                $inv->save();
            }
            $transaction->commit();
            $msg = 'Berhasil generate faktur';
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg'     => $msg
        ));
        Yii::app()->end();
    }

    public function actionExportCsv(){
        $this->layout = 'plain';
		Yii::import('ext.ECSVExport');
        $header1 = self::setDataCsv([
            'LT','NPWP','NAMA','JALAN','BLOK','NOMOR','RT','RW','KECAMATAN','KELURAHAN','KABUPATEN','PROPINSI','KODE_POS','NOMOR_TELEPON'
        ]);
        $header2 = self::setDataCsv([
            'OF','KODE_OBJEK','NAMA','HARGA_SATUAN','JUMLAH_BARANG','HARGA_TOTAL','DISKON','DPP','PPN','TARIF_PPPBM','PPNBM'
        ]);
        $objPajak = self::setDataCsv([
            'FAPR','CV MERDEKA DWI PUTRA','JL. KURINCI NO 168 RT 001 RW 001','PODOSUGIH'
        ]);
        $data = [
            $header1, $header2
        ];
        # transaksi merdeka / flazz
        $criteria = new CDbCriteria();
        $criteria->addCondition("tgl >= :from AND tgl <= :to AND lunas is not null and no_faktur is not null");
        $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
        $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        $salesFalzz = SalesFlazz::model()->findAll($criteria);
        foreach($salesFalzz as $item){
            $dTgl = DateTime::createFromFormat('Y-m-d', $item->tgl);
            $data[] = self::setDataCsv([
                'FK','01','0',$item->no_faktur, $dTgl->format('m'), $dTgl->format('Y'),$dTgl->format('m/d/Y'),$item->customer->npwp,
                $item->customer->nama_customer,$item->customer->alamat, $item->bruto, $item->vatrp,
                0,0,0,0,0,0,$item->doc_ref.' - '.$item->customer->nama_customer
            ]);
            $data[] = $objPajak;
        }
        # transaksi bodypaint
        $criteria = new CDbCriteria();
        $criteria->addCondition("inv_tgl is not null and inv_no is not null and inv_tgl >= :from AND inv_tgl <= :to AND lunas is not null and no_faktur is not null and deleted is null");
        $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
        $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        $bodypaints = BodyPaint::model()->findAll($criteria);
        foreach($bodypaints as $item){
            $dTgl = DateTime::createFromFormat('Y-m-d', $item->inv_tgl);
            $data[] = self::setDataCsv([
                'FK','01','0',$item->no_faktur, $dTgl->format('m'), $dTgl->format('Y'),$dTgl->format('m/d/Y'),$item->asuransi ? $item->asuransi->npwp : $item->customer->npwp,
                $item->asuransi ? $item->asuransi->nama : $item->customer->nama_customer,$item->asuransi ? $item->asuransi->address : $item->customer->alamat,
                $item->total, $item->vatrp,
                0,0,0,0,0,0,$item->inv_no.' - '.$item->customer->nama_customer
            ]);
            $data[] = $objPajak;
            foreach($item->bpJasas as $itemJasa){
                $data[] = self::setDataCsv([
                    'OF',$itemJasa->jasaBp->kode,$itemJasa->jasaBp->nama.' '.$itemJasa->note,$itemJasa->harga,
                    1,$itemJasa->harga,$itemJasa->discrp,$itemJasa->dpp,$itemJasa->ppn,0,0
                ]);
            }
            foreach($item->bpParts as $itemPart){
                $data[] = self::setDataCsv([
                    'OF',$itemPart->kode,$itemPart->nama,$itemPart->harga,
                    $itemPart->qty,$itemPart->harga,0,$itemPart->total_parts_line,$itemPart->ppnout,0,0
                ]);
            }
        }
		$csv = new ECSVExport($data);
		$content = $csv->toCSV();
		Yii::app()->getRequest()->sendFile('test.csv', $content, "text/csv", false);
		exit();
    }

    private static function setDataCsv($data){
        $template = [
            'FK','KD_JENIS_TRANSAKSI','FG_PENGGANTI','NOMOR_FAKTUR','MASA_PAJAK','TAHUN_PAJAK','TANGGAL_FAKTUR','NPWP','NAMA','ALAMAT_LENGKAP','JUMLAH_DPP','JUMLAH_PPN','JUMLAH_PPNBM','ID_KETERANGAN_TAMBAHAN','FG_UANG_MUKA','UANG_MUKA_DPP','UANG_MUKA_PPN','UANG_MUKA_PPNBM','REFERENSI','KODE_DOKUMEN_PENDUKUNG'
        ];
        $values = array_pad($data, count($template), null);
        return array_combine($template, $values);
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg    = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'NoFaktur')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg    = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg'     => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(
                400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.')
            );
        }
    }

    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit  = $limit;
            $criteria->offset = $start;
        }
        $model = NoFaktur::model()->findAll($criteria);
        $total = NoFaktur::model()->count($criteria);

        $this->renderJson($model, $total);
    }
}
