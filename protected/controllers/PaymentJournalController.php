<?php
class PaymentJournalController extends GxController
{
    public function actionCreate()
    {
            if (!Yii::app()->request->isAjaxRequest)
                    return;
            if (isset($_POST) && !empty($_POST)) {
                $is_new = $_POST['mode'] == 0;
                $ij_id = $_POST['id'];
                $posting = false;
                if (isset($_POST['posting'])) {
                        $posting = $_POST['posting'] == 'true';
                }
                $status = false;
                $msg = "Stored data failed.";
                $detils = CJSON::decode($_POST['detil']);
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                        $model = $is_new ? new PaymentJournal : $this->loadModel($ij_id, "PaymentJournal");
                        if ($is_new) {
                            $ref = new Reference();
                            $docref = $ref->get_next_reference(PAYMENT_JOURNAL);
                        } else {
                            $sudahposting = PaymentJournal::model()->findByPk($_POST['id']);
                            if($sudahposting->p == 1){
                                echo CJSON::encode(array(
                                    'success' => FALSE,
                                    'msg' => 'Gagal! Payment Journal sudah terposting!'));
                                Yii::app()->end();
                            }
                            PaymentJournalDetail::model()->deleteAll("payment_journal_id = :payment_journal_id", array(':payment_journal_id' => $ij_id));
                            $docref = $model->doc_ref;
                        }
                        foreach ($_POST as $k => $v) {
                            if ($k == 'detil')
                                continue;
                            if (is_angka($v))
                                $v = get_number($v);
                            $_POST['PaymentJournal'][$k] = $v;
                        }
                        $_POST['PaymentJournal']['doc_ref'] = $docref;
                        $model->attributes = $_POST['PaymentJournal'];
                        if (!$model->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PaymentJournal')) . CHtml::errorSummary($model));
                        }
                        $gl = new GL();
                        foreach ($detils as $detil) {
                            $item_details = new PaymentJournalDetail();
                            $_POST['PaymentJournalDetail']['store_kode'] = $detil['store_kode'];
                            $_POST['PaymentJournalDetail']['debit'] = get_number($detil['debit']);
                            $_POST['PaymentJournalDetail']['kredit'] = get_number($detil['kredit']);
                            $_POST['PaymentJournalDetail']['account_code'] = $detil['account_code'];
                            $_POST['PaymentJournalDetail']['memo_'] = $detil['memo_'];
                            $_POST['PaymentJournalDetail']['payment_journal_id'] = $model->payment_journal_id;
                            $item_details->attributes = $_POST['PaymentJournalDetail'];
                            if (!$item_details->save()) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Payment Journal')) . CHtml::errorSummary($item_details));
                            }
                        if ($posting) {
                                if ($item_details->kredit == 0) {
    //                      Debit
                                    $gl->add_gl(PAYMENT_JOURNAL, $model->payment_journal_id, $model->tgl, $model->doc_ref, $item_details->account_code, $item_details->memo_, '', $item_details->debit, 0, $item_details->store_kode);
                                } else if ($item_details->debit == 0) {
    //                      Kredit
                                    $gl->add_gl(PAYMENT_JOURNAL, $model->payment_journal_id, $model->tgl, $model->doc_ref, $item_details->account_code, $item_details->memo_, '', -($item_details->kredit), 0, $item_details->store_kode);
                                } else {
                                    echo CJSON::encode(array(
                                            'success' => FALSE,
                                            'msg' => 'Gagal! Debit dan Kredit tidak masuk dalam jurnal!'));
                                    Yii::app()->end();
                                }
                            }
                        }
                        if ($posting) {
                                $gl->validate();
                                $model->p = 1;
                        }
                        $model->save();
                        if ($is_new) {
                                $ref->save(PAYMENT_JOURNAL, $model->payment_journal_id, $docref);
                        }
                        $transaction->commit();
                        $msg = "Sukses disimpan";
                        $status = true;
                } catch (Exception $ex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $ex->getMessage();
                }
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg));
                Yii::app()->end();
            }
    }

//<<<<<<< HEAD

//    public function actionPosting() {
//        $id = $_POST['id'];
//        app()->db->autoCommit = false;
//        $transaction = Yii::app()->db->beginTransaction();
//        try {
//            $model = $this->loadModel($id, 'InvoiceJournal');
//            $model->p = 1;
//            $model->save();
//            $transaction->commit();
//            $status = true;
//            $msg = 'taraaaa';
//        } catch (Exception $ex) {
//            $transaction->rollback();
//            $msg = $ex->getMessage();
//            $status = false;
//        }
//        echo CJSON::encode(array(
//            'success' => $status,
//            'msg' => $msg));
//        Yii::app()->end();
//    }

//    public function actionIndex() {
//=======
    public function actionUnposting($id)
    {
            /** @var InvoiceJournal $model */
            $model = $this->loadModel($id, 'PaymentJournal');
            $msg = 'Payment jurnal berhasil diunposting';
//        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                GlTrans::model()->deleteAllByAttributes([
                    'type' => PAYMENT_JOURNAL,
                    'type_no' => $id
                ]);
                BankTrans::model()->deleteAllByAttributes([
                    'type_' => PAYMENT_JOURNAL,
                    'trans_no' => $id
                ]);
                if (!$model->saveAttributes(['p' => 0])) {
                    throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Payment Journal')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $e) {
                $transaction->rollback();
                $status = false;
                $msg = $e->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
            ));
            Yii::app()->end();
//        }
    }
    public function actionPosting()
    {
        $id = $_POST['id'];
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = $this->loadModel($id, 'PaymentJournal');
            $model->p = 1;
            $model->save();
            $transaction->commit();
            $status = true;
            $msg = 'taraaaa';
        } catch (Exception $ex) {
            $transaction->rollback();
            $msg = $ex->getMessage();
            $status = false;
        }
        echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
        Yii::app()->end();
    }
    public function actionIndex()
    {
//>>>>>>> 00f23e4e0a2d8a03774de01464fb862336b2baff
            if (isset($_POST['limit'])) {
                    $limit = $_POST['limit'];
            } else {
                    $limit = 20;
            }
            if (isset($_POST['start'])) {
                    $start = $_POST['start'];
            } else {
                    $start = 0;
            }
            $param = array();
            $criteria = new CDbCriteria();
            if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                    (isset($_POST['limit']) && isset($_POST['start']))
            ) {
                    $criteria->limit = $limit;
                    $criteria->offset = $start;
            }
            if (isset($_POST['tgl'])) {
                    $criteria->addCondition('tgl = date(:tgl)');
                    $param[':tgl'] = $_POST['tgl'];
            }
            if (isset($_POST['doc_ref'])) {
                    $docref = $_POST['doc_ref'];
                    $criteria->addCondition('doc_ref like :doc_ref');
                    $param[':doc_ref'] = "%$docref%";
            }
            if (isset($_POST['doc_ref_other'])) {
                    $docrefother = $_POST['doc_ref_other'];
                    $criteria->addCondition('doc_ref_other like :doc_ref_other');
                    $param[':doc_ref_other'] = "%$docrefother%";
            }
//        if (isset($_POST['tgl'])) {
//            $criteria->addCondition('tgl = date(:tgl)');
//            $param[':tgl'] = $_POST['tgl'];
//        }
            $criteria->order = 'doc_ref';
            $criteria->params = $param;
            $model = PaymentJournal::model()->findAll($criteria);
            $total = PaymentJournal::model()->count($criteria);
            $this->renderJson($model, $total);
    }

}
