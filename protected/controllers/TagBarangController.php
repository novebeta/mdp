<?php

class TagBarangController extends GxController {

    public function actionCreate() {
        $model = new TagBarang;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['TagBarang'][$k] = $v;
            }
            $model->security_roles_id = Users::model()->findByPk(Yii::app()->user->getId())->security_roles_id;
            $model->attributes = $_POST['TagBarang'];
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->tag_barang_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'TagBarang');


        if (isset($_POST) && !empty($_POST)) {
            if (Users::model()->findByPk(Yii::app()->user->getId())->security_roles_id != $model->security_roles_id) {
                $msg = "Data tidak dapat dirubah ";
                $status = false;
            } else {



                foreach ($_POST as $k => $v) {
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['TagBarang'][$k] = $v;
                }
                $msg = "Data gagal disimpan";
                $model->security_roles_id = Users::model()->findByPk(Yii::app()->user->getId())->security_roles_id;
                $model->attributes = $_POST['TagBarang'];

                if ($model->save()) {

                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->tag_barang_id;
                } else {
                    $msg .= " " . implode(", ", $model->getErrors());
                    $status = false;
                }
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->tag_barang_id));
            }
        }
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id, 'TagBarang');
        if (Users::model()->findByPk(Yii::app()->user->getId())->security_roles_id != $model->security_roles_id) {
            $msg = "Data tidak dapat dirubah ";
            $status = false;
        } else {
            if (Yii::app()->request->isPostRequest) {
                $msg = 'Data berhasil dihapus.';
                $status = true;
                try {
                    $this->loadModel($id, 'TagBarang')->delete();
                } catch (Exception $ex) {
                    $status = false;
                    $msg = $ex;
                }
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg));
                Yii::app()->end();
            } else
                throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $user = Users::model()->findByPk(Yii::app()->user->getId())->security_roles_id;
        $criteria->addCondition("security_roles_id = :security_roles_id");
        $criteria->params = array(':security_roles_id' => $user);

        $model = TagBarang::model()->findAll($criteria);
        $total = TagBarang::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
