<?php
class JualController extends GxController
{
    public function actionCreate()
    {
        $model = new Jual;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Jual'][$k] = $v;
            }
            $model->attributes = $_POST['Jual'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->jual_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Jual');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Jual'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Jual'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->jual_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->jual_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Jual')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->select = 'jual_id,price,cost,disc,discrp,store,np.up,np.barang_id,(duration div 60) duration';
        if (isset($_POST['kode_barang'])) {
            $criteria->alias = "np";
            $criteria->join = "INNER JOIN nscc_barang nb ON np.barang_id = nb.barang_id";
            $criteria->addCondition("kode_barang like :kode_barang");
            $param[':kode_barang'] = "%" . $_POST['kode_barang'] . "%";
        }
        if (isset($_POST['nama_barang'])) {
            $criteria->alias = "np";
            $criteria->join = "INNER JOIN nscc_barang nb ON np.barang_id = nb.barang_id";
            $criteria->addCondition("nama_barang like :nama_barang");
            $param[':nama_barang'] = "%" . $_POST['nama_barang'] . "%";
        }
        if (isset($_POST['sat'])) {
            $criteria->alias = "np";
            $criteria->join = "INNER JOIN nscc_barang nb ON np.barang_id = nb.barang_id";
            $criteria->addCondition("sat like :sat");
            $param[':sat'] = "%" . $_POST['sat'] . "%";
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition("store like :store");
            $param[':store'] = "%" . $_POST['store'] . "%";
        }
        if (isset($_POST['price'])) {
            $criteria->addCondition("price = :price");
            $param[':price'] = $_POST['price'];
        }
        if (isset($_POST['f'])) {
            $criteria->alias = "np";
//            $criteria->select = array('np.barang_id', 'price');
            $criteria->addCondition("store = :store");
            $param[':store'] = STOREID;
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Jual::model()->findAll($criteria);
        $total = Jual::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionExport()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $dir_backup = DIR_BACKUP;
        $files = scandir($dir_backup);
        foreach ($files as $file) {
            if (is_file("$dir_backup\\$file")) {
                unlink("$dir_backup\\$file");
            }
        }
        $filename = date("Y-m-d-H-i-s") . '.hjn';
        $backup_file = $dir_backup . $filename;
        $size = filesize($backup_file);
        if ($size > 0) {
            $zip = new ZipArchive();
            if ($zip->open("$backup_file.zip", ZIPARCHIVE::CREATE)) {
                $zip->addFile($backup_file, $filename);
                $zip->close();
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mimeType = finfo_file($finfo, "$backup_file.zip");
                $size = filesize("$backup_file.zip");
                $name = basename("$backup_file.zip");
                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                    // cache settings for IE6 on HTTPS
                    header('Cache-Control: max-age=120');
                    header('Pragma: public');
                } else {
                    header('Cache-Control: private, max-age=120, must-revalidate');
                    header("Pragma: no-cache");
                }
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // long ago
                header("Content-Type: $mimeType");
                header('Content-Disposition: attachment; filename="' . $name . '";');
                header("Accept-Ranges: bytes");
                header('Content-Length: ' . filesize("$backup_file.hjn"));
                print readfile("$backup_file.zip");
                exit;
            }
        }
    }
    public function actionImport()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = dirname(Yii::app()->request->scriptFile) . "\\backup\\";
        if (isset($_FILES["filename"])) { // it is recommended to check file type and size here
            if ($_FILES["filename"]["error"] > 0) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $_FILES["file"]["error"]
                ));
            } else {
                $backup_file = $dir_backup . $_FILES["filename"]["name"];
                move_uploaded_file($_FILES["filename"]["tmp_name"], $backup_file);
                $mysql = dirname(Yii::app()->request->scriptFile) . "\\mysql";
                $gzip = dirname(Yii::app()->request->scriptFile) . "\\gzip";
                $command = "$gzip -d $backup_file";
                system($command);
                $backup_file = substr($backup_file, 0, -3);
                if (!file_exists($backup_file)) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => "Failed restore file " . $_FILES["file"]["name"]
                    ));
                } else {
                    Yii::app()->db->createCommand("DROP DATABASE $db")
                        ->execute();
                    Yii::app()->db->createCommand("CREATE DATABASE IF NOT EXISTS $db")
                        ->execute();
                    $command = "$mysql -h $host -u $user --password=$pass -P $port " .
                        "$db < $backup_file";
                    system($command);
                    echo CJSON::encode(array(
                        'success' => true,
                        'msg' => "Succefully restore file " . $_FILES["file"]["name"]
                    ));
                }
                Yii::app()->end();
            }
        }
    }
}