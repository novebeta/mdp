<?php

class SyncStatusController extends GxController {

    public function actionCreate()
    {
        $model = new SyncStatus;
        if (!Yii::app()->request->isAjaxRequest)
            return;

        try
        {
            if (isset($_POST) && !empty($_POST))
            {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SyncStatus'][$k] = $v;
                }

                $check = SyncStatus::model()->findByAttributes(['store_code' => $_POST['SyncStatus']['store_code']]);

                if($check)
                {
                    $msg = "Cabang sudah ada di database.";
                    $status = true;
                    return;
                }

                $model->attributes = $_POST['SyncStatus'];
                $msg = "Data berhasil disimpan.";
                $status = true;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SyncStatus')) . CHtml::errorSummary($model));

            }
        }
        catch (Exception $ex) {
            //$msg = "Data gagal disimpan.";
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
            Yii::app()->end();

        }

    }

    public function actionCancel($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil di Cancel.';
            $status = true;
            try {
                $model = $this->loadModel($id, 'SyncStatus');
                $model->status_sync = '0';
                $model->note = 'CANCELED';
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SyncStatus')) . CHtml::errorSummary($model));


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionMark($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil di Mark.';
            $status = true;
            try {
                $model = $this->loadModel($id, 'SyncStatus');
                $model->status_scan = '3';
                $model->status_sync = '3';
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SyncStatus')) . CHtml::errorSummary($model));


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionUnMark($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil di UnMark.';
            $status = true;
            try {
                $model = $this->loadModel($id, 'SyncStatus');

                $model->status_scan = '0';
                $model->status_sync = '0';
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SyncStatus')) . CHtml::errorSummary($model));


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }



    public function actionUpdate($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['SyncStatus'][$k] = $v;
            }
            $msg = "Data gagal disimpan";

            $model = $this->loadModel($id, 'SyncStatus');
            $model->attributes = $_POST['SyncStatus'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sync_id;
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg
                ));
                Yii::app()->end();
            } else
            {
                $this->redirect(array('view', 'id' => $model->sync_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SyncStatus')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex() {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        $param = array();

        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        if (isset($_POST['store_code'])) {
            $store_code = $_POST['store_code'];
            $criteria->addCondition('store_code like :store_code');
            $param[':store_code'] = "%$store_code%";
        }
        $criteria->params = $param;
        $criteria->order = "store_code ASC";

        $model = SyncStatus::model()->findAll($criteria);
        $total = SyncStatus::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}