<?php
class StoreController extends GxController {
	public function actionCreate() {
		$model = new Store;
//		if ( ! Yii::app()->request->isAjaxRequest ) {
//			return;
//		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Store'][ $k ] = $v;
			}
//            $_POST['Store']['conn'] = Encrypt($_POST['Store']['conn']);
			$model->attributes = $_POST['Store'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				if ( isset( $_FILES["logo"] ) ) { // it is recommended to check file type and size here
					if ( $_FILES["logo"]["error"] > 0 ) {
						echo CJSON::encode( array(
							'success' => false,
							'msg'     => $_FILES["logo"]["error"]
						) );
					} else {
						$info = getimagesize( $_FILES['logo']['tmp_name'] );
						if ( $info === false ) {
							echo CJSON::encode( array(
								'success' => false,
								'msg'     => "Unable to determine image type of uploaded file"
							) );
							Yii::app()->end();
						}
						if ( $info[2] !== IMAGETYPE_JPEG ) {
							echo CJSON::encode( array(
								'success' => false,
								'msg'     => "File yang diupload harus *.jpeg"
							) );
							Yii::app()->end();
						}
						$dir_backup  = Yii::getPathOfAlias( 'webroot' ) . '/logo/';
						$backup_file = $dir_backup . $model->store_kode . '.jpeg';
						move_uploaded_file( $_FILES["logo"]["tmp_name"], $backup_file );
					}
				}
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->store_kode;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Store' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Store'][ $k ] = $v;
			}
			$msg = "Data gagal disimpan";
//            $_POST['Store']['conn'] = Encrypt($_POST['Store']['conn']);
			$model->attributes = $_POST['Store'];
			if ( $model->save() ) {
				if ( isset( $_FILES["logo"] ) ) { // it is recommended to check file type and size here
					if ( $_FILES["logo"]["error"] > 0 ) {
						echo CJSON::encode( array(
							'success' => false,
							'msg'     => $_FILES["logo"]["error"]
						) );
					} else {
						$info = getimagesize( $_FILES['logo']['tmp_name'] );
						if ( $info === false ) {
							echo CJSON::encode( array(
								'success' => false,
								'msg'     => "Unable to determine image type of uploaded file"
							) );
							Yii::app()->end();
						}
						if ( $info[2] !== IMAGETYPE_JPEG ) {
							echo CJSON::encode( array(
								'success' => false,
								'msg'     => "File yang diupload harus *.jpeg"
							) );
							Yii::app()->end();
						}
						$dir_backup  = Yii::getPathOfAlias( 'webroot' ) . '/logo/';
						$backup_file = $dir_backup . $model->store_kode . '.jpeg';
						move_uploaded_file( $_FILES["logo"]["tmp_name"], $backup_file );
					}
				}
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->store_kode;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->store_kode ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'Store' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
		$criteria->order = "id_cabang";
		$model           = Store::model()->findAll( $criteria );
		$total           = Store::model()->count( $criteria );
		$this->renderJson( $model, $total );
//        $argh = array();
//        foreach ($model AS $dodol) {
//            $attrib = $dodol->getAttributes();
//            $attrib['conn'] = strlen($attrib['conn']) == 0 ? '' : Decrypt($attrib['conn']);
//            $argh[] = $attrib;
//        };
//        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($argh) . '}';
		Yii::app()->end( $jsonresult );
	}
}