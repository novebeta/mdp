<?php
class KonsulDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new KonsulDetil;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KonsulDetil'][$k] = $v;
            }
            $model->attributes = $_POST['KonsulDetil'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->konsul_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'KonsulDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KonsulDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['KonsulDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->konsul_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->konsul_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'KonsulDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
        $criteria = new CDbCriteria();
        $param = [];
        if (isset($_POST['id_antrian'])) {
            $konsul = Konsul::model()->findByAttributes(['id_antrian' => $_POST['id_antrian']]);
            if ($konsul != null) {
                $_POST['konsul_id'] = $konsul->konsul_id;
            } else {
                $_POST['konsul_id'] = '-1';
            }
        }
        if (isset($_POST['konsul_id'])) {
            $criteria->addCondition('konsul_id = :konsul_id');
            $param[':konsul_id'] = $_POST['konsul_id'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->params = $param;
        $model = KonsulDetil::model()->findAll($criteria);
        $total = KonsulDetil::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}