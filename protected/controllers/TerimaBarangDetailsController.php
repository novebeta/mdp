<?php
class TerimaBarangDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        
        if (isset($_POST['terima_barang_id'])) {
            $criteria->addCondition('visible = 1');
            $criteria->addCondition("terima_barang_id = :terima_barang_id");
            $criteria->params = array(':terima_barang_id' => $_POST['terima_barang_id']);
            $criteria->order = "seq";
            $model = TerimaBarangDetails::model()->findAll($criteria);
            $total = TerimaBarangDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        } else if (isset($_POST['po_id'])) {
            /* saat buat penerimaan baru, data detail ambil dari POdetails*/
            $criteria->addCondition("po_id = :po_id");
            $criteria->params = array(':po_id' => $_POST['po_id']);
            $criteria->order = "seq";
            $model = PoSisaTerima::model()->findAll($criteria);
            $total = PoSisaTerima::model()->count($criteria);
            $this->renderJson($model, $total);
        }
    }
    public function actionItemDetilInvoice()
    {
        /* saat input invoice dari penerimaan barang */
        $model = TerimaBarangDetails::get_item_detil_invoice();
        $total = count($model);
        $this->renderJsonArrWithTotal($model, $total);
    }
    public function actionItemDetilReturn()
    {
        /* saat return barang dr supplier */
        $model = TerimaBarangDetails::get_item_detil_return();
        $total = count($model);
        $this->renderJsonArrWithTotal($model, $total);
    }
}