<?php
class CustomerBpController extends GxController {
	public function actionCreate() {
		$model = new CustomerBp;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['CustomerBp'][ $k ] = $v;
			}
			$model->attributes = $_POST['CustomerBp'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->customer_bp_id;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'CustomerBp' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['CustomerBp'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['CustomerBp'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->customer_bp_id;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->customer_bp_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'CustomerBp' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$param    = [];
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( isset( $_POST['customer_id'] ) ) {
			unset( $_POST['query'] );
			$criteria->addCondition( 'customer_bp_id = :customer_id' );
			$param[':customer_id'] = $_POST['customer_id'];
		}
		if ( isset( $_POST['query'] ) ) {
			$criteria->addCondition( 'nama_customer like :nama_customer', 'OR' );
			$param[':nama_customer'] = '%' . $_POST['query'] . '%';
		}
		if (isset($_POST['search'])) {
            $criteria->addCondition("nama_customer like :search");
            $criteria->addCondition("email like :search",'OR');
            $criteria->addCondition("telp like :search",'OR');
            $criteria->addCondition("alamat like :search",'OR');
            $criteria->addCondition("city like :search",'OR');
            $criteria->addCondition("ktp like :search",'OR');
            $criteria->addCondition("npwp like :search",'OR');
            $param[':search'] = "%" . $_POST['search'] . "%";
        }
		$criteria->params = $param;
		$model = CustomerBp::model()->findAll( $criteria );
		$total = CustomerBp::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}