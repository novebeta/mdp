<?php
class ClinicalTransController extends GxController
{
    public function actionCreateIn()
    {
        $model = new ClinicalTrans;
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['ClinicalTrans'][$k] = $v;
                }
                $ref = new Reference();
                $docref = $ref->get_next_reference(CLINICAL_IN);
                $_POST['ClinicalTrans']['arus'] = 1;
                $_POST['ClinicalTrans']['doc_ref'] = $docref;
                $_POST['ClinicalTrans']['store'] = STOREID;
                $model->attributes = $_POST['ClinicalTrans'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Clinical In')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $model_details = new ClinicalTransDetail;
                    foreach ($detil as $n => $val) {
                        if (is_angka($val)) {
                            $val = get_number($val);
                        }
                        $_POST['ClinicalTransDetail'][$n] = $val;
                    }
                    $_POST['ClinicalTransDetail']['clinical_trans_id'] = $model->clinical_trans_id;
                    $model_details->attributes = $_POST['ClinicalTransDetail'];
                    if (!$model_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Clinical Details In')) . CHtml::errorSummary($model_details));
                    }
                    StockMovesClinical::add_stock_moves($model->clinical_trans_id, $model->tgl,
                        $model_details->barang_clinical,
                        $model_details->qty, $model->tipe_clinical_id, $docref, $model->store);
                }
                $ref->save(CLINICAL_IN, $model->clinical_trans_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionCreateOut()
    {
        $model = new ClinicalTrans;
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['ClinicalTrans'][$k] = $v;
                }
                $ref = new Reference();
                $docref = $ref->get_next_reference(CLINICAL_OUT);
                $_POST['ClinicalTrans']['arus'] = -1;
                $_POST['ClinicalTrans']['doc_ref'] = $docref;
                $_POST['ClinicalTrans']['store'] = STOREID;
                $model->attributes = $_POST['ClinicalTrans'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Clinical In')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $model_details = new ClinicalTransDetail;
                    foreach ($detil as $n => $val) {
                        if (is_angka($val)) {
                            $val = get_number($val);
                        }
                        $_POST['ClinicalTransDetail'][$n] = $val;
                    }
                    $_POST['ClinicalTransDetail']['clinical_trans_id'] = $model->clinical_trans_id;
                    $model_details->attributes = $_POST['ClinicalTransDetail'];
                    if (!$model_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Clinical Details In')) . CHtml::errorSummary($model_details));
                    }
                    $model_details->awal = $model_details->barangClinical->get_saldo();
                    $model_details->qty = $model_details->awal - $model_details->sisa;
                    if (!$model_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Clinical Details In')) . CHtml::errorSummary($model_details));
                    }
                    StockMovesClinical::add_stock_moves($model->clinical_trans_id, $model->tgl,
                        $model_details->barang_clinical,
                        -$model_details->qty, $model->tipe_clinical_id, $docref, $model->store);
                }
                $ref->save(CLINICAL_OUT, $model->clinical_trans_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['arus'])) {
            $criteria->addCondition("arus = :arus");
            $param[':arus'] = $_POST['arus'];
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition("tgl = :tgl");
            $param[':tgl'] = $_POST['tgl'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = ClinicalTrans::model()->findAll($criteria);
        $total = ClinicalTrans::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}