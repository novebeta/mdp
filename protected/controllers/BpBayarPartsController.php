<?php
class BpBayarPartsController extends GxController {
	public function actionCreate() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				$model          = new BpBayarParts();
				$model->ref     = new CDbExpression(
					"fnc_ref_bp_bayar_parts('" . $_POST['tgl'] . "')" );
				$model->tgl     = $_POST['tgl'];
				$model->total   = get_number( $_POST['total'] );
				$model->bank_id = $_POST['bank_id'];
				$model->note    = $_POST['note'];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'BpBayarParts' ) ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				$gl   = new GL();
				$ref  = $model->ref;
				$bank = Bank::model()->findByPk( $model->bank_id );
				foreach ( $detils as $row ) {
					/** @var BpParts $sales */
					$sales = BpParts::model()->findByPk( $row['bp_parts_id'] );
					if ( $sales->bp_bayar_parts_id != null ) {
						throw new Exception( "Sudah dibayar." );
					}
					$sales->bp_bayar_parts_id = $model->bp_bayar_parts_id;
					if ( ! $sales->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'BpParts' ) ) . CHtml::errorSummary( $sales ) );
					}
					$bayarSUpp = floatval($sales->hpp) + floatval($sales->ppnout);
					$gl->add_gl( BODYPAINT_BAYAR_SUPP_PARTS, $model->bp_bayar_parts_id, $model->tgl,
						$ref, '21-01-03', "BAYAR PART KE SUPPLIER - INV $ref",
						"BAYAR PART KE SUPPLIER - INV $ref", $bayarSUpp, 1, '' );
				}
				$gl->add_gl( BODYPAINT_BAYAR_SUPP_PARTS, $model->bp_bayar_parts_id, $model->tgl,
					$ref, $bank->account_code, "BAYAR PART KE SUPPLIER - INV $ref",
					"BAYAR PART KE SUPPLIER - INV $ref", - $model->total, 0, '' );
				$gl->add_bank_trans( BODYPAINT_BAYAR_SUPP_PARTS, $model->bp_bayar_parts_id, $bank->bank_id,
					$ref, $model->tgl, -$model->total, Yii::app()->user->getId(), '' );
				$gl->validate();
				$msg = "Data berhasil disimpan";
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'BpBayarParts' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['BpBayarParts'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['BpBayarParts'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bp_bayar_parts_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->bp_bayar_parts_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'BpBayarParts' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $criteria->addCondition("tgl >= :from AND tgl <= :to");
            $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
            $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        }
		$model = BpBayarParts::model()->findAll( $criteria );
		$total = BpBayarParts::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionList() {
		$bp_bayar_parts_id = null;
		if ( isset( $_POST['bp_bayar_parts_id'] ) ) {
			$bp_bayar_parts_id = $_POST['bp_bayar_parts_id'];
		}
		$data = BpBayarParts::getList( $bp_bayar_parts_id );
		$this->renderJsonArrWithTotal( $data, sizeof( $data ) );
	}

	public function actionPrint($id) {
		$this->layout = 'a5lanscape';
		$model        = $this->loadModel( $id, 'BpBayarParts' );
		$this->render( 'BuktiKas', [ 'model' => $model ] );
	}
}