<?php
class BpBayar2Controller extends GxController {
	public function actionCreate() {
		$model = new BpBayar2;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST[ 'BpBayar2' ][ $k ] = $v;
			}
			$model->attributes = $_POST[ 'BpBayar2' ];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bp_bayar_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg ] );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'BpBayar2' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
//			foreach ( $_POST as $k => $v ) {
//				if ( is_angka( $v ) ) {
//					$v = get_number( $v );
//				}
//				$_POST[ 'BpBayar2' ][ $k ] = $v;
//			}
			$msg               = "Data gagal disimpan";
			$model->tgl            = $_POST['tgl'];
			if ( $model->save() ) {
				BodyPaint::model()->updateAll(array( 'b2_tgl' => $_POST['tgl'],'lunas' => $_POST['tgl']  ), 'bp_bayar_id = :bp_bayar_id ',array(':bp_bayar_id'=>$id) );
				GlTrans::model()->updateAll(array( 'tran_date' => $_POST['tgl'] ), 'type_no = :type_no ',array(':type_no'=>$id) );
				BankTrans::model()->updateAll(array( 'tgl' => $_POST['tgl'] ), 'trans_no = :trans_no ',array(':trans_no'=>$id) );
				$status = true;
				$msg    = "Data berhasil di simpan";
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( [
					'success' => $status,
					'msg'     => $msg
				] );
				Yii::app()->end();
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'BpBayar2' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg ] );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST[ 'limit' ] ) ) {
			$limit = $_POST[ 'limit' ];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST[ 'start' ] ) ) {
			$start = $_POST[ 'start' ];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$criteria->select = 'bp_bayar_id,DATE(tgl) as tgl,total,bank_id,id_user,ref,asuransi_bp_id,tagih';
		if ( ( isset ( $_POST[ 'mode' ] ) && $_POST[ 'mode' ] == 'grid' ) ||
		     ( isset( $_POST[ 'limit' ] ) && isset( $_POST[ 'start' ] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $criteria->addCondition("tgl >= :from AND tgl <= :to");
            $criteria->params[':from'] = date('Y-m-d', strtotime($_POST['tglfrom']));
            $criteria->params[':to']   = date('Y-m-d', strtotime($_POST['tglto']));
        }
		$model = BpBayar2::model()->findAll( $criteria );
		$total = BpBayar2::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}