<?php
class InvoiceUgController extends GxController {
	public function actionCreate() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				$model            = new InvoiceUg;
				$model->doc_ref   = new CDbExpression(
					"fnc_ref_woug_bayar()" );
//				$model->tgl_bayar = new CDbExpression( 'NOW()' );
				$model->tgl_bayar = $_POST['tgl_bayar'];
				$model->total     = get_number( $_POST['total'] );
				$model->bank_id   = $_POST['bank_id'];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Woug' ) ) . CHtml::errorSummary( $model ) );
				}
				foreach ( $detils as $row ) {
					/** @var SalesMdp $sales */
					$sales                = SalesMdp::model()->findByPk( $row['sales_id'] );
					$sales->invoice_ug_id = $model->invoice_ug_id;
					if ( ! $sales->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'SalesMdp' ) ) . CHtml::errorSummary( $sales ) );
					}
				}
				$model->refresh();
				/** @var Bank $bank */
				$bank = Bank::model()->findByPk( $_POST['bank_id'] );
				$gl   = new GL();
				$ref  = $model->doc_ref;
				$gl->add_gl( PELUNASAN_WOUG, $model->invoice_ug_id, $model->tgl_bayar,
					$ref, '21-01-01', "Pelunasan $ref",
					"Pelunasan $ref", $model->total, 1, '' );
				$gl->add_gl( PELUNASAN_WOUG, $model->invoice_ug_id, $model->tgl_bayar,
					$ref, $bank->account_code, "Pelunasan $ref",
					"Pelunasan $ref", - $model->total, 0, '' );
				$gl->add_bank_trans( PELUNASAN_WOUG, $model->invoice_ug_id, $bank->bank_id, $ref,
					$model->tgl_bayar, - $model->total, Yii::app()->user->getId(), '' );
				$gl->validate();
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'InvoiceUg' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['InvoiceUg'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['InvoiceUg'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->invoice_ug_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->invoice_ug_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'InvoiceUg' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = InvoiceUg::model()->findAll( $criteria );
		$total = InvoiceUg::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}