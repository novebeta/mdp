<?php
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 2/27/18
 * Time: 2:04 PM
 */
class ApiController extends GxController
{
    Const APPLICATION_ID = 'B7076B5F-269E-11E8-8341-6DB33A098066';
    Const HEADER_KEY = 'Authorization';
    private $format = 'json';
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*')
            )
        );
    }
    private function _checkAuth()
    {

        $headers = getallheaders();
// returns the Accept header value
//        $accept = $headers->get('Authorization');
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if (!array_key_exists(self::HEADER_KEY, $headers)) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
//        $uaparser = Yii::app()->uaparser;
//        $uaparser->setUserAgent($_SERVER['HTTP_USER_AGENT']);
//        $os = $uaparser->platform();
//        $os_version = $uaparser->version($os);
//        $auth = $os . $os_version;
//        $value = Decrypt($headers[self::HEADER_KEY]);
        $value = $headers[self::HEADER_KEY];
        if ($value === false) {
            $this->_sendResponse(401);
        } elseif ($value !== self::APPLICATION_ID) {
            $this->_sendResponse(401);
        }
//        $username = $_SERVER['HTTP_X_USERNAME'];
//        $password = $_SERVER['HTTP_X_PASSWORD'];
//        // Find the user
//        $user = User::model()->find('LOWER(username)=?', array(strtolower($username)));
//        if ($user === null) {
//            // Error: Unauthorized
//            $this->_sendResponse(401, 'Error: User Name is invalid');
//        } else if (!$user->validatePassword($password)) {
//            // Error: Unauthorized
//            $this->_sendResponse(401, 'Error: User Password is invalid');
//        }
    }
    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);
        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';
            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }
            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
            // this should be templated in a real-world solution
            $body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
</head>
<body>
    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';
            echo $body;
        }
        Yii::app()->end();
    }
    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    private function getClientAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    public function actionGetToken()
    {
//        var_dump($_SERVER);
//        return true;
// Check if we have the USERNAME and PASSWORD HTTP headers set?
        if (!(isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
//            echo 'empty';
        }
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        $password = hash_password($password);
        $model = new LoginForm;
        if ($username != "") {
            $model->username = $username;
            $model->password = $password;
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
//                $uaparser = Yii::app()->uaparser;
//                $uaparser->setUserAgent($_SERVER['HTTP_USER_AGENT']);
//                $os = $uaparser->platform();
//                $os_version = $uaparser->version($os);
//                $auth = $os.$os_version;
//                $browser = get_browser(null, true);
////                print_r($browser);
//                echo " || $os || $os_version || $auth";
//                echo Encrypt($auth);
                echo self::APPLICATION_ID;
            } else
//                echo 'jika salah login';
                $this->_sendResponse(401);
        } else {
//            echo 'jika user ksong';
            $this->_sendResponse(401);
        }
    }
    public function actionGetBonusSummary()
    {
        self::_checkAuth();
        $from = $_POST['tglfrom'];
        $to = $_POST['tglto'];
        $param = array(':from' => $from, ':to' => $to);
        $where = "";
        if ($store != null) {
            $where = "AND nbs.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                ne.kode_employee,
                ne.nama_employee,
                Sum(nbs.amount_bonus) AS amount_bonus
            FROM nscc_bonus AS nbs
                LEFT JOIN nscc_employees AS ne ON nbs.employee_id = ne.employee_id
                INNER JOIN nscc_tipe_employee AS te ON te.tipe_employee_id=ne.tipe
            WHERE
                nbs.visible = 1
                AND DATE(nbs.tgl) >= :from
                AND DATE(nbs.tgl) <= :to
                $where
            GROUP BY ne.nama_employee
            ORDER BY ne.nama_employee
        ");
        $a = $comm->queryAll(true, $param);
        return $this->renderJsonArrWithTotal($a, count($a));
    }
}