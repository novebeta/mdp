<?php
class WorkOrderController extends GxController {
	public function actionCreate() {
		$model = new WorkOrder;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['WorkOrder'][ $k ] = $v;
			}
			$model->attributes = $_POST['WorkOrder'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->sales_id;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'SalesMdp' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['WorkOrder'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['WorkOrder'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->sales_id;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->sales_id ) );
			}
		}
	}
	public function actionRevisi( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil direvisi.';
			$status = true;
			try {
				$this->loadModel( $id, 'SalesMdp' )->updateByPk( $id, [
					'tgl_wo' => null,
					'edited' => new CDbExpression( '(edited+1)' )
				] );
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ! isset( $_POST['store'] ) ) {
			$criteria->addCondition( 'store_kode = :store_kode' );
			$criteria->params[':store_kode'] = STOREID;
		}
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = WorkOrder::model()->findAll( $criteria );
		$total = WorkOrder::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionUpload() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		if ( isset( $_FILES["filename"] ) ) { // it is recommended to check file type and size here
			if ( $_FILES["filename"]["error"] > 0 ) {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => $_FILES["filename"]["error"]
				) );
			} else {
				$info = getimagesize( $_FILES['filename']['tmp_name'] );
				if ( $info === false ) {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => "Unable to determine image type of uploaded file"
					) );
					Yii::app()->end();
				}
				if ( $info[2] !== IMAGETYPE_JPEG ) {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => "File yang diupload harus *.jpeg"
					) );
					Yii::app()->end();
				}
				$dir_backup  = Yii::getPathOfAlias( 'webroot' ) . '/upload/';
				$backup_file = $dir_backup . $_POST['sales_id'] . '.jpeg';
				move_uploaded_file( $_FILES["filename"]["tmp_name"], $backup_file );
				if ( ! file_exists( $backup_file ) ) {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => "Failed upload file " . $_FILES["filename"]["name"]
					) );
				} else {
					/** @var SalesMdp $sales */
					$sales = SalesMdp::model()->findByPk( $_POST['sales_id'] );
					if ( $sales != null ) {
						$sales->tgl_finish = new CDbExpression( 'NOW()' );
						$sales->save();
					}
					echo CJSON::encode( array(
						'success' => true,
						'msg'     => "Succefully upload file " . $_FILES["filename"]["name"]
					) );
				}
				Yii::app()->end();
			}
		}
	}
}