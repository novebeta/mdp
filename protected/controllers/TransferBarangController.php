<?php
Yii::import( 'application.components.Reference' );
Yii::import( 'application.components.U' );
Yii::import( 'application.components.GL' );
class TransferBarangController extends GxController {
	public function actionCreateIn() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->redirect( url( '/' ) );
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
//            if (Tender::is_exist($_POST['tgl'])) {
//                echo CJSON::encode(array(
//                    'success' => false,
//                    'msg' => 'Tender Declaration already created'
//                ));
//                Yii::app()->end();
//            }
			$status               = false;
			$msg                  = "Stored data failed.";
			$detils               = CJSON::decode( $_POST[ 'detil' ] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				$is_new = $_POST[ 'mode' ] == 0;
				$model  = $is_new ? new TransferBarang : $this->loadModel( $_POST[ 'id' ], 'TransferBarang' );
				if ( ! $is_new && ( $model == null ) ) {
					throw new Exception( t( 'save.model.fail', 'app',
							[ '{model}' => 'TransferBarang' ] ) . "Fatal error, record not found." );
				}
				if ( $is_new ) {
					$ref    = new Reference();
					$docref = $ref->get_next_reference( ITEM_IN );
				} else {
					$docref = $model->doc_ref;
//                    TransferBarangDetails::model()->updateAll(array('visible' => 0), 'transfer_barang_id = :transfer_barang_id',
//                        array(':transfer_barang_id' => $model->transfer_barang_id));
					$type    = ITEM_IN;
					$type_no = $model->transfer_barang_id;
					TransferBarangDetails::model()->deleteAll( 'transfer_barang_id = :transfer_barang_id',
						[ ':transfer_barang_id' => $type_no ] );
					$this->delete_gl_trans( $type, $type_no );
					$this->delete_stock_moves( $type, $type_no );
//                    $this->delete_stock_moves_perlengkapan($type, $type_no);
				}
				foreach ( $_POST as $k => $v ) {
					if ( $k == 'detil' ) {
						continue;
					}
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST[ 'TransferBarang' ][ $k ] = $v;
				}
				$_POST[ 'TransferBarang' ][ 'doc_ref' ] = $docref;
				$model->attributes                      = $_POST[ 'TransferBarang' ];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app', [ '{model}' => 'Receive item' ] ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				if ( $is_new ) {
					$ref->save( ITEM_IN, $model->transfer_barang_id, $docref );
				}
				$gl = new GL();
				foreach ( $detils as $detil ) {
					$item_details                                             = new TransferBarangDetails;
					$_POST[ 'TransferBarangDetails' ][ 'barang_id' ]          = $detil[ 'barang_id' ];
					$_POST[ 'TransferBarangDetails' ][ 'qty' ]                = get_number( $detil[ 'qty' ] );
					$_POST[ 'TransferBarangDetails' ][ 'price' ]              = get_number( $detil[ 'price' ] );
					$_POST[ 'TransferBarangDetails' ][ 'bruto' ]              = get_number( $detil[ 'bruto' ] );
					$_POST[ 'TransferBarangDetails' ][ 'vat' ]                = get_number( $detil[ 'vat' ] );
					$_POST[ 'TransferBarangDetails' ][ 'vatrp' ]              = get_number( $detil[ 'vatrp' ] );
					$_POST[ 'TransferBarangDetails' ][ 'total' ]              = get_number( $detil[ 'total' ] );
					$_POST[ 'TransferBarangDetails' ][ 'transfer_barang_id' ] = $model->transfer_barang_id;
					$item_details->attributes                                 = $_POST[ 'TransferBarangDetails' ];
					if ( ! $item_details->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', [ '{model}' => 'Receive barang detail' ] ) . CHtml::errorSummary( $item_details ) );
					}
//                    $total = $item_details->total;
					$grup = Grup::model()->findByPk( $item_details->barang->grup_id );
					if ( $grup->kategori->is_have_stock() ) {
//                        U::add_stock_moves(ITEM_IN, $model->transfer_barang_id, $model->tgl,
//                            $item_details->barang_id, $item_details->qty, $model->doc_ref,
//                            $item_details->barang->get_cost($model->store), $model->store);
						U::add_stock_moves_all(
							null,
							ITEM_IN,
							$model->transfer_barang_id,
							$model->tgl,
							$item_details->barang_id,
							$item_details->qty,
							$model->doc_ref,
//							$item_details->barang->get_cost( $model->store ),
							0, ''
						);
						$gl->add_gl( ITEM_IN, $model->transfer_barang_id, $model->tgl, $docref,
							$item_details->barang->persediaan, $model->note, $model->note,
							$item_details->bruto, 0, $model->store );
						if ( $item_details->vatrp > 0 ) {
							$gl->add_gl( ITEM_IN, $model->transfer_barang_id, $model->tgl, $docref,
								'11-09-03', $model->note, $model->note,
								$item_details->vatrp, 0, $model->store );
						}
						$gl->add_gl( ITEM_IN, $model->transfer_barang_id, $model->tgl, $docref,
							$model->suppFlazz->account_code, $model->note, $model->note,
							- $item_details->total, 0, $model->store );
					}
				}

				$gl->validate();
				$transaction->commit();
				$msg    = t( 'save.success', 'app' );
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( [
				'success' => $status,
				'id'      => $docref,
				'msg'     => $msg
			] );
			Yii::app()->end();
		}
	}
	public function actionBayar( $id ) {
		/** @var TransferBarang $model */
		$model = $this->loadModel( $id, 'TransferBarang' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				if ( $model->lunas != null ) {
					throw new Exception( "Sudah lunas." );
				}
				$model->lunas   = $_POST[ 'tgl' ];
				$model->bank_id = $_POST[ 'bank_id' ];
				$msg            = "Data gagal disimpan";
//			$model->attributes = $_POST['Kas'];
				if ( $model->save() ) {
					$status = true;
					$msg    = "Data berhasil di update";
				} else {
					$msg    .= " " . CHtml::errorSummary( $model );
					$status = false;
				}
				$model->refresh();
				$gl   = new GL();
				$ref  = $model->doc_ref;
				$bank = Bank::model()->findByPk( $model->bank_id );
				
				$gl->add_gl( BAYAR_ITEM_IN, $model->transfer_barang_id, $model->lunas,
					$ref, $model->suppFlazz->account_code, "Pelunasan $ref",
					"Pelunasan $ref", $model->total, 1, '' );
				$gl->add_gl( BAYAR_ITEM_IN, $model->transfer_barang_id, $model->lunas,
					$ref, $bank->account_code, "Pelunasan $ref",
					"Pelunasan $ref", - $model->total, 0, '' );
				$gl->add_bank_trans( BAYAR_ITEM_IN, $model->transfer_barang_id, $bank->bank_id,
					$ref, $model->lunas, - $model->total, Yii::app()->user->getId(), '' );
				$gl->validate();
				$refs            = new Refs();
				$refs->type_     = BAYAR_ITEM_IN;
				$refs->type_no   = $model->transfer_barang_id;
				$refs->reference = $ref;
				$refs->save();
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( [
					'success' => $status,
					'msg'     => $msg
				] );
				Yii::app()->end();
			} else {
				$this->redirect( [ 'view', 'id' => $model->kas_id ] );
			}
		}
	}

	public function actionPrint( $id ) {
		$this->layout = 'a5lanscape';
		$model        = $this->loadModel( $id, 'TransferBarang' );
		$this->render( 'BuktiKas', [ 'model' => $model ] );
	}
	public function actionIndexIn() {
		if ( isset( $_POST[ 'limit' ] ) ) {
			$limit = $_POST[ 'limit' ];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST[ 'start' ] ) ) {
			$start = $_POST[ 'start' ];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( isset( $_POST[ 'tglfrom' ] ) && isset( $_POST[ 'tglto' ] ) ) {
			$criteria->addCondition( "tgl >= :from AND tgl <= :to" );
			$criteria->params[ ':from' ] = $_POST[ 'tglfrom' ];
			$criteria->params[ ':to' ]   = $_POST[ 'tglto' ];
		}
		if ( ( isset ( $_POST[ 'mode' ] ) && $_POST[ 'mode' ] == 'grid' ) ||
		     ( isset( $_POST[ 'limit' ] ) && isset( $_POST[ 'start' ] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = TransferBarang::model()->findAll( $criteria );
		$total = TransferBarang::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionCreateOut() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->redirect( url( '/' ) );
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$status               = false;
			$msg                  = "Stored data failed.";
			$detils               = CJSON::decode( $_POST[ 'detil' ] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			$docref               = '';
			try {
				$is_new = $_POST[ 'mode' ] == 0;
				$model  = $is_new ? new TransferBarang : $this->loadModel( $_POST[ 'id' ], 'TransferBarang' );
				if ( ! $is_new && ( $model == null ) ) {
					throw new Exception( t( 'save.model.fail', 'app',
							[ '{model}' => 'TransferBarang' ] ) . "Fatal error, record not found." );
				}
				if ( $is_new ) {
					$ref    = new Reference();
					$docref = $ref->get_next_reference( ITEM_OUT );
				} else {
					$docref = $model->doc_ref;
					TransferBarangDetails::model()->updateAll( [ 'visible' => 0 ], 'transfer_barang_id = :transfer_barang_id',
						[ ':transfer_barang_id' => $model->transfer_barang_id ] );
					$type    = ITEM_OUT;
					$type_no = $model->transfer_barang_id;
					$this->delete_stock_moves( $type, $type_no );
					$this->delete_stock_moves_perlengkapan( $type, $type_no );
				}
				foreach ( $_POST as $k => $v ) {
					if ( $k == 'detil' ) {
						continue;
					}
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST[ 'TransferBarang' ][ $k ] = $v;
				}
				$_POST[ 'TransferBarang' ][ 'doc_ref' ] = $docref;
				$model->attributes                      = $_POST[ 'TransferBarang' ];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app', [ '{model}' => 'Receive item' ] ) . CHtml::errorSummary( $model ) );
				}
				foreach ( $detils as $detil ) {
					$item_details                                             = new TransferBarangDetails;
					$_POST[ 'TransferBarangDetails' ][ 'barang_id' ]          = $detil[ 'barang_id' ];
					$_POST[ 'TransferBarangDetails' ][ 'qty' ]                = get_number( $detil[ 'qty' ] );
					$_POST[ 'TransferBarangDetails' ][ 'transfer_barang_id' ] = $model->transfer_barang_id;
					$item_details->attributes                                 = $_POST[ 'TransferBarangDetails' ];
					if ( ! $item_details->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', [ '{model}' => 'Receive barang detail' ] ) . CHtml::errorSummary( $item_details ) );
					}
					$total = $item_details->total;
					$item_details->barang->count_biaya_beli( $item_details->qty, $total, $model->store );
//------------------ Pemisahan pengecekan stock moves perlengkapan dan tidak ------------------------------------
//                    $saldo_stock = StockMoves::get_saldo_item($item_details->barang_id, $model->store);
					$barang = $item_details->barang->tipe_barang_id;
					if ( $item_details->barang->grup->kategori->is_have_stock() ) {
						$saldo_stock = 0;
						switch ( $barang ) {
							case TIPE_FINISH_GOODS:
							case TIPE_RAW_MATERIAL:
								$saldo_stock = StockMoves::get_saldo_item( $item_details->barang_id, $model->store );
								break;
							case TIPE_PERLENGKAPAN:
								$saldo_stock = StockMovesPerlengkapan::get_saldo_item( $item_details->barang_id, $model->store );
								break;
						}
						if ( $saldo_stock < $item_details->qty ) {
							throw new Exception( t( 'saldo.item.fail',
								'app', [
									'{item}' => $item_details->barang->kode_barang,
									'{h}'    => $saldo_stock,
									'{r}'    => $item_details->qty
								] ) );
						}
//                        U::add_stock_moves(ITEM_OUT, $model->transfer_barang_id, $model->tgl,
//                            $item_details->barang_id, -$item_details->qty, $model->doc_ref,
//                            $item_details->barang->get_cost($model->store), $model->store);
						U::add_stock_moves_all(
							null,
							ITEM_OUT,
							$model->transfer_barang_id,
							$model->tgl,
							$item_details->barang_id,
							- $item_details->qty,
							$model->doc_ref,
							$item_details->barang->get_cost( $model->store ),
							$model->store
						);
					}
				}
				if ( $is_new ) {
					$ref->save( ITEM_OUT, $model->transfer_barang_id, $docref );
				}
				$transaction->commit();
				$msg    = t( 'save.success', 'app' );
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( [
				'success' => $status,
				'id'      => $docref,
				'msg'     => $msg
			] );
			Yii::app()->end();
		}
	}
	public function actionIndexOut() {
		$criteria = new CDbCriteria();
		$criteria->addCondition( "type_ = 1 AND DATE(tgl) = :tgl" );
		$criteria->params = [ ':tgl' => $_POST[ 'tgl' ] ];
		$model            = TransferBarang::model()->findAll( $criteria );
		$total            = TransferBarang::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}