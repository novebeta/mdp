<?php
class TransferItemDetailsController extends GxController
{
    public function actionIndexIn()
    {
        $cmd = DbCmd::instance()
            ->addSelect([
                'tid.*',
                'tbd.qty qty_terima',
                't2.qty_invoiced',
                'tid.price price_before'
            ])
            ->addFrom('{{transfer_item_details}} tid')
            ->addLeftJoin('{{transfer_item}} ti', 'tid.transfer_item_id = ti.transfer_item_id')
            ->addLeftJoin('{{terima_barang_details}} tbd', 'tbd.terima_barang_id = ti.terima_barang_id AND tbd.barang_id = tid.barang_id AND tbd.item_id = tid.item_id')
            ->addLeftJoin(
                DbCmd::instance()
                    ->addSelect([
                        'ti.terima_barang_id',
                        'tid.barang_id',
                        'tid.item_id',
                        'SUM(tid.qty) qty_invoiced',
                    ])
                    ->addFrom('{{transfer_item_details}} tid')
                    ->addLeftJoin('{{transfer_item}} ti', 'tid.transfer_item_id = ti.transfer_item_id')
                    ->addCondition('ti.terima_barang_id = (SELECT terima_barang_id FROM {{transfer_item}} WHERE transfer_item_id = :transfer_item_id)')
                    ->addCondition('tid.transfer_item_id != :transfer_item_id')
                    ->addCondition('tid.visible = 1')
                    ->addGroup('ti.terima_barang_id, tid.barang_id, tid.item_id')
                    ->setAs('t2')
                , 't2.terima_barang_id = tbd.terima_barang_id AND t2.barang_id = tbd.barang_id AND t2.item_id = tbd.item_id')
            ->addCondition('tid.transfer_item_id = :transfer_item_id')
            ->addCondition('tid.visible = 1')
            ->addParams([':transfer_item_id'=>$_POST['transfer_item_id']])
            ;
        //$txt = $cmd->getQuery();
        $this->renderJsonArr($cmd->queryAll());
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("visible = 1");
        $criteria->select = "transfer_item_details_id, -qty qty, barang_id, transfer_item_id, price, sub_total, disc, disc_rp, total_disc, total_dpp, ppn, ppn_rp, pph_id, pph, pph_rp, total, charge";
        $criteria->addCondition("transfer_item_id = :transfer_item_id");
        $criteria->params = array(':transfer_item_id'=>$_POST['transfer_item_id']);
        $model = TransferItemDetails::model()->findAll($criteria);
        $total = TransferItemDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}