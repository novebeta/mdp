<?php
class HargaJualBpController extends GxController {
	public function actionCreate() {
		$model = new HargaJualBp;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['HargaJualBp'][ $k ] = $v;
			}
			$model->attributes = $_POST['HargaJualBp'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->harga_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'HargaJualBp' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['HargaJualBp'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['HargaJualBp'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->harga_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->harga_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'HargaJualBp' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = HargaJualBp::model()->findAll( $criteria );
		$total = HargaJualBp::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionGetPrice() {
		/** @var HargaJualBp $m */
		$m     = HargaJualBp::model()
		                    ->findByAttributes( [
			                    'jasa_bp_id'        => $_POST['jasa_bp_id'],
			                    'tipe'              => $_POST['tipe'],
			                    'mobil_kategori_id' => $_POST['mobil_kategori_id']
		                    ] );
		$nilai = [
			'harga'  => 0,
			'hpp'    => 0,
			'disc'   => 0,
			'discrp' => 0,
		];
		if ( $m != null ) {
			$nilai['hpp']    = $m->hpp;
			$nilai['harga']  = $m->harga;
			$nilai['disc']   = $m->disc;
			$nilai['discrp'] = $m->discrp;
		}
		$jsonresult = '{"total":"' . 1 . '","results":' . json_encode( $nilai ) . '}';
		Yii::app()->end( $jsonresult );
	}
}