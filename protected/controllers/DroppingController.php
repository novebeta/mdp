<?php
class DroppingController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $docref = "";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = ($_POST['mode'] == 0) || ($_POST['mode'] == 3);

                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();
                $history = new DroppingHistory;

                $model = $is_new ? new Dropping : $this->loadModel($_POST['id'], 'Dropping');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Dropping')) . "Fatal error, record not found.");
                }
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(DROPPING);
                } else {
                    if ($model->store != STOREID) {
                        throw new Exception("Anda tidak diperbolehkan mengubah data ini.");
                    }

                    if ($model->lunas != PR_NEED_SHIPMENT && $model->lunas != PR_OPEN) {
                        throw new Exception("Data Transfer tidak bisa diubah.");
                    }
                    $docref = $model->doc_ref;
                    DroppingDetails::model()->updateAll(array('visible' => 0), 'dropping_id = :dropping_id',
                        array(':dropping_id' => $model->dropping_id));

                    U::delete_stock_moves_all(DROPPING, $model->dropping_id);

                    $history->status = "EDITED";
                    $history->desc = $model->store." telah mengubah transfer ke ".$model->store_penerima;
                }

                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Dropping'][$k] = $v;
                }
                $_POST['Dropping']['doc_ref'] = $docref;
                $model->attributes = $_POST['Dropping'];
                $model->up = DR_SEND;

                if($is_new){
                    /*
                     * Jika tanpa Order Dropping / Transfer Request
                     * maka harus di-approve
                     * maka status -2 / PR_NEED_SHIPMENT
                     */
                    if($model->order_dropping_id){
                        $model->approved = 1;
                        $model->lunas = PR_OPEN;
                    }else{
                        $model->approved = 0;
                        $model->lunas = PR_NEED_SHIPMENT;
                        $history->status = "CREATED";
                        $history->desc = $model->store." telah membuat transfer ke ".$model->store_penerima;
                    }
                }

	            $model->terima_barang_id = $_POST['id'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping')) . CHtml::errorSummary($model));

                foreach ($detils as $detil) {
                    $barang = Barang::model()->findByPk($detil['barang_id']);

                    $item_details = new DroppingDetails;
                    $_POST['DroppingDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['DroppingDetails']['qty'] = get_number($detil['qty']);
                    $_POST['DroppingDetails']['dropping_id'] = $model->dropping_id;
                    $_POST['DroppingDetails']['price'] = $barang->get_cost($model->store);
                    $item_details->attributes = $_POST['DroppingDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping Details')) . CHtml::errorSummary($item_details));

                    //------------------------------ khusus barang non jasa ------------------------------------
                    $saldo_stock = 0;
                    switch ($barang->tipe_barang_id){
                        case TIPE_FINISH_GOODS:
                        case TIPE_RAW_MATERIAL:
                            $saldo_stock = StockMoves::get_saldo_item($item_details->barang_id, $model->store); break;
                        case TIPE_PERLENGKAPAN:
                            $saldo_stock = StockMovesPerlengkapan::get_saldo_item($item_details->barang_id, $model->store); break;
                    }

                    if ($saldo_stock < $item_details->qty) {
                        throw new Exception(t('saldo.item.fail',
                            'app', array(
                                '{item}' => $barang->kode_barang,
                                '{h}' => $saldo_stock,
                                '{r}' => $item_details->qty
                            )));
                    }

                    /*
                     * Transfer tanpa Transfer Request
                     * tidak include stock move,
                     * harus di-approve dulu, baru di-shipment
                     * saat shipment maka akan dilakukan stock move
                     */
                    if (
                        $barang->grup->kategori->is_have_stock()
                        && ($model->order_dropping_id || $model->lunas != PR_NEED_SHIPMENT)
                    ) {
                        U::add_stock_moves_all(
                            null,
                            DROPPING,
                            $model->dropping_id,
                            $model->tgl,
                            $item_details->barang_id,
                            -$item_details->qty,
                            $model->doc_ref,
                            $barang->get_cost($model->store),
                            $model->store
                        );
                        U::add_stock_moves_all(
                            null,
                            DROPPING,
                            $model->dropping_id,
                            $model->tgl,
                            $item_details->barang_id,
                            $item_details->qty,
                            $model->doc_ref,
                            $barang->get_cost($model->store),
                            STORE_TRANSIT
                        );
                    }

                }
                if ($is_new) {
                    $ref->save(DROPPING, $model->dropping_id, $docref);
                }



                if ($model->order_dropping_id)
                {
                    //jika create Dropping berdasarkan order Dropping
                    $model_number = OrderDroppingSisa::model()
                        ->countByAttributes(array('order_dropping_id' => $model->order_dropping_id));

                    $orderDropping = OrderDropping::model()->findByPk($model->order_dropping_id);
                    if ($orderDropping) {
                        if ($model_number <= 0) {
                            $orderDropping->lunas = PR_CLOSED;
                            $history->status = "PROCESSED";
                            $history->desc = $model->store." telah mengirim semua request barang ke ".$model->store_penerima;
                            //$orderDropping->up = DR_PROCESS;
                        } else {
                            $orderDropping->lunas = PR_PROCESS;
                            $history->status = "PROCESSING";
                            $history->desc = $model->store." telah mengirim sebagian barang ke ".$model->store_penerima;
                            //$orderDropping->up = DR_PROCESS;
                        }
                        if (!$orderDropping->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Order Dropping')) .
                                CHtml::errorSummary($orderDropping));
                    }
                }


                $history->order_dropping_id = $model->order_dropping_id ? $model->order_dropping_id : $model->dropping_id;
                $history->user_id = $iduser;
                $history->user = $namauser;
                $history->doc_ref = $docref;
                $history->store = $model->store;
                $history->store_pengirim = $model->store_penerima;
                $history->tgl = new CDbExpression('NOW()');
                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping History')) . CHtml::errorSummary($history));


                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;

                U::runCommand('soap','droppingall', '--id=' . $model->dropping_id, 'droppingall_' . $model->dropping_id . '.log');
                U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');

                if ($model->order_dropping_id) {
                    U::runCommand('soap','orderdroppingall', '--id=' . $model->order_dropping_id, 'orderdroppingall_' . $model->order_dropping_id . '.log');
                }


                StockMoves::push($model->dropping_id);
                StockMovesPerlengkapan::push($model->dropping_id);
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionReapprove()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses Re approval gagal.";

            try {
                if (!HEADOFFICE) {
                    throw new Exception("Anda tidak dapat melakukan approve.");
                }

                $model = $this->loadModel($_POST['id'], 'Dropping');
                $model->up = DR_APPROVE;

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Dropping')) . CHtml::errorSummary($model));
                }

                $status = true;
                $msg = "Nomor Transfer Approval <b>".$model->doc_ref."</b> telah dikirim ulang.";

                app()->db->autoCommit = true;

                U::runCommand('soap','droppingall', '--id=' . $model->dropping_id, 'droppingall_' . $model->dropping_id . '.log');

                U::runCommand('soap','droppinghistoryresend', '--id=' . $model->dropping_id,  'droppinghistory_'.$model->dropping_id.'.log');


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->dropping_id));
            }
        }
    }

    public function actionApprove()
    {
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($_POST['id'], 'Dropping');
                $history = new DroppingHistory;
                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();

                if(!$model->approved){
                    $model->approved = 1;
                    $model->approved_user_id = Yii::app()->user->getId();
                    $model->approved_date = new CDbExpression('NOW()');
                    $model->up = DR_APPROVE;
                    if (!$model->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping')) . CHtml::errorSummary($model));


                    $history->order_dropping_id = $model->dropping_id;
                    $history->user_id = $iduser;
                    $history->user = $namauser;
                    $history->doc_ref = $model->doc_ref;
                    $history->store = $model->store;
                    $history->store_pengirim = $model->store_penerima;
                    $history->tgl = new CDbExpression('NOW()');
                    $history->status = "APPROVED";
                    $history->desc = "Transfer telah di approve oleh ".$namauser;
                    if (!$history->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping History')) . CHtml::errorSummary($history));


                    $transaction->commit();
                    $status = true;
                    $msg = "Nomor Dropping <b>".$model->doc_ref."</b> telah disetujui.";

                    U::runCommand('soap','droppingall', '--id=' . $model->dropping_id, 'droppingall_' . $model->dropping_id . '.log');
                    U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');

                }else{
                    throw new Exception("Tidak dapat approve nomor Dropping <b>".$model->doc_ref."</b>. Dropping telah disetujui.");
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = "Proses approval gagal.<br>".$ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionOpen()
    {
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($_POST['dropping_id'], 'Dropping');
                $history = new DroppingHistory;
                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();


                if($model->lunas == PR_NEED_SHIPMENT){
                    $model->lunas = PR_OPEN;
                    $model->up = DR_SEND;
                    if (!$model->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping')) . CHtml::errorSummary($model));

                    $history->order_dropping_id = $model->dropping_id;
                    $history->user_id = $iduser;
                    $history->user = $namauser;
                    $history->doc_ref = $model->doc_ref;
                    $history->store = $model->store;
                    $history->store_pengirim = $model->store_penerima;
                    $history->tgl = new CDbExpression('NOW()');
                    $history->status = "SHIPPING";
                    $history->desc = "Transfer telah di proses oleh ".$namauser;
                    if (!$history->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping History')) . CHtml::errorSummary($history));




                    U::delete_stock_moves_all(DROPPING, $model->dropping_id);
                    $tgl = app()->db->createCommand("SELECT NOW();")->queryScalar();

                    foreach ($model->droppingDetails as $item_details){
                        if (
                            $item_details->barang->grup->kategori->is_have_stock()
                            && !$model->order_dropping_id
                            && $item_details->visible == 1
                        ) {
                            U::add_stock_moves_all(
                                null,
                                DROPPING,
                                $model->dropping_id,
                                $tgl, //$model->tgl,
                                $item_details->barang_id,
                                -$item_details->qty,
                                $model->doc_ref,
                                $item_details->barang->get_cost($model->store),
                                $model->store
                            );
                            U::add_stock_moves_all(
                                null,
                                DROPPING,
                                $model->dropping_id,
                                $tgl, //$model->tgl,
                                $item_details->barang_id,
                                $item_details->qty,
                                $model->doc_ref,
                                $item_details->barang->get_cost($model->store),
                                STORE_TRANSIT
                            );
                        }
                    }

                    $transaction->commit();
                    $status = true;
                    $msg = "Nomor Dropping <b>".$model->doc_ref."</b> telah di-open.";

                    U::runCommand('soap','droppingall', '--id=' . $model->dropping_id, 'droppingall_' . $model->dropping_id . '.log');
                    U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');

                    StockMoves::push($model->dropping_id);
                    StockMovesPerlengkapan::push($model->dropping_id);
                }else{
                    throw new Exception("Tidak dapat OPEN nomor Dropping <b>".$model->doc_ref."</b>. Dropping telah di-open.");
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = "Proses open gagal.<br>".$ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Dropping')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref LIKE :doc_ref');
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition('note LIKE :note');
            $param[':note'] = '%' . $_POST['note'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store LIKE :store');
            $param[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['store_penerima'])) {
            $criteria->addCondition('store_penerima LIKE :store_penerima');
            $param[':store_penerima'] = '%' . $_POST['store_penerima'] . '%';
        }
        if (isset($_POST['lunas']) && $_POST['lunas'] != 'all') {
            $criteria->addCondition('lunas = :lunas');
            $param[':lunas'] = $_POST['lunas'];
        }
        if (isset($_POST['up']) && $_POST['up'] != 'all') {
            $criteria->addCondition('up = :up');
            $param[':up'] = $_POST['up'];
        }
        if (isset($_POST['approved']) && $_POST['approved'] != 'all') {
            $criteria->addCondition('approved = :approved');
            $param[':approved'] = $_POST['approved'];
        }
        
        if (isset($_POST['dropping_id'])) {
            $criteria->addCondition('dropping_id = :dropping_id');
            $param[':dropping_id'] = $_POST['dropping_id'];
        }
        if (isset($_POST['query']) && $_POST['query'] != '') {
            /*
             * Filter saat input No. Dropping pada form Receive Dropping
             * No. Dropping yang ditampilkan adalah :
             * - lunas != 2, Qty yang diminta belum terpenuhi
             * - approved = 1, Order Dropping telah diapprove pihak operasional
             * - cabang adalah store penerima
             */
            $criteria->addCondition('doc_ref like :query');
            $criteria->addCondition('store_penerima = :branch');
            $criteria->addCondition('lunas NOT IN (:status_close, :status_draft, :status_need_shipment)');
            $criteria->addCondition('approved = 1');
            $param[':query'] = '%' . $_POST['query'] . '%';
            $param[':branch'] = STOREID;
            $param[':status_draft'] = PR_DRAFT;
            $param[':status_need_shipment'] = PR_NEED_SHIPMENT;
            $param[':status_close'] = PR_CLOSED;
        } else {
            /*
             * Filter saat menampilkan Menu Order Dropping
             * hanya menampilkan :
             * - Dropping yang dibuat oleh cabang bersangkutan
             * - Dropping yang ditujukan kepada cabang bersangkutan
             *   (status tidak sama dengan DRAFT)
             */
            $criteria->addCondition('store = :branch OR (store_penerima = :branch AND lunas != :status_draft)');
            $param[':branch'] = STOREID;
            $param[':status_draft'] = PR_DRAFT;
        }
        
        $criteria->order = 'tgl DESC';
        
        $criteria->params = $param;
        $model = Dropping::model()->findAll($criteria);
        $total = Dropping::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    
    public function actionApprovalList()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref LIKE :doc_ref');
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition('note LIKE :note');
            $param[':note'] = '%' . $_POST['note'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store LIKE :store');
            $param[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['store_penerima'])) {
            $criteria->addCondition('store_penerima LIKE :store_penerima');
            $param[':store_penerima'] = '%' . $_POST['store_penerima'] . '%';
        }
        if (isset($_POST['lunas']) && $_POST['lunas'] != 'all') {
            $criteria->addCondition('lunas = :lunas');
            $param[':lunas'] = $_POST['lunas'];
        }
        if (isset($_POST['up']) && $_POST['up'] != 'all') {
            $criteria->addCondition('up = :up');
            $param[':up'] = $_POST['up'];
        }
        if (isset($_POST['approved']) && $_POST['approved'] != 'all') {
            $criteria->addCondition('approved = :approved');
            $param[':approved'] = $_POST['approved'];
        }
        if (isset($_POST['jenis']) && $_POST['jenis'] != 'all') {
            $criteria->addCondition('jenis = :jenis');
            $param[':jenis'] = $_POST['jenis'];
        }
        /*
         * jenis :
         * 0 -> ORDER DROPPING
         * 1 -> DROPPING
            $criteria->addCondition('jenis = 0');
         */
        
        $criteria->order = 'tgl DESC, tdate DESC';
        
        $criteria->params = $param;
        $model = DroppingApproval::model()->findAll($criteria);
        $total = DroppingApproval::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionResend()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses kirim ulang gagal.";

            try {
                $model = $this->loadModel($_POST['id'], 'Dropping');
                $model->up = DR_SEND;

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Dropping')) . CHtml::errorSummary($model));
                }

                $msg = "Nomor Transfer <b>".$model->doc_ref."</b> telah di kirim ulang..";
                $status = true;

                U::runCommand('soap','droppingall', '--id=' . $model->dropping_id, 'droppingall_' . $model->dropping_id . '.log');

                if ($model->order_dropping_id)
                {
                    //jika create Dropping berdasarkan order Dropping
                    $model_number = OrderDroppingSisa::model()
                        ->countByAttributes(array('order_dropping_id' => $model->order_dropping_id));

                    $orderDropping = OrderDropping::model()->findByPk($model->order_dropping_id);
                    if ($orderDropping == null) {
                        throw new Exception("Transfer Requests tidak ditemukan!");
                    } else {
                        if ($model_number <= 0) {
                            $orderDropping->lunas = PR_CLOSED;
                            //$orderDropping->up = DR_PROCESS;
                        } else {
                            $orderDropping->lunas = PR_PROCESS;
                            //$orderDropping->up = DR_PROCESS;
                        }
                        if (!$orderDropping->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Order Dropping')) .
                                CHtml::errorSummary($orderDropping));
                    }

                    U::runCommand('soap','orderdroppingall', '--id=' . $model->order_dropping_id, 'orderdroppingall_' . $model->order_dropping_id . '.log');
                    U::runCommand('soap','droppinghistoryresend', '--id=' . $model->order_dropping_id,  'droppinghistory_'.$model->order_dropping_id.'.log');
                }
                else
                {
                    U::runCommand('soap','droppinghistoryresend', '--id=' . $model->dropping_id,  'droppinghistory_'.$model->dropping_id.'.log');
                }


                StockMoves::push($model->dropping_id);
                StockMovesPerlengkapan::push($model->dropping_id);
            }
            catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $model->doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

	public function actionGetsisatransfer()
	{
		if (!isset($_POST['id_terima']))
			return;

		$tr = "";
		if (isset($_POST['id_tr']))
			$tr = $_POST['id_tr'];

		$model = Dropping::get_available_terima_barang($_POST['id_terima'], $tr);
		$this->renderJsonArr($model);

	}
}