<?php

class CustomerslinkageController extends GxController {

    public function actionCreate() {
        $model = new CustomersLinkage;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                    $_POST['CustomersLinkage'][$k] = $v;
            }
            $model->attributes = $_POST['CustomersLinkage'];
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->linkage_id;
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
            Yii::app()->end();

        }

    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'CustomersLinkage');


        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                    $_POST['CustomersLinkage'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['CustomersLinkage'];

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->linkage_id;
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest){
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg
                ));
                Yii::app()->end();
            }else{
                $this->redirect(array('view', 'id' => $model->linkage_id));
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'CustomersLinkage')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
            Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    /* public function actionIndex() {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||(isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = CustomersLinkage::model()->findAll($criteria);
        $total = CustomersLinkage::model()->count($criteria);

        $this->renderJson($model, $total);

    } */

    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $select = "SELECT nc.customer_id,nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,
          nc.email,nc.telp,nc.alamat,nc.kecamatan_id,nc.status_cust_id,nc.sex,nc.kerja,nc.store,nc.kota_id,
          nc.provinsi_id,nc.negara_id,nc.friend_id,nc.info_id,nl.customers_id,nl.online_id FROM nscc_customers AS nc LEFT JOIN nscc_customers_linkage as nl ON nc.customer_id = nl.customers_id";
        $limit = " LIMIT $start,$limit";
        $where = "";
        $order = " ORDER BY nc.awal";
        $param = array();
        $wheretgl = "";
        if (isset($_POST['customer_id'])) {
            unset($_POST['query']);
        }
        if (isset($_POST['query'])) {
            $param = array(
                ':nama_customer' => "%" . $_POST['query'] . "%",
//                ':alamat' => "%" . $_POST['query'] . "%",
                ':no_customer' => "%" . $_POST['query'] . "%",
//                ':telp' => "%" . $_POST['query'] . "%"
            );
	        if (NATASHA_CUSTOM)
		        $param = array(
			        ':nama_customer' => "%" . $_POST['query'] . "%",
			        ':no_customer' => $_POST['query'],
		        );

            if (isset($_POST['mode']) && $_POST['mode'] == 'master') {
                $wheretgl = ' AND tgl_lahir = :tgl';
                $param[':tgl'] = $_POST['tgl'];
            }
            $where = " WHERE (nc.nama_customer LIKE :nama_customer OR no_customer LIKE :no_customer) " . $wheretgl;
	        if (NATASHA_CUSTOM)
		        $where = " WHERE (nc.nama_customer LIKE :nama_customer OR no_customer = :no_customer) " . $wheretgl;

            $total_cmd = Yii::app()->db->createCommand($select . $where);
            $total = $total_cmd->query($param)->rowCount;
            $comm = Yii::app()->db->createCommand($select . $where . $order . $limit);
            $row = $comm->queryAll(true, $param);
            $this->renderJsonArrWithTotal($row, $total);
            Yii::app()->end();
        }
        if (isset($_POST['no_customer'])) {
            $where = " WHERE no_customer LIKE :no_customer ";
	        $param[':no_customer'] = "%" . $_POST['no_customer'] . "%";

	        if (NATASHA_CUSTOM) {
		        $where = " WHERE no_customer = :no_customer ";
		        $param[':no_customer'] = $_POST['no_customer'];
	        }
        }
        if (isset($_POST['nama_customer'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " nama_customer like :nama_customer ";
            $param[':nama_customer'] = "%" . $_POST['nama_customer'] . "%";
        }
        
        if (isset($_POST['telp'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " telp like :telp ";
            $param[':telp'] = "%" . $_POST['telp'] . "%";
        }
        if (isset($_POST['customer_id'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " customer_id = :customer_id ";
            $param[':customer_id'] = $_POST['customer_id'];
        }
        $total_cmd = Yii::app()->db->createCommand($select . $where);
        $total = $total_cmd->query($param)->rowCount;
        $comm = Yii::app()->db->createCommand($select . $where . $order . $limit);
//        $comm = Yii::app()->db->createCommand($select . $where . $order);
        $row = $comm->queryAll(true, $param);
        $this->renderJsonArrWithTotal($row, $total);
    }

    public function actionLinkage($id){
        $model = Customers::model()->findByPk($id);
        $data = [];
        $data['phone'] = $model->telp;
        $data['name'] = $model->nama_customer;
        $data['email'] = $model->email;
        $data['birthday'] = $model->tgl_lahir;
        $data['gender'] = ucfirst(strtolower($model->sex));
        $data['customer_id'] = $model->customer_id;
        $data['no_pasien'] = $model->no_customer;
        $data['outlet_code'] = $model->store;
        $data = JSON_ENCODE($data);

        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/users/create/natasha');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/users/create/natasha',$data);
        
        //echo $output["status"];
        //echo $output;
        $output = json_decode($output,true);
        $msg = '';
        $status = 'success';
        if($output['status'] == 'success'){
            $link  =  new CustomersLinkage;
            $link->customers_id = $id;
            $link->online_id = $output['result']['online_id'];
            //$model->online_id = $output['result']['online_id'];
            if (!$link->save()) {
                $status = 'fail';
                $msg = t('save.model.fail', 'app',
                        array('{link}' => 'Customers')) . CHtml::errorSummary($model);
            }
            $msg = "id online = ".$link->online_id;
        }else{
            $msg = $output['messages'];
            $status = $output['status'];
        }

        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();

    }

    public function actionVouchers(){
        
        $model = Customers::model()->findByPk($_POST['id']);
        $linkage = CustomersLinkage::model()->findByAttributes(array('customers_id'=>$_POST['id']));
        /* $model = Customers::model()->findByPk('affbd05d-dc1c-11e8-bc44-a81e843c3480');
        $linkage = CustomersLinkage::model()->findByAttributes(array('customers_id'=>'affbd05d-dc1c-11e8-bc44-a81e843c3480')); */
        $data = [];
        $data['id_online'] = $linkage->online_id;
        $data['outlet_code'] = $model->store;
        $data = JSON_ENCODE($data);

        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api//voucher/myvoucher');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/voucher/myvoucher',$data);

       // $output = json_decode($output,true);
        echo $output;
        //print_r($output);
    }

    

    public function actionOtp(){

        $id = $_POST['id'];
        $vid = $_POST['vid'];
        $store = $_POST['store'];

        $data = [];
        $data['id_voucher_redemption'] = $vid;
        $data['id_online'] = $id;
        $data['outlet_code'] = $store;
        $data = JSON_ENCODE($data);

        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/voucher/myvoucher/otp');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/voucher/myvoucher/otp',$data);

        echo $output;//= json_decode($output,true);
    }

    public function actionVoucheruse(){

        $id = $_POST['id'];
        $vid = $_POST['vid'];
        $store = $_POST['store'];
        $otp = $_POST['otp'];

        $data = [];
        $data['id_voucher_redemption'] = $vid;
        $data['outlet_code'] = $store;
        $data['id_online'] = $id;
        $data['otp'] = $otp;
        $data = JSON_ENCODE($data);

        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api//voucher/myvoucher/use');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/voucher/myvoucher/use',$data);

        echo $output;// = json_decode($output,true);
    }

    public function actionVoucherbuy(){
        $data = [];
        $data['id_online'] = $_POST['id'];
        $data['id_voucher'] = $_POST['vid'];
        $data['outlet_code'] = $_POST['store'];
        $data = JSON_ENCODE($data);

        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api//voucher/myvoucher/buy');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/voucher/myvoucher/buy',$data);

       // $output = json_decode($output,true);
        echo $output;
    }


}