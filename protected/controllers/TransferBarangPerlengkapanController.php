<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.GL');
class TransferBarangPerlengkapanController extends GxController
{
    public function actionCreateIn()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new TransferBarangPerlengkapan;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PERLENGKAPAN_IN);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferBarangPerlengkapan'][$k] = $v;
                }
                /*
//                $new_details = array();
//                foreach ($detils as $detil) {
//                    $barang = BarangPerlengkapan::model()->findByPk($detil['barang_perlengkapan_id']);
//                    $qty = get_number($detil['qty']);
//                    $harga_beli = Beli::model()->findByAttributes(array('barang_id' => $barang->barang_id, 'store' => STOREID));
//                    if ($harga_beli == null) {
//                        throw new Exception("Default purchase price not define.");
//                    }
//                    $price = $barang->harga_perlengkapan;
//                    $vat = $harga_beli->tax != 0 ? round($harga_beli->tax / 100, 2) : 0;
//                    $bruto = round($price * $qty, 2);
//                    $vatrp = round($vat * $bruto, 2);
//                    $total = $bruto;
//                    $detil['price'] = $price;
//                    $detil['bruto'] = $bruto;
//                    $detil['total'] = $total;
//                    $detil['vat'] = $vat;
//                    $detil['vatrp'] = $vatrp;
//                    $_POST['TransferBarangPerlengkapan']['vat'] += $vatrp;
//                    $_POST['TransferBarangPerlengkapan']['bruto'] += $bruto;
//                    $_POST['TransferBarangPerlengkapan']['total'] += $total;
//                    $new_details[] = $detil;
//                }
                 */
                $_POST['TransferBarangPerlengkapan']['doc_ref'] = $docref;
                $_POST['TransferBarangPerlengkapan']['user_id'] = app()->user->getId();
                $model->attributes = $_POST['TransferBarangPerlengkapan'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive Barang Perlengkapan')) . CHtml::errorSummary($model));
//                $gl = new GL();
//                $tipe_beli = "Supplier perlengkapan " . $model->supplier->supplier_name;
//                $coa_hutang = $model->supplier->account_code;
                foreach ($detils as $detil) {
                    $item_details = new TransferBarangPerlengkapanDetails;
                    $_POST['TransferBarangPerlengkapanDetails']['barang_perlengkapan_id'] = $detil['barang_perlengkapan_id'];
                    $_POST['TransferBarangPerlengkapanDetails']['qty'] = get_number($detil['qty']);
                    $_POST['TransferBarangPerlengkapanDetails']['price'] = get_number($detil['price']);
                    $_POST['TransferBarangPerlengkapanDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['TransferBarangPerlengkapanDetails']['total'] = get_number($detil['total']);
                    $_POST['TransferBarangPerlengkapanDetails']['account_code'] = $detil['account_code'];
                    $_POST['TransferBarangPerlengkapanDetails']['vat'] = get_number($detil['vat']);
                    $_POST['TransferBarangPerlengkapanDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['TransferBarangPerlengkapanDetails']['transfer_perlengkapan_id'] = $model->transfer_perlengkapan_id;
                    $item_details->attributes = $_POST['TransferBarangPerlengkapanDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    
                    $total = $item_details->total;
                    $item_details->barangPerlengkapan->count_biaya_beli($item_details->qty, $total, $model->store);
                    
                    //if ($item_details->barangPerlengkapan->kategori->is_have_stock()) {
                        U::add_stock_moves_perlengkapan(PERLENGKAPAN_IN, $model->transfer_perlengkapan_id, $model->tgl,
                            $item_details->barang_perlengkapan_id, $item_details->qty, $model->doc_ref,
                            $item_details->barangPerlengkapan->get_cost($model->store), $model->store);
                    //}
//                    if ($item_details->total != 0) {
//                        $gl->add_gl_Perlengkapan(PERLENGKAPAN_IN, $model->transfer_perlengkapan_id, $model->tgl, $docref, $item_details->account_code,
//                            "Purchase $tipe_beli", "Purchase $tipe_beli", $item_details->total, 0,$model->store);
//                        $gl->add_gl_Perlengkapan(PERLENGKAPAN_IN, $model->transfer_perlengkapan_id, $model->tgl, $docref, $coa_hutang,
//                            "Purchase $tipe_beli", "Purchase $tipe_beli", -$item_details->total, 0,$model->store);
//                    }
                }
//                $gl->validate();
                $ref->save(PERLENGKAPAN_IN, $model->transfer_perlengkapan_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = 0 AND DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = TransferBarangPerlengkapan::model()->findAll($criteria);
        $total = TransferBarangPerlengkapan::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    
    public function actionCreateOut()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new TransferBarangPerlengkapan;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PERLENGKAPAN_OUT);
                
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferBarangPerlengkapan'][$k] = $v;
                }
                
                $_POST['TransferBarangPerlengkapan']['total'] = 0;
                $_POST['TransferBarangPerlengkapan']['lunas'] = 1;
                $new_details = array();
                foreach ($detils as $detil) {
                    $saldoPerlengkapan = StockMovesPerlengkapan::get_saldo_item($detil['barang_perlengkapan_id'], $_POST['store']);
                    if ($saldoPerlengkapan<get_number($detil['qty'])) {
                        $barang = BarangPerlengkapan::model()->findByAttributes(array('barang_perlengkapan_id' => $detil['barang_perlengkapan_id']));
                        throw new Exception("Jumlah Barang Perlengkapan tidak mencukupi.<br>".$barang->kode_brg_perlengkapan." - ".$barang->nama_brg_perlengkapan."<br>Saldo  : ".$saldoPerlengkapan." ".$barang->satuan_perlengkapan);
                    }
                    $harga_beli = CostBarangPerlengkapan::model()->findByAttributes(array('barang_perlengkapan_id' => $detil['barang_perlengkapan_id']));
                    if ($harga_beli == null) {
                        throw new Exception("Default purchase price not define, please contact accounting person");
                    }
                    $qty = get_number($detil['qty']);
                    $price = $harga_beli->cost;
                    $total = $qty*$price;
                    $detil['total'] = $total;
                    $_POST['TransferBarangPerlengkapan']['total'] += $total;
                    $new_details[] = $detil;
                }
                $_POST['TransferBarangPerlengkapan']['total'] = -$_POST['TransferBarangPerlengkapan']['total'];
                $_POST['TransferBarangPerlengkapan']['doc_ref'] = $docref;
                $model->attributes = $_POST['TransferBarangPerlengkapan'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                
                $gl = new GL();
//                $coa_hutang = "";
//                $tipe_beli = "";
                foreach ($new_details as $detil) {
                    $item_details = new TransferBarangPerlengkapanDetails;
                    $_POST['TransferBarangPerlengkapanDetails']['barang_perlengkapan_id'] = $detil['barang_perlengkapan_id'];
                    $_POST['TransferBarangPerlengkapanDetails']['qty'] = -get_number($detil['qty']);
                    $_POST['TransferBarangPerlengkapanDetails']['transfer_perlengkapan_id'] = $model->transfer_perlengkapan_id;
//                    $_POST['TransferBarangPerlengkapanDetails']['price'] = get_number($detil['price']);
//                    $_POST['TransferBarangPerlengkapanDetails']['bruto'] = -get_number($detil['bruto']);
                    $_POST['TransferBarangPerlengkapanDetails']['total'] = -get_number($detil['total']);
//                    $_POST['TransferBarangPerlengkapanDetails']['vat'] = get_number($detil['vat']);
//                    $_POST['TransferBarangPerlengkapanDetails']['vatrp'] = -get_number($detil['vatrp']);
                    $item_details->attributes = $_POST['TransferBarangPerlengkapanDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive barang detail')) . CHtml::errorSummary($item_details));
                    
                    U::add_stock_moves_perlengkapan(PERLENGKAPAN_OUT, $model->transfer_perlengkapan_id, $model->tgl,
                            $item_details->barang_perlengkapan_id, $item_details->qty, $model->doc_ref,
                            $item_details->barangPerlengkapan->get_cost($model->store), $model->store);
                    
//                    if ($item_details->total != 0) {
//                        $gl->add_gl_Perlengkapan(PERLENGKAPAN_OUT, $model->transfer_perlengkapan_id, $model->tgl, $docref, COA_PERSEDIAAN_CABANG,
//                            "Perlengkapan Keluar ".$docref, "Perlengkapan Keluar ".$docref, $item_details->total, 0);
//                        $gl->add_gl_Perlengkapan(PERLENGKAPAN_OUT, $model->transfer_perlengkapan_id, $model->tgl, $docref, COA_PERSEDIAAN_PUSAT,
//                            "Perlengkapan Keluar ".$docref, "Perlengkapan Keluar ".$docref, -$item_details->total, 0);
//                    }
                }
                $gl->validate();
                $ref->save(PERLENGKAPAN_OUT, $model->transfer_perlengkapan_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = 1 AND DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = TransferBarangPerlengkapan::model()->findAll($criteria);
        $total = TransferBarangPerlengkapan::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}