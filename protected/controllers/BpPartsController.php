<?php
class BpPartsController extends GxController {
	public function actionCreate() {
		$model = new BpParts;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST[ 'BpParts' ][ $k ] = $v;
			}
			$model->attributes = $_POST[ 'BpParts' ];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bp_parts_id;
			} else {
				$msg    .= " " . Chtml::errorSummary( $model );
				$status = false;
			}
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg
			] );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'BpParts' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST[ 'BpParts' ][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST[ 'BpParts' ];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bp_parts_id;
			} else {
				$msg    .= " " . Chtml::errorSummary( $model );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( [
					'success' => $status,
					'msg'     => $msg
				] );
				Yii::app()->end();
			} else {
				$this->redirect( [ 'view', 'id' => $model->bp_parts_id ] );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'BpParts' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg
			] );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		$criteria = new CDbCriteria();
		$criteria->addCondition( 'body_paint_id = :body_paint_id' );
		$criteria->params[ ':body_paint_id' ] = $_POST[ 'body_paint_id' ];
		$model                                = BpParts::model()->findAll( $criteria );
		$total                                = BpParts::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionPosting( $id ) {
		/** @var BpParts $model */
		$model = $this->loadModel( $id, 'BpParts' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$msg                  = "Data gagal disimpan";
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				$model->hpp     = floatval($_POST[ 'hpp' ]) * $model->qty;
				$model->posting = new CDbExpression( 'NOW()' );
				$gl      = new GL();
				$ref     = $model->bodyPaint->wo_no;
				$note    = $ref . " " . $model->nama;
				$nominal = $model->hpp;
				$ppn     = 0;
				$gl->add_gl( BODYPAINT_PARTS, $model->bp_parts_id, $model->posting,
					$ref, '52-00-03', $note, $note, $nominal, 0, '' );
				if ( boolval( $_POST[ 'ppn' ] ) ) {
					$ppn = floatval($_POST[ 'ppnout' ]) * $model->qty;
					$gl->add_gl( BODYPAINT_PARTS, $model->bp_parts_id, $model->posting,
						$ref, '11-09-03', $note, $note, $ppn, 0, '' );
				}
				$gl->add_gl( BODYPAINT_PARTS, $model->bp_parts_id, $model->posting,
					$ref, '21-01-03', $note, $note, - ( $nominal + $ppn ), 0, '' );
				$gl->validate();
				$model->ppnout           = $ppn;
				$model->tipe             = $_POST[ 'tipe' ];
				$model->bp_supp_parts_id = $_POST[ 'bp_supp_parts_id' ];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							[ '{model}' => 'BodyPaint' ] ) . CHtml::errorSummary( $model ) );
				}
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg//,
			] );
			Yii::app()->end();
		}
	}
	public function actionUnPosting( $id ) {
		/** @var BpParts $model */
		$model = $this->loadModel( $id, 'BpParts' );
		$msg                  = "Data gagal disimpan";
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		try {
			GlTrans::model()->deleteAllByAttributes( [ 'type' => BODYPAINT_PARTS, 'type_no' => $model->bp_parts_id ] );
			$model->hpp              = 0;
			$model->posting          = new CDbExpression( 'NULL' );
			$model->ppnout           = 0;
			$model->tipe             = new CDbExpression( 'NULL' );
			$model->bp_supp_parts_id = new CDbExpression( 'NULL' );
			if ( ! $model->save() ) {
				throw new Exception( t( 'save.model.fail', 'app',
						[ '{model}' => 'BodyPaint' ] ) . CHtml::errorSummary( $model ) );
			}
			$msg = 'Berhasil di unposting';
			$transaction->commit();
			$status = true;
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
		}
		app()->db->autoCommit = true;
		echo CJSON::encode( [
			'success' => $status,
			'msg'     => $msg//,
		] );
	}
}