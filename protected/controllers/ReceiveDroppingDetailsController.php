<?php
class ReceiveDroppingDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();

        if (isset($_POST['receive_dropping_id'])) {
            $criteria->addCondition("visible = 1");
            $criteria->addCondition("receive_dropping_id = :receive_dropping_id");
            $criteria->params = array(':receive_dropping_id' => $_POST['receive_dropping_id']);
            $model = ReceiveDroppingDetails::model()->findAll($criteria);
            $total = ReceiveDroppingDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        } elseif (isset($_POST['dropping_id'])) {
            $criteria->addCondition("dropping_id = :dropping_id");
            $criteria->params = array(':dropping_id' => $_POST['dropping_id']);
            $model = DroppingSisa::model()->findAll($criteria);
            $total = DroppingSisa::model()->count($criteria);
            $this->renderJson($model, $total);
        }

    }
}