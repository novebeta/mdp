<?php
Yii::import('application.components.GL');
class GlTransController extends GxController
{
    public function actionCreate()
    {
        if (!app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $msg = t('save.success', 'app');
            $detils = CJSON::decode($_POST['detil']);
            $is_new = $_POST['mode'] == 0;
            app()->db->autoCommit = false;
            $transaction = app()->db->beginTransaction();
            try {
                $gl = new GL();
                if ($is_new) {
                    $ref = new Reference;
                    $docref = $ref->get_next_reference(JURNAL_UMUM);
                    $command = Yii::app()->db->createCommand("SELECT UUID();");
                    $uuid = $command->queryScalar();
                    $jurnal_umum_id = $uuid;
                } else {
                    $docref = Reference::get_reference(JURNAL_UMUM, $_POST['id']);
                    $this->delete_gl_trans(JURNAL_UMUM, $_POST['id']);
                    $jurnal_umum_id = $_POST['id'];
                }
                $total_debit = 0;
                $total_kredit = 0;
                $current_branch = '';
                switch ($_POST['all_store']) {
                    case 0 :
                        foreach ($detils as $detil) {
                            $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                            $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                                $detil['account_code'], $detil["memo_"], '', $amount, 0, $detil['store']);
                        }
                        break;
                    case 1 :
                        $store_acc = Store::getStoreBebanAcc();
                        $jml_store = count($store_acc);
                        foreach ($detils as $detil) {
                            $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                            if ($amount > 0) {
                                $amount = round($amount / $jml_store, 2);
                                foreach ($store_acc as $key => $item) {
                                    $total_debit += $amount;
                                    $current_branch = $item['store_kode'];
                                    $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                                        $detil['account_code'], $detil["memo_"], '', $amount, 0, $current_branch);
                                }
                            } else {
                                $total_kredit += $amount;
                                $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                                    $detil['account_code'], $detil["memo_"], '', $amount, 0, $detil['store']);
                            }
                        }
                        break;
                    case 2 :
                        $store_acc = Store::getStoreBebanAcc();
                        $jml_store = count($store_acc);
                        foreach ($detils as $detil) {
                            $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                            if ($amount > 0) {
                                $total_debit += $amount;
                                $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                                    $detil['account_code'], $detil["memo_"], '', $amount, 0, $detil['store']);
                            } else {
                                $amount = round($amount / $jml_store, 2);
                                foreach ($store_acc as $key => $item) {
                                    $total_kredit += $amount;
                                    $current_branch = $item['store_kode'];
                                    $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                                        $detil['account_code'], $detil["memo_"], '', $amount, 0, $current_branch);
                                }
                            }
                        }
                        break;
                    case 3 :
                        $store_acc = Store::getStoreBebanAcc();
                        $jml_store = count($store_acc);
                        foreach ($detils as $detil) {
                            $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                            $amount = round($amount / $jml_store, 2);
                            foreach ($store_acc as $key => $item) {
                                $current_branch = $item['store_kode'];
                                if ($amount > 0) {
                                    $total_debit += $amount;
                                } else {
                                    $total_kredit += $amount;
                                }
                                $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                                    $detil['account_code'], $detil["memo_"], '', $amount, 0, $current_branch);
                            }
                        }
                        break;
                }
                $selisih = $total_debit - abs($total_kredit);
                if ($selisih != 0.00) {
                    $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                        COA_ROUNDING, 'Pembulatan sisa bagi beban all cabang', '',
                        -$selisih, 0, $current_branch);
                }
                $gl->validate();
                if ($is_new) {
                    $ref->save(JURNAL_UMUM, $jurnal_umum_id, $docref);
                }
                $transaction->commit();
                $status = true;
                if(PUSH_PUSAT){
                    
                    U::runCommand('checkdata', 'gltrans', '--tno='.$jurnal_umum_id , 'gl_tno'.$jurnal_umum_id.'.log');
                    
                    U::runCommand('checkdata', 'ref', '--tno='.$jurnal_umum_id,  'ref_tno'.$jurnal_umum_id.'.log');
                }
                
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            app()->end();
        }
    }
    public function actionIndex()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $this->renderJsonArr(GlTrans::jurnal_umum_details($_POST['type_no']));
        }
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
//        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        if ((isset($_POST['tran_date']))){
//            $criteria->addCondition("tran_date like '%".$_POST['tran_date']."%'");
//        }
//        if ((isset($_POST['reference']))){
//            $criteria->addCondition("reference like '%".$_POST['reference']."%'");
//        }
//        if ((isset($_POST['tot_debit']))){
//            $criteria->addCondition("tot_debit like '%".$_POST['tot_debit']."%'");
//        }
//        if ((isset($_POST['store']))){
//            $criteria->addCondition("store like '%".$_POST['store']."%'");
//        }
//        $model = GlTrans::model()->findAll($criteria);
//        $total = GlTrans::model()->count($criteria);
//        $this->renderJson($model, $total);
    }
    public function actionListJurnalUmum()
    {
        //RUMUS Penerapan Add Condition di dalam Array
        $a = '';
        if ((isset($_POST['tran_date']))) {
            $a .= " AND tran_date like '%" . $_POST['tran_date'] . "%' ";
        }
        if ((isset($_POST['reference']))) {
            $a .= " AND reference like '%" . $_POST['reference'] . "%' ";
        }
        if ((isset($_POST['tot_debit']))) {
            $a .= " AND tot_debit like '%" . $_POST['tot_debit'] . "%' ";
        }
        if ((isset($_POST['memo_']))) {
            $a .= " AND memo_ like '%" . $_POST['memo_'] . "%' ";
        }
        if ((isset($_POST['store']))) {
            $a .= " AND store like '%" . $_POST['store'] . "%' ";
        }
        $this->renderJsonArr(GlTrans::jurnal_umum_index($_POST['tgl'], $a));
    }
    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $this->delete_gl_trans(JURNAL_UMUM, $_POST['type_no']);
                $this->delete_refs(JURNAL_UMUM, $_POST['type_no']);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg)
            );
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}