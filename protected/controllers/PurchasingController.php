<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.GL');
class PurchasingController extends GxController
{
    public function actionCreateIn()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new PurchaseCoa;
                $ref = new Reference();
                $docref = $ref->get_next_reference(SUPPIN);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferItem'][$k] = $v;
                }
                $_POST['TransferItem']['bruto'] = 0;
//                $_POST['TransferItem']['total'] = 1000;
                $_POST['TransferItem']['vat'] = 0;
               
                $new_details = array();
                foreach ($detils as $detil) {
//                    $barang = Barang::model()->findByPk($detil['barang_id']);
////                    $qty = get_number($detil['qty']);
////                    $harga_beli = Beli::model()->findByAttributes(array('barang_id' => $barang->barang_id, 'store' => STOREID));
////                    if ($harga_beli == null) {
////                        throw new Exception("Default purchase price not define.");
////                    }
////                    $price = $harga_beli->price;
////                    $vat = $harga_beli->tax != 0 ? round($harga_beli->tax / 100, 2) : 0;
//                    $bruto = round($price * $qty, 2);
//                    $vatrp = round($vat * $bruto, 2);
                    $detail['account_code'] = $detil['account_code'];
                    $detail['item_name'] = $detil['item_name'];
                    $detail['total'] = $detil['total'];
//                    $detil['total'] = $total;
//                    $detil['vat'] = $vat;
//                    $detil['vatrp'] = $vatrp;                    
//                    $_POST['TransferItem']['vat'] += $vatrp;
//                    $_POST['TransferItem']['bruto'] += $bruto;
//                    $_POST['TransferItem']['total'] += $total;
                    $new_details[] = $detail;
                }               
                $_POST['TransferItem']['doc_ref'] = $docref;
                $_POST['TransferItem']['terima_barang_id'] = 'd29b4c45-6068-11e6-a95a-201a069ec688';
                $_POST['TransferItem']['user_id'] = app()->user->getId();
                $model->attributes = $_POST['TransferItem'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
//                $gl = new GL();
//                $coa_hutang = "";
//                $tipe_beli = "";
////                $bank = "";
//                if ($model->supplier_id == null) {
//                    $tipe_beli = "CASH";
//                    $bank = Bank::get_bank_cash();
//                    $coa_hutang = $bank->account_code;
//                } else {
//                    $tipe_beli = "supplier " . $model->supplier->supplier_name;
//                    $coa_hutang = $model->supplier->account_code;
//                }
                foreach ($detils as $detil) {
//                    $detail['account_code'] = $detil['account_code'];
//                    $detail['item_name'] = $detil['item_name'];
//                    $detail['total'] = $detil['total'];
                    $kas_detail = new PurchaseCoaDetail;
                    $_POST['PurchasingDetailsGrid']['account_code'] = $detil['account_code'];
                    $_POST['PurchasingDetailsGrid']['item_name'] = $detil['item_name'];
                    $_POST['PurchasingDetailsGrid']['total'] =  $detil['total'];
                    $_POST['PurchasingDetailsGrid']['purchase_coa_id'] = $model->purchase_coa_id;
                    $kas_detail->attributes = $_POST['PurchasingDetailsGrid'];
                    if (!$kas_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Detail Cash')) . CHtml::errorSummary($kas_detail));
                    }
//                    if ($model->all_store == 1) {
//                        $nominal_item = round($kas_detail->total / $model->jml_cabang, 2);
//                        foreach ($store_acc as $key => $row) {
//                            $current_branch = $row['store_kode'];
//                            $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
//                                $kas_detail->account_code, $kas_detail->item_name, "",
//                                -$nominal_item, 1, $current_branch);
//                            $total_nominal += $nominal_item;
//                        }
//                    } else {
//                        $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
//                            $kas_detail->account_code, $kas_detail->item_name, '',
//                            -$kas_detail->total, 1, $model->store);
//                    }
                }
//                $gl->validate();
                $ref->save(SUPPIN, $model->purchase_coa_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
  
    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = 0 AND DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = PurchaseCoa::model()->findAll($criteria);
        $total = PurchaseCoa::model()->count($criteria);
        $this->renderJson($model, $total);
    }
//    public function actionIndexOut()
//    {
//        $criteria = new CDbCriteria();
//        $criteria->select = "purchase_coa_id,tgl,doc_ref,note,tdate,doc_ref_other,
//        user_id,type_,store,supplier_id,-total total,disc,-discrp discrp,-bruto bruto,-vat vat";
//        $criteria->addCondition("type_ = 1 AND DATE(tgl) = :tgl");
//        $criteria->params = array(':tgl' => $_POST['tgl']);
//        $model = TransferItem::model()->findAll($criteria);
//        $total = TransferItem::model()->count($criteria);
//        $this->renderJson($model, $total);
//    }
}