<?php
class SalesMdpController extends GxController {
	public function actionCreate() {
//		$model = new SalesMdp;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$is_new               = $_POST['mode'] == 0;
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				/** @var SalesMdp $model */
				if ( ! $is_new ) {
					SalesDetailMdp::model()->deleteAll( 'sales_id = :sales_id', [ ':sales_id' => $_POST['id'] ] );
				}
				$model = $is_new ? new SalesMdp : $this->loadModel( $_POST['id'], 'SalesMdp' );
				if ( $is_new ) {
					$model->tgl_so  = $_POST['tgl_so'];
					$model->doc_ref = new CDbExpression(
						"fnc_ref_faktur('" . STOREID . "','" . $_POST['tgl_so'] . "')" );
				}
				$model->mobil_id  = $_POST['mobil_id'];
				$model->mobil_km  = $_POST['mobil_km'];
				$model->sub_total = get_number( $_POST['sub_total'] );
				$model->totalpot  = get_number( $_POST['totalpot'] );
				$model->total_vat = get_number( $_POST['total_vat'] );
				$model->total     = get_number( $_POST['total'] );
//				$model->tgl_wo    = new CDbExpression( 'NOW()' );
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				foreach ( $detils as $detil ) {
					$salesD                 = new SalesDetailMdp;
					$salesD->sales_id       = $model->sales_id;
					$salesD->barang_id      = $detil['barang_id'];
					$salesD->qty            = get_number( $detil['qty'] );
					$salesD->harga          = get_number( $detil['harga'] );
					$salesD->discrp         = get_number( $detil['discrp'] );
					$salesD->total          = get_number( $detil['total'] );
					$salesD->vatrp          = get_number( $detil['vatrp'] );
					$salesD->vat            = get_number( $detil['vat'] );
					$salesD->tot_qty_harga  = get_number( $detil['tot_qty_harga'] );
					$salesD->tot_after_disc = get_number( $detil['tot_after_disc'] );
					if ( ! $salesD->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'Sales Detail' ) ) . CHtml::errorSummary( $salesD ) );
					}
				}
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg//,
				//'print' => $print
			) );
			Yii::app()->end();
		}
	}
	public function actionCheck() {
		if ( STOREID == '' ) {
			echo CJSON::encode( [
				'status' => true,
				'msg'    => 'OK'
			] );
			Yii::app()->end();
		}
		/** @var Store $store */
		$store = Store::model()->findByPk( STOREID );
		$date  = date( 'd' );
		if ( $date > $store->tgl_jt_tagihan ) {
			$inv = InvoiceMdp::model()->count( 'store_kode = :store_kode AND tgl_bayar is NULL',
				[ ':store_kode' => STOREID ] );
			if ( $inv > 0 ) {
				echo CJSON::encode( [
					'status' => true,
					'msg'    => 'Sales Order tidak bisa dibuat karena ' . $inv . ' invoice belum dibayar.'
				] );
				Yii::app()->end();
			}
		}
		echo CJSON::encode( [
			'status' => true,
			'msg'    => 'OK'
		] );
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition( 'store_kode = :store_kode' );
//		$criteria->addCondition( 'tgl_wo IS NULL' );
//		$criteria->addCondition( 'tgl_finish IS NULL' );
		$criteria->params[':store_kode'] = STOREID;
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = SalesOrder::model()->findAll( $criteria );
		$total = SalesOrder::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionPrintInstruksiKerja() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			/** @var SalesMdp $salesMdp */
			$salesMdp = SalesMdp::model()->findByPk( $_POST['sales_id'] );
			/** @var Store $reseller */
			$reseller              = Store::model()->findByPk( $salesMdp->store_kode );
			$data['ref']           = $salesMdp->doc_ref;
			$data['nama']          = $salesMdp->mobil->customer->nama_customer;
			$data['no_hp']         = $salesMdp->mobil->customer->telp;
			$data['alamat']        = $salesMdp->mobil->customer->alamat;
			$data['merk']          = $salesMdp->mobil->mobilTipe->merk->nama;
			$data['tipe']          = $salesMdp->mobil->mobilTipe->nama;
			$data['nopol']         = $salesMdp->mobil->no_pol;
			$data['no_rangka']     = $salesMdp->mobil->no_rangka;
			$data['produk_1']      = array_key_exists( 0, $salesMdp->salesDetailMdps ) ? $salesMdp->salesDetailMdps[0]->barang->nama_barang : '-';
			$data['produk_2']      = array_key_exists( 1, $salesMdp->salesDetailMdps ) ? $salesMdp->salesDetailMdps[1]->barang->nama_barang : '-';
			$data['produk_3']      = array_key_exists( 2, $salesMdp->salesDetailMdps ) ? $salesMdp->salesDetailMdps[2]->barang->nama_barang : '-';
			$data['city_reseller'] = $reseller->city;
			$data['title']         = ( $salesMdp->edited > 0 ) ? 'REVISI INSTRUKSI  PEKERJAAN (' . $salesMdp->edited . ')' : 'INSTRUKSI  PEKERJAAN';
			$tz                    = 'Asia/Jakarta';
			$timestamp             = time();
			$dt                    = new DateTime( "now", new DateTimeZone( $tz ) ); //first argument "must" be a string
			$dt->setTimestamp( $timestamp ); //adjust the object to correct timestamp
			$data['tgl_print']  = $dt->format( 'd-m-Y' );
			$data['store_kode'] = $salesMdp->store_kode;
			$this->layout       = 'plain';
			$this->render( 'PrintInstruksiKerja', $data );
			$salesMdp->tgl_wo = new CDbExpression( 'NOW()' );
			if ( ! $salesMdp->save() ) {
				throw new Exception( t( 'save.model.fail', 'app',
						array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $salesMdp ) );
			}
		}
	}
	public function actionPrintSertifikatPaintProtection() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			/** @var SalesMdp $salesMdp */
			$salesMdp = SalesMdp::model()->findByPk( $_POST['sales_id'] );
			/** @var Store $reseller */
//			$reseller    = Store::model()->findByPk( $salesMdp->store_kode );
			$data['ref']        = $salesMdp->doc_ref;
			$data['tahun_km']   = $salesMdp->mobil_km;
			$data['warna']      = $salesMdp->mobil->warna;
			$data['no_mesin']   = $salesMdp->mobil->no_mesin;
			$data['merk']       = $salesMdp->mobil->mobilTipe->merk->nama;
			$data['tipe']       = $salesMdp->mobil->mobilTipe->nama;
			$data['no_rangka']  = $salesMdp->mobil->no_rangka;
			$data['store_kode'] = $salesMdp->store_kode;
			$data['p']          = false;
			$data['s']          = false;
			foreach ( $salesMdp->salesDetailMdps as $item ) {
				if ( strpos( $item->barang->nama_barang, 'PAINT' ) !== false ) {
					$data['p'] = true;
				}
				if ( strpos( $item->barang->nama_barang, 'SOUNDPROOF' ) !== false ) {
					$data['s'] = true;
				}
			}
			$this->layout = 'plain';
			$this->render( 'SertifikatPaintProtection', $data );
		}
	}
	public function actionPrintSertifikatRushProtection() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			/** @var SalesMdp $salesMdp */
			$salesMdp           = SalesMdp::model()->findByPk( $_POST['sales_id'] );
			$data['ref']        = $salesMdp->doc_ref;
			$data['tahun_km']   = $salesMdp->mobil_km;
			$data['warna']      = $salesMdp->mobil->warna;
			$data['no_mesin']   = $salesMdp->mobil->no_mesin;
			$data['merk']       = $salesMdp->mobil->mobilTipe->merk->nama;
			$data['tipe']       = $salesMdp->mobil->mobilTipe->nama;
			$data['no_rangka']  = $salesMdp->mobil->no_rangka;
			$data['nc_garansi'] = '';
			$data['nc_nilai']   = '';
			$data['uc_garansi'] = '';
			$data['uc_nilai']   = '';
			$data['top']        = '';
			$data['top_media']  = '';
			$data['store_kode'] = $salesMdp->store_kode;
			foreach ( $salesMdp->salesDetailMdps as $item ) {
				if ( strpos( $item->barang->nama_barang, 'NC RUST' ) !== false ) {
					$data['nc_garansi'] = '5 Tahun';
					$data['nc_nilai']   = number_format( $item->harga );
					$data['top']        = 117;
					$data['top_media']  = 142; //142
					break;
				} elseif ( strpos( $item->barang->nama_barang, 'UC RUST' ) !== false ) {
					$data['uc_garansi'] = '3 Tahun';
					$data['uc_nilai']   = number_format( $item->harga );
					$data['top']        = 147;
					$data['top_media']  = 180;
				}
			}
			$this->layout = 'plain';
			$this->render( 'SertifikatRushProtection', $data );
		}
	}
	public function actionPrintWordOrderUltraGrand() {
		global $ref, $tgl, $nama_customer, $nama_reseller, $merk, $tipe, $no_rangka;
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			/** @var SalesMdp $salesMdp */
			$salesMdp = SalesMdp::model()->findByPk( $_POST['sales_id'] );
			if ( $salesMdp->no_woug == null ) {
				$salesMdp->no_woug  = new CDbExpression(
					"fnc_ref_woug('" . $salesMdp->store_kode . "')" );
				$salesMdp->tgl_woug = new CDbExpression( 'NOW()' );
				if ( ! $salesMdp->save() ) {
					echo CHtml::errorSummary( $salesMdp );
					Yii::app()->end();
				}
			}
			$salesMdp->refresh();
			$salesMdp->woug_persen = SysPrefs::get_val( 'UG_PERSEN' );
			/** @var Store $store */
			$store = Store::model()->findByPk( $salesMdp->store_kode );
			if ( $store == null ) {
				echo "Reseller tidak ditemukan";
				Yii::app()->end();
			}
			$ref           = $salesMdp->no_woug;
			$tgl           = $salesMdp->tgl_woug;
			$nama_customer = $salesMdp->mobil->customer->nama_customer;
			$nama_reseller = $store->nama_store . "(" . $store->store_kode . ")";
			$merk          = $salesMdp->mobil->mobilTipe->merk->nama;
			$tipe          = $salesMdp->mobil->mobilTipe->nama;
			$no_rangka     = $salesMdp->mobil->no_rangka;
			Yii::import( "application.components.tbs_class", true );
			Yii::import( "application.components.tbs_plugin_excel", true );
			Yii::import( "application.components.tbs_plugin_opentbs", true );
			$TBS = new clsTinyButStrong;
			$TBS->PlugIn( TBS_INSTALL, OPENTBS_PLUGIN );
			$TBS->LoadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) . DIRECTORY_SEPARATOR . 'work_order.xlsx', OPENTBS_ALREADY_UTF8 );
			$summary              = [];
			$salesMdp->woug_total = 0;
			/** @var SalesDetailMdp $item */
			foreach ( $salesMdp->salesDetailMdps as $item ) {
				$summary[] = [ 'nama_barang' => $item->barang->nama_barang ];
				/** @var HargaJualMdp $harga_ug */
				$harga_ug = HargaJualMdp::model()->findByAttributes( [
					'barang_id'         => $item->barang_id,
					'mobil_kategori_id' => $salesMdp->mobil->mobilTipe->mobil_kategori_id
				] );
				if ( $harga_ug == null ) {
					echo 'Harga UG untuk produk dan mobil kategori ini tidak ditemukan.';
					Yii::app()->end();
				}
				$tot_qty_harga        = $item->qty * $harga_ug->harga_ug;
				$disc                 = ( $salesMdp->woug_persen / 100 ) * $tot_qty_harga;
				$item->woug           = $tot_qty_harga - $disc;
				$salesMdp->woug_total += $item->woug;
				if ( ! $item->save() ) {
					echo CHtml::errorSummary( $item );
					Yii::app()->end();
				}
			}
			$salesMdp->wo_ppn    = $salesMdp->woug_total * 0.1;
			$salesMdp->wo_hutang = $salesMdp->woug_total + $salesMdp->wo_ppn;
			if ( ! $salesMdp->save() ) {
				echo CHtml::errorSummary( $salesMdp );
				Yii::app()->end();
			}
			$salesMdp->refresh();
			$gl = new GL();
			$this->delete_gl_trans( TAGIHAN_WOUG, $salesMdp->sales_id );
			$ref = $salesMdp->no_woug;
			$gl->add_gl( TAGIHAN_WOUG, $salesMdp->sales_id, $salesMdp->tgl_woug,
				$ref, '52-00-01', "Tagihan WOUG $ref",
				"Beban $ref", $salesMdp->woug_total, 0, '' );
			$gl->add_gl( TAGIHAN_WOUG, $salesMdp->sales_id, $salesMdp->tgl_woug,
				$ref, '11-09-03', "Tagihan WOUG $ref",
				"PPN $ref", $salesMdp->wo_ppn, 0, '' );
			$gl->add_gl( TAGIHAN_WOUG, $salesMdp->sales_id, $salesMdp->tgl_woug,
				$ref, '21-01-01', "Tagihan WOUG $ref",
				"Hutang $ref", - $salesMdp->wo_hutang, 0, '' );
			$gl->validate();
			$TBS->MergeBlock( 'a', $summary );
			$TBS->Show( TBS_EXCEL_DOWNLOAD, "WorkOrderUltraGand" . $ref . ".xlsx" );
		}
	}
}