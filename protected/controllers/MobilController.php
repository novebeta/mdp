<?php
class MobilController extends GxController {
	public function actionCreate() {
		$model = new Mobil;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Mobil'][ $k ] = $v;
			}
			$model->attributes = $_POST['Mobil'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->mobil_id;
			} else {
				$msg    .= " " . CHtml::errorSummary($model);
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Mobil' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Mobil'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['Mobil'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->mobil_id;
			} else {
				$msg    .= " " . CHtml::errorSummary($model);
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->mobil_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'Mobil' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria        = new CDbCriteria();
		$criteria->alias = "m";
		$criteria->join  = "INNER JOIN nscc_customers nc ON m.customer_id = nc.customer_id";
		$criteria->addCondition( "nc.store = :store" );
		$criteria->params[':store'] = STOREID;
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = Mobil::model()->findAll( $criteria );
		$total = Mobil::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}