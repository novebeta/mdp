<?php
class SalesFlazzItemsController extends GxController {
	public function actionCreate() {
		$model = new SalesFlazzItems;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST[ 'SalesFlazzItems' ][ $k ] = $v;
			}
			$model->attributes = $_POST[ 'SalesFlazzItems' ];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->sales_flazz_item_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg ] );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'SalesFlazzItems' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST[ 'SalesFlazzItems' ][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST[ 'SalesFlazzItems' ];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->sales_flazz_item_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( [
					'success' => $status,
					'msg'     => $msg
				] );
				Yii::app()->end();
			} else {
				$this->redirect( [ 'view', 'id' => $model->sales_flazz_item_id ] );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'SalesFlazzItems' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg ] );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
//		if ( isset( $_POST[ 'limit' ] ) ) {
//			$limit = $_POST[ 'limit' ];
//		} else {
//			$limit = 20;
//		}
//		if ( isset( $_POST[ 'start' ] ) ) {
//			$start = $_POST[ 'start' ];
//		} else {
//			$start = 0;
//		}
		$criteria = new CDbCriteria();
		$criteria->addCondition("sales_flazz_id = :sales_flazz_id");
		$criteria->params = array(':sales_flazz_id' => $_POST['sales_flazz_id']);
//		if ( ( isset ( $_POST[ 'mode' ] ) && $_POST[ 'mode' ] == 'grid' ) ||
//		     ( isset( $_POST[ 'limit' ] ) && isset( $_POST[ 'start' ] ) ) ) {
//			$criteria->limit  = $limit;
//			$criteria->offset = $start;
//		}
		$model = SalesFlazzItems::model()->findAll( $criteria );
		$total = SalesFlazzItems::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}