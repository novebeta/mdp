<?php
class SalesFlazzController extends GxController {
	public function actionCreate() {
//		$model = new SalesFlazz;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$is_new               = $_POST[ 'mode' ] == 0;
			$type                 = PENJUALAN;
			$type_no              = null;
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				if ( DateTime::createFromFormat( 'Y-m-d', $_POST[ 'tgl' ] ) === false ||
				     $_POST[ 'tgl' ] === '' || $_POST[ 'tgl' ] === '0000-00-00' || $_POST[ 'tgl' ] == null ) {
					throw new Exception( 'Tgl harus diisi dengan benar.' );
				}
//				$ref    = new Reference();
				$docref = '';
				$model  = $is_new ? new SalesFlazz : $this->loadModel( $_POST[ 'id' ], 'SalesFlazz' );
				if ( $is_new ) {
//					$docref = $ref->get_next_reference( PENJUALAN );
					$docref = new CDbExpression(
						"fnc_ref_invoice_flazz('" . $_POST[ 'tgl' ] . "')" );
				} else {
					$docref  = $model->doc_ref;
					$type_no = $model->sales_flazz_id;
					SalesFlazzItems::model()->deleteAll( 'sales_flazz_id = :sales_flazz_id',
						[ ':sales_flazz_id' => $type_no ] );
					$this->delete_stock_moves( $type, $type_no );
					$this->delete_gl_trans( $type, $type_no );
					$this->delete_bank_trans( $type, $type_no );
				}
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST[ 'SalesFlazz' ][ $k ] = $v;
				}
				$model->attributes = $_POST[ 'SalesFlazz' ];
				$model->doc_ref    = $docref;
				$msg               = "Data gagal disimpan.";
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							[ '{model}' => 'SalesFlazz' ] ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				$docref  = $model->doc_ref;
				$gl      = new GL();
				$is_bank = $model->bank_id != null;
				if ( $is_bank ) {
					/** @var BarangAttr $coa_piutang */
					$coa_piutang = BarangAttr::model()->find(
						"customer_id = :customer_id AND barang_id = :barang_id", [
						':customer_id' => $model->customer_id,
						':barang_id'   => $model->barang_id
					] );
					if ( $coa_piutang == null ) {
						throw new Exception( 'COA Piutang tidak ditemukan.' );
					}
					$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
						$docref, $coa_piutang->account_code, "Penjualan $docref",
						"Penjualan $docref", $model->total, 1, '' );
					$bank = Bank::model()->findByPk( $model->bank_id );
					$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
						$docref, $bank->account_code, "Penjualan $docref",
						"Penjualan $docref", - $model->total, 0, '' );
					$gl->add_bank_trans( PENJUALAN, $model->sales_flazz_id, $model->bank_id,
						$docref, $model->tgl, - $model->total, Yii::app()->user->getId(), '' );
				} else {
					$detils = CJSON::decode( $_POST[ 'detil' ] );
					foreach ( $detils as $detil ) {
						$sales_detail                                   = new SalesFlazzItems;
						$_POST[ 'SalesFlazzItems' ][ 'sales_flazz_id' ] = $model->sales_flazz_id;
						$_POST[ 'SalesFlazzItems' ][ 'barang_id' ]      = $detil[ 'barang_id' ];
						$_POST[ 'SalesFlazzItems' ][ 'total' ]          = get_number( $detil[ 'total' ] );
						$_POST[ 'SalesFlazzItems' ][ 'qty' ]            = get_number( $detil[ 'qty' ] );
						$_POST[ 'SalesFlazzItems' ][ 'harga' ]          = get_number( $detil[ 'harga' ] );
						$_POST[ 'SalesFlazzItems' ][ 'vatrp' ]          = get_number( $detil[ 'vatrp' ] );
						$_POST[ 'SalesFlazzItems' ][ 'bruto' ]          = get_number( $detil[ 'bruto' ] );
						$_POST[ 'SalesFlazzItems' ][ 'harga_beli' ]     = get_number( $detil[ 'harga_beli' ] );
						$sales_detail->attributes                       = $_POST[ 'SalesFlazzItems' ];
						if ( ! $sales_detail->save() ) {
							throw new Exception( t( 'save.model.fail', 'app',
									[ '{model}' => 'Detail Cash' ] ) . CHtml::errorSummary( $sales_detail ) );
						}
						$sales_detail->refresh();
						$grup = Grup::model()->findByPk( $sales_detail->barang->grup_id );
						$note = strtoupper( "PENJUALAN " . $sales_detail->qty . " " . $sales_detail->barang->nama_barang . " - " . $model->customer->nama_customer . ' '. $docref);
						/** @var BarangAttr $coa_piutang */
						$coa_piutang = BarangAttr::model()->find(
							"customer_id = :customer_id AND barang_id = :barang_id", [
							':customer_id' => $model->customer_id,
							':barang_id'   => $sales_detail->barang_id
						] );
						if ( $coa_piutang == null ) {
							throw new Exception( 'COA Piutang tidak ditemukan.' );
						}
						$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
							$docref, $coa_piutang->account_code, $note,
							$note, $sales_detail->total, 0, '' );
						$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
							$docref, $sales_detail->barang->sales, $note,
							$note, - floatval( $sales_detail->bruto ), 0, '' );
						$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
							$docref, '21-02-03', $note,
							$note, - floatval( $sales_detail->vatrp ), 0, '' );
						if ( $grup->kategori->is_have_stock() ) {
							U::add_stock_moves_all(
								null,
								PENJUALAN,
								$model->sales_flazz_id,
								$model->tgl,
								$sales_detail->barang_id,
								- $sales_detail->qty,
								$model->doc_ref,
								0, ''
							);
							/** Persediaan */
							// $note = strtoupper( "PENJUALAN " . $sales_detail->qty . " " . $sales_detail->barang->nama_barang . " - " . $model->customer->nama_customer );
							$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
								$docref, $sales_detail->barang->hpp, $note,
								$note, ( floatval( $sales_detail->harga_beli ) * $sales_detail->qty ), 0, '' );
							$gl->add_gl( PENJUALAN, $model->sales_flazz_id, $model->tgl,
								$docref, $sales_detail->barang->persediaan, $note,
								$note, - ( floatval( $sales_detail->harga_beli ) * $sales_detail->qty ), 0, '' );
						}
					}
				}
				$gl->validate();
//				$ref->save( PENJUALAN, $model->sales_flazz_id, $docref );
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( [
				'success' => $status,
				'msg'     => $msg
			] );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'SalesFlazz' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST[ 'SalesFlazz' ][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST[ 'SalesFlazz' ];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->sales_flazz_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( [
					'success' => $status,
					'msg'     => $msg
				] );
				Yii::app()->end();
			} else {
				$this->redirect( [ 'view', 'id' => $model->sales_flazz_id ] );
			}
		}
	}
	public function actionBayar( $id ) {
		/** @var SalesFlazz $model */
		$model = $this->loadModel( $id, 'SalesFlazz' );
		$id = '';
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				if ( $model->lunas != null ) {
					throw new Exception( "Sudah lunas." );
				}
				$sisaBayar = $model->getKurangBayar();
				$amount =  get_number($_POST[ 'amount' ] );
				if($amount > $sisaBayar){
					throw new Exception( "Maksimal pembayaran ".format_number_report($sisaBayar));
				}
				$salesBayar = new SalesFlazzBayar;
				$salesBayar->sales_flazz_id = $model->sales_flazz_id;
				$salesBayar->doc_ref = new CDbExpression(
					"fnc_ref_sales_flazz_bayar('" . $_POST[ 'tgl' ] . "')" );
				$salesBayar->tgl         = $_POST[ 'tgl' ];
				$salesBayar->bank_id = $_POST[ 'bank_id' ];
				$salesBayar->note = $_POST[ 'note' ];
				$salesBayar->amount = $amount;
				if ( ! $salesBayar->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							[ '{model}' => 'SalesFlazz' ] ) . CHtml::errorSummary( $model ) );
				}
				$sisaBayar = $model->getKurangBayar();
				if($sisaBayar <= 0){
					$model->lunas         = $_POST[ 'tgl' ];
					$model->lunas_bank_id = $_POST[ 'bank_id' ];
					if ( ! $model->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								[ '{model}' => 'SalesFlazz' ] ) . CHtml::errorSummary( $model ) );
					}
				}
				$salesBayar->refresh();
				$id = $salesBayar->sales_flazz_bayar_id;
				$msg                  = "Data berhasil disimpan";
//			$model->attributes = $_POST['Kas'];
				
				$gl   = new GL();
				$ref  = $model->doc_ref;
				$bank = Bank::model()->findByPk( $salesBayar->bank_id );
				$gl->add_gl( BAYAR_SALES, $salesBayar->sales_flazz_bayar_id, $salesBayar->tgl,
					$ref, $bank->account_code, "Pembayaran $ref",
					"Pembayaran $ref", $amount, 0, '' );
				$gl->add_bank_trans( BAYAR_SALES, $salesBayar->sales_flazz_bayar_id, $bank->bank_id,
					$ref, $salesBayar->tgl, $amount, Yii::app()->user->getId(), '' );
				// // throw new Exception( print_r($is_bank,true) );
				// $coa_piutang = BarangAttr::model()->find(
				// 	"customer_id = :customer_id", [
				// 	':customer_id' => $model->customer_id
				// ] );
				// if ( $coa_piutang == null ) {
				// 	throw new Exception( 'COA Piutang tidak ditemukan.' );
				// }
				// $gl->add_gl( BAYAR_SALES, $salesBayar->sales_flazz_bayar_id, $salesBayar->tgl,
				// 	$ref, $coa_piutang->account_code, "Pembayaran $ref",
				// 	"Pembayaran $ref", - $amount, 1, '' );

				//  else {
					/** @var SalesFlazzItems[] $detils */
					$detils = SalesFlazzItems::model()->findall( "sales_flazz_id = :sales_flazz_id", [
						':sales_flazz_id' => $model->sales_flazz_id
					] );
					if(empty($detils) && !empty($model->barang_id)){
						$coa_piutang = BarangAttr::model()->find(
							"customer_id = :customer_id AND barang_id = :barang_id ", [
							':customer_id' => $model->customer_id,
							':barang_id' => $model->barang_id,
						] );
						if ( $coa_piutang == null ) {
							throw new Exception( 'COA Piutang tidak ditemukan.' );
						}
						$gl->add_gl( BAYAR_SALES, $salesBayar->sales_flazz_bayar_id, $salesBayar->tgl,
							$ref, $coa_piutang->account_code, "Pembayaran $ref",
							"Pembayaran $ref", - $amount, 1, '' );
					}else{
						foreach ( $detils as $detil ) {
							$coa_piutang = BarangAttr::model()->find(
								"customer_id = :customer_id AND barang_id = :barang_id", [
								':customer_id' => $model->customer_id,
								':barang_id'   => $detil->barang_id
							] );
							if ( $coa_piutang == null ) {
								throw new Exception( 'COA Piutang tidak ditemukan.' );
							}
							$gl->add_gl( BAYAR_SALES, $salesBayar->sales_flazz_bayar_id, $salesBayar->tgl,
								$ref, $coa_piutang->account_code, "Pembayaran $ref",
								"Pembayaran $ref", - $detil->total, 1, '' );
						}
					}
				// }
				$gl->validate();
				$refs            = new Refs();
				$refs->type_     = BAYAR_SALES;
				$refs->type_no   = $salesBayar->sales_flazz_bayar_id;
				$refs->reference = $ref;
				$refs->save();
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( [
				'success' => $status,
				'id' => $id,
				'msg'     => $msg
			] );
			Yii::app()->end();
		}
	}

	

	public function actionIndex() {
		if ( isset( $_POST[ 'limit' ] ) ) {
			$limit = $_POST[ 'limit' ];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST[ 'start' ] ) ) {
			$start = $_POST[ 'start' ];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition( "bank_id is null" );
		if ( isset( $_POST[ 'tglfrom' ] ) && isset( $_POST[ 'tglto' ] ) ) {
			$criteria->addCondition( "tgl >= :from AND tgl <= :to" );
			$criteria->params[ ':from' ] = $_POST[ 'tglfrom' ];
			$criteria->params[ ':to' ]   = $_POST[ 'tglto' ];
		}
		if ( ( isset ( $_POST[ 'mode' ] ) && $_POST[ 'mode' ] == 'grid' ) ||
		     ( isset( $_POST[ 'limit' ] ) && isset( $_POST[ 'start' ] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = SalesFlazzView::model()->findAll( $criteria );
		$total = SalesFlazzView::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionIndexTopup() {
		if ( isset( $_POST[ 'limit' ] ) ) {
			$limit = $_POST[ 'limit' ];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST[ 'start' ] ) ) {
			$start = $_POST[ 'start' ];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition( "bank_id is not null" );
		if ( isset( $_POST[ 'tglfrom' ] ) && isset( $_POST[ 'tglto' ] ) ) {
			$criteria->addCondition( "tgl >= :from AND tgl <= :to" );
			$criteria->params[ ':from' ] = $_POST[ 'tglfrom' ];
			$criteria->params[ ':to' ]   = $_POST[ 'tglto' ];
		}
		if ( ( isset ( $_POST[ 'mode' ] ) && $_POST[ 'mode' ] == 'grid' ) ||
		     ( isset( $_POST[ 'limit' ] ) && isset( $_POST[ 'start' ] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = SalesFlazz::model()->findAll( $criteria );
		$total = SalesFlazz::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionPrint( $id ) {
		global $ref, $tgl, $pt, $alamat, $city, $no_so, $tgl_so, $qty, $harga,
		       $total, $ppn, $grand_total, $finance, $nama_produk;
//		if ( isset( $_POST ) && ! empty( $_POST ) ) {
		$command = "SELECT nbf.nama_barang,nsfi.qty, nsfi.harga, nsfi.bruto as total 
		FROM nscc_sales_flazz_items nsfi 
		INNER JOIN nscc_barang_flazz nbf ON nsfi.barang_id = nbf.barang_id
		WHERE nsfi.sales_flazz_id = :sales_flazz_id";
		$comm    = Yii::app()->db->createCommand( $command );
		$data    = $comm->queryAll( true, [ ':sales_flazz_id' => $id ] );
		/** @var SalesFlazz $sales */
		$sales       = SalesFlazz::model()->findByPk( $id );
		$ref         = $sales->doc_ref;
		$tgl         = $sales->tgl;
		$nama_produk = ( $sales->barang_id == '39c4fdb0-bcfd-11e9-a4ce-f8da0c234127' ) ? $sales->barang->nama_barang : '';
		$pt          = ( $sales->customer->pt == null ) ? '' : $sales->customer->pt;
		$alamat      = ( $sales->customer->alamat == null ) ? '' : $sales->customer->alamat;
		$city        = ( $sales->customer->city == null ) ? '' : $sales->customer->city;
		$no_so       = ( $sales->note == null ) ? '' : $sales->note;
		$tgl_so      = ( $sales->tgl_jatuh_tempo == null ) ? '' : $sales->tgl_jatuh_tempo;
		$qty         = ( $sales->barang_id == '39c4fdb0-bcfd-11e9-a4ce-f8da0c234127' ) ? $sales->qty : '';
		$harga       = $sales->harga;
		$total       = $sales->bruto;
		$ppn         = $sales->vatrp;
		$grand_total = $sales->total;
		$finance     = SysPrefs::get_val( 'INVOCE_NAME' );
		Yii::import( "application.components.tbs_class", true );
		Yii::import( "application.components.tbs_plugin_excel", true );
		Yii::import( "application.components.tbs_plugin_opentbs", true );
		$TBS = new clsTinyButStrong;
		$TBS->PlugIn( TBS_INSTALL, OPENTBS_PLUGIN );
		$nama_template = 'invoice_flazz.xlsx';
		if ( $sales->barang_id !== '39c4fdb0-bcfd-11e9-a4ce-f8da0c234127' ) {
			$nama_template = 'invoice_flazz_card.xlsx';
		}
		$TBS->LoadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) . DIRECTORY_SEPARATOR . $nama_template, OPENTBS_ALREADY_UTF8 );
		if ( $sales->barang_id !== '39c4fdb0-bcfd-11e9-a4ce-f8da0c234127' ) {
			$TBS->MergeBlock( 'a', $data );
		}
		$TBS->Show( TBS_EXCEL_DOWNLOAD, "InvoiceSalesFlazz" . $ref . ".xlsx" );
//		}
	}
}