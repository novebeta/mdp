<?php
class PaymentJournalDetailController extends GxController
{
	public function actionCreate()
	{
		$model = new PaymentJournalDetail;
		if (!Yii::app()->request->isAjaxRequest)
			return;
		if (isset($_POST) && !empty($_POST)) {
			foreach ($_POST as $k => $v) {
				if (is_angka($v)) $v = get_number($v);
				$_POST['PaymentJournalDetail'][$k] = $v;
			}
			$model->attributes = $_POST['PaymentJournalDetail'];
			$msg = "Data gagal disimpan.";
			if ($model->save()) {
				$status = true;
				$msg = "Data berhasil di simpan dengan id " . $model->payment_journal_detail_id;
			} else {
				$msg .= " " . implode(", ", $model->getErrors());
				$status = false;
			}
			echo CJSON::encode(array(
				'success' => $status,
				'msg' => $msg));
			Yii::app()->end();
		}
	}
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id, 'PaymentJournalDetail');
		if (isset($_POST) && !empty($_POST)) {
			foreach ($_POST as $k => $v) {
				if (is_angka($v)) $v = get_number($v);
				$_POST['PaymentJournalDetail'][$k] = $v;
			}
			$msg = "Data gagal disimpan";
			$model->attributes = $_POST['PaymentJournalDetail'];
			if ($model->save()) {
				$status = true;
				$msg = "Data berhasil di simpan dengan id " . $model->payment_journal_detail_id;
			} else {
				$msg .= " " . implode(", ", $model->getErrors());
				$status = false;
			}
			if (Yii::app()->request->isAjaxRequest) {
				echo CJSON::encode(array(
					'success' => $status,
					'msg' => $msg
				));
				Yii::app()->end();
			} else {
				$this->redirect(array('view', 'id' => $model->payment_journal_detail_id));
			}
		}
	}
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$msg = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel($id, 'PaymentJournalDetail')->delete();
			} catch (Exception $ex) {
				$status = false;
				$msg = $ex;
			}
			echo CJSON::encode(array(
				'success' => $status,
				'msg' => $msg));
			Yii::app()->end();
		} else
			throw new CHttpException(400,
				Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
	}

	public function actionIndex() {
		$criteria = new CDbCriteria();
		if(isset($_POST['payment_journal_id'])){
			$criteria->addCondition('payment_journal_id = :payment_journal_id');
			$param[':payment_journal_id'] = $_POST['payment_journal_id'];
		}
		$criteria->params = $param;
		$model = PaymentJournalDetail::model()->findAll($criteria);
		$total = PaymentJournalDetail::model()->count($criteria);

		$this->renderJson($model, $total);
	}
}