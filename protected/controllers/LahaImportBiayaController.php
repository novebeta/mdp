<?php
class LahaImportBiayaController extends GxController
{
    public function actionCreate()
    {
        $model = new LahaImportBiaya;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['LahaImportBiaya'][$k] = $v;
            }
            $model->attributes = $_POST['LahaImportBiaya'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->laha_import_biaya_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'LahaImportBiaya');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['LahaImportBiaya'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['LahaImportBiaya'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->laha_import_biaya_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->laha_import_biaya_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'LahaImportBiaya')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
        $criteria = new CDbCriteria();
//        $criteria->select = "nlib.laha_import_biaya_id,nlib.amount,nlib.laha_import_id,nlib.account_code,
//            nlib.note_,nlib.store AS store_";
//        $criteria->alias = 'nlib';
        $params = [];
        if (isset($_POST['laha_import_id'])) {
            $criteria->addCondition('laha_import_id = :laha_import_id');
            $params[':laha_import_id'] = $_POST['laha_import_id'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->params = $params;
        $model = LahaImportBiaya::model()->findAll($criteria);
        $total = LahaImportBiaya::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}