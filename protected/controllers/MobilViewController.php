<?php
class MobilViewController extends GxController {
	public function actionCreate() {
		$model = new MobilView;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['MobilView'][ $k ] = $v;
			}
			$model->attributes = $_POST['MobilView'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->mobil_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'MobilView' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['MobilView'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['MobilView'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->mobil_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->mobil_id ) );
			}
		}
	}
	public function actionDeskripsi( $id ) {
		$hasil  = "";
		$status = false;
		/** @var Mobil $mobil */
		$mobil = $this->loadModel( $id, 'Mobil' );
		if ( $mobil != null ) {
			$tipe   = $mobil->mobilTipe->nama;
			$merk   = $mobil->mobilTipe->merk->nama;
			$rangka = $mobil->no_rangka;
			$nama   = $mobil->customer->nama_customer;
			$phone  = $mobil->customer->telp;
			$hasil  = "<pre>
Merk 			: $merk
Tipe 			: $tipe
No. Rangka 		: $rangka
Nama 			: $nama
Phone 			: $phone
</pre>";
			$status = true;
		}
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $hasil
		) );
		Yii::app()->end();
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$param    = '';
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( isset( $_POST['mobil_id'] ) ) {
			unset( $_POST['query'] );
			$criteria->addCondition( 'mobil_id = :mobil_id' );
			$param[':mobil_id'] = $_POST['mobil_id'];
		}
		if ( isset( $_POST['query'] ) ) {
			$criteria->addCondition( 'no_pol like :no_pol', 'OR' );
			$param[':no_pol'] = '%' . $_POST['query'] . '%';
		}
		$criteria->params = $param;
		$model            = MobilView::model()->findAll( $criteria );
		$total            = MobilView::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}