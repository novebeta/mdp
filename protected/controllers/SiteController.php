<?php
Yii::import( 'application.components.U' );
class SiteController extends GxController {
//    public function accessRules() {
//        return array(
//            array('allow',
//                'users' => array('*'),
//                'actions' => array('login'),
//            )           
//        );
//    }
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => array( 'class' => 'CViewAction', ),
		);
	}
	public function actionCreateTagihan() {
		if ( isset( $_POST['tgl'] ) ) {
//			echo $_POST['tgl'];
//			die();
			U::createTagihan();
		} else {
			echo '
			<!DOCTYPE html>
			<html>			<body>			
			<h1>Buat Tagihan</h1>			
			<form method="post">
			  Periode: <input type="date" name="tgl">
			  <input type="submit">
			</form>			
			<p><strong>Note:</strong> this is not supported in Safari or Internet Explorer 11 (or earlier).</p>			
			</body>			</html>
			';
		}
	}
	public function actionPreviewClosing() {
		$this->layout = 'plain';
		if ( !empty($_POST) && isset( $_POST['year'] ) ) {

			echo '
			<!DOCTYPE html>
			<html><head><title>';
			switch($_POST['mode']){
				case '0':
					echo 'Tutup Buku / Closing</title></head><body>';
					U::closing($_POST['year'],$_POST['dry']);
					break;
				case '1':
					echo 'unClosing</title></head><body>';
					U::unclosing($_POST['year'],$_POST['dry']);
					break;
				case '2':
					echo 'Pindah Berjalan ke Laba Ditahan</title></head><body>';
					U::transfer($_POST['year'],$_POST['dry']);
					break;
			}
			echo "<br><br><a href='".Yii::app()->createUrl("site/previewclosing/")."'>Kembali</a></body></html>";
		}else{
			$comm                    = Yii::app()->db->createCommand( 'SELECT  DISTINCT(DATE_FORMAT(tran_date, "%Y"))
			FROM nscc_gl_trans ngt WHERE ngt.type = 500  AND ngt.visible  = 1;');
			$tahunClosing                 = $comm->queryColumn();
			$this->render('previewclosing',['tahunClosing'=>$tahunClosing]);
		}
	}
	public function actionReDocRef() {
		$criteria = new CDbCriteria();
		$criteria->addCondition( 'type_ = 1' );
		$criteria->order      = 'tdate';
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		/** @var Salestrans[] $sales_all */
		$sales_all = Salestrans::model()->findAll( $criteria );
		try {
			foreach ( $sales_all as $sales ) {
				$ref            = new Reference();
				$doc_ref        = $ref->get_next_reference( PENJUALAN );
				$sales->doc_ref = $doc_ref;
				$sales->saveAttributes( [ 'doc_ref' => $doc_ref ] );
				BankTrans::model()->updateAll(
					[ 'ref' => $doc_ref ],
					'type_ = 1 AND trans_no = :trans_no',
					[ ':trans_no' => $sales->salestrans_id ]
				);
				$ref->save( PENJUALAN, $sales->salestrans_id, $doc_ref );
			}
			$transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			var_dump( $ex );
		}
		app()->db->autoCommit = true;
		echo $doc_ref;
		echo $ref->get_next_reference( PENJUALAN );
	}
	public function actionDateDiff() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$date1 = $_POST['dari'];
			$date2 = $_POST['sampai'];
			$diff  = abs( strtotime( $date2 ) - strtotime( $date1 ) );
			$years = floor( $diff / ( 365 * 60 * 60 * 24 ) );
			echo CJSON::encode(
				array(
					'status' => true,
					'msg'    => $years
				) );
			Yii::app()->end();
		}
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$this->layout = 'main';
//		if ( ! isset( $_POST['app'] ) ) {
//			$user = Users::model()->findByPk( user()->getId() );
//			$tree = new MenuTree( $user->security_roles_id );
//			if ( $tree->getState( 9996 ) ) {
//				$this->redirect( url( 'site/counter' ) );
//			} elseif ( $tree->getState( 9997 ) ) {
//				$this->redirect( url( 'site/dokter' ) );
//			} elseif ( $tree->getState( 9998 ) ) {
//				$this->redirect( url( 'site/perawatan' ) );
//			} elseif ( $tree->getState( 9999 ) ) {
//				$this->render( 'sales', array( 'state' => 0, 'username' => $user->name ) );
//			} else {
//				$this->render( 'index' );
//			}
//		} else {
		$this->render( 'index' );
//		}
	}
	public function actionMarket() {
		$this->layout = 'market';
//        if (!isset($_POST['app'])) {
//            $user = Users::model()->findByPk(user()->getId());
//            $tree = new MenuTree($user->security_roles_id);
//            $state = $tree->getState();
//            if ($state > 0) {
//                $this->render('sales', array('state' => $state, 'username' => $user->name));
//            }
//        }
		$this->render( 'market' );
	}
	public function actionDokter() {
		$this->layout = 'dokter';
//        if (!isset($_POST['app'])) {
//            $user = Users::model()->findByPk(user()->getId());
//            $tree = new MenuTree($user->security_roles_id);
//            $state = $tree->getState();
//            if ($state > 0) {
//                $this->render('sales', array('state' => $state, 'username' => $user->name));
//            }
//        }
		$this->render( 'dokter' );
	}
	public function actionServerAntrian() {
		$this->layout = 'dokter';
		$this->render( 'serverantrian' );
	}
	public function actionClientantrian( $id = '0' ) {
		if ( $id != '0' ) {
			$modelnew = new AishaAntrian;
			$modelold = new AishaAntrian;
			$criteria = new CDbCriteria;
			$param    = array();
			$tanggal  = date( "Y-m-d" );
			$criteria->addCondition( "DATE(tanggal) = :tanggal" );
			$param[':tanggal']       = $tanggal;
			$criteria->params        = $param;
			$criteria->select        = 'max(nomor_antrian) AS nomor_antrian';
			$row                     = $modelold->model()->find( $criteria );
			$lastnoan                = $row['nomor_antrian'];
			$modelnew->nomor_antrian = $lastnoan + 1;
			$modelnew->counter       = $id;
			$modelnew->counter_asal  = $id;
			if ( ! $modelnew->save() ) {
				throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'AishaAntrian' ) ) . CHtml::errorSummary( $modelnew ) );
			}
			$antrianp['header0'] = SysPrefs::get_val( 'receipt_header0' );
			$antrianp['header1'] = SysPrefs::get_val( 'receipt_header1' );
			$antrianp['header2'] = SysPrefs::get_val( 'receipt_header2' );
			$antrianp['header3'] = SysPrefs::get_val( 'receipt_header3' );
			$antrianp['header4'] = SysPrefs::get_val( 'receipt_header4' );
			$antrianp['header5'] = SysPrefs::get_val( 'receipt_header5' );
			$antrianp['no']      = $modelnew->nomor_antrian;
			$antrianp['counter'] = $modelnew->counter;
			$antrianp['footer0'] = SysPrefs::get_val( 'receipt_footer0' );
			$antrianp['footer1'] = SysPrefs::get_val( 'receipt_footer1' );
			$antrianp['footer2'] = SysPrefs::get_val( 'receipt_footer2' );
			$antrianp['footer3'] = SysPrefs::get_val( 'receipt_footer3' );
			$antrianp['footer4'] = SysPrefs::get_val( 'receipt_footer4' );
			$antrianp['footer5'] = SysPrefs::get_val( 'receipt_footer5' );
			$prt                 = new PrintAntrianc();
			echo CJSON::encode( array(
				'success' => $antrianp['no'] != null,
				'msg'     => $prt->buildTxtAntrianc( $antrianp )
			) );
			Yii::app()->end();
		} else {
			$this->layout = 'dokter';
			$this->render( 'clientantrian' );
		}
	}
	public function actionCounter() {
		$this->layout = 'main';
//        if (!isset($_POST['app'])) {
//            $user = Users::model()->findByPk(user()->getId());
//            $tree = new MenuTree($user->security_roles_id);
//            $state = $tree->getState();
//            if ($state > 0) {
//                $this->render('sales', array('state' => $state, 'username' => $user->name));
//            }
//        }
		$this->render( 'counter' );
	}
	public function actionCc() {
		var_dump( opcache_reset() );
	}
	public function actionPerawatan() {
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = 'main';
		$this->render( 'perawatan' );
	}
	public function actionPosMode() {
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = 'main';
		$this->render( 'sales' );
	}
	public function actionGetDateTime() {
		if ( Yii::app()->request->isAjaxRequest ) {
			echo CJSON::encode( array(
				'success'  => true,
				'datetime' => date( 'Y-m-d H:i:s' )
			) );
			Yii::app()->end();
		}
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ( $error = Yii::app()->errorHandler->error ) {
			if ( Yii::app()->request->isAjaxRequest ) {
//                if (is_array($error)) {
//                    $error = implode("\n", $error);
//                }
				throw new Exception( $error['message'] );
//                echo CJSON::encode(array(
//                    'success' => false,
//                    'msg' => $error
//                ));
//                Yii::app()->end();
			} else {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => $error
				) );
				//$this->render( 'error', $error );
			}
		}
	}
	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if ( isset( $_POST['ContactForm'] ) ) {
			$model->attributes = $_POST['ContactForm'];
			if ( $model->validate() ) {
				$name    = '=?UTF-8?B?' . base64_encode( $model->name ) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode( $model->subject ) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" .
				           "MIME-Version: 1.0\r\n" . "Content-type: text/plain; charset=UTF-8";
				mail( Yii::app()->params['adminEmail'], $subject, $model->body, $headers );
				Yii::app()->user->setFlash( 'contact',
					'Thank you for contacting us. We will respond to you as soon as possible.' );
				$this->refresh();
			}
		}
		$this->render( 'contact', array( 'model' => $model ) );
	}
	public function actionLogout() {
		header( 'X-LiteSpeed-Cache-Control: no-cache' );
		Yii::app()->user->logout();
		Yii::app()->request->cookies->clear();
//		$this->redirect( 'login' );
//		sleep( 10 );
		$this->redirect( Yii::app()->homeUrl );
	}
	public function actionLogin() {
		header( 'X-LiteSpeed-Cache-Control: no-cache' );
//		 else {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			if ( ! Yii::app()->user->isGuest ) {
				$this->redirect( Yii::app()->homeUrl );
				Yii::app()->end();
			}
//			$url_ = reset( $_SESSION );
//			if ( $url_ == bu( 'login' ) ) {
//				$url_ = bu();
//			}
			$this->layout = 'login';
			$this->render( 'login', array( 'url_' => Yii::app()->homeUrl ) );
		} else {
			$model         = new LoginForm;
			$loginUsername = isset( $_POST["loginUsername"] ) ? $_POST["loginUsername"]
				: "";
			$loginPassword = isset( $_POST["loginPassword"] ) ? $_POST["loginPassword"]
				: "";
			if ( $loginUsername != "" ) {
				//$model->attributes = $_POST['LoginForm'];
				// validate user input and redirect to the previous page if valid
				$model->username = $loginUsername;
				$model->password = $loginPassword;
//				$model->browser  = $browser_id;
				if ( $model->validate() && $model->login() ) {
//						$browser = Browser::saveInfo( $_POST['ip_local'], $_POST['ip_public'], $_POST['browserdata'], $_POST['fingerprint'], $_POST['useragent'],
//							$_POST['browser'], $_POST['browserversion'], $_POST['browsermajorversion'], $_POST['ie'], $_POST['chrome'], $_POST['firefox'], $_POST['safari'],
//							$_POST['opera'], $_POST['engine'], $_POST['engineversion'], $_POST['os'], $_POST['osversion'], $_POST['windows'], $_POST['mac_os'], $_POST['linux'], $_POST['ubuntu'],
//							$_POST['solaris'], $_POST['device'], $_POST['devicetype'], $_POST['devicevendor'], $_POST['cpu'], $_POST['mobile'], $_POST['mobilemajor'], $_POST['mobileandroid'],
//							$_POST['mobileopera'], $_POST['mobilewindows'], $_POST['mobileblackberry'], $_POST['mobileios'], $_POST['iphone'], $_POST['ipad'], $_POST['ipod'], $_POST['screenprint'],
//							$_POST['colordepth'], $_POST['currentresolution'], $_POST['availableresolution'], $_POST['devicexdpi'], $_POST['deviceydpi'], $_POST['plugins'], $_POST['java'],
//							$_POST['javaversion'], $_POST['flash'], $_POST['flashversion'], $_POST['silverlight'], $_POST['silverlightversion'], $_POST['mimetypes'], $_POST['ismimetypes'],
//							$_POST['font'], $_POST['fonts'], $_POST['localstorage'], $_POST['sessionstorage'], $_POST['cookie'], $_POST['timezone'], $_POST['language'], $_POST['systemlanguage'], $_POST['canvas'] );
//						Yii::app()->user->setBrowser( $browser->browser_id );
//						LogTrans::saveLog( '', '', 'LOGIN BERHASIL', '' );
//					while ( Yii::app()->user->isGuest ) {
//						sleep( 10 );
//					}
//					sleep( 10 );
//					$this->redirect( Yii::app()->homeUrl );
					echo CJSON::encode( array(
						'success' => true,
						'msg'     => 'OK'
					) );
				} else {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => 'Login failed. Try again.',
						'errors'  => [
							'reason' => 'Login failed. Try again.'
						]
					) );
				}
			} else {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => 'Login failed. User Name is empty.',
					'errors'  => [
						'reason' => 'Login failed. User Name is empty.'
					]
				) );
			}
		}
//		}
	}
	public function actionLogged() {
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => ! Yii::app()->user->isGuest ? 'YES' : 'NO'
		) );
	}
	public function actionLoginOverride() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->redirect( '/' );
		} else {
			$count = Users::get_override( $_POST["loginUsername"], $_POST["loginPassword"] );
			if ( $count ) {
				echo CJSON::encode( array(
					'success' => true,
					'msg'     => $count
				) );
			} else {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => "You don't have permission."
				) );
			}
		}
	}
	public function actionGenerate() {
		$templatePath = './css/silk_v013/icons';
		$files        = scandir( $templatePath );
		$txt          = "";
		foreach ( $files as $file ) {
			if ( is_file( $templatePath . '/' . $file ) ) {
				$basename = explode( ".", $file );
				$name     = $basename[0];
				$txt      .= ".silk13-$name { background-image: url(icons/$file) !important; background-repeat: no-repeat; }\n";
			}
		}
		$myFile = "silk013.css";
		$fh = fopen( $myFile, 'w' ) or die( "can't open file" );
		fwrite( $fh, $txt );
		fclose( $fh );
	}
	public function actionTree() {
		$user = Users::model()->findByPk( user()->getId() );
		$menu = new MenuTree( $user->security_roles_id );
		$data = $menu->get_menu();
		Yii::app()->end( $data );
	}
	public function actionLock() {
		$x = @fopen( SALES_LOCK, "w" );
		if ( ! flock( $x, LOCK_EX | LOCK_NB ) ) {
			echo CJSON::encode( array(
				'success' => false,
				'id'      => '',
				'msg'     => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
			) );
			Yii::app()->end();
		}
		sleep( 10 );
		fflush( $x );            // flush output before releasing the lock
		flock( $x, LOCK_UN );
		fclose( $x );
	}
	public function actionUnLock() {
		$x = @fopen( SALES_LOCK, "w" );
		if ( ! flock( $x, LOCK_EX | LOCK_NB ) ) {
			echo CJSON::encode( array(
				'success' => false,
				'id'      => '',
				'msg'     => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
			) );
			Yii::app()->end();
		}
		fflush( $x );            // flush output before releasing the lock
		flock( $x, LOCK_UN );
		fclose( $x );
	}
	public function actionBackupAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		ini_set( 'max_execution_time', 600 );
		$conn = Yii::app()->db;
		$user = $conn->username;
		$pass = $conn->password;
		if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
			list( $all, $host, $db, $port ) = $result;
		}
		$dir_backup = DIR_BACKUP;
		$files      = scandir( $dir_backup );
		foreach ( $files as $file ) {
			if ( is_file( "$dir_backup\\$file" ) ) {
				unlink( "$dir_backup\\$file" );
			}
		}
		$filename    = $db . date( "Y-m-d-H-i-s" ) . '.pos';
		$backup_file = $dir_backup . $filename;
		$mysqldump   = MYSQLDUMP;
		$command     = "$mysqldump --opt -h $host -u $user --password=$pass -P $port --routines " .
		               "$db > $backup_file";
		system( $command );
		$size = filesize( $backup_file );
		if ( $size > 0 ) {
			$zip = new ZipArchive();
			if ( $zip->open( "$backup_file.zip", ZIPARCHIVE::CREATE ) ) {
				$zip->addFile( $backup_file, $filename );
				$zip->close();
				$finfo    = finfo_open( FILEINFO_MIME_TYPE );
				$mimeType = finfo_file( $finfo, "$backup_file.zip" );
				$size     = filesize( "$backup_file.zip" );
				$name     = basename( "$backup_file.zip" );
				if ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' ) {
					// cache settings for IE6 on HTTPS
					header( 'Cache-Control: max-age=120' );
					header( 'Pragma: public' );
				} else {
					header( 'Cache-Control: private, max-age=120, must-revalidate' );
					header( "Pragma: no-cache" );
				}
				header( "Expires: Sat, 26 Jul 1997 05:00:00 GMT" ); // long ago
				header( "Content-Type: $mimeType" );
				header( 'Content-Disposition: attachment; filename="' . $name . '";' );
				header( "Accept-Ranges: bytes" );
				header( 'Content-Length: ' . filesize( "$backup_file.zip" ) );
				print readfile( "$backup_file.zip" );
				exit;
			}
//            $bu = $dir_backup . basename("$backup_file.zip");
//            header("Location: $bu");
		}
		ini_set( 'max_execution_time', 30 );
	}
	public function actionRestoreAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		$conn = Yii::app()->db;
		$user = $conn->username;
		$pass = $conn->password;
		if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
			list( $all, $host, $db, $port ) = $result;
		}
		$dir_backup = dirname( Yii::app()->request->scriptFile ) . "\\backup\\";
		if ( isset( $_FILES["filename"] ) ) { // it is recommended to check file type and size here
			if ( $_FILES["filename"]["error"] > 0 ) {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => $_FILES["file"]["error"]
				) );
			} else {
				$backup_file = $dir_backup . $_FILES["filename"]["name"];
				move_uploaded_file( $_FILES["filename"]["tmp_name"], $backup_file );
				$mysql   = dirname( Yii::app()->request->scriptFile ) . "\\mysql";
				$gzip    = dirname( Yii::app()->request->scriptFile ) . "\\gzip";
				$command = "$gzip -d $backup_file";
				system( $command );
				$backup_file = substr( $backup_file, 0, - 3 );
				if ( ! file_exists( $backup_file ) ) {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => "Failed restore file " . $_FILES["file"]["name"]
					) );
				} else {
					Yii::app()->db->createCommand( "DROP DATABASE $db" )
					              ->execute();
					Yii::app()->db->createCommand( "CREATE DATABASE IF NOT EXISTS $db" )
					              ->execute();
					$command = "$mysql -h $host -u $user --password=$pass -P $port " .
					           "$db < $backup_file";
					system( $command );
					echo CJSON::encode( array(
						'success' => true,
						'msg'     => "Succefully restore file " . $_FILES["file"]["name"]
					) );
				}
				Yii::app()->end();
			}
		}
	}
	public function actionDeleteTransAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		Yii::app()->db->createCommand( "
        /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
        TRUNCATE TABLE nscc_bank_trans;
        TRUNCATE TABLE nscc_beauty_services;
        TRUNCATE TABLE nscc_kas_detail;
        TRUNCATE TABLE nscc_kas;
        TRUNCATE TABLE nscc_salestrans_details;
        TRUNCATE TABLE nscc_salestrans;
        TRUNCATE TABLE nscc_stock_moves;
        TRUNCATE TABLE nscc_tender_details;
        TRUNCATE TABLE nscc_tender;
        TRUNCATE TABLE nscc_transfer_item_details;
        TRUNCATE TABLE nscc_transfer_item;
        TRUNCATE TABLE nscc_gl_trans;
        TRUNCATE TABLE nscc_comments;
        TRUNCATE TABLE nscc_payment;
        TRUNCATE TABLE nscc_refs;
        TRUNCATE TABLE nscc_sync;
        TRUNCATE TABLE nscc_upload_log;
        TRUNCATE TABLE nscc_pelunasan_utang_detil;
        TRUNCATE TABLE nscc_pelunasan_utang;
        /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
        " )->execute();
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => "Succefully delete all transaction "
		) );
	}
	public function actionSync() {
		session_write_close();
		$stat = SysPrefs::model()->find( 'name_ = :name AND store =:store',
			array( ':name' => 'sync_active', ':store' => STOREID ) );
		if ( $stat->value_ != "0" ) {
			$date    = strtotime( $stat->value_ );
			$date2   = time();
			$subTime = $date2 - $date;
			$h       = $subTime / ( 60 * 60 );
			if ( $h > 1 ) {
				$stat->value_ = '0';
				$stat->save();
			}
			$msg = 'other client already sync. Diff time ' . $h;
		} else {
			$stat->value_ = date( 'Y-m-d H:i:s' );
			$stat->save();
			$s            = new SyncData();
			$msg          = $s->sync();
			$stat->value_ = '0';
			$stat->save();
		}
		$report = json_encode( array(
			'type' => 'event',
			'name' => 'message',
			'time' => date( 'g:i:s a' ),
			'data' => $msg
		),
			JSON_PRETTY_PRINT );
		echo $report;
	}
	public function actionTestEncrypt() {
		$global   = array(
			'SecurityRoles',
			'Gol',
			'TransTipe',
			'StatusCust',
			'ChartMaster',
			'Supplier',
			'Kategori',
			'Negara',
			'Provinsi',
			'Kota',
			'Kecamatan',
			'Store'
		);
		$criteria = new CDbCriteria;
		$criteria->addCondition( "up = 0" );
		$upload  = array();
		$content = array();
		foreach ( $global as $modelName ) {
			$model  = new $modelName();
			$tables = $model::model()->findAll( $criteria );
			if ( $tables != null ) {
				$upload[ $modelName ]  = $tables;
				$content[ $modelName ] = number_format( count( $tables ) ) . ' record';
			}
		}
		$to = yiiparam( 'Username' );
		echo mailsend( $to, $to, 'GLOBAL-' . sha1( STOREID . "^" . date( 'Y-m-d H:i:s' ) ),
			json_encode( array(
				'from'    => STOREID,
				'date'    => date( 'Y-m-d H:i:s' ),
				'content' => array( $content )
			), JSON_PRETTY_PRINT ),
			bzcompress( Encrypt( CJSON::encode( $upload ) ), 9 ) );
	}
	public function actionCheckEmail() {
		$username      = 'nscc.sync@gmail.com';
		$password      = 'zaq!@#$%';
		$imapmainbox   = "TSBK01";
		$messagestatus = "ALL";
		$imapaddress   = "{imap.gmail.com:993/imap/ssl}";
		$hostname      = $imapaddress . $imapmainbox;
		$connection = imap_open( $hostname, $username,
			$password ) or die( 'Cannot connect to Gmail: ' . imap_last_error() );
		$emails      = imap_search( $connection, $messagestatus );
		$totalemails = imap_num_msg( $connection );
		echo "Total Emails: " . $totalemails . "<br>";
		if ( $emails ) {
			//sort emails by newest first
			//rsort($emails);
			//loop through every email int he inbox
			foreach ( $emails as $email_number ) {
				//grab the overview and message
				$header = imap_fetch_overview( $connection, $email_number, 0 );
				//Because attachments can be problematic this logic will default to skipping the attachments
				$structure   = imap_fetchstructure( $connection, $email_number );
				$attachments = array();
				if ( isset( $structure->parts ) && count( $structure->parts ) ) {
					for ( $i = 0; $i < count( $structure->parts ); $i ++ ) {
						$attachments[ $i ] = array(
							'is_attachment' => false,
							'filename'      => '',
							'name'          => '',
							'attachment'    => ''
						);
						if ( $structure->parts[ $i ]->ifdparameters ) {
							foreach ( $structure->parts[ $i ]->dparameters as $object ) {
								if ( strtolower( $object->attribute ) == 'filename' ) {
									$attachments[ $i ]['is_attachment'] = true;
									$attachments[ $i ]['filename']      = $object->value;
								}
							}
						}
						if ( $structure->parts[ $i ]->ifparameters ) {
							foreach ( $structure->parts[ $i ]->parameters as $object ) {
								if ( strtolower( $object->attribute ) == 'name' ) {
									$attachments[ $i ]['is_attachment'] = true;
									$attachments[ $i ]['name']          = $object->value;
								}
							}
						}
						if ( $attachments[ $i ]['is_attachment'] ) {
							$attachments[ $i ]['attachment'] = imap_fetchbody( $connection, $email_number, $i + 1 );
							if ( $structure->parts[ $i ]->encoding == 3 ) { // 3 = BASE64
								$attachments[ $i ]['attachment'] = base64_decode( $attachments[ $i ]['attachment'] );
							} elseif ( $structure->parts[ $i ]->encoding == 4 ) { // 4 = QUOTED-PRINTABLE
								$attachments[ $i ]['attachment'] = quoted_printable_decode( $attachments[ $i ]['attachment'] );
							}
						}
					} // for($i = 0; $i < count($structure->parts); $i++)
				} // if(isset($structure->parts) && count($structure->parts))
				$status  = ( $header[0]->seen ? 'read' : 'unread' );
				$subject = $header[0]->subject;
				$from    = $header[0]->from;
				$date    = $header[0]->date;
				echo "status: " . $status . "<br>";
				echo "subject: " . $subject . "<br>";
				echo "from: " . $from . "<br>";
				echo "date: " . $date . "<br>";
				$message = imap_fetchbody( $connection, $email_number, 1.1 );
				if ( $message == "" ) { // no attachments is the usual cause of this
					$message = imap_fetchbody( $connection, $email_number, 1 );
				}
				echo "body: " . $message . "<br>";
				if ( count( $attachments ) != 0 ) {
					foreach ( $attachments as $at ) {
						if ( $at[ is_attachment ] == 1 ) {
//                            file_put_contents($at[filename], $at[attachment]);
							echo "Attachment: " . Decrypt( bzdecompress( $at[ attachment ] ) ) . "<br>";
						}
					}
				}
				echo "<hr><br>";
			}
		}
		imap_close( $connection );
	}
	public function actionWord() {
		Yii::import( "application.components.tbs_class", true );
		Yii::import( "application.components.tbs_plugin_opentbs", true );
		// Initialize the TBS instance
		$TBS = new clsTinyButStrong; // new instance of TBS
		$TBS->Plugin( TBS_INSTALL, OPENTBS_PLUGIN ); // load the OpenTBS plugin
		// Retrieve the user name to display
		global $doc_ref;
		global $x;
		$doc_ref  = 'test_ref';
		$x        = '/home/nove/DATA_D/xampp/htdocs/mdb/KJS.jpeg';
		$template = 'instruksi_pekerjaan.docx';
		$TBS->LoadTemplate( $template, OPENTBS_ALREADY_UTF8 );
		$output_file_name = str_replace( '.', '_' . date( 'Y-m-d' ) . '.', $template );
//		$prms = array('unique' => true);
//		$TBS->Plugin(OPENTBS_CHANGE_PICTURE, 'Table 1', $x, $prms);
		$files = $TBS->Plugin( OPENTBS_GET_OPENED_FILES );
		var_dump( $files );
//		$TBS->Show( OPENTBS_DOWNLOAD, $output_file_name );
//		echo realpath( YiiBase::getPathOfAlias( "application.extensions.vendor.dompdf.dompdf" ) );
//		echo realpath( __DIR__ .'/../extensions/');
//		Yii::app()->end();
//		Yii::import( "application.extensions.vendor.phpoffice.phpword.bootstrap", true );
//		require_once 'bootstrap.php';
// Creating the new document...
//		$phpWord = new \PhpOffice\PhpWord\PhpWord();
//		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor( 'instruksi_pekerjaan.docx' );
//		$templateProcessor->setValue( 'nama', 'myvar' );
//		$templateProcessor->saveAs( 'helloWorld.docx' );
//
//		$phpWord = \PhpOffice\PhpWord\IOFactory::load('helloWorld.docx'); // Read the temp file
//		$documentProtection = $phpWord->getSettings()->getDocumentProtection();
//		$documentProtection->setEditing(\PhpOffice\PhpWord\SimpleType\DocProtect::READ_ONLY);
//		$documentProtection->setPassword('myPassword');
//		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
//		$xmlWriter->save('result.docx');
//		$domPdfPath        = YiiBase::getPathOfAlias( "application.extensions.vendor.dompdf.dompdf" );
//		\PhpOffice\PhpWord\Settings::setPdfRendererPath( $domPdfPath );
//		\PhpOffice\PhpWord\Settings::setPdfRendererName( 'DomPDF' );
//
//		$phpWord = \PhpOffice\PhpWord\IOFactory::load('helloWorld.docx');
////Save it
//		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//		$xmlWriter->save('result.pdf');
// Saving the document as OOXML file...
//		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter( $phpWord, 'Word2007' );
//		$objWriter->save( 'helloWorld.docx' );
	}
}
