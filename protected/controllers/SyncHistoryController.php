<?php

class SyncHistoryController extends GxController {

    public function actionCreate() {
        $model = new SyncHistory;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['SyncHistory'][$k] = $v;
            }
            $model->attributes = $_POST['SyncHistory'];
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->history_id;
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
            Yii::app()->end();

        }

    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'SyncHistory');


        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['SyncHistory'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SyncHistory'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->history_id;
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg
                ));
                Yii::app()->end();
            } else
            {
                $this->redirect(array('view', 'id' => $model->history_id));
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SyncHistory')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex()
    {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $param = array();
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        $criteria->addCondition('store = :store');
        $param[':store'] = $_POST['store'];

        $criteria->params = $param;
        $criteria->order = "tdate DESC";

        $model = SyncHistory::model()->findAll($criteria);
        $total = SyncHistory::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}