<?php
class PembantuPelunasanPiutangController extends GxController
{
	public function actionCreate()
	{
		if (!Yii::app()->request->isAjaxRequest)
			$this->redirect(url('/'));
		if (isset($_POST) && !empty($_POST)) {
			$msg = "Data gagal disimpan.";
			$status = false;
			$detils = CJSON::decode($_POST['detil']);
//            $detils2 = CJSON::decode($_POST['detil2']);
//            $is_new = $_POST['mode'] == 0;
			app()->db->autoCommit = false;
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$model = new PembantuPelunasanPiutang;
				$ref = new Reference();
				$docref = $ref->get_next_reference(PIUTANG_VOUCHER);
				foreach ($_POST as $k => $v) {
					if ($k == 'detil')
						continue;
					if (is_angka($v))
						$v = get_number($v);
					$_POST['PembantuPelunasanPiutang'][$k] = $v;
				}
				$_POST['PembantuPelunasanPiutang']['doc_ref'] = $docref;
				$model->attributes = $_POST['PembantuPelunasanPiutang'];
//                $balance = BankTrans::get_balance($_POST['bank_id'], $_POST['tgl']);
//                $balance = BankTrans::get_balance($_POST['bank_id'], $_POST['tgl']);
//                if (($balance - $_POST['PembantuPelunasanUtang']['total']) < 0) {
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) . "Insufficient funds");
//                }
				if (!$model->save()) {
					throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Debt Payment')) . CHtml::errorSummary($model));
				}
				$gl = new GL();
				foreach ($detils as $detil) {
					$pelunasan_detil = new PembantuPelunasanPiutangDetil;
					$_POST['PembantuPelunasanPiutangDetil']['kas_dibayar'] = get_number($detil['kas_dibayar']);
					$_POST['PembantuPelunasanPiutangDetil']['no_faktur'] = $detil['no_faktur'];
					$_POST['PembantuPelunasanPiutangDetil']['account_code'] = $detil['account_code'];
					$_POST['PembantuPelunasanPiutangDetil']['customer_id'] = $detil['customer_id'];
					$_POST['PembantuPelunasanPiutangDetil']['payment_journal_id'] = $detil['payment_journal_id'];
					$_POST['PembantuPelunasanPiutangDetil']['sisa'] = get_number($detil['sisa']);
					$_POST['PembantuPelunasanPiutangDetil']['type_'] = $detil['type_']; //= FALSE ? 0 : 1;
					$_POST['PembantuPelunasanPiutangDetil']['pembantu_pelunasan_piutang_id'] = $model->pembantu_pelunasan_piutang_id;
					$pelunasan_detil->attributes = $_POST['PembantuPelunasanPiutangDetil'];
					if (!$pelunasan_detil->save())
						throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Debt Payment')) . CHtml::errorSummary($pelunasan_detil));
					if ($pelunasan_detil->sisa == 0) {
						$coa = $this->loadModel($pelunasan_detil->payment_journal_id, 'PaymentJournal');
						$coa->lunas = 1;
						if (!$coa->save())
							throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase')) . CHtml::errorSummary($transferItem));
					}
					$ij = PaymentJournal::model()->findByPk($pelunasan_detil->payment_journal_id);
					$ij->editable = 1;
					$ij->save();
//                    $sup = Supplier::model()->findByPk($pelunasan_detil->supplier_id);
					$gl->add_gl(PIUTANG_VOUCHER, $model->pembantu_pelunasan_piutang_id, $model->tgl, $model->doc_ref, $pelunasan_detil->account_code, "Pelunasan Piutang $pelunasan_detil->no_faktur", '', $pelunasan_detil->kas_dibayar, 1, $model->store);
				}
				$bank = Bank::model()->findByPk($model->bank_id);
				if ($bank == null) {
					throw new Exception('Bank tidak ditemukan.');
				}
				/*HANYA UNTUK PAYMENT
//				$balance = BankTrans::get_balance($model->bank_id, $model->tgl);
//				if (($balance - abs($model->amount_bank)) < 0) {
//					throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PembantuPelunasanPiutang')) .
//						"Kurang dana. Tersedia " . number_format($balance, 2) . ' diperlukan ' . number_format($model->amount_bank, 2) .
//						"\nTotal Kekurangan " . number_format($balance - $model->amount_bank, 2)
//					);
//				}
				*/
				$gl->add_bank_trans(PIUTANG_VOUCHER, $model->pembantu_pelunasan_piutang_id, $model->bank_id, $docref, $model->tgl,
					-$model->amount_bank, $model->user_id, $model->store);
				$gl->add_gl(PIUTANG_VOUCHER, $model->pembantu_pelunasan_piutang_id, $model->tgl, $model->doc_ref, $bank->account_code, "Pelunasan Piutang $pelunasan_detil->no_faktur", '', -($model->amount_bank), 0, $model->store);
				$gl->add_gl(PIUTANG_VOUCHER, $model->pembantu_pelunasan_piutang_id, $model->tgl, $model->doc_ref, COA_ROUNDING, "Pelunasan Piutang $pelunasan_detil->no_faktur", '', -($model->rounding), 1, $model->store);
				$gl->validate();
				$ref->save(PIUTANG_VOUCHER, $model->pembantu_pelunasan_piutang_id, $docref);
//                $auw->save();
				$transaction->commit();
				$msg = t('save.success', 'app');
				$status = true;
				/* if(PUSH_PUSAT){
                    
                    U::runCommand('checkdata', 'gltrans', '--tno='.$model->pembantu_pelunasan_piutang_id , 'ppp_gl_tno'.$model->pembantu_pelunasan_piutang_id.'.log');

                    U::runCommand('checkdata', 'banktrans', '--tno='.$model->pembantu_pelunasan_piutang_id,  'ppp_bt_tno'.$model->pembantu_pelunasan_piutang_id.'.log');
                    
                    U::runCommand('checkdata', 'ref', '--tno='.$model->pembantu_pelunasan_piutang_id,  'ppp_ref_tno'.$model->pembantu_pelunasan_piutang_id.'.log');
                } */
			} catch (Exception $ex) {
				$transaction->rollback();
				$status = false;
				$msg = $ex->getMessage();
			}
			echo CJSON::encode(array(
				'success' => $status,
				'msg' => $msg
			));
			Yii::app()->end();
		}
	}
	public function actionIndex()
	{
		if (isset($_POST['limit'])) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if (isset($_POST['start'])) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$param = array();
		if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
			(isset($_POST['limit']) && isset($_POST['start']))
		) {
			$criteria->limit = $limit;
			$criteria->offset = $start;
		}
		if (isset($_POST['tgl'])) {
			$criteria->addCondition('tgl = :tgl');
			$param[':tgl'] = $_POST['tgl'];
		}
		$criteria->params = $param;
		$model = PembantuPelunasanPiutang::model()->findAll($criteria);
		$total = PembantuPelunasanPiutang::model()->count($criteria);
		$this->renderJson($model, $total);
	}
}
