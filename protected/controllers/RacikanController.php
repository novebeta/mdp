<?php

class RacikanController extends GxController {

    public function actionCreate() {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";

            $is_new = $_POST['mode'] == 0;
            $konversi_id = $_POST['id'];
            $detail = CJSON::decode($_POST['detil']);

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Racikan() : $this->loadModel($konversi_id, "Racikan");

                if (!$is_new) {
                    RacikanDetails::model()->deleteAll("racikan_id = :racikan_id", array(':racikan_id' => $model->racikan_id));
                }

                foreach ($_POST as $k => $v) {
                    if ($k == 'detil')
                        continue;
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['Racikan'][$k] = $v;
                }
                $_POST['Racikan']['qty'] = $_POST['qty_konv'];
                $model->attributes = $_POST['Racikan'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Konversi Barang')) . CHtml::errorSummary($model));
                }
//                $doc_ref = $model->no_batch;

                foreach ($detail as $detil) {
                    $item_details = new RacikanDetails;
                    $_POST['RacikanDetail']['barang_id'] = $detil['barang_id'];
                    $_POST['RacikanDetail']['qty'] = get_number($detil['qty']);
                    $_POST['RacikanDetail']['satuan'] = $detil['satuan'];
                    $_POST['RacikanDetail']['racikan_id'] = $model->racikan_id;

                    $item_details->attributes = $_POST['RacikanDetail'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Konvesi Barang Detail')) . CHtml::errorSummary($item_details));
                    }
                }

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Racikan');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Racikan'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Racikan'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->racikan_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->racikan_id));
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Racikan')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex() {        
        $cmd = new DbCmd('{{racikan}} t');
        
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(isset($_POST['limit'])? $_POST['limit'] : 20, isset($_POST['start'])? $_POST['start'] : 0);
        }

        $cmd->addSelect('t.*, b.kode_barang, b.nama_barang');
        $cmd->addLeftJoin('{{barang}} b', ' b.barang_id = t.barang_id ');
        $cmd->addOrder = 'b.kode_barang ASC';
            
        $model = $cmd->queryAll();
        $this->renderJsonArr($model);
    }

}
