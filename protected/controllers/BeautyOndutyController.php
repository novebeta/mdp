<?php
class BeautyOndutyController extends GxController
{
    public function actionCreate()
    {
        $model = new BeautyOnduty;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BeautyOnduty'][$k] = $v;
            }
            $model->attributes = $_POST['BeautyOnduty'];
            $model->time_start = new CDbExpression('NOW()');
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->beauty_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $id = $_POST['beauty_onduty_id'];
                /** @var BeautyOnduty $model */
                $model = $this->loadModel($id, 'BeautyOnduty');
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['BeautyOnduty'][$k] = $v;
                }
                $msg = "Data gagal disimpan";
                $model->attributes = $_POST['BeautyOnduty'];
                $model->time_start = new CDbExpression('NOW()');
                if ($model->note_ == 'KOSONG') {
                    $model->note_ = 'ISHOMA';
                } else {
                    $model->note_ = 'KOSONG';
                }
                if ($model->update()) {
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->beauty_onduty_id;
                } else {
                    $msg .= " " . Chtml::errorSummary($model);
                    $status = false;
                }
            } catch (Exception $e) {
                $status = false;
                $msg = $e->getMessage();
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'BeautyOnduty')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('tgl = DATE(now())');
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = BeautyOnduty::model()->findAll($criteria);
        $total = BeautyOnduty::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}