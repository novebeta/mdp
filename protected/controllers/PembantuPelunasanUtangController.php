<?php
class PembantuPelunasanUtangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan.";
            $status = false;
            $detils = CJSON::decode($_POST['detil']);
//            $detils2 = CJSON::decode($_POST['detil2']);
//            $is_new = $_POST['mode'] == 0;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new PembantuPelunasanUtang;
                $ref = new Reference();
                $docref = $ref->get_next_reference(SUPPIN);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil')
                        continue;
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['PembantuPelunasanUtang'][$k] = $v;
                }
                $_POST['PembantuPelunasanUtang']['doc_ref'] = $docref;
                $model->attributes = $_POST['PembantuPelunasanUtang'];
//                $balance = BankTrans::get_balance($_POST['bank_id'], $_POST['tgl']);
//                $balance = BankTrans::get_balance($_POST['bank_id'], $_POST['tgl']);
//                if (($balance - $_POST['PembantuPelunasanUtang']['total']) < 0) {
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) . "Insufficient funds");
//                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Debt Payment')) . CHtml::errorSummary($model));
                }
                $gl = new GL();
                foreach ($detils as $detil) {
                    $pelunasan_detil = new PembantuPelunasanUtangDetil;
                    $_POST['PembantuPelunasanUtangDetil']['kas_dibayar'] = get_number($detil['kas_dibayar']);
                    $_POST['PembantuPelunasanUtangDetil']['no_faktur'] = $detil['no_faktur'];
                    $_POST['PembantuPelunasanUtangDetil']['account_code'] = $detil['account_code'];
                    $_POST['PembantuPelunasanUtangDetil']['supplier_id'] = $detil['supplier_id'];
                    $_POST['PembantuPelunasanUtangDetil']['invoice_journal_id'] = $detil['invoice_journal_id'];
                    $_POST['PembantuPelunasanUtangDetil']['sisa'] = get_number($detil['sisa']);
                    $_POST['PembantuPelunasanUtangDetil']['type_'] = $detil['type_']; //= FALSE ? 0 : 1;
                    $_POST['PembantuPelunasanUtangDetil']['pembantu_pelunasan_utang_id'] = $model->pembantu_pelunasan_utang_id;
                    $pelunasan_detil->attributes = $_POST['PembantuPelunasanUtangDetil'];
                    if (!$pelunasan_detil->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Debt Payment')) . CHtml::errorSummary($pelunasan_detil));
                    if ($pelunasan_detil->sisa == 0) {
                        $coa = $this->loadModel($pelunasan_detil->invoice_journal_id, 'InvoiceJournal');
                        $coa->lunas = 1;
                        if (!$coa->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase')) . CHtml::errorSummary($transferItem));
                    }
                    $ij = InvoiceJournal::model()->findByPk($pelunasan_detil->invoice_journal_id);
                    $ij->editable = 1;
                    $ij->save();
//                    $sup = Supplier::model()->findByPk($pelunasan_detil->supplier_id);
                    $gl->add_gl(SUPPIN, $model->pembantu_pelunasan_utang_id, $model->tgl, $model->doc_ref, $pelunasan_detil->account_code, "Pelunasan Hutang $pelunasan_detil->no_faktur", '', $pelunasan_detil->kas_dibayar, 1, $model->store);
                }
                $bank = Bank::model()->findByPk($model->bank_id);
                if ($bank == null) {
                    throw new Exception('Bank tidak ditemukan.');
                }
//                $balance = BankTrans::get_balance($model->bank_id, $model->tgl);
//                if (($balance - abs($model->amount_bank)) < 0) {
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PembantuPelunasanUtang')) .
//                        "Kurang dana. Tersedia " . number_format($balance, 2) . ' diperlukan ' . number_format($model->amount_bank, 2) .
//                        "\nTotal Kekurangan " . number_format($balance - $model->amount_bank, 2)
//                    );
//                }
                $gl->add_bank_trans(SUPPIN, $model->pembantu_pelunasan_utang_id, $model->bank_id, $docref, $model->tgl,
                    -$model->amount_bank, $model->user_id, $model->store);
                $gl->add_gl(SUPPIN, $model->pembantu_pelunasan_utang_id, $model->tgl, $model->doc_ref, $bank->account_code, "Pelunasan Hutang $pelunasan_detil->no_faktur", '', -($model->amount_bank), 0, $model->store);
                $gl->add_gl(SUPPIN, $model->pembantu_pelunasan_utang_id, $model->tgl, $model->doc_ref, COA_ROUNDING, "Pelunasan Hutang $pelunasan_detil->no_faktur", '', -($model->rounding), 1, $model->store);
                $gl->validate();
                $ref->save(SUPPIN, $model->pembantu_pelunasan_utang_id, $docref);
//                $auw->save();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
                /* if(PUSH_PUSAT){
                    
                    U::runCommand('checkdata', 'gltrans', '--tno='.$model->pembantu_pelunasan_utang_id , 'ppu_gl_tno'.$model->pembantu_pelunasan_utang_id.'.log');

                    U::runCommand('checkdata', 'banktrans', '--tno='.$model->pembantu_pelunasan_utang_id,  'ppu_bt_tno'.$model->pembantu_pelunasan_utang_id.'.log');
                    
                    U::runCommand('checkdata', 'ref', '--tno='.$model->pembantu_pelunasan_utang_id,  'ppu_ref_tno'.$model->pembantu_pelunasan_utang_id.'.log');
                } */
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdateTgl()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['id'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var PembantuPelunasanUtang $model */
                $model = PembantuPelunasanUtang::model()->findByPk($id);
                if ($_POST['tgl'] != $model->tgl) {
                    $model->updateTgl($_POST['tgl']);
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionVoid($id)
    {
        if (Yii::app()->request->isPostRequest) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($id, 'PembantuPelunasanUtang');
                if ($model == null) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PembantuPelunasanUtang')) .
                        "Fatal error, record not found.");
                }

                $model->void_user_id = Yii::app()->user->getId();
                $model->void_tdate = new CDbExpression('NOW()');

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Debt Payment')) . CHtml::errorSummary($model));
                }
                $gl = new GL();
                $pelunasan_detils = PembantuPelunasanUtangDetil::model()->findAll("pembantu_pelunasan_utang_id=:pembantu_pelunasan_utang_id", [':pembantu_pelunasan_utang_id'=>$model->pembantu_pelunasan_utang_id]);
                foreach ( $pelunasan_detils as $pelunasan_detil) {
                        $coa = $this->loadModel($pelunasan_detil->invoice_journal_id, 'InvoiceJournal');
                        $coa->lunas = 0;
                        if (!$coa->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase')) . CHtml::errorSummary($transferItem));
                    $ij = InvoiceJournal::model()->findByPk($pelunasan_detil->invoice_journal_id);
                    $ij->editable = 0;
                    $ij->save();
                    $gl->add_gl(SUPPOUT, $model->pembantu_pelunasan_utang_id, $model->tgl, $model->doc_ref, $pelunasan_detil->account_code, "Void Pelunasan Hutang $pelunasan_detil->no_faktur", '', -($pelunasan_detil->kas_dibayar), 1, $model->store);
                }
                $bank = Bank::model()->findByPk($model->bank_id);
                if ($bank == null) {
                    throw new Exception('Bank tidak ditemukan.');
                }
                $gl->add_bank_trans(SUPPOUT, $model->pembantu_pelunasan_utang_id, $model->bank_id, $model->doc_ref, $model->tgl,
                    $model->amount_bank, $model->user_id, $model->store);
                $gl->add_gl(SUPPOUT, $model->pembantu_pelunasan_utang_id, $model->tgl, $model->doc_ref, $bank->account_code, "Void Pelunasan Hutang $pelunasan_detil->no_faktur", '', $model->amount_bank, 0, $model->store);
                $gl->add_gl(SUPPOUT, $model->pembantu_pelunasan_utang_id, $model->tgl, $model->doc_ref, COA_ROUNDING, "Void Pelunasan Hutang $pelunasan_detil->no_faktur", '', $model->rounding, 1, $model->store);
                $gl->validate();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        $criteria->params = $param;
        $model = PembantuPelunasanUtang::model()->findAll($criteria);
        $total = PembantuPelunasanUtang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}
