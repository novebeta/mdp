<?php
class BpBayar3Controller extends GxController {
	public function actionCreate() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				/** @var BodyPaint $invoice */
				$invoice         = BodyPaint::model()->findByPk( $_POST['body_paint_id'] );
				
				$invoice->b3_tgl = $_POST['tgl'];
				$invoice->lunas  = $_POST['tgl'];
				if ( ! $invoice->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'BodyPaint' ) ) . CHtml::errorSummary( $invoice ) );
				}
				foreach ( BpBayar3::model()->findAllByAttributes( [ 'body_paint_id' => $_POST['body_paint_id'] ] ) as $item ) {
					GlTrans::model()->deleteAllByAttributes( [ 'type_no' => $item->bp_bayar3_id, 'type' => BODYPAINT_BAYAR_3 ] );
					$item->delete();
				}
				foreach ( $detils as $row ) {
					$model                = new BpBayar3();
					$model->tgl           = $_POST['tgl'];
					$model->total         = get_number( $row['total'] );
					$model->bank_id       = $row['bank_id'];
					$model->body_paint_id = $_POST['body_paint_id'];
					if ( ! $model->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'BpBayar3' ) ) . CHtml::errorSummary( $model ) );
					}
					$gl   = new GL();
					$ref  = $invoice->inv_no;
					$bank = Bank::model()->findByPk( $model->bank_id );
					$gl->add_gl( BODYPAINT_BAYAR_3, $model->bp_bayar3_id, $model->tgl,
						$ref, $bank->account_code, "BAYAR PRIBADI INV $ref",
						"BAYAR PRIBADI INV $ref", $model->total, 0, '' );
					$gl->add_bank_trans( BODYPAINT_BAYAR_3, $model->bp_bayar3_id, $bank->bank_id,
						$ref, $model->tgl, $model->total, Yii::app()->user->getId(), '' );
					$gl->add_gl( BODYPAINT_BAYAR_3, $model->bp_bayar3_id, $model->tgl,
						$ref, '11-05-30', "BAYAR PRIBADI INV $ref",
						"BAYAR PRIBADI INV $ref", - $model->total, 1, '' );
					$gl->validate();
				}
				$total_all = get_number( $_POST['total'] );
				$grand_total = get_number( $_POST['grand_total'] );
				$pph23 = $grand_total - $total_all;
				if($pph23 > 0){
					$gl   = new GL();
					$ref  = $invoice->inv_no;
					$gl->add_gl( BODYPAINT_BAYAR_3, $model->bp_bayar3_id, $model->tgl,
						$ref, SysPrefs::get_val('PPH23'), "BAYAR PRIBADI INV $ref",
						"BAYAR PRIBADI INV $ref", $pph23, 0, '' );
					$gl->add_gl( BODYPAINT_BAYAR_3, $model->bp_bayar3_id, $model->tgl,
						$ref, '11-05-30', "BAYAR PRIBADI INV $ref",
						"BAYAR PRIBADI INV $ref", - $pph23, 0, '' );
					$gl->validate();
				}
				$msg = "Data berhasil disimpan";
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'BpBayar3' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['BpBayar3'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['BpBayar3'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bp_bayar3_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->bp_bayar3_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'BpBayar3' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition( "body_paint_id = :body_paint_id" );
		$criteria->params[':body_paint_id'] = $_POST['body_paint_id'];
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = BpBayar3::model()->findAll( $criteria );
		$total = BpBayar3::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}