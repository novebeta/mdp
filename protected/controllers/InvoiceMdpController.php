<?php
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Html;
class InvoiceMdpController extends GxController {
	public function actionCreate() {
		$model = new InvoiceMdp;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['InvoiceMdp'][ $k ] = $v;
			}
			$model->attributes = $_POST['InvoiceMdp'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->invoice_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'InvoiceMdp' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['InvoiceMdp'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['InvoiceMdp'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->invoice_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->invoice_id ) );
			}
		}
	}
	public function actionBayar( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg                  = 'Data berhasil dibayar.';
			$status               = true;
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				/** @var Bank $bank */
				$bank = Bank::model()->findByPk( $_POST['bank_id'] );
				/** @var InvoiceMdp $invoice */
				$invoice            = $this->loadModel( $id, 'InvoiceMdp' );
				$invoice->tgl_bayar = $_POST['tgl'];
				$invoice->bank_id   = $bank->bank_id;
				if ( ! $invoice->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Invoice' ) ) . CHtml::errorSummary( $invoice ) );
				}
				$invoice->refresh();
				$gl  = new GL();
				$ref = $invoice->doc_ref;
				$gl->add_gl( PELUNASAN_RESELLER, $invoice->invoice_id, $invoice->tgl_bayar,
					$ref, $bank->account_code, "Pelunasan $ref",
					"Pelunasan $ref", $invoice->total, 0, '' );
				$gl->add_bank_trans( PELUNASAN_RESELLER, $invoice->invoice_id, $bank->bank_id, $ref,
					$invoice->tgl_bayar, $invoice->total, Yii::app()->user->getId(), '' );
				/** @var Store $store */
				$store = Store::model()->findByPk( $invoice->store_kode );
				if ( $store == null ) {
					throw new Exception( ' Reseller ['.$invoice->store_kode.'] tidak ditemukan.' );
				}
				$gl->add_gl( PELUNASAN_RESELLER, $invoice->invoice_id, $invoice->tgl_bayar,
					$ref, $store->account_code, "Pelunasan $ref",
					"Pelunasan $ref", - $invoice->total, 1, '' );
				$gl->validate();
				$transaction->commit();
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionPrintInvoice() {
		global $periode, $ref, $total_jual, $disc, $disc_rp, $ppn, $bayar;
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dibayar.';
			$status = true;
			try {
				/** @var InvoiceMdp $inv */
				$inv = $this->loadModel( $_POST['invoice_id'], 'InvoiceMdp' );
				/** @var Store $cbg */
				$cbg              = $this->loadModel( $inv->store_kode, 'Store' );
				$lastDayThisMonth = sql2date( $inv->tgl_periode );
				$thisDay          = sql2date( $inv->tgl_periode, 'MMM yyyy' );
				$ref              = $inv->doc_ref;
				$disc             = number_format( $inv->disc, 0 ) . "%";
				$total_jual       = $inv->sub_total;
				$disc_rp          = $inv->disc_rp;
				$ppn              = $inv->ppn;
				$bayar            = $inv->total;
//				$periode          = "$thisDay s/d $lastDayThisMonth";
//				Yii::import( "application.components.tbs_class", true );
//				Yii::import( "application.components.tbs_plugin_excel", true );
//				Yii::import( "application.components.tbs_plugin_opentbs", true );
//				$TBS = new clsTinyButStrong;
//				$TBS->PlugIn( TBS_INSTALL, OPENTBS_PLUGIN );
//				$TBS->LoadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) . DIRECTORY_SEPARATOR . 'invoice_mdp.xlsx', OPENTBS_ALREADY_UTF8 );
				$comm    = Yii::app()->db->createCommand( "SELECT 
       nb.nama_barang AS nama_barang,
       SUM(nsdm.qty) AS qty,
	   nk.nama AS nama_kategori,
       nsdm.harga AS harga,       
       SUM(nsdm.tot_qty_harga) AS total
FROM
(((((((nscc_sales_mdp AS nsm
JOIN nscc_mobil AS nm ON (nsm.mobil_id = nm.mobil_id))
JOIN nscc_mobil_tipe AS nmt ON (nm.mobil_tipe_id = nmt.mobil_tipe_id))))
JOIN nscc_sales_detail_mdp AS nsdm ON (nsdm.sales_id = nsm.sales_id))
JOIN nscc_barang AS nb ON (nsdm.barang_id = nb.barang_id))
JOIN nscc_mobil_kategori AS nk ON (nmt.mobil_kategori_id = nk.mobil_kategori_id))
WHERE nsm.invoice_id = :invoice_id
GROUP BY nsdm.barang_id,nk.mobil_kategori_id" );
				$summary = $comm->queryAll( true, [ ':invoice_id' => $_POST['invoice_id'] ] );
				$jml     = sizeof( $summary );
//				$TBS->MergeBlock( 'a', $summary );
//				$TBS->Show( TBS_EXCEL_DOWNLOAD, "Invoice" . $ref . ".xlsx" );
				$reader      = new Xlsx();
				$spreadsheet = $reader->load( Yii::getPathOfAlias( 'application.views.reports' ) . DIRECTORY_SEPARATOR . 'invoice_mdp.xlsx' );
				$worksheet   = $spreadsheet->getActiveSheet();
				$worksheet->getCell( 'F6' )->setValue( 'No. Invoice :' . $ref );
				$worksheet->getCell( 'F7' )->setValue( 'Tanggal     :' . $lastDayThisMonth );
				$worksheet->getCell( 'C9' )->setValue( $cbg->nama_store );
				$worksheet->getCell( 'C10' )->setValue( $cbg->alamat );
				$worksheet->getCell( 'C11' )->setValue( $cbg->city );
				$worksheet->getCell( 'C14' )->setValue( 'Bulan ' . $thisDay );
				$worksheet->getCell( 'G18' )->setValue( $total_jual );
				$worksheet->getCell( 'G19' )->setValue( $disc_rp );
				$worksheet->getCell( 'G20' )->setValue( $ppn );
				$worksheet->getCell( 'G21' )->setValue( $bayar );
				$worksheet->getCell( 'F19' )->setValue( $disc );
				if ( $jml > 0 ) {
					$worksheet->insertNewRowBefore( 18, $jml );
				}
				$spreadsheet->getActiveSheet()
				            ->fromArray(
					            $summary,   // The data to set
					            null,           // Array values with this value will not be set
					            'C17'            // Top left coordinate of the worksheet range where
				            //    we want to set these values (default is A1)
				            );
				$writer = new Html( $spreadsheet );
				echo $writer->generateHTMLHeader();
				echo "<style>";
				echo "td    {padding: 2px;}";
				echo $writer->generateStyles( false );
				echo "</style>";
				echo "<script src='" . Yii::app()->request->baseUrl . "/js/jquery-1.11.1.min.js" . "'></script>";
				echo "<script>
$(document).ready(function() {
    var img = $('img'); 
//    img[0].setAttribute('src','" . Yii::app()->request->baseUrl . "/images/ug.png');
    img[0].setAttribute('src','" . Yii::app()->request->baseUrl . "/images/mdp.png');
});";
				echo "</script>";
				echo $writer->generateSheetData();
				echo $writer->generateHTMLFooter();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
//			echo CJSON::encode( array(
//				'success' => $status,
//				'msg'     => $msg
//			) );
//			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria         = new CDbCriteria();
		$criteria->select = 'nim.invoice_id,nim.doc_ref,nim.tgl_cetak,nim.total,nim.store_kode,nim.tgl_bayar,
		CONCAT("1-",DATE_FORMAT(nim.tgl_periode, "%d %M %y" )) AS tgl_periode';
		$criteria->alias  = 'nim';
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( STOREID != '' ) {
			$criteria->addCondition( 'nim.store_kode = :store_kode' );
			$criteria->params[':store_kode'] = STOREID;
		}
		if ( isset( $_POST['store'] ) && $_POST['store'] != null ) {
			$criteria->addCondition( 'nim.store_kode = :store_kode' );
			$criteria->params[':store_kode'] = $_POST['store'];
		}
		if ( isset( $_POST['blmlunas'] ) && $_POST['blmlunas'] == 'true' ) {
			$criteria->addCondition( 'nim.tgl_bayar is null' );
		}
		$model = InvoiceMdp::model()->findAll( $criteria );
		$total = InvoiceMdp::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}