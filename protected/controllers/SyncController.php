<?php

class SyncController extends GxController
{
    public function actionSync()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));

        $msg = "Sync data failed.";

        try {

            $cabang = $_POST['branch'];
            if($cabang == "")
            {
                $cabang = "All";
            }

            $startdate = $_POST['startdate'];
            $enddate = $_POST['enddate'];
            $action = $_POST['action'];

            $start = date('d-m-Y', strtotime($startdate));
            $end = date('d-m-Y', strtotime($enddate));
            $param = $cabang." ".$start." ".$end;

            $namauser = Yii::app()->user->getName();
            $txt="";
            $date = date('Y-m-d H:i:s');


                if ($action == "H") //History
                {
                    U::runSync('Syncron', 'SyncHistory', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'synchistory_' . $cabang . '-' . $start . '-' . $end . '.log');
                    U::runSync('Syncron', 'SyncPasien', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'syncpasien' . $cabang . '-' . $start . '-' . $end . '.log');
                    $txt = "\n\nStart : ".$date."\nUser : ".$namauser."\nStatus : Running SyncHistoryPasien ".$param;
                }
                elseif ($action == "BTR") //Bank trans
                {
                    U::runSync('Syncron', 'SyncBtr', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'syncbtr_' . $cabang . '-' . $start . '-' . $end . '.log');
                    $txt = "\n\nStart : ".$date."\nUser : ".$namauser."\nStatus : Running SyncBtr ".$param;
                }
                elseif ($action == "GLT") //GL trans
                {
                    U::runSync('Syncron', 'SyncGlt', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'syncglt_' . $cabang . '-' . $start . '-' . $end . '.log');
                    $txt = "\n\nStart : ".$date."\nUser : ".$namauser."\nStatus : Running SyncGlt ".$param;
                }
                /*elseif ($action == "SLS") //Sales
                {
                    //U::runSync('Syncron', 'SyncSls', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'syncsls_' . $cabang . '-' . $start . '-' . $end . '.log');
                    //$txt = "\n\nStart : ".$date."\nUser : ".$namauser."\nStatus : Running SyncSls ".$param;
                }*/
                elseif ($action == "C") //Customer
                {
                    U::runSync('Syncron', 'SyncCustomer', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'synccustomer_' . $cabang . '-' . $start . '-' . $end . '.log');
                    $txt = "\n\nStart : ".$date."\nUser : ".$namauser."\nStatus : Running SyncCustomer ".$param;
                }

                $history = new SyncHistory;
                $history->username = $namauser;
                $history->store = $cabang;
                $history->tdate = $date;
                $history->startdate = $startdate;
                $history->enddate = $enddate;
                $history->action = $action;
                $history->type = "1";
                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SyncHistory')) . CHtml::errorSummary($history));


                file_put_contents("logsync.log", $txt, FILE_APPEND);
                $msg = "Sync data ".$action." success.";
                $status = true;
        }
        catch (Exception $ex)
        {
            $msg = $ex->getMessage();
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionScan()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));

        $msg = "Scan data diproses.. harap tunggu...";
        $checkconn = "";

        try
        {
            $namauser = Yii::app()->user->getName();
            $date = date('Y-m-d H:i:s');

            $cabang = $_POST['branch'];

            if($cabang == "")
            {
                $cabang = "All";
            }

            $startdate = $_POST['startdate'];
            $enddate = $_POST['enddate'];

            U::runSync('Syncron', 'Scan', '--cabang=' . $cabang, '--start=' . $startdate, '--end=' . $enddate, 'scan_' . $cabang . '-' . $startdate . '-' . $enddate . '.log');

                $history = new SyncHistory;
                $history->username = $namauser;
                $history->store = $cabang;
                $history->tdate = $date;
                $history->startdate = $startdate;
                $history->enddate = $enddate;
                $history->action = "---";
                $history->type = "0";
                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SyncHistory')) . CHtml::errorSummary($history));

                $status = true;
        }
        catch (Exception $ex)
        {
            $msg = $ex->getMessage();
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
}