<?php

class NsccVoucherOnlineController extends GxController {

    public function actionCreate()
    {
        //$model = new NsccVoucherOnline;
        if (!Yii::app()->request->isAjaxRequest)
        return;
        if (isset($_POST) && !empty($_POST)) {
        /*foreach($_POST as $k=>$v){
            if (is_angka($v)) $v = get_number($v);
            $_POST['NsccVoucherOnline'][$k] = $v;
        }*/
        //$model->attributes = $_POST['NsccVoucherOnline'];


            $model = new NsccVoucherOnline;
            $model->trx_id = $_POST['id'];
            $model->kode_voucher = $_POST['vid'];
            $model->nama_voucher = $_POST['vnama'];
            $model->desc = $_POST['vdesc'];
            $model->exp = $_POST['exp'];
            $model->tdate = $_POST['tdate'];
            $model->outlet_id = $_POST['oid'];
            $model->outlet_name = $_POST['oname'];
            $model->outlet_code = $_POST['ocode'];
            $model->outlet_address = $_POST['oaddress'];
            $model->online_id = $_POST['uid'];

        $msg = "Data gagal disimpan.";
            $status = true;
            if (!$model->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'NsccVoucherOnline')) . CHtml::errorSummary($model));

            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
                Yii::app()->end();

        }

    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'NsccVoucherOnline');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['NsccVoucherOnline'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['NsccVoucherOnline'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'NsccVoucherOnline')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}
$model = NsccVoucherOnline::model()->findAll($criteria);
$total = NsccVoucherOnline::model()->count($criteria);

$this->renderJson($model, $total);

}

}