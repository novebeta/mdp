<?php
class MobilBpController extends GxController {
	public function actionCreate() {
		$model = new MobilBp;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['MobilBp'][ $k ] = $v;
			}
			$model->attributes = $_POST['MobilBp'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->mobil_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'MobilBp' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['MobilBp'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['MobilBp'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->mobil_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->mobil_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'MobilBp' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$param    = [];
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( isset( $_POST['mobil_id'] ) ) {
			unset( $_POST['query'] );
			$criteria->addCondition( 'mobil_id = :mobil_id' );
			$param[':mobil_id'] = $_POST['mobil_id'];
		}
		if ( isset( $_POST['no_rangka'] ) ) {
			$criteria->addCondition( 'no_rangka like :no_rangka');
			$param[':no_rangka'] = '%' . $_POST['no_rangka'] . '%';
		}
		if ( isset( $_POST['no_pol'] ) ) {
			$criteria->addCondition( 'no_pol like :no_pol');
			$param[':no_pol'] = '%' . $_POST['no_pol'] . '%';
		}
		if ( isset( $_POST['query'] ) ) {
			$criteria->addCondition( 'no_pol like :no_pol', 'OR' );
			$param[':no_pol'] = '%' . $_POST['query'] . '%';
		}
		$criteria->params = $param;
//		$criteria->select = 'p.merk_id';
		$criteria->with = [
			'mobilTipe' => [
				'alias'  => 'p',
				'select' => 'p.merk_id,p.mobil_kategori_id'
			],
			'customer' => [
				'alias'  => 'c',
				'select' => 'c.nama_customer,c.ktp,c.email,c.alamat,c.telp'
			]
		];
		/** @var MobilBp[] $model */
		$model = MobilBp::model()->findAll( $criteria );
		$total = MobilBp::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}