<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/14/14
 * Time: 8:13 AM
 */
class PrintRPJ extends BasePrint
{
    /** @var  $s Rpg */
    private $s;
    function __construct($s)
    {
        $this->s = $s;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
//        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Doc. Ref", $this->s->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", sql2date($this->s->tgl, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $user = Users::model()->findByPk($this->s->id_user);
        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $raw .= $newLine;
        $sales = Salestrans::model()->findByPk($this->s->salestrans_id);
        $raw .= parent::addHeaderSales("No. Receipt", $sales->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Amount", $this->s->amount);
        $raw .= $newLine;
        $bank = Bank::model()->findByPk($this->s->bank_id);
        $raw .= parent::addHeaderSales("Pay Method", $bank->nama_bank);
        $raw .= $newLine;
        $raw .= "Note:";
        $raw .= $newLine;
//        $raw .= wordwrap($this->s->note, CHARLENGTHRECEIPT);
        $raw .= parent::__wordwrap($this->s->note, 1, 0);
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
//        $raw .= '.';
//        $raw .= $newLine;
        U::save_file(ReportPath . $this->s->doc_ref . '.txt', $raw);
        return $raw;
    }
} 