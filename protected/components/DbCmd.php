<?php
class DbCmd {
    private  $cmd;

    private $connection;
    public  $select;
    private $arrSelect;
    public  $from;
    private  $arrFrom;
    private  $join;
    private  $arrWhere;
    public  $params;
    public  $group;
    private  $arrGroup;
    public  $order;
    private  $arrOrder;
    private  $arrHaving;
    public  $limit;
    public  $offset;
    public  $as;
    public  $isUnion;

    private static $inCounter = 0;

    private $tablePattern = "/\S+((\s+(AS|as|As|sA)\s+)|(\s+))\S+/";

    /**
     * @return DbCmd
     */
    static public function instance(){
        return new self();
    }

    /**
     * DbCmd constructor.
     * @param string $table
     */
    public function __construct($table=''){
        $this->reset();
        $table && $this->addFrom($table);
        $this->connection = Yii::app()->db;
    }

    public function reset(){
        $this->select = '';
        $this->arrSelect = [];

        $this->from = '';
        $this->arrFrom = [];

        $this->join = [];

        $this->arrWhere = [];
        $this->params = [];

        $this->group = '';
        $this->arrGroup = [];

        $this->order = '';
        $this->arrOrder = [];

        $this->arrHaving = [];

        $this->limit = false;
        $this->offset = false;

        $this->as = false;

        $this->isUnion = false;

        //self::$inCounter = 0;
    }

    /**
     * @param CDbConnection $connection
     */
    public function setConnection($connection){
        $this->connection = $connection;
    }

    /**
     * @param array $params
     */
    private function mergeParams($params = []){
        $this->params = array_merge($this->params, $params);
    }

    /**
     * @param string|DbCmd $table
     */
    private function addArrFrom($table){
        if($table instanceof self){
            $this->arrFrom[] = '('.$table->getText().')'.($this->isUnion?'':' AS '.($table->as? $table->as : 't'.count($this->arrFrom)));
            $this->mergeParams($table->params);
        } else
            $this->arrFrom[] = $table;
    }

    /**
     * @param string|array $table
     * @return $this
     */
    public function addFrom($table){
        if(is_array($table)){
            foreach ($table as $t){
                $this->addArrFrom($t);
            }
        } else
            $this->addArrFrom($table);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearFrom(){
        $this->arrFrom = [];

        return $this;
    }

    /**
     * @param string|array $fields
     * @return $this
     */
    public function addSelect($fields){
        if(is_array($fields)) $this->arrSelect = array_merge($this->arrSelect, $fields);
        else $this->arrSelect[] = $fields;

        return $this;
    }

    /**
     * @param int|string $index
     * @param mixed $fields
     * @return $this
     */
    public function updateSelect($index = -1, $fields){
        $this->arrSelect[$index] = $fields;

        return $this;
    }

    /**
     * @return $this
     */
    public function clearSelect(){
        $this->arrSelect = [];

        return $this;
    }

    const Equal = '=';
    const NotEqual = '!=';

    /**
     * @param string $field1
     * @param string $field2
     * @param string $rule
     * @param string $operand
     * @return $this
     */
    public function addCompare($field1, $field2, $rule = self::Equal, $operand = 'AND'){
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $this->arrWhere[] = '(('.$field1.' IS NOT NULL AND '.$field2.' IS NOT NULL AND '.' '.$field1.$rule.' '.$field2.') OR ('.$field1.' IS NULL '.$rule.' '.$field2.' IS NULL))';

        return $this;
    }

    const OperatorAND = 'AND';
    const OperatorOR = 'OR';

    /**
     * @param string $condition
     * @param string $operand
     * @return $this
     */
    public function addCondition($condition, $operand = self::OperatorAND){
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $this->arrWhere[] = $condition;

        return $this;
    }

    /**
     * @param string $field
     * @param string $operand
     * @return $this
     */
    public function addIsNullCondition($field, $operand = self::OperatorAND){
        $this->addCondition($field.' IS NULL', $operand);

        return $this;
    }

    /**
     * @param string $field
     * @param string $operand
     * @return $this
     */
    public function addIsNotNullCondition($field, $operand = self::OperatorAND){
        $this->addCondition($field.' IS NOT NULL', $operand);

        return $this;
    }

    /**
     * @param string $field
     * @param string|array|DbCmd $values
     * @param string $operand
     * @return $this
     */
    public function addInCondition($field, $values, $operand = self::OperatorAND){
        $this->addInOrNotInCondition('IN', $field, $values, $operand);

        return $this;
    }

    /**
     * @param string $field
     * @param string|array|DbCmd $values
     * @param string $operand
     * @return $this
     */
    public function addNotInCondition($field, $values, $operand = self::OperatorAND){
        $this->addInOrNotInCondition('NOT IN', $field, $values, $operand);

        return $this;
    }

    /**
     * @param string $in
     * @param string $field
     * @param string|array|DbCmd $values
     * @param string $operand
     */
    private function addInOrNotInCondition($in, $field, $values, $operand = self::OperatorAND){
        self::$inCounter++;
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $condition = $field." $in ( ";

        if($values instanceof self){
            $condition .= $values->getText();
            $this->params = array_merge($this->params, $values->params);
        }else if(is_array($values)){
            for($i = 0; $i < count($values); $i++){
                if($i > 0 && $i < count($values)) $condition .= ' , ';
                $key = ":inOrNotIn".self::$inCounter.$i;
                $condition .= $key;
                $this->params[$key] = $values[$i];
            }
        }else{
            $condition .= $values;
        }

        $this->arrWhere[] = $condition.' )';
    }

    /**
     * @return $this
     */
    public function clearCondition(){
        $this->arrWhere = [];

        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addParam($key, $value){
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function addParams($params=[]){
        if(is_array($params))
            $this->params = array_merge($this->params, $params);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearParam(){
        $this->params = [];

        return $this;
    }

    const LEFT_JOIN = 'LEFT JOIN';
    const RIGHT_JOIN = 'RIGHT JOIN';
    const INNER_JOIN = 'INNER JOIN';

    /**
     * @param string|DbCmd $table
     * @param string|DbCmd $condition
     * @param string $join
     * @return $this
     */
    public function addJoin($table, $condition, $join = self::LEFT_JOIN){
        if($table instanceof self){
            $joinTable = '('.$table->getText().') AS '.($table->as? $table->as : 'jt'.count($this->join));
            $this->params = array_merge($this->params, $table->params);
        }else{
            $joinTable = $table;
        }

        if($condition instanceof self){
            $this->mergeParams($condition->params);
            $condition = $condition->getWhere();
        }

        $this->join[] = ' ' . $join . ' ' . $joinTable . ' ON ' . $condition . ' ';

        return $this;
    }

    /**
     * @param string|DbCmd $table
     * @param string|DbCmd $condition
     * @return $this
     */
    public function addLeftJoin($table, $condition){
        $this->addJoin($table, $condition, self::LEFT_JOIN);

        return $this;
    }

    /**
     * @param string|DbCmd $table
     * @param string|DbCmd $condition
     * @return $this
     */
    public function addRightJoin($table, $condition){
        $this->addJoin($table, $condition, self::RIGHT_JOIN);

        return $this;
    }

    /**
     * @param string|DbCmd $table
     * @param string|DbCmd $condition
     * @return $this
     */
    public function addInnerJoin($table, $condition){
        $this->addJoin($table, $condition, self::INNER_JOIN);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearJoin(){
        $this->join = [];

        return $this;
    }

    /**
     * @param string|array $fields
     * @return $this
     */
    public function addOrder($fields){
        if(is_array($fields)) $this->arrOrder = array_merge($this->arrOrder, $fields);
        else $this->arrOrder[] = $fields;

        return $this;
    }

    /**
     * @return $this
     */
    public function clearOrder(){
        $this->arrOrder = [];

        return $this;
    }

    /**
     * @param string|array $fields
     * @return $this
     */
    public function addGroup($fields){
        if(is_array($fields)) $this->arrGroup = array_merge($this->arrGroup, $fields);
        else $this->arrGroup[] = $fields;

        return $this;
    }

    /**
     * @return $this
     */
    public function clearGroup(){
        $this->arrGroup = [];

        return $this;
    }

    /**
     * @param string $condition
     * @param string $operand
     * @return $this
     */
    public function addHaving($condition, $operand = self::OperatorAND){
        if(count($this->arrHaving)) $this->arrHaving[] = $operand;
        $this->arrHaving[] = $condition;

        return $this;
    }

    /**
     * @return $this
     */
    public function clearHaving(){
        $this->arrHaving = [];

        return $this;
    }

    /**
     * @param int $limit
     * @param bool $offset
     * @return $this
     */
    public function setLimit($limit, $offset = false){
        $this->limit = (int)$limit;
        if($offset!==false) $this->offset = (int)$offset;

        return $this;
    }

    /**
     * @param array $tables
     * @return $this
     */
    public function union(array $tables){
        $this->reset();
        $this->isUnion = true;
        foreach ($tables as $table)
            $this->addArrFrom($table);

        return $this;
    }

    private function prepare(){
        if($this->isUnion){
            $this->cmd = $this->connection->createCommand(implode(" UNION ", $this->arrFrom));
        }else{
            $newLine = chr(13).chr(10);

            $this->cmd = $this->connection->createCommand();
            $this->cmd->select = ($this->select? $this->select.(count($this->arrSelect)?', ':''):'').implode($newLine.", ", $this->arrSelect);
            $this->cmd->from = ($this->from? $this->from.(count($this->arrFrom)?', ':''):'').implode($newLine.", ", $this->arrFrom);
            $this->cmd->join = implode(" ".$newLine, $this->join);
            $this->cmd->where = implode(" ".$newLine, $this->arrWhere);
            $this->cmd->order = ($this->order? $this->order.(count($this->arrOrder)?', ':''):'').implode($newLine.", ", $this->arrOrder);
            $this->cmd->group = ($this->group? $this->group.(count($this->arrGroup)?', ':''):'').implode($newLine.", ", $this->arrGroup);
            $this->cmd->having = implode(" ".$newLine, $this->arrHaving);
            if($this->limit !== false) $this->cmd->limit($this->limit);
            if( $this->offset !== false) $this->cmd->offset($this->offset);
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function query($params = []){
        $this->prepare();
        return $this->cmd->query(count($params)? array_merge($this->params, $params):$this->params);
    }

    /**
     * @param bool $fetchAssociative
     * @param array $params
     * @return mixed
     */
    public function queryAll($fetchAssociative=true, $params = []){
        $this->prepare();
        return $this->cmd->queryAll($fetchAssociative, count($params)? array_merge($this->params, $params):$this->params);
    }

    /**
     * @param bool $fetchAssociative
     * @param array $params
     * @return mixed
     */
    public function queryRow($fetchAssociative=true, $params = []){
        $this->prepare();
        return $this->cmd->queryRow($fetchAssociative, count($params)? array_merge($this->params, $params):$this->params);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function queryScalar($params = []){
        $this->prepare();
        return $this->cmd->queryScalar(count($params)? array_merge($this->params, $params):$this->params);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function queryColumn($params = []){
        $this->prepare();
        return $this->cmd->queryColumn(count($params)? array_merge($this->params, $params):$this->params);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function queryCount($params = []){
        $this->prepare();
        $this->cmd->select = 'COUNT(*)';
        return $this->cmd->queryScalar(count($params)? array_merge($this->params, $params):$this->params);
    }

    /**
     * @return mixed
     */
    public function getText(){
        $this->prepare();
        return $this->cmd->getText();
    }

    /**
     * @return mixed
     */
    public function getQuery(){
        $query = $this->getText();
        foreach ($this->params as $k => $v){
            $val = is_string($v)? "'$v'" : $v;
            $query = str_replace($k, $val, $query);
        }
        return $query;
    }

    /**
     * The alias name,
     * Will be used when applied as subquery
     * @param string $alias
     * @return $this
     */
    public function setAs($alias){
        $this->as = $alias;

        return $this;
    }

    /**
     * @return string
     */
    public function getWhere(){
        return implode(" ", $this->arrWhere);
    }
}