<?php
class PrintRekapBeauty extends BasePrint
{
    /**
     * @var
     */
    private $tgl;
    private $col_width = 40;
    /**
     * PrintRekapBeauty constructor.
     * @param $tgl
     */
    public function __construct($tgl)
    {
        $this->tgl = $tgl;
    }
    public function buildTxt()
    {
        $hasil = BeautyServices::getSummary($this->tgl, $this->tgl);
        if (empty($hasil)) {
            return false;
        }
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Tanggal", sql2date($this->tgl, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $raw .= $newLine;
        $total_jasa = 0;
        $kode_beauty = null;
        foreach ($hasil as $beauty) {
            if ($kode_beauty == $beauty['kode_beauty']) {
                continue;
            }
            $kode_beauty = $beauty['kode_beauty'];
            $raw .= parent::addHeaderSales("Nama Beauty", $beauty['nama_beauty']);
            $raw .= $newLine;
            $raw .= parent::addHeaderSales("Kode Beauty", $beauty['kode_beauty']);
            $raw .= $newLine;
            $raw .= parent::addHeaderSales("Golongan", $beauty['nama_gol']);
            $raw .= $newLine;
            $raw .= parent::fillWithChar("=", $this->col_width);
            $raw .= $newLine;
            $data = $hasil;
            $total = 0;
            $urut = 1;
            foreach ($data as $key => $detil) {
                if ($kode_beauty == $detil['kode_beauty']) {
                    $total += $detil['tip'];
                    $raw .= parent::addItemRekapBeauty($urut . ".", $detil['kode_barang'],
                        $detil['qty'], number_format($detil['tip'], 2), $this->col_width);
                    $raw .= $newLine;
                    unset($data[$key]);
                }
            }
            $raw .= parent::addLeftRight("", self::fillWithChar("-", 17), $this->col_width);
            $raw .= $newLine;
            $raw .= parent::addLeftRight(" " . $beauty['kode_beauty'], number_format($total, 2), $this->col_width);
            $raw .= $newLine;
            $raw .= $newLine;
            $total_jasa += $total;
            $hasil = $data;
        }
//        $raw .= $newLine;
        $raw .= parent::addLeftRight("", self::fillWithChar("-", 23), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::addLeftRight(" Total All Beauty", number_format($total_jasa, 2), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"), $this->col_width);
        return $raw;
    }
}