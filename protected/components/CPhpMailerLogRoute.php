<?php
/**
 * CPHPMailerLogRoute class file.
 *
 * This file does the configuration necessary with
 * user's preferences and sends the email.
 * PHPMailerLogRoute could be used by 4 methods:
 * 'SMTP', 'SENDMAIL', 'MAIL and 'QMAIL'. All the
 * methods needs to be configured on computer.
 * Example:
 *  config => array (
 *          'SMTP',
 *          'Host' => "your.host.com",
 *          'SMTPAuth' => true,
 *          'SMTPSecure' => "ssl",
 *          'Host' => "smtp.<local>.com",
 *          'Port' => <port>,
 *          'Username' => "<user>@<domain>.com",
 *          'Password' => "<password>",
 *          'Charset' => "UTF-8",
 *  )
 *
 * @author    Léo Willian Kölln <leo@opasoft.com.br>
 * @author    Giovanna Garcia Basilio <giovanna@opasoft.com.br>
 * @copyright 2012 Opasoft Soluções Ltda.
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 */
//require_once (dirname(__FILE__) . '/class.phpmailer.php');
Yii::import('application.extensions.smtpmail.PHPMailer');
/**
 * CPHPMailerLogRoute sends selected log messages to email addresses using the PHPMailer system.
 *
 * @property array  $emails   List of destination email addresses.
 * @property string $subject  Email subject.
 * @property string $sentFrom Send from address of the email.
 * @property array  $headers  Additional headers to use when sending an email.
 * @property array  $config   Configurations for the send mechanism of PHPMailer.
 *
 * @author    Léo Willian Kölln <leo@opasoft.com.br>
 * @author    Giovanna Garcia Basilio <giovanna@opasoft.com.br>
 */
class CPHPMailerLogRoute extends CEmailLogRoute
{
    /**
     * @var array list of configurations to use when sending an email.
     */
    private $_config = array();
    /**
     * Sends an email.
     *
     * @param string $email single email address
     * @param string $subject email subject
     * @param string $message email content
     *
     * @return void
     */
    protected function sendEmail($email, $subject, $message)
    {
        $configuration = $this->getConfig();
        $sendMethod = $configuration[0];
        unset($configuration[0]);
        $mailer = new PHPMailer();
        switch ($sendMethod) {
            case "SMTP":
                $mailer->IsSMTP();
                break;
            case "SENDMAIL":
                $mailer->IsSendmail();
                break;
            case "QMAIL":
                $mailer->IsQmail();
                break;
            case "MAIL":
            default:
                $mailer->IsMail();
        }
        foreach ($configuration as $attribute => $value) {
            $mailer->$attribute = $value;
        }
        $mailer->SetFrom($this->getSentFrom());
        $mailer->AddAddress($email);
        $mailer->Subject = $subject;
        $mailer->MsgHTML($message);
        $mailer->AddStringAttachment(
            bzcompress(json_encode($this->logs, JSON_PRETTY_PRINT), 9),
            "$subject.bz2");
        $mailer->Send();
    }
    /**
     * Get the configurations
     *
     * @return array configurations for the send mechanism.
     */
    public function getConfig()
    {
        return $this->_config;
    }
    /**
     * Set all configurations inserted
     *
     * @param mixed $value list of configurations to the PHPMailer send mechanism.
     *
     * @return void
     */
    public function setConfig($value)
    {
        if (is_array($value)) {
            $this->_config = $value;
        } else {
            $this->_config = array($value);
        }
    }
}