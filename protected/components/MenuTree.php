<?php
class MenuTree {
	var $security_role;
	var $menu_users = array(
		'text' => 'User Manajement',
		'id'   => 'jun.UsersGrid',
		'leaf' => true
	);
	var $security = array(
		'text' => 'Security Roles',
		'id'   => 'jun.SecurityRolesGrid',
		'leaf' => true
	);
	function __construct( $id ) {
		$role                = SecurityRoles::model()->findByPk( $id );
		$this->security_role = explode( ",", $role->sections );
	}
	function getChildMaster() {
		/** @var TODO 112 kosong */
		$child = array();
//		if ( in_array( 100, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Country',
//				'id'   => 'jun.NegaraGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 101, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'State',
//				'id'   => 'jun.ProvinsiGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 102, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'City',
//				'id'   => 'jun.KotaGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 103, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sub District',
//				'id'   => 'jun.KecamatanGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 104, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Doctor',
//				'id'   => 'jun.DokterGrid',
//				'leaf' => true
//			);
//		}
//
//
//		if ( in_array( 106, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Group',
//				'id'   => 'jun.GrupGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 107, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Paket Treatment',
//				'id'   => 'jun.PaketPerawatanGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 133, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Resep',
//				'id'   => 'jun.ResepGrid',
//				'leaf' => true
//			);
//		}
		if ( in_array( 115, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Reseller',
				'id'   => 'jun.StoreGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 155, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Grup Kategori Mobil',
				'id'   => 'jun.MobilGrupGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 156, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kategori Mobil',
				'id'   => 'jun.MobilKategoriGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 157, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Merk Mobil',
				'id'   => 'jun.MerkGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 158, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Tipe Mobil',
				'id'   => 'jun.MobilTipeGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 108, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Customers',
				'id'   => 'jun.CustomersGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 159, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Mobil',
				'id'   => 'jun.MobilGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 107, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Produk',
				'id'   => 'jun.BarangGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 160, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Harga Jual',
				'id'   => 'jun.HargaJualMdpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 407, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Range Diskon',
				'id'   => 'jun.RangeDiskonGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 408, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Disk Ultra Grand',
				'id'   => 'jun.UgPersenWin',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 273, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Sales Order',
				'id'   => 'jun.SalesMdpGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 274, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pekerjaan Dalam Proses',
				'id'   => 'jun.WorkOrderGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 277, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pekerjaan Dalam Proses (All)',
				'id'   => 'jun.WorkOrderAllGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 275, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pekerjaan Selesai',
				'id'   => 'jun.WorkFinishGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 276, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pembayaran',
				'id'   => 'jun.InvoiceMdpGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 278, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Work Order UG',
				'id'   => 'jun.WougGrid ',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 365, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pekerjaan Dalam Proses',
				'id'   => 'jun.PekerjaanDalamProses',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 366, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pekerjaan Selesai',
				'id'   => 'jun.PekerjaanSelesai',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		return $child;
	}
	function getMaster( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Ultra Grand',
			'expanded' => false,
			'children' => $child
		);
	}
	function getTransaction( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Body Paint',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildTransaction() {
		$child = array();
		if ( in_array( 295, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Supplier Parts',
				'id'   => 'jun.BpSuppPartsGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 279, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Customer',
				'id'   => 'jun.CustomerBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 280, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Merk',
				'id'   => 'jun.MerkBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 281, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Tipe Mobil',
				'id'   => 'jun.MobilTipeBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 282, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Mobil',
				'id'   => 'jun.MobilBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 283, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Asuransi',
				'id'   => 'jun.AsuransiBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 284, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kategori Mobil',
				'id'   => 'jun.MobilKategoriBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 285, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Jasa',
				'id'   => 'jun.JasaBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 286, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Harga Jasa',
				'id'   => 'jun.HargaJualBpGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 409, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Disc Body Paint',
				'id'   => 'jun.DiscBodyPaintWin',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 287, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Estimasi',
				'id'   => 'jun.BodyPaintGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 288, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Work Order',
				'id'   => 'jun.BodyPaintWOGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 289, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Invoice',
				'id'   => 'jun.BodyPaintINVGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 290, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Bayar Jasa',
				'id'   => 'jun.BpBayarJasaGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 291, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Bayar Parts',
				'id'   => 'jun.BpBayarPartsGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 294, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Invoice HO',
				'id'   => 'jun.BodyPaintCloseGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 297, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pembayaran Asuransi',
				'id'   => 'jun.BpBayar2Grid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 292, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kas/Bank BP Masuk',
				'id'   => 'jun.KasGridBp',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 293, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kas/Bank BP Keluar',
				'id'   => 'jun.KasGridBpOut',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 367, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Laporan Pekerjaan Luar',
				'id'   => 'jun.PekerjaanLuar ',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 368, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Laporan Analisa Faktur',
				'id'   => 'jun.LaporanAnalisaBP ',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 372, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Laporan BP Trans',
				'id'   => 'jun.LaporanBPTrans ',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		return $child;
	}
	function getAccounting( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Accounting',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildAccounting() {
		$child = array();
		if ( in_array( 117, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Akun',
				'id'   => 'jun.DesignLabaRugiGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kas/Bank',
				'id'   => 'jun.BankGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 324, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Nomor Faktur',
				'id'   => 'jun.NoFakturGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 211, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Jurnal Umum',
				'id'   => 'jun.JurnalUmum',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 212, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kas/Bank Masuk',
				'id'   => 'jun.KasGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 213, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kas/Bank Keluar',
				'id'   => 'jun.KasGridOut',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 214, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Mutasi Kas/Bank',
				'id'   => 'jun.BankTransGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 215, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Buat Laba Tahun Berjalan',
				'id'   => 'jun.GenerateLabaRugi',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 323, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Rekening Koran',
				'id'   => 'jun.ReportRekeningKoran',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 325, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Export CSV Faktur',
				'id'   => 'jun.ReportCSVFaktur',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 302, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Mutasi Stok',
				'id'   => 'jun.ReportInventoryMovements',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 303, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Kartu Stok',
				'id'   => 'jun.ReportInventoryCard',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 315, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Jurnal',
				'id'   => 'jun.ReportGeneralJournal',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 314, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Buku Besar',
				'id'   => 'jun.ReportGeneralLedger',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 319, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Neraca Lajur',
				'id'   => 'jun.ReportBalanceSheet',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 318, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Laba Rugi',
				'id'   => 'jun.ReportLabaRugi',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 320, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Neraca',
				'id'   => 'jun.ReportNeraca',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		if ( in_array( 369, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Tutup Buku',
				'id'   => 'url.tutupbuku',
				'url'   => 'site/previewclosing',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		return $child;
	}
	function getReport( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Report',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildReport() {
		$child = array();
		/*
		if (in_array(506, $this->security_role)) {
			$child[] = array(
				'text' => 'Asset Details',
				'id' => 'jun.AssetDetailGrid',
				'leaf' => true
			);
		}*/
//		if ( in_array( 603, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Assets',
//				'id'   => 'jun.ReportAssetTotal',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 507, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Show Assets',
//				'id'   => 'jun.ShowAssetDetailGrid',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 313, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales Summary',
//				'id'   => 'jun.ReportSalesSummaryReceipt',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 316, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales Details',
//				'id'   => 'jun.ReportSalesSummaryReceiptDetails',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 317, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Customer Details',
//				'id'   => 'jun.ReportCustomerSummaryDetails',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 321, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Payments',
//				'id'   => 'jun.ReportPayments',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 336, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Fee Card',
//				'id'   => 'jun.ReportFeeCard',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 300, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales Summary Group',
//				'id'   => 'jun.ReportSalesSummary',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 301, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales Details Group',
//				'id'   => 'jun.ReportSalesDetails',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 335, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales Item Details',
//				'id'   => 'jun.ReportSalesItemDetails',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 361, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Print Resep Apotik',
//				'id'   => 'jun.PrintResepApotik',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 331, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales and Return Sales',
//				'id'   => 'jun.ReportSalesNReturnDetails',
//				'leaf' => true
//			);
//		}
//		if ( NATASHA_CUSTOM ) {
//			if ( in_array( 338, $this->security_role ) ) {
//				$child[] = array(
//					'text' => 'Report Transaksi',
//					'id'   => 'jun.ReportTransaksi',
//					'leaf' => true
//				);
//			}
//		}
//	    else
//	    {
//		if ( in_array( 338, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Rekap Penjualan',
//				'id'   => 'jun.ReportRekapPerawatan',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 338, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Rekap Penjualan & Disc. Amount',
//				'id'   => 'jun.ReportRekapPenjualanplusdiscamount',
//				'leaf' => true
//			);
//		}
//	    }
//		if ( in_array( 333, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Inventory Movements Perlengkapan',
//				'id'   => 'jun.ReportInventoryMovementsPerlengkapan',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 350, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Inventory Card Perlengkapan',
//				'id'   => 'jun.ReportInventoryCardPerlengkapan',
//				'leaf' => true
//			);
//		}
//		if ( NATASHA_CUSTOM ) {
//			if ( in_array( 305, $this->security_role ) ) {
//				$child[] = array(
//					'text' => 'Report Jasa Beauty, KMR, KMT',
//					'id'   => 'jun.ReportJasaBeauty',
//					'leaf' => true
//				);
//			}
//		} else {
//			if ( in_array( 304, $this->security_role ) ) {
//				$child[] = array(
//					'text' => 'Beautician Services Summary',
//					'id'   => 'jun.ReportBeautySummary',
//					'leaf' => true
//				);
//			}
//			if ( in_array( 305, $this->security_role ) ) {
//				$child[] = array(
//					'text' => 'Beautician Services Details',
//					'id'   => 'jun.ReportBeautyDetails',
//					'leaf' => true
//				);
//			}
//			if ( in_array( 311, $this->security_role ) ) {
//				$child[] = array(
//					'text' => 'Doctors Services Summary',
//					'id'   => 'jun.ReportDokterSummary',
//					'leaf' => true
//				);
//			}
//			if ( in_array( 312, $this->security_role ) ) {
//				$child[] = array(
//					'text' => 'Doctors Services Details',
//					'id'   => 'jun.ReportDokterDetails',
//					'leaf' => true
//				);
//			}
//		}
//		if ( in_array( 306, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'New Customers',
//				'id'   => 'jun.ReportNewCustomers',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 337, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Real Customers',
//				'id'   => 'jun.ReportRealCustomers',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 334, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'BirthDay Customers',
//				'id'   => 'jun.ReportBirthDayCustomers',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 332, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Category Customers',
//				'id'   => 'jun.ReportCategoryCustomers',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 307, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Tenders',
//				'id'   => 'jun.ReportTender',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 308, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Daily Report',
//				'id'   => 'jun.ReportLaha',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 309, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Return Sales Summary',
//				'id'   => 'jun.ReportReturSalesSummary',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 310, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Return Sales Details',
//				'id'   => 'jun.ReportReturSalesDetails',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 322, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Sales Price',
//				'id'   => 'jun.ReportSalesPrice',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 330, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Purchase Price',
//				'id'   => 'jun.ReportPurchasePrice',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 324, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Kartu Hutang',
//				'id'   => 'jun.ReportKartuHutang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 345, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Buku Hutang',
//				'id'   => 'jun.ReportBukuHutang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 324, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Hutang',
//				'id'   => 'jun.R_ReportHutang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 324, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Kartu Piutang',
//				'id'   => 'jun.ReportKartuPiutang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 345, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Buku Piutang',
//				'id'   => 'jun.ReportBukuPiutang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 324, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Piutang',
//				'id'   => 'jun.R_ReportPiutang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 325, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Chart Customers Attendance',
//				'id'   => 'jun.ChartCustAtt',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 326, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Chart New Customers',
//				'id'   => 'jun.ChartNewCustomers',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 327, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Chart Sales Group',
//				'id'   => 'jun.ChartSalesGrup',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 328, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Chart Top Customers',
//				'id'   => 'jun.ChartTopCust',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 329, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Chart Top Sales Group',
//				'id'   => 'jun.ChartTopSalesGrup',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 339, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Info Efektivitas',
//				'id'   => 'jun.ReportEfektivitas',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 340, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Transfer Barang Masuk',
//				'id'   => 'jun.ReportTerimaBarang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 341, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Pasien Baru Riil Faktur',
//				'id'   => 'jun.ReportPasienBaruRiilFaktur',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 342, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Stock In Transit',
//				'id'   => 'jun.ReportStockInTransit',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 343, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Rekap Purchase Order',
//				'id'   => 'jun.ReportPurchaseOrder',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 344, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Rekap Transfer Request',
//				'id'   => 'jun.ReportRekapOrderDropping',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 346, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Invoice Supplier',
//				'id'   => 'jun.ReportSupplierInvoice',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 347, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Bonus',
//				'id'   => 'jun.ReportBonus',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 348, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report CashFlow',
//				'id'   => 'jun.ReportCashFlow',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 349, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Cashier Turnover',
//				'id'   => 'jun.ReportOmsetKasir',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 352, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Produksi',
//				'id'   => 'jun.ReportProduksi',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 353, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Omset Per Group',
//				'id'   => 'jun.ReportOmsetGroup',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 354, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Customer nox',
//				'id'   => 'jun.ReportSalesSummaryReceiptDetailsCus',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 355, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Fee Referral',
//				'id'   => 'jun.ReportFeeReferral',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 356, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Efektivitas Doctor',
//				'id'   => 'jun.ReportEfektivitasDokter',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 357, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Efektivitas Doctor Service',
//				'id'   => 'jun.ReportEfektivitasDoctorService',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 358, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Dropping',
//				'id'   => 'jun.ReportDropping',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 359, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Dropping Return',
//				'id'   => 'jun.ReportDroppingReturn',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 600, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Rekap Transfer',
//				'id'   => 'jun.ReportRekapDropping',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 601, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Transfer Barang In and Out',
//				'id'   => 'jun.ReportTransferBarang',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 602, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Kas Kecil',
//				'id'   => 'jun.ReportRekeningKoranKasKecil',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 361, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Apotek',
//				'id'   => 'jun.ReportApotek',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 360, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Rating Keluhan',
//				'id'   => 'jun.ReportRatingKetdisc',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 362, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Kas Marketing',
//				'id'   => 'jun.ReportKegiatan',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 363, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Rekap Kegiatan',
//				'id'   => 'jun.ReportRekapKegiatan',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 364, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Rekap Kegiatan Details',
//				'id'   => 'jun.ReportRekapKegiatanDetails',
//				'leaf' => true
//			);
//		}
//		if ( in_array( 602, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Report Monitoring Omset',
//				'id'   => 'jun.ReportMonitoringOmset',
//				'leaf' => true
//			);
//		}
		return $child;
	}
	function getAdministration( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Administration',
			'expanded' => false,
			'children' => $child
		);
	}

	function getChildTransaksiMerdeka() {
		$child = array();

		if ( in_array( 701, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Customer',
				'id'   => 'jun.CustomersFlazzGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 702, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Supplier',
				'id'   => 'jun.SuppFlazzGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 703, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Barang',
				'id'   => 'jun.BarangFlazzGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}
		if ( in_array( 704, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Setting Barang',
				'id'   => 'jun.BarangAttrGrid',
				'cls'  => 'master',
				'leaf' => true
			);
		}

		if ( in_array( 705, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pembelian',
				'id'   => 'jun.TransferBarangGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 706, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Penjualan',
				'id'   => 'jun.SalesFlazzGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 708, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Penjualan Bayar',
				'id'   => 'jun.SalesFlazzBayarGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 707, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Flazz Top Up',
				'id'   => 'jun.SalesFlazzTopupGrid',
				'cls'  => 'transaksi',
				'leaf' => true
			);
		}
		if ( in_array( 730, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Laporan Trans Merdeka',
				'id'   => 'jun.LaporanTransMerdeka',
				'cls'  => 'report',
				'leaf' => true
			);
		}
		return $child;
	}
	function getTransaksiMerdeka( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Transaksi Merdeka',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildAdministration() {
		$child = array();
		if ( in_array( 400, $this->security_role ) ) {
			$child[] = array(
				'text' => 'User Management',
				'id'   => 'jun.UsersGrid',
				'leaf' => true
			);
		}
		if ( in_array( 401, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Security Roles',
				'id'   => 'jun.SecurityRolesGrid',
				'leaf' => true
			);
		}
		if ( in_array( 402, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Backup / Restore',
				'id'   => 'jun.BackupRestoreWin',
				'leaf' => true
			);
		}
		if ( in_array( 403, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Import',
				'id'   => 'jun.ImportXlsx',
				'leaf' => true
			);
		}
		if ( in_array( 404, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Setting Client',
				'id'   => 'jun.SettingsClientsGrid',
				'leaf' => true
			);
		}
		if ( in_array( 405, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Preferences',
				'id'   => 'jun.SysPrefsWin',
				'leaf' => true
			);
		}
		if ( in_array( 406, $this->security_role ) ) {
			$child[] = array(
				'text' => 'User Employee',
				'id'   => 'jun.UserEmployeeGrid',
				'leaf' => true
			);
		}
		if ( in_array( 502, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Restrict Date User',
				'id'   => 'jun.RestrictDateGrid',
				'leaf' => true
			);
		}
		if ( in_array( 503, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Upload History',
				'id'   => 'jun.UploadHistoryManual',
				'leaf' => true
			);
		}
		if ( in_array( 508, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Sync',
				'id'   => 'jun.SyncStatusGrid',
				'leaf' => true
			);
		}
		return $child;
	}
	function getGeneral() {
		$username = Yii::app()->user->name;
		$child    = array();
		if ( in_array( 000, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Change Password',
				'id'   => 'jun.PasswordWin',
				'leaf' => true
			);
		}
		if ( in_array( 001, $this->security_role ) ) {
			$child[] = array(
				'text' => "Logout ($username)",
				'id'   => 'logout',
				'leaf' => true
			);
		}
		return $child;
	}
	public function get_menu() {
		$data   = array();
		$master = self::getMaster( self::getChildMaster() );
		if ( ! empty( $master ) ) {
			$data[] = $master;
		}
		$transMerdeka = self::getTransaksiMerdeka( self::getChildTransaksiMerdeka() );
		if ( ! empty( $transMerdeka ) ) {
			$data[] = $transMerdeka;
		}
		$trans = self::getTransaction( self::getChildTransaction() );
		if ( ! empty( $trans ) ) {
			$data[] = $trans;
		}
		$trans = self::getAccounting( self::getChildAccounting() );
		if ( ! empty( $trans ) ) {
			$data[] = $trans;
		}
		$report = self::getReport( self::getChildReport() );
		if ( ! empty( $report ) ) {
			$data[] = $report;
		}
		$adm = self::getAdministration( self::getChildAdministration() );
		if ( ! empty( $adm ) ) {
			$data[] = $adm;
		}
		/*if(HEADOFFICE)
		{
			$sync = self::getSync(self::getChildSync());
			if (!empty($sync)) {
				$data[] = $sync;
			}
		}*/
		$username = Yii::app()->user->name;
		if ( in_array( 000, $this->security_role ) ) {
			$data[] = array(
				'text' => 'Change Password',
				'id'   => 'jun.PasswordWin',
				'leaf' => true
			);
		}
		if ( in_array( 001, $this->security_role ) ) {
			$data[] = array(
				'text' => "Logout ($username)",
				'id'   => 'logout',
				'leaf' => true
			);
		}
		return CJSON::encode( $data );
	}
	public function getState( $section ) {
//        $state = 0;
//        if (in_array($section, $this->security_role)) {
//            $state++;
//            if (count($this->security_role) > 1) $state++;
//        }
//        return $state;
		return in_array( $section, $this->security_role );
	}
}
