<?php
/**
 * @property Antrian $s
 * @property AntrianPrint $antrianPrint
 */
class PrintAntrianc extends BasePrint
{
    /**
     * @return AntrianPrint
     */

    /**
     * @param AntrianPrint $antrianPrint
     */
//    public function setResepPrint(ResepPrint $resepPrint)
//    {
//        $this->resepPrint = $resepPrint;
//    }


    public function buildTxtAntrianc($antrianp)

    {
        if($antrianp['counter']!='E'){
            $antrianp['counter']='';
        }

        $fontsize11="\x1d!".sprintf("%c", 0);
        $fontsize31="\x1d!".sprintf("%c", 17);
        $fontbold="\x1bE".sprintf("%c", 1);
        $fontunbold="\x1bE".sprintf("%c", 0);
        $newLine = "\r\n";
        $raw = parent::setCenter($antrianp['header0']);
        $raw .= $newLine;
        $raw .= parent::setCenter($antrianp['header1']);
        $raw .= $newLine;
        $raw .= parent::setCenter($antrianp['header2']);
        $raw .= $newLine;
        $raw .= parent::setCenter($antrianp['header3']);
        $raw .= $newLine;
        $raw .= parent::setCenter($antrianp['header4']);
        $raw .= $newLine;
        $raw .= parent::setCenter($antrianp['header5']);
        $raw .= $newLine;
        $raw .= $fontbold;
        $raw .= $this->setTextSize(2,2);
        $raw .= parent::setCenter('ANTREAN',24);
//        $raw .= $this->setTextSize(1,1);
        $raw .= $newLine;
        $raw .= $this->setTextSize(3,3);
        $raw .= parent::setCenter($antrianp['counter'].$antrianp['no'],16);
        $raw .= $this->setTextSize(1,1);
        $raw .= $fontunbold;

        $raw .= $newLine;
        if ($antrianp['footer0'] != '') {
            $raw .= parent::setCenter($antrianp['footer0']);
            $raw .= $newLine;
        }
        if ($antrianp['footer1'] != '') {
            $raw .= parent::setCenter($antrianp['footer1']);
            $raw .= $newLine;
        }
        if ($antrianp['footer2'] != '') {
            $raw .= parent::setCenter($antrianp['footer2']);
            $raw .= $newLine;
        }
        if ($antrianp['footer3'] != '') {
            $raw .= parent::setCenter($antrianp['footer3']);
            $raw .= $newLine;
        }
        if ($antrianp['footer4'] != '') {
            $raw .= parent::setCenter($antrianp['footer4']);
            $raw .= $newLine;
        }
        if ($antrianp['footer5'] != '') {
            $raw .= parent::setCenter($antrianp['footer5']);
            $raw .= $newLine;
        }
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"));
        $raw .= $newLine;
//        U::save_file(ReportPath . $this->s->doc_ref . '.txt', $raw);
        return $raw;
//        return base64_encode(chr(27) . chr(64) . parent::fillWithChar("-") . chr(27) . chr(105));
    }

    protected function setTextSize($widthMultiplier, $heightMultiplier)
    {
        if($widthMultiplier < 1 || $widthMultiplier > 8 || $heightMultiplier < 1 || $heightMultiplier > 8) return false;
        $c = pow(2, 4) * ($widthMultiplier - 1) + ($heightMultiplier - 1);
        return "\x1d!". chr($c);
    }



}