<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/12/14
 * Time: 11:30 AM
 */
class BasePrint
{
    const NEWLINE = "\r\n";
    function fillWithChar($char, $l = CHARLENGTHRECEIPT)
    {
//        $res = "";
//        for ($i = 0; $i < $l; $i++) {
//            $res .= $char;
//        }
        if ($l < 0) {
            return "";
        }
        return str_repeat($char, $l);
//        return $res;
    }
    function setCenter($msg, $l = CHARLENGTHRECEIPT)
    {
        $lmsg = strlen($msg);
        if ($lmsg > $l) {
            return substr($msg, 0, $l);
        }
        return str_pad($msg, $l, " ", STR_PAD_BOTH);
//        $sisa = $l - $lmsg;
//        $awal = $sisa >> 1;
//        $res = self::fillWithChar(" ", $awal);
//        $res .= $msg . self::fillWithChar(" ", $sisa - $awal);
//        return $res;
    }
    function __wordwrap($msg, $left, $right, $l = CHARLENGTHRECEIPT)
    {
        $words = wordwrap($msg, $l - $left - $right, '$$');
        $arr_word = explode('$$', $words);
        $res = '';
        foreach ($arr_word as $line) {
            $res .= str_repeat(' ', $left) . $line . str_repeat(' ', $right) . self::NEWLINE;
        }
        return $res;
    }
    function addHeaderSales($msg1, $msg2, $l = 15)
    {
        $lmsg1 = strlen($msg1);
        if ($lmsg1 > $l) {
            $msg1 = substr($msg1, 0, $l);
            $lmsg1 = 15;
        }
        $res = $msg1 . self::fillWithChar(" ", $l - $lmsg1);
        $res .= ":" . $msg2;
        return $res;
    }
    function addHeaderResep($msg1, $msg2, $l = 12)
    {
        $lmsg1 = strlen($msg1);
        if ($lmsg1 > $l) {
            $msg1 = substr($msg1, 0, $l);
            $lmsg1 = 12;
        }
        $res = $msg1 . self::fillWithChar(" ", $l - $lmsg1);
        $res .= ":" . $msg2;
        return $res;
    }
    function addLeftRight($msg1, $msg2, $l = CHARLENGTHRECEIPT)
    {
        $lmsg1 = strlen($msg1);
        $mmsg1 = 36;
        if ($lmsg1 > $mmsg1) {
            $msg1 = substr($msg1, 0, $mmsg1);
            $lmsg1 = $mmsg1;
        }
        $lmsg2 = strlen($msg2);
        $mmsg2 = 21;
        if ($lmsg2 > $mmsg2) {
            $msg2 = substr($msg2, 0, $mmsg2);
            $lmsg2 = $mmsg2;
        }
        $sisa = $l - ($lmsg1 + $lmsg2);
        return $msg1 . self::fillWithChar(" ", $sisa) . $msg2;
    }
    function addLeftRightSide($msg1, $msg2, $l = CHARLENGTHRECEIPT)
    {
        $lmsg1 = strlen($msg1);
//        $mmsg1 = 36;
//        if ($lmsg1 > $mmsg1) {
//            $msg1 = substr($msg1, 0, $mmsg1);
//            $lmsg1 = $mmsg1;
//        }
        $lmsg2 = strlen($msg2);
//        $mmsg2 = 14;
//        if ($lmsg2 > $mmsg2) {
//            $msg2 = substr($msg2, 0, $mmsg2);
//            $lmsg2 = $mmsg2;
//        }
        $tl = $lmsg1 + $lmsg2;
        if ($tl > $l) {
            $lmgs1 = ceil($tl / 2);
            $lmsg2 = $tl - $lmsg1;
            $msg1 = substr($msg1, 0, $lmgs1);
            $msg2 = substr($msg2, 0, $lmsg2);
        }
        $sisa = $l - ($lmsg1 + $lmsg2);
        return $msg1 . self::fillWithChar(" ", $sisa) . $msg2;
    }
    function addItemCodeReceipt($itemKode, $qty, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $litemKode = strlen($itemKode); // max 30
        $mitemKode = 30;
        $lqty = strlen($qty); //max 10
        $mqty = 10;
        $lsubtotal = strlen($subtotal); //max 22
        $msubtotal = 22;
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
//            $litemKode = $mitemKode;
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mqty);
//            $lqty = $mqty;
        }
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
//            $lsubtotal = $msubtotal;
        }
        $msg1 = self::addLeftRightSide($itemKode, $qty, $mitemKode + $mqty);
        $result = self::addLeftRightSide($msg1, $subtotal, $l);
        return $result;
    }
    function addItemCodeReceiptNoSat($no, $itemKode, $qty, $sat, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $mno = 4;
        $lno = strlen($no); // max 5
        $litemKode = strlen($itemKode); // max 30
        $mitemKode = 15;
        $lqty = strlen($qty); //max 10
        $mqty = 10;
        $lsat = strlen($sat); //max 15
        $msat = 9;
        $lsubtotal = strlen($subtotal); //max 22
        $msubtotal = 22;
        if ($lno > $mno) {
            $no = substr($no, 0, $mno);
        }
        $no = str_pad($no, $mno, ' ', STR_PAD_LEFT);
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mqty);
        }
        if ($lsat > $msat) {
            $sat = substr($sat, 0, $msat);
        }
        $sat = str_pad($sat, $msat, ' ', STR_PAD_RIGHT);
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
        }
        $msg2 = self::addLeftRightSide("$no $itemKode", $qty, $mno + $mitemKode + $mqty);
//        $msg3 = self::addLeftRightSide($msg2, $sat, $mno + $mitemKode + $mqty + $msat);
        $msg3 = "$msg2 $sat";
        $result = self::addLeftRightSide($msg3, $subtotal, $l);
        return $result;
    }
    function addItem($no, $itemKode, $qty, $sat, $l = CHARLENGTHRECEIPT)
    {
        $mno = 4;
        $lno = strlen($no); // max 5
        $litemKode = strlen($itemKode); // max 30
        $mitemKode = (NATASHA_CUSTOM) ? 20:25;
        $lqty = strlen($qty); //max 10
        $mqty = (NATASHA_CUSTOM) ? 5 : 10;
        $lsat = strlen($sat); //max 15
        $msat = (NATASHA_CUSTOM) ? 10 : 9;
        if ($lno > $mno) {
            $no = substr($no, 0, $mno);
        }
        $no = str_pad($no, $mno, ' ', STR_PAD_LEFT);
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mqty);
        }
        if ($lsat > $msat) {
            $sat = substr($sat, 0, $msat);
        }
        $sat = str_pad($sat, $msat, ' ', STR_PAD_RIGHT);
        $msg2 = self::addLeftRightSide("$no $itemKode", $qty, $mno + $mitemKode + $mqty);
//        $msg3 = self::addLeftRightSide($msg2, $sat, $mno + $mitemKode + $mqty + $msat);
        $msg3 = "$msg2 $sat";
        $result = $msg3;
        return $result;
    }
    function addItemRekapBeauty($no, $itemKode, $qty, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $mno = 4;
        $lno = strlen($no); // max 5
        $litemKode = strlen($itemKode); // max 30
        $mitemKode = 15;
        $lqty = strlen($qty); //max 10
        $mqty = 10;
        $lsubtotal = strlen($subtotal); //max 22
        $msubtotal = 22;
        if ($lno > $mno) {
            $no = substr($no, 0, $mno);
        }
        $no = str_pad($no, $mno, ' ', STR_PAD_LEFT);
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mqty);
        }
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
        }
        $msg2 = self::addLeftRightSide("$no $itemKode", $qty, $mno + $mitemKode + $mqty);
        $result = self::addLeftRightSide($msg2, $subtotal, $l);
        return $result;
    }
    function addItemCodeResepSat($itemKode, $qty, $sat, $l = CHARLENGTHRECEIPT)
    {
        $litemKode = strlen($itemKode); // max 30
        $mitemKode = 15;
        $lqty = strlen($qty); //max 10
        $mqty = 11;
        $lsat = strlen($sat); //max 15
        $msat = 20;
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mqty);
        }
        if ($lsat > $msat) {
            $sat = substr($sat, 0, $msat);
        }
        $msg1 = self::addLeftRightSide($itemKode, $qty, $mitemKode + $mqty);
        $result = "$msg1 $sat";
        return $result;
    }
    function addResepBahan($no, $itemKode, $qty, $sat, $l = CHARLENGTHRECEIPT)
    {
        $mno = (PRINTER_STOCKER_U220) ? 4:7;
        $mqty = (PRINTER_STOCKER_U220) ? 5:11;
        $sat = (PRINTER_STOCKER_U220) ? $sat : $sat . '   ';
        $no = str_pad($no, $mno, ' ', STR_PAD_LEFT);
//        $qty = str_pad($qty, $mqty, STR_PAD_LEFT);
        $lno = strlen($no); // max 5
        $litemKode = strlen($itemKode); // max 30
        $mitemKode = 23;
        $lqty = strlen($qty); //max 10
        $lsat = strlen($sat); //max 15
        $msat = (PRINTER_STOCKER_U220)? 12 : 20;
        if ($lno > $mno) {
            $no = substr($no, 0, $mno);
        }
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
        } else {
            $itemKode = str_pad($itemKode, $mitemKode, ' ', STR_PAD_RIGHT);
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mqty);
        }
        if ($lsat > $msat) {
            $sat = substr($sat, 0, $msat);
        }
        $msg1 = "$no $itemKode";
        $msg2 =  (PRINTER_STOCKER_U220) ? self::addLeftRightSide($msg1, $qty, ($l-5)) : self::addLeftRightSide($msg1, $qty, $mno + $mitemKode + $mqty);
        $result = "$msg2 $sat";
        return $result;
    }
    function addItemNameReceipt($itemName, $l, $prefix = 5)
    {
        $itemName = self::fillWithChar(" ", $prefix) . $itemName;
        if (strlen($itemName) > $l) {
            return substr($itemName, 0, $l);
        }
        return $itemName;
    }
    function addItemDiscReceipt($disc, $subtotal, $prefix = 6, $l = CHARLENGTHRECEIPT)
    {
        $lbldisc = self::fillWithChar(" ", $prefix) . "Disc: $disc";
        return self::addLeftRight($lbldisc, $subtotal, $l);
    }
} 