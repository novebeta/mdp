<?php
class SoapClientYii extends SoapClient
{
    public function __doRequest($req, $location, $action, $version = SOAP_1_1)
    {
        $xml = explode("\r\n", parent::__doRequest($req, $location, $action, $version));
        $response = preg_replace('/^(\x00\x00\xFE\xFF|\xFF\xFE\x00\x00|\xFE\xFF|\xFF\xFE|\xEF\xBB\xBF)/', "", $xml[5]);
        return $response;
    }
}