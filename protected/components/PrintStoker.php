<?php
class PrintStoker extends PrintReceipt
{
    public function buildTxt()
    {
//        $this->setResepPrint(parent::createResepPrint());
        $newLine = "\r\n";
        $width = (PRINTER_STOCKER_U220) ? 40 : CHARLENGTHRECEIPT;
	    $raw = parent::setCenter(SysPrefs::get_val('resep_header0'), $width);
	    $raw .= $newLine;
	    $raw .= parent::setCenter(SysPrefs::get_val('resep_header1'), $width);
	    $raw .= $newLine;
	    $raw .= parent::setCenter(SysPrefs::get_val('resep_header2'), $width);
	    $raw .= $newLine;
	    $raw .= parent::setCenter(SysPrefs::get_val('resep_header3'), $width);
	    $raw .= $newLine;
	    $raw .= parent::setCenter(SysPrefs::get_val('resep_header4'), $width);
	    $raw .= $newLine;
	    $raw .= parent::setCenter(SysPrefs::get_val('resep_header5'), $width);
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Receipt", $this->resepPrint->doc_ref_receipt);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", sql2date($this->resepPrint->date_, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Customer Code", $this->resepPrint->pasien_code);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Customer Name", $this->resepPrint->pasien_name);
        $raw .= $newLine;
//        $user = Users::model()->findByPk($this->s->user_id);
        $raw .= parent::addHeaderSales("Cashier", $this->resepPrint->kasir);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Stocker", $this->resepPrint->stocker);
//        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
//        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=", $width);
        $raw .= $newLine;
        $crite = new CDbCriteria();
        $crite->addCondition('stok = 1');
        $crite->addCondition('resep_print_id = :resep_print_id');
        $crite->order = 'no_urut';
        $crite->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
        /** @var ResepPrintDetails[] $resepPrintDetils */
        $resepPrintDetils = ResepPrintDetails::model()->findAll($crite);
        if (count($resepPrintDetils) == 0) {
            return false;
        }
        $urut = 1;
        foreach ($resepPrintDetils as $rpd) {
            $raw .= parent::addItem(number_format($urut, 0) . ".",
                $rpd->kode_item, $rpd->qty, $rpd->sat);
            $raw .= $newLine;
            $raw .= parent::addLeftRight($rpd->ketpot, "", $width);
            $raw .= $newLine;
//            if ($rpd->discrp != 0) {
//                $raw .= parent::addItemDiscReceipt('',
//                    number_format(-($rpd->discrp), 0, ',', '.'));
//                $raw .= $newLine;
//            }
            $urut++;
        }
        $raw .= parent::fillWithChar("-", $width);
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"), $width);
        $raw .= $newLine;
//        U::save_file(ReportPath . $this->s->doc_ref . '.txt', $raw);
        return $raw;
//        return base64_encode(chr(27) . chr(64) . parent::fillWithChar("-") . chr(27) . chr(105));
    }
}