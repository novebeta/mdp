<?php
class PrintResep extends PrintReceipt
{
    public function buildTxt($dokter = NULL, $tgl = NULL, $dokter_id = NULL)
    {
        $newLine = "\r\n";
	    $width = (PRINTER_STOCKER_U220) ? 40 : CHARLENGTHRECEIPT;
        $raw = parent::setCenter(SysPrefs::get_val('resep_header0'), $width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('resep_header1'), $width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('resep_header2'), $width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('resep_header3'), $width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('resep_header4'), $width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('resep_header5'), $width);
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        if ($this->resepPrint->doc_ref_resep != null) {
            $raw .= $newLine;
            $nama_dokter =  is_null($dokter) ? $this->resepPrint->dokter : $dokter;
            $tgl_resep = is_null($tgl) ? $this->resepPrint->date_ : $tgl;
            $raw .= parent::addHeaderResep("Dokter", $nama_dokter);
            $raw .= $newLine;
            if ($this->resepPrint->dokter || !is_null($dokter_id)){
            	$dktr_id = is_null($dokter_id) ? $this->resepPrint->salestrans->dokter_id : $dokter_id;
                $sip = Employee::model()->findByPk($dktr_id);
                $raw .= parent::addHeaderResep("SIP", $sip->no_ijin);
                $raw .= $newLine;
            }            
            $raw .= parent::addHeaderResep("No. Resep", $this->resepPrint->doc_ref_resep);
            $raw .= $newLine;
            $raw .= parent::addHeaderResep("Tanggal", sql2date($tgl_resep, "dd-MMM-yyyy"));
            $raw .= $newLine;
        } else {
            return false;
        }
        $raw .= $newLine;
        $criteria = new CDbCriteria();
        $criteria->addCondition('r_ = 1');
        $criteria->addCondition('resep_print_id = :resep_print_id');
        $criteria->order = 'no_resep';
        $criteria->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
        /** @var ResepPrintDetails[] $resepPrintDetils */
        $resepPrintDetils = ResepPrintDetails::model()->findAll($criteria);
        foreach ($resepPrintDetils as $rpd) {
            $raw .= parent::addItemCodeResepSat("R$rpd->no_resep /",
                number_format($rpd->qty, 1, ',', '.'), $rpd->sat);
            $raw .= $newLine;
            $cri = new CDbCriteria();
            $cri->addCondition('resep_print_details_id = :resep_print_details_id');
            $cri->order = 'no_urut';
            $cri->params = [':resep_print_details_id' => $rpd->resep_print_details_id];
            /** @var ResepPrintBahan[] $resepPrintBahan */
            $resepPrintBahan = ResepPrintBahan::model()->findAll($cri);
            foreach ($resepPrintBahan as $rpb) {
                $qty = number_format($rpd->qty, 0);
//                BELUM DITERAPIN
                if ($rpb->sat == "note" || $rpb->sat == "Note" || !$rpb->sat){
                    $raw .= parent::addResepBahan("", "     $rpb->nama_bahan", "", "", $width);
                    $raw .= $newLine;
                } else {
                    $raw .= parent::addResepBahan("", "$rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat, $width);
                    $raw .= $newLine;
                }
//                $raw .= parent::addResepBahan("$rpb->no_urut.", "$qty x $rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
//                $raw .= $newLine;
            }
            $raw .= $newLine;
        }
        $raw .= $newLine;
        $raw .= parent::addHeaderResep("Nama Pasien",$this->resepPrint->salestrans->customer->nama_customer);
        $raw .= $newLine;
        $raw .= parent::addHeaderResep("No. RM",$this->resepPrint->salestrans->customer->no_customer);
        $raw .= $newLine;
        $raw .= $newLine;
        if (is_null($tgl))
            $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"), $width);
        $raw .= $newLine;
        return $raw;
    }
//    public function buildTxt()
//    {
//        $newLine = "\r\n";
//        $raw = parent::setCenter($this->resepPrint->header0);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header1);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header2);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header3);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header4);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header5);
//        $raw .= $newLine;
////        $raw .= parent::fillWithChar("=");
//        $raw .= $newLine;
//        if ($this->resepPrint->doc_ref_resep != null) {
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("No. Resep", $this->resepPrint->doc_ref_resep);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Tanggal", sql2date($this->resepPrint->date_, "dd-MMM-yyyy"));
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Apt./AA", $this->resepPrint->apoteker);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("A/N. Resep", $this->resepPrint->pasien_name);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Alamat", $this->resepPrint->alamat);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Dokter", $this->resepPrint->dokter);
//            $raw .= $newLine;
//        } else {
//            return false;
//        }
//        $raw .= $newLine;
//        $criteria = new CDbCriteria();
//        $criteria->addCondition('r_ = 1');
//        $criteria->addCondition('resep_print_id = :resep_print_id');
//        $criteria->order = 'no_resep';
//        $criteria->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
//        /** @var ResepPrintDetails[] $resepPrintDetils */
//        $resepPrintDetils = ResepPrintDetails::model()->findAll($criteria);
//        foreach ($resepPrintDetils as $rpd) {
//            $raw .= parent::addItemCodeResepSat("R$rpd->no_resep /",
//                number_format($rpd->qty, 1, ',', '.'), $rpd->sat);
//            $raw .= $newLine;
//            $cri = new CDbCriteria();
//            $cri->addCondition('resep_print_details_id = :resep_print_details_id');
//            $cri->order = 'no_urut';
//            $cri->params = [':resep_print_details_id' => $rpd->resep_print_details_id];
//            /** @var ResepPrintBahan[] $resepPrintBahan */
//            $resepPrintBahan = ResepPrintBahan::model()->findAll($cri);
//            foreach ($resepPrintBahan as $rpb) {
//                $qty = number_format($rpd->qty, 0);
////                BELUM DITERAPIN
////                if ($rpb->sat == "note" || $rpb->sat == "Note" || !$rpb->sat){
////                    $raw .= parent::addResepBahan("", "     $rpb->nama_bahan", "", "");
////                    $raw .= $newLine;
////                } else {
////                    $raw .= parent::addResepBahan("", "$rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
////                    $raw .= $newLine;
////                }
//                $raw .= parent::addResepBahan("$rpb->no_urut.", "$qty x $rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
//                $raw .= $newLine;
//            }
//            $raw .= $newLine;
//        }
//        $raw .= $newLine;
//        if ($this->resepPrint->footer0 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer0);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer1 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer1);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer2 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer2);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer3 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer3);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer4 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer4);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer5 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer5);
//            $raw .= $newLine;
//        }
//        $raw .= $newLine;
//        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"));
//        $raw .= $newLine;
//        return $raw;
//    }


	public function pisahTxt($dokter = NULL, $tgl = NULL, $dokter_id = NULL)
	{
		$raw = '';
		$comm = Yii::app()->db->createCommand("SELECT sd.dokter_kmr FROM nscc_salestrans_details AS sd
			INNER JOIN nscc_resep_print_details AS pd ON pd.salestrans_details=sd.salestrans_details
			WHERE r_=1 AND pd.resep_print_id = :resep_print_id
			GROUP BY sd.dokter_kmr");
		$cnt = $comm->queryAll(true, array(
				':resep_print_id' => $this->resepPrint->resep_print_id)
		);

		$newLine = "\r\n";
		foreach ($cnt as $row) {
			$width = (PRINTER_STOCKER_U220) ? 40 : CHARLENGTHRECEIPT;
			$raw .= parent::setCenter(SysPrefs::get_val('resep_header0'), $width);
			$raw .= $newLine;
			$raw .= parent::setCenter(SysPrefs::get_val('resep_header1'), $width);
			$raw .= $newLine;
			$raw .= parent::setCenter(SysPrefs::get_val('resep_header2'), $width);
			$raw .= $newLine;
			$raw .= parent::setCenter(SysPrefs::get_val('resep_header3'), $width);
			$raw .= $newLine;
			$raw .= parent::setCenter(SysPrefs::get_val('resep_header4'), $width);
			$raw .= $newLine;
			$raw .= parent::setCenter(SysPrefs::get_val('resep_header5'), $width);
			$raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
			$raw .= $newLine;
			if ($this->resepPrint->doc_ref_resep != null) {
				$raw .= $newLine;

				if ($row["dokter_kmr"] == "")
					$nama_dokter = is_null($dokter) ? $this->resepPrint->dokter : $dokter;
				else {
					$sip = Employee::model()->findByPk($row['dokter_kmr']);
					$nama_dokter = $sip->nama_employee;
				}

				$tgl_resep = is_null($tgl) ? $this->resepPrint->date_ : $tgl;
				$raw .= parent::addHeaderResep("Dokter", $nama_dokter);
				$raw .= $newLine;
				if ($this->resepPrint->dokter || !is_null($dokter_id)) {
					if ($row["dokter_kmr"] == "")
					{
						$dktr_id = is_null($dokter_id) ? $this->resepPrint->salestrans->dokter_id : $dokter_id;
						$sip = Employee::model()->findByPk($dktr_id);
					}
					$raw .= parent::addHeaderResep("SIP", $sip->no_ijin);
					$raw .= $newLine;
				}
				$raw .= parent::addHeaderResep("No. Resep", $this->resepPrint->doc_ref_resep);
				$raw .= $newLine;
				$raw .= parent::addHeaderResep("Tanggal", sql2date($tgl_resep, "dd-MMM-yyyy"));
				$raw .= $newLine;
			} else {
				return false;
			}
			$raw .= $newLine;

			if ($row["dokter_kmr"] == "")
				$where = " (sd.dokter_kmr is null or sd.dokter_kmr = :dokter_kmr)";
			else
				$where = " sd.dokter_kmr = :dokter_kmr";

			$param = array(
				':resep_print_id' => $this->resepPrint->resep_print_id,
				':dokter_kmr' =>$row["dokter_kmr"]
			);

			$comm = Yii::app()->db->createCommand("SELECT * FROM nscc_salestrans_details AS sd
			INNER JOIN nscc_resep_print_details AS pd ON pd.salestrans_details=sd.salestrans_details
			WHERE r_=1 AND pd.resep_print_id = :resep_print_id AND $where");
			$resepPrintDetils = $comm->queryAll(true, $param);
			foreach ($resepPrintDetils as $rpd) {
				$raw .= parent::addItemCodeResepSat("R".$rpd["no_resep"]." /",
					number_format($rpd["qty"], 1, ',', '.'), $rpd["sat"]);
				$raw .= $newLine;
				$cri = new CDbCriteria();
				$cri->addCondition('resep_print_details_id = :resep_print_details_id');
				$cri->order = 'no_urut';
				$cri->params = [':resep_print_details_id' => $rpd["resep_print_details_id"]];
				/** @var ResepPrintBahan[] $resepPrintBahan */
				$resepPrintBahan = ResepPrintBahan::model()->findAll($cri);
				foreach ($resepPrintBahan as $rpb) {
					$qty = number_format($rpd["qty"], 0);
//                BELUM DITERAPIN
					if ($rpb->sat == "note" || $rpb->sat == "Note" || !$rpb["sat"]) {
						$raw .= parent::addResepBahan("", "     $rpb->nama_bahan", "", "", $width);
						$raw .= $newLine;
					} else {
						$raw .= parent::addResepBahan("", "$rpb->nama_bahan", number_format($rpb->qty / $qty, 2), $rpb->sat, $width);
						$raw .= $newLine;
					}
//                $raw .= parent::addResepBahan("$rpb->no_urut.", "$qty x $rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
//                $raw .= $newLine;
				}
				$raw .= $newLine;
			}
			$raw .= $newLine;
			$raw .= parent::addHeaderResep("Nama Pasien", $this->resepPrint->salestrans->customer->nama_customer);
			$raw .= $newLine;
			$raw .= parent::addHeaderResep("No. RM", $this->resepPrint->salestrans->customer->no_customer);
			$raw .= $newLine;
			$raw .= $newLine;
		}
		$raw .= $newLine;
		if (is_null($tgl))
			$raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"), $width);
		return $raw;
	}
}