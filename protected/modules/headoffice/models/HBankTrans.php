<?php

/**
 * This is the model class for table "{{bank_trans}}".
 *
 * The followings are the available columns in table '{{bank_trans}}':
 * @property string $bank_trans_id
 * @property integer $type_
 * @property string $trans_no
 * @property string $ref
 * @property string $tgl
 * @property string $amount
 * @property string $id_user
 * @property string $tdate
 * @property string $bank_id
 * @property string $store
 * @property integer $up
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property Bank $bank
 */
class HBankTrans extends PusatActiveRecord
{
	public function getDbConnection()
    {
        return self::getPusatDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bank_trans}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bank_trans_id, type_, trans_no, tgl, id_user, tdate, bank_id, store', 'required'),
			array('type_, up, visible', 'numerical', 'integerOnly'=>true),
			array('bank_trans_id', 'length', 'max'=>36),
			array('trans_no, ref, id_user, bank_id', 'length', 'max'=>50),
			array('amount', 'length', 'max'=>30),
			array('store', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bank_trans_id, type_, trans_no, ref, tgl, amount, id_user, tdate, bank_id, store, up, visible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bank_trans_id' => 'Bank Trans',
			'type_' => 'Type',
			'trans_no' => 'Trans No',
			'ref' => 'Ref',
			'tgl' => 'Tgl',
			'amount' => 'Amount',
			'id_user' => 'Id User',
			'tdate' => 'Tdate',
			'bank_id' => 'Bank',
			'store' => 'Store',
			'up' => 'Up',
			'visible' => 'Visible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bank_trans_id',$this->bank_trans_id,true);
		$criteria->compare('type_',$this->type_);
		$criteria->compare('trans_no',$this->trans_no,true);
		$criteria->compare('ref',$this->ref,true);
		$criteria->compare('tgl',$this->tgl,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('tdate',$this->tdate,true);
		$criteria->compare('bank_id',$this->bank_id,true);
		$criteria->compare('store',$this->store,true);
		$criteria->compare('up',$this->up);
		$criteria->compare('visible',$this->visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HBankTrans the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
