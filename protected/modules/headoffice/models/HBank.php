<?php

/**
 * This is the model class for table "{{bank}}".
 *
 * The followings are the available columns in table '{{bank}}':
 * @property string $bank_id
 * @property string $nama_bank
 * @property string $ket
 * @property string $account_code
 * @property string $store
 * @property integer $up
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property ChartMaster $accountCode
 * @property BankTrans[] $bankTrans
 * @property Kas[] $kases
 * @property Payment[] $payments
 * @property PelunasanUtang[] $pelunasanUtangs
 * @property TenderDetails[] $tenderDetails
 */
class HBank extends PusatActiveRecord
{
	public function getDbConnection()
    {
        return self::getPusatDbConnection();
    }
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bank}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bank_id, nama_bank, store', 'required'),
			array('up, visible', 'numerical', 'integerOnly'=>true),
			array('bank_id', 'length', 'max'=>50),
			array('nama_bank', 'length', 'max'=>100),
			array('ket', 'length', 'max'=>255),
			array('account_code', 'length', 'max'=>15),
			array('store', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bank_id, nama_bank, ket, account_code, store, up, visible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accountCode' => array(self::BELONGS_TO, 'ChartMaster', 'account_code'),
			'bankTrans' => array(self::HAS_MANY, 'BankTrans', 'bank_id'),
			'kases' => array(self::HAS_MANY, 'Kas', 'bank_id'),
			'payments' => array(self::HAS_MANY, 'Payment', 'bank_id'),
			'pelunasanUtangs' => array(self::HAS_MANY, 'PelunasanUtang', 'bank_id'),
			'tenderDetails' => array(self::HAS_MANY, 'TenderDetails', 'bank_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bank_id' => 'Bank',
			'nama_bank' => 'Nama Bank',
			'ket' => 'Ket',
			'account_code' => 'Account Code',
			'store' => 'Store',
			'up' => 'Up',
			'visible' => 'Visible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bank_id',$this->bank_id,true);
		$criteria->compare('nama_bank',$this->nama_bank,true);
		$criteria->compare('ket',$this->ket,true);
		$criteria->compare('account_code',$this->account_code,true);
		$criteria->compare('store',$this->store,true);
		$criteria->compare('up',$this->up);
		$criteria->compare('visible',$this->visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HBank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
