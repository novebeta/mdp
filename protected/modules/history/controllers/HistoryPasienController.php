<?php

class HistoryPasienController extends GxController {

public function actionCreate() {
$model = new HistoryPasien;
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['HistoryPasien'][$k] = $v;
}
$model->attributes = $_POST['HistoryPasien'];
$msg = "Data gagal disimpan.";

    if ($model->save()) {
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->id_pasien;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}

public function actionUpdate($id) {
$model = $this->loadModel($id, 'HistoryPasien');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['HistoryPasien'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['HistoryPasien'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->id_pasien;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->id_pasien));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'HistoryPasien')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


    public function actionIndex()
    {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        //$criteria = new CDbCriteria();
        $param = array();

        $where = "";
        $select = "SELECT *, YEAR(NOW()) - YEAR(tgllh) as 'usia'  FROM pasien";
        $order = " ORDER BY awal ";
        if (isset($_POST['telp'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= ' telp = :telp';
            $param[':telp'] = $_POST['telp'];
        }
        if (isset($_POST['nobase'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= ' nobase = :nobase';
            $param[':nobase'] = $_POST['nobase'];
        }
        if (isset($_POST['namacus'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= ' namacus like :namacus';
            $param[':namacus'] = "%" . $_POST['namacus'] . "%";
            $limit = 1000;
        }
        if (isset($_POST['alamat'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= ' alamat like :alamat';
            $param[':alamat'] = "%" . $_POST['alamat'] . "%";
            $limit = 1000;
        }

        $total_cmd = Yii::app()->dbnars->createCommand($select . $where . ' limit '. $limit);
        $total = $total_cmd->query($param)->rowCount;
        $comm = Yii::app()->dbnars->createCommand($select . $where . $order. ' limit '. $limit);
        $row = $comm->queryAll(true, $param);
        $this->renderJsonArrWithTotal($row, $total);

        /*if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
        (isset($_POST['limit']) && isset($_POST['start'])))
        {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        $criteria->params = $param;
        $model = HistoryPasien::model()->findAll($criteria);
        $total = HistoryPasien::model()->count($criteria);

        $this->renderJson($model, $total);*/

    }

}