<?php
Yii::import('application.modules.history.models._base.BaseHistoryPasien');

class HistoryPasien extends BaseHistoryPasien
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->id_pasien == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id_pasien = $uuid;
        }
        return parent::beforeValidate();
    }
}