<?php
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 11/28/18
 * Time: 9:55 AM
 */
class CashbankController extends Controller {
	public $layout = '//layouts/column1';
	public function actionIndex() {
		$arr = [];
		if ( isset( $_POST['from_'] ) ) {
			$arr_akun = explode(';',trim($_POST['account_code']));
			foreach ($arr_akun as $row) {
				$gl        = Yii::app()->db
					->createCommand( 'SELECT SUM(ngt.amount) 
				FROM nscc_gl_trans ngt 
				WHERE ngt.tran_date >= :from_  AND ngt.tran_date <= :to_ 
				AND ngt.visible = 1 AND ngt.account_code = :account_code;' )
					->queryScalar( [
						':from_'        => $_POST['from_'],
						':to_'          => $_POST['to_'],
						':account_code' => $row,
					] );
				$bank      = Yii::app()->db
					->createCommand( 'SELECT Sum(nbt.amount)
				FROM nscc_bank_trans AS nbt
				INNER JOIN nscc_bank nb ON nbt.bank_id = nb.bank_id
				WHERE nbt.tgl >= :from_  AND nbt.tgl <= :to_ 
				AND nbt.visible = 1 AND nb.account_code = :account_code;' )
					->queryScalar( [
						':from_'        => $_POST['from_'],
						':to_'          => $_POST['to_'],
						':account_code' => $row,
					] );
				$rawData[] = [
					'account_code' => $row,
					'gl'           => number_format($gl),
					'bank'         => number_format($bank),
					'selisih'      => number_format($gl - $bank)
				];
			}
			$dataProvider        = new CArrayDataProvider( $rawData, array(
				'keyField' => 'account_code',
			) );
			$arr['dataProvider'] = $dataProvider;
		}
		$this->render( 'index', $arr );
	}
	public function actionDetailsSelisih($type,$from_,$to_,$account_code){
		$arr = [];
		if($type == 'gl') {
			$rawData = Yii::app()->db
				->createCommand( 'SELECT ngt.type,ngt.type_no,ngt.amount
				FROM nscc_gl_trans AS ngt
				LEFT JOIN nscc_bank_trans AS nbt ON ngt.type = nbt.type_ AND ngt.type_no = nbt.trans_no
				WHERE ngt.tran_date >= :from_ AND ngt.tran_date <= :to_ 
				AND ngt.account_code = :account_code AND nbt.type_ IS NULL;' )
				->queryAll( true, [
					':from_'        => $from_,
					':to_'          => $to_,
					':account_code' => $account_code,
				] );
			$dataProvider        = new CArrayDataProvider( $rawData, array(
				'keyField' => 'type_no',
			) );
			$arr['dataProvider'] = $dataProvider;
		}elseif ($type == 'bank'){
			$rawData = Yii::app()->db
				->createCommand( 'SELECT ngt.type,ngt.type_no,ngt.amount
				FROM nscc_gl_trans AS ngt
				LEFT JOIN nscc_bank_trans AS nbt ON ngt.type = nbt.type_ AND ngt.type_no = nbt.trans_no
				WHERE ngt.tran_date >= :from_ AND ngt.tran_date <= :to_ 
				AND ngt.account_code = :account_code AND nbt.type_ IS NULL;' )
				->queryAll( true, [
					':from_'        => $from_,
					':to_'          => $to_,
					':account_code' => $account_code,
				] );
			$dataProvider        = new CArrayDataProvider( $rawData, array(
				'keyField' => 'type_no',
			) );
			$arr['dataProvider'] = $dataProvider;
		}
		$this->render( 'detailsselisih', $arr );
	}
}