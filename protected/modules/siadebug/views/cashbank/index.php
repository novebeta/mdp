<?php
/* @var $this DefaultController */
$this->breadcrumbs = array(
	$this->module->id,
);
?>
<div class="divTable">
    <div class="divTableBody">
        <div class="divTableRow">
            <div class="divTableCell form" style="width: 180px">
				<?php echo CHtml::beginForm(); ?>
                <div class="row">
					<?php echo CHtml::label( 'Dari Tanggal', 'id_from_' ); ?>
					<?php
					$this->widget( 'zii.widgets.jui.CJuiDatePicker', array(
						'name'        => 'from_',
						'value'       => date( 'Y-m-d' ),
						'options'     => array(
							'showButtonPanel' => true,
							'dateFormat'      => 'yy-mm-dd',
							//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
						),
						'htmlOptions' => array(
							'id'    => 'id_from_',
							'style' => ''
						),
					) );
					?>
                </div>
                <div class="row">
					<?php echo CHtml::label( 'Sampai Tanggal', 'id_to_' ); ?>
					<?php
					$this->widget( 'zii.widgets.jui.CJuiDatePicker', array(
						'name'        => 'to_',
						'value'       => date( 'Y-m-d' ),
						'options'     => array(
							'showButtonPanel' => true,
							'dateFormat'      => 'yy-mm-dd',
							//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
						),
						'htmlOptions' => array(
							'id'    => 'id_to_',
							'style' => ''
						),
					) );
					?>
                </div>
                <div class="row">
					<?php echo CHtml::label( 'Nomer Akun', 'id_akun_' ); ?>
					<?php echo CHtml::textField( 'account_code', '', [ 'id' => 'id_akun_' ] ) ?>
                </div>
                <div class="row submit">
					<?php echo CHtml::submitButton(); ?>
                </div>
				<?php echo CHtml::endForm(); ?>
            </div>
            <div class="divTableCell">
				<?php
				if ( isset( $dataProvider ) ) {
					$this->widget( 'zii.widgets.grid.CGridView', array(
						'dataProvider' => $dataProvider,
						'id'           => 'satu-grid',
						'columns'      => [
							[
								'header' => 'No Akun',
								'value'  => '$data["account_code"]'
							],
                            [
	                            'header' => 'Total Neraca',
	                            'value'  => '$data["gl"]'
                            ],
                            [
	                            'header' => 'Total Bank',
	                            'value'  => '$data["bank"]'
                            ],
                            [
	                            'header' => 'Selisih',
	                            'value'  => '$data["selisih"]'
                            ],
							[
								'class'=>'CButtonColumn',
								'template'=>'{gl} {bank}',
								'header' => 'Detil Selisih By',
								'buttons'=>
									[
									'gl' =>
										[
										'label'=>'Neraca',
//										'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
										'url'=>'Yii::app()->createUrl("/siadebug/cashbank/detailsselisih", 
										    ["type"=>"gl","from_"=>$_POST["from_"],"to_"=>$_POST["to_"],
										    "account_code"=>$_POST["account_code"]])',
										],
									'bank' =>
										[
										'label'=>'Bank',
										'url'=>'Yii::app()->createUrl("/siadebug/cashbank/detailsselisih", 
										    ["type"=>"bank","from_"=>$_POST["from_"],"to_"=>$_POST["to_"],
										    "account_code"=>$_POST["account_code"]])',
										],
									],
							],
						]
					) );
				}
				?>
            </div>
        </div>
    </div>
</div>

