<?php
Yii::import('application.modules.asset.models._base.BaseAssetDetailView');

class AssetDetailView extends BaseAssetDetailView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_trans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}