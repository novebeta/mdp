<?php

class DivisiKegiatanController extends GxController
{

    public function actionCreate()
    {

        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            $dk = new DivisiKegiatan();
           //=  $this->loadModel(, 'DivisiKegiatan');
//            $is_new = $_POST['mode'] == 0;
           //$mode = $_POST['mode'] ;
//
           //$abc=  $_POST['id'];
           // $aaa=  $this->loadModel($_POST['id'], 'DivisiKegiatan');


            $detils = CJSON::decode($_POST['detil']);
            $is_new = $_POST['mode'] == 0;
            $msg = "Data gagal disimpan.";
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new DivisiKegiatan : $this->loadModel($_POST['id'], 'DivisiKegiatan');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'DivisiKegiatan')) . "Fatal error, record not found.");
                }


                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(DANA_KEGIATAN_OUT);

                } else {
                    $dkd = new DivisiKegiatanDetail();
                    $docref = $model->doc_ref;
                    $type = DANA_KEGIATAN_OUT;
                    $type_no = $model->kegiatan_id;
                    //$dk->delete_divisi_kegiatan($type_no);
                    $dkd->delete_kegiatan_detail($type_no);
                    $dk->delete_divisi_bank_trans($type, $type_no);
                    $dk->delete_divisi_gl_trans($type, $type_no);



                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['DivisiKegiatan'][$k] = $v;
                }
                $_POST['DivisiKegiatan']['doc_ref']= $docref ;
                //$_POST['DivisiKegiatan']['total_biaya']= $_POST['DivisiKegiatan']['total_biaya'];
                $_POST['DivisiKegiatan']['arus'] = -1;
                 $_POST['DivisiKegiatan']['total_biaya_real']=-$_POST['DivisiKegiatan']['total_biaya'];
                 // $_POST['DivisiKegiatan']['total_kredit']=$_POST['DivisiKegiatan']['total_biaya'];

                $model->attributes = $_POST['DivisiKegiatan'];
                $abc = $_POST['DivisiKegiatan'];
                $addddddd = $model->kegiatan_id;

                $limit = DivisiBank::get_minimal_dana($model->bank_id);
                $balance = DivisiBankTrans::get_balance_bank($model->bank_id, $model->tgl);

                if ((($balance - abs($model->total_biaya_real)) < $limit) && $model->total_biaya_real < 0) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Dana')) . "Insufficient funds");
                }


                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($model));
                }

                //$abbbbbbbb = $model->kegiatan_id;
                if ($is_new) {
                    $ref->save(DANA_KEGIATAN_OUT, $model->kegiatan_id, $docref);
                }
                //ini----------------------------------------------------------------
             //   $aa=$model->bank->account_code;

                //--------------------------Bank Trans-------------------------------------

               // $dk = new DivisiKegiatan();
                // ---------------------add_bank_trans-----------------
                $dk->add_divisi_bank_trans(DANA_KEGIATAN_OUT,$model->kegiatan_id,$model->bank_id,$docref,$model->tgl,-$model->total_biaya);
                //------------------------- GL Trans----------------------------------------
                $dk->add_divisi_gl_trans(DANA_KEGIATAN_OUT,$model->kegiatan_id,$model->tgl, $model->bank->account_code,$model->note,-$model->total_biaya,0);
                $dk->add_divisi_gl_trans(DANA_KEGIATAN_OUT,$model->kegiatan_id,$model->tgl, $model->account_code,$model->note,$model->total_biaya,1);

                /*
                * detail
                */
                foreach ($detils as $detil) {
                    if(strpos($detil['item_name'], "'")){
                        echo CJSON::encode(array(
                            'success' => false,
                            'msg' => "Tidak boleh ada apostrophe ( ' ) pada note."
                        ));
                        Yii::app()->end();
                    }
                    $kegiatan_detail = new DivisiKegiatanDetail();
                    //$_POST['DivisiKegiatanDetail']['kegiatan_detail_id'] = $detil['item_name'];
                    $_POST['DivisiKegiatanDetail']['kegiatan_id'] = $model->kegiatan_id;
                    $_POST['DivisiKegiatanDetail']['item_name'] = $detil['item_name'];
                    $_POST['DivisiKegiatanDetail']['total'] = -$detil['total'];
                    $_POST['DivisiKegiatanDetail']['store'] = STOREID;

                    $kegiatan_detail->attributes=$_POST['DivisiKegiatanDetail'];

                    if (!$kegiatan_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Kegiatan Detail')) . CHtml::errorSummary($kegiatan_detail));
                    }

                }


                $status = true;
                $msg = "data berhasil disimpan!";
                $transaction->commit();
            }  catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

           // app()->db->autoCommit = true;

            echo CJSON::encode(array(
                'success' => $status,
               // 'id' => $docref,
                'msg' => $msg//,
                //'print' => $print
            ));
            Yii::app()->end();
//            $_POST['DivisiKegiatan']['tdate']= '1' ;
//
//
//            $a = $_POST['DivisiKegiatan'];
//            //$_POST['DivisiKegiatan'];
//            $msg = "Data gagal disimpan.";
//
//            if ($model->save()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->kegiatan_id;
//            } else {
//                $msg .= " " . implode(", ", $model->getErrors());
//                $status = false;
//            }
//
//            echo CJSON::encode(array(
//                'success' => $status,
//                'msg' => $msg));
//            Yii::app()->end();

        }

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'DivisiKegiatan');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['DivisiKegiatan'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['DivisiKegiatan'];








            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kegiatan_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kegiatan_id));
            }
        }


    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {



            $msg = 'Data berhasil dihapus.';
            $status = true;


            try {

                $model = $this->loadModel($id, 'DivisiKegiatan');
                $type = DANA_KEGIATAN_OUT;

                $type_no = $model->kegiatan_id;
                $tgl = $model->tgl;


                if (Tender::is_exist($tgl)) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => 'Tender Declaration already created'
                    ));
                    Yii::app()->end();
                }
               // $this->loadModel($id, 'DivisiKegiatan')->delete();


                $dk = new DivisiKegiatan();
                $dkd = new DivisiKegiatanDetail();
                DivisiKegiatan::model()->updateAll( array(
                    'visible' => 0
                ), 'kegiatan_id = :kegiatan_id', array( ':kegiatan_id' => $type_no ) );
                $dkd->delete_kegiatan_detail($type_no);
                $dk->delete_divisi_bank_trans($type, $type_no);
                $dk->delete_divisi_gl_trans($type, $type_no);

            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex()
    {

        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->addCondition("visible = 1");
        $criteria->order = 'doc_ref DESC';

        $model = DivisiKegiatan::model()->findAll($criteria);
        $total = DivisiKegiatan::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}