<?php

class DivisiKasController extends GxController
{

    public function actionCreate()
    {
       // $model = new DivisiKas;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            $dk = new DivisiKegiatan();
            $is_new = $_POST['mode'] == 0;
            $is_in = $_POST['arus'] == 1;
            $msg = "Data gagal disimpan.";

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new DivisiKas : $this->loadModel($_POST['id'], 'DivisiKas');

                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($is_in ? DANA_IN : DANA_OUT);
                } else {
                    $docref = $model->doc_ref;
                    DivisiKasDetail::delete__($model->kas_id);
                    $type = $model->arus == 1 ? DANA_IN : DANA_OUT;
                    $type_no = $model->kas_id;
                    $dk->delete_divisi_bank_trans($type, $type_no);
                    $dk->delete_divisi_gl_trans($type, $type_no);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['DivisiKas'][$k] = $v;
                }
                $total = $is_in ? $_POST['DivisiKas']['total'] : -$_POST['DivisiKas']['total'];
                $_POST['DivisiKas']['doc_ref'] = $docref;
                $_POST['DivisiKas']['total'] = $total;


                $model->attributes = $_POST['DivisiKas'];

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Divisi Kas')) . CHtml::errorSummary($model));
                }


               // $a = $model->kas_id;
                $divisikasdetail = new DivisiKasDetail;
                $_POST['DivisiKasDetail']['kas_id'] = $model->kas_id;
                $_POST['DivisiKasDetail']['item_name'] = $model->keperluan;
                $_POST['DivisiKasDetail']['total'] = $model->total;
                $_POST['DivisiKasDetail']['account_code'] = $model->bank->account_code;
                $_POST['DivisiKasDetail']['store'] = STOREID;
                $divisikasdetail->attributes = $_POST['DivisiKasDetail'];


                $limit = DivisiBank::get_minimal_dana($model->bank_id);
                $balance = DivisiBankTrans::get_balance_bank($model->bank_id, $model->tgl);
                $abbb = abs($model->total);
                if ((($balance - abs($model->total)) < $limit) && $model->total < 0) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Dana')) . "Insufficient funds");
                }


                if (!$divisikasdetail->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Divisi Kas Detail')) . CHtml::errorSummary($divisikasdetail));
                }

                if ($is_new) {
                    //  $ref->save(DANA_KEGIATAN_OUT, $model->kegiatan_id, $docref);
                    $ref->save($is_in ? DANA_IN : DANA_OUT, $model->kas_id, $docref);
                }

                $dk->add_divisi_bank_trans($is_in ? DANA_IN : DANA_OUT,$model->kas_id,$model->bank_id,$docref,$model->tgl,$model->total);
                $dk->add_divisi_gl_trans($is_in ? DANA_IN : DANA_OUT,$model->kas_id,$model->tgl, $model->bank->account_code,$model->keperluan,$model->total,0);
               // $dk->add_divisi_gl_trans(DANA_KEGIATAN_OUT,$model->kas_id,$model->tgl, $model->account_code,$model->note,$model->total_biaya,1);

                $status = true;
                $msg = "data berhasil disimpan!";
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
//            $msg = "Data gagal disimpan.";
//
//            if ($model->save()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->kas_id;
//            } else {
//                $msg .= " " . implode(", ", $model->getErrors());
//                $status = false;
//            }
//
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();

        }

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'DivisiKas');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['DivisiKas'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['DivisiKas'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'DivisiKas')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();


        if (isset ($_POST['arus']) && $_POST['arus'] == 'abc'){
            $aaa= "a";
        }
//
        if (isset ($_POST['arus'])) {
            $criteria->select = "
                    kas_id,
                    doc_ref,
                    no_kwitansi,
                    keperluan,
                    IF(total>=0,total,-total) total,
                    bank_id,tgl,user_id,tdate,type_,store,arus,

                    IF(amount>=0,amount,-amount) amount
                ";
            $criteria->addCondition(($_POST['arus'] == 'masuk') ? "arus = 1" : "arus = -1");
       //     $criteria->addCondition("visible = 1 AND tgl = :tgl");
        }

        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->order = 'doc_ref DESC';
        $model = DivisiKas::model()->findAll($criteria);
        $total = DivisiKas::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}