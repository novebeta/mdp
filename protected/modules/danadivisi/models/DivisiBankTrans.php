<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiBankTrans');

class DivisiBankTrans extends BaseDivisiBankTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->bank_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bank_trans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
//        if (!$this->getIsNewRecord()) {
//            if (substr($this->doc_ref, 0, strlen(STOREID)) != STOREID) {
//                throw new Exception('Untuk edit data cabang harus dari server cabang. Terima Kasih.');
//            }
//        }
//        if($this->tgl != null){
//            if($this->tgl < U::getResDate()){
//                throw new Exception('error pembatasan tanggal.');
//            }
//        }
        return parent::beforeValidate();
    }
    public static function get_balance_bank( $id, $date ) {
        $comm = Yii::app()->db->createCommand( "
        SELECT IFNULL(SUM(nbt.amount),0) AS total
        FROM nscc_divisi_bank_trans nbt
        WHERE nbt.visible = 1 AND nbt.bank_id = :bank_id AND nbt.tgl <= :tgl" );
        return $comm->queryScalar( array( ':bank_id' => $id, ':tgl' => $date ) );
    }
}