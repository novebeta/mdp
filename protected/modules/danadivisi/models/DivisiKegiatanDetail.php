<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiKegiatanDetail');

class DivisiKegiatanDetail extends BaseDivisiKegiatanDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->kegiatan_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kegiatan_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function delete_kegiatan_detail($kegiatan_id) {
        DivisiKegiatanDetail::model()->updateAll( array(
            'visible' => 0
        ), 'kegiatan_id = :kegiatan_id', array( ':kegiatan_id' => $kegiatan_id ) );
    }
    public static function get_details($kegiatan_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                kegiatan_detail_id,
                kegiatan_id,
                
                 if(total < 0,-total,0) as total,
                item_name,
              
                store
            FROM nscc_divisi_kegiatan_detail
            WHERE kegiatan_id = :kegiatan_id AND visible = 1");
        return $comm->queryAll(true, array(':kegiatan_id' => $kegiatan_id));
    }
}