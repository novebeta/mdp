<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiBank');

class DivisiBank extends BaseDivisiBank
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->bank_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bank_id = $uuid;
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
    public static function get_minimal_dana($bank_id)
    {
        $comm = Yii::app()->db->createCommand("SELECT minimum FROM nscc_divisi_bank WHERE bank_id = :bank_id");
        return $comm->queryScalar(array(':bank_id' => $bank_id));
    }
}