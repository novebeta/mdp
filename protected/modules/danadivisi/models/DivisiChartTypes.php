<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiChartTypes');

class DivisiChartTypes extends BaseDivisiChartTypes
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->chart_types_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->chart_types_id = $uuid;
        }
        return parent::beforeValidate();
    }
}