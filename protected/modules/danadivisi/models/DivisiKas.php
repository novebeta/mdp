<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiKas');

class DivisiKas extends BaseDivisiKas
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->kas_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_id = $uuid;
        }

        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }

        return parent::beforeValidate();
    }


}