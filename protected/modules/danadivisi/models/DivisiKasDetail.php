<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiKasDetail');

class DivisiKasDetail extends BaseDivisiKasDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->kas_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function delete__($kas_id)
    {
        DivisiKasDetail::model()->updateAll(array('visible' => 0), 'kas_id = :kas_id', array(':kas_id' => $kas_id));
    }
}