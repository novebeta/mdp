<?php

/**
 * This is the model base class for the table "{{divisi_bank_trans}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "DivisiBankTrans".
 *
 * Columns in table "{{divisi_bank_trans}}" available as properties of the model,
 * followed by relations of table "{{divisi_bank_trans}}" available as properties of the model.
 *
 * @property string $bank_trans_id
 * @property integer $type_
 * @property string $trans_no
 * @property string $ref
 * @property string $tgl
 * @property double $amount
 * @property string $id_user
 * @property string $tdate
 * @property string $bank_id
 * @property string $store
 * @property integer $up
 * @property integer $visible
 *
 * @property DivisiBank $bank
 */
abstract class BaseDivisiBankTrans extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{divisi_bank_trans}}';
	}

	public static function representingColumn() {
		return 'trans_no';
	}

	public function rules() {
		return array(
			array('bank_trans_id, type_, trans_no, tgl, id_user, tdate, bank_id, store', 'required'),
			array('type_, up, visible', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('bank_trans_id', 'length', 'max'=>36),
			array('trans_no, ref, id_user, bank_id', 'length', 'max'=>50),
			array('store', 'length', 'max'=>20),
			array('ref, amount, up, visible', 'default', 'setOnEmpty' => true, 'value' => null),
			array('bank_trans_id, type_, trans_no, ref, tgl, amount, id_user, tdate, bank_id, store, up, visible', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'bank' => array(self::BELONGS_TO, 'DivisiBank', 'bank_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'bank_trans_id' => Yii::t('app', 'Bank Trans'),
			'type_' => Yii::t('app', 'Type'),
			'trans_no' => Yii::t('app', 'Trans No'),
			'ref' => Yii::t('app', 'Ref'),
			'tgl' => Yii::t('app', 'Tgl'),
			'amount' => Yii::t('app', 'Amount'),
			'id_user' => Yii::t('app', 'Id User'),
			'tdate' => Yii::t('app', 'Tdate'),
			'bank_id' => Yii::t('app', 'Bank'),
			'store' => Yii::t('app', 'Store'),
			'up' => Yii::t('app', 'Up'),
			'visible' => Yii::t('app', 'Visible'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('bank_trans_id', $this->bank_trans_id, true);
		$criteria->compare('type_', $this->type_);
		$criteria->compare('trans_no', $this->trans_no, true);
		$criteria->compare('ref', $this->ref, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('id_user', $this->id_user, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('up', $this->up);
		$criteria->compare('visible', $this->visible);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}