<?php

/**
 * This is the model base class for the table "{{divisi}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Divisi".
 *
 * Columns in table "{{divisi}}" available as properties of the model,
 * followed by relations of table "{{divisi}}" available as properties of the model.
 *
 * @property string $divisi_id
 * @property string $name
 * @property integer $up
 * @property string $seq
 * @property integer $hide
 *
 * @property DivisiChartMaster[] $divisiChartMasters
 */
abstract class BaseDivisi extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{divisi}}';
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('divisi_id', 'required'),
			array('up, hide', 'numerical', 'integerOnly'=>true),
			array('divisi_id', 'length', 'max'=>36),
			array('name', 'length', 'max'=>60),
			array('seq', 'length', 'max'=>10),
			array('name, up, seq, hide', 'default', 'setOnEmpty' => true, 'value' => null),
			array('divisi_id, name, up, seq, hide', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'divisiChartMasters' => array(self::HAS_MANY, 'DivisiChartMaster', 'divisi_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'divisi_id' => Yii::t('app', 'Divisi'),
			'name' => Yii::t('app', 'Name'),
			'up' => Yii::t('app', 'Up'),
			'seq' => Yii::t('app', 'Seq'),
			'hide' => Yii::t('app', 'Hide'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('divisi_id', $this->divisi_id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('up', $this->up);
		$criteria->compare('seq', $this->seq, true);
		$criteria->compare('hide', $this->hide);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}