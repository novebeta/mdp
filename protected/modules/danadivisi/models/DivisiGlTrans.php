<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiGlTrans');

class DivisiGlTrans extends BaseDivisiGlTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->counter == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->counter = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
//        if (!$this->getIsNewRecord()) {
//            if (substr($this->doc_ref, 0, strlen(STOREID)) != STOREID) {
//                throw new Exception('Untuk edit data cabang harus dari server cabang. Terima Kasih.');
//            }
//        }
//        if($this->tgl != null){
//            if($this->tgl < U::getResDate()){
//                throw new Exception('error pembatasan tanggal.');
//            }
//        }
        return parent::beforeValidate();
    }
}