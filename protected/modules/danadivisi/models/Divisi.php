<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisi');

class Divisi extends BaseDivisi
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->divisi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->divisi_id = $uuid;
        }
        return parent::beforeValidate();
    }
}