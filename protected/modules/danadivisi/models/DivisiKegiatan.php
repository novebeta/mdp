<?php
Yii::import('application.modules.danadivisi.models._base.BaseDivisiKegiatan');

class DivisiKegiatan extends BaseDivisiKegiatan
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->kegiatan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kegiatan_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
//        if (!$this->getIsNewRecord()) {
//            if (substr($this->doc_ref, 0, strlen(STOREID)) != STOREID) {
//                throw new Exception('Untuk edit data cabang harus dari server cabang. Terima Kasih.');
//            }
//        }
//        if($this->tgl != null){
//            if($this->tgl < U::getResDate()){
//                throw new Exception('error pembatasan tanggal.');
//            }
//        }
        return parent::beforeValidate();
    }

    public function add_divisi_gl_trans($type, $type_no,$date_, $account_code, $memo_, $amount, $cf)
    {

        $divisi_gl_trans = new DivisiGlTrans();
        $divisi_gl_trans->type = $type;
        $divisi_gl_trans->type_no = $type_no;
        $divisi_gl_trans->tran_date = $date_;
        $divisi_gl_trans->account_code = $account_code;//$model->account_code;
        $divisi_gl_trans->memo_ = $memo_;
        //$divisi_gl_trans->id_user = $person_id;
        $divisi_gl_trans->amount = $amount;
        $divisi_gl_trans->cf = $cf;
        // $divisi_gl_trans->store = $store;
        if (!$divisi_gl_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => "Receive item detail ")) . CHtml::errorSummary($divisi_gl_trans));
        }
    }

    public function add_divisi_bank_trans($type_, $trans_no, $bank_id, $ref, $tgl, $amount)
    {
        $divisi_bank_trans = new DivisiBankTrans;
        $divisi_bank_trans->type_ = $type_;
        $divisi_bank_trans->trans_no = $trans_no;
        $divisi_bank_trans->bank_id = $bank_id;
        $divisi_bank_trans->ref = $ref;
        $divisi_bank_trans->tgl = $tgl;
        $divisi_bank_trans->amount = $amount;
        //$divisi_bank_trans->id_user = $person_id;
        //$divisi_bank_trans->store = $store;
        if (!$divisi_bank_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($divisi_bank_trans));
        }
    }

    public function delete_divisi_kegiatan($kegiatan_id) {
        DivisiKegiatan::model()->updateAll( array(
            'visible' => 0,
            'up'      => 0
        ), 'kegiatan_id = :kegiatan_id', array( ':kegiatan_id' => $kegiatan_id) );
    }

    public function delete_divisi_bank_trans($type, $type_no ) {
        DivisiBankTrans::model()->updateAll( array(
            'visible' => 0,
            'up'      => 0
        ), 'type_ = :type AND trans_no = :type_no', array( ':type' => $type, ':type_no' => $type_no ) );
    }
    public function delete_divisi_gl_trans($type, $type_no ) {
        DivisiGlTrans::model()->updateAll( array(
            'visible' => 0,
            'up'      => 0
        ), 'type = :type AND type_no = :type_no', array( ':type' => $type, ':type_no' => $type_no ) );
    }
}