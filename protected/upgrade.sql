/*
Created On      : 12-Feb-2022
Description     : add total_bayar upon pay service
*/
ALTER TABLE `nscc_bp_bayar_jasa` ADD COLUMN `total_bayar` decimal(15, 2) NULL DEFAULT 0.00 AFTER `pph23`;
UPDATE `nscc_bp_bayar_jasa` SET `total_bayar` = `total`;