<?php
Yii::import('application.models._base.BaseNsccVoucherOnline');

class NsccVoucherOnline extends BaseNsccVoucherOnline
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id = $uuid;
        }
        return parent::beforeValidate();
    }
}