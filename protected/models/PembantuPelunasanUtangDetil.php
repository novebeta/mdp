<?php

Yii::import('application.models._base.BasePembantuPelunasanUtangDetil');

class PembantuPelunasanUtangDetil extends BasePembantuPelunasanUtangDetil {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function get_utang($supplier_id, $store) {
        $comm = Yii::app()->db->createCommand("
        SELECT
            nti.invoice_journal_id,
            nti.tgl,
            nti.doc_ref_other no_faktur,
            nti.total nilai,
            nti.doc_ref,
            nti.total - COALESCE (- nti1.total, 0) - COALESCE (SUM(npud.kas_dibayar), 0) AS sisa
        FROM
            nscc_invoice_journal nti
        LEFT JOIN (
            SELECT
                npud1.*
            FROM
                nscc_pembantu_pelunasan_utang_detil npud1
            LEFT JOIN nscc_pembantu_pelunasan_utang npu ON npu.pembantu_pelunasan_utang_id = npud1.pembantu_pelunasan_utang_id
            WHERE
                npu.void_user_id IS NULL
        ) npud ON nti.invoice_journal_id = npud.invoice_journal_id
        LEFT JOIN nscc_invoice_journal nti1 ON nti.doc_ref = nti1.doc_ref_other
        WHERE
            nti.supplier_id = :supplier_id
        AND nti.p = 1
        GROUP BY
            nti.invoice_journal_id
        HAVING
            sisa != 0
        ");
        return $comm->queryAll(true, array(
               ':supplier_id' => $supplier_id
        ));
    }

    public function beforeValidate() {
        if ($this->pembantu_pelunasan_utang_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pembantu_pelunasan_utang_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }

}
