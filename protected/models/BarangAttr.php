<?php
Yii::import('application.models._base.BaseBarangAttr');

class BarangAttr extends BaseBarangAttr
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->barang_attr_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_attr_id = $uuid;
        }
        return parent::beforeValidate();
    }
}