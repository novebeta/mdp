<?php
Yii::import('application.models._base.BaseBarangView');

class BarangView extends BaseBarangView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function primaryKey()
	{
		return 'barang_id';
	}
}