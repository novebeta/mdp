<?php
Yii::import('application.models._base.BaseTagBarang');

class TagBarang extends BaseTagBarang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->tag_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tag_barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
}