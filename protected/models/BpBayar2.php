<?php
Yii::import( 'application.models._base.BaseBpBayar2' );
class BpBayar2 extends BaseBpBayar2 {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->bp_bayar_id == null ) {
			$command           = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid              = $command->queryScalar();
			$this->bp_bayar_id = $uuid;
		}
		if ( $this->id_user == null ) {
			$this->id_user = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
}