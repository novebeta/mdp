<?php
Yii::import('application.models._base.BaseAssignEmployees');

class AssignEmployees extends BaseAssignEmployees
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->assign_employee_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->assign_employee_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->tgl == null) {
            $this->tgl= date('Y-m-d');
        }
        return parent::beforeValidate();
    }
}