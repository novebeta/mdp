<?php
Yii::import( 'application.models._base.BaseSalesMdp' );
class SalesMdp extends BaseSalesMdp {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->sales_id == null ) {
			$command        = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid           = $command->queryScalar();
			$this->sales_id = $uuid;
		}
		if ( $this->store_kode == null ) {
			$this->store_kode = STOREID;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public function createSalesRef(){

	}
}