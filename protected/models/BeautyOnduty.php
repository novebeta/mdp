<?php
Yii::import('application.models._base.BaseBeautyOnduty');
class BeautyOnduty extends BaseBeautyOnduty
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->beauty_onduty_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->beauty_onduty_id = $uuid;
        }
        if ($this->tgl == null) {
            $this->tgl = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }
}