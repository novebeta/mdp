<?php
Yii::import('application.models._base.BaseUlptView');

class UlptView extends BaseUlptView
{
	public function primaryKey()
	{
		return 'ulpt_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}