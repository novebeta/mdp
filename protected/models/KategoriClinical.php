<?php
Yii::import('application.models._base.BaseKategoriClinical');

class KategoriClinical extends BaseKategoriClinical
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->kategori_clinical_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kategori_clinical_id = $uuid;
        }
        return parent::beforeValidate();
    }
}