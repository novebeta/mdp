<?php
Yii::import('application.models._base.BaseResync');

class Resync extends BaseResync
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->id_resync == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id_resync = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function add($trans_id, $type, $command, $action, $startdate, $enddate, $status, $store){

	    $chck = self::model()->findByAttributes(['trans_id' => $trans_id, 'status' => '1']);
	    if(!$chck)
        {
            $res = new self();
            $res->trans_id = $trans_id;
            $res->store = $store;
            $res->type = $type;
            $res->command = $command;
            $res->action = $action;
            $res->startdate = $startdate;
            $res->enddate = $enddate;
            $res->status = $status;
            if (!$res->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Resync')) . CHtml::errorSummary($res));
            else
                return $res;
        }

    }
}