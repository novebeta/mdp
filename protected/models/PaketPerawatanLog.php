<?php
Yii::import('application.models._base.BasePaketPerawatanLog');

class PaketPerawatanLog extends BasePaketPerawatanLog
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_perawatan_log_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_perawatan_log_id = $uuid;
        }
        return parent::beforeValidate();
    }

	public static function gettrans($cust, $barang, $max, $tgl, $expired)
    {
	    $param[':cust'] = $cust;
	    $param[':barang'] = $barang;
	    $param[':max'] = $max;
	    $param[':tgl'] = $tgl;
	    $param[':expired'] = $expired;

	    $comm = Yii::app()->db->createCommand("SELECT pl.*, sum(pt.bruto-pt.total_pot) as net, pt.dokter_id FROM nscc_paket_perawatan_log AS pl
			INNER JOIN nscc_paket_perawatan_log_trans as pt ON pt.paket_perawatan_log_id=pl.paket_perawatan_log_id
			LEFT JOIN nscc_paket_perawatan_log_trans as pt_ret ON pt.salestrans_id=pt_ret.return_salestrans_id AND pt.paket_perawatan_log_id=pt_ret.paket_perawatan_log_id
			WHERE pl.barang_id=:barang AND pl.customer_id=:cust AND pt.qty > 0
				AND pt_ret.paket_perawatan_log_trans_id is null 
				AND pl.awal_trans <= :tgl AND pl.awal_trans >= DATE_SUB(:tgl, INTERVAL :expired DAY)
			GROUP BY pl.paket_perawatan_log_id
			HAVING COUNT(pl.paket_perawatan_log_id) < :max");

//	    $comm = Yii::app()->db->createCommand("SELECT pl.* FROM nscc_paket_perawatan_log AS pl
//			INNER JOIN nscc_paket_perawatan_log_trans as pt ON pt.paket_perawatan_log_id=pl.paket_perawatan_log_id
//			LEFT JOIN nscc_paket_perawatan_log_trans as pt_ret ON pt.salestrans_id=pt_ret.return_salestrans_id
//			WHERE pl.barang_id=:barang AND pl.customer_id=:cust AND pl.jumlah_trans<:max AND pt.qty > 0
//				AND pt_ret.paket_perawatan_log_trans_id is null AND pl.awal_trans <= :tgl
//			GROUP BY pl.paket_perawatan_log_id
//			HAVING COUNT(pl.paket_perawatan_log_id) < :max");

	    return $comm->queryRow(true, $param);
    }

    public static function maxtrans($cust, $barang, $max, $days)
    {
	    $param[':cust'] = $cust;
	    $param[':barang'] = $barang;
	    $param[':day'] = $days;
	    $param[':max'] = $max;

	    $comm = Yii::app()->db->createCommand("SELECT pl.* FROM nscc_paket_perawatan_log AS pl
			LEFT JOIN nscc_paket_perawatan_log AS pr ON pl.salestrans_id=pr.return_salestrans_id
			WHERE pl.barang_id=:barang AND pl.customer_id=:cust AND pr.salestrans_id is null AND pl.qty > 0
			ORDER BY pl.tgl DESC");
	    return $comm->queryAll(true, $param);
    }
}