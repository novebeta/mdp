<?php
Yii::import('application.models._base.BasePaketDetails');

class PaketDetails extends BasePaketDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}