<?php
Yii::import('application.models._base.BaseCustomerBp');

class CustomerBp extends BaseCustomerBp
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->customer_bp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_bp_id = $uuid;
        }
        return parent::beforeValidate();
    }
}