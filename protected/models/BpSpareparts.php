<?php
Yii::import('application.models._base.BaseBpSpareparts');

class BpSpareparts extends BaseBpSpareparts
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate(){
		if ($this->sparepart_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->sparepart_id = $uuid;
		}
		return parent::beforeValidate();
	}
}