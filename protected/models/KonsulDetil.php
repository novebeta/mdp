<?php
Yii::import('application.models._base.BaseKonsulDetil');
class KonsulDetil extends BaseKonsulDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->konsul_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->konsul_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}