<?php
Yii::import('application.models._base.BaseVoucherOnlineView');

class VoucherOnlineView extends BaseVoucherOnlineView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this-> == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this-> = $uuid;
        }
        return parent::beforeValidate();
    }
}