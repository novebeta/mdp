<?php
Yii::import('application.models._base.BaseKasDetail');
class KasDetail extends BaseKasDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->kas_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function delete__($kas_id)
    {
        KasDetail::model()->updateAll(array('visible' => 0), 'kas_id = :kas_id', array(':kas_id' => $kas_id));
    }
    public static function get_details($kas_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                kas_detail_id,
                kas_id,
                if(total >= 0,total,0) debit,
                if(total < 0,-total,0) kredit,
                item_name,
                total,
                account_code,
                store
            FROM nscc_kas_detail
            WHERE kas_id = :kas_id AND visible = 1");
        return $comm->queryAll(true, array(':kas_id' => $kas_id));
    }

    /**
     * After save attributes
     */
    /* protected function afterSave() {
        parent::afterSave();
        U::runCommand('kasdetail', '--id=' . $this->kas_detail_id, 'protected/runtime/kasdetail_'.$this->kas_id.'.log');             
            
    } */
}