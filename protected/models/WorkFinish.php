<?php
Yii::import( 'application.models._base.BaseWorkFinish' );
class WorkFinish extends BaseWorkFinish {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function primaryKey() {
		return 'sales_id';
	}
}