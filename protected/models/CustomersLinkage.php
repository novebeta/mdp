<?php
Yii::import('application.models._base.BaseCustomersLinkage');

class CustomersLinkage extends BaseCustomersLinkage
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->linkage_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->linkage_id = $uuid;
        }
        return parent::beforeValidate();
    }
}