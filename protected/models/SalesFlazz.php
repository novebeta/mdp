<?php
Yii::import('application.models._base.BaseSalesFlazz');
class SalesFlazz extends BaseSalesFlazz
{

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function beforeValidate(){
        if ($this->sales_flazz_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_flazz_id = $uuid;
        }
	    if ( $this->tdate == null ) {
		    $this->tdate = new CDbExpression( 'NOW()' );
	    }
	    if ( $this->user_id == null ) {
		    $this->user_id = Yii::app()->user->getId();
	    }
        return parent::beforeValidate();
    }
	public function getTotalBayar(){
		$command = $this->dbConnection->createCommand("SELECT SUM(amount) FROM 
		`nscc_sales_flazz_bayar` WHERE sales_flazz_id = :sales_flazz_id;");
        $total = $command->queryScalar([':sales_flazz_id'=>$this->sales_flazz_id]);
		if($total === false){
			$total = 0;
		}
		if($total == 0 && $this->lunas != null){
			$total = floatval($this->total);
		}
		return $total;
	}
	public function getKurangBayar(){
		$total = floatval($this->total);
		$sudahBayar = $this->getTotalBayar();
		return ($total - $sudahBayar);
	}

	public function lapTransMerdeka($dtFrom, $dtTo) {
		$query = $this->dbConnection->createCommand( "SELECT
			flazz.tgl,	flazz.doc_ref,	flazz.note,	flazz.bruto,	flazz.vatrp,
			flazz.total, IF( isnull( flazz.barang_id ), '', 'Pengisian Kartu BCA Flazz' ) AS barang_id,
			flazz.qty,	cust.nama_customer,	cust.no_customer,	cust.city,	cust.pt
		FROM
			nscc_sales_flazz AS flazz
			INNER JOIN nscc_customers_flazz AS cust ON flazz.customer_id = cust.customer_id
		WHERE
			flazz.tgl BETWEEN :dtFrom AND :dtTo
		ORDER BY tgl" );
		$inv = $query->queryAll( true, [ ':dtFrom' => $dtFrom, ':dtTo' => $dtTo ] );
		$query = $this->dbConnection->createCommand( "SELECT
			flazz.doc_ref,	brg.kode_barang,	brg.nama_barang,	item.qty,
			item.harga,	item.bruto,	item.vatrp,	item.total
		FROM
			nscc_sales_flazz AS flazz
			INNER JOIN nscc_sales_flazz_items AS item ON flazz.sales_flazz_id = item.sales_flazz_id
			INNER JOIN nscc_barang_flazz AS brg ON item.barang_id = brg.barang_id
		WHERE
			flazz.tgl BETWEEN :dtFrom AND :dtTo
		ORDER BY flazz.tgl,	flazz.doc_ref" );
		$item = $query->queryAll( true, [ ':dtFrom' => $dtFrom, ':dtTo' => $dtTo ] );
		return compact('inv','item');
	}
}