<?php

Yii::import('application.models._base.BaseTransferItemDetails');

class TransferItemDetails extends BaseTransferItemDetails {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->transfer_item_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_item_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
    
    
    static function get_details_to_print($transfer_item_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                b.kode_barang,
                b.nama_barang,
                tid.qty,
                b.sat,
                tid.price,
                tid.disc,
                tid.ppn,
                tid.pph,
                tid.total
            FROM nscc_transfer_item_details tid
                LEFT JOIN nscc_barang b ON b.barang_id = tid.barang_id
            WHERE 
                tid.transfer_item_id = :transfer_item_id
                AND tid.visible = 1
                AND tid.qty != 0
        ");
        return $comm->queryAll(true, array(':transfer_item_id' => $transfer_item_id));
    }

}
