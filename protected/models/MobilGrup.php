<?php
Yii::import('application.models._base.BaseMobilGrup');

class MobilGrup extends BaseMobilGrup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->mobil_grup_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->mobil_grup_id = $uuid;
        }
        return parent::beforeValidate();
    }
}