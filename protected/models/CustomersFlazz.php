<?php
Yii::import('application.models._base.BaseCustomersFlazz');

class CustomersFlazz extends BaseCustomersFlazz
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->customer_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_id = $uuid;
        }
        return parent::beforeValidate();
    }
}