<?php
Yii::import('application.models._base.BaseKas');
Yii::import('application.components.U');
class Kas extends BaseKas
{
    private $_oldAttributes = array();
    public function setOldAttributes($value)
    {
        $this->_oldAttributes = $value;
    }
    public function getOldAttributes()
    {
        return $this->_oldAttributes;
    }
    public function init()
    {
        $this->attachEventHandler("onAfterFind", function ($event) {
            $event->sender->OldAttributes = $event->sender->Attributes;
        });
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->kas_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        if (!$this->getIsNewRecord()) {
            if (substr($this->doc_ref, 0, strlen(STOREID)) != STOREID) {
                throw new Exception('Untuk edit data cabang harus dari server cabang. Terima Kasih.');
            }
        }
        if($this->tgl != null){
            if($this->tgl < U::getResDate()){
               throw new Exception('error pembatasan tanggal.');  
            }
        }

        return parent::beforeValidate();
    }
    public static function is_modal_exist($tgl)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("total > 0");
        $criteria->addCondition("type_ = 1");
        $criteria->addCondition("DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $tgl);
        $res = Kas::model()->count($criteria);
        return $res > 0;
    }
    public static function get_cash_in_out($tgl, $arus, $bank_id = null, $store = STOREID)
    {
//        $bank_id = Bank::get_bank_cash_id();
        // AND ns.bank_id = :bank_id
        $where = "";
        $param = array(
            ':tgl' => $tgl,
            ':arus' => $arus
        );
        if ($bank_id != null) {
        	if(is_array($bank_id)){
		        $arr_filt = implode("','", $bank_id);
		        $where .= " AND ns.bank_id IN ('$arr_filt')";
		        //$param[':bank_id'] = "$arr_filt";
	        }else{
		        $where .= " AND ns.bank_id = :bank_id";
		        $param[':bank_id'] = $bank_id;
	        }

        }
        if ($store != null) {
            $where .= " AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus = :arus AND ns.visible = 1 AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_cash_in($tgl, $bank_id = null, $store = STOREID)
    {
        return Kas::get_cash_in_out($tgl, 1, $bank_id, $store);
    }
    public static function get_cash_out($tgl, $bank_id = null, $store = STOREID)
    {
        return Kas::get_cash_in_out($tgl, -1, $bank_id, $store);
    }
//    protected function afterSave()
//    {
//        parent::afterSave();
//        $is_in = $this->total >= 0;
//        U::add_bank_trans($is_in ? CASHIN : CASHOUT, $this->kas_id, $this->bank_id, $this->doc_ref, $this->tgl,
//            $this->total, $this->user_id);
//    }

    /**
     * After save attributes
     */
    /* protected function afterSave() {
        parent::afterSave();
        U::runCommand('kas', '--id=' . $this->kas_id, 'protected/runtime/kas_'.$this->kas_id.'.log');             
            
    } */
    	public function generateGltransKas($val) {
//	public function generateGltransKas() {
		$gl                   = new GL();
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		try {
			$model  = $val;
//			$model  = $this;
			$is_in = $model->arus == 1;
			$docref = $model->doc_ref;
			$gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref, $model->bank->account_code,
                            $model->keperluan, $is_in ? "Cash In" : "Cash Out", $model->amount, 0,
                            $model->store);
                        $total_nominal = 0;
			foreach ( $model->kasDetails as $kas_detail ) {
                            if ($kas_detail->visible == 1){
                                    if ($model->all_store == 1) {
                                        $nominal_item = round($kas_detail->total / $model->jml_cabang, 2);
                                        foreach ($store_acc as $key => $row) {
                                            $current_branch = $row['store_kode'];
                                            $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                                                $kas_detail->account_code, $kas_detail->item_name, "",
                                                $nominal_item, 1, $current_branch);
                                            $total_nominal += $nominal_item;
                                        }
                                    } else {
                                        $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                                            $kas_detail->account_code, $kas_detail->item_name, '',
                                            $kas_detail->total, 1, $kas_detail->store);
                                }
                            }				
			}
			if ($model->all_store == 1) {
                            $total = abs($is_in ? $model->total_debit : $model->total_kredit) - abs($model->amount);
                            $selisih = $total - abs($total_nominal);
                            if ($selisih != 0.00) {
                                $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                                    COA_ROUNDING, 'Pembulatan sisa bagi beban all cabang', '',
                                    ($is_in ? $selisih : -$selisih), 1, $current_branch);
                            }
                        } 
			$gl->validate();
                        $status = true;
                        $msg = 'sukses';
                        $transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
                        echo $msg;
		}
		app()->db->autoCommit = true;
		return [
			'status' => $status,
			'msg'    => $msg
		];
	}    	
}