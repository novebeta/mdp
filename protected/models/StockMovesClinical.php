<?php

Yii::import('application.models._base.BaseStockMovesClinical');
class StockMovesClinical extends BaseStockMovesClinical
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->stock_moves_clinical_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->stock_moves_clinical_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function add_stock_moves($clinical_trans_id,$tran_date,$barang_clinical,
        $qty,$tipe_clinical_id, $reference, $store) {
        $move = new StockMovesClinical;
        $move->tran_date = $tran_date;
        $move->reference = $reference;
        $move->qty = $qty;
        $move->store = $store;
        $move->barang_clinical = $barang_clinical;
        $move->tipe_clinical_id = $tipe_clinical_id;
        $move->clinical_trans_id = $clinical_trans_id;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Stock moves clinical')) . CHtml::errorSummary($move));
        }
    }
}