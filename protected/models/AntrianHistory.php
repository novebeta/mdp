<?php
Yii::import('application.models._base.BaseAntrianHistory');
class AntrianHistory extends BaseAntrianHistory
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->antrian_history_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->antrian_history_id = $uuid;
        }
        return parent::beforeValidate();
    }
}