<?php
Yii::import('application.models._base.BaseOnlineReservation');

class OnlineReservation extends BaseOnlineReservation
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->reservation_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->reservation_id = $uuid;
        }
        return parent::beforeValidate();
    }
}