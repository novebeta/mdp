<?php
Yii::import( 'application.models._base.BaseBpBayarJasa' );
class BpBayarJasa extends BaseBpBayarJasa {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->bp_bayar_jasa_id == null ) {
			$command                = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                   = $command->queryScalar();
			$this->bp_bayar_jasa_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->id_user == null ) {
			$this->id_user = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public static function getList( $account_code, $bp_bayar_jasa_id = null ) {
		$ppn = yiiparam('ppn',10)/100;
		$comm   = Yii::app()->db
			->createCommand()
			->select( "bj.bp_jasa_id,	bp.inv_no,
			bp.inv_tgl,	SUM((bj.hpp1 + bj.hpp2) + ((bj.hpp1 + bj.hpp2) * (".$ppn."*COALESCE(bj.ppn_out,0))) ) AS hpp,
			CONCAT( njb.nama, ' ', COALESCE(bj.note,'') ) AS nama,
			njb.kode,	njb.account_code " )
			->from( "nscc_bp_jasa AS bj" )
			->join( "nscc_jasa_bp AS njb", "bj.jasa_bp_id = njb.jasa_bp_id" )
			->join( "nscc_body_paint AS bp", "bj.body_paint_id = bp.body_paint_id" )
			->group( "bj.bp_jasa_id,bp.body_paint_id" );
		$params = [];
		if ( $bp_bayar_jasa_id != null ) {
			$comm->where( "bj.bp_bayar_jasa_id = :bp_bayar_jasa_id" );
			$params[ ':bp_bayar_jasa_id' ] = $bp_bayar_jasa_id;
		} else {
			$comm->where( "bj.bp_bayar_jasa_id IS NULL AND bp.inv_no IS NOT NULL AND njb.account_code = :account_code" );
			$params[ ':account_code' ] = $account_code;
		}
		try {
			return $comm->queryAll( true, $params );
		} catch ( CException $e ) {
			return null;
		}
	}
	public function getSupplierName() {
		$supp = BpSupp::model()->findByPk($this->account_code);
		if($supp != null){
			return $supp->nama;
		}
		return '';
	}
}