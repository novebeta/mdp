<?php
Yii::import('application.models._base.BaseTerimaBarangDetails');
class TerimaBarangDetails extends BaseTerimaBarangDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->terima_barang_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_item_detil_invoice(){
        $cmd = DbCmd::instance()
            ->addSelect([
                'tbd.barang_id',
                'tbd.seq',
                'tbd.qty qty_terima',
                't2.qty_invoiced',
                '(tbd.qty-IFNULL(t2.qty_invoiced, 0)) qty',
                'pod.charge',
                'pod.price',
                'pod.disc',
                'pod.ppn',
                'pod.pph_id',
                'pod.pph',
                'tbd.item_id',
                'pod.price price_before'
            ])
            ->addFrom('{{terima_barang_details}} tbd')
            ->addLeftJoin(
                DbCmd::instance()
                    ->addSelect([
                        'ti.terima_barang_id',
                        'tid.barang_id',
                        'tid.item_id',
                        'SUM(tid.qty) qty_invoiced',
                    ])
                    ->addFrom('{{transfer_item_details}} tid')
                    ->addLeftJoin('{{transfer_item}} ti', 'tid.transfer_item_id = ti.transfer_item_id')
                    ->addCondition('ti.terima_barang_id = :terima_barang_id')
                    ->addCondition('tid.visible = 1')
                    ->addGroup('ti.terima_barang_id, tid.barang_id, tid.item_id')
                    ->setAs('t2')
                , 't2.terima_barang_id = tbd.terima_barang_id AND t2.barang_id = tbd.barang_id AND t2.item_id = tbd.item_id')
            ->addLeftJoin('{{terima_barang}} tb', 'tb.terima_barang_id = tbd.terima_barang_id')
            ->addLeftJoin('{{po_details}} pod', 'tb.po_id = pod.po_id AND tbd.barang_id = pod.barang_id AND tbd.item_id = pod.item_id AND pod.visible = 1')
            ->addCondition('tbd.terima_barang_id = :terima_barang_id')
            ->addCondition('tbd.visible = 1')
            ->addParams([':terima_barang_id'=>$_POST['terima_barang_id']]);
//            $txt = $cmd->getQuery();
            return $cmd->queryAll();
    }
    public static function get_item_detil_return(){
        $comm = Yii::app()->db->createCommand("
            SELECT
                tbd.barang_id,
                tbd.seq,
                (tbd.qty-ifnull(t.qty,0)) qty,
                pod.charge,
                pod.price,
                pod.disc,
                pod.ppn,
                pod.pph
            FROM nscc_terima_barang_details tbd
                    LEFT JOIN nscc_terima_barang tb ON tb.terima_barang_id = tbd.terima_barang_id
                LEFT JOIN nscc_po_details pod ON tb.po_id = pod.po_id AND tbd.barang_id = pod.barang_id AND pod.visible = 1
                LEFT JOIN (
                            SELECT
                                    tid.barang_id,
                        sum(tid.qty) qty
                            FROM nscc_transfer_item_details tid
                                    LEFT JOIN nscc_transfer_item ti ON tid.transfer_item_id = ti.transfer_item_id
                            WHERE
                                    tid.visible = 1
                        AND ti.terima_barang_id = :terima_barang_id
                        AND ti.type_ = 1
                            GROUP BY tid.barang_id
                ) t ON tbd.barang_id = t.barang_id
            WHERE
                    tbd.terima_barang_id = :terima_barang_id
                AND tbd.visible = 1
        ");
        return $comm->queryAll(true,array(':terima_barang_id'=>$_POST['terima_barang_id']));
    }
}