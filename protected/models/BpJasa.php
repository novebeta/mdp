<?php
Yii::import('application.models._base.BaseBpJasa');

class BpJasa extends BaseBpJasa
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->bp_jasa_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bp_jasa_id = $uuid;
        }
        return parent::beforeValidate();
    }
}