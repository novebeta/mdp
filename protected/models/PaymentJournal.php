<?php
Yii::import('application.models._base.BasePaymentJournal');

class PaymentJournal extends BasePaymentJournal
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->payment_journal_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->payment_journal_id = $uuid;
        }
	    if ($this->tdate == null) {
		    $this->tdate = new CDbExpression('NOW()');
	    }
	    if ($this->user_id == null) {
		    $this->user_id = Yii::app()->user->getId();
	    }
        return parent::beforeValidate();
    }
}