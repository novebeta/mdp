<?php
Yii::import('application.models._base.BaseLahaImport');
class LahaImport extends BaseLahaImport
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->laha_import_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->laha_import_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function reposting()
    {
        $msg = 'Laha Import berhasil direposting';
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            GlTrans::model()->deleteAllByAttributes([
                'type' => IMPORTLAHA,
                'type_no' => $this->laha_import_id
            ]);
            BankTrans::model()->deleteAllByAttributes([
                'type_' => IMPORTLAHA,
                'trans_no' => $this->laha_import_id
            ]);
            $gl = new GL();
            /** @var Bank $kas_cabang */
            $kas_cabang = Bank::model()->findByPk($this->p_d_kas_bank_id);
            if ($kas_cabang == null) {
                throw new Exception('Kas Cabang tidak ditemukan.');
            }
            # Kas Cabang - Debit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                $kas_cabang->account_code, 'Kas Cabang Penjualan', '',
                $this->p_d_kas_n, 0, $this->store, $this->id_user);
            $gl->add_bank_trans(IMPORTLAHA, $this->laha_import_id, $this->p_d_kas_bank_id, $this->doc_ref,
                $this->tgl, $this->p_d_kas_n, $this->id_user, $this->store);
            # Piutang Card - Debit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_PIUTANG_CARD, 'Piutang Card Penjualan', '',
                $this->p_d_piutangcard_n, 1, $this->store, $this->id_user);
            # Penjualan - Debit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_SALES_DISK, 'Potongan Penjualan', '',
                $this->p_d_pot_jual_n, 1, $this->store, $this->id_user);
            # Penjualan Obat - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_SALES_OBAT, 'Penjualan Obat', '',
                -($this->p_k_obat_n), 1, $this->store, $this->id_user);
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_SALES_OBAT_PPN, 'Ppn Penjualan Obat', '',
                -($this->p_k_ppn_obat_n), 1, $this->store, $this->id_user);
            # Penjualan Apotik - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_SALES_APOTIK, 'Penjualan Apotik', '',
                -($this->p_k_apotik_n), 1, $this->store, $this->id_user);
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_SALES_APOTIK_PPN, 'Ppn Penjualan Apotik', '',
                -($this->p_k_ppn_apotik_n), 1, $this->store, $this->id_user);
            # Penjualan Perawatan - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_SALES_JASA, 'Penjualan Perawatan', '',
                -($this->p_k_jasa_n), 1, $this->store, $this->id_user);
            # Pendapatan Lain Lain - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_PENDAPATAN_LAIN, 'Pendapatan Lain Lain', '',
                -($this->p_k_lain_lain_n), 1, $this->store, $this->id_user);
            # Pendapatan Di Muka - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_PENDAPATAN_DIMUKA, 'Pendapatan Di Muka', '',
                -($this->p_k_pendmuka_n), 1, $this->store, $this->id_user);
            # Pembulatan - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_ROUNDING, 'Pembulatan', '',
                -($this->p_k_round_n), 1, $this->store, $this->id_user);
            foreach ($this->lahaImportBankFees as $rows) {
                if ($rows->type_ == 0) {
                    # Bank - Debit #
                    $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                        $rows->bank0->account_code, $rows->bank0->nama_bank, '',
                        (floatval($rows->bank)), 0, $this->store, $this->id_user);
                    $gl->add_bank_trans(IMPORTLAHA, $this->laha_import_id, $rows->bank_id, $this->doc_ref,
                        $this->tgl, (floatval($rows->bank)), $this->id_user, $this->store);
                } else {
                    # Bank - Debit #
                    $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                        $rows->bank0->account_code, $rows->bank0->nama_bank, '',
                        (floatval($rows->bank)), 0, $this->store, $this->id_user);
                    $gl->add_bank_trans(IMPORTLAHA, $this->laha_import_id, $rows->bank_id, $this->doc_ref,
                        $this->tgl, (floatval($rows->bank)), $this->id_user, $this->store);
                    # Kas Cabang - Kredit #
                    $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                        $kas_cabang->account_code, $kas_cabang->nama_bank, '',
                        -(floatval($rows->bank)), 0, $this->store, $this->id_user);
                    $gl->add_bank_trans(IMPORTLAHA, $this->laha_import_id, $this->p_d_kas_bank_id, $this->doc_ref,
                        $this->tgl, -(floatval($rows->bank)), $this->id_user, $this->store);
                }
            }
            # Fee Card - Debit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_FEE_CARD, 'Fee Card', '',
                ($this->piu_d_fee_n), 1, $this->store, $this->id_user);
            # Piutang Card - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_PIUTANG_CARD, 'Piutang Card Penjualan', '',
                -($this->p_d_piutangcard_n), 1, $this->store, $this->id_user);
            foreach ($this->lahaImportBiayas as $row1) {
                # Biaya - Debit #
                $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                    $row1->account_code, $row1->note_, $row1->note_,
                    (floatval($row1->amount)), 1, $row1->store, $this->id_user);
            }
            # Kas Cabang - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                $kas_cabang->account_code, 'Kas Cabang Biaya', '',
                -(floatval($this->b_k_kas_n)), 0, $this->store, $this->id_user);
            $gl->add_bank_trans(IMPORTLAHA, $this->laha_import_id, $this->p_d_kas_bank_id, $this->doc_ref,
                $this->tgl, -(floatval($this->b_k_kas_n)), $this->id_user, $this->store);
            # Pph - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_HUTANG_PPH21, $this->b_k_pph21_note, '',
                -($this->b_k_pph21_n), 1, $this->store, $this->id_user);
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_HUTANG_PPH22, $this->b_k_pph22_note, '',
                -($this->b_k_pph22_n), 1, $this->store, $this->id_user);
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_HUTANG_PPH23, $this->b_k_pp23_note, '',
                -($this->b_k_pp23_n), 1, $this->store, $this->id_user);
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_HUTANG_PPH42, $this->b_k_pph4_2_note, '',
                -($this->b_k_pph4_2_n), 1, $this->store, $this->id_user);
            # Pembulatan - Kredit #
            $gl->add_gl(IMPORTLAHA, $this->laha_import_id, $this->tgl, $this->doc_ref,
                COA_ROUNDING, 'Pembulatan Biaya', '',
                -($this->b_k_round_n), 1, $this->store, $this->id_user);
            $gl->validate();
            $gl->add_comments(IMPORTLAHA, $this->laha_import_id, $this->tgl,
                'LAHA IMPORT ' . $this->doc_ref);
            $this->p = 1;
            if (!$this->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Biaya')) . CHtml::errorSummary($this));
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $e) {
            $transaction->rollback();
            $status = false;
            $msg = $e->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
    }
}