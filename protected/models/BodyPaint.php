<?php
Yii::import( 'application.models._base.BaseBodyPaint' );
class BodyPaint extends BaseBodyPaint {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->body_paint_id == null ) {
			$command             = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                = $command->queryScalar();
			$this->body_paint_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public function breakDownJasa() {
		$query = $this->dbConnection->createCommand( "SELECT
			mjasa.account_code, 
			SUM(tjasa.total_jasa_line) AS tot
		FROM
			nscc_bp_jasa AS tjasa
			INNER JOIN
			nscc_jasa_bp AS mjasa
			ON 
				tjasa.jasa_bp_id = mjasa.jasa_bp_id
		WHERE tjasa.body_paint_id = :body_paint_id
		GROUP BY
			mjasa.account_code" );
		return $query->queryAll( true, [ ':body_paint_id' => $this->body_paint_id ] );
	}
	public function relations() {
		return array_merge(parent::relations(), array(
			'customer' =>array(self::BELONGS_TO, 'CustomerBp', array('customer_id'=>'customer_bp_id'), 'through'=>'mobil'),
			'asuransi' => array(self::BELONGS_TO, 'AsuransiBp', 'asuransi_bp_id'),
		));
	}

	public function lapBPTrans($dtFrom, $dtTo) {
		$query = $this->dbConnection->createCommand( "SELECT
			bp.inv_tgl,	bp.inv_no, bp.est_no,	bp.est_tgl,	bp.wo_no,	bp.wo_tgl,	bp.est_kerja,	bp.nama_sa,	bp.est_start,	bp.est_end,
			bp.total_jasa,	bp.total_parts,	bp.grand_total,	bp.no_spk,	bp.total_jasa_line,	bp.total_jasa_discrp,
			bp.biaya,	bp.qty_kejadian,	bp.own_risk,	bp.total_asuransi,	bp.vat,	bp.vatrp,	bp.total,	cust.nama_customer,
			cust.alamat,	cust.city,	cust.telp,	cust.ktp,	cust.npwp,	cust.tahun_pembuatan,	cust.no_pol,
			cust.no_mesin,	cust.no_rangka,	cust.warna,	cust.tipe,	cust.merk,	asu.asuransi_bp_id,	asu.kode,
			asu.nama,	asu.address,	asu.city,	asu.npwp
		FROM nscc_body_paint AS bp
			INNER JOIN nscc_cust_bp_view AS cust ON bp.mobil_id = cust.mobil_id
			LEFT JOIN nscc_asuransi_bp AS asu ON bp.asuransi_bp_id = asu.asuransi_bp_id
		WHERE ( bp.inv_tgl BETWEEN :dtFrom AND :dtTo ) AND isnull( bp.deleted )
		ORDER BY bp.inv_tgl" );
		$inv = $query->queryAll( true, [ ':dtFrom' => $dtFrom, ':dtTo' => $dtTo ] );
		$query = $this->dbConnection->createCommand( "SELECT
			bp.inv_no,	jasa.nama,	bpjasa.note,	bpjasa.tipe,	bpjasa.harga,
			bpjasa.disc,	bpjasa.discrp,	bpjasa.dpp,	bpjasa.hpp2
		FROM
			nscc_body_paint AS bp,
			nscc_bp_jasa AS bpjasa,
			nscc_jasa_bp AS jasa
		WHERE
			( bp.body_paint_id = bpjasa.body_paint_id )
			AND ( bpjasa.jasa_bp_id = jasa.jasa_bp_id )
			AND ( bp.inv_tgl BETWEEN :dtFrom AND :dtTo )
			AND (isnull( bp.deleted ))" );
		$jasa = $query->queryAll( true, [ ':dtFrom' => $dtFrom, ':dtTo' => $dtTo ] );
		$query = $this->dbConnection->createCommand( "SELECT
			bp.inv_no,	part.kode,	part.nama,	part.qty,
			part.harga,	part.hpp,	part.total_parts_line
		FROM
			nscc_body_paint AS bp,
			nscc_bp_parts AS part
		WHERE
			( bp.body_paint_id = part.body_paint_id )
			AND ( bp.inv_tgl BETWEEN :dtFrom AND :dtTo )
			AND (isnull( bp.deleted ))" );
		$part = $query->queryAll( true, [ ':dtFrom' => $dtFrom, ':dtTo' => $dtTo ] );
		return compact('inv','jasa','part');
	}
}