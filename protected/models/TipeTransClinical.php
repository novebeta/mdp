<?php

Yii::import('application.models._base.BaseTipeTransClinical');

class TipeTransClinical extends BaseTipeTransClinical
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->tipe_clinical_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tipe_clinical_id = $uuid;
        }
        return parent::beforeValidate();
    }
}