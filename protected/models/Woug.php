<?php
Yii::import( 'application.models._base.BaseWoug' );
class Woug extends BaseWoug {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function primaryKey() {
		return 'sales_id';
	}
}