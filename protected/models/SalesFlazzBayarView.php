<?php
Yii::import('application.models._base.BaseSalesFlazzBayarView');

class SalesFlazzBayarView extends BaseSalesFlazzBayarView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
   
    public function primaryKey()
    {
        return 'sales_flazz_bayar_id';
    }
}