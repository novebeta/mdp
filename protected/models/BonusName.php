<?php

Yii::import('application.models._base.BaseBonusName');

class BonusName extends BaseBonusName {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->bonus_name_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bonus_name_id = $uuid;
        }
        return parent::beforeValidate();
    }

}
