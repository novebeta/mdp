<?php
Yii::import('application.models._base.BaseNoFaktur');

class NoFaktur extends BaseNoFaktur
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->no_faktur_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->no_faktur_id = $uuid;
        }
        if ( $this->created_at == null ) {
		    $this->created_at = time();
	    }
	    if ( $this->created_user == null ) {
		    $this->created_user = Yii::app()->user->getId();
	    }
        return parent::beforeValidate();
    }

    public static function getNoFaktur($tgl){
        $criteria=new CDbCriteria;
        $criteria->condition='mulai_berlaku <= :tgl AND YEAR(DATE(mulai_berlaku)) = YEAR(DATE(:tgl)) AND tipe_trans is NULL AND id_trans is NULL';
        $criteria->order = 'mulai_berlaku ASC';
        $criteria->params = array(':tgl'=>$tgl);
        return NoFaktur::model()->find($criteria);
    }
}