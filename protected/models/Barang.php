<?php

Yii::import('application.models._base.BaseBarang');
class Barang extends BaseBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function new_data_jual($store)
    {
        $jual = new Jual();
        $jual->barang_id = $this->barang_id;
        $jual->store = $store;
        $jual->save();
        return $jual;
    }
    public function count_biaya_beli($unit, $harga, $store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        if ($jual == null) {
            $jual = $this->new_data_jual($store);
        }
        $jml_stok = StockMoves::get_saldo_item($this->barang_id, $store);
        $harga_lama = $jual->cost;
        $total_lama = round($jml_stok * $harga_lama, 2);
        $total_baru = $harga;
        $jml_baru = $jml_stok + $unit;
        $harga_baru = $jml_baru == 0? 0 : round(($total_baru + $total_lama) / $jml_baru, 2);
        $jual->cost = $harga_baru;
        if (!$jual->save()) {
            throw new Exception(t('save.fail', 'app') . CHtml::errorSummary($jual));
        }
    }
    public function count_hpp($qty_before, $harga_before, $unit, $harga, $store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        if ($jual == null) {
            $jual = $this->new_data_jual($store);
        }
        $jml_stok = $qty_before;
        $harga_lama = $harga_before;
        $total_lama = round($jml_stok * $harga_lama, 2);
        $total_baru = $harga;
        $jml_baru = $jml_stok + $unit;
        
        if($jml_baru == 0) return;

        $harga_baru = round(($total_baru + $total_lama) / $jml_baru, 2);
        $jual->cost = $harga_baru;
        if (!$jual->save()) {
            throw new Exception(t('save.fail', 'app') . CHtml::errorSummary($jual));
        }
    }
    public function get_coa_sales($store){
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_jual;
        }
    }
    public function get_coa_sales_hpp($store){
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_sales_hpp;
        }
    }
    public function get_coa_sales_disc($store){
        /** @var GrupAttr $grupattr */
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_sales_disc;
        }
    }
	public function get_coa_sales_discrp1($store){
		/** @var GrupAttr $grupattr */
		$grupattr = GrupAttr::model()->findByAttributes(array(
			'grup_id' => $this->grup_id,
			'store' => $store
		));
		if ($grupattr == null) {
			return null;
		}else{
			return $grupattr->coa_sales_discrp1;
		}
	}
    public function get_coa_sales_return($store){
        /** @var GrupAttr $grupattr */
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return null;
        }else{
            return $grupattr->coa_jual_return;
        }
    }
    public function get_tax($store)
    {
        $grupattr = GrupAttr::model()->findByAttributes(array(
            'grup_id' => $this->grup_id,
            'store' => $store
        ));
        if ($grupattr == null) {
            return 0;
        } else {
            if ($grupattr->tax == 1 && $grupattr->vat > 0) {
                return round($grupattr->vat / 100, 2);
            } else {
                return 0;
            }
        }
    }
    public function get_cost($store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        if ($jual == null) {
            $jual = $this->new_data_jual($store);
        }
        return $jual->cost;
    }
    public function get_price($store)
    {
        $jual = Jual::model()->findByAttributes(array('barang_id' => $this->barang_id, 'store' => $store));
        if ($jual == null) {
            return 0;
        } else {
            return $jual->price;
        }
    }
    public function beforeValidate()
    {
        if ($this->barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function isPerawatan()
    {
        return $this->grup->kategori_id == 1;
    }

    /**
     * Query filter : table 'nscc_barang'
     *
     * @param DbCmd $cmd
     * @param array $params
     * @return DbCmd
     */
    public static function queryFilter(DbCmd $cmd, $params = array()){
        $cmd->addSelect(array(
                '{{barang}}.kode_barang',
                '{{barang}}.nama_barang',
                '{{barang}}.sat',
            ))
            ->addLeftJoin('{{barang}}', '{{barang}}.barang_id = t.barang_id')
        ;

        if (array_key_exists('kode_barang', $params)) {
            $cmd->addCondition('{{barang}}.kode_barang LIKE :kode_barang');
            $cmd->addParam(':kode_barang', '%'.$params['kode_barang'].'%');
        }
        if (array_key_exists('nama_barang', $params)) {
            $cmd->addCondition('{{barang}}.nama_barang LIKE :nama_barang');
            $cmd->addParam(':nama_barang', '%'.$params['nama_barang'].'%');
        }
        if (array_key_exists('sat', $params)) {
            $cmd->addCondition('{{barang}}.sat LIKE :sat');
            $cmd->addParam(':sat', '%'.$params['sat'].'%');
        }

        return $cmd;
    }
      /**
     * After save attributes
     */
//    protected function afterSave() {
//        parent::afterSave();
//        U::runCommand('barang', '--id=' . $this->barang_id,  'protected/runtime/barang_'.$this->barang_id.'.log');        
//    }
}