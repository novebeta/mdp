<?php
Yii::import('application.models._base.BaseAsuransiBp');

class AsuransiBp extends BaseAsuransiBp
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asuransi_bp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asuransi_bp_id = $uuid;
        }
        return parent::beforeValidate();
    }
}