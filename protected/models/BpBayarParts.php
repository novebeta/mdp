<?php
Yii::import( 'application.models._base.BaseBpBayarParts' );
class BpBayarParts extends BaseBpBayarParts {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->bp_bayar_parts_id == null ) {
			$command                 = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                    = $command->queryScalar();
			$this->bp_bayar_parts_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->id_user == null ) {
			$this->id_user = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public static function getList( $bp_bayar_parts_id = null ) {
		$comm   = Yii::app()->db
			->createCommand()
			->select( "nbp.bp_parts_id,	bp.inv_no,
				bp.inv_tgl,	(nbp.hpp+nbp.ppnout) AS hpp,
			    nbp.nama,	nbp.kode" )
			->from( "nscc_bp_parts AS nbp" )
			->join( "nscc_body_paint AS bp", "nbp.body_paint_id = bp.body_paint_id" );
		$params = [];
		if ( $bp_bayar_parts_id != null ) {
			$comm->where( "nbp.bp_bayar_parts_id = :bp_bayar_parts_id" );
			$params[':bp_bayar_parts_id'] = $bp_bayar_parts_id;
		} else {
			$comm->where( "nbp.bp_bayar_parts_id IS NULL AND bp.inv_no IS NOT NULL " );
		}
		try {
			return $comm->queryAll( true, $params );
		} catch ( CException $e ) {
			return null;
		}
	}
}