<?php
Yii::import('application.models._base.BaseSalesFlazzBayar');

class SalesFlazzBayar extends BaseSalesFlazzBayar
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->sales_flazz_bayar_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_flazz_bayar_id = $uuid;
        }
	    if ( $this->tdate == null ) {
		    $this->tdate = new CDbExpression( 'NOW()' );
	    }
	    if ( $this->user_id == null ) {
		    $this->user_id = Yii::app()->user->getId();
	    }
        return parent::beforeValidate();
    }
}