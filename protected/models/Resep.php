<?php
Yii::import('application.models._base.BaseResep');
class Resep extends BaseResep
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->resep_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->resep_id = $uuid;
        }
        return parent::beforeValidate();
    }
}