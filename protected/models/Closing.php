<?php
Yii::import('application.models._base.BaseClosing');

class Closing extends BaseClosing
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->closng_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->closng_id = $uuid;
        }
        return parent::beforeValidate();
    }
}