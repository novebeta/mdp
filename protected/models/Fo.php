<?php
Yii::import('application.models._base.BaseFo');
class Fo extends BaseFo
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'fo_id';
    }
}