<?php
Yii::import('application.models._base.BaseRacikanDetails');

class RacikanDetails extends BaseRacikanDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->racikan_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->racikan_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}