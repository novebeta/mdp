<?php
Yii::import('application.models._base.BaseDropping');
class Dropping extends BaseDropping
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->dropping_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->dropping_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }

	public static function get_available_terima_barang($id_terima, $id_dropping = "")
	{
		if (!empty($id_dropping)) {
			$id_dropping = "AND od.order_dropping_id='".addslashes($id_dropping)."'";
		}

		if (empty($id_dropping))
		{
			$comm = Yii::app()->db->createCommand("
			SELECT b.barang_id, b.kode_barang, bd.qty AS qty_diterima FROM nscc_terima_barang AS tb
			INNER JOIN nscc_terima_barang_details AS bd ON tb.terima_barang_id=bd.terima_barang_id
			INNER JOIN nscc_barang AS b ON b.barang_id=bd.barang_id
			WHERE tb.terima_barang_id=:terima_barang_id AND bd.visible=1
			GROUP BY b.kode_barang");
		}
		else
		{
			$comm = Yii::app()->db->createCommand("
				SELECT  a.terima_barang_details_id, a.barang_id, a.kode_barang, 
					if(a.qty_diterima=0,'-', sum(a.qty_diterima)) as qty_diterima, 
					sum(a.qty_order) as qty_order FROM
				(
				SELECT bd.terima_barang_details_id, b.barang_id, b.kode_barang, sum(bd.qty) AS qty_diterima, '0' as qty_order, '1' as nomor  FROM nscc_terima_barang AS tb
					INNER JOIN nscc_terima_barang_details AS bd ON tb.terima_barang_id=bd.terima_barang_id
					INNER JOIN nscc_barang AS b ON b.barang_id=bd.barang_id
					WHERE tb.terima_barang_id=:terima_barang_id AND bd.visible=1
					GROUP BY b.kode_barang
				union	
				SELECT '' as terima_barang_details_id, dd.barang_id, b.kode_barang, '0' AS qty_diterima, sum(dd.qty) as qty_order,  '2' as nomor from nscc_order_dropping as od
					INNER JOIN nscc_order_dropping_details as dd on od.order_dropping_id=dd.order_dropping_id
					INNER JOIN nscc_barang AS b ON b.barang_id=dd.barang_id
					WHERE dd.visible=1 $id_dropping
					group by dd.barang_id
				) as a
				GROUP BY a.barang_id
				order by a.nomor , a.kode_barang asc");
		}

		return $comm->queryAll(true,array(':terima_barang_id'=>$id_terima));
	}
}