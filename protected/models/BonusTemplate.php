<?php
Yii::import('application.models._base.BaseBonusTemplate');

class BonusTemplate extends BaseBonusTemplate
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->bonus_template_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bonus_template_id = $uuid;
        }
        return parent::beforeValidate();
    }
}