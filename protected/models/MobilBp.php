<?php
Yii::import('application.models._base.BaseMobilBp');

class MobilBp extends BaseMobilBp
{
//	var $merk_id = 'test';
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->mobil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->mobil_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public function getMerkId() {
	    return $this->mobilTipe->merk_id;
    }

    public function relations() {
		return array_merge(parent::relations(), array(
			'customer' => array(self::BELONGS_TO, 'CustomerBp', array('customer_id'=>'customer_bp_id')),
		));
	}
}