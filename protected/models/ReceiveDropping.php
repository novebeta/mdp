<?php
Yii::import('application.models._base.BaseReceiveDropping');
class ReceiveDropping extends BaseReceiveDropping
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->receive_dropping_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->receive_dropping_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    
    public static function get_total_received($barang_id, $store, $dropping_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                SUM(ifnull(d.qty,0))
            FROM nscc_receive_dropping_details d
                LEFT JOIN nscc_receive_dropping h ON h.receive_dropping_id = d.receive_dropping_id
            WHERE d.barang_id = :barang_id AND d.visible = 1 AND h.store = :store AND h.dropping_id = :dropping_id
        ");
        return $comm->queryScalar(array(
            ':barang_id' => $barang_id,
            ':store' => $store,
            ':dropping_id' => $dropping_id
        ));
    }
}