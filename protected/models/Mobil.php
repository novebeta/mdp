<?php
Yii::import('application.models._base.BaseMobil');

class Mobil extends BaseMobil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->mobil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->mobil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}