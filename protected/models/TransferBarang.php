<?php
Yii::import( 'application.models._base.BaseTransferBarang' );
/*
 * @property SuppFlazz $suppFlazz
 */
class TransferBarang extends BaseTransferBarang {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->transfer_barang_id == null ) {
			$command                  = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                     = $command->queryScalar();
			$this->transfer_barang_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->store == null ) {
			$this->store = STOREID;
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	static function get_details_to_print( $dropping_id ) {
		$comm = Yii::app()->db->createCommand( "
            SELECT 
                b.kode_barang,
                b.nama_barang,
                tid.qty,
                b.sat
            FROM nscc_transfer_barang_details tid
            	INNER JOIN nscc_transfer_barang as tb on tid.transfer_barang_id=tb.transfer_barang_id
                LEFT JOIN nscc_barang b ON b.barang_id = tid.barang_id
            WHERE 
                tb.doc_ref = :dropping_id
                AND tid.visible = 1
        " );
		return $comm->queryAll( true, [ ':dropping_id' => $dropping_id ] );
	}
	public function relations() {
		$relasi =  array_merge( parent::relations(), [
			'suppFlazz' => [ self::BELONGS_TO, 'SuppFlazz', 'supp_id' ],
		] );
		return $relasi;
	}
}