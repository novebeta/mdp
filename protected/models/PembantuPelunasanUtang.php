<?php
Yii::import('application.models._base.BasePembantuPelunasanUtang');

class PembantuPelunasanUtang extends BasePembantuPelunasanUtang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->pembantu_pelunasan_utang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pembantu_pelunasan_utang_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }

    public function updateTgl($tgl)
    {
        $this->tgl = $tgl;
        if (!$this->save()) {
            throw new Exception(CHtml::errorSummary($this));
        }
        GlTrans::model()->updateAll(['tran_date' => $tgl], "type = :type_no AND type_no = :trans_no",
            array(':type_no' => SUPPIN, ':trans_no' => $this->pembantu_pelunasan_utang_id));
        BankTrans::model()->updateAll(['tgl' => $tgl], 'type_ = :type_ AND trans_no = :trans_no',
            [':type_' => SUPPIN, ':trans_no' => $this->pembantu_pelunasan_utang_id]);
    }
}