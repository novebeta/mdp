<?php
Yii::import('application.models._base.BasePaketPerawatanDetails');

class PaketPerawatanDetails extends BasePaketPerawatanDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_perawatan_details == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_perawatan_details = $uuid;
        }
        return parent::beforeValidate();
    }
}