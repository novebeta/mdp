<?php
Yii::import( 'application.models._base.BaseSalesOrder' );
class SalesOrder extends BaseSalesOrder {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function primaryKey() {
		return 'sales_id';
	}
}