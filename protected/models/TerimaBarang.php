<?php
Yii::import('application.models._base.BaseTerimaBarang');
class TerimaBarang extends BaseTerimaBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->terima_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        
        return parent::beforeValidate();
    }
    static function get_details_to_print($terima_barang_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                b.kode_barang,
                b.nama_barang,
                tid.qty,
                b.sat
            FROM nscc_terima_barang_details tid
                LEFT JOIN nscc_barang b ON b.barang_id = tid.barang_id
            WHERE 
                tid.terima_barang_id = :terima_barang_id
                AND tid.visible = 1
            ORDER BY tid.seq
        ");
        return $comm->queryAll(true, array(':terima_barang_id' => $terima_barang_id));
    }

    public function isInvoiced(){
        $cmd = DbCmd::instance()
            ->addSelect('(tbd.qty-IFNULL(t2.qty_invoiced, 0)) qty')
            ->addFrom('{{terima_barang_details}} tbd')
            ->addLeftJoin(
                DbCmd::instance()
                    ->addSelect([
                        'ti.terima_barang_id',
                        'tid.barang_id',
                        'tid.item_id',
                        'SUM(tid.qty) qty_invoiced',
                    ])
                    ->addFrom('{{transfer_item_details}} tid')
                    ->addLeftJoin('{{transfer_item}} ti', 'tid.transfer_item_id = ti.transfer_item_id')
                    ->addCondition('ti.terima_barang_id = :terima_barang_id')
                    ->addCondition('tid.visible = 1')
                    ->addGroup('ti.terima_barang_id, tid.barang_id, tid.item_id')
                    ->setAs('t2')
                , 't2.terima_barang_id = tbd.terima_barang_id AND t2.barang_id = tbd.barang_id AND t2.item_id = tbd.item_id')
            ->addCondition('tbd.terima_barang_id = :terima_barang_id')
            ->addCondition('tbd.visible = 1')
            ->addParams([':terima_barang_id'=>$this->terima_barang_id])
            ->addHaving('qty > 0');

        return !(count($cmd->queryAll())>0);
    }
}