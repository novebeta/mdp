<?php
Yii::import('application.models._base.BasePaketPerawatan');

class PaketPerawatan extends BasePaketPerawatan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_perawatan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_perawatan_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function gethargapaket($barang)
    {
	    $param[':barang'] = $barang;

    	$comm = Yii::app()->db->createCommand("SELECT pp.*, GROUP_CONCAT(ppd.payment_percent ORDER BY ppd.trans_ke ASC) AS payment_percent  FROM nscc_paket_perawatan AS pp
			INNER JOIN nscc_paket_perawatan_details AS ppd ON pp.paket_perawatan_id=ppd.paket_perawatan_id
			WHERE pp.barang_id=:barang AND ppd.visible=1
			GROUP BY pp.paket_perawatan_id");

	    return $comm->queryRow(true, $param);
    }


}