<?php
Yii::import('application.models._base.BaseMobilTipe');

class MobilTipe extends BaseMobilTipe
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->mobil_tipe_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->mobil_tipe_id = $uuid;
        }
        return parent::beforeValidate();
    }
}