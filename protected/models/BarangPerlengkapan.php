<?php
Yii::import('application.models._base.BaseBarangPerlengkapan');
class BarangPerlengkapan extends BaseBarangPerlengkapan
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->barang_perlengkapan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_perlengkapan_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function count_biaya_beli($unit, $harga, $store)
    {
        $barang = CostBarangPerlengkapan::model()->findByAttributes(array('barang_perlengkapan_id' => $this->barang_perlengkapan_id, 'store_kode' => $store));
        if ($barang === NULL) {
            $barang = new CostBarangPerlengkapan();
            $barang->barang_perlengkapan_id = $this->barang_perlengkapan_id;
            $barang->store_kode = $store;
            $barang->cost = $harga / $unit;
        } else {
            $jml_stok = StockMovesPerlengkapan::get_saldo_item($this->barang_perlengkapan_id, $store);
            $harga_lama = $barang->cost;
            $total_lama = round($jml_stok * $harga_lama, 2);
            $total_baru = $harga;
            $jml_baru = $jml_stok + $unit;
            $harga_baru = round(($total_baru + $total_lama) / $jml_baru, 2);
            $barang->cost = $harga_baru;
        }
        if (!$barang->save()) {
            throw new Exception(t('save.fail', 'app') . CHtml::errorSummary($this));
        }
    }
    public function get_cost($store)
    {
        $cbp = CostBarangPerlengkapan::model()->findByAttributes(array('barang_perlengkapan_id' => $this->barang_perlengkapan_id, 'store_kode' => $store));
        if ($cbp == null) {
            return 0;
        } else {
            return $cbp->cost;
        }
    }
}