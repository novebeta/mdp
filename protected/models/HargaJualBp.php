<?php
Yii::import('application.models._base.BaseHargaJualBp');

class HargaJualBp extends BaseHargaJualBp
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->harga_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->harga_id = $uuid;
        }
        return parent::beforeValidate();
    }
}