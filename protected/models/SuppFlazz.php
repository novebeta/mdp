<?php
Yii::import('application.models._base.BaseSuppFlazz');

class SuppFlazz extends BaseSuppFlazz
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->supp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->supp_id = $uuid;
        }
        return parent::beforeValidate();
    }
}