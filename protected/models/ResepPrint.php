<?php
Yii::import('application.models._base.BaseResepPrint');
class ResepPrint extends BaseResepPrint
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->resep_print_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->resep_print_id = $uuid;
        }
        return parent::beforeValidate();
    }
}