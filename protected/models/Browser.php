<?php
Yii::import( 'application.models._base.BaseBrowser' );
class Browser extends BaseBrowser {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->browser_id == null ) {
			$command          = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid             = $command->queryScalar();
			$this->browser_id = $uuid;
		}
		if ( $this->session == null ) {
			$this->session = Yii::app()->user->getSession();
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public static function saveInfo(
		$ip_local, $ip_public, $browserdata, $fingerprint, $useragent,
		$browser, $browserversion, $browsermajorversion, $ie, $chrome, $firefox, $safari,
		$opera, $engine, $engineversion, $os, $osversion, $windows, $mac_os, $linux, $ubuntu,
		$solaris, $device, $devicetype, $devicevendor, $cpu, $mobile, $mobilemajor, $mobileandroid,
		$mobileopera, $mobilewindows, $mobileblackberry, $mobileios, $iphone, $ipad, $ipod, $screenprint,
		$colordepth, $currentresolution, $availableresolution, $devicexdpi, $deviceydpi, $plugins, $java,
		$javaversion, $flash, $flashversion, $silverlight, $silverlightversion, $mimetypes, $ismimetypes,
		$font, $fonts, $localstorage, $sessionstorage, $cookie, $timezone, $language, $systemlanguage, $canvas
	) {
		$model                      = new Browser;
		$model->ip_local            = $ip_local;
		$model->ip_public           = $ip_public;
		$model->browserdata         = $browserdata;
		$model->fingerprint         = $fingerprint;
		$model->useragent           = $useragent;
		$model->browser             = $browser;
		$model->browserversion      = $browserversion;
		$model->browsermajorversion = $browsermajorversion;
		$model->ie                  = intval($ie);
		$model->chrome              = intval($chrome);
		$model->firefox             = intval($firefox);
		$model->safari              = intval($safari);
		$model->opera               = intval($opera);
		$model->engine_             = $engine;
		$model->engineversion       = $engineversion;
		$model->os                  = $os;
		$model->osversion           = $osversion;
		$model->windows             = intval($windows);
		$model->mac_os              = intval($mac_os);
		$model->linux               = intval($linux);
		$model->ubuntu              = intval($ubuntu);
		$model->solaris             = intval($solaris);
		$model->device              = $device;
		$model->devicetype          = $devicetype;
		$model->devicevendor        = $devicevendor;
		$model->cpu                 = $cpu;
		$model->mobile              = intval($mobile);
		$model->mobilemajor         = intval($mobilemajor);
		$model->mobileandroid       = intval($mobileandroid);
		$model->mobileopera         = intval($mobileopera);
		$model->mobilewindows       = intval($mobilewindows);
		$model->mobileblackberry    = intval($mobileblackberry);
		$model->mobileios           = intval($mobileios);
		$model->iphone              = intval($iphone);
		$model->ipad                = intval($ipad);
		$model->ipod                = intval($ipod);
		$model->screenprint         = $screenprint;
		$model->colordepth          = $colordepth;
		$model->currentresolution   = $currentresolution;
		$model->availableresolution = $availableresolution;
		$model->devicexdpi          = $devicexdpi;
		$model->deviceydpi          = $deviceydpi;
		$model->plugins             = $plugins;
		$model->java                = intval($java);
		$model->javaversion         = intval($javaversion);
		$model->flash               = intval($flash);
		$model->flashversion        = $flashversion;
		$model->silverlight         = intval($silverlight);
		$model->silverlightversion  = $silverlightversion;
		$model->mimetypes           = $mimetypes;
		$model->ismimetypes         = intval($ismimetypes);
		$model->font                = intval($font);
		$model->fonts               = $fonts;
		$model->localstorage        = intval($localstorage);
		$model->sessionstorage      = intval($sessionstorage);
		$model->cookie              = intval($cookie);
		$model->timezone            = $timezone;
		$model->language            = $language;
		$model->systemlanguage      = $systemlanguage;
		$model->canvas              = intval($canvas);
		$model->save();
//		$error = CHtml::errorSummary($model);
		return $model;
	}
}