<?php
Yii::import('application.models._base.BaseUserEmployee');
class UserEmployee extends BaseUserEmployee
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->user_employee_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->user_employee_id = $uuid;
        }
        return parent::beforeValidate();
    }
}