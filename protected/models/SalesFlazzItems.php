<?php
Yii::import('application.models._base.BaseSalesFlazzItems');

class SalesFlazzItems extends BaseSalesFlazzItems
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->sales_flazz_item_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_flazz_item_id = $uuid;
        }
        return parent::beforeValidate();
    }

    /*
     * INSERT INTO `mern8546_app`.`nscc_sales_flazz_items`(`sales_flazz_item_id`, `note`, `total`, `disc`, `discrp`, `bruto`, `vat`, `up`, `total_pot`, `total_discrp1`, `barang_id`, `qty`, `harga`, `vatrp`, `harga_beli`, `sales_flazz_id`)
SELECT UUID(),'',nsf.total,nsf.disc,nsf.discrp,nsf.bruto,nsf.vat,0,nsf.total_pot,nsf.total_discrp1,
nsf.barang_id, nsf.qty, nsf.harga,nsf.vatrp,nsf.harga_beli,nsf.sales_flazz_id
FROM nscc_sales_flazz nsf
     */

}