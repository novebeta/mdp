<?php
Yii::import('application.models._base.BaseMobilView');

class MobilView extends BaseMobilView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function primaryKey()
	{
		return 'mobil_id';
	}
}