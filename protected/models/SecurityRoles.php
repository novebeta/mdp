<?php

Yii::import('application.models._base.BaseSecurityRoles');

class SecurityRoles extends BaseSecurityRoles
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->security_roles_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->security_roles_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function chksecrole($id ) {
        $comm = Yii::app()->db->createCommand( "select s.security_roles_id from nscc_security_roles s
                                                where s.security_roles_id = :id
                                                and s.sections like '%9996%'" );
        return $comm->queryScalar( array( ':id' => $id ) );
    }


}