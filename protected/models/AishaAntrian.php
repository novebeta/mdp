<?php
Yii::import('application.models._base.BaseAishaAntrian');
class AishaAntrian extends BaseAishaAntrian
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->id_antrian == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id_antrian = $uuid;
        }
        if ( $this->tanggal == null ) {
            $this->tanggal = new CDbExpression( 'NOW()' );
        }
        if ( $this->timestamp == null ) {
            $this->timestamp = new CDbExpression( 'NOW()' );
        }
        return parent::beforeValidate();
    }
}