<?php

Yii::import('application.models._base.BaseClinicalIn');

class ClinicalIn extends BaseClinicalIn
{
    public function primaryKey()
    {
        return 'tipe_clinical_id';
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}