<?php
Yii::import('application.models._base.BaseTransferBarangMasukDetail');

class TransferBarangMasukDetail extends BaseTransferBarangMasukDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->transfer_barang_masuk_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_barang_masuk_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}