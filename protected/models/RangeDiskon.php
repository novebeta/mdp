<?php
Yii::import( 'application.models._base.BaseRangeDiskon' );
class RangeDiskon extends BaseRangeDiskon {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->range_diskon_id == null ) {
			$command               = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                  = $command->queryScalar();
			$this->range_diskon_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public function nBeetween( $value ) {
		if ( $value < $this->nilai_awal ) {
			return false;
		}
		if ( $value > $this->nilai_akhir ) {
			return false;
		}
		return true;
	}
	public static function getDiskon( $jml, $store ) {
		/** @var RangeDiskon $item */
		foreach ( RangeDiskon::model()->findAll( 'store_kode = :store', [ ':store' => $store ] ) as $item ) {
			if ( $item->nBeetween( $jml ) ) {
				return $item->disc;
			}
		}
		return 0;
	}
}