<?php
Yii::import('application.models._base.BaseMobilKategori');

class MobilKategori extends BaseMobilKategori
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->mobil_kategori_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->mobil_kategori_id = $uuid;
        }
        return parent::beforeValidate();
    }
}