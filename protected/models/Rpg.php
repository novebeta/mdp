<?php
Yii::import('application.models._base.BaseRpg');

class Rpg extends BaseRpg
{
    public function primaryKey()
    {
        return 'rpg_id';
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function create_rpj($salestrans_id, $tgl, $amount, $bank_id, $note, $parent_id){
        $model = new Ulpt;
        $ref = new Reference();
        $docref = $ref->get_next_reference(RPG);
        $model->salestrans_id = $salestrans_id;
        $model->tgl = $tgl;
        $model->amount = -$amount;
        $model->bank_id = $bank_id;
        $model->note = $note;
        $model->store = STOREID;
        $model->tipe = TIPE_RPG;
        $model->doc_ref = $docref;
        $model->parent_id = $parent_id;
        if (!$model->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'RPG')) . CHtml::errorSummary($model));
        }
        $ulpt = Ulpt::model()->findByPk($model->parent_id);
        if ($model->salestrans_id != null) {
            $sales = Salestrans::model()->findByPk($ulpt->salestrans_id);
            if (!$sales->is_customer_equal($model->salestrans_id)) {
                throw new Exception("RPG harus digunakan oleh pemilik ULPT.");
            }
            if($model->salestrans_id == $ulpt->salestrans_id){
                throw new Exception("RPG tidak bisa digunakan pada transaksi yang sama dengan ULPT.");
            }
        }
        $sisa = $ulpt->get_sisa();
        $before = $sisa - $model->amount;
        if ($sisa < 0) {
            throw new Exception("RPG gagal karena lebih besar dari sisa ULPT. Sisa ULPT " .
                number_format($before, 2));
        }
        if ($sisa == 0) {
            $ulpt->lunas = 1;
            if (!$ulpt->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'ULPT')) . CHtml::errorSummary($ulpt));
            }
        }
        $ref->save(RPG, $model->ulpt_id, $docref);
        /** @var Ulpt $ulpt */
        $ulpt = Ulpt::model()->findByPk($parent_id);
        /** @var Salestrans $sales */
        $sales = Salestrans::model()->findByPk($salestrans_id);
	    $cust = Customers::model()->findByPk($sales->customer_id);
        $memo = "RPG $docref - ".$ulpt->doc_ref.' '.$cust->nama_customer.' '.$note;
        $gl = new GL();
        $bank = Bank::model()->findByPk($model->bank_id);
        $gl->add_gl(RPG, $model->ulpt_id, $model->tgl, $docref, $bank->account_code,
            $memo, $memo, $model->amount, 0, $model->store);
        if($gl->is_bank_account($bank->account_code)){
            $gl->add_bank_trans(RPG,$model->ulpt_id, $bank->bank_id, $docref,  $model->tgl,
                $model->amount, $model->id_user,  $model->store);
        }
        $gl->add_gl(RPG, $model->ulpt_id, $model->tgl, $docref, COA_ULPT,
            $memo, $memo, -$model->amount, 1, $model->store);
        $gl->validate();
    }
    
    public function generateGltransRpj($val) {
//	public function generateGltransKas() {
		$gl                   = new GL();
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		try {
//			$sales = Salestrans::model()->findByPk($salestrans_id);
                    $model = $val;
                    $docref = $model->doc_ref;
                    $memo = $model->note;
                    
                    $bank = Bank::model()->findByPk($model->bank_id);
                    $gl->add_gl(RPG, $model->rpg_id, $model->tgl, $docref, $bank->account_code,
                        $memo, $memo, -$model->amount, 0, $model->store);
                    
                    $gl->add_gl(RPG, $model->rpg_id, $model->tgl, $docref, COA_ULPT,
                        $memo, $memo, $model->amount, 1, $model->store);
                    $gl->validate();
                    $status = true;
                    $msg = 'sukses';
                    $transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
                        echo $msg;
		}
		app()->db->autoCommit = true;
		return [
			'status' => $status,
			'msg'    => $msg
		];
	}
}