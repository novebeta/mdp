<?php
Yii::import('application.models._base.BaseBeautyOndutyStatus');
class BeautyOndutyStatus extends BaseBeautyOndutyStatus
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'beauty_id';
    }
}