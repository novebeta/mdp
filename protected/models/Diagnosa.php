<?php
Yii::import('application.models._base.BaseDiagnosa');
class Diagnosa extends BaseDiagnosa
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->diagnosa_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->diagnosa_id = $uuid;
        }
        return parent::beforeValidate();
    }
    protected function afterSave()
    {
        parent::afterSave();
//        if (UPLOAD_NARS) {
//            U::runCommandNars('diagnosaPasien', '--id=' . $this->diagnosa_id);
//        }
    }
}