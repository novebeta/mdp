<?php
Yii::import('application.models._base.BaseBpSuppParts');

class BpSuppParts extends BaseBpSuppParts
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->bp_supp_parts_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bp_supp_parts_id = $uuid;
        }
        return parent::beforeValidate();
    }
}