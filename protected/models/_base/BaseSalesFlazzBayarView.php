<?php

/**
 * This is the model base class for the table "{{sales_flazz_bayar_view}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SalesFlazzBayarView".
 *
 * Columns in table "{{sales_flazz_bayar_view}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $nama_bank
 * @property string $doc_ref
 * @property string $lunas
 * @property string $amount
 * @property string $note
 * @property string $tdate
 * @property string $user_id
 * @property string $sales_flazz_bayar_id
 * @property string $tgl
 * @property string $nama_customer
 *
 */
abstract class BaseSalesFlazzBayarView extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{sales_flazz_bayar_view}}';
	}

	public static function representingColumn() {
		return 'nama_bank';
	}

	public function rules() {
		return array(
			array('nama_bank, lunas, amount, tdate, user_id, sales_flazz_bayar_id, tgl, nama_customer', 'required'),
			array('nama_bank, nama_customer', 'length', 'max'=>100),
			array('doc_ref, user_id', 'length', 'max'=>50),
			array('amount', 'length', 'max'=>15),
			array('sales_flazz_bayar_id', 'length', 'max'=>36),
			array('note', 'safe'),
			array('doc_ref, note', 'default', 'setOnEmpty' => true, 'value' => null),
			array('nama_bank, doc_ref, lunas, amount, note, tdate, user_id, sales_flazz_bayar_id, tgl, nama_customer', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'nama_bank' => Yii::t('app', 'Nama Bank'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'lunas' => Yii::t('app', 'Lunas'),
			'amount' => Yii::t('app', 'Amount'),
			'note' => Yii::t('app', 'Note'),
			'tdate' => Yii::t('app', 'Tdate'),
			'user_id' => Yii::t('app', 'User'),
			'sales_flazz_bayar_id' => Yii::t('app', 'Sales Flazz Bayar'),
			'tgl' => Yii::t('app', 'Tgl'),
			'nama_customer' => Yii::t('app', 'Nama Customer'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('nama_bank', $this->nama_bank, true);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('lunas', $this->lunas, true);
		$criteria->compare('amount', $this->amount, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('sales_flazz_bayar_id', $this->sales_flazz_bayar_id, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('nama_customer', $this->nama_customer, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}