<?php

/**
 * This is the model base class for the table "{{paket_perawatan_details}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PaketPerawatanDetails".
 *
 * Columns in table "{{paket_perawatan_details}}" available as properties of the model,
 * followed by relations of table "{{paket_perawatan_details}}" available as properties of the model.
 *
 * @property string $paket_perawatan_details
 * @property string $paket_perawatan_id
 * @property integer $trans_ke
 * @property integer $payment_percent
 * @property integer $visible
 *
 * @property PaketPerawatan $paketPerawatan
 */
abstract class BasePaketPerawatanDetails extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{paket_perawatan_details}}';
	}

	public static function representingColumn() {
		return 'paket_perawatan_details';
	}

	public function rules() {
		return array(
			array('paket_perawatan_details', 'required'),
			array('trans_ke, payment_percent, visible', 'numerical', 'integerOnly'=>true),
			array('paket_perawatan_details, paket_perawatan_id', 'length', 'max'=>36),
			array('paket_perawatan_id, trans_ke, payment_percent, visible', 'default', 'setOnEmpty' => true, 'value' => null),
			array('paket_perawatan_details, paket_perawatan_id, trans_ke, payment_percent, visible', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'paketPerawatan' => array(self::BELONGS_TO, 'PaketPerawatan', 'paket_perawatan_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'paket_perawatan_details' => Yii::t('app', 'Paket Perawatan Details'),
			'paket_perawatan_id' => Yii::t('app', 'Paket Perawatan'),
			'trans_ke' => Yii::t('app', 'Trans Ke'),
			'payment_percent' => Yii::t('app', 'Payment Percent'),
			'visible' => Yii::t('app', 'Visible'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('paket_perawatan_details', $this->paket_perawatan_details, true);
		$criteria->compare('paket_perawatan_id', $this->paket_perawatan_id);
		$criteria->compare('trans_ke', $this->trans_ke);
		$criteria->compare('payment_percent', $this->payment_percent);
		$criteria->compare('visible', $this->visible);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}