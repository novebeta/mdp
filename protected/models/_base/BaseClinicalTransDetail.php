<?php

/**
 * This is the model base class for the table "{{clinical_trans_detail}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ClinicalTransDetail".
 *
 * Columns in table "{{clinical_trans_detail}}" available as properties of the model,
 * followed by relations of table "{{clinical_trans_detail}}" available as properties of the model.
 *
 * @property string $clinical_trans_detail_id
 * @property integer $qty
 * @property string $note
 * @property string $clinical_trans_id
 * @property string $barang_clinical
 * @property integer $sisa
 * @property integer $awal
 *
 * @property ClinicalTrans $clinicalTrans
 * @property BarangClinical $barangClinical
 */
abstract class BaseClinicalTransDetail extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{clinical_trans_detail}}';
	}

	public static function representingColumn() {
		return 'clinical_trans_detail_id';
	}

	public function rules() {
		return array(
			array('clinical_trans_detail_id, clinical_trans_id, barang_clinical', 'required'),
			array('qty, sisa, awal', 'numerical', 'integerOnly'=>true),
			array('clinical_trans_detail_id, clinical_trans_id, barang_clinical', 'length', 'max'=>36),
			array('note', 'length', 'max'=>600),
			array('qty, note, sisa, awal', 'default', 'setOnEmpty' => true, 'value' => null),
			array('clinical_trans_detail_id, qty, note, clinical_trans_id, barang_clinical, sisa, awal', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'clinicalTrans' => array(self::BELONGS_TO, 'ClinicalTrans', 'clinical_trans_id'),
			'barangClinical' => array(self::BELONGS_TO, 'BarangClinical', 'barang_clinical'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'clinical_trans_detail_id' => Yii::t('app', 'Clinical Trans Detail'),
			'qty' => Yii::t('app', 'Qty'),
			'note' => Yii::t('app', 'Note'),
			'clinical_trans_id' => Yii::t('app', 'Clinical Trans'),
			'barang_clinical' => Yii::t('app', 'Barang Clinical'),
			'sisa' => Yii::t('app', 'Sisa'),
			'awal' => Yii::t('app', 'Awal'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('clinical_trans_detail_id', $this->clinical_trans_detail_id, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('clinical_trans_id', $this->clinical_trans_id);
		$criteria->compare('barang_clinical', $this->barang_clinical);
		$criteria->compare('sisa', $this->sisa);
		$criteria->compare('awal', $this->awal);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}