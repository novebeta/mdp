<?php

/**
 * This is the model base class for the table "{{harga_jual_bp}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "HargaJualBp".
 *
 * Columns in table "{{harga_jual_bp}}" available as properties of the model,
 * followed by relations of table "{{harga_jual_bp}}" available as properties of the model.
 *
 * @property string $harga_id
 * @property string $harga
 * @property string $jasa_bp_id
 * @property string $mobil_kategori_id
 * @property string $discrp
 * @property double $disc
 * @property string $tipe
 * @property string $hpp
 *
 * @property JasaBp $jasaBp
 * @property MobilKategoriBp $mobilKategori
 */
abstract class BaseHargaJualBp extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{harga_jual_bp}}';
	}

	public static function representingColumn() {
		return 'harga';
	}

	public function rules() {
		return array(
			array('harga_id, jasa_bp_id, tipe', 'required'),
			array('disc', 'numerical'),
			array('harga_id, jasa_bp_id, mobil_kategori_id', 'length', 'max'=>36),
			array('harga, discrp, hpp', 'length', 'max'=>15),
			array('tipe', 'length', 'max'=>10),
			array('harga, mobil_kategori_id, discrp, disc, hpp', 'default', 'setOnEmpty' => true, 'value' => null),
			array('harga_id, harga, jasa_bp_id, mobil_kategori_id, discrp, disc, tipe, hpp', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'jasaBp' => array(self::BELONGS_TO, 'JasaBp', 'jasa_bp_id'),
			'mobilKategori' => array(self::BELONGS_TO, 'MobilKategoriBp', 'mobil_kategori_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'harga_id' => Yii::t('app', 'Harga'),
			'harga' => Yii::t('app', 'Harga'),
			'jasa_bp_id' => Yii::t('app', 'Jasa Bp'),
			'mobil_kategori_id' => Yii::t('app', 'Mobil Kategori'),
			'discrp' => Yii::t('app', 'Discrp'),
			'disc' => Yii::t('app', 'Disc'),
			'tipe' => Yii::t('app', 'Tipe'),
			'hpp' => Yii::t('app', 'Hpp'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('harga_id', $this->harga_id, true);
		$criteria->compare('harga', $this->harga, true);
		$criteria->compare('jasa_bp_id', $this->jasa_bp_id);
		$criteria->compare('mobil_kategori_id', $this->mobil_kategori_id);
		$criteria->compare('discrp', $this->discrp, true);
		$criteria->compare('disc', $this->disc);
		$criteria->compare('tipe', $this->tipe, true);
		$criteria->compare('hpp', $this->hpp, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}