<?php

/**
 * This is the model base class for the table "{{assign_employees}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AssignEmployees".
 *
 * Columns in table "{{assign_employees}}" available as properties of the model,
 * followed by relations of table "{{assign_employees}}" available as properties of the model.
 *
 * @property string $assign_employee_id
 * @property string $employee_id
 * @property string $tipe_employee_id
 * @property string $tgl
 * @property string $tdate
 *
 * @property TipeEmployee $tipeEmployee
 * @property Employees $employee
 */
abstract class BaseAssignEmployees extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{assign_employees}}';
	}

	public static function representingColumn() {
		return 'tgl';
	}

	public function rules() {
		return array(
			array('assign_employee_id, employee_id, tipe_employee_id, tgl, tdate', 'required'),
			array('assign_employee_id, employee_id, tipe_employee_id', 'length', 'max'=>36),
			array('assign_employee_id, employee_id, tipe_employee_id, tgl, tdate', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'tipeEmployee' => array(self::BELONGS_TO, 'TipeEmployee', 'tipe_employee_id'),
			'employee' => array(self::BELONGS_TO, 'Employees', 'employee_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'assign_employee_id' => Yii::t('app', 'Assign Employee'),
			'employee_id' => Yii::t('app', 'Employee'),
			'tipe_employee_id' => Yii::t('app', 'Tipe Employee'),
			'tgl' => Yii::t('app', 'Tgl'),
			'tdate' => Yii::t('app', 'Tdate'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('assign_employee_id', $this->assign_employee_id, true);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('tipe_employee_id', $this->tipe_employee_id);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('tdate', $this->tdate, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}