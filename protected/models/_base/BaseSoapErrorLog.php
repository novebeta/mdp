<?php

/**
 * This is the model base class for the table "{{soap_error_log}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SoapErrorLog".
 *
 * Columns in table "{{soap_error_log}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $table
 * @property string $table_id
 * @property string $note
 * @property string $from
 * @property string $to
 * @property string $created
 *
 */
abstract class BaseSoapErrorLog extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{soap_error_log}}';
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('id', 'required'),
			array('id, table, table_id, from, to, created', 'length', 'max'=>255),
			array('note', 'safe'),
			array('table, table_id, note, from, to, created', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, table, table_id, note, from, to, created', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'table' => Yii::t('app', 'Table'),
			'table_id' => Yii::t('app', 'Table'),
			'note' => Yii::t('app', 'Note'),
			'from' => Yii::t('app', 'From'),
			'to' => Yii::t('app', 'To'),
			'created' => Yii::t('app', 'Created'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('table', $this->table, true);
		$criteria->compare('table_id', $this->table_id, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('from', $this->from, true);
		$criteria->compare('to', $this->to, true);
		$criteria->compare('created', $this->created, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}