<?php

/**
 * This is the model base class for the table "{{no_faktur}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "NoFaktur".
 *
 * Columns in table "{{no_faktur}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $no_faktur_id
 * @property string $no_faktur
 * @property string $mulai_berlaku
 * @property integer $tipe_trans
 * @property string $id_trans
 * @property integer $created_at
 * @property integer $modified_at
 * @property string $created_user
 * @property string $modified_user
 *
 */
abstract class BaseNoFaktur extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{no_faktur}}';
	}

	public static function representingColumn() {
		return 'no_faktur';
	}

	public function rules() {
		return array(
			array('no_faktur_id, no_faktur, mulai_berlaku, created_user', 'required'),
			array('tipe_trans, created_at, modified_at', 'numerical', 'integerOnly'=>true),
			array('no_faktur_id, id_trans', 'length', 'max'=>36),
			array('no_faktur', 'length', 'max'=>20),
			array('created_user, modified_user', 'length', 'max'=>50),
			array('tipe_trans, id_trans, created_at, modified_at, modified_user', 'default', 'setOnEmpty' => true, 'value' => null),
			array('no_faktur_id, no_faktur, mulai_berlaku, tipe_trans, id_trans, created_at, modified_at, created_user, modified_user', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'no_faktur_id' => Yii::t('app', 'No Faktur'),
			'no_faktur' => Yii::t('app', 'No Faktur'),
			'mulai_berlaku' => Yii::t('app', 'Mulai Berlaku'),
			'tipe_trans' => Yii::t('app', 'Tipe Trans'),
			'id_trans' => Yii::t('app', 'Id Trans'),
			'created_at' => Yii::t('app', 'Created At'),
			'modified_at' => Yii::t('app', 'Modified At'),
			'created_user' => Yii::t('app', 'Created User'),
			'modified_user' => Yii::t('app', 'Modified User'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('no_faktur_id', $this->no_faktur_id, true);
		$criteria->compare('no_faktur', $this->no_faktur, true);
		$criteria->compare('mulai_berlaku', $this->mulai_berlaku, true);
		$criteria->compare('tipe_trans', $this->tipe_trans);
		$criteria->compare('id_trans', $this->id_trans, true);
		$criteria->compare('created_at', $this->created_at);
		$criteria->compare('modified_at', $this->modified_at);
		$criteria->compare('created_user', $this->created_user, true);
		$criteria->compare('modified_user', $this->modified_user, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}