<?php

/**
 * This is the model base class for the table "{{produksi_detil}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProduksiDetil".
 *
 * Columns in table "{{produksi_detil}}" available as properties of the model,
 * followed by relations of table "{{produksi_detil}}" available as properties of the model.
 *
 * @property string $produksi_detil_id
 * @property integer $qty
 * @property string $price
 * @property string $produksi_id
 * @property string $barang_id
 * @property string $total
 *
 * @property Produksi $produksi
 * @property Barang $barang
 */
abstract class BaseProduksiDetil extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{produksi_detil}}';
	}

	public static function representingColumn() {
        return 'price';
	}

	public function rules() {
		return array(
			array('produksi_detil_id, produksi_id, barang_id', 'required'),
			array('qty', 'numerical', 'integerOnly'=>true),
			array('produksi_detil_id, produksi_id, barang_id', 'length', 'max'=>36),
            array('price, total', 'length', 'max' => 30),
			array('qty, price, total', 'default', 'setOnEmpty' => true, 'value' => null),
			array('produksi_detil_id, qty, price, produksi_id, barang_id, total', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'produksi' => array(self::BELONGS_TO, 'Produksi', 'produksi_id'),
			'barang' => array(self::BELONGS_TO, 'Barang', 'barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'produksi_detil_id' => Yii::t('app', 'Produksi Detil'),
			'qty' => Yii::t('app', 'Qty'),
			'price' => Yii::t('app', 'Price'),
			'produksi_id' => Yii::t('app', 'Produksi'),
			'barang_id' => Yii::t('app', 'Barang'),
			'total' => Yii::t('app', 'Total'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('produksi_detil_id', $this->produksi_detil_id, true);
		$criteria->compare('qty', $this->qty);
        $criteria->compare('price', $this->price, true);
		$criteria->compare('produksi_id', $this->produksi_id);
		$criteria->compare('barang_id', $this->barang_id);
        $criteria->compare('total', $this->total, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}