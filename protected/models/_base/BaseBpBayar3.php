<?php

/**
 * This is the model base class for the table "{{bp_bayar3}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "BpBayar3".
 *
 * Columns in table "{{bp_bayar3}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $bp_bayar3_id
 * @property string $tgl
 * @property string $total
 * @property string $bank_id
 * @property string $id_user
 * @property string $ref
 * @property string $body_paint_id
 *
 */
abstract class BaseBpBayar3 extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{bp_bayar3}}';
	}

	public static function representingColumn() {
		return 'body_paint_id';
	}

	public function rules() {
		return array(
			array('bp_bayar3_id, body_paint_id', 'required'),
			array('bp_bayar3_id, bank_id, body_paint_id', 'length', 'max'=>36),
			array('total', 'length', 'max'=>15),
			array('id_user, ref', 'length', 'max'=>50),
			array('tgl', 'safe'),
			array('tgl, total, bank_id, id_user, ref', 'default', 'setOnEmpty' => true, 'value' => null),
			array('bp_bayar3_id, tgl, total, bank_id, id_user, ref, body_paint_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'bp_bayar3_id' => Yii::t('app', 'Bp Bayar3'),
			'tgl' => Yii::t('app', 'Tgl'),
			'total' => Yii::t('app', 'Total'),
			'bank_id' => Yii::t('app', 'Bank'),
			'id_user' => Yii::t('app', 'Id User'),
			'ref' => Yii::t('app', 'Ref'),
			'body_paint_id' => Yii::t('app', 'Body Paint'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('bp_bayar3_id', $this->bp_bayar3_id, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('total', $this->total, true);
		$criteria->compare('bank_id', $this->bank_id, true);
		$criteria->compare('id_user', $this->id_user, true);
		$criteria->compare('ref', $this->ref, true);
		$criteria->compare('body_paint_id', $this->body_paint_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}