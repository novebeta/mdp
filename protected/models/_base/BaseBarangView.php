<?php

/**
 * This is the model base class for the table "{{barang_view}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "BarangView".
 *
 * Columns in table "{{barang_view}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $barang_id
 * @property string $mobil_kategori_id
 * @property string $nama_barang
 * @property string $harga
 * @property double $disc
 * @property string $discrp
 * @property double $vat
 *
 */
abstract class BaseBarangView extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{barang_view}}';
	}

	public static function representingColumn() {
		return 'barang_id';
	}

	public function rules() {
		return array(
			array('barang_id', 'required'),
			array('disc, vat', 'numerical'),
			array('barang_id, mobil_kategori_id', 'length', 'max'=>36),
			array('nama_barang', 'length', 'max'=>100),
			array('harga, discrp', 'length', 'max'=>15),
			array('mobil_kategori_id, nama_barang, harga, disc, discrp, vat', 'default', 'setOnEmpty' => true, 'value' => null),
			array('barang_id, mobil_kategori_id, nama_barang, harga, disc, discrp, vat', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'barang_id' => Yii::t('app', 'Barang'),
			'mobil_kategori_id' => Yii::t('app', 'Mobil Kategori'),
			'nama_barang' => Yii::t('app', 'Nama Barang'),
			'harga' => Yii::t('app', 'Harga'),
			'disc' => Yii::t('app', 'Disc'),
			'discrp' => Yii::t('app', 'Discrp'),
			'vat' => Yii::t('app', 'Vat'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('mobil_kategori_id', $this->mobil_kategori_id, true);
		$criteria->compare('nama_barang', $this->nama_barang, true);
		$criteria->compare('harga', $this->harga, true);
		$criteria->compare('disc', $this->disc);
		$criteria->compare('discrp', $this->discrp, true);
		$criteria->compare('vat', $this->vat);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}