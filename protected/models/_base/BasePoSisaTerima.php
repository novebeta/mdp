<?php

/**
 * This is the model base class for the table "{{po_sisa_terima}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PoSisaTerima".
 *
 * Columns in table "{{po_sisa_terima}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $po_details_id
 * @property string $po_id
 * @property integer $seq
 * @property string $barang_id
 * @property double $qty
 * @property double $price
 * @property double $total
 * @property double $disc_rp
 * @property double $sub_total
 * @property double $ppn
 * @property double $ppn_rp
 * @property double $pph
 * @property double $pph_rp
 * @property double $disc
 * @property double $total_disc
 * @property double $total_dpp
 * @property string $charge
 * @property integer $visible
 * @property string $item_id
 *
 */
abstract class BasePoSisaTerima extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{po_sisa_terima}}';
	}

	public static function representingColumn() {
		return 'po_details_id';
	}

	public function rules() {
		return array(
			array('po_details_id, po_id, barang_id, price, total, disc_rp, sub_total', 'required'),
			array('seq, visible', 'numerical', 'integerOnly'=>true),
			array('qty, price, total, disc_rp, sub_total, ppn, ppn_rp, pph, pph_rp, disc, total_disc, total_dpp', 'numerical'),
			array('po_details_id, po_id, barang_id, item_id', 'length', 'max'=>36),
			array('charge', 'length', 'max'=>100),
			array('seq, qty, ppn, ppn_rp, pph, pph_rp, disc, total_disc, total_dpp, charge, visible, item_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('po_details_id, po_id, seq, barang_id, qty, price, total, disc_rp, sub_total, ppn, ppn_rp, pph, pph_rp, disc, total_disc, total_dpp, charge, visible, item_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'po_details_id' => Yii::t('app', 'Po Details'),
			'po_id' => Yii::t('app', 'Po'),
			'seq' => Yii::t('app', 'Seq'),
			'barang_id' => Yii::t('app', 'Barang'),
			'qty' => Yii::t('app', 'Qty'),
			'price' => Yii::t('app', 'Price'),
			'total' => Yii::t('app', 'Total'),
			'disc_rp' => Yii::t('app', 'Disc Rp'),
			'sub_total' => Yii::t('app', 'Sub Total'),
			'ppn' => Yii::t('app', 'Ppn'),
			'ppn_rp' => Yii::t('app', 'Ppn Rp'),
			'pph' => Yii::t('app', 'Pph'),
			'pph_rp' => Yii::t('app', 'Pph Rp'),
			'disc' => Yii::t('app', 'Disc'),
			'total_disc' => Yii::t('app', 'Total Disc'),
			'total_dpp' => Yii::t('app', 'Total Dpp'),
			'charge' => Yii::t('app', 'Charge'),
			'visible' => Yii::t('app', 'Visible'),
			'item_id' => Yii::t('app', 'Item'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('po_details_id', $this->po_details_id, true);
		$criteria->compare('po_id', $this->po_id, true);
		$criteria->compare('seq', $this->seq);
		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('price', $this->price);
		$criteria->compare('total', $this->total);
		$criteria->compare('disc_rp', $this->disc_rp);
		$criteria->compare('sub_total', $this->sub_total);
		$criteria->compare('ppn', $this->ppn);
		$criteria->compare('ppn_rp', $this->ppn_rp);
		$criteria->compare('pph', $this->pph);
		$criteria->compare('pph_rp', $this->pph_rp);
		$criteria->compare('disc', $this->disc);
		$criteria->compare('total_disc', $this->total_disc);
		$criteria->compare('total_dpp', $this->total_dpp);
		$criteria->compare('charge', $this->charge, true);
		$criteria->compare('visible', $this->visible);
		$criteria->compare('item_id', $this->item_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}