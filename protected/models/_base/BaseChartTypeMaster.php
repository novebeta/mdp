<?php

/**
 * This is the model base class for the table "{{chart_type_master}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ChartTypeMaster".
 *
 * Columns in table "{{chart_type_master}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $account_code
 * @property string $account_name
 * @property string $deskripsi
 * @property string $kategori
 * @property string $saldo_normal
 * @property string $tipe
 *
 */
abstract class BaseChartTypeMaster extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{chart_type_master}}';
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('id, kategori', 'length', 'max'=>36),
			array('account_code', 'length', 'max'=>15),
			array('account_name', 'length', 'max'=>60),
			array('saldo_normal, tipe', 'length', 'max'=>1),
			array('deskripsi', 'safe'),
			array('id, account_code, account_name, deskripsi, kategori, saldo_normal, tipe', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, account_code, account_name, deskripsi, kategori, saldo_normal, tipe', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'account_code' => Yii::t('app', 'Account Code'),
			'account_name' => Yii::t('app', 'Account Name'),
			'deskripsi' => Yii::t('app', 'Deskripsi'),
			'kategori' => Yii::t('app', 'Kategori'),
			'saldo_normal' => Yii::t('app', 'Saldo Normal'),
			'tipe' => Yii::t('app', 'Tipe'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('account_code', $this->account_code, true);
		$criteria->compare('account_name', $this->account_name, true);
		$criteria->compare('deskripsi', $this->deskripsi, true);
		$criteria->compare('kategori', $this->kategori, true);
		$criteria->compare('saldo_normal', $this->saldo_normal, true);
		$criteria->compare('tipe', $this->tipe, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}