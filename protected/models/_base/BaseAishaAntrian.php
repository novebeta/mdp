<?php

/**
 * This is the model base class for the table "aisha_antrian".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AishaAntrian".
 *
 * Columns in table "aisha_antrian" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id_antrian
 * @property string $nomor_pasien
 * @property string $customer_id
 * @property string $nama_customer
 * @property string $alasan
 * @property integer $nomor_antrian
 * @property integer $hold
 * @property string $bagian
 * @property string $counter
 * @property string $tanggal
 * @property string $timestamp
 * @property string $medis
 * @property integer $end_
 * @property integer $sales
 * @property string $counter_asal
 * @property string $note
 * @property string $counter_cashier
 *
 */
abstract class BaseAishaAntrian extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'aisha_antrian';
	}

	public static function representingColumn() {
		return 'tanggal';
	}

	public function rules() {
		return array(
			array('id_antrian, tanggal, timestamp', 'required'),
			array('nomor_antrian, hold, end_, sales', 'numerical', 'integerOnly'=>true),
			array('id_antrian, customer_id', 'length', 'max'=>36),
			array('nomor_pasien, nama_customer', 'length', 'max'=>100),
			array('alasan, note', 'length', 'max'=>255),
			array('bagian', 'length', 'max'=>20),
			array('counter, medis, counter_asal, counter_cashier', 'length', 'max'=>5),
			array('nomor_pasien, customer_id, nama_customer, alasan, nomor_antrian, hold, bagian, counter, medis, end_, sales, counter_asal, note, counter_cashier', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id_antrian, nomor_pasien, customer_id, nama_customer, alasan, nomor_antrian, hold, bagian, counter, tanggal, timestamp, medis, end_, sales, counter_asal, note, counter_cashier', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id_antrian' => Yii::t('app', 'Id Antrian'),
			'nomor_pasien' => Yii::t('app', 'Nomor Pasien'),
			'customer_id' => Yii::t('app', 'Customer'),
			'nama_customer' => Yii::t('app', 'Nama Customer'),
			'alasan' => Yii::t('app', 'Alasan'),
			'nomor_antrian' => Yii::t('app', 'Nomor Antrian'),
			'hold' => Yii::t('app', 'Hold'),
			'bagian' => Yii::t('app', 'Bagian'),
			'counter' => Yii::t('app', 'Counter'),
			'tanggal' => Yii::t('app', 'Tanggal'),
			'timestamp' => Yii::t('app', 'Timestamp'),
			'medis' => Yii::t('app', 'Medis'),
			'end_' => Yii::t('app', 'End'),
			'sales' => Yii::t('app', 'Sales'),
			'counter_asal' => Yii::t('app', 'Counter Asal'),
			'note' => Yii::t('app', 'Note'),
			'counter_cashier' => Yii::t('app', 'Counter Cashier'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id_antrian', $this->id_antrian, true);
		$criteria->compare('nomor_pasien', $this->nomor_pasien, true);
		$criteria->compare('customer_id', $this->customer_id, true);
		$criteria->compare('nama_customer', $this->nama_customer, true);
		$criteria->compare('alasan', $this->alasan, true);
		$criteria->compare('nomor_antrian', $this->nomor_antrian);
		$criteria->compare('hold', $this->hold);
		$criteria->compare('bagian', $this->bagian, true);
		$criteria->compare('counter', $this->counter, true);
		$criteria->compare('tanggal', $this->tanggal, true);
		$criteria->compare('timestamp', $this->timestamp, true);
		$criteria->compare('medis', $this->medis, true);
		$criteria->compare('end_', $this->end_);
		$criteria->compare('sales', $this->sales);
		$criteria->compare('counter_asal', $this->counter_asal, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('counter_cashier', $this->counter_cashier, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}