<?php

/**
 * This is the model base class for the table "{{dropping_recall_details}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "DroppingRecallDetails".
 *
 * Columns in table "{{dropping_recall_details}}" available as properties of the model,
 * followed by relations of table "{{dropping_recall_details}}" available as properties of the model.
 *
 * @property string $dropping_recall_details_id
 * @property string $dropping_recall_id
 * @property string $dropping_id
 * @property integer $seq
 * @property string $barang_id
 * @property integer $qty
 * @property double $price
 * @property integer $visible
 *
 * @property DroppingRecall $droppingRecall
 * @property Dropping $dropping
 * @property Barang $barang
 */
abstract class BaseDroppingRecallDetails extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{dropping_recall_details}}';
	}

	public static function representingColumn() {
		return 'dropping_recall_details_id';
	}

	public function rules() {
		return array(
			array('dropping_recall_details_id, dropping_recall_id, dropping_id, barang_id', 'required'),
			array('seq, qty, visible', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('dropping_recall_details_id, dropping_recall_id, dropping_id, barang_id', 'length', 'max'=>36),
			array('seq, qty, price, visible', 'default', 'setOnEmpty' => true, 'value' => null),
			array('dropping_recall_details_id, dropping_recall_id, dropping_id, seq, barang_id, qty, price, visible', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'droppingRecall' => array(self::BELONGS_TO, 'DroppingRecall', 'dropping_recall_id'),
			'dropping' => array(self::BELONGS_TO, 'Dropping', 'dropping_id'),
			'barang' => array(self::BELONGS_TO, 'Barang', 'barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'dropping_recall_details_id' => Yii::t('app', 'Dropping Recall Details'),
			'dropping_recall_id' => Yii::t('app', 'Dropping Recall'),
			'dropping_id' => Yii::t('app', 'Dropping'),
			'seq' => Yii::t('app', 'Seq'),
			'barang_id' => Yii::t('app', 'Barang'),
			'qty' => Yii::t('app', 'Qty'),
			'price' => Yii::t('app', 'Price'),
			'visible' => Yii::t('app', 'Visible'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('dropping_recall_details_id', $this->dropping_recall_details_id, true);
		$criteria->compare('dropping_recall_id', $this->dropping_recall_id);
		$criteria->compare('dropping_id', $this->dropping_id);
		$criteria->compare('seq', $this->seq);
		$criteria->compare('barang_id', $this->barang_id);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('price', $this->price);
		$criteria->compare('visible', $this->visible);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}