<?php

/**
 * This is the model base class for the table "{{perawatan_tambahan}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PerawatanTambahan".
 *
 * Columns in table "{{perawatan_tambahan}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id_antrian
 * @property string $salestrans_id
 * @property string $doc_ref
 * @property string $tgl
 * @property string $nama_customer
 * @property string $no_customer
 * @property string $tdate
 * @property string $start_at
 * @property string $end_at
 *
 */
abstract class BasePerawatanTambahan extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{perawatan_tambahan}}';
	}

	public static function representingColumn() {
		return 'salestrans_id';
	}

	public function rules() {
		return array(
			array('id_antrian', 'length', 'max'=>36),
			array('salestrans_id, doc_ref', 'length', 'max'=>50),
			array('nama_customer', 'length', 'max'=>100),
			array('no_customer', 'length', 'max'=>15),
			array('tgl, tdate, start_at, end_at', 'safe'),
			array('id_antrian, salestrans_id, doc_ref, tgl, nama_customer, no_customer, tdate, start_at, end_at', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id_antrian, salestrans_id, doc_ref, tgl, nama_customer, no_customer, tdate, start_at, end_at', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id_antrian' => Yii::t('app', 'Id Antrian'),
			'salestrans_id' => Yii::t('app', 'Salestrans'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'tgl' => Yii::t('app', 'Tgl'),
			'nama_customer' => Yii::t('app', 'Nama Customer'),
			'no_customer' => Yii::t('app', 'No Customer'),
			'tdate' => Yii::t('app', 'Tdate'),
			'start_at' => Yii::t('app', 'Start At'),
			'end_at' => Yii::t('app', 'End At'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id_antrian', $this->id_antrian, true);
		$criteria->compare('salestrans_id', $this->salestrans_id, true);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('nama_customer', $this->nama_customer, true);
		$criteria->compare('no_customer', $this->no_customer, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('start_at', $this->start_at, true);
		$criteria->compare('end_at', $this->end_at, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}