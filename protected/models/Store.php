<?php
Yii::import('application.models._base.BaseStore');
class Store extends BaseStore
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function countStoreBebanAcc()
    {
        $command = Yii::app()->db->createCommand("SELECT COUNT(*) FROM nscc_store s WHERE s.beban_acc = 1;");
        $jml = $command->queryScalar();
        return $jml;
    }
    public static function getStoreBebanAcc($fetchAssociative=true)
    {
        $command = Yii::app()->db->createCommand("SELECT * FROM nscc_store s WHERE s.beban_acc = 1;");
        $jml = $command->queryAll($fetchAssociative);
        return $jml;
    }
}