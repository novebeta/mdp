<?php
Yii::import('application.models._base.BaseInvoiceMdp');

class InvoiceMdp extends BaseInvoiceMdp
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->invoice_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->invoice_id = $uuid;
        }
        return parent::beforeValidate();
    }
}