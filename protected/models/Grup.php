<?php

Yii::import('application.models._base.BaseGrup');
class Grup extends BaseGrup
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->grup_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->grup_id = $uuid;
        }
        return parent::beforeValidate();
    }
}