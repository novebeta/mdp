<?php
Yii::import('application.models._base.BaseResepPayment');
class ResepPayment extends BaseResepPayment
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->resep_payment_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->resep_payment_id = $uuid;
        }
        return parent::beforeValidate();
    }
}