<?php
Yii::import('application.models._base.BaseJasaBp');

class JasaBp extends BaseJasaBp
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->jasa_bp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->jasa_bp_id = $uuid;
        }
        return parent::beforeValidate();
    }
}