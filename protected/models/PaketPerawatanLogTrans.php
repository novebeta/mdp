<?php
Yii::import('application.models._base.BasePaketPerawatanLogTrans');

class PaketPerawatanLogTrans extends BasePaketPerawatanLogTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_perawatan_log_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_perawatan_log_trans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}