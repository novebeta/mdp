<?php

class MobileCommand extends CConsoleCommand{

    public function actionUser($id){
        
        $model = Customers::model()->findByPk($id);
        //print_r($model);
        $data = [];
        $data['phone'] = $model->telp;
        $data['customer_id'] = $model->customer_id;
        $data['no_pasien'] = $model->no_customer;
        $data['outlet_code'] = $model->store;
        $data = JSON_ENCODE($data);

        //$data = array($phone,$id_pasien,$no_pasien,$outlet_code);
        //$output = Yii::app()->curl->post('https://api.natashaskin.net/api/users/sync',$data);

        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/users/sync');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/users/sync',$data);
        //echo $output["status"];
        var_dump($output); 
    }

    /* diganti ke tabel store
    // belum revisi
    */
    public function actionOutlet($id){
        $model = Outlet::model()->findByPk($id);
        $data = [];
        $data['outlet'] = [];
        /* foreach($model as $val){ */
            $data['outlet']['0']['outlet_code'] = $model->code;
            $data['outlet']['0']['outlet_name'] = $model->name;
            $data['outlet']['0']['outlet_address'] = $model->address;
            $data['outlet']['0']['city'] = $model->city;
            $data['outlet']['0']['outlet_postal_code'] =$model->postal_code;
            $data['outlet']['0']['outlet_phone'] = $model->phone;
            $data['outlet']['0']['outlet_email'] = $model->email;
            $data['outlet']['0']['outlet_open_hours'] = $model->open;
            $data['outlet']['0']['outlet_close_hours'] = $model->close;
            
       /*  } */
        
        $data = JSON_ENCODE($data); 
        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/outlet/sync');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/outlet/sync',$data);
        //print_r($output);
        var_dump($output);
    }

    public function actionTreatment($id){
        
        $model = Barang::model()->findByPk($id);
        $data = [];
        $data['treatment'] = [];
        $data['treatment']['0']['treatment_code'] = $model->kode_barang;
        $data['treatment']['0']['treatment_name'] = $model->nama_barang;
        $data['treatment']['0']['treatment_short_description'] = $model->ket;
        $data['treatment']['0']['treatment_long_description'] = $model->ket;
        $data['treatment']['0']['treatment_visibility'] = 'Visible';

        $data = JSON_ENCODE($data); 
        //print_r($data);
        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/treatment/sync');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/treatment/sync',$data);
        var_dump($output);
    }

    public function actionProduct($id){
        
        $model = Barang::model()->findByPk($id);
        
        $data = [];
        $data['product'] = [];
        $data['product']['0']['product_code'] = $model->kode_barang;
        $data['product']['0']['product_name'] = $model->nama_barang;
        $data['product']['0']['product_description'] = $model->ket;
        $data['product']['0']['product_price'] = '0';
        $data['product']['0']['product_weight'] = '0';
        $data['product']['0']['product_visibility'] = 'Visible';
        $data = JSON_ENCODE($data); 
        /* print_r($data);
        file_put_contents('product1.txt',$data); */
        
        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/product/sync');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/product/sync',$data);
        var_dump($output);
        //file_put_contents('product.txt',$output);

    }

    public function actionTransaction($id){
        $model =  Salestrans::model()->findByPk($id);
        /* $criteria = new CDbCriteria;
        $criteria->compare('salestrans_id',$model->salestrans_id);
        $detils = SalestransDetails::model()->findAll($criteria);  */
        $criteria2 = new CDbCriteria;
		$criteria2->compare('salestrans_id',$id);
		$criteria2->with = array('barang.grup');
		$criteria2->addInCondition('grup.kategori_id',array(2,4,5));
		$produk =  SalestransDetails::model()->findAll($criteria2);

		$criteria1 = new CDbCriteria;
		$criteria1->compare('salestrans_id',$id);
		$criteria1->with = array('barang.grup');
		$criteria1->addInCondition('grup.kategori_id',array(1,6));
        $treatment =  SalestransDetails::model()->findAll($criteria1);
        
        $data = [];
        $data['phone'] = $model->customer->telp;
        $data['transaction_date'] = $model->tdate;
        $data['receipt_number'] = $model->doc_ref;
        $data['outlet_code'] = $model->store;
        $data['total'] = $model->bruto;
        $data['vat'] = $model->vat;
        $data['grand_total'] = $model->total;
        $data['doctor_name'] = Employee::model()->findByPk($model->dokter_id)->nama_employee;;
        if($produk != null){
			$data['products'] = [];
			foreach($produk as $prod){
				$row = [];
				$row['product_code'] = $prod->barang->kode_barang;
				$row['product_name'] = $prod->barang->nama_barang;
				$row['product_price'] = get_number($prod->price);
				$row['product_qty'] = get_number($prod->qty);
			}
			array_push($data['products'], $row);
		}
		if($treatment != null){
			$data['treatments'] = [];
			foreach($treatment as $treat){
				$row2 = [];
				$row2['treatment_code'] = $treat->barang->kode_barang;
				$row2['treatment_name'] = $treat->barang->nama_barang;
				$row2['treatment_price'] = get_number($treat->price);
				$row2['treatment_qty'] = get_number($treat->qty);
			}
			array_push($data['treatments'], $row2);
		}
		
        $token = MOBILE_API_TOKEN;
        $auth = array('Authorization:'.$token);
        $output = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get(MOBILE_API_URL.'api/transaction/add');
        $output = Yii::app()->curl->post(MOBILE_API_URL.'api/transaction/add',json_encode($data));
        var_dump($output);
        
    }
        
        

}
?>
