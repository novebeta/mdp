<?php
class UtilsCommand extends CConsoleCommand {
	public function generate_uuid() {
		$command = Yii::app()->db->createCommand( "SELECT UUID();" );
		return $command->queryScalar();
	}
	public function actionSentEmail( $to, $from, $fromAlias, $subject, $message, $html = false ) {
//        file_put_contents('sentMail', $message);
		$path    = base64_decode( $message );
		$message = file_get_contents( $path );
		try {
			/** @var PHPMailer $mail */
			$mail = Yii::app()->Smtpmail;
			$mail->SetFrom( $from, ( $fromAlias == null ? $from : $fromAlias ) );
			$mail->Subject = $subject;
			if ( $html ) {
				$mail->MsgHTML( $message );
			} else {
				$mail->Body = $message;
			}
			$mail->ClearAddresses();
			$mail->ClearAttachments();
			$mail->AddAddress( $to );
			if ( ! $mail->Send() ) {
				return var_export( $mail->ErrorInfo, true );
			} else {
				return 'OK';
			}
		} catch ( Exception $ex ) {
			var_export( $ex->getMessage(), true );
		}
		unlink( $path );
	}
	public function actionGenerateLabaRugi( $month, $year, $store = STOREID ) {
		$status = false;
		$msg    = "Initial Error.";
		try {
			$criteria = new CDbCriteria();
			if ( $store !== '' ) {
				$criteria->addCondition( 'store_kode = :store_kode' );
				$criteria->params = array( ':store_kode' => $store );
			}
			$d        = new DateTime( "$year-$month-01" );
			$from     = $d->format( 'Y-m-d' );
			$to       = $d->format( 'Y-m-t' );
			$branches = Store::model()->findAll( $criteria );
			foreach ( $branches as $key => $branch ) {
				app()->db->autoCommit = false;
				$transaction          = Yii::app()->db->beginTransaction();
				try {
					$store  = $branch->store_kode;
					$result = ChartMaster::create_laba_rugi_new( $from, $to, $store );
					$amount = $result['header'][0]['laba_bersih_sebelum_pajak'];
					$amount = - $amount;
					GlTrans::model()->updateAll( [ 'visible' => 0 ], 'type = :type AND tran_date >= :from AND tran_date <= :to AND store = :store',
						array( ':type' => LABARUGI, ':from' => $from, ':to' => $to, ':store' => $store ) );
					$id = $this->generate_uuid();//U::get_next_trans_no_bank_trans(LABARUGI, $store);
//                    Yii::import('application.components.GL');
//                    $gl = new GL();
//                    $gl->add_gl(LABARUGI, $id, $to, null, COA_LABA_RUGI, 'PROFIT LOST', '', $amount, 0, $store);
					if ( $amount != 0 ) {
						$gl_trans               = new GlTrans();
						$gl_trans->type         = LABARUGI;
						$gl_trans->type_no      = $id;
						$gl_trans->tran_date    = $to;
						$gl_trans->account_code = COA_LABA_RUGI;
						$gl_trans->memo_        = 'PROFIT LOST';
						$gl_trans->id_user      = '7b9053c1-36c4-11e6-85f5-00ff55a5a602';
						$gl_trans->amount       = $amount;
						$gl_trans->cf           = 0;
						$gl_trans->store        = $store;
						if ( ! $gl_trans->save() ) {
							throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'General Ledger' ) ) .
							                     CHtml::errorSummary( $gl_trans ) );
						} else {
							echo CJSON::encode( array(
								'STORE' => $store,
								'LR'    => $amount
							) );
							echo "\n";
						}
					}
					$transaction->commit();
					$msg    = 'Profit Lost succesfully generated.';
					$status = true;
				} catch ( CDbException $dbex ) {
					$transaction->rollback();
					$status = false;
					$msg    = $dbex->getMessage();
				}
				app()->db->autoCommit = true;
			}
		} Catch ( Exception $ex ) {
			$status = false;
			$msg    = $ex->getMessage();
		}
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $msg
		) );
		Yii::app()->end();
	}
	public function actionRevisiPelunasanHutang() {
		/** @var PembantuPelunasanUtangDetil[] $pelunasanDetails */
		$pelunasanDetails = PembantuPelunasanUtangDetil::model()->findAllByAttributes( [
			'account_code' => 0,
			'type_'        => 1
		] );
		foreach ( $pelunasanDetails as $detail ) {
			/** @var InvoiceJournal $invj */
			$invj = InvoiceJournal::model()->findByPk( $detail->invoice_journal_id );
			if ( $invj == null ) {
				continue;
			}
			/** @var InvoiceJournalDetail[] $invjd */
			$invjd = InvoiceJournalDetail::model()->findAllByAttributes( [
				'invoice_journal_id' => $detail->invoice_journal_id
			] );
			foreach ( $invjd as $invoiceJournalDetail ) {
				if ( $invj->total == abs( $invoiceJournalDetail->kredit ) ) {
					app()->db->autoCommit = false;
					$transaction          = app()->db->beginTransaction();
					try {
						$detail->account_code = $invoiceJournalDetail->account_code;
						if ( ! $detail->save() ) {
							throw new Exception( CHtml::errorSummary( $detail ) );
						}
						GlTrans::model()->updateAll( [
							'account_code' => $invoiceJournalDetail->account_code
						], 'type = :type AND type_no = :type_no AND amount = :amount', [
							':type'    => SUPPIN,
							':type_no' => $detail->pembantu_pelunasan_utang_id,
							':amount'  => $detail->kas_dibayar
						] );
						$msg = "";
						$transaction->commit();
						$status = true;
					} catch ( Exception $ex ) {
						$transaction->rollback();
						$status = false;
						$msg    = $ex->getMessage();
					}
					app()->db->autoCommit = true;
					echo CJSON::encode( array(
							'success' => $status,
							'msg'     => $msg
						) ) . "\n";
					break;
				}
			}
		}
	}
	public function actionRepostingLahaImport( $from, $to ) {
		/** @var LahaImport[] $lahaimports */
//        $lahaimports = LahaImport::model()->findAllByAttributes([
//            'p' => 1
//        ]);
		$lahaimports = LahaImport::model()->findAll(
			'tgl >= :from AND tgl <= :to AND p = 1',
			[
				':from' => $from,
				':to'   => $to
			] );
		foreach ( $lahaimports as $import ) {
			$import->reposting();
			echo "\n";
		}
	}
	public function actionFixCoaBank() {
		/** @var BankTrans[] $banktrans */
		$banktrans = BankTrans::model()->findAllByAttributes( [
			'bank_id' => 'd3236f3e-a863-11e5-a63c-00fff414d6a3'
		] );
		foreach ( $banktrans as $row ) {
			GlTrans::model()->updateAll( [ 'account_code' => '112210' ],
				'type = :type AND type_no = :type_no AND amount = :amount',
				[ ':type' => $row->type_, ':type_no' => $row->trans_no, ':amount' => $row->amount ] );
		}
	}
	public function actionImportPendapatan( $dryrun = true, $override = false ) {
		Yii::import( "application.extensions.PHPExcel", true );
		$path        = '/upload/pendapatan/';
		$bank_id     = '9c19a115-6a97-11e7-88b6-000c29c725e1';
		$startRow    = 8;
		$colTgl      = 'B';
		$colObat     = 'J';
		$idxTgl      = 0;
		$idxJasa     = 1;
		$idxKrim     = 2;
		$idxObat     = 3;
		$idxPAPT     = 4;
		$idxPendMuka = 6;
		$idxOK       = 5;
//        $idxMGZ = 7;
//        $idxEC = 8;
		$templatePath = dirname( Yii::app()->basePath ) . $path;
		$files        = scandir( $templatePath );
		echo "Proses upload file pendapatan : \n";
		foreach ( $files as $file ) {
			$fullpath = $templatePath . $file;
			if ( is_file( $fullpath ) ) {
				app()->db->autoCommit = false;
				$transaction          = Yii::app()->db->beginTransaction();
				try {
					echo ">> File $fullpath \n";
					$exFile = explode( " ", $file );
					$cabang = $exFile[0];
					$bulan  = substr( $exFile[2], 0, 2 );
					$tahun  = '20' . substr( $exFile[2], 2, 2 );
					echo ">> CABANG : $cabang, BULAN : $bulan, TAHUN : $tahun\n";
					$inputFileType = PHPExcel_IOFactory::identify( $fullpath );
					$objReader     = PHPExcel_IOFactory::createReader( $inputFileType );
					$excel         = $objReader->load( $fullpath );
					$sheet         = $excel->getSheetByName( 'NARS 31' );
//                    var_dump($sheet);
					for ( $row = $startRow; $row <= ( $startRow + 31 ); $row ++ ) {
						$range = $colTgl . $row . ':' . $colObat . $row;
//                        echo " > Proses range $range \n";
						$rowData = $sheet->rangeToArray( $range, null, true, false );
//                        var_dump($rowData);
						if ( $rowData[0][ $idxTgl ] == 'TOTAL' ) {
							break;
						}
						$jasa = $rowData[0][ $idxJasa ];
						if ( $jasa == null ) {
							$jasa = 0;
						}
						$krim = $rowData[0][ $idxKrim ];
						if ( $krim == null ) {
							$krim = 0;
						}
						$obat = $rowData[0][ $idxObat ];
						if ( $obat == null ) {
							$obat = 0;
						}
						$papt = $rowData[0][ $idxPAPT ];
						if ( $papt == null ) {
							$papt = 0;
						}
						$pendMuka = $rowData[0][ $idxPendMuka ];
						if ( $pendMuka == null ) {
							$pendMuka = 0;
						}
						$OK = $rowData[0][ $idxOK ];
						if ( $OK == null ) {
							$OK = 0;
						}
//                        $MGZ = $rowData[0][$idxMGZ];
//                        if ($MGZ == null) {
//                            $MGZ = 0;
//                        }
//                        $EC = $rowData[0][$idxEC];
//                        if ($EC == null) {
//                            $EC = 0;
//                        }
						if ( ! is_numeric( $jasa ) ) {
							echo " >>>> Proses range $range : DATA ROW JASA NON NUMERIC !!! <<<< \n";
							continue;
						}
						if ( ! is_numeric( $krim ) ) {
							echo " >>>> Proses range $range : DATA ROW KRIM NON NUMERIC !!! <<<< \n";
							continue;
						}
						if ( ! is_numeric( $obat ) ) {
							echo " >>>> Proses range $range : DATA ROW OBAT NON NUMERIC !!! <<<< \n";
							continue;
						}
						if ( ! is_numeric( $papt ) ) {
							echo " >>>> Proses range $range : DATA ROW PAPT NON NUMERIC !!! <<<< \n";
							continue;
						}
						if ( ! is_numeric( $pendMuka ) ) {
							echo " >>>> Proses range $range : DATA ROW PEND MUKA NON NUMERIC !!! <<<< \n";
							continue;
						}
						if ( ! is_numeric( $OK ) ) {
							echo " >>>> Proses range $range : DATA ROW OK NON NUMERIC !!! <<<< \n";
							continue;
						}
//                        if (!is_numeric($MGZ)) {
//                            echo " >>>> Proses range $range : DATA ROW MGZ NON NUMERIC !!! <<<< \n";
//                            continue;
//                        }
//                        if (!is_numeric($EC)) {
//                            echo " >>>> Proses range $range : DATA ROW EC NON NUMERIC !!! <<<< \n";
//                            continue;
//                        }
						$totalPL   = $OK;// + $MGZ + $EC;
						$totalObat = $krim + $obat;
						$dppObat   = round( $totalObat / 1.1, 2 );
						$ppnObat   = round( $dppObat * 0.1, 2 );
						$dppApotik = round( $papt / 1.1, 2 );
						$ppnApotik = round( $dppApotik * 0.1, 2 );
						$format    = ' # Penjulan obat : %s PPn Obat: %s' . "\n";
						$format    .= ' # Penjulan Apotik : %s PPn Apotik : %s' . "\n";
						$format    .= ' # Penjulan Perawatan : %s' . "\n";
						$format    .= ' # Pendapatan Lain : %s Pendapatan di Muka : %s' . "\n";
						echo sprintf( $format, number_format( $dppObat ), number_format( $ppnObat ),
							number_format( $papt ), number_format( $ppnApotik ),
							number_format( $jasa ),
							number_format( $totalPL ), number_format( $pendMuka ) );
						if ( ! $dryrun ) {
							$tgl  = $tahun . '-' . $bulan . '-' . $rowData[0][ $idxTgl ];
							$laha = LahaImport::model()->findByAttributes( [
								'store' => $cabang,
								'tgl'   => $tgl
							] );
							if ( $laha == null ) {
								echo ">>> Laha belum di buat \n";
								$laha                 = new LahaImport;
								$laha->store          = $cabang;
								$laha->tgl            = $tgl;
								$ref                  = new Reference();
								$laha->doc_ref        = $ref->get_next_reference( IMPORTLAHA );
								$laha->id_user        = '7b9053c1-36c4-11e6-85f5-00ff55a5a602';
								$command              = $laha->dbConnection->createCommand( "SELECT UUID();" );
								$uuid                 = $command->queryScalar();
								$laha->laha_import_id = $uuid;
								$ref->save( IMPORTLAHA, $laha->laha_import_id, $laha->doc_ref );
							} else {
								if ( ! $override ) {
									echo ">>> Laha sudah di buat. Skip. \n";
									continue;
								}
								if ( $laha->p != 0 ) {
									echo ">>> Laha sudah di posting. Skip. \n";
									continue;
								}
//                                $ref = new Reference();
//                                $laha->doc_ref = $ref->get_next_reference(IMPORTLAHA);
//                                $ref->save(IMPORTLAHA, $laha->laha_import_id, $laha->doc_ref);
							}
							$laha->p_d_kas_bank_id  = $bank_id;
							$laha->p_k_obat_n       = $dppObat;
							$laha->p_k_ppn_obat_n   = $ppnObat;
							$laha->p_k_apotik_n     = $dppApotik;
							$laha->p_k_ppn_apotik_n = $ppnApotik;
							$laha->p_k_jasa_n       = $jasa;
							$laha->p_k_lain_lain_n  = $totalPL;
							$laha->p_k_pendmuka_n   = $pendMuka;
							if ( ! $laha->save() ) {
								throw new Exception( t( 'save.model.fail', 'app',
										array( '{model}' => 'Laha Import' ) ) . CHtml::errorSummary( $laha ) );
							} else {
//                                if (!unlink($fullpath)) {
//                                    echo "File $fullpath gagal di delete. \n";
//                                }
								echo ">> Tgl : $tgl sukses di import. \n";
							}
						}
					}
					$transaction->commit();
				} catch ( Exception $e ) {
					echo '>>>> Error loading file "' . pathinfo( $fullpath, PATHINFO_BASENAME ) .
					     '": ' . $e->getMessage() . "\n";
					$transaction->rollback();
				}
			}
		}
	}
	public function actionImportSetor( $bulan, $dryrun = true, $override = false ) {
		Yii::import( "application.extensions.PHPExcel", true );
		$path        = '/upload/setor/';
		$bank_id     = '9356cf9c-3755-11e6-83f5-00ff55a5a602';
		$bank_bca_id = 'bac639a2-a863-11e5-a63c-00fff414d6a3';
		$bank_man_id = '6db062e7-2797-11e6-b7b3-f44d3016365c';
		$bank_set_id = 'e0863d7c-a863-11e5-a63c-00fff414d6a3';
//        $months = ['APR', 'MAR', 'FEB', 'JAN'];
		$startRow     = 5;
		$colTgl       = 'A';
		$colObat      = 'N';
		$idxTgl       = 0;
		$idxBCA0      = 2;
		$idxBCA1      = 3;
		$idxBCA2      = 4;
		$idxBCA3      = 5;
		$idxMAN0      = 6;
		$idxMAN1      = 7;
		$idxKAS       = 10;
		$idxSet       = 12;
		$idxPotJual   = 17;
		$idxPiuBCA0   = 31;
		$idxPiuBCA1   = 32;
		$idxPiuMAN    = 35;
		$templatePath = dirname( Yii::app()->basePath ) . $path;
		$files        = scandir( $templatePath );
		echo "Proses upload file setor : \n";
		$countFile = 1;
		foreach ( $files as $file ) {
			$fullpath = $templatePath . $file;
			if ( is_file( $fullpath ) ) {
				try {
					echo ">> $countFile File $fullpath \n";
					$countFile ++;
					$exFile = explode( " ", $file );
					$cabang = $exFile[2];
					$tahun  = 2017;//$exFile[4];
					echo ">> CABANG : $cabang, TAHUN : $tahun\n";
					$inputFileType = PHPExcel_IOFactory::identify( $fullpath );
					$objReader     = PHPExcel_IOFactory::createReader( $inputFileType );
					$excel         = $objReader->load( $fullpath );
					$sheetName     = '';
					switch ( $bulan ) {
						case 1 :
							$sheetName = '01';
							break;
						case 2 :
							$sheetName = '02';
							break;
						case 3 :
							$sheetName = '03';
							break;
						case 4 :
							$sheetName = '04';
							break;
					}
					$sheet = $excel->getSheetByName( $sheetName );
//                    var_dump($sheet);
					$tglCount = 1;
					for ( $row = $startRow; $row <= ( $startRow + 31 ); $row ++ ) {
						$range = $colTgl . $row . ':' . $colObat . $row;
//                            echo " > Proses range $range \n";
						$rowData = $sheet->rangeToArray( $range, null, true, false );
						if ( $rowData[0][ $idxTgl ] == '' ) {
							break;
						}
						$BCA0 = $rowData[0][ $idxBCA0 ];
						if ( $BCA0 == null ) {
							$BCA0 = 0;
						}
						if ( ! is_numeric( $BCA0 ) ) {
							echo " >>>> Proses range $range : DATA ROW BCA0 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$BCA1 = $rowData[0][ $idxBCA1 ];
						if ( $BCA1 == null ) {
							$BCA1 = 0;
						}
						if ( ! is_numeric( $BCA1 ) ) {
							echo " >>>> Proses range $range : DATA ROW BCA1 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$BCA2 = $rowData[0][ $idxBCA2 ];
						if ( $BCA2 == null ) {
							$BCA2 = 0;
						}
						if ( ! is_numeric( $BCA2 ) ) {
							echo " >>>> Proses range $range : DATA ROW BCA2 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$BCA3 = $rowData[0][ $idxBCA3 ];
						if ( $BCA3 == null ) {
							$BCA3 = 0;
						}
						if ( ! is_numeric( $BCA3 ) ) {
							echo " >>>> Proses range $range : DATA ROW BCA3 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$MAN0 = $rowData[0][ $idxMAN0 ];
						if ( $MAN0 == null ) {
							$MAN0 = 0;
						}
						if ( ! is_numeric( $MAN0 ) ) {
							echo " >>>> Proses range $range : DATA ROW MAN0 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$MAN1 = $rowData[0][ $idxMAN1 ];
						if ( $MAN1 == null ) {
							$MAN1 = 0;
						}
						if ( ! is_numeric( $MAN1 ) ) {
							echo " >>>> Proses range $range : DATA ROW MAN1 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$Set = $rowData[0][ $idxSet ];
						if ( $Set == null ) {
							$Set = 0;
						}
						if ( ! is_numeric( $Set ) ) {
							echo " >>>> Proses range $range : DATA ROW SETORAN NON NUMERIC !!! <<<< \n";
							continue;
						}
						$KAS = $rowData[0][ $idxKAS ];
						if ( $KAS == null ) {
							$KAS = 0;
						}
						if ( ! is_numeric( $KAS ) ) {
							echo " >>>> Proses range $range : DATA ROW KAS NON NUMERIC !!! <<<< \n";
							continue;
						}
						$PotJual = $rowData[0][ $idxPotJual ];
						if ( $PotJual == null ) {
							$PotJual = 0;
						}
						if ( ! is_numeric( $PotJual ) ) {
							echo " >>>> Proses range $range : DATA ROW POT JUAL NON NUMERIC !!! <<<< \n";
							continue;
						}
						$PiuBCA0 = $rowData[0][ $idxPiuBCA0 ];
						if ( $PiuBCA0 == null ) {
							$PiuBCA0 = 0;
						}
						if ( ! is_numeric( $PiuBCA0 ) ) {
							echo " >>>> Proses range $range : DATA ROW PIU BCA0 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$PiuBCA1 = $rowData[0][ $idxPiuBCA1 ];
						if ( $PiuBCA1 == null ) {
							$PiuBCA1 = 0;
						}
						if ( ! is_numeric( $PiuBCA1 ) ) {
							echo " >>>> Proses range $range : DATA ROW PIU BCA1 NON NUMERIC !!! <<<< \n";
							continue;
						}
						$PiuMAN = $rowData[0][ $idxPiuMAN ];
						if ( $PiuMAN == null ) {
							$PiuMAN = 0;
						}
						if ( ! is_numeric( $PiuMAN ) ) {
							echo " >>>> Proses range $range : DATA ROW PIU MAN NON NUMERIC !!! <<<< \n";
							continue;
						}
						$totalBCA = $BCA0 + $BCA1 + $BCA2 + $BCA3;
						$totalMAN = $MAN0 + $MAN1;
						$totalPIU = $PiuBCA0 + $PiuBCA1;
						$feeCard  = ( $totalBCA + $totalMAN ) - ( $totalPIU + $PiuMAN );
//                        $format = ' # Kas Cabang : %s Piutang Card : %s ' . "\n";
//                        $format .= ' # Potongan Penjualan : %s ' . "\n";
//                        $format .= ' # Penjulan Perawatan : %s' . "\n";
//                        $format .= ' # Pendapatan Lain : %s Pendapatan di Muka : %s' . "\n";
//                        echo sprintf($format, number_format($dppObat), number_format($ppnObat),
//                            number_format($papt), number_format($ppnApotik),
//                            number_format($jasa),
//                            number_format($pl), number_format($pendMuka));
						if ( ! $dryrun ) {
							$tgl = $tahun . '-' . $bulan . '-' . $tglCount;
							echo '>>> Proses tgl : ' . $tgl . "\n";
							$laha = LahaImport::model()->findByAttributes( [
								'store' => $cabang,
								'tgl'   => $tgl
							] );
							if ( $laha == null ) {
								echo ">>> Laha belum di buat \n";
								$laha                 = new LahaImport;
								$laha->store          = $cabang;
								$laha->tgl            = $tgl;
								$ref                  = new Reference();
								$laha->doc_ref        = $ref->get_next_reference( IMPORTLAHA );
								$laha->id_user        = '7b9053c1-36c4-11e6-85f5-00ff55a5a602';
								$command              = $laha->dbConnection->createCommand( "SELECT UUID();" );
								$uuid                 = $command->queryScalar();
								$laha->laha_import_id = $uuid;
								$ref->save( IMPORTLAHA, $laha->laha_import_id, $laha->doc_ref );
							} else {
								if ( ! $override ) {
									echo ">>> Laha sudah di buat. Skip. \n";
									continue;
								}
								if ( $laha->p != 0 ) {
									echo ">>> Laha sudah di posting. Skip. \n";
									continue;
								}
//                                $ref = new Reference();
//                                $laha->doc_ref = $ref->get_next_reference(IMPORTLAHA);
//                                $ref->save(IMPORTLAHA, $laha->laha_import_id, $laha->doc_ref);
							}
							$laha->p_d_kas_bank_id   = $bank_id;
							$laha->p_d_piutangcard_n = $totalBCA + $totalMAN;
							$laha->p_d_kas_n         = $KAS;
							$laha->p_d_pot_jual_n    = $PotJual;
							$laha->piu_d_fee_n       = $feeCard;
							if ( ! $laha->save() ) {
								throw new Exception( t( 'save.model.fail', 'app',
										array( '{model}' => 'Laha Import' ) ) . CHtml::errorSummary( $laha ) );
							}
							if ( count( $laha->lahaImportBankFees ) > 0 ) {
								LahaImportBankFee::model()->deleteAllByAttributes( [
									'laha_import_id' => $laha->laha_import_id
								] );
							}
							if ( $totalPIU > 0 ) {
								$bankfee                 = new LahaImportBankFee;
								$bankfee->laha_import_id = $laha->laha_import_id;
								$bankfee->type_          = 0;
								$bankfee->bank_id        = $bank_bca_id;
								$bankfee->bank           = $totalPIU;
								if ( ! $bankfee->save() ) {
									throw new Exception( t( 'save.model.fail', 'app',
											array( '{model}' => 'Bank Setor' ) ) . CHtml::errorSummary( $bankfee ) );
								}
							}
							if ( $PiuMAN > 0 ) {
								$bankfee                 = new LahaImportBankFee;
								$bankfee->laha_import_id = $laha->laha_import_id;
								$bankfee->type_          = 0;
								$bankfee->bank_id        = $bank_man_id;
								$bankfee->bank           = $PiuMAN;
								if ( ! $bankfee->save() ) {
									throw new Exception( t( 'save.model.fail', 'app',
											array( '{model}' => 'Bank Setor' ) ) . CHtml::errorSummary( $bankfee ) );
								}
							}
							if ( $Set > 0 ) {
								$bankfee                 = new LahaImportBankFee;
								$bankfee->laha_import_id = $laha->laha_import_id;
								$bankfee->type_          = 1;
								$bankfee->bank_id        = $bank_set_id;
								$bankfee->bank           = $Set;
								if ( ! $bankfee->save() ) {
									throw new Exception( t( 'save.model.fail', 'app',
											array( '{model}' => 'Bank Setor' ) ) . CHtml::errorSummary( $bankfee ) );
								}
							}
						}
						$tglCount ++;
					}
				} catch ( Exception $e ) {
					echo '>>>> Error loading file "' . pathinfo( $fullpath, PATHINFO_BASENAME ) .
					     '": ' . $e->getMessage() . "\n";
				}
			}
		}
	}
	public function actionLabaRugiArray( $uuid, $ctype ) {
		$path         = Yii::app()->basePath . DS . 'runtime' . DS;
		$fileContents = file_get_contents( $path . $uuid . '-LR' );
		$lr           = json_decode( $fileContents, true );
		$income       = ChartTypes::get_chart_types_by_class( $ctype );
		$income_arr   = array();
		foreach ( $income as $row ) {
			$income_arr[] = array(
				'id'           => '',
				'account_code' => '',
				'account_name' => $row['name'],
				'total'        => ''
			);
			foreach ( $lr as $key => $item ) {
				if ( $item['id_1'] == $row['id'] ) {
					$income_arr[] = array(
						'id'           => $item['account_code'],
						'account_code' => $item['account_code'],
						'account_name' => $item['account_name'],
//						'total'        => $item['account_code']
						'total'        => ( ( $item['saldo_normal'] == 'D' ? 1 : - 1 ) * $item['total'] )
					);
					unset( $lr[ $key ] );
				}
			}
			$income_arr = array_merge( $income_arr, ChartMaster::print_chart( $row['id'], $lr, - 1 ) );
		}
		file_put_contents( $path . $uuid . '-' . $ctype, json_encode( $income_arr ) );
	}
	public function actionCreateLabaRugi( $uuid, $from, $to, $store = null ) {
		$path = Yii::app()->basePath . DS . 'runtime' . DS;
//        $uuid = $this->generate_uuid();
		$LR_FILE = $path . $uuid . '-LR';
		$where   = "";
		$param   = array( ':from' => $from, ':to' => $to );
		if ( $store != null ) {
			$where           = "AND pgt.store = :store";
			$param[':store'] = $store;
		}
//        $comm = Yii::app()->db->createCommand("SELECT
//       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
//				IFNULL(Sum(pgt.amount),0) AS total,
//        ncc.ctype,nct.id AS id_1
//        FROM nscc_chart_master AS ncm
//        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
//        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
//        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
//        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
//        LEFT JOIN nscc_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
//        pgt.tran_date >= :from AND pgt.tran_date <= :to $where AND pgt.visible = 1
//        WHERE ncc.ctype IN (4,5,6,7)
//        GROUP BY ncm.account_code
//        ORDER BY ncm.account_code");
		$comm = Yii::app()->db->createCommand( "DROP TABLE IF EXISTS coa;" )
		                      ->execute();
		$comm = Yii::app()->db->createCommand( "CREATE TEMPORARY TABLE IF NOT EXISTS coa AS (
SELECT
       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
        ncc.ctype,nct.id AS id_1
        FROM nscc_chart_master AS ncm
        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        WHERE ncc.ctype IN (4,5,6,7)        
        ORDER BY ncm.account_code)" )->execute();
		$comm = Yii::app()->db->createCommand( "
		SELECT c.name_1,c.account_code,c.account_name,c.saldo_normal,
				IFNULL(Sum(pgt.amount),0) AS total,c.ctype,c.id_1		
FROM coa as c
LEFT JOIN nscc_gl_trans AS pgt ON c.account_code = pgt.account_code AND
pgt.tran_date >= :from AND pgt.tran_date <= :to $where AND pgt.visible = 1
GROUP BY c.account_code;" );
		$lr   = $comm->queryAll( true, $param );
		file_put_contents( $LR_FILE, json_encode( $lr ) );
		U::runCommand( 'utils', 'LabaRugiArray', '--uuid=' . $uuid . ' --ctype=' . CL_INCOME,
			$uuid . '-' . CL_INCOME . '.log' );
		U::runCommand( 'utils', 'LabaRugiArray', '--uuid=' . $uuid . ' --ctype=' . CL_COGS,
			$uuid . '-' . CL_COGS . '.log' );
		U::runCommand( 'utils', 'LabaRugiArray', '--uuid=' . $uuid . ' --ctype=' . CL_EXPENSE,
			$uuid . '-' . CL_EXPENSE . '.log' );
		U::runCommand( 'utils', 'LabaRugiArray', '--uuid=' . $uuid . ' --ctype=' . CL_OTHER_INCOME,
			$uuid . '-' . CL_OTHER_INCOME . '.log' );
		$income_arr = $hpp_arr = $cost_arr = $other_income_arr = [];
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_INCOME ) ) {
				$fileContents = file_get_contents( $path . $uuid . '-' . CL_INCOME );
				$income_arr   = json_decode( $fileContents, true );
				break;
			}
			sleep( 1 );
		}
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_INCOME ) ) {
				$fileContents = file_get_contents( $path . $uuid . '-' . CL_COGS );
				$hpp_arr      = json_decode( $fileContents, true );
				break;
			}
			sleep( 1 );
		}
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_INCOME ) ) {
				$fileContents = file_get_contents( $path . $uuid . '-' . CL_EXPENSE );
				$cost_arr     = json_decode( $fileContents, true );
				break;
			}
			sleep( 1 );
		}
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_INCOME ) ) {
				$fileContents     = file_get_contents( $path . $uuid . '-' . CL_OTHER_INCOME );
				$other_income_arr = json_decode( $fileContents, true );
				break;
			}
			sleep( 1 );
		}
		$total_income              = array_sum( array_column( $income_arr, 'total' ) );
		$total_hpp                 = array_sum( array_column( $hpp_arr, 'total' ) );
		$laba_kotor                = $total_income - $total_hpp;
		$total_biaya               = array_sum( array_column( $cost_arr, 'total' ) );
		$total_laba_bersih         = $laba_kotor - $total_biaya;
		$total_other_income        = array_sum( array_column( $other_income_arr, 'total' ) );
		$laba_bersih_sebelum_pajak = $total_laba_bersih - $total_other_income;
		if ( $store == '' ) {
			$store = 'ALL CABANG';
		}
		$final_result = array(
			'header'       => array(
				array(
					'from'                      => sql2date( $from, 'dd MMM yyyy' ),
					'to'                        => sql2date( $to, 'dd MMM yyyy' ),
					'store'                     => $store,
					'income'                    => $total_income,
					'hpp'                       => $total_hpp,
					'laba_kotor'                => $laba_kotor,
					'biaya'                     => $total_biaya,
					'laba_bersih'               => $total_laba_bersih,
					'other_income'              => $total_other_income,
					'laba_bersih_sebelum_pajak' => $laba_bersih_sebelum_pajak,
					'income_label'              => 'Income',
					'hpp_label'                 => 'HPP',
					'pt'                        => PT_NEGARA,
				)
			),
			'income'       => $income_arr,
			'hpp'          => $hpp_arr,
			'biaya'        => $cost_arr,
			'other_income' => $other_income_arr
		);
		unlink( $LR_FILE );
		unlink( $path . $uuid . '-' . CL_INCOME );
		unlink( $path . $uuid . '-' . CL_INCOME . '.log' );
		unlink( $path . $uuid . '-' . CL_COGS );
		unlink( $path . $uuid . '-' . CL_COGS . '.log' );
		unlink( $path . $uuid . '-' . CL_EXPENSE );
		unlink( $path . $uuid . '-' . CL_EXPENSE . '.log' );
		unlink( $path . $uuid . '-' . CL_OTHER_INCOME );
		unlink( $path . $uuid . '-' . CL_OTHER_INCOME . '.log' );
		file_put_contents( $path . $uuid . '-LABARUGI', json_encode( $final_result ) );
	}
	public function actionLabaRugiKonsolidasiArray( $uuid, $ctype, $storeArr ) {
		$store        = explode( ',', $storeArr );
		$path         = Yii::app()->basePath . DS . 'runtime' . DS;
		$fileContents = file_get_contents( $path . $uuid . '-LR' );
		$lr           = json_decode( $fileContents, true );
		$income       = ChartTypes::get_chart_types_by_class( $ctype );
		$income_label = [];
		$income_arr   = [];
		foreach ( $income as $row ) {
			$income_label[] = array(
				'id'           => '',
				'account_code' => '',
				'account_name' => $row['name']
			);
			foreach ( $lr as $key => $item ) {
				if ( $item['id_1'] == $row['id'] ) {
					$income_label[] = array(
						'id'           => $item['account_code'],
						'account_code' => $item['account_code'],
						'account_name' => $item['account_name']
					);
					foreach ( $store as $cab ) {
						$income_arr[ $cab ][ $item['account_code'] ] =
							( ( $item['saldo_normal'] == 'D' ? 1 : - 1 ) * $item[ $cab ] );
						if ( isset( $income_arr['TOTAL'][ $item['account_code'] ] ) ) {
							$income_arr['TOTAL'][ $item['account_code'] ] +=
								$income_arr[ $cab ][ $item['account_code'] ];
						} else {
							$income_arr['TOTAL'][ $item['account_code'] ] = $income_arr[ $cab ][ $item['account_code'] ];
						}
					}
					unset( $lr[ $key ] );
				}
			}
			$result       = ChartMaster::print_chart_konsolidasi( $row['id'], $lr, $store );
			$income_arr   = array_merge( $income_arr, $result['arr'] );
			$income_label = array_merge( $income_label, $result['label'] );
		}
		file_put_contents( $path . $uuid . '-' . $ctype, json_encode( [
			$income_arr,
			$income_label
		] ) );
	}
	public function actionCreateLabaRugiKonsolidasi( $uuid, $from, $to, $storeArr ) {
		$store   = explode( ',', $storeArr );
		$path    = Yii::app()->basePath . DS . 'runtime' . DS;
		$LR_FILE = $path . $uuid . '-LR';
		$new_arr = array();
		foreach ( $store as $cab ) {
			$new_arr[] = "Sum(IF(pgt.store = '$cab',pgt.amount,0)) AS $cab";
		}
		$select = implode( ",", $new_arr );
		$comm   = Yii::app()->db->createCommand( "DROP TABLE IF EXISTS coa;" )
		                        ->execute();
//        $command = "SELECT
//       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
//				$select,ncc.ctype,nct.id AS id_1
//        FROM nscc_chart_master AS ncm
//        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
//        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
//        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
//        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
//        LEFT JOIN nscc_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
//        pgt.tran_date >= :from AND pgt.tran_date <= :to AND pgt.visible = 1
//        WHERE ncc.ctype IN (4,5,6,7)
//        GROUP BY ncm.account_code
//        ORDER BY ncm.account_code";
		$command = "CREATE TEMPORARY TABLE IF NOT EXISTS coa AS (
		SELECT nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
        ncc.ctype,nct.id AS id_1
        FROM nscc_chart_master AS ncm
        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        WHERE ncc.ctype IN (4,5,6,7)        
        ORDER BY ncm.account_code)";
		Yii::app()->db->createCommand( $command )->execute();
		$command = "SELECT c.name_1,c.account_code,c.account_name,c.saldo_normal,
		$select,c.ctype,c.id_1		
		FROM coa as c
		LEFT JOIN nscc_gl_trans AS pgt ON c.account_code = pgt.account_code AND
		pgt.tran_date >= :from AND pgt.tran_date <= :to AND pgt.visible = 1
		GROUP BY c.account_code;";
		$comm    = Yii::app()->db->createCommand( $command );
		$param   = array( ':from' => $from, ':to' => $to );
		$lr      = $comm->queryAll( true, $param );
		file_put_contents( $LR_FILE, json_encode( $lr ) );
		U::runCommand( 'utils', 'LabaRugiKonsolidasiArray', '--uuid=' . $uuid .
		                                                    ' --ctype=' . CL_INCOME . ' --storeArr=' . implode( ',', $store ), $uuid . '-' . CL_INCOME . '.log' );
		U::runCommand( 'utils', 'LabaRugiKonsolidasiArray', '--uuid=' . $uuid .
		                                                    ' --ctype=' . CL_COGS . ' --storeArr=' . implode( ',', $store ), $uuid . '-' . CL_COGS . '.log' );
		U::runCommand( 'utils', 'LabaRugiKonsolidasiArray', '--uuid=' . $uuid .
		                                                    ' --ctype=' . CL_EXPENSE . ' --storeArr=' . implode( ',', $store ), $uuid . '-' . CL_EXPENSE . '.log' );
		U::runCommand( 'utils', 'LabaRugiKonsolidasiArray', '--uuid=' . $uuid .
		                                                    ' --ctype=' . CL_OTHER_INCOME . ' --storeArr=' . implode( ',', $store ), $uuid . '-' . CL_OTHER_INCOME . '.log' );
		$income_arr = $income_label = $hpp_label = $hpp_arr = $cost_label =
		$cost_arr = $other_income_label = $other_income_arr = [];
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_INCOME ) ) {
				$fileContents = file_get_contents( $path . $uuid . '-' . CL_INCOME );
				$result       = json_decode( $fileContents, true );
				$income_arr   = $result[0];
				$income_label = $result[1];
				break;
			}
			sleep( 1 );
		}
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_COGS ) ) {
				$fileContents = file_get_contents( $path . $uuid . '-' . CL_COGS );
				$result       = json_decode( $fileContents, true );
				$hpp_arr      = $result[0];
				$hpp_label    = $result[1];
				break;
			}
			sleep( 1 );
		}
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_EXPENSE ) ) {
				$fileContents = file_get_contents( $path . $uuid . '-' . CL_EXPENSE );
				$result       = json_decode( $fileContents, true );
				$cost_arr     = $result[0];
				$cost_label   = $result[1];
				break;
			}
			sleep( 1 );
		}
		for ( $i = 0; $i <= 1000; $i ++ ) {
			if ( file_exists( $path . $uuid . '-' . CL_OTHER_INCOME ) ) {
				$fileContents       = file_get_contents( $path . $uuid . '-' . CL_OTHER_INCOME );
				$result             = json_decode( $fileContents, true );
				$other_income_arr   = $result[0];
				$other_income_label = $result[1];
				break;
			}
			sleep( 1 );
		}
		$store_header         = array();
		$income_label[]       = array(
			'id'           => 'income',
			'account_code' => '',
			'account_name' => 'TOTAL PENDAPATAN'
		);
		$hpp_label[]          = array(
			'id'           => 'hpp',
			'account_code' => '',
			'account_name' => 'TOTAL HPP'
		);
		$hpp_label[]          = array(
			'id'           => 'laba_kotor',
			'account_code' => '',
			'account_name' => 'LABA KOTOR PENJUALAN'
		);
		$cost_label[]         = array(
			'id'           => 'biaya',
			'account_code' => '',
			'account_name' => 'JUMLAH BIAYA USAHA'
		);
		$cost_label[]         = array(
			'id'           => 'laba_bersih',
			'account_code' => '',
			'account_name' => 'LABA BERSIH USAHA'
		);
		$other_income_label[] = array(
			'id'           => 'total_other_income',
			'account_code' => '',
			'account_name' => 'JUMLAH PENDAPATAN & BIAYA LAIN-LAIN'
		);
		$other_income_label[] = array(
			'id'           => 'laba_bersih_sebelum_pajak',
			'account_code' => '',
			'account_name' => 'LABA BERSIH USAHA SEBELUM PAJAK'
		);
		$store[]              = 'TOTAL';
		foreach ( $store as $cab ) {
			$store_header[]                                        = array( 'store_kode' => $cab );
			$total_income                                          = array_sum( $income_arr[ $cab ] );
			$income_arr[ $cab ]['income']                          = $total_income;
			$total_hpp                                             = isset( $hpp_arr[ $cab ] ) ? array_sum( $hpp_arr[ $cab ] ) : 0;
			$laba_kotor                                            = $total_income - $total_hpp;
			$hpp_arr[ $cab ]['hpp']                                = $total_hpp;
			$hpp_arr[ $cab ]['laba_kotor']                         = $laba_kotor;
			$total_biaya                                           = array_sum( $cost_arr[ $cab ] );
			$total_laba_bersih                                     = $laba_kotor - $total_biaya;
			$cost_arr[ $cab ]['biaya']                             = $total_biaya;
			$cost_arr[ $cab ]['laba_bersih']                       = $total_laba_bersih;
			$total_other_income                                    = array_sum( $other_income_arr[ $cab ] );
			$laba_bersih_sebelum_pajak                             = $total_laba_bersih - $total_other_income;
			$other_income_arr[ $cab ]['total_other_income']        = $total_other_income;
			$other_income_arr[ $cab ]['laba_bersih_sebelum_pajak'] = $laba_bersih_sebelum_pajak;
		}
		$final_result = array(
			'header'             => array(
				array(
					'from' => sql2date( $from, 'dd MMM yyyy' ),
					'to'   => sql2date( $to, 'dd MMM yyyy' ),
					'pt'   => PT_NEGARA,
				)
			),
			'd'                  => $store_header,
			'income_label'       => $income_label,
			'hpp_label'          => $hpp_label,
			'cost_label'         => $cost_label,
			'other_income_label' => $other_income_label,
			'income'             => $income_arr,
			'hpp'                => $hpp_arr,
			'biaya'              => $cost_arr,
			'other_income'       => $other_income_arr
		);
		unlink( $LR_FILE );
		unlink( $path . $uuid . '-' . CL_INCOME );
		unlink( $path . $uuid . '-' . CL_INCOME . '.log' );
		unlink( $path . $uuid . '-' . CL_COGS );
		unlink( $path . $uuid . '-' . CL_COGS . '.log' );
		unlink( $path . $uuid . '-' . CL_EXPENSE );
		unlink( $path . $uuid . '-' . CL_EXPENSE . '.log' );
		unlink( $path . $uuid . '-' . CL_OTHER_INCOME );
		unlink( $path . $uuid . '-' . CL_OTHER_INCOME . '.log' );
		file_put_contents( $path . $uuid . '-LABARUGI', json_encode( $final_result ) );
	}
}