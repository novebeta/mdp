<?php
class TagihanCommand extends CConsoleCommand {
	/**
	 * Sample crontab every day at 14.01
	 * 1 14 * * * /usr/bin/php /home/username/public_html/cron.php >/dev/null 2>&1
	 *
	 */
	public function actionCreate() {
		U::createTagihan();
	}
	public function actionClosing(){
	    $year = date("Y");
		U::closing($year,0);
	}
}