<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 06/01/2015
 * Time: 9:42
 */
class SyncCommand extends CConsoleCommand
{
    public function run($args)
    {
        //echo  dirname(__file__) . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR.'sync';
//        $x = fopen(SYNC_LOCK, "w");
//        if (!flock($x, LOCK_EX | LOCK_NB)) {
//            echo json_encode(
//                    array(
//                        'time' => date("Y-m-d H:i:s"),
//                        'note' => "Sync masih ada yang berjalan..."
//                    )) . PHP_EOL;
//            return 1;
//        }
        date_default_timezone_set('Asia/Jakarta');
        Yii::import('application.models.Store');
//        $store = new Store();
        if (count($args) == 0) {
//            $file_sync = dirname(__file__) . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR.'sync';
            $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'yiic';
//            $thread = THREAD;
            if (time() >= strtotime(TIME_HS_START) || time() <= strtotime(TIME_HS_STOP)) {
                $limit = LIMIT_ROW_SYNC_DOWN_HS;
                $max_thread = MAX_THREAD_HS;
            } else {
                $limit = LIMIT_ROW_SYNC_DOWN;
                $max_thread = MAX_THREAD;
            }
            $log = CMD_LOG;
            $start = 0;
            $start_mysql = 0;
            app()->dbadvert
                ->createCommand("DELETE FROM email_tmp WHERE store = :store")
                ->query(array(':store' => STOREID));
//            $command = app()->dbadvert->dbConnection->createCommand("SELECT UUID();");
//            $uuid = $command->queryScalar();
            $t = 'T' . STOREID;
            $m = 'M' . STOREID;
            app()->dbadvert->createCommand("INSERT INTO email_tmp
            SELECT uuid(),`subject`,body,attachments,target,:store,now() FROM email 
            WHERE target IN ('GLOBAL', 'PASIEN','$m','$t') AND (email.subject NOT IN 
            (SELECT sync_email.subject FROM sync_email WHERE store = :store));")
                ->query(array(
                    ':store' => STOREID
                ));
            $command = app()->dbadvert
                ->createCommand("SELECT count(*) FROM email_tmp et WHERE et.store = :store");
            $total_pages = $command->queryScalar(array(':store' => STOREID));
            $thread = ceil($total_pages / $limit);
            if ($thread > $max_thread) {
                $thread = $max_thread;
            }
            echo "THREAD $thread, TOTAL $total_pages, LIMIT $limit" . PHP_EOL;
            for ($i = 1; $i <= $thread; $i++) {
                if ($i > 1) {
                    $start_mysql = ($i - 1) * $limit;
                } else {
                    $start_mysql = 0;
                }
                if (substr(php_uname(), 0, 7) == "Windows") {
                    exec("start /B php $yiic sync $start_mysql $limit >> $log");
                } else {
                    exec("php $yiic sync $start_mysql $limit >> $log &");
                }
                $start += $limit;
            }
            if (substr(php_uname(), 0, 7) == "Windows") {
                exec("start /B php $yiic sync -1 0 >> $log");
            } else {
                exec("php $yiic sync -1 0 >> $log &");
            }
            if (substr(php_uname(), 0, 7) == "Windows") {
                exec("start /B php $yiic sync -2 0 >> $log");
            } else {
                exec("php $yiic sync -2 0 >> $log &");
            }
        } else {
            $start_time = microtime(TRUE);
            $s = new SyncData($args[0], $args[1]);
            switch ($args[0]) {
                case -1 :
                    $s->upload();
                    break;
                case -2 :
                    $s->history();
                    break;
                default :
                    $s->sync();
                    break;
            }
            $end_time = microtime(TRUE);
            echo $end_time - $start_time;
        }
//        $stat = SysPrefs::model()->find('name_ = :name AND store =:store',
//            array(':name' => 'sync_active', ':store' => STOREID));
//        if ($stat->value_ != "0") {
//            $date = strtotime($stat->value_);
//            $date2 = time();
//            $subTime = $date2 - $date;
//            $h = $subTime / (60 * 60);
//            if ($h > 1) {
//                $stat->value_ = '0';
//                $stat->save();
//            }
//            $msg = 'other client already sync. Diff time ' . $h;
//        } else {
//            $stat->value_ = date('Y-m-d H:i:s');
//            $stat->save();
//            $s = new SyncData();
//            $msg = $s->sync();
//            $stat->value_ = '0';
//            $stat->save();
//        }
//        $report = json_encode(array(
//            'type' => 'event',
//            'name' => 'message',
//            'time' => date('g:i:s a'),
//            'data' => $msg
//        ),
//            JSON_PRETTY_PRINT);
//        echo $report;
//        fflush($x);            // flush output before releasing the lock
//        flock($x, LOCK_UN);
//        fclose($x);
        return 0;
    }
}