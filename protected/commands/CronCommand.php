<?php
/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 03/01/2018
 * Time: 14:49
 */

Yii::import('application.components.SoapClientYii');
class CronCommand extends CConsoleCommand
{
    public function SyncPusatSoap($id)
    {
        try {
            /** @var Store $str */
            $str = Store::model()->findByAttributes(STOREID);
            if ($str == null) {
                throw new Exception('Store tidak ditemukan.');
            }
            $attrib = $str->getAttributes();
            $attrib['nama_store'] = $str->nama_store;

            $client = new SoapClient(SOAP_SYNC, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->syncData($attrib, STOREID);
            var_dump($result);
        } catch (Exception $e) {
            var_dump($e);
        }
    }
}