<? /** @var TransferBarang $model */
$arus = 'KELUAR';
?>
<section class="sheet padding-10mm">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" style="width:55px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" style="margin-left: 380px;width: 275px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="margin-bottom: 15px;width: 715px">
    <p class="15" style="margin-bottom: 15px;text-align: center;" align="center"><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 22.0000pt;">BUKTI KAS / BANK <? echo $arus;?></span></u></strong>
    </p>
    <table style="border-collapse: collapse; width: 979px; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
        <tbody>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NO. BUKTI  <? echo $arus;?></span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?=$model->doc_ref;?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">TANGGAL</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=sql2date($model->tgl)?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">Lunas</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=time2date($model->lunas,'dd/MM/yyyy')?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"> <? echo 'DISERAHKAN KEPADA';?></span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"></span><?=strtoupper($model->suppFlazz->nama_supp);?></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">UANG SEBESAR</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">RP <?=number_format(abs($model->total),2,',','.')?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <div class="15" style="width: 500px;overflow-wrap: break-word;font-family: Calibri; font-size: 11.0000pt;"># <?=strtoupper(terbilang(abs($model->total)));?> RUPIAH #</div>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">KEPERLUAN</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=strtoupper($model->note);?></span></p>
            </td>
        </tr>
        </tbody>
    </table>
    <p style="padding-bottom: 15px"></p>

	<table style="border-collapse: collapse; width: 647px; border: none; font-family: 'Calibri'; font-size: 10pt; height: 107px;" border="0" cellspacing="0">
		<tbody>
		<tr style="">
			<td style="width: 201.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">DISERAHKAN OLEH,</span></strong></p>
			</td>
			<td style="width: 203.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">DITERIMA OLEH,</span></strong></p>
			</td>
			<td style="width: 198.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">DISETUJUI OLEH,</span></strong></p>
			</td>
		</tr>
		<tr style="">
			<td style="width: 201.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
			</td>
			<td style="width: 203.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
			</td>
			<td style="width: 198.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
				<p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
			</td>
		</tr>
		<tr style="">
			<td style="width: 201.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</span></strong></p>
			</td>
			<td style="width: 203.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</span></strong></p>
			</td>
			<td style="width: 198.625px; padding: 0pt 5.4pt; border: none; " valign="top">
				<p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</span></strong></p>
			</td>
		</tr>
		</tbody>
	</table>
</section>
