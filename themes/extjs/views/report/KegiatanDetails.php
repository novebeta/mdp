<h1>Rekap Kegiatan Marketing Details</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>

<?
$this->pageTitle='Rekap Kegiatan ';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('tgl', 'doc_ref', 'store','note','account_name','tgl_keg'),
   
    'columns' => array(
        array(
            'header' => 'Tanggal Post',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Branch',
            'name' => 'store'
        ),
        array(
            'header' => 'Tanggal Kegiatan ',
            'name' => 'tgl_keg'
        ),

        array(
            'header' => 'Doc. Ref',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Jenis Kegiatan',
            'name' => 'account_name'
        ),
        array(
            'header' => 'Memo',
            'name' => 'note'
        ),
        array(
            'header' => 'Rincian',
            'name' => 'item_name',
            'footer' => "Total :"
        ),
//        array(
//            'header' => 'Debit',
//            'name' => 'Debit',
//            'value' => function ($data) {
//                    return format_number_report($data['Debit'], 2);
//                },
//            'htmlOptions' => array ('style' => 'text-align: right;' )
//        ),
        array(
            'header' => 'Biaya',
            'name' => 'amount',
            'value' => function ($data) {
                    return format_number_report($data['amount'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
                'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
//        array(
//            'header' => 'Balance',
//            'name' => 'Balance',
//            'value' => function ($data) {
//                    return format_number_report($data['Balance'], 2);
//                },
//            'htmlOptions' => array ('style' => 'text-align: right;' )
//        )
    )
));
?>