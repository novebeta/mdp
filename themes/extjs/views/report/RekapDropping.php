<h1>Rekap Transfer <?= ($ratarata?"(Rata-rata)":"") ?></h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Branch : <?= $branch ?></h3>
<?
$this->pageTitle = 'Rekap Transfer Request';
$gridColums = array();
array_push($gridColums,
        array(
            'header' => 'Kode Barang',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Nama Barang',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Satuan',
            'name' => 'sat'
        )
);

foreach($store as $s){
    array_push($gridColums,
        array(
            'header' => $s['store_penerima'],
            'name' => $s['store_penerima'],
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    );
}

array_push($gridColums,
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                if($data['show_ratarata']){
                    return number_format($data['total'], 2);
                }
                else {
                    return format_number_report($data['total'], 0);
                }
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));