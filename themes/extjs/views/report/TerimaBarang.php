<h1>Transfer Barang Masuk</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Transfer Barang Masuk';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => array('tgl','doc_ref','doc_ref_other','note'),
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Doc. Ref',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'No. Invoice',
            'name' => 'doc_ref_other'
        ),
        array(
            'header' => 'Note',
            'name' => 'note'
        ),
        array(
            'header' => 'Kode Barang',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Jumlah',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    ),
));
?>