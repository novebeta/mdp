<h1><?=$this->pageTitle?></h1>
<h3>Transfer From : <?= $from ?></h3>
<h3>Transfer To : <?= $to ?></h3>
<?php if ($store_pengirim) { ?>
<h3>Branch Send : <?= $store_pengirim ?></h3>
<?php } ?>
<h3>Branch received : <?= $store ?></h3>

<?php if ($group_name) { ?>
<h3>Group : <?= $group_name ?></h3>
<?php } ?>

<?
//$this->pageTitle = 'Inventory Movements';
//$id = Yii::app()->user->getId();
//$user = Users::model()->findByPk($id);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Dropping',
            'name' => 'qty_dropping',
            'value' => function ($data) {
                return format_number_report($data['qty_dropping'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Received',
            'name' => 'qty_received',
            'value' => function ($data) {
                return format_number_report($data['qty_received'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Recalled',
            'name' => 'qty_recalled',
            'value' => function ($data) {
                return format_number_report($data['qty_recalled'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Intransit',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));