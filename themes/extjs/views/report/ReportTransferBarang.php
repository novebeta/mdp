<h1><?=$this->pageTitle?></h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
//$this->pageTitle = 'Inventory Movements';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Barang Masuk (IN)',
            'name' => 'BarangMasuk',
            'value' => function ($data) {
                return format_number_report($data['BarangMasuk'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Barang Keluar (OUT)',
            'name' => 'BarangKeluar',
            'value' => function ($data) {
                return format_number_report($data['BarangKeluar'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));