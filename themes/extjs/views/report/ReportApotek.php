<h1><?=$this->pageTitle?></h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
//$this->pageTitle = 'Inventory Movements';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('DocRef','Nama','NoPasien'),
    'columns' => array(
       array(
            'header' => 'Tanggal',
            'name' => 'Tanggal',
        ),
        array(
            'header' => 'Doc Ref.',
            'name' => 'DocRef'
        ),
        array(
            'header' => 'Nama Cust.',
            'name' => 'Nama'
        ),
        array(
            'header' => 'No Cust.',
            'name' => 'NoPasien'
        ),
        array(
            'header' => 'Nama Dokter',
            'name' => 'Dokter'
        ),
        array(
            'header' => 'Komposisi',
            'name' => 'Komposisi'
        ),
        array(
            'header' => 'Qty',
            'name' => 'Qty',
            'value' => function ($data) 
            {
                return format_number_report($data['Qty'],0);
            }
        ),
        array(
            'header' => 'Satuan',
            'name' => 'Satuan'
        )
    )
));
