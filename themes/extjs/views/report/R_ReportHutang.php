<h1>Report Hutang</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->pageTitle = 'Report Hutang';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $R_ReportHutang,
    'columns' => array(
        array(
            'header' => 'supplier',
            'name' => 'supplier'
        ),
        array(
            'header' => 'saldo awal',
            'name' => 'saldo_awal',
	        'value' => function ($data) {
		        return format_number_report($data['saldo_awal'], 2);
	        },
	        'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'debet',
            'name' => 'debet',
	        'value' => function ($data) {
		        return format_number_report($data['debet'], 2);
	        },
	        'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'kredit',
            'name' => 'kredit',
	        'value' => function ($data) {
		        return format_number_report($data['kredit'], 2);
	        },
	        'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Saldo akhir',
            'name' => 'saldo_akhir',
            'value' => function ($data) {
                return format_number_report($data['saldo_akhir'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>