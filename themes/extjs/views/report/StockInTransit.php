<h1>Stock In Transit</h1>
<h3>Date : <?= $tgl ?></h3>
<?
$this->pageTitle = 'Stock In Transit';
$gridColums = array();
array_push($gridColums,
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Satuan',
            'name' => 'sat'
        ),
        array(
            'header' => 'Dropping Ref.',
            'name' => 'dropping_doc_ref'
        ),
        array(
            'header' => 'Dropping Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Branch Pengirim',
            'name' => 'store_pengirim'
        ),
        array(
            'header' => 'Branch Tujuan',
            'name' => 'store_penerima'
        )
    );


$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));