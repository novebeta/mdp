<h1>Beauty Summary</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle='Beauty Summary';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('nama_beauty'),
    'extraRowColumns' => is_report_excel() ? array() : array('nama_beauty'),
    'extraRowPos' => 'below',
    'extraRowTotals' => function($data, $row, &$totals) {
        if(!isset($totals['sum'])) $totals['sum'] = 0;
        $totals['sum'] += $data['tip'];
        if (!isset($totals['qty'])) $totals['qty'] = 0;
        $totals['qty'] += $data['qty'];
    },
    'extraRowExpression' => '"<span class=\"subtotal\">Total ".$data["nama_beauty"]." Qty : ".$totals["qty"]." Nominal : ".format_number_report($totals["sum"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Beautician Name',
            'name' => 'nama_beauty',
            'footer' => "Total All Services"
        ),
//        array(
//            'header' => 'Item Code',
//            'name' => 'kode_barang',
//            'footer' => "Total All Services"
//        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                    return format_number_report($data['qty']);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Total Services Tips',
            'name' => 'tip',
            'value' => function ($data) {
                    return format_number_report($data['tip'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footerHtmlOptions' => array ('style' => 'text-align: right;' ),
            'footer' => format_number_report($tip,2)
        )
    ),
));
?>