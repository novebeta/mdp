<h1>Report Apotek</h1>

<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'ReportApotek';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => 
    array(
        array(
            'header' => 'Tanggal',
            'name' => 'tgl',
        ),
        array(
            'header' => 'Doc Ref.',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Nama Cust.',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'No Cust.',
            'name' => 'no_customer'
        ),
        array(
            'header' => 'Nama Dokter',
            'name' => 'nama_dokter'
        ),
        array(
            'header' => 'Komposisi',
            'name' => 'nama_bahan'
        ),
        array(
            'header' => 'Qty',
            'name' => 'Qty',
            'value' => function ($data) {
                return format_number_report($data['Qty']);
            }
        //     ,
        //     'htmlOptions' => array('style' => 'text-align: right;'),
        //     'footerHtmlOptions' => array('style' => 'text-align: right;'),
        //     'footer' => format_number_report($qty)
        // ),
        array(
            'header' => 'Satuan',
            'name' => 'sat'
        ),
        
    ),
));
?>