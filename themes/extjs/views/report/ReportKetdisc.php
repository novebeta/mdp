<h1><?=$this->pageTitle?></h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
//$this->pageTitle = 'Inventory Movements';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
       array(
            'header' => 'Keluhan',
            'name' => 'name_',
        ),
        array(
            'header' => 'Total',
            'name' => 'c',
            'value' => function ($data) 
            {
                return format_number_report($data['c'],0);
            }
        )
    )
));
