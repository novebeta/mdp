<h1>Report Hutang</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->pageTitle = 'Report Bonus Beauty, KMR, dan KMT';
$this->widget('ext.groupgridview.GroupGridView', array(
	'id' => 'the-table',
	'dataProvider' => $R_Bonbet,
	'columns' => array(
		array(
			'header' => 'NIK',
			'name' => 'nik'
		),
		array(
			'header' => 'NAMA',
			'name' => 'nama'
		),
		array(
			'header' => 'PAYCODE',
			'name' => 'paycode'
		),
		array(
			'header' => 'TANGGAL',
			'name' => 'tanggal'
		),
		array(
			'header' => 'AMOUNT',
			'name' => 'amount',
			'value' => function ($data) {
				return format_number_report($data['amount'], 2);
			},
			'htmlOptions' => array('style' => 'text-align: right;')
		)
	)
));
?>