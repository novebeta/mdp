<html>
<head>


    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
    <script type='text/javascript' src="<?php echo Yii::app()->request->baseUrl; ?>/js/natasha/sticky/jquery.stickytableheaders.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/natasha/tabel.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/natasha/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css" />

</head>
<body>

<h1>Report Transaksi</h1>
<h2>Periode <?=$from?> sampai <?=$to?></h2>
<div class="application">

    <div class="application">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Transaksi</a></li>
                <li><a href="#tabs-2">Total Per Cream</a></li>
                <li><a href="#tabs-5">Total Per Perawatan</a></li>
                <li><a href="#tabs-3">Detail Transaksi</a></li>
                <li><a href="#tabs-4">Total Tiap Transaksi</a></li>
            </ul>

            <!-- TODO: ---------------------------------------------------------------------------RINCIAN PER-TRANSAKSI -->

            <div id="tabs-1">
                <table class="full">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>No Faktur</th>
                        <th>Customer</th>
                        <th>Item</th>
                        <th>Qty</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?
                    $tot_qty = 0;
                    foreach ($detail as $row)
                    {
                        echo '<tr>';
                        echo '<td>'.date('d F Y', strtotime($row['tgl'])).'</td>';
                        echo '<td>'.$row['doc_ref'].'</td>';
                        echo '<td>'.$row['pasien'].'</td>';
                        echo '<td>'.$row['kode_barang'].'</td>';
                        echo '<td>'.number_format($row['qty'], 2, ',', '.').'</td>';
                        echo '</tr>';
	                    $tot_qty += $row['qty'];
                    }
                    echo '<tr>';
                    echo '<td colspan="4" style="text-align: right"><strong>TOTAL</strong></td>';
                    echo '<td>'.number_format($tot_qty, 2, ',', '.').'</td>';
                    echo '</tr>';
                    ?>
                    </tbody>
                </table>
            </div>

            <!-- TODO: ---------------------------------------------------------------------------TOTAL PER ITEM -->

            <div id="tabs-2">
                <table class="full">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Harga</th>
                        <th>Qty</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?
                    $tot_qty = 0;
                    $tot_harga = 0;
                    foreach ($cream as $row)
                    {
                        echo '<tr>';
                        echo '<td>'.$row['kode_barang'].'</td>';
                        echo '<td>'.number_format($row['total_harga'], 2, ',', '.').'</td>';
                        echo '<td>'.number_format($row['total_qty'], 2, ',', '.').'</td>';
                        echo '</tr>';
	                    $tot_qty +=$row['total_qty'];
	                    $tot_harga +=$row['total_harga'];
                    }
                    echo '<tr>';
                    echo '<td  style="text-align: right"><strong>TOTAL</strong></td>';
                    echo '<td>'.number_format($tot_harga, 2, ',', '.').'</td>';
                    echo '<td>'.number_format($tot_qty, 2, ',', '.').'</td>';
                    echo '</tr>';
                    ?>
                    </tbody>
                </table>
            </div>




            <div id="tabs-5">
                <table class="full">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Harga</th>
                        <th>Qty</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?
                    $tot_qty = 0;
                    $tot_harga = 0;
                    foreach ($perawatan as $row)
                    {
	                    echo '<tr>';
	                    echo '<td>'.$row['kode_barang'].'</td>';
	                    echo '<td>'.number_format($row['total_harga'], 2, ',', '.').'</td>';
	                    echo '<td>'.number_format($row['total_qty'], 2, ',', '.').'</td>';
	                    echo '</tr>';
	                    $tot_qty +=$row['total_qty'];
	                    $tot_harga +=$row['total_harga'];
                    }

                    echo '<tr>';
                    echo '<td style="text-align: right"><strong>TOTAL</strong></td>';
                    echo '<td>'.number_format($tot_harga, 2, ',', '.').'</td>';
                    echo '<td>'.number_format($tot_qty, 2, ',', '.').'</td>';
                    echo '</tr>';
                    ?>
                    </tbody>
                </table>
            </div>
            <!-- TODO: ---------------------------------------------------------------------------DETAIL TRANSAKSI -->

            <div id="tabs-3">
                <table class="full">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th style="width:180px">No Faktur</th>
                        <th>Customer</th>
                        <th style="width:150px" >Item</th>
                        <th style="width:40px" >Qty</th>
                        <th style="width:130px" >Harga</th>
                        <th style="width:80px">Diskon</th>
                        <th style="width:130px" >Total NET</th>
                        <th style="width:80px" >VAT</th>
                        <th style="width:110px" >Total</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?
                    $tot_faktur = 0;
                    $tot_all = 0;
                    $tot_faktur_all = 0;
                    $tot_net_all = 0;
                    $tot_qty_all = 0;
                    $tot_bruto_all = 0;
                    $tot_disc_all = 0;
                    $tot_vat_all = 0;
                    $rounding = 0;
                    $tmp = "";
                    foreach ($detail_all as $row)
                    {
	                    if ($tmp == "")
		                    $tmp = $row['doc_ref'];

	                    if ($tmp != $row['doc_ref'])
	                    {
		                    $tot_faktur += $rounding;
		                    echo '<tr>';
		                    echo '<td colspan="5" style="text-align: right"><strong>TOTAL NET</strong></td>';
		                    echo '<td><strong>'.number_format($tot_net, 2, ',', '.').'</strong></td>';
		                    echo '<td style="text-align: right"><strong>ROUNDING</strong></td>';
		                    echo '<td><strong>'.number_format($rounding, 2, ',', '.').'</strong></td>';
		                    echo '<td style="text-align: right"><strong>TOTAL ALL</strong></td>';
		                    echo '<td><strong>'.number_format($tot_faktur, 2, ',', '.').'</strong></td>';
		                    echo '</tr>';
		                    $tmp = $row['doc_ref'];
		                    $tot_faktur_all += $tot_faktur;
		                    $tot_faktur = 0;
		                    $tot_net = 0;
	                    }
	                    $rounding = $row['rounding'];

	                    echo '<tr>';
	                    echo '<td>'.date('d F Y', strtotime($row['tgl'])).'</td>';
	                    echo '<td>'.$row['doc_ref'].'</td>';
	                    echo '<td>'.$row['pasien'].'</td>';
	                    echo '<td>'.$row['kode_barang'].'</td>';
	                    echo '<td>'.number_format($row['qty'], 2, ',', '.').'</td>';
	                    echo '<td>'.number_format($row['bruto'], 2, ',', '.').'</td>';
	                    echo '<td>'.number_format($row['total_pot'], 2, ',', '.').'</td>';
	                    echo '<td>'.number_format($row['net'], 2, ',', '.').'</td>';
	                    echo '<td>'.number_format($row['vatrp'], 2, ',', '.').'</td>';
	                    echo '<td>'.number_format($row['total'], 2, ',', '.').'</td>';
	                    echo '</tr>';

	                    $tot_net += $row['net'];
	                    $tot_faktur += $row['total'];
	                    $tot_net_all += $row['net'];
	                    $tot_qty_all += $row['qty'];
	                    $tot_bruto_all += $row['bruto'];
	                    $tot_disc_all += $row['total_pot'];
	                    $tot_vat_all += $row['vatrp'];
                    }

                    $rounding = $row['rounding'];
                    $tot_faktur += $rounding;
                    $tot_faktur_all += $tot_faktur;
                    echo '<tr>';
                    echo '<td colspan="5" style="text-align: right"><strong>TOTAL NET</strong></td>';
                    echo '<td><strong>'.number_format($tot_net, 2, ',', '.').'</strong></td>';
                    echo '<td style="text-align: right"><strong>ROUNDING</strong></td>';
                    echo '<td><strong>'.number_format($rounding, 2, ',', '.').'</strong></td>';
                    echo '<td style="text-align: right"><strong>TOTAL ALL</strong></td>';
                    echo '<td><strong>'.number_format($tot_faktur, 2, ',', '.').'</strong></td>';
                    echo '</tr>';

                    echo '<tr>';
                    echo '<td colspan="4" style="text-align: right"><strong>TOTAL</strong></td>';
                    echo '<td><strong>'.number_format($tot_qty_all, 2, ',', '.').'</strong></td>';
                    echo '<td><strong>'.number_format($tot_bruto_all, 2, ',', '.').'</strong></td>';
                    echo '<td><strong>'.number_format($tot_disc_all, 2, ',', '.').'</strong></td>';
                    echo '<td><strong>'.number_format($tot_net_all, 2, ',', '.').'</strong></td>';
                    echo '<td><strong>'.number_format($tot_vat_all, 2, ',', '.').'</strong></td>';
                    echo '<td><strong>'.number_format($tot_faktur_all, 2, ',', '.').'</strong></td>';
                    echo '</tr>';
                    ?>
                    </tbody>
                </table>
            </div>


            <!-- TODO: ---------------------------------------------------------------------------TOTAL PER TRANSAKSI -->
            <div id="tabs-4">
                <table class="full">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>No Faktur</th>
                        <th>Customer</th>
                        <th style="width:60px">Counter</th>
                        <th style="width:80px">User</th>
                        <th style="width:100px">Nama</th>
                        <th>Payment</th>
                        <th>Total</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?
                    $total_amount = 0;
                    foreach ($total as $row)
                    {
                        echo '<tr>';
                        echo '<td>'.date('d F Y', strtotime($row['tgl'])).'</td>';
                        echo '<td>'.$row['doc_ref'].'</td>';
                        echo '<td>'.$row['pasien'].'</td>';
	                    echo '<td>'.$row['counter'].'</td>';
	                    echo '<td>'.$row['user_id'].'</td>';
	                    echo '<td>'.$row['name'].'</td>';
                        echo '<td>'.$row['nama_bank'].'</td>';
                        echo '<td>'.number_format($row['amount'], 2, ',', '.').'</td>';
                        echo '</tr>';
                        $total_amount += $row['amount'];
                    }
                    ?>
                    <tr>
                        <td colspan="7" style="text-align: right"><strong>TOTAL</strong></td>
                        <td><strong><?=number_format($total_amount, 2, ',', '.')?></strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>



</div>
</body>



<script>
    $("#tabs").tabs();
    $("table").stickyTableHeaders();
</script>
