<h1>Invoice Supplier Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Supplier : <?= $supplier ?></h3>
<?php
$this->pageTitle = 'Invoice Supplier Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType'=> 'nested',
    'mergeColumns' => array('type_', 'tgl', 'doc_ref', 'supplier_name', 'tgl_jatuh_tempo'),
    'extraRowColumns' => array('doc_ref'),
    'extraRowPos' => 'below',
    'extraRowTotals' => function ($data, $row, &$totals) {
        if (!isset($totals['sum'])) {
            $totals['sum'] = 0;
        }
        $totals['sum'] += $data['total'];
    },
    'extraRowExpression' => '"<span class=\"subtotal\">Total ".$data["doc_ref"]." : ".format_number_report($totals["sum"])."</span>"',
    'columns' => array(
        array(
            'header' => 'Tipe',
            'name' => 'type_'
        ),
        array(
            'header' => 'Tanggal',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Doc. Ref.',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Supplier',
            'name' => 'supplier_name'
        ),
        array(
            'header' => 'Jatuh Tempo',
            'name' => 'tgl_jatuh_tempo'
        ),
        array(
            'header' => 'Charge',
            'name' => 'charge'
        ),
        array(
            'header' => 'Item',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                    return format_number_report($data['qty'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'value' => function ($data) {
                    return format_number_report($data['price'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Bruto',
            'name' => 'sub_total',
            'value' => function ($data) {
                    return format_number_report($data['sub_total'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Discount',
            'name' => 'disc_rp',
            'value' => function ($data) {
                    return format_number_report($data['disc_rp'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'PPN',
            'name' => 'ppn_rp',
            'value' => function ($data) {
                    return format_number_report($data['ppn_rp'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'PPh',
            'name' => 'pph_id',
            'value' => function ($data) {
                    switch($data['pph_id']){
                        case 21:
                        case 22:
                        case 23:
                            return "PPH ".$data['pph_id'];
                        default:
                            return "-";
                    };
                },
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'PPh',
            'name' => 'pph_rp',
            'value' => function ($data) {
                    return format_number_report($data['pph_rp'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footer' => "Total All"
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                    return format_number_report($data['total'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footerHtmlOptions' => array ('style' => 'text-align: right;' ),
            'footer' => format_number_report($total,2)
        ),
        array(
            'header' => 'Status',
            'name' => 'status',
            'value' => function ($data) {
                switch ($data['status']) {
                    case 0: return 'OPEN'; break;
                    case 1: return 'LUNAS'; break;
                    default: return '-'; break;
                }
            },
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )
    ),
));
?>