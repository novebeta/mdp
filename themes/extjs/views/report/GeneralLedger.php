<h1>General Ledger</h1>
<h2><?= $account_code ?> - <?= $account_name ?></h2>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<!--<h3>BRANCH : --><? //= $store ?><!--</h3>-->
<?
$this->pageTitle = 'General Ledger';
$this->widget( 'ext.groupgridview.GroupGridView', array(
	'id'           => 'the-table',
	'dataProvider' => $dp,
	'columns'      => array(
		array(
			'header' => 'Date',
			'name'   => 'tgl'
		),
//        array(
//            'header' => 'Branch',
//            'name' => 'store'
//        ),
		array(
			'header' => 'Description',
			'name'   => 'memo_'
		),
		array(
			'header'      => 'Debit',
			'name'        => 'Debit',
			'value'       => function ( $data ) {
				if ( $data['Debit'] != null ) {
					return number_format( $data['Debit'], 0 );
				}
				return '';
			},
			'htmlOptions' => array( 'style' => 'text-align: right;' )
		),
		array(
			'header'      => 'Credit',
			'name'        => 'Credit',
			'value'       => function ( $data ) {
				if ( $data['Credit'] != null ) {
					return number_format( $data['Credit'], 0 );
				}
				return '';
			},
			'htmlOptions' => array( 'style' => 'text-align: right;' )
		),
		array(
			'header'      => 'Balance',
			'name'        => 'Balance',
			'value'       => function ( $data ) {
				return number_format( $data['Balance'], 0 );
			},
			'htmlOptions' => array( 'style' => 'text-align: right;' )
		)
	)
) );
?>