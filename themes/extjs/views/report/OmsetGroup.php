<h1>Omset Group</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Omset Group';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array(),
    'extraRowColumns' => is_report_excel() ? array() : array(),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
//        array(
//            'header' => 'Date',
//            'name' => 'tgl'
//        ),
        array(
            'header' => 'Group',
            'name' => 'nama_grup'
        ),
        array(
            'header' => 'Bruto',
            'name' => 'bruto',
            'value' => function ($data) {
                return format_number_report($data['bruto'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($bruto, 2)
        ),
        array(
            'header' => 'Disc Amount',
            'name' => 'discrp',
            'value' => function ($data) {
                return format_number_report($data['discrp'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($discrp, 2)
        ),
        array(
            'header' => 'Disc 2 Amount',
            'name' => 'discrp1',
            'value' => function ($data) {
                return format_number_report($data['discrp1'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($discrp1, 2)
        ),
        array(
            'header' => 'Total Disc',
            'name' => 'total_pot',
            'value' => function ($data) {
                return format_number_report($data['total_pot'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total_pot, 2)
        ),
        array(
            'header' => 'VAT',
            'name' => 'vatrp',
            'value' => function ($data) {
                return format_number_report($data['vatrp'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($vatrp, 2)
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        )
    ),
));
?>