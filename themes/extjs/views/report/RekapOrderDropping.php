<h1>Rekap Transfer Request <?= ($ratarata?"(Rata-rata)":"") ?></h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Branch : <?= $branch ?></h3>
<?
if($_POST['approved'] != "all") echo "<h3>Status : ".($_POST['approved']==1?"":"NOT ")."APPROVED</h3>";

$this->pageTitle = 'Rekap Transfer Request';
$gridColums = array();
array_push($gridColums,
        array(
            'header' => 'Kode Barang',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Nama Barang',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Satuan',
            'name' => 'sat'
        )
);

foreach($store as $s){
    array_push($gridColums,
        array(
            'header' => $s['store'],
            'name' => $s['store'],
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    );
}

array_push($gridColums,
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                if($data['show_ratarata']){
                    return number_format($data['total'], 2);
                }
                else {
                    return format_number_report($data['total'], 0);
                }
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));