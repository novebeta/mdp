<h1>Doctors Service Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?php
$this->pageTitle = 'Doctors Service Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => "No. ". $pt_negara ." Receipt",
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Doctor Name',
            'name' => 'nama_dokter'
        ),
        array(
            'header' => 'Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'Service Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Service Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Total Services Tips',
            'name' => 'tip',
            'value' => function ($data) {
                return format_number_report($data['tip']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    ),
));
?>