<h1>Produksi Total</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?php
$this->pageTitle='Produksi Total';

$columns = [];
$rec = $dp->rawData;
if(count($rec)>0)
foreach($rec[0] as $k=>$v){
    $clm = array(
        'name' => $k
    );
    
    switch ($k){
        case 'kode_barang': $clm['header'] = 'Kode Barang';break;
        case 'nama_barang': $clm['header'] = 'Nama Barang';break;
        case 'sat': $clm['header'] = 'Satuan';break;
        case 'qty':
            $clm['header'] = 'Qty';
            $clm['value'] = function ($data) {
                    return format_number_report($data['qty'], 2);
                };
            $clm['htmlOptions'] = array ('style' => 'text-align: right;' );
            break;
    }
    
    array_push($columns, $clm);
}

$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array(),
    'columns' => $columns
));
?>