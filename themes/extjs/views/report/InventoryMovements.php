<h1><?=$this->pageTitle?></h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
//$this->pageTitle = 'Inventory Movements';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Kode Barang',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Nama Barang',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Stok Awal',
            'name' => 'before',
            'value' => function ($data) {
                return format_number_report($data['before'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
//        array(
//            'header' => 'Purchase (IN)',
//            'name' => 'Purchase',
//            'value' => function ($data) {
//                return format_number_report($data['Purchase'],0);
//            },
//            'htmlOptions' => array('style' => 'text-align: right;')
//        ),
//        array(
//            'header' => 'Return Sales (IN)',
//            'name' => 'ReturnSales',
//            'value' => function ($data) {
//                return format_number_report($data['ReturnSales'],0);
//            },
//            'htmlOptions' => array('style' => 'text-align: right;')
//        ),
        array(
            'header' => 'Pembelian',
            'name' => 'BarangMasuk',
            'value' => function ($data) {
                return format_number_report($data['BarangMasuk'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
//        array(
//            'header' => 'Return Purchase (OUT)',
//            'name' => 'ReturnPurchase',
//            'value' => function ($data) {
//                return format_number_report($data['ReturnPurchase'],0);
//            },
//            'htmlOptions' => array('style' => 'text-align: right;')
//        ),
        array(
            'header' => 'Penjualan',
            'name' => 'Sales',
            'value' => function ($data) {
                return format_number_report($data['Sales'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
//        array(
//            'header' => 'Real Sales (OUT)',
//            'name' => 'RealSales',
//            'value' => function ($data) {
//                return format_number_report($data['RealSales'],0);
//            },
//            'htmlOptions' => array('style' => 'text-align: right;')
//        ),
//        array(
//            'header' => 'Barang Keluar (OUT)',
//            'name' => 'BarangKeluar',
//            'value' => function ($data) {
//                return format_number_report($data['BarangKeluar'],0);
//            },
//            'htmlOptions' => array('style' => 'text-align: right;')
//        ),
        array(
            'header' => 'Stok Akhir',
            'name' => 'after',
            'value' => function ($data) {
                return format_number_report($data['after'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Cost Price',
            'name' => 'price',
            'visible' => $user->is_available_role(351),
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));