<h1>Rekap Kegiatan Marketing</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>

<?
$this->pageTitle='Rekap Kegiatan ';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('tgl', 'doc_ref', 'store'),
   
    'columns' => array(
        array(
            'header' => 'Tanggal Post',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Tanggal Kegiatan ',
            'name' => 'tgl_keg'
        ),
        array(
            'header' => 'Branch',
            'name' => 'store'
        ),        
        array(
            'header' => 'Doc. Ref',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Jenis Kegiatan',
            'name' => 'account_name'
        ),      
        array(
            'header' => 'Note',
            'name' => 'note',
            'footer' => "Total :"
        ),
//        array(
//            'header' => 'Debit',
//            'name' => 'Debit',
//            'value' => function ($data) {
//                    return format_number_report($data['Debit'], 2);
//                },
//            'htmlOptions' => array ('style' => 'text-align: right;' )
//        ),
        array(
            'header' => 'Biaya',
            'name' => 'total_biaya',
            'value' => function ($data) {
                    return format_number_report($data['total_biaya'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
                'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
//        array(
//            'header' => 'Balance',
//            'name' => 'Balance',
//            'value' => function ($data) {
//                    return format_number_report($data['Balance'], 2);
//                },
//            'htmlOptions' => array ('style' => 'text-align: right;' )
//        )
    )
));
?>