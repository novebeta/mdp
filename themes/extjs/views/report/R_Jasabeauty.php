<html>
<head>


    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
    <script type='text/javascript' src="<?php echo Yii::app()->request->baseUrl; ?>/js/natasha/sticky/jquery.stickytableheaders.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/natasha/tabel.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/natasha/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css" />

</head>
<body>

<h1>Report Jasa Beauty</h1>
<h2>Periode <?=$from?> sampai <?=$to?></h2>
<div class="application">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Perincian Jasa</a></li>
            <li><a href="#tabs-2">Total Jasa Tiap Perawatan</a></li>
            <li><a href="#tabs-3">Total Jasa Beauty</a></li>
            <li><a href="#tabs-4">Total KMR-KMT Tiap Perawatan</a></li>
        </ul>

        <div id="tabs-1">
            <table class="full">
                <thead>
                <tr>
                    <th>Tanggal</th>
                    <th style="width:18%">No Faktur</th>
                    <th>Nama Pasien</th>
                    <th>Perawatan</th>
                    <th>Qty</th>
                    <th>KMR</th>
                    <th>KMT</th>
                    <th>Beauty</th>
                    <th>Jasa</th>
                </tr>
                </thead>

                <tbody>
                <?
                    foreach ($detail as $row)
                    {
                        echo '<tr>';
                        echo '<td>'.date('d F Y', strtotime($row['tgl'])).'</td>';
	                    echo '<td>'.$row['doc_ref'].'</td>';
	                    echo '<td>'.$row['nama_customer'].'</td>';
	                    echo '<td>'.$row['kode_barang'].'</td>';
	                    echo '<td>'.$row['qty'].'</td>';
	                    echo '<td>'.$row['dokter_kmr'].'</td>';
	                    echo '<td>'.$row['dokter_kmt'].'</td>';
	                    echo '<td>'.$row['beauty'].'</td>';
	                    echo '<td>'.$row['amount'].'</td>';
	                    echo '</tr>';
                    }
                ?>
                </tbody>
            </table>
        </div>

        <div id="tabs-2">
            <table class="full">
                <thead>
                <tr>
                    <th>Beauty</th>
                    <th>Perawatan</th>
                    <th>Jumlah</th>
                    <th>Total Jasa</th>
                </tr>
                </thead>

                <tbody>
			    <?
			    foreach ($totalitem as $row)
			    {
				    echo '<tr>';
				    echo '<td>'.$row['beauty'].'</td>';
				    echo '<td>'.$row['kode_barang'].'</td>';
				    echo '<td>'.$row['qty'].'</td>';
				    echo '<td>'.$row['total'].'</td>';
				    echo '</tr>';
			    }
			    ?>
                </tbody>
            </table>
        </div>

        <div id="tabs-3">
            <table class="full">
                <thead>
                <tr>
                    <th>Beauty</th>
                    <th>Total</th>
                </tr>
                </thead>

                <tbody>
			    <?
			    foreach ($totalall as $row)
			    {
				    echo '<tr>';
				    echo '<td>'.$row['beauty'].'</td>';
				    echo '<td>'.$row['total'].'</td>';
				    echo '</tr>';
			    }
			    ?>
                </tbody>
            </table>
        </div>

        <div id="tabs-4">
            <table class="full">
                <thead>
                <tr>
                    <th colspan="2">KMR</th>
                </tr>
                <tr>
                    <th>Nama Dokter</th>
                    <th>Total</th>
                </tr>
                </thead>

                <tbody>
			    <?
			    $all = 0;
			    $tmp = "";
			    $tot = 0;
			    foreach($totalkmr as $row)
			    {
				    if ($tmp == "")
					    $tmp = $row['kmr'];

				    if ($tmp != $row['kmr'])
				    {
					    echo '<tr>';
					    echo '<td class="text_right"><strong>TOTAL '.$tmp.'</strong></td>';
					    echo '<td><strong>'.number_format(abs($tot), 2, ',', '.').'</strong></td>';
					    echo '</tr>';
					    $tot = 0;
					    $tmp = $row['kmr'];
				    }
				    echo '<tr>';
				    echo '<td>'.$tmp.'</td>';
				    echo '<td>'.$row['total'].'</td>';
				    echo '</tr>';
				    $all += abs($row['total']);
				    $tot += abs($row['total']);
			    }

			    echo '<tr>';
			    echo '<td class="text_right"><strong>TOTAL '.$tmp.'</strong></td>';
			    echo '<td><strong>'.number_format(abs($tot), 2, ',', '.').'</strong></td>';
			    echo '</tr>';
			    ?>
                <tr>
                    <td><strong>TOTAL</strong></td>
                    <td><strong><?=number_format(abs($all), 2, ',', '.')?></strong></td>
                </tr>
                </tbody>
            </table>
            <br />
            <br />


            <table class="full">
                <thead>
                <tr>
                    <th colspan="3">KMR Perawatan</th>
                </tr>
                <tr>
                    <th colspan="3">Hanya Untuk Evaluasi Kinerja Peresepan Perawatan</th>
                </tr>
                <tr>
                    <th>Nama Dokter</th>
                    <th>Perawatan</th>
                    <th>Total</th>
                </tr>
                </thead>

                <tbody>
			    <?
			    $all = 0;
			    $tmp = "";
			    $tot = 0;
			    foreach($totalkmritem as $row)
			    {
				    if ($tmp == "")
					    $tmp = $row['kmr'];

				    if ($tmp != $row['kmr'])
				    {
					    echo '<tr>';
					    echo '<td colspan="2" class="text_right"><strong>TOTAL '.$tmp.'</strong></td>';
					    echo '<td><strong>'.$tot.'</strong></td>';
					    echo '</tr>';
					    $tot = 0;
					    $tmp = $row['kmr'];
				    }

				    echo '<tr>';
				    echo '<td>'.$row['kmr'].'</td>';
				    echo '<td>'.$row['kode_barang'].'</td>';
				    echo '<td>'.abs($row['qty']).'</td>';
				    echo '</tr>';

				    $all += number_format(abs($row['qty']), 2, ',', '.');
				    $tot += number_format(abs($row['qty']), 2, ',', '.');
			    }
			    echo '<tr>';
			    echo '<td colspan="2" class="text_right"><strong>TOTAL '.$tmp.'</strong></td>';
			    echo '<td><strong>'.$tot.'</strong></td>';
			    echo '</tr>';
			    ?>
                <tr>
                    <td colspan="2"><strong>TOTAL</strong></td>
                    <td><strong><?=$all?></strong></td>
                </tr>
                </tbody>
            </table>
            <br />
            <br />

            <table class="full">
                <thead>
                <tr>
                    <th colspan="4">KMT</th>
                </tr>
                <tr>
                    <th>Nama Dokter</th>
                    <th>Perawatan</th>
                    <th>Qty</th>
                    <th>Net</th>
                </tr>
                </thead>

                <tbody>
			    <?
			    $all = 0;
			    $all_net = 0;
			    $tmp = "";
			    $tot = 0;
			    $tot_net = 0;
			    foreach($totalkmt as $row)
			    {
				    if ($tmp == "")
					    $tmp = $row['kmt'];

				    if ($tmp != $row['kmt'])
				    {
					    echo '<tr>';
					    echo '<td colspan="2" class="text_right"><strong>TOTAL '.$tmp.'</strong></td>';
					    echo '<td><strong>'.$tot.'</strong></td>';
					    echo '<td><strong>'.number_format($tot_net, 2, ',', '.').'</strong></td>';
					    echo '</tr>';
					    $tot = 0;
					    $tot_net = 0;
					    $tmp = $row['kmt'];
				    }

				    echo '<tr>';
				    echo '<td>'.$row['kmt'].'</td>';
				    echo '<td>'.$row['kode_barang'].'</td>';
				    echo '<td>'.abs($row['qty']).'</td>';
				    echo '<td>'.number_format(abs($row['net']), 2, ',', '.').'</td>';
				    echo '</tr>';

				    $all += number_format(abs($row['qty']), 2, ',', '.');
				    $tot += number_format(abs($row['qty']), 2, ',', '.');
				    $tot_net += abs($row['net']);
				    $all_net += abs($row['net']);
			    }
			    echo '<tr>';
			    echo '<td colspan="2" class="text_right"><strong>TOTAL '.$tmp.'</strong></td>';
			    echo '<td><strong>'.$tot.'</strong></td>';
			    echo '<td><strong>'.number_format($tot_net, 2, ',', '.').'</strong></td>';
			    echo '</tr>';
			    ?>
                <tr>
                    <td colspan="2"><strong>TOTAL</strong></td>
                    <td><strong><?=$all?></strong></td>
                    <td><strong><?=number_format(abs($all_net), 2, ',', '.');?></strong></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>

</div>
</body>



<script>
    $("#tabs").tabs();
    $("table").stickyTableHeaders();
</script>
