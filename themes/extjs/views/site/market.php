<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/grid.locale-en.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_store.js"></script>
<script>
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu().app()->params['url_logo']; ?>" alt=""/>';
    SALES_OVERRIDE = false;
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    jun.ajaxCounter = 0;
    jun.runner = new Ext.util.TaskRunner();
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        var a = function () {
            Ext.get("loading").remove();
            Ext.fly("loading-mask").fadeOut({
                remove: !0
            });
        };
        Ext.QuickTips.init();
//    loadText = "Sedang proses... silahkan tunggu";
        Ext.Ajax.on("beforerequest", function (conn, opts) {
            jun.myMask.show();
            jun.ajaxCounter++;
        });
        Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
            if (jun.ajaxCounter > 1) {
                jun.ajaxCounter--;
            } else {
                jun.ajaxCounter = 0;
                jun.myMask.hide();
            }
        });
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            if (jun.ajaxCounter > 1) {
                jun.ajaxCounter--;
            } else {
                jun.ajaxCounter = 0;
                jun.myMask.hide();
            }
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        var viewport = new Ext.Viewport({
            layout: 'border',
            items: [
                {
                    region: 'west',
                    id: 'west-panel',
                    split: true,
                    width: '60%',
                    collapsible: false,
                    margins: '5 0 5 5',
                    cmargins: '35 5 5 5',
                    layout: 'accordion',
                    layoutConfig: {
                        animate: true
                    },
                    items: [
                        new jun.AntrianGridWin({}),
                        {
                            html: '<table id="jsGrid" style="width:100%;"></table>',
//                            title: 'History Terapi',
                            items: [
                                {
                                    html: '<table id="gridPesanan" style="width:100%;"></table>',
                                    title: 'Pesanan'
                                },
                                {
                                    html: '<table id="gridHistoryTerapi" style="width:100%;"></table>',
                                    title: 'History Terapi'
                                }
                            ]
                        }
                    ]
                },
                {
                    region: 'center',
                    margins: '5 5 5 0',
                    layout: {
                        type: 'vbox',
                        padding: '5',
                        align: 'stretch'
                    },
                    defaults: {
                        margins: '0 0 5 0',
                        padding: '5'
                    },
                    items: [
                        {
                            title: 'Informasi Pasien',
                            ref: '../../panel_info_pasien',
                            id: 'panel_info_pasien_id',
                            height: 100
                        },
                        new jun.SalestransNoBeautyWin({
                            title: "Sales",
//                            ref: '../../gridDetails',
//                            frameHeader: !1,
//                            header: !1,
                            flex: 1,
                            margins: '0'
                        })
                    ],
                    fbar: {
                        xtype: 'toolbar',
                        items: [
                            {
                                xtype: 'button',
                                scale: 'large',
                                width: 100,
                                text: 'Logout',
                                ref: '../btnlogout'
                            },
                            {
                                xtype: 'button',
                                scale: 'large',
                                width: 100,
                                text: 'Save',
                                ref: '../btnSaveClose'
                            }
                        ]
                    }
                }
            ]
        });
        var data = {
            name: 'Jack Slocum',
            company: 'Ext JS, LLC',
            address: '4 Red Bulls Drive',
            city: 'Cleveland',
            state: 'Ohio',
            zip: '44102',
            kids: [{
                name: 'Sara Grace',
                age: 3
            }, {
                name: 'Zachary',
                age: 2
            }, {
                name: 'John James',
                age: 0
            }]
        };
        var tpl = new Ext.Template(
            '<p>Name: {name}</p>',
            '<p>Company: {company}</p>',
            '<p>Location: {city}, {state}</p>'
        );
        var p = Ext.getCmp('panel_info_pasien_id');
        tpl.overwrite(p.body, data);
        p.body.highlight('#c3daf9', {block: true});
        $(function () {
            'use strict';
            var arrtSetting = function (rowId, val, rawObject, cm) {
                var attr = rawObject.attr[cm.name], result;
                if (attr.rowspan) {
                    result = ' rowspan=' + '"' + attr.rowspan + '"';
                } else if (attr.display) {
                    result = ' style="display:' + attr.display + '"';
                }
                return result;
            };
            $("#gridHistoryTerapi").jqGrid({
                url: 'http://localhost:81/posng/customers/gethistory/nobase/7559KDR01',
                postData: {
                    nobase: 1000
                },
                mtype: 'POST',
                datatype: "json",
                colNames: ['Faktur', 'Tgl', 'Kode Barang', 'Qty', 'Unit', 'Cabang'],
                colModel: [
                    {name: 'no_faktur', align: 'center', cellattr: arrtSetting},
                    {name: 'tgl'},
                    {name: 'kd_brng', width: 75},
                    {name: 'qty', align: 'right', width: 50},
                    {name: 'satuan'},
                    {name: 'nama'}
                ],
                cmTemplate: {sortable: false},
                rowNum: 100,
                //rowList: [5, 10, 20],
                //pager: '#pager',
                gridview: true,
                hoverrows: false,
                autoencode: true,
                ignoreCase: true,
                viewrecords: true,
                height: '100%',
                width: '100%',
                beforeSelectRow: function () {
                    return false;
                }
            });
            $("#gridHistoryTerapi").jqGrid('clearGridData');
            $("#gridHistoryTerapi").jqGrid('setGridParam', {postData: {nobase: '7559KDR01'}});
            $("#gridHistoryTerapi").trigger('reloadGrid');
        });
    });
</script>
<div id="dokterView" style="height: 100%"></div>