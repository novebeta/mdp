<script>
    NATASHA_CUSTOM = <? echo NATASHA_CUSTOM ? 'true' : 'false'?>;
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/defiant.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/CheckColumn.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grup_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/locstor.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/barang_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/dokter_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beauty_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beautyOndutyStatus_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beautyServices_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beautyServices_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beautyServices_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/aishaAntrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/diagnosa_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsulDetil_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsulDetil_grid.js"></script>
<script>
    //    try {
    //        var gui = require('nw.gui');
    //        if (gui != null) {
    //            var win = gui.Window.get();
    //            win.x = 1366;
    //            win.y = 0;
    //            win.maximize();
    //			var Player = require('player');
    //			var player = new Player('./1.mp3');
    //			player.play(function(err, player){
    //				console.log('playend!');
    //			});
    //        }
    //    } catch (err) {
    //        console.log(err.message);
    //    }
    startConnection();

    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu() . app()->params['url_logo']; ?>" alt=""/>';
    SALES_OVERRIDE = false;
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    jun.ajaxCounter = 0;
    jun.runner = new Ext.util.TaskRunner();
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        Ext.QuickTips.init();
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        jun.reloadAllStorePerawatan = function () {
            Ext.Ajax.request({
                url: 'SysPrefs/get',
                method: 'POST',
                params: {
                    val: 'tdate_antrian'
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    var msg = window.btoa(response.msg);
                    var tdate_antrian = Locstor.get('tdate_antrian');
                    if (tdate_antrian != null) {
                        if (tdate_antrian != msg) {
                            jun.rztAntrianBeautyTrans.reload();
                            jun.rztAntrianPendingBeautyTrans.reload();
                            jun.rztProsesBeautyTrans.reload();
                            jun.rztTambahanBeautyTrans.reload();
                            jun.rztBeautyOndutyStatus.reload();
                            jun.rztBeautyOffduty1Cmp.reload();
//            jun.rztBeautyOffduty2Cmp.reload();
//            jun.rztBeautyOffduty3Cmp.reload();
//            jun.rztBeautyOffduty4Cmp.reload();
//            jun.rztBeautyOffduty5Cmp.reload();
                        }
                    }
                    Locstor.set('tdate_antrian', msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        };
        jun.runner.start({
            run: jun.reloadAllStorePerawatan,
            interval: 5 * 1000
        });
        var viewport = new Ext.Viewport({
            layout: 'border',
            layoutConfig: {
                padding: 2,
                align: 'stretch'
            },
            items: [
                {
                    region: 'center',
                    anchor: '100%',
                    layout: 'hbox',
                    border: false,
                    frame: false,
                    layoutConfig: {
                        padding: 1,
                        align: 'stretch'
                    },
                    items: [
//                        new jun.BeautyServicesOpenGrid({
//                            title: 'Perawatan Antrian',
//                            margins: '0 2 0 0',
//                            // header: !1,
//                            flex: 5
//                        }),
                        new jun.AntrianPerawatWin({
                            title: 'Antrian Perawatan',
                            margins: '0 2 0 0',
                            // header: !1,
                            flex: 7
                        }),
                        {
                            layout: 'vbox',
                            border: false,
                            frame: false,
                            layoutConfig: {
                                padding: 1,
                                align: 'stretch'
                            },
                            flex: 3,
                            items: [
                                new jun.KonsulGrid({
                                    title: 'Perawatan Tambahan',
                                    // margins: '0 5 0 0',
                                    // header: !1,
                                    flex: 6
                                }),
                                new jun.PerawatTambahanGrid({
                                    title: 'Perawatan VIP',
                                    // margins: '0 5 0 0',
                                    // header: !1,
                                    flex: 4
                                })
                            ]
                        }
                    ]
                },
                {
                    region: 'south',
                    height: 200,
                    anchor: '100%',
                    layout: 'hbox',
                    border: false,
                    frame: false,
                    layoutConfig: {
                        padding: 2,
                        align: 'stretch'
                    },
                    items: [
                        new jun.BeautyServicesProsesGrid({
                            title: 'Proses Perawatan',
                            ref: '../BeautyServicesProsesGrid',
                            margins: '0 2 0 0',
                            flex: 6
                        }),
                        new jun.BeautyMostReadyGrid({
                            title: 'Beauty',
                            ref: '../BeautyMostReadyGrid',
//                            margins: '0 2 0 0',
                            flex: 4
                        })
                    ]
                }
            ]
        });
    });
</script>
<div id="perawatanView" style="height: 100%"></div>