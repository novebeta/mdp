<script>
    DATE_NOW = Date.parseDate('<?=date( "Y-m-d H:i:s" )?>', 'Y-m-d H:i:s');
    DATE_NOW_UTC = Date.parseDate('<?=gmdate( 'Y-m-d H:i:s' )?>', 'Y-m-d H:i:s');
    BASE_URL = '<? echo Yii::app()->homeUrl; ?>';
</script>