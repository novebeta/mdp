<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/defiant.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/tabs/Ext.ux.VrTabPanel.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/onlinereservation_form.js"></script>  
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/onlinereservation_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/onlinereservation_store.js"></script>

<script>
    startConnection();
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    DATE5YEAR = Date.parseDate('<?=date("Y-m-d H:i:s",strtotime("+5 years"))?>', 'Y-m-d H:i:s'); 
    STORE = '<?=STOREID;?>';
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
        closeSerialPort(COM_POSIFLEX);
        endConnection();
    }
    window.onbeforeunload = goodbye;
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        Ext.QuickTips.init();
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        var viewport = new Ext.Viewport({
                layout: 'fit',
                items: [
                    {
                        xtype: 'tabpanel',
                        tabBarPosition: 'top',
                        activeTab: 0,
                        frame: true,
                        resizeTabs: true,
                        enableTabScroll: true,
                        tabWidth: 200,
                        tabBar: {
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            defaults: {flex: 1}
                        },
                        items: [
                            new jun.OnlineReservationGrid({
                                title: 'Online Reservation'
                            })
                            /* new Ext.Panel({
                                title: 'K A S I R',
                                id: 'docs-jun.SalestransCounterPanelWin',
                                layout: 'border',
                                height: 260,
                                items: [
                                ],
                                
                            }), */

                        ]
                    }
                ]
            });
    });        
</script>
<div id="onlineView" style="height: 100%"></div>           