<iframe id="myFrame" name="myFrame" style="border:none"></iframe>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script>
    startConnection();
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu().app()->params['url_logo']; ?>" alt=""/>';
    SALES_TYPE = '<?=Users::is_audit();?>';
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    EDIT_TGL =  <?
    $user = Users::model()->findByPk(Yii::app()->user->getId());
    echo $user->is_available_role(246) ? 'true' : 'false';
    ?>;
    HEADOFFICE = <?if (defined('HEADOFFICE')) {
        echo HEADOFFICE ? 'true' : 'false';
    } else {
        echo 'false';
    }?>;
    SYSTEM_BANK_CASH = '<?=Bank::get_bank_cash_id();?>';
    SYSTEM_PETTY_CASH = '<?=Bank::get_petty_cash_id();?>';
    SYSTEM_BANK_CASH_WHILE = '<?=Bank::get_bank_cash_while_id();?>';
    PORT_CLOSED = true;
    //    Ext.chart.Chart.CHART_URL = '<?//=bu(); ?>///js/ext340/resources/charts.swf';
    VALID_CARD = <?=VALID_CARD;?>;
    INTERVALSYNC = <?=INTERVALSYNC;?>;
    ENABLESYNC = <?=ENABLESYNC ? 'true' : 'false';?>;
    STORE = '<?=STOREID;?>';
    //    if (!notReady()) {
    //        var fs = require('fs');
    //        var data = fs.readFileSync('./config.json'), myObj;
    //        try {
    //            myObj = JSON.parse(data);
    //            PRINTER_RECEIPT = myObj.PRINTER_RECEIPT;
    //            PRINTER_CARD = myObj.PRINTER_CARD;
    //            COM_POSIFLEX = myObj.COM_POSIFLEX;
    //        }
    //        catch (err) {
    //            console.log('There has been an error parsing your JSON.');
    //            console.log(err);
    //        }
    //    }
    SALES_OVERRIDE = '<?
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        echo Users::get_override($user->user_id, $user->password) ? 1 : 0;
        ?>';
    var PRINTER_RECEIPT = 'default';
    var PRINTER_STOCKER = 'default';
    var PRINTER_CARD = 'default';
    var COM_POSIFLEX = 'COM3';
    var LINE_SPACING = 50; // atau 25;
    var POSCUSTOMERREADONLY = false;
    var POSCUSTOMERDEFAULT = '';
    var local = localStorage.getItem("settingClient");
    if (local != null) {
        var data_ = JSON.parse(local);
        PRINTER_RECEIPT = JSON.search(data_, '//data[name_="PRINTER_RECEIPT"]/value_')[0];
        PRINTER_STOCKER = JSON.search(data_, '//data[name_="PRINTER_STOCKER"]/value_')[0];
        LINE_SPACING = JSON.search(data_, '//data[name_="LINE_SPACING"]/value_')[0];
        PRINTER_CARD = JSON.search(data_, '//data[name_="PRINTER_CARD"]/value_')[0];
        COM_POSIFLEX = JSON.search(data_, '//data[name_="COM_POSIFLEX"]/value_')[0];
        POSCUSTOMERREADONLY = JSON.search(data_, '//data[name_="POSCUSTOMERREADONLY"]/value_')[0];
        POSCUSTOMERDEFAULT = JSON.search(data_, '//data[name_="POSCUSTOMERDEFAULT"]/value_')[0];
    }
    var __cutPaper = [{type: 'raw', data: chr(27) + chr(105), options: {language: 'ESCP', dotDensity: 'double'}}];
    var __feedPaper = [{
        type: 'raw',
        data: chr(27) + chr(100) + chr(5),
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __openCashDrawer = [{
        type: 'raw',
        data: chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r",
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __printData = [
        {
            type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(LINE_SPACING) + //space vertical
        chr(27) + chr(77) + chr(49),// font B
            options: {language: 'ESCP', dotDensity: 'double'}
        }
    ];
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    //    if (!notReady()) {
    //        var gui = require('nw.gui');
    //        var win = gui.Window.get();
    //        win.maximize();
    //        win.on('new-win-policy', function (frame, url, policy) {
    //            policy.forceNewPopup();
    //        });
    //    }
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grupAttr_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grup_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/barang_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/bank_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beauty_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/card_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/dokter_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/store_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/ketTrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/paketTrans_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/paketTrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/quickSalestrans_form.js"></script>
<script>
    jun.ajaxCounter = 0;
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        var a = function () {
            Ext.get("loading").remove();
            Ext.fly("loading-mask").fadeOut({
                remove: !0
            });
        };
        Ext.QuickTips.init();
//    loadText = "Sedang proses... silahkan tunggu";
        Ext.Ajax.on("beforerequest", function (conn, opts) {
            if (opts.url == "site/Sync") return;
            jun.myMask.show();
            jun.ajaxCounter++;
        });
        Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
            if (opts.url == "site/Sync") return;
            if (jun.ajaxCounter > 1) {
                jun.ajaxCounter--;
            } else {
                jun.ajaxCounter = 0;
                jun.myMask.hide();
            }
        });
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            if (opts.url == "site/Sync") return;
            if (jun.ajaxCounter > 1) {
                jun.ajaxCounter--;
            } else {
                jun.ajaxCounter = 0;
                jun.myMask.hide();
            }
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        displayText("Welcome To Naavagreen", 0);
//        if (jun.rztBarangLib.getTotalCount() === 0) {
//            jun.rztBarangLib.load();
//        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
//        if (jun.rztBankLib.getTotalCount() === 0) {
//            jun.rztBankLib.load();
//        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
//        if (jun.rztBeautyLib.getTotalCount() === 0) {
//            jun.rztBeautyLib.load();
//        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
//        if (jun.rztCardLib.getTotalCount() === 0) {
//            jun.rztCardLib.load();
//        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
//        if (jun.rztStoreCmp.getTotalCount() === 0) {
//            jun.rztStoreCmp.load();
//        }
//        if (jun.rztGrupLib.getTotalCount() === 0) {
//            jun.rztGrupLib.load();
//        }
        //var form = new jun.SalestransWin({modez: 0});
        //form.show();
        var m = new jun.QuickSalesMenu({state:<?php echo $state ?>, username: "<?php echo $username ?>"});
        m.show();
        $(function () {
            'use strict';
            var arrtSetting = function (rowId, val, rawObject, cm) {
                var attr = rawObject.attr[cm.name], result;
                if (attr.rowspan) {
                    result = ' rowspan=' + '"' + attr.rowspan + '"';
                } else if (attr.display) {
                    result = ' style="display:' + attr.display + '"';
                }
                return result;
            }
            $("#GridHistory").jqGrid({
                url: 'http://localhost:81/posng/customers/gethistory/nobase/7559KDR01',
                postData: {
                    nobase: 1000
                },
                mtype: 'POST',
                datatype: "json",
                colNames: ['Faktur', 'Tgl', 'Kode Barang', 'Qty', 'Unit', 'Cabang'],
                colModel: [
                    {name: 'no_faktur', align: 'center', cellattr: arrtSetting},
                    {name: 'tgl'},
                    {name: 'kd_brng', width: 75},
                    {name: 'qty', align: 'right', width: 50},
                    {name: 'satuan'},
                    {name: 'nama'}
                ],
                cmTemplate: {sortable: false},
                rowNum: 100,
                //rowList: [5, 10, 20],
                //pager: '#pager',
                gridview: true,
                hoverrows: false,
                autoencode: true,
                ignoreCase: true,
                viewrecords: true,
                height: '100%',
                width: '100%',
                beforeSelectRow: function () {
                    return false;
                }
            });
            $("#GridHistory").jqGrid('clearGridData');
            $("#GridHistory").jqGrid('setGridParam', {postData: {nobase: '7559KDR01'}});
            $("#GridHistory").trigger('reloadGrid');
        });
    });
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>
<script type="text/javascript"></script>
<div id="SalestransView" style="height: 100%"></div>