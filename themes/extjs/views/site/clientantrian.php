<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jplayer.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jplayer.playlist.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/0.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/locstor.min.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/konsul_form.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/konsul_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/konsul_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/aishaAntrian_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/antrian_form.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/antrian_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/antrian_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/barang_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/diagnosa_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/diagnosa_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/customers_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/salestransDetails_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/salestransDetails_store.js"></script>-->
<script>
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu().app()->params['url_logo']; ?>" alt=""/>';
    SALES_OVERRIDE = false;
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        // if (!LOGOUT) {
        //     if (!e) e = window.event;
        //     e.cancelBubble = true;
        //     e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
        //     if (e.stopPropagation) {
        //         e.stopPropagation();
        //         e.preventDefault();
        //     }
        // }
    }
    window.onbeforeunload = goodbye;
    jun.ajaxCounter = 0;
    jun.is_nwjs = function is_enable_tools() {
        try {
            var gui = require('nw.gui');
            if (gui != null) {
                var win = gui.Window.get();
                win.maximize();
                // win.showDevTools();
                return true;
            }
        } catch (err) {
            console.log(err.message);
            return false;
        }
    };
    jun.Counter = 'A';
    jun.runner = new Ext.util.TaskRunner();
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        Ext.QuickTips.init();
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
//        if (jun.is_nwjs()) {
//            var fs = require('fs');
//            var obj = JSON.parse(fs.readFileSync('config.json', 'utf8'));
//            jun.Counter = obj.COUNTER;
//        }
//        jun.reloadAllStoreDokter = function () {
//            jun.rztAntrianDokter.reload();
//            jun.rztAntrianDokterPending.reload();
//        };
//        jun.runner.start({
//            run: jun.reloadAllStoreDokter,
//            interval: 5 * 1000
//        });
        var viewport = new Ext.Viewport({
            layout: 'border',

            items: [
                {
                    xtype: "box",
                    region: "north",
                    id: "app-header",
                    html: '<p align="center";">'+ SYSTEM_LOGO + '</p>',
                    height: 80
                },
                {
                    region: 'center',
                    style: 'margin-right:10px',
                    frame: true,

                    layout: {
                        type: 'vbox',
                        padding: '2',
                        align: 'center',
                        width: 1300
                    },
                    items: [
                        {

                          xtype: 'container',
                          height: 200,
                          width: 1300,
                          html:' <style>\n' +
                              '.btn {\n' +
                              '    border: none;\n' +
                              '    background-color: inherit;\n' +
                              '    font-size: 30px;\n' +
                              '    cursor: pointer;\n' +
                              '    display: inline-block;\n' +
                              '    width: 100%; \n' +
                              '    height: 100%; \n' +
                              '}\n' +
                              '\n' +
                              '/* Green */\n' +
                              '.x {\n' +
                              '    color: green;\n' +
                              '}\n' +
                              '\n' +
                              '.x:hover {\n' +
                              '    background-color: #4CAF50;\n' +
                              '    color: white;\n' +
                              '}\n' +
                              '\n' +
                              '.x:active {\n' +
                              '    background-color: #4CAF50;\n' +
                              '    color: white;\n' +
                              '}\n' +
                              '\n' +

                              '</style>' +
                              '<button class="btn x" onclick="getantrian(1)">PEMBELIAN LANGSUNG</button>'
                        },
                        {
                            xtype: 'label',
                            text: '_____________________________________________________',
                            hidden: false,
                            width: 300,
                            height: 30
                        },
                        {
                            xtype: 'label',
                            text: '_____________________________________________________',
                            hidden: false,
                            width: 300,
                            height: 30
                        },
                        {

                            xtype: 'container',
                            height: 200,
                            width: 1300,
                            html:'<button class="btn x" onclick="getantrian(2)">KONSULTASI</button>'
                        }


                        // {
                        //     region: 'center',
                        //     html: 'east',
                        //     frame: true,
                        //     width: 860,
                        //     height: 540,
                        //     style: 'margin:2px',
                        //     id: 'jquery_jplayer_1'
                        // }
                    ]
                }
                // {
                //     region: "south",
                //     style: 'margin-top:2px;font-size: 3em;color: limegreen;',
                //     html: '<marquee behavior="scroll" direction="left" style="font-size: 3em;">Scrolling text...</marquee>',
                //     //height: 50
                // }
            ]
        });
    });
    // this.btna1.on('click', this.onbtnA1click, this);
    // this.btna2.on('click', this.onbtnA2click, this);


</script>
<script>
    function getantrian(link) {
        if (link==1){x='E';} else{x='Z'}
        Ext.Ajax.request({
            url: 'clientantrian/'+x,
            method: 'POST',
            scope: this,

            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                print(PRINTER_RECEIPT, printData);

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
</script>
<div id="dokterView" style="height: 100%"></div>