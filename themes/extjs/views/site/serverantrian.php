<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jplayer.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jplayer.playlist.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/0.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/locstor.min.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/konsul_form.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/konsul_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/konsul_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/aishaAntrian_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/antrian_form.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/antrian_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/antrian_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/barang_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/diagnosa_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/diagnosa_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/customers_store.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/salestransDetails_grid.js"></script>-->
<!--<script type="text/javascript"-->
<!--        src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/view/salestransDetails_store.js"></script>-->
<script>
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu().app()->params['url_logo']; ?>" alt=""/>';
    SALES_OVERRIDE = false;
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    jun.ajaxCounter = 0;
    jun.is_nwjs = function is_enable_tools() {
        try {
            var gui = require('nw.gui');
            if (gui != null) {
                var win = gui.Window.get();
                win.maximize();
                // win.showDevTools();
                return true;
            }
        } catch (err) {
            console.log(err.message);
            return false;
        }
    };
    jun.Counter = 'A';
    jun.runner = new Ext.util.TaskRunner();
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        Ext.QuickTips.init();
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
//        if (jun.is_nwjs()) {
//            var fs = require('fs');
//            var obj = JSON.parse(fs.readFileSync('config.json', 'utf8'));
//            jun.Counter = obj.COUNTER;
//        }
//        jun.reloadAllStoreDokter = function () {
//            jun.rztAntrianDokter.reload();
//            jun.rztAntrianDokterPending.reload();
//        };
//        jun.runner.start({
//            run: jun.reloadAllStoreDokter,
//            interval: 5 * 1000
//        });
        var viewport = new Ext.Viewport({
            layout: 'border',
            listeners: {
                afterrender: function () {
                    $("#jquery_jplayer_1").jPlayer({
                        ready: function () {
                            $(this).jPlayer("setMedia", {
                                title: "Big Buck Bunny",
                                m4v: "<?php echo Yii::app()->request->baseUrl; ?>/video/aimer.mkv"
                                // ,
                               // poster: "http://www.jplayer.org/video/poster/Big_Buck_Bunny_Trailer_480x270.png"
                            }).jPlayer("play");
                        },
                        swfPath: "../js",
                        supplied: "m4v",
                        size: {
                            width: "860px",
                            height: "540px",
                            cssClass: "jp-video-360p"
                        },
                        loop: true,
                        muted: true,
                        useStateClassSkin: true,
                        autoBlur: false,
                        smoothPlayBar: true,
                        keyEnabled: true,
                        remainingDuration: true,
                        toggleDuration: true
                    });
                }
            },
            items: [
                {
                    xtype: "box",
                    region: "north",
                    id: "app-header",
                    html: '<p align="center";">'+ SYSTEM_LOGO + '</p>',
                    height: 60
                },
                {
                    region: 'center',
                    style: 'margin-right:2px',
                    frame: true,
                    layout: {
                        type: 'hbox',
                        padding: '2',
                        align: 'stretch'
                    },
                    items: [
                        {
                            region: 'west',
                            style: 'margin-right:2px',
                            frame: true,
                            layout: {
                                type: 'vbox',
                                padding: '2',
                                align: 'stretch'
                            },
                            flex: 3,
                            items: [
                                {
                                    html: '10',
                                    title: 'COUNTER A',
                                    flex: 3
                                },
                                {
                                    html: '10',
                                    title: 'COUNTER B',
                                    flex: 3
                                },
                                {
                                    html: '10',
                                    title: 'COUNTER C',
                                    flex: 3
                                },
                                {
                                    html: '10',
                                    title: 'COUNTER D',
                                    flex: 3
                                },
                            ]
                        },
                        {
                            region: 'center',
                            html: 'east',
                            frame: true,
                            width: 860,
                            height: 540,
                            style: 'margin:2px',
                            id: 'jquery_jplayer_1'
                        },
                        {
                            region: 'east',
                            style: 'margin-right:2px',
                            frame: true,
                            layout: {
                                type: 'vbox',
                                padding: '2',
                                align: 'stretch'
                            },
                            flex: 3,
                            items: [
                                {
                                    html: '10',
                                    title: 'COUNTER E',
                                    flex: 3
                                },
                                {
                                    html: '10',
                                    title: 'KONSUL 1',
                                    flex: 3
                                },
                                {
                                    html: '10',
                                    title: 'KONSUL 2',
                                    flex: 3
                                },
                                {
                                    html: '10',
                                    title: 'PERAWATAN',
                                    flex: 3
                                },
                            ]
                        }
                    ]
                },
                {
                    region: "south",
                    style: 'margin-top:2px;font-size: 3em;color: limegreen;',
                    html: '<marquee behavior="scroll" direction="left" style="font-size: 3em;">Scrolling text...</marquee>',
                    //height: 50
                }
            ]
        });
    });
</script>
<div id="dokterView" style="height: 100%"></div>