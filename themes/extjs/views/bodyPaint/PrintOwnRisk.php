<? /** @var BodyPaint $model */
/** @var Bank $bank */
$kasir   = Users::model()->findByPk( Yii::app()->user->getId() )->name;
$bank    = Bank::model()->findByPk( $model->b1_bank_id );
$payment = 'TUNAI';
if ( $bank->accountCode->isBank() ) {
	$payment = "TRANSFER";
}
?>
<section class="sheet padding-10mm">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" style="width:45px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" style="margin-left: 410px;width: 255px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="width: 715px">
    <p class="15" style="text-align: center;" align="center"><strong><u><span style="font-family: Tahoma; font-weight: bold; text-decoration: underline; font-size: 22.0000pt;">BUKTI PELUNASAN</span></u></strong>
    </p>
    <table style="border-collapse: collapse; width: 979px; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
        <tbody>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">NOMOR</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p><span style="font-family: Tahoma; font-size: 11.0000pt;"><?= $model->inv_no; ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">TANGGAL</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;"><?= sql2date( $model->b1_tgl ) ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">TERIMA DARI</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span
                            style="font-family: Tahoma; font-size: 11.0000pt;"><?= strtoupper( $model->asuransi->nama ) . " QQ " . strtoupper( $model->mobil->customer->nama_customer ) ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">JUMLAH UANG</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">RP <?= number_format( abs( $model->b1_total ), 2, ',', '.' ) ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;"># <?= strtoupper( terbilang( abs( $model->b1_total ) ) ); ?> RUPIAH #</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 163.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">JENIS PENERIMAAN</span></p>
            </td>
            <td style="width: 24.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="width: 747.625px;  border: none;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 11.0000pt;">PENERIMAAN FAKTUR SERVICE BODY REPAIR</span></p>
            </td>
        </tr>
        </tbody>
    </table>
    <table style="border-collapse: collapse; width: 717px; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
        <tbody>
        <tr>
            <td style="width: 25px;  border-left: none; border-right: none; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">NO.</span></strong></p>
            </td>
            <td style="width: 75px;  border-left: none; border-right: none; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">PERKIRAAN</span></strong></p>
            </td>
            <td style="width: 75px;  border-left: none; border-right: none; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">NO. FAKTUR</span></strong></p>
            </td>
            <td style="width: 75px;  border-left: none; border-right: none; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">NAMA CUSTOMER</span></strong></p>
            </td>
            <td style="width: 75px;  border-left: none; border-right: none; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">NO. POLISI</span></strong></p>
            </td>
            <td style="width: 75px;  border-left: none; border-right: none; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: right;" align="right"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">JUMLAH (RP)</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;">1.</span></p>
            </td>
            <td style="border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= $bank->account_code; ?></span></p>
            </td>
            <td style="border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= $model->inv_no; ?></span></p>
            </td>
            <td style="border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= strtoupper( $model->mobil->customer->nama_customer ) ?></span></p>
            </td>
            <td style=" border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= strtoupper( $model->mobil->no_pol ) ?></span></p>
            </td>
            <td style="border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: right;" align="right"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= number_format( abs( $model->b1_total ), 0, ',', '.' ) ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" colspan="2" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= $payment ?></span></p>
            </td>
            <td style=" border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= ( $payment == "TRANSFER" ) ? "BANK : " : "" ?> <?= $bank->nama_bank; ?></span></p>
            </td>
            <td style=" border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;">TGL. <?= sql2date( $model->b1_tgl ) ?></span></p>
            </td>
            <td style="  border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15"><span style="font-family: Tahoma; font-size: 9.0000pt;">TOTAL</span></p>
            </td>
            <td style="  border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: right;" align="right"><span style="font-family: Tahoma; font-size: 9.0000pt;"><?= number_format( abs( $model->b1_total ), 0, ',', '.' ) ?></span></p>
            </td>
        </tr>
        </tbody>
    </table>
    <table style="border-collapse: collapse; width: 647px; border: none; font-family: 'Tahoma'; font-size: 10pt; height: 107px;" border="0" cellspacing="0">
        <tbody>
        <tr style="">
            <td style="width: 201.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">KASIR</span></strong></p>
            </td>
            <td style="width: 203.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;"></span></strong></p>
            </td>
            <td style="width: 198.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">KONSUMEN</span></strong></p>
            </td>
        </tr>
        <tr style="">
            <td style="width: 201.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
            </td>
            <td style="width: 203.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
            </td>
            <td style="width: 198.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
            </td>
        </tr>
        <tr style="">
            <td style="width: 201.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">(<?= $kasir; ?>)</span></strong></p>
            </td>
            <td style="width: 203.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;"></span></strong></p>
            </td>
            <td style="width: 198.625px; padding: 0pt 5.4pt; border: none; " valign="top">
                <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Tahoma; font-weight: bold; font-size: 9.0000pt;">(<?=strtoupper( $model->mobil->customer->nama_customer )?>)</span></strong>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
</section>
