<? /** @var BodyPaint $model */
/** @var Bank $bank */
?>
<section class="sheet padding-10mm">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" style="width:45px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" style="margin-left: 410px;width: 255px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="width: 715px">
    <p class="15" style="text-align: center;" align="center"><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 22.0000pt;">KWITANSI</span></u></strong>
    </p>

    <table style="border-collapse: collapse; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
        <tbody>
        <tr>
            <td style="width:125px;border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NOMOR</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $model->inv_no; ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NAMA</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper($model->mobil->customer->nama_customer); ?></span></p>
            </td>
        </tr>
        <tr>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">TERBILANG</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"># <?= strtoupper( terbilang( abs( $model->grand_total ) ) ); ?> RUPIAH #</span></p>
            </td>
        </tr>
        <tr>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">GUNA MEMBAYAR</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">TITIPAN PEMBAYARAN SERVICE BODY REPAIR (OWN RISK)</span></p>
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
            <td style="border: none;" valign="top">
                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
            </td>
            <td style="border: none;" valign="top">
                <table style="border-collapse: collapse; margin-left: 5.4pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td style="  border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">NO</span></p>
                        </td>
                        <td style=" width: 200px; border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">NO. POLISI</span></p>
                        </td>
                        <td style="width: 200px;  border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">NO. WO</span></p>
                        </td>
                        <td style=" width: 200px; border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">TGL. WO</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">1.</span></p>
                        </td>
                        <td style="  border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;"><?= $model->mobil->no_pol; ?></span></p>
                        </td>
                        <td style="  border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;"><?= $model->wo_no; ?></span></p>
                        </td>
                        <td style="  border: none;" valign="top">
                            <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;"><?= sql2date($model->wo_tgl); ?></span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>


            </td>
        </tr>
        </tbody>
    </table>
    <p style="padding: 5px"></p>
    <table style="border-collapse: collapse; width: 584.85pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
        <tbody>
        <tr style="height: 26.9000pt;">
            <td style="width: 362.1000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;" valign="top">
                <table style="border-collapse: collapse; width: 129.55pt; margin-left: 7.55pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
                    <tbody>
                    <tr style="height: 22.1000pt;">
                        <td style="width: 129.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 2.2500pt solid windowtext;" valign="center">
                            <p class="15" style="text-align: center;" align="center"><strong><span style="font-family: Calibri; color: #000000; font-weight: bold; font-size: 11.0000pt;">RP <?= number_format( $model->grand_total, 0, ',', '.' ) ?></span></strong></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">&nbsp;</span></strong></p>
            </td>
            <td style="width: 222.7500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">TEGAL, <?=strtoupper(strftime("%d %B %Y"))?></span></p>
            </td>
        </tr>
        </tbody>
    </table>
</section>
