<?
/** @var BodyPaint $model */
$nama     = $alamat = $city = '';
$customer = $model->mobil->customer;
if ( $model->biaya == 'PRIBADI' ) {
	$nama   = $customer->nama_customer;
	$alamat = $customer->alamat;
	$city   = $customer->city;
} else {
	/** @var AsuransiBp $asurasi */
	$asurasi = AsuransiBp::model()->findByPk( $model->asuransi_bp_id );
	$nama    = $asurasi->nama;
	$alamat  = $asurasi->address;
	$city    = $asurasi->city;
}
$sm = SysPrefs::get_val( 'BPSM' );
?>
<div class="book">
    <div class="page">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" width=85>
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" width=335 style="margin-left: 290">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="margin-bottom: 15" width="715">
        <p style="text-align: center;"><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 11.0000pt;">ESTIMASI BODY REPAIR</span></u></strong>
        </p>
        <p style="text-align: center;"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO : <?= $model->est_no ?></span></strong></p>
        <br>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">Kepada Yth,</span></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $nama ) ?></span></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $alamat ) ?></span></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $city ) ?></span></p>
        <br>
        <table style="border-collapse: collapse;  border: none; width: 715">
            <tbody>
            <tr>
                <td style="border: none;">
                    <span style="font-family: Calibri; font-size: 11.0000pt;">MERK/ TAHUN</span>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p>
                        <span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->mobilTipe->merk->nama ) ?> <?= strtoupper( $model->mobil->mobilTipe->nama ) ?> / <?= strtoupper( $model->mobil->tahun_pembuatan ) ?></span>
                    </p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. POLIS</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->no_polis ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. POLISI</span></p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_pol ) ?></span></p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. RANGKA</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_rangka ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">PEMILIK/ PHONE</span></p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->customer->nama_customer ) ?> / <?= strtoupper( $model->mobil->customer->telp ) ?></span>
                    </p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. MESIN</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_mesin ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">WARNA</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style=" border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->warna ) ?></span></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">JASA PERBAIKAN</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: 1; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 28.1000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO</span></strong></p>
                </td>
                <td style="width: 368.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">PERINCIAN</span></strong></p>
                </td>
                <td style="width: 70.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">STATUS</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">HARGA</span></strong></p>
                </td>
            </tr>
			<? $i = 1;
			foreach ( $model->bpJasas as $j ) : ?>
                <tr>
                    <td style="width: 28.1000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $i;
								$i ++; ?>.</span></p>
                    </td>
                    <td style="width: 368.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $j->jasaBp->nama . " " . $j->note  ) ?></span></p>
                    </td>
                    <td style="width: 70.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $j->tipe ) ?></span></p>
                    </td>
                    <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $j->harga, 0, ',', '.' ) ?></span></p>
                    </td>
                </tr>
			<? endforeach; ?>
            <tr>
                <td style="width: 467.5000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;"
                    colspan="3">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL JASA</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->total_jasa_line, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">SPAREPART</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: 1; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 28.4500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO</span></strong></p>
                </td>
                <td style="width: 108.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">KODE PARTS</span></strong></p>
                </td>
                <td style="width: 208.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NAMA PARTS</span></strong></p>
                </td>
                <td style="width: 35.2500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">QTY</span></strong></p>
                </td>
                <td style="width: 50.4000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">HARGA</span></strong></p>
                </td>
                <td style="width: 50.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">JUMLAH</span></strong></p>
                </td>
            </tr>
			<? $i = 1;
			foreach ( $model->bpParts as $p ) : ?>
                <tr>
                    <td style="width: 28.4500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $i;
								$i ++; ?>.</span></p>
                    </td>
                    <td style="width: 108.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $p->kode ) ?></span></p>
                    </td>
                    <td style="width: 208.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $p->nama ) ?></span></p>
                    </td>
                    <td style="width: 35.2500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->qty, 0, ',', '.' ) ?></span></p>
                    </td>
                    <td style="width: 50.4000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->harga, 0, ',', '.' ) ?></span></p>
                    </td>
                    <td style="width: 50.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->total_parts_line, 0, ',', '.' ) ?></span></p>
                    </td>
                </tr>
			<? endforeach; ?>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;"
                    colspan="4">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL SPAREPARTS</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->total_parts, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <table style="border-collapse: collapse; width: 715; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->total_jasa_line + $model->total_parts, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">PPN</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( ($model->total_jasa_line + $model->total_parts) * floatval(yiiparam('ppn',10)/100), 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">GRAND TOTAL</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( ($model->total_jasa_line + $model->total_parts + (($model->total_jasa_line + $model->total_parts) * floatval(yiiparam('ppn',10)/100))), 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <table style="border-collapse: collapse; width: 715; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 191.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">TEGAL, <?= sql2date( get_date_today(), 'dd MMMM yyyy' ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 191.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 11.0000pt;">Disetujui oleh,</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 11.0000pt;">Diketahui oleh,</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 11.0000pt;">Dibuat oleh,</span></p>
                </td>
            </tr>
            <tr style="height: 36.4000pt;">
                <td style="width: 191.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 191.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 11.0000pt;">(<?= strtoupper( $nama ) ?>)</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 11.0000pt;">(<?= strtoupper( $sm ) ?>)</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 11.0000pt;">(<?= strtoupper( $model->nama_sa ) ?>)</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 191.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Penanggung Jawab</span></strong></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Service Manager</span></strong></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">SA</span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 9.0000pt;">PERHATIAN :</span></u></strong></p>
        <ul style="margin-top: 0px; margin-bottom: 0px;">
            <li style="text-indent: 10px; margin-left: 34px;"><span style="font-family: Calibri; font-size: 9.0000pt;">Perkiraan tersebut diatas berdasarkan apa yang dapat diketahui sementara, dan dapat berubah sesuai keadaan yang sebenarnya </span>
                <span style="font-family: Calibri; font-size: 9.0000pt;margin-left: 12">pada saat pelaksanaan kerja perbaikan.</span></li>
            <li style="text-indent: 10px; margin-left: 34px;"><span style="font-family: Calibri; font-size: 9.0000pt;">Perbaikan baru dapat dilaksanakan jika sudah ada persetujuan harga reparasi order (RO).</span>
            </li>
            <li style="text-indent: 10px; margin-left: 34px;"><span style="font-family: Calibri; font-size: 9.0000pt;">Mohon segera ditertibkan </span><u><span
                            style="font-family: Calibri; text-decoration: underline; font-size: 9.0000pt;">Surat Perintah Kerja</span></u><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;untuk kami.</span>
            </li>
        </ul>
    </div>
</div>