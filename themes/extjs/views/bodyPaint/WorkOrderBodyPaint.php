<?
/** @var BodyPaint $model */
$nama     = $alamat = $city = $tlp = $nama_asuransi ='';
$customer = $model->mobil->customer;
$nama   = $customer->nama_customer;
$alamat = $customer->alamat;
$city   = $customer->city;
$telp   = $customer->telp;
if($model->asuransi_bp_id != ''){
    $asuransi = AsuransiBp::model()->findByPk($model->asuransi_bp_id);
    if($asuransi != null){
        $nama_asuransi = $asuransi->nama;
    }
}
?>
<div class="book">
    <div class="page">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" width=85>
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" width=335 style="margin-left: 290">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="margin-bottom: 15" width="715">
        <p style="text-align: center;"><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 11.0000pt;">WORK ORDER (WO)</span></u></strong>
        </p>
        <p style="text-align: center;"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO : <?= $model->wo_no ?></span></strong></p>
        <br>
        <table style="border-collapse: collapse; width: 715; border: none; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">NAMA PEMILIK</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 457.4500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $nama ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">ALAMAT</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 457.4500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $alamat ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">NO. TELEPON</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 457.4500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $telp ) ?></span></p>
                </td>
            </tr>
            </tbody>
        </table>
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="margin: 5 0" width="715">
        <table style="border-collapse: collapse; width: 566.75pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">NO. POLISI</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.9500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_pol ) ?></span></p>
                </td>
                <td style="width: 95.6000pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">TGL. MASUK</span></p>
                </td>
                <td style="width: 14.3500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= sql2date( $model->est_start, 'dd MMMM yyyy' ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">NO. RANGKA</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.9500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_rangka ) ?></span></p>
                </td>
                <td style="width: 95.6000pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">KM</span></p>
                </td>
                <td style="width: 14.3500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->tahun_km )?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">NO. MESIN</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.9500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_mesin ) ?></span></p>
                </td>
                <td style="width: 95.6000pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">ASURANSI</span></p>
                </td>
                <td style="width: 14.3500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?=strtoupper($nama_asuransi)?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">MERK/ TAHUN</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.9500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->mobilTipe->merk->nama ." ".$model->mobil->mobilTipe->nama  ) ?> / <?= strtoupper( $model->mobil->tahun_pembuatan ) ?></span></p>
                </td>
                <td style="width: 95.6000pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">RENCANA SELESAI</span></p>
                </td>
                <td style="width: 14.3500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= sql2date( $model->est_end, 'dd MMMM yyyy' ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 95.5500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">WARNA</span></p>
                </td>
                <td style="width: 13.7500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 173.9500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->warna ) ?></span></p>
                </td>
                <td style="width: 95.6000pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 14.3500pt; border: none;" >
                    <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 173.5500pt; border: none;" >
                    <p >&nbsp;</p>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <p><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">JASA PERBAIKAN</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: 1; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 28.1000pt; border: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO</span></strong></p>
                </td>
                <td style="width: 368.5500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">PERINCIAN</span></strong></p>
                </td>
                <td style="width: 70.8500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">STATUS</span></strong></p>
                </td>
            </tr>
			<? $i = 1;
			foreach ( $model->bpJasas as $j ) : ?>
                <tr>
                    <td style="width: 28.1000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $i;
								$i ++; ?>.</span></p>
                    </td>
                    <td style="width: 368.5500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $j->jasaBp->nama." ".$j->note  ) ?></span></p>
                    </td>
                    <td style="width: 70.8500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $j->tipe ) ?></span></p>
                    </td>
                </tr>
			<? endforeach; ?>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">SPAREPART</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: 1; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 28.4500pt; border: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO</span></strong></p>
                </td>
                <td style="width: 208.8500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NAMA PARTS</span></strong></p>
                </td>
                <td style="width: 35.2500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">QTY</span></strong></p>
                </td>
            </tr>
			<? $i = 1;
			foreach ( $model->bpParts as $p ) : ?>
                <tr>
                    <td style="width: 28.4500pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $i;
								$i ++; ?>.</span></p>
                    </td>
                    <td style="width: 208.8500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $p->nama ) ?></span></p>
                    </td>
                    <td style="width: 35.2500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->qty, 0, ',', '.' ) ?></span></p>
                    </td>
                </tr>
			<? endforeach; ?>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">SERVICE ADVISOR</span> <span style="font-family: Calibri; font-size: 11.0000pt;">: <?= strtoupper( $model->nama_sa ) ?></span></p>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <div align="center">
            <table style="border-collapse: collapse; width: 715; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
                <tbody>
                <tr>
                    <td style="width: 63.7000pt; border: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Bongkar</span></strong></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Las Ketok</span></strong></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Dempul</span></strong></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Cat</span></strong></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Masking</span></strong></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Sanding</span></strong></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Pasang</span></strong></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Poles</span></strong></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 10.0000pt;">Finishing</span></strong></p>
                    </td>
                </tr>
                <tr style="height: 36.5500pt;">
                    <td style="width: 63.7000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 63.7000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p  style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                    <td style="width: 63.7500pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" >
                        <p ><span style="font-family: Calibri; font-size: 10.0000pt;">&nbsp;</span></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p ><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">Keterangan :</span></strong></p>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p ><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p ><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">Pekerjaan Di Luar WO :</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
            <tbody>
            <tr style="height: 97.2000pt;">
                <td style="width: 715; border: 1.0000pt solid windowtext;" >
                    <p >&nbsp;</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>