<?
/** @var BodyPaint $model */
$nama     = $alamat = $city = $tlp = $nama_asuransi = $user = '';
$customer = $model->mobil->customer;
$nama     = $customer->nama_customer;
$alamat   = $customer->alamat;
$city     = $customer->city;
$telp     = $customer->telp;
if ( $model->asuransi_bp_id != '' ) {
	$asuransi = AsuransiBp::model()->findByPk( $model->asuransi_bp_id );
	if ( $asuransi != null ) {
		$nama_asuransi = $asuransi->nama;
	}
}
/** @var Users $userfind */
$userfind = Users::model()->findByPk( Yii::app()->user->getId() );
if ( $userfind != null ) {
	$user = $userfind->name;
}
?>
<div class="book">
    <div class="page">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" width=85>
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" width=335 style="margin-left: 290">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="margin-bottom: 15" width="715">
        <p style="text-align: center;"><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 11.0000pt;">I N V O I C E</span></u></strong>
        </p>
        <p style="text-align: center;"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO : <?= $model->inv_no ?></span></strong></p>
        <br>
        <table style="border-collapse: collapse; width: 715px; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 112.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. POLISI</span></p>
                </td>
                <td style="width: 26.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 214.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_pol ) ?></span></p>
                </td>
                <td style="width: 118.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NAMA PEMILIK</span></p>
                </td>
                <td style="width: 27.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 386.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $nama ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 112.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. RANGKA</span></p>
                </td>
                <td style="width: 26.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 214.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_rangka ) ?></span></p>
                </td>
                <td style="width: 118.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">ALAMAT</span></p>
                </td>
                <td style="width: 27.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 386.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $alamat ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 112.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">NO. MESIN</span></p>
                </td>
                <td style="width: 26.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 214.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->no_mesin ) ?></span></p>
                </td>
                <td style="width: 118.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 27.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 386.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $city ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 112.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">MERK/ TAHUN</span></p>
                </td>
                <td style="width: 26.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 214.625px;  border: none;">
                    <p>
                        <span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->mobilTipe->merk->nama ) ?> <?= strtoupper( $model->mobil->mobilTipe->nama ) ?> / <?= strtoupper( $model->mobil->tahun_pembuatan ) ?></span>
                    </p>
                </td>
                <td style="width: 118.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 27.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 386.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 112.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">WARNA</span></p>
                </td>
                <td style="width: 26.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 214.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->mobil->warna ) ?></span></p>
                </td>
                <td style="width: 118.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">ASURANSI</span></p>
                </td>
                <td style="width: 27.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 386.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $nama_asuransi ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 112.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">KM</span></p>
                </td>
                <td style="width: 26.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 214.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->tahun_km ) ?></span></p>
                </td>
                <td style="width: 118.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 27.625px;  border: none;">
                    <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
                </td>
                <td style="width: 386.625px;  border: none;">
                    <p>&nbsp;</p>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <p><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">JASA DAN BAHAN PERBAIKAN</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: 1; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 28.1000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO</span></strong></p>
                </td>
                <td style="width: 368.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">PERINCIAN</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">HARGA</span></strong></p>
                </td>
                <td style="width: 70.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">DISC(%)</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">HARGA</span></strong></p>
                </td>
            </tr>
			<? $i = 1;
			foreach ( $model->bpJasas as $j ) : ?>
                <tr>
                    <td style="width: 28.1000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $i;
								$i ++; ?>.</span></p>
                    </td>
                    <td style="width: 368.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $j->jasaBp->nama ." ".$j->note ) ?></span></p>
                    </td>
                    <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $j->harga, 0, ',', '.' ) ?></span></p>
                    </td>
                    <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $j->disc, 0, ',', '.' ) ?></span></p>
                    </td>
                    <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $j->dpp, 0, ',', '.' ) ?></span></p>
                    </td>
                </tr>
			<? endforeach; ?>
            <tr>
                <td style="width: 467.5000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;"
                    colspan="4">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL JASA DAN BAHAN</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->total_jasa, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">SPAREPART</span></strong></p>
        <table style="border-collapse: collapse; width: 715; border: 1; font-family: 'Times New Roman'; font-size: 10pt;">
            <tbody>
            <tr>
                <td style="width: 28.4500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NO</span></strong></p>
                </td>
                <td style="width: 100.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">KODE PARTS</span></strong></p>
                </td>
                <td style="width: 208.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">NAMA PARTS</span></strong></p>
                </td>
                <td style="width: 35.2500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">QTY</span></strong></p>
                </td>
                <td style="width: 50.4000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">HARGA</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">JUMLAH</span></strong></p>
                </td>
            </tr>
			<? $i = 1;
			foreach ( $model->bpParts as $p ) : ?>
                <tr>
                    <td style="width: 28.4500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $i;
								$i ++; ?>.</span></p>
                    </td>
                    <td style="width: 100.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $p->kode ) ?></span></p>
                    </td>
                    <td style="width: 208.8500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $p->nama ) ?></span></p>
                    </td>
                    <td style="width: 35.2500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->qty, 0, ',', '.' ) ?></span></p>
                    </td>
                    <td style="width: 99.4000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->harga, 0, ',', '.' ) ?></span></p>
                    </td>
                    <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                        <p style="text-align: right;" align="right"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= number_format( $p->total_parts_line, 0, ',', '.' ) ?></span></p>
                    </td>
                </tr>
			<? endforeach; ?>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;"
                    colspan="4">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL SPAREPARTS</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->total_parts, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <table style="border-collapse: collapse; width: 715; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->total, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">PPN</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->vatrp, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            <tr>
                <td style="width: 467.5500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">GRAND TOTAL</span></strong></p>
                </td>
                <td style="width: 92.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: none;">
                    <p style="text-align: right;" align="right"><strong><span
                                    style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;"><?= number_format( $model->grand_total, 0, ',', '.' ) ?></span></strong></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><strong><u><span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 9.0000pt;">Saran :</span></u></strong></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <p><span style="font-family: Calibri; font-size: 11.0000pt;">&nbsp;</span></p>
        <table style="border-collapse: collapse; width: 254.75pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 49.4000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border: 1.0000pt solid windowtext;">
                    <p><span style="font-family: Calibri; font-size: 9.0000pt;">SA</span></p>
                </td>
                <td style="width: 14.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p><span style="font-family: Calibri; font-size: 9.0000pt;">:</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;">
                    <p><span style="font-family: Calibri; font-size: 9.0000pt;"><?= strtoupper( $model->nama_sa ) ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 49.4000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p><span style="font-family: Calibri; font-size: 9.0000pt;">NO. WO</span></p>
                </td>
                <td style="width: 14.1500pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p><span style="font-family: Calibri; font-size: 9.0000pt;">:</span></p>
                </td>
                <td style="width: 191.2000pt; padding: 0.0000pt 5.4000pt 0.0000pt 5.4000pt; border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;">
                    <p><span style="font-family: Calibri; font-size: 9.0000pt;"><?= strtoupper( $model->wo_no ) ?></span></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
        <p style="margin-left: 368.5500pt; text-align: center;" align="center"><span
                    style="font-family: Calibri; font-size: 9.0000pt;">TEGAL, <?= sql2date( $model->inv_tgl, 'dd MMMM yyyy' ) ?></span></p>
        <p style="margin-left: 368.5500pt; text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
        <p style="margin-left: 368.5500pt; text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
        <p style="margin-left: 368.5500pt; text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
        <p style="margin-left: 368.5500pt; text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
        <p style="margin-left: 368.5500pt; text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">(<?= strtoupper( $user ); ?>)</span></p>
    </div>
</div>