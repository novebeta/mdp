<? /** @var BodyPaint $model */
$kasir   = Users::model()->findByPk( Yii::app()->user->getId() )->name;
?>
<section class="sheet padding-10mm">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" style="width:45px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" style="margin-left: 410px;width: 255px">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="width: 715px;margin-bottom: 5px">
    <p class="15" style="text-align: center;margin-bottom: 0px;line-height: .7" align="center"><strong><u><span
                        style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 22.0000pt;">PASS KELUAR</span></u></strong></p>
    <p class="15" style="text-align: center;margin-top: 0px;margin-bottom: 5px" align="center"><strong><span
                    style="font-family: Calibri; font-weight: bold; font-size: 12.0000pt;">NO : <?= $model->inv_no; ?></span></strong></p>
    <table style="border-collapse: collapse; width: 100%;">
        <tbody>
        <tr>
            <td style="width: 50%;">
                <table id="main" style="border-collapse: collapse; width: 100%; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
                    <tbody>
                    <tr>
                        <td style=" border: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NAMA</span></p>
                        </td>
                        <td style="border-left: none; border-right: 1pt solid windowtext; border-top: 1pt solid windowtext; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style=" border-left: none; border-right: 1pt solid windowtext; border-top: 1pt solid windowtext; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=$model->mobil->customer->nama_customer;?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">MERK/ TAHUN</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">
                                    <?=strtoupper($model->mobil->mobilTipe->merk->nama.' '. $model->mobil->mobilTipe->nama.' / '.$model->mobil->tahun_pembuatan);?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">WARNA</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=strtoupper($model->mobil->warna);?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NO. RANGKA</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=$model->mobil->no_rangka;?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NO. WO</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=$model->wo_no;?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">TGL. WO</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=strtoupper(strftime("%d %B %Y",strtotime($model->wo_tgl)))?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">STATUS</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=$model->biaya;?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">SERVICE ADVISOR</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=strtoupper($model->nama_sa);?></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="  border-left: 1pt solid windowtext; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">WAKTU CETAK</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                        </td>
                        <td style="  border-left: none; border-right: 1pt solid windowtext; border-top: none; border-bottom: 1pt solid windowtext;">
                            <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?=date("d/m/Y H:i:s");?></span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td style="width: 50%; vertical-align: middle;text-align: center">
                <table style="border-collapse: collapse; width: 90%; margin-left: 25px" border="5">
                    <tbody>
                    <tr>
                        <td style="width: 455.219px; text-align: center; vertical-align: middle;">
                            <p style="font-family: Calibri;"><strong><span
                                            style="font-family: Calibri; color: #000000; font-weight: bold; font-size: 36.0000pt;"><?=$model->mobil->no_pol;?></span></strong></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="15" style="text-align: center;" align="center"></p>
    <table style="border-collapse: collapse;width: 100%; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="1" cellspacing="0">
        <tbody>
        <tr>
            <td style="width: 191.1500pt;  border: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">TANDA TANGAN &amp; STEMPEL</span></p>
            </td>
            <td style="width: 191.2000pt;  border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 191.2000pt;  border-left: none; border-right: 1.0000pt solid windowtext; border-top: 1.0000pt solid windowtext; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">PENERIMA</span></p>
            </td>
        </tr>
        <tr style="height: 45pt;">
            <td style="width: 191.1500pt;  border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 191.2000pt;  border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 191.2000pt;  border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 191.1500pt;  border-left: 1.0000pt solid windowtext; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">(<?=strtoupper($kasir);?>)</span></p>
            </td>
            <td style="width: 191.2000pt;  border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
            </td>
            <td style="width: 191.2000pt;  border-left: none; border-right: 1.0000pt solid windowtext; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                <p class="15" style="text-align: center;" align="center"><span style="font-family: Calibri; font-size: 9.0000pt;">(<?=strtoupper($model->mobil->customer->nama_customer);?>)</span></p>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 9.0000pt;">KUPON INI HARAP DISERAHKAN KE SATPAM PADA SAAT ANDA KELUAR DARI BENGKEL/ SHOWROOM</span></strong>
    </p>
</section>
<style>
    table#main td p
    {
        margin: 1px !important;
    }
</style>