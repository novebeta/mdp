<?
/** @var BodyPaint $model */
//$nama     = $alamat = $city = $tlp = $nama_asuransi = $user = '';
//$customer = $model->mobil->customer;
//$nama     = $customer->nama_customer;
//$alamat   = $customer->alamat;
//$city     = $customer->city;
//$telp     = $customer->telp;
//if ( $model->asuransi_bp_id != '' ) {
//	$asuransi = AsuransiBp::model()->findByPk( $model->asuransi_bp_id );
//	if ( $asuransi != null ) {
//		$nama_asuransi = $asuransi->nama;
//	}
//}
/** @var Users $userfind */
//$userfind = Users::model()->findByPk( Yii::app()->user->getId() );
//if ( $userfind != null ) {
//	$user = $userfind->name;
//}
// update vat %
// UPDATE nscc_body_paint nbp SET vat = floor((nbp.grand_total/(nbp.total_jasa+nbp.total_parts))*100)-100
$jasa = $bahan_baku = 0 ;

if($model->total_jasa > 0){
    $totalJasa = ($model->total_jasa);
    $jasa = intval($totalJasa * 0.2);
    $bahan_baku = intval($totalJasa) - $jasa;
}
$totalAll = $jasa + $bahan_baku + $model->total_parts;
?>
<div class="book">
    <div class="page">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header1.png" width=85>
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header2.png" width=335 style="margin-left: 290">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/line1.png" style="margin-bottom: 15px" width="715">
        <p style="margin-bottom: 15px;font-family: Calibri; font-size: 10.5pt; text-align: center;"><strong><u>
        <span style="font-family: Calibri; font-weight: bold; text-decoration: underline; font-size: 22.0000pt;">KWITANSI
        </span></u></strong></p>
        <table style="border-collapse: collapse; width: 600pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
            <tbody>
            <tr>
                <td style="width: 113.1500pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NOMOR</span></p>
                </td>
                <td style="width: 14.2000pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 389.8000pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $model->inv_no; ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 113.1500pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">TELAH TERIMA DARI</span></p>
                </td>
                <td style="width: 14.2000pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 389.8000pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper( $model->asuransi->nama ); ?></span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 113.1500pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">TERBILANG</span></p>
                </td>
                <td style="width: 14.2000pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 389.8000pt;  border: none;" valign="top">
                    <p class="15" style="width:500px">
                        <span style="font-family: Calibri; font-size: 11.0000pt;"># <?= strtoupper( terbilang( abs( $model->grand_total ) ) ); ?>  RUPIAH #</span>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="width: 113.1500pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">UNTUK PEMBAYARAN</span></p>
                </td>
                <td style="width: 14.2000pt;  border: none;" valign="top">
                    <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                </td>
                <td style="width: 389.8000pt;  border: none;" valign="top">
                    <table style="border-collapse: collapse; width: 249.85pt; margin-left: 5.4pt; border: none; font-family: 'Times New Roman'; font-size: 10pt;" border="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">PERBAIKAN</span></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">MOBIL <?= $model->mobil->mobilTipe->nama; ?></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">NO. POLISI</span></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= $model->mobil->no_pol; ?></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">ATAS NAMA</span></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;"><?= strtoupper($model->mobil->customer->nama_customer); ?></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">BAHAN BAKU</span></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;display:inline-block;width: 20px;">RP </span> <span
                                            style="font-family: Calibri; font-size: 11.0000pt;display:inline-block;width: 100px;text-align: right"><?=number_format( $bahan_baku, 0, ',', '.' )?></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">JASA</span></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;display:inline-block;width: 20px;">RP </span> <span
                                            style="font-family: Calibri; font-size: 11.0000pt;display:inline-block;width: 100px;text-align: right"><?=number_format( $jasa, 0, ',', '.' )?></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">SPAREPART</span></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;">:</span></p>
                            </td>
                            <td style="width: 110.4000pt;  border-left: none; border-right: none; border-top: none; border-bottom: 1.0000pt solid windowtext;" valign="top">
                                <p class="15"><span style="font-family: Calibri; font-size: 11.0000pt;display:inline-block;width: 20px;">RP </span>
                                    <span style="font-family: Calibri; font-size: 11.0000pt;display:inline-block;width: 100px;text-align: right"><?= number_format( $model->total_parts, 0, ',', '.' ) ?></span>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TOTAL</span></strong></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">:</span></strong></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;display:inline-block;width: 20px;">RP </span></strong> <strong>
                                        <span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;display:inline-block;width: 100px;text-align: right"><?= number_format( $totalAll, 0, ',', '.' ) ?></span></strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">PPN</span></strong></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">:</span></strong></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;display:inline-block;width: 20px;">RP </span></strong> <strong>
                                        <span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;display:inline-block;width: 100px;text-align: right"><?= number_format( $model->vatrp, 0, ',', '.' ) ?></span></strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 125.6000pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">GRAND TOTAL</span></strong></p>
                            </td>
                            <td style="width: 13.8500pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">:</span></strong></p>
                            </td>
                            <td style="width: 110.4000pt;  border: none;" valign="top">
                                <p class="15"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;display:inline-block;width: 20px;">RP </span></strong> <strong>
                                        <span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;display:inline-block;width: 100px;text-align: right"><?= number_format( $model->grand_total, 0, ',', '.' ) ?></span></strong></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <p class="15">&nbsp;</p>
                </td>
            </tr>
            </tbody>
        </table>
        <p class="15"><span style="font-family: Calibri; font-size: 9.0000pt;">&nbsp;</span></p>
        <p class="15" style="margin-left: 354.4000pt; text-align: center;" align="center"><strong><span
                        style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">TEGAL, <?=strtoupper(strftime("%d %B %Y"))?></span></strong></p>
        <p class="15" style="margin-left: 354.4000pt; text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">&nbsp;</span></strong>
        </p>
        <p class="15" style="margin-left: 354.4000pt; text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">&nbsp;</span></strong>
        </p>
        <p class="15" style="margin-left: 354.4000pt; text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">&nbsp;</span></strong>
        </p>
        <p class="15" style="margin-left: 354.4000pt; text-align: center;" align="center"><strong><span style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">&nbsp;</span></strong>
        </p>
        <p class="15" style="margin-left: 354.4000pt; text-align: center;" align="center"><strong><span
                        style="font-family: Calibri; font-weight: bold; font-size: 11.0000pt;">(MERDEKA DWI PUTRA)</span></strong></p>
    </div>
</div>