/*
*	Create index di antrian history
*/

ALTER TABLE `nscc_antrian_history` MODIFY COLUMN `bagian`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `tanggal`;
CREATE INDEX `bagian` ON `nscc_antrian_history`(`bagian`) USING BTREE ;
CREATE INDEX `id_antrian` ON `nscc_antrian_history`(`id_antrian`) USING BTREE ;
CREATE INDEX `tanggal` ON `nscc_antrian_history`(`tanggal`) USING BTREE ;
CREATE INDEX `counter` ON `nscc_antrian_history`(`counter`) USING BTREE ;