/*
* Index nscc_user_employee
*/
SET FOREIGN_KEY_CHECKS=0;
DROP INDEX `idx_nscc_user_employee` ON `nscc_user_employee`;
CREATE UNIQUE INDEX `idx_nscc_user_employee` ON `nscc_user_employee`(`id`) USING BTREE ;
DROP INDEX `idx_nscc_user_employee_0` ON `nscc_user_employee`;
CREATE UNIQUE INDEX `idx_nscc_user_employee_0` ON `nscc_user_employee`(`employee_id`) USING BTREE ;
SET FOREIGN_KEY_CHECKS=1;

/*
* Add view nscc_user_employee_view
*/
CREATE 
ALGORITHM=UNDEFINED 
SQL SECURITY DEFINER 
VIEW `nscc_user_employee_view`AS 
select 
u.id, e.employee_id, ue.user_employee_id,
u.`name`, u.`password`, u.user_id, e.nama_employee, e.kode_employee, e.tipe, t.kode,
u.active, u.last_visit_date, u.security_roles_id, u.store, u.up
FROM nscc_users u
LEFT JOIN nscc_user_employee ue on ue.id = u.id
LEFT JOIN nscc_employees e on e.employee_id = ue.employee_id
LEFT JOIN nscc_tipe_employee t on t.tipe_employee_id = e.tipe
ORDER BY user_id ;