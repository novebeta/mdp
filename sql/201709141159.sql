/*
*	Add diagnosa view
*/
CREATE 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `nscc_diagnosa_view`AS 
SELECT
	d.*, k.tgl,
	k.customer_id,
	c.nama_customer
FROM
	nscc_diagnosa d
LEFT JOIN nscc_konsul k ON k.konsul_id = d.konsul_id
LEFT JOIN nscc_customers c ON k.customer_id = c.customer_id ;