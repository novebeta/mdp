

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nscc_dropping_history
-- ----------------------------
DROP TABLE IF EXISTS `nscc_dropping_history`;
CREATE TABLE `nscc_dropping_history` (
  `id_history` varchar(50) NOT NULL,
  `order_dropping_id` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `store` varchar(20) DEFAULT NULL,
  `store_pengirim` varchar(20) DEFAULT NULL,
  `status` text,
  `desc` text,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
