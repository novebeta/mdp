/*
* Add nscc_assign_employee
*/
CREATE TABLE `nscc_assign_employees` (
`assign_employee_id`  varchar(36) NOT NULL ,
`employee_id`  varchar(36) NOT NULL ,
`tipe_employee_id`  varchar(36) NOT NULL ,
`tgl`  date NOT NULL ,
`tdate`  datetime NOT NULL ,
PRIMARY KEY (`assign_employee_id`)
)
ENGINE=InnoDB
ROW_FORMAT=Compact
;

/*
* Add nscc_assign_employee_view
*/
CREATE 
ALGORITHM=UNDEFINED 
SQL SECURITY DEFINER 
VIEW `nscc_assign_employees_view`AS 
select
nae.assign_employee_id, nae.employee_id, nae.tipe_employee_id, 
ne.nama_employee, nte.kode,
nae.tgl, nae.tdate
FROM nscc_assign_employees nae
LEFT JOIN nscc_employees ne on ne.employee_id = nae.employee_id
LEFT JOIN nscc_tipe_employee nte on nte.tipe_employee_id = nae.tipe_employee_id
where nae.tgl = cast(now() AS date) ;

/*
* Add field assign (default 0)
*/
ALTER TABLE `nscc_tipe_employee` ADD COLUMN `assign`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 supaya muncul di assign employee' AFTER `nama_`;
INSERT INTO `adwis`.`nscc_tipe_employee` (`tipe_employee_id`, `kode`, `nama_`, `assign`) VALUES ('9', 'FINC', 'Finance', '0');
