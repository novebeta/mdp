/*
Navicat MariaDB Data Transfer

Source Server         : Maria
Source Server Version : 100206
Source Host           : localhost:3366
Source Database       : posng

Target Server Type    : MariaDB
Target Server Version : 100206
File Encoding         : 65001

Date: 2018-12-27 09:15:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nscc_customers_linkage
-- ----------------------------
DROP TABLE IF EXISTS `nscc_customers_linkage`;
CREATE TABLE `nscc_customers_linkage` (
  `linkage_id` varchar(255) NOT NULL,
  `customers_id` varchar(255) NOT NULL,
  `online_id` int(11) NOT NULL,
  PRIMARY KEY (`linkage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
