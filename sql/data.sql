-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.36 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for pos
CREATE DATABASE IF NOT EXISTS `pos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pos`;


-- Dumping structure for table pos.nsc_barang
CREATE TABLE IF NOT EXISTS `nsc_barang` (
  `barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(15) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `ket` varchar(50) DEFAULT NULL,
  `grup_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`barang_id`),
  UNIQUE KEY `idx_nsc_barang_0` (`kode_barang`,`nama_barang`),
  KEY `idx_nsc_barang` (`grup_id`),
  CONSTRAINT `fk_nsc_barang` FOREIGN KEY (`grup_id`) REFERENCES `nsc_grup` (`grup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_barang: ~0 rows (approximately)
/*!40000 ALTER TABLE `nsc_barang` DISABLE KEYS */;
REPLACE INTO `nsc_barang` (`barang_id`, `kode_barang`, `nama_barang`, `ket`, `grup_id`, `active`) VALUES
	(1, 'AAR', 'KRIM PAGI AA', 'Krim pagi anti aging', 3, 1);
/*!40000 ALTER TABLE `nsc_barang` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_beauty
CREATE TABLE IF NOT EXISTS `nsc_beauty` (
  `beauty_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_beauty` varchar(20) NOT NULL,
  `gol_id` int(11) NOT NULL,
  PRIMARY KEY (`beauty_id`),
  KEY `idx_nsc_beauty` (`gol_id`),
  CONSTRAINT `fk_nsc_beauty` FOREIGN KEY (`gol_id`) REFERENCES `nsc_gol` (`gol_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_beauty: ~0 rows (approximately)
/*!40000 ALTER TABLE `nsc_beauty` DISABLE KEYS */;
/*!40000 ALTER TABLE `nsc_beauty` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_gol
CREATE TABLE IF NOT EXISTS `nsc_gol` (
  `gol_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_gol` varchar(20) NOT NULL,
  PRIMARY KEY (`gol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_gol: ~0 rows (approximately)
/*!40000 ALTER TABLE `nsc_gol` DISABLE KEYS */;
REPLACE INTO `nsc_gol` (`gol_id`, `nama_gol`) VALUES
	(1, 'GOL BARANG'),
	(2, 'GOL I'),
	(3, 'GOL II'),
	(4, 'GOL III'),
	(5, 'GOL IV'),
	(6, 'GOL V');
/*!40000 ALTER TABLE `nsc_gol` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_grup
CREATE TABLE IF NOT EXISTS `nsc_grup` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_grup` varchar(20) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  PRIMARY KEY (`grup_id`),
  KEY `idx_nsc_grup` (`kategori_id`),
  CONSTRAINT `fk_nsc_grup` FOREIGN KEY (`kategori_id`) REFERENCES `nsc_kategori` (`kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_grup: ~3 rows (approximately)
/*!40000 ALTER TABLE `nsc_grup` DISABLE KEYS */;
REPLACE INTO `nsc_grup` (`grup_id`, `nama_grup`, `kategori_id`) VALUES
	(1, 'FACIAL', 1),
	(2, 'MEDIK', 2),
	(3, 'KOSMESTIK', 2);
/*!40000 ALTER TABLE `nsc_grup` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_kategori
CREATE TABLE IF NOT EXISTS `nsc_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_kategori: ~2 rows (approximately)
/*!40000 ALTER TABLE `nsc_kategori` DISABLE KEYS */;
REPLACE INTO `nsc_kategori` (`kategori_id`, `nama_kategori`) VALUES
	(1, 'JASA / PERAWATAN'),
	(2, 'OBAT / CREAM');
/*!40000 ALTER TABLE `nsc_kategori` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_pasien
CREATE TABLE IF NOT EXISTS `nsc_pasien` (
  `pasien_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pasien` varchar(20) NOT NULL,
  `no_pasien` varchar(10) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `telp` varchar(25) DEFAULT NULL,
  `alamat` text,
  `kota` varchar(30) DEFAULT NULL,
  `negara` varchar(30) DEFAULT NULL,
  `awal` datetime NOT NULL COMMENT 'AWAL DAFTAR',
  `akhir` datetime NOT NULL COMMENT 'TERAKHIR TRANSAKSI',
  PRIMARY KEY (`pasien_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_pasien: ~1 rows (approximately)
/*!40000 ALTER TABLE `nsc_pasien` DISABLE KEYS */;
REPLACE INTO `nsc_pasien` (`pasien_id`, `nama_pasien`, `no_pasien`, `tempat_lahir`, `tgl_lahir`, `email`, `telp`, `alamat`, `kota`, `negara`, `awal`, `akhir`) VALUES
	(1, 'PROBANDUS', 'P.0001', 'YOGYAKARTA', '1984-05-13', 'PROBANDUS@GMAIL.COM', '0888217132899', 'JL. Nologaten', 'YOGYAKARTA', 'INDONESIA', '2014-05-14 14:33:47', '2014-05-14 14:33:47');
/*!40000 ALTER TABLE `nsc_pasien` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_price
CREATE TABLE IF NOT EXISTS `nsc_price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` double NOT NULL DEFAULT '0',
  `barang_id` int(11) NOT NULL,
  `gol_id` int(11) NOT NULL,
  PRIMARY KEY (`price_id`),
  KEY `idx_nsc_price` (`barang_id`),
  KEY `idx_nsc_price_0` (`gol_id`),
  CONSTRAINT `fk_nsc_price_0` FOREIGN KEY (`gol_id`) REFERENCES `nsc_gol` (`gol_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_nsc_price` FOREIGN KEY (`barang_id`) REFERENCES `nsc_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_price: ~0 rows (approximately)
/*!40000 ALTER TABLE `nsc_price` DISABLE KEYS */;
REPLACE INTO `nsc_price` (`price_id`, `value`, `barang_id`, `gol_id`) VALUES
	(2, 220000, 1, 1);
/*!40000 ALTER TABLE `nsc_price` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_security_roles
CREATE TABLE IF NOT EXISTS `nsc_security_roles` (
  `security_roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  `ket` varchar(50) DEFAULT NULL,
  `sections` text,
  PRIMARY KEY (`security_roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_security_roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `nsc_security_roles` DISABLE KEYS */;
REPLACE INTO `nsc_security_roles` (`security_roles_id`, `role`, `ket`, `sections`) VALUES
	(2, 'System Administrator', 'System Administrator', '0000,1000,2000,3000');
/*!40000 ALTER TABLE `nsc_security_roles` ENABLE KEYS */;


-- Dumping structure for table pos.nsc_users
CREATE TABLE IF NOT EXISTS `nsc_users` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `last_visit_date` datetime DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `security_roles_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE,
  KEY `idx_nsc_users` (`security_roles_id`),
  CONSTRAINT `fk_nsc_users` FOREIGN KEY (`security_roles_id`) REFERENCES `nsc_security_roles` (`security_roles_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos.nsc_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `nsc_users` DISABLE KEYS */;
REPLACE INTO `nsc_users` (`id`, `user_id`, `password`, `last_visit_date`, `inactive`, `security_roles_id`) VALUES
	(1, 'admin', '7X8ARtt1hFwT2XqxQjzOfo5DKq7kVsxcWsG/vfTf2JBKugQTrJZclTN3J08Nv5qe7UOSTnimU3KTQtzSb9gzGw', '2014-05-14 08:29:48', 0, 2);
/*!40000 ALTER TABLE `nsc_users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
