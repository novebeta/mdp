ALTER TABLE `nscc_pembantu_pelunasan_piutang_detil`
  ADD COLUMN `account_code` VARCHAR(15)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0'
  AFTER `pembantu_pelunasan_piutang_detil_id`;

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table posnsc.nscc_pelunasan_piutang
CREATE TABLE IF NOT EXISTS `nscc_pelunasan_piutang` (
  `pelunasan_piutang_id` varchar(50) NOT NULL,
  `total` decimal(30,2) NOT NULL DEFAULT '0.00',
  `no_bg_cek` varchar(50) DEFAULT NULL,
  `no_bukti` varchar(50) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `tgl` date NOT NULL,
  `bank_id` varchar(50) NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `store` varchar(20) NOT NULL,
  `tdate` datetime NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `up` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pelunasan_piutang_id`),
  KEY `idx_nscc_pelunasan_utang` (`bank_id`) USING BTREE,
  KEY `idx_nscc_pelunasan_utang_0` (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Dumping data for table posnsc.nscc_pelunasan_piutang: ~0 rows (approximately)
/*!40000 ALTER TABLE `nscc_pelunasan_piutang` DISABLE KEYS */;
/*!40000 ALTER TABLE `nscc_pelunasan_piutang` ENABLE KEYS */;


-- Dumping structure for table posnsc.nscc_pelunasan_piutang_detil
CREATE TABLE IF NOT EXISTS `nscc_pelunasan_piutang_detil` (
  `pelunasan_piutang_detil_id` varchar(36) NOT NULL,
  `kas_dibayar` decimal(30,2) NOT NULL DEFAULT '0.00',
  `no_faktur` varchar(50) DEFAULT NULL,
  `sisa` decimal(30,2) NOT NULL DEFAULT '0.00',
  `pelunasan_piutang_id` varchar(50) NOT NULL,
  `transfer_item_id` varchar(50) NOT NULL,
  PRIMARY KEY (`pelunasan_piutang_detil_id`),
  KEY `idx_nscc_pelunasan_utang_detil` (`pelunasan_piutang_id`) USING BTREE,
  KEY `idx_nscc_pelunasan_utang_detil_0` (`transfer_item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Dumping data for table posnsc.nscc_pelunasan_piutang_detil: ~0 rows (approximately)
/*!40000 ALTER TABLE `nscc_pelunasan_piutang_detil` DISABLE KEYS */;
/*!40000 ALTER TABLE `nscc_pelunasan_piutang_detil` ENABLE KEYS */;


-- Dumping structure for table posnsc.nscc_pelunasan_sisa_piutang
CREATE TABLE IF NOT EXISTS `nscc_pelunasan_sisa_piutang` (
  `transfer_item_id` tinyint(4) NOT NULL,
  `tgl` tinyint(4) NOT NULL,
  `no_faktur` tinyint(4) NOT NULL,
  `nilai` tinyint(4) NOT NULL,
  `doc_ref` tinyint(4) NOT NULL,
  `sisa` tinyint(4) NOT NULL,
  `customer_id` tinyint(4) NOT NULL,
  `store` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table posnsc.nscc_pelunasan_sisa_piutang: 0 rows
/*!40000 ALTER TABLE `nscc_pelunasan_sisa_piutang` DISABLE KEYS */;
/*!40000 ALTER TABLE `nscc_pelunasan_sisa_piutang` ENABLE KEYS */;


-- Dumping structure for trigger posnsc.nscc_pelunasan_piutang_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `nscc_pelunasan_piutang_before_insert` BEFORE INSERT ON `nscc_pelunasan_piutang` FOR EACH ROW BEGIN
IF NEW.pelunasan_piutang_id IS NULL OR LENGTH(NEW.pelunasan_piutang_id) = 0 THEN
  SET NEW.pelunasan_piutang_id = UUID();
  END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger posnsc.nscc_pelunasan_piutang_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `nscc_pelunasan_piutang_before_update` BEFORE UPDATE ON `nscc_pelunasan_piutang` FOR EACH ROW BEGIN
  IF !(OLD.total  <=> NEW.total AND OLD.no_bg_cek <=> NEW.no_bg_cek
	AND OLD.no_bukti <=> NEW.no_bukti AND OLD.no_bg_cek <=> NEW.no_bg_cek
	AND OLD.doc_ref <=> NEW.doc_ref AND OLD.tgl <=> NEW.tgl
	AND OLD.bank_id <=> NEW.bank_id AND OLD.customer_id <=> NEW.customer_id
	AND OLD.store <=> NEW.store AND OLD.tdate <=> NEW.tdate
	AND OLD.user_id <=> NEW.user_id) THEN
		SET NEW.up = 0;
  END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger posnsc.nscc_pelunasan_piutang_detil_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `nscc_pelunasan_piutang_detil_before_insert` BEFORE INSERT ON `nscc_pelunasan_piutang_detil` FOR EACH ROW BEGIN
IF NEW.pelunasan_piutang_detil_id IS NULL OR LENGTH(NEW.pelunasan_piutang_detil_id) = 0 THEN
  SET NEW.pelunasan_piutang_detil_id = UUID();
END IF;
UPDATE nscc_pelunasan_piutang npu SET npu.up = 0 WHERE npu.pelunasan_piutang_id = NEW.pelunasan_piutang_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger posnsc.nscc_pelunasan_piutang_detil_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `nscc_pelunasan_piutang_detil_before_update` BEFORE UPDATE ON `nscc_pelunasan_piutang_detil` FOR EACH ROW BEGIN
  IF !(OLD.kas_dibayar  <=> NEW.kas_dibayar AND OLD.no_faktur <=> NEW.no_faktur
	AND OLD.sisa <=> NEW.sisa AND OLD.pelunasan_piutang_id <=> NEW.pelunasan_piutang_id
	AND OLD.transfer_item_id <=> NEW.transfer_item_id) THEN
		UPDATE nscc_pelunasan_piutang npu SET npu.up = 0 WHERE npu.pelunasan_piutang_id = NEW.pelunasan_piutang_id;
		UPDATE nscc_pelunasan_piutang npu SET npu.up = 0 WHERE npu.pelunasan_piutang_id = OLD.pelunasan_piutang_id;
  END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
