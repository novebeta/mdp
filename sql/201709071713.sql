/*
* Ini compare structure ADWIS di local sama POSNSC01 di server Natasha.
*/

CREATE
  ALGORITHM = UNDEFINED
  DEFINER =`root`@`127.0.0.1`
  SQL SECURITY DEFINER
VIEW `nscc_stocker` AS
  SELECT
    `ne`.`employee_id`   AS `stocker_id`,
    `ne`.`nama_employee` AS `nama_stocker`,
    `ne`.`gol_id`        AS `gol_id`,
    `ne`.`kode_employee` AS `kode_stocker`,
    `ne`.`active`        AS `active`,
    `ne`.`store`         AS `store`,
    `ne`.`up`            AS `up`,
    `ne`.`tipe`          AS `tipe`
  FROM
    (
        `nscc_employees` `ne`
        JOIN `nscc_tipe_employee` `nte` ON (
        (
          `ne`.`tipe` = `nte`.`tipe_employee_id`
        )
        )
    )
  WHERE
    (`nte`.`kode` = 'STK');
CREATE
  ALGORITHM = UNDEFINED
  DEFINER =`root`@`127.0.0.1`
  SQL SECURITY DEFINER
VIEW `nscc_pengelola` AS
  SELECT
    `ne`.`employee_id`   AS `pc_id`,
    `ne`.`nama_employee` AS `nama_pc`,
    `ne`.`gol_id`        AS `gol_id`,
    `ne`.`kode_employee` AS `kode_pc`,
    `ne`.`active`        AS `active`,
    `ne`.`store`         AS `store`,
    `ne`.`up`            AS `up`,
    `ne`.`tipe`          AS `tipe`
  FROM
    (
        `nscc_employees` `ne`
        JOIN `nscc_tipe_employee` `nte` ON (
        (
          `ne`.`tipe` = `nte`.`tipe_employee_id`
        )
        )
    )
  WHERE
    (`nte`.`kode` = 'PC');
CREATE
  ALGORITHM = UNDEFINED
  DEFINER =`root`@`127.0.0.1`
  SQL SECURITY DEFINER
VIEW `nscc_admin` AS
  SELECT
    `ne`.`employee_id`   AS `admin_id`,
    `ne`.`nama_employee` AS `nama_admin`,
    `ne`.`gol_id`        AS `gol_id`,
    `ne`.`kode_employee` AS `kode_admin`,
    `ne`.`active`        AS `active`,
    `ne`.`store`         AS `store`,
    `ne`.`up`            AS `up`,
    `ne`.`tipe`          AS `tipe`
  FROM
    (
        `nscc_employees` `ne`
        JOIN `nscc_tipe_employee` `nte` ON (
        (
          `ne`.`tipe` = `nte`.`tipe_employee_id`
        )
        )
    )
  WHERE
    (`nte`.`kode` = 'ADM');
