/*

MIGRASI KAS DAN KAS DETAIL

*/

START TRANSACTION ;

CREATE TABLE `antrian` (
  `id_antrian`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `nomor`  int(11) NOT NULL ,
  `counter`  varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `no_member`  varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `kartu`  varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `bagian`  enum('pendaftaran','counter','medis','kasir','selesai') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'pendaftaran' ,
  `tujuan`  enum('konsultasi','pembelian') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'konsultasi' ,
  `pending`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `picked`  enum('picked','pick') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'pick' ,
  `tanggal`  datetime NOT NULL ,
  `timestamp`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `spesial`  enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id_antrian`)
)
  ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
  ROW_FORMAT=Compact
;

CREATE DEFINER=`root`@`::1` TRIGGER `nscc_antrian_before_insert` BEFORE INSERT ON `antrian`
FOR EACH ROW BEGIN
  IF NEW.id_antrian IS NULL OR LENGTH(NEW.id_antrian) = 0 THEN
    SET NEW.id_antrian = UUID();
  END IF;
END;

CREATE TABLE `nscc_area` (
  `area_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `area_name`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `up`  tinyint(4) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`area_id`)
)
  ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
  ROW_FORMAT=Compact
;

CREATE DEFINER=`root`@`localhost` TRIGGER `nscc_area_before_insert` BEFORE INSERT ON `nscc_area`
FOR EACH ROW BEGIN
  IF NEW.area_id IS NULL OR LENGTH(NEW.area_id) = 0 THEN
    SET NEW.area_id = UUID();
  END IF;
END;

CREATE TABLE `nscc_laha_import` (
  `laha_import_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `doc_ref`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `p_d_kas_bank_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_d_kas_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_d_piutangcard_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_d_piutangcard_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_obat_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_k_jasa_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_k_obat_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_jasa_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_ppn_obat_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_k_ppn_obat_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_d_total_biaya`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_k_pph21_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_pph22_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_pph23_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_pph4_2_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_kas_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_k_pph21_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_k_pph22_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_k_pp23_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_k_pph4_2_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `s_total_setor`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `tgl`  date NOT NULL ,
  `tdate`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `id_user`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_d_pot_jual_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_d_pot_jual_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_apotik_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_k_apotik_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_ppn_apotik_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_ppn_apotik_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_k_round_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `p_k_round_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
  `piu_d_total_bank`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `piu_d_fee_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `piu_d_fee_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_round_coa`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_round_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_lain_lain_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `p_k_pendmuka_n`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `b_k_pph21_note`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_pph22_note`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_pp23_note`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `b_k_pph4_2_note`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  PRIMARY KEY (`laha_import_id`),
  INDEX `idx_nscc_laha_import` (`p_d_kas_bank_id`) USING BTREE
)
  ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
  ROW_FORMAT=Compact
;

CREATE TABLE `nscc_laha_import_bank_fee` (
  `laha_import_bank_fee_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `bank`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `laha_import_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `bank_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `note_`  varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `type_`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 = fee\n1 = setoran' ,
  PRIMARY KEY (`laha_import_bank_fee_id`),
  CONSTRAINT `fk_nscc_laha_import_bank_fee` FOREIGN KEY (`laha_import_id`) REFERENCES `nscc_laha_import` (`laha_import_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_nscc_laha_import_bank_fee_0` FOREIGN KEY (`bank_id`) REFERENCES `nscc_bank` (`bank_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `idx_nscc_laha_import_details_0` (`laha_import_id`) USING BTREE ,
  INDEX `idx_nscc_laha_import_bank_fee` (`bank_id`) USING BTREE
)
  ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
  ROW_FORMAT=Compact
;

CREATE TABLE `nscc_laha_import_biaya` (
  `laha_import_biaya_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `amount`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
  `laha_import_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `account_code`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' ,
  `note_`  varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
  `store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  PRIMARY KEY (`laha_import_biaya_id`),
  CONSTRAINT `fk_nscc_laha_import_details` FOREIGN KEY (`laha_import_id`) REFERENCES `nscc_laha_import` (`laha_import_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `idx_nscc_laha_import_details` (`laha_import_id`) USING BTREE
)
  ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
  ROW_FORMAT=Compact
;

CREATE TABLE `nscc_store_area` (
  `store_area_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `area_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
  `up`  tinyint(4) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`store_area_id`),
  CONSTRAINT `nscc_store_area_fk0` FOREIGN KEY (`store`) REFERENCES `nscc_store` (`store_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nscc_store_area_fk1` FOREIGN KEY (`area_id`) REFERENCES `nscc_area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  INDEX `nscc_store_area_fk0_idx` (`store`) USING BTREE ,
  INDEX `nscc_store_area_fk1_idx` (`area_id`) USING BTREE
)
  ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
  ROW_FORMAT=Compact
;

CREATE DEFINER=`root`@`localhost` TRIGGER `nscc_store_area_before_insert` BEFORE INSERT ON `nscc_store_area`
FOR EACH ROW BEGIN
  IF NEW.store_area_id IS NULL OR LENGTH(NEW.store_area_id) = 0 THEN
    SET NEW.store_area_id = UUID();
  END IF;
END;




ALTER TABLE `nscc_kas` MODIFY COLUMN `total`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `keperluan`;

ALTER TABLE `nscc_kas` ADD COLUMN `amount`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `bank_id`;

ALTER TABLE `nscc_kas` MODIFY COLUMN `type_`  tinyint(4) NOT NULL DEFAULT 0 AFTER `tdate`;

ALTER TABLE `nscc_kas` MODIFY COLUMN `arus`  tinyint(4) NOT NULL AFTER `up`;

ALTER TABLE `nscc_kas` ADD COLUMN `total_debit`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `jml_cabang`;

ALTER TABLE `nscc_kas` ADD COLUMN `total_kredit`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `total_debit`;

ALTER TABLE `nscc_kas_detail` ADD COLUMN `store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `account_code`;

CREATE INDEX `nscc_kas_detail_fk3_idx` ON `nscc_kas_detail`(`store`) USING BTREE ;

SET @tgl = "2017-08-09";

DROP TRIGGER IF EXISTS nscc_kas_detail_before_update;

UPDATE nscc_kas_detail INNER JOIN nscc_kas ON nscc_kas_detail.kas_id = nscc_kas.kas_id
SET nscc_kas_detail.total = -nscc_kas_detail.total,nscc_kas_detail.store = nscc_kas.store
WHERE nscc_kas.tgl <= @tgl;

UPDATE nscc_kas SET amount = total,
  total_debit =
  (SELECT SUM(IF(nscc_kas_detail.total>=0,nscc_kas_detail.total,0))
   FROM nscc_kas_detail WHERE nscc_kas_detail.kas_id = nscc_kas.kas_id AND nscc_kas_detail.visible = 1
   GROUP BY  nscc_kas.kas_id) + IF(nscc_kas.arus = 1,nscc_kas.amount,0),
  total_kredit =
  -(SELECT SUM(IF(nscc_kas_detail.total<0,nscc_kas_detail.total,0))
    FROM nscc_kas_detail WHERE nscc_kas_detail.kas_id = nscc_kas.kas_id AND nscc_kas_detail.visible = 1
    GROUP BY  nscc_kas.kas_id) + IF(nscc_kas.arus = -1,-(nscc_kas.amount),0)
WHERE nscc_kas.tgl <= @tgl;

DELIMITER //
CREATE DEFINER=`root`@`localhost` TRIGGER `nscc_kas_detail_before_update`
BEFORE UPDATE ON `nscc_kas_detail`
FOR EACH ROW
  BEGIN
    IF !(OLD.item_name  <=> NEW.item_name AND OLD.total <=> NEW.total
         AND OLD.kas_id <=> NEW.kas_id AND OLD.account_code <=> NEW.account_code) THEN
      UPDATE nscc_kas SET up = 0 WHERE kas_id = NEW.kas_id;
      UPDATE nscc_kas SET up = 0 WHERE kas_id = OLD.kas_id;
    END IF;
  END; //

DELIMITER ;

DROP TABLE IF EXISTS `nscc_restrict_date`;
CREATE TABLE `nscc_restrict_date` (
  `res_date_id` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `author` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `updater` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `up` tinyint(1) NOT NULL,
  PRIMARY KEY (`res_date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE
  ALGORITHM = UNDEFINED
  DEFINER =`root`@`127.0.0.1`
  SQL SECURITY DEFINER
VIEW `nscc_stocker` AS
  SELECT
    `ne`.`employee_id`   AS `stocker_id`,
    `ne`.`nama_employee` AS `nama_stocker`,
    `ne`.`gol_id`        AS `gol_id`,
    `ne`.`kode_employee` AS `kode_stocker`,
    `ne`.`active`        AS `active`,
    `ne`.`store`         AS `store`,
    `ne`.`up`            AS `up`,
    `ne`.`tipe`          AS `tipe`
  FROM
    (
        `nscc_employees` `ne`
        JOIN `nscc_tipe_employee` `nte` ON (
        (
          `ne`.`tipe` = `nte`.`tipe_employee_id`
        )
        )
    )
  WHERE
    (`nte`.`kode` = 'STK');
CREATE
  ALGORITHM = UNDEFINED
  DEFINER =`root`@`127.0.0.1`
  SQL SECURITY DEFINER
VIEW `nscc_pengelola` AS
  SELECT
    `ne`.`employee_id`   AS `pc_id`,
    `ne`.`nama_employee` AS `nama_pc`,
    `ne`.`gol_id`        AS `gol_id`,
    `ne`.`kode_employee` AS `kode_pc`,
    `ne`.`active`        AS `active`,
    `ne`.`store`         AS `store`,
    `ne`.`up`            AS `up`,
    `ne`.`tipe`          AS `tipe`
  FROM
    (
        `nscc_employees` `ne`
        JOIN `nscc_tipe_employee` `nte` ON (
        (
          `ne`.`tipe` = `nte`.`tipe_employee_id`
        )
        )
    )
  WHERE
    (`nte`.`kode` = 'PC');
CREATE
  ALGORITHM = UNDEFINED
  DEFINER =`root`@`127.0.0.1`
  SQL SECURITY DEFINER
VIEW `nscc_admin` AS
  SELECT
    `ne`.`employee_id`   AS `admin_id`,
    `ne`.`nama_employee` AS `nama_admin`,
    `ne`.`gol_id`        AS `gol_id`,
    `ne`.`kode_employee` AS `kode_admin`,
    `ne`.`active`        AS `active`,
    `ne`.`store`         AS `store`,
    `ne`.`up`            AS `up`,
    `ne`.`tipe`          AS `tipe`
  FROM
    (
        `nscc_employees` `ne`
        JOIN `nscc_tipe_employee` `nte` ON (
        (
          `ne`.`tipe` = `nte`.`tipe_employee_id`
        )
        )
    )
  WHERE
    (`nte`.`kode` = 'ADM');


COMMIT;