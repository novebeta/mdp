
      /*
      * Script created by Quest Schema Compare at 14/09/2017 13:42:59.
      * Please back up your database before running this script.
      *
      * Synchronizing objects from posng to posng.
      */
    USE `posng`;

-- Drop foreign keys, referenced to nscc_employees
ALTER TABLE `posng`.`nscc_beauty_services` DROP FOREIGN KEY `nscc_beauty_services_ibfk_1`;

-- Drop foreign keys, referenced to nscc_employees
ALTER TABLE `posng`.`nscc_user_employee` DROP FOREIGN KEY `fk_nscc_user_employee_0`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `fk_nscc_bonus`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk1`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk2`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk3`;

ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk0`;

ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk1`;

ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk2`;

ALTER TABLE `posng`.`nscc_employees` DROP FOREIGN KEY `nscc_employees_ibfk_1`;

/* Header line. Object: nscc_bonus. Script date: 14/09/2017 13:42:59. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_bonus`;

CREATE TABLE `posng`.`_temp_nscc_bonus` (
	`bonus_id` varchar(36) NOT NULL,
	`tgl` date NOT NULL,
	`employee_id` varchar(50) NOT NULL,
	`type_` int(11) NOT NULL,
	`trans_no` varchar(50) NOT NULL,
	`barang_id` varchar(36) NOT NULL,
	`amount` decimal(30,2) NOT NULL default '0.00',
	`bonus_jual_id` varchar(36) NOT NULL,
	`persen_bonus` decimal(10,2) NOT NULL default '0.00',
	`amount_bonus` decimal(30,2) NOT NULL default '0.00',
	`store` varchar(20) NOT NULL,
	`tipe` tinyint(1) NOT NULL default '1',
	`tdate` datetime NOT NULL,
	`id_user` varchar(50) NOT NULL,
	`up` tinyint(1) NOT NULL default '0',
	`visible` tinyint(1) NOT NULL default '1',
	`tipe_trans` varchar(20) default 'sales',
	KEY `idx_nscc_bonus` ( `employee_id` ),
	KEY `nscc_bonus_fk1_idx` ( `barang_id` ),
	KEY `nscc_bonus_fk2_idx` ( `bonus_jual_id` ),
	KEY `nscc_bonus_fk3_idx` ( `store` ),
	PRIMARY KEY  ( `bonus_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_bonus`
( `amount`, `amount_bonus`, `barang_id`, `bonus_id`, `bonus_jual_id`, `employee_id`, `id_user`, `persen_bonus`, `store`, `tdate`, `tgl`, `tipe_trans`, `trans_no`, `type_`, `up`, `visible` )
SELECT
`amount`, `amount_bonus`, `barang_id`, `bonus_id`, `bonus_jual_id`, `employee_id`, `id_user`, `persen_bonus`, `store`, `tdate`, `tgl`, `tipe_trans`, `trans_no`, `type_`, `up`, `visible`
FROM `posng`.`nscc_bonus`;

DROP TABLE `posng`.`nscc_bonus`;

ALTER TABLE `posng`.`_temp_nscc_bonus` RENAME `nscc_bonus`;

/* Header line. Object: nscc_bonus_jual. Script date: 14/09/2017 13:42:59. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_bonus_jual`;

CREATE TABLE `posng`.`_temp_nscc_bonus_jual` (
	`bonus_jual_id` varchar(36) NOT NULL,
	`barang_id` varchar(36) NOT NULL,
	`bonus_name_id` varchar(36) NOT NULL,
	`persen_bonus` decimal(10,2) NOT NULL default '0.00',
	`store` varchar(20) NOT NULL,
	`up` tinyint(1) NOT NULL default '0',
	KEY `nscc_bonus_jual_fk0_idx` ( `barang_id` ),
	KEY `nscc_bonus_jual_fk1_idx` ( `store` ),
	KEY `nscc_bonus_jual_fk2_idx` ( `bonus_name_id` ),
	UNIQUE INDEX `nscc_bonus_jual_unique1` ( `barang_id`, `bonus_name_id`, `store` ),
	PRIMARY KEY  ( `bonus_jual_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
COMMENT = 'Persentase bonus penjualan (cream/perawatan)'
;

INSERT INTO `posng`.`_temp_nscc_bonus_jual`
( `barang_id`, `bonus_jual_id`, `bonus_name_id`, `persen_bonus`, `store`, `up` )
SELECT
`barang_id`, `bonus_jual_id`, `bonus_name_id`, `persen_bonus`, `store`, `up`
FROM `posng`.`nscc_bonus_jual`;

DROP TABLE `posng`.`nscc_bonus_jual`;

ALTER TABLE `posng`.`_temp_nscc_bonus_jual` RENAME `nscc_bonus_jual`;

/* Header line. Object: nscc_bonus_name. Script date: 14/09/2017 13:42:59. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_bonus_name`;

CREATE TABLE `posng`.`_temp_nscc_bonus_name` (
	`bonus_name_id` varchar(36) NOT NULL,
	`bonus_name` varchar(50) NOT NULL,
	`up` tinyint(1) NOT NULL default '0',
	PRIMARY KEY  ( `bonus_name_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
COMMENT = 'Nama bonus'
;

INSERT INTO `posng`.`_temp_nscc_bonus_name`
( `bonus_name`, `bonus_name_id`, `up` )
SELECT
`bonus_name`, `bonus_name_id`, `up`
FROM `posng`.`nscc_bonus_name`;

DROP TABLE `posng`.`nscc_bonus_name`;

ALTER TABLE `posng`.`_temp_nscc_bonus_name` RENAME `nscc_bonus_name`;

/* Header line. Object: nscc_employees. Script date: 14/09/2017 13:42:59. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_employees`;

CREATE TABLE `posng`.`_temp_nscc_employees` (
	`employee_id` varchar(50) NOT NULL,
	`nama_employee` varchar(100) NOT NULL,
	`gol_id` varchar(36) NOT NULL,
	`kode_employee` varchar(20) NOT NULL,
	`active` tinyint(4) NOT NULL default '1',
	`store` varchar(20) NOT NULL,
	`up` tinyint(3) unsigned NOT NULL default '0',
	`tipe` varchar(36) default NULL,
	`nickname` varchar(100) default NULL,
	KEY `idx_nsc_beauty` ( `gol_id` ),
	UNIQUE INDEX `idx_nscc_beauty` ( `nama_employee`, `store` ),
	PRIMARY KEY  ( `employee_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_employees`
( `active`, `employee_id`, `gol_id`, `kode_employee`, `nama_employee`, `nickname`, `store`, `tipe`, `up` )
SELECT
`active`, `employee_id`, `gol_id`, `kode_employee`, `nama_employee`, `nickname`, `store`, `tipe`, `up`
FROM `posng`.`nscc_employees`;

DROP TABLE `posng`.`nscc_employees`;

ALTER TABLE `posng`.`_temp_nscc_employees` RENAME `nscc_employees`;

-- Restore foreign keys, referenced to nscc_employees
ALTER TABLE `posng`.`nscc_beauty_services` ADD CONSTRAINT `nscc_beauty_services_ibfk_1`
	FOREIGN KEY ( `beauty_id` ) REFERENCES `nscc_employees` ( `employee_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Restore foreign keys, referenced to nscc_employees
ALTER TABLE `posng`.`nscc_user_employee` ADD CONSTRAINT `fk_nscc_user_employee_0`
	FOREIGN KEY ( `employee_id` ) REFERENCES `nscc_employees` ( `employee_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_bonus
ALTER TABLE `posng`.`nscc_bonus` ADD CONSTRAINT `fk_nscc_bonus`
	FOREIGN KEY ( `employee_id` ) REFERENCES `nscc_employees` ( `employee_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_bonus` ADD CONSTRAINT `nscc_bonus_fk2`
	FOREIGN KEY ( `bonus_jual_id` ) REFERENCES `nscc_bonus_jual` ( `bonus_jual_id` ) ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_bonus` ADD CONSTRAINT `nscc_bonus_fk3`
	FOREIGN KEY ( `store` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_bonus_jual
ALTER TABLE `posng`.`nscc_bonus_jual` ADD CONSTRAINT `nscc_bonus_jual_fk1`
	FOREIGN KEY ( `store` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_bonus_jual` ADD CONSTRAINT `nscc_bonus_jual_fk2`
	FOREIGN KEY ( `bonus_name_id` ) REFERENCES `nscc_bonus_name` ( `bonus_name_id` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_employees
ALTER TABLE `posng`.`nscc_employees` ADD CONSTRAINT `nscc_employees_ibfk_1`
	FOREIGN KEY ( `gol_id` ) REFERENCES `nscc_gol` ( `gol_id` ) ON UPDATE CASCADE;

