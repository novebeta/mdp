SET FOREIGN_KEY_CHECKS=0;

CREATE DEFINER=`root`@`localhost` TRIGGER `aisha_antrian_before_insert` BEFORE INSERT ON `aisha_antrian`
FOR EACH ROW BEGIN
  IF NEW.id_antrian IS NULL OR LENGTH(NEW.id_antrian) = 0 THEN
    SET NEW.id_antrian = UUID();
  END IF;
  IF NEW.counter IS NULL OR LENGTH(NEW.counter) = 0 THEN
    SET NEW.counter = NULL;
  END IF;
  SET NEW.end_ = 1;
  INSERT INTO `nscc_antrian_history`
  (`antrian_history_id`, `id_antrian`, `tanggal`, `bagian`, `counter`, `action`, `timestamp`, `nomor_pasien`, `nomor_antrian`) VALUES
    (UUID(), NEW.id_antrian, NEW.tanggal, 'daftar', NULL, 'Registrasi', NEW.`timestamp`, '', NEW.nomor_antrian);
  UPDATE `nscc_sys_prefs` SET `name_`='tdate_antrian', `value_`=now()
  WHERE (`name_`='tdate_antrian');
END;

CREATE DEFINER=`root`@`localhost` TRIGGER `aisha_antrian_after_update` AFTER UPDATE ON `aisha_antrian`
FOR EACH ROW BEGIN
  UPDATE `nscc_sys_prefs` SET `name_`='tdate_antrian', `value_`=now()
  WHERE (`name_`='tdate_antrian');
END;

CREATE INDEX `type_` ON `nscc_bank_trans`(`type_`) USING BTREE ;

CREATE INDEX `trans_no` ON `nscc_bank_trans`(`trans_no`) USING BTREE ;

CREATE INDEX `visible` ON `nscc_bank_trans`(`visible`) USING BTREE ;

CREATE INDEX `store` ON `nscc_bank_trans`(`store`) USING BTREE ;

CREATE DEFINER=`root`@`localhost` TRIGGER `nscc_beauty_onduty_before_insert` BEFORE INSERT ON `nscc_beauty_onduty`
FOR EACH ROW BEGIN
  UPDATE `nscc_sys_prefs` SET `name_`='tdate_antrian', `value_`=now()
  WHERE (`name_`='tdate_antrian');
END;

CREATE DEFINER=`root`@`localhost` TRIGGER `nscc_beauty_onduty_after_update` AFTER UPDATE ON `nscc_beauty_onduty`
FOR EACH ROW BEGIN
  UPDATE `nscc_sys_prefs` SET `name_`='tdate_antrian', `value_`=now()
  WHERE (`name_`='tdate_antrian');
END;

DROP TRIGGER `nscc_beauty_services_before_insert`;

CREATE DEFINER=`root`@`::1` TRIGGER `nscc_beauty_services_before_insert` BEFORE INSERT ON `nscc_beauty_services`
FOR EACH ROW BEGIN
  IF NEW.nscc_beauty_services_id IS NULL OR LENGTH(NEW.nscc_beauty_services_id) = 0 THEN
    SET NEW.nscc_beauty_services_id = UUID();
  END IF;
  UPDATE `nscc_sys_prefs` SET `name_`='tdate_antrian', `value_`=now()
  WHERE (`name_`='tdate_antrian');
END;

DROP TRIGGER `nscc_beauty_services_before_update`;

CREATE DEFINER=`root`@`::1` TRIGGER `nscc_beauty_services_before_update` BEFORE UPDATE ON `nscc_beauty_services`
FOR EACH ROW BEGIN
  IF !(OLD.total  <=> NEW.total AND OLD.beauty_id <=> NEW.beauty_id
       AND OLD.salestrans_details <=> NEW.salestrans_details AND OLD.final <=> NEW.final) THEN
    SET NEW.up = 0;
  END IF;
  UPDATE `nscc_sys_prefs` SET `name_`='tdate_antrian', `value_`=now()
  WHERE (`name_`='tdate_antrian');
END;

CREATE UNIQUE INDEX `Index 2` ON `nscc_comments`(`type`, `type_no`) USING BTREE ;

CREATE INDEX `type` ON `nscc_comments`(`type`) USING BTREE ;

CREATE INDEX `type_no` ON `nscc_comments`(`type_no`) USING BTREE ;

DROP TRIGGER `nscc_customers_before_update`;

CREATE INDEX `type` ON `nscc_gl_trans`(`type`) USING BTREE ;

CREATE INDEX `type_no` ON `nscc_gl_trans`(`type_no`) USING BTREE ;

CREATE INDEX `visible` ON `nscc_gl_trans`(`visible`) USING BTREE ;

DROP TABLE `nscc_store_area`;

ALTER EVENT `event_delete_session`
DISABLE;

DROP TABLE IF EXISTS `nscc_store_area`;
CREATE TABLE `nscc_store_area` (
  `store_area_id` varchar(36) NOT NULL,
  `store` varchar(20) NOT NULL,
  `area_id` varchar(36) NOT NULL,
  `up` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_area_id`),
  KEY `nscc_store_area_fk0_idx` (`store`) USING BTREE,
  KEY `nscc_store_area_fk1_idx` (`area_id`) USING BTREE,
  CONSTRAINT `nscc_store_area_fk0` FOREIGN KEY (`store`) REFERENCES `nscc_store` (`store_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nscc_store_area_fk1` FOREIGN KEY (`area_id`) REFERENCES `nscc_area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;
DROP TRIGGER IF EXISTS `nscc_store_area_before_insert`;
DELIMITER ;;
CREATE TRIGGER `nscc_store_area_before_insert` BEFORE INSERT ON `nscc_store_area` FOR EACH ROW BEGIN
  IF NEW.store_area_id IS NULL OR LENGTH(NEW.store_area_id) = 0 THEN
    SET NEW.store_area_id = UUID();
  END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS=1;