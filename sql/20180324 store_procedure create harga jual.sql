-- --------------------------------------------------------
-- Host:                         192.168.2.1
-- Server version:               5.5.47-0+deb8u1 - (Debian)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for procedure posnsc.nscc_create_jual
DELIMITER //
CREATE DEFINER=`rCpp9uvJpztw`@`%` PROCEDURE `nscc_create_jual`(IN `kode_brg` VARCHAR(50), IN `harga` decIMAL(10,0))
BEGIN
set @store = (SELECT store from nscc_sys_prefs where name_='stocker');
set @barang = (SELECT barang_id from nscc_barang where kode_barang=kode_brg);
INSERT INTO nscc_jual (jual_id, price, cost, disc, discrp, store, up, barang_id, duration)
values (uuid(), harga, 0, 0, 0, @store, 0, @barang,0)
ON DUPLICATE KEY UPDATE price=harga;

END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;