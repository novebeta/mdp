-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table posnsc.nscc_payment_journal
CREATE TABLE IF NOT EXISTS `nscc_payment_journal` (
  `payment_journal_id` varchar(36) NOT NULL,
  `doc_ref` varchar(50) NOT NULL,
  `doc_ref_other` varchar(50) DEFAULT NULL,
  `tgl` date NOT NULL,
  `tdate` datetime NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `note` mediumtext,
  `user_id` varchar(50) NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `total` decimal(30,2) NOT NULL DEFAULT '0.00',
  `lunas` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `store_kode` varchar(20) NOT NULL,
  `p` tinyint(3) NOT NULL DEFAULT '0',
  `up` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `editable` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payment_journal_id`),
  UNIQUE KEY `doc_ref_UNIQUE` (`doc_ref`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table posnsc.nscc_payment_journal_detail
CREATE TABLE IF NOT EXISTS `nscc_payment_journal_detail` (
  `payment_journal_detail_id` varchar(36) NOT NULL,
  `payment_journal_id` varchar(36) NOT NULL,
  `store_kode` varchar(20) NOT NULL,
  `debit` decimal(30,2) NOT NULL DEFAULT '0.00',
  `kredit` decimal(30,2) NOT NULL DEFAULT '0.00',
  `account_code` varchar(15) NOT NULL,
  `memo_` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`payment_journal_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table posnsc.nscc_pembantu_pelunasan_piutang
CREATE TABLE IF NOT EXISTS `nscc_pembantu_pelunasan_piutang` (
  `pembantu_pelunasan_piutang_id` varchar(36) NOT NULL,
  `tgl` date NOT NULL,
  `doc_ref` varchar(50) NOT NULL,
  `total` decimal(30,2) NOT NULL DEFAULT '0.00',
  `store` varchar(20) NOT NULL,
  `tdate` datetime NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `up` tinyint(4) NOT NULL DEFAULT '0',
  `bank_id` varchar(36) NOT NULL,
  `amount_bank` decimal(30,2) NOT NULL DEFAULT '0.00',
  `rounding` decimal(30,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`pembantu_pelunasan_piutang_id`),
  UNIQUE KEY `doc_ref_UNIQUE` (`doc_ref`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table posnsc.nscc_pembantu_pelunasan_piutang_detil
CREATE TABLE IF NOT EXISTS `nscc_pembantu_pelunasan_piutang_detil` (
  `pembantu_pelunasan_piutang_detil_id` varchar(36) NOT NULL,
  `kas_dibayar` decimal(30,2) NOT NULL DEFAULT '0.00',
  `no_faktur` varchar(50) NOT NULL,
  `sisa` decimal(30,2) NOT NULL DEFAULT '0.00',
  `pembantu_pelunasan_piutang_id` varchar(50) NOT NULL,
  `payment_journal_id` varchar(36) NOT NULL,
  `type_` tinyint(3) NOT NULL DEFAULT '0',
  `customer_id` varchar(36) NOT NULL,
  PRIMARY KEY (`pembantu_pelunasan_piutang_detil_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
