jun.RestrictDatestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.RestrictDatestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RestrictDateStoreId',
            url: 'RestrictDate',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'res_date_id'},
                {name:'user_id'},
                {name:'user_name'},
                {name:'start_date'},
                {name:'end_date'},
                {name:'created'},
                {name:'updated'},
                {name:'author'},
                {name:'updater'},
                {name:'up'},
            ]
        }, cfg));
    }
});
jun.rztRestrictDate = new jun.RestrictDatestore();
//jun.rztRestrictDate.load();
