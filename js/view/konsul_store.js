jun.Konsulstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Konsulstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KonsulStoreId',
            url: 'Konsul',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'konsul_id'},
                {name: 'no_antrian'},
                {name: 'customer_id'},
                {name: 'id_antrian'},
                {name: 'diagnosa'},
                {name: 'dokter_id'},
                {name: 'salestrans_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'counter'},
                {name: 'medis'},
                {name: 'beauty_id'},
                {name: 'type_'},
                {name: 'tdate'}, // hack => dipake buat no pasien
                {name: 'user_id'} // hack => dipake buat nama pasien
            ]
        }, cfg));
    }
});
jun.rztKonsul = new jun.Konsulstore();
jun.rztKonsulCounter = new jun.Konsulstore();
jun.rztKonsulTambahan = new jun.Konsulstore();
//jun.rztKonsul.load();
