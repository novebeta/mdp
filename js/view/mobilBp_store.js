jun.MobilBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilBpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilBpStoreId',
            url: 'MobilBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_id'},
                {name: 'mobil_tipe_id'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'customer_id'},
                {name: 'no_mesin'},
                {name: 'tahun_km'},
                {name: 'warna'},
                {name: 'mobilTipe'},
                {name: 'customer'},
            ]
        }, cfg));
    }
});
jun.rztMobilBp = new jun.MobilBpstore();
jun.rztMobilBpCmp = new jun.MobilBpstore();
jun.rztMobilBpLib = new jun.MobilBpstore();
//jun.rztMobilBp.load();
