jun.SyncHistorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.SyncHistorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SyncHistoryStoreId',
            url: 'SyncHistory',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'history_id'},
                {name:'sync_id'},
                {name:'store'},
                {name:'username'},
                {name:'type'},
                {name:'tdate'},
                {name:'startdate'},
                {name:'enddate'},
                {name:'action'},
                {name:'note'},
            ]
        }, cfg));
    }
});
jun.rztSyncHistory = new jun.SyncHistorystore();
//jun.rztSyncHistory.load();
