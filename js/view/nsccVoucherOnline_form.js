jun.NsccVoucherOnlineWin = Ext.extend(Ext.Window, {
    title: 'NsccVoucherOnline',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-NsccVoucherOnline',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'kode_voucher',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'kode_voucher',
                                    id:'kode_voucherid',
                                    ref:'../kode_voucher',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'nama_voucher',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'nama_voucher',
                                    id:'nama_voucherid',
                                    ref:'../nama_voucher',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'textfield',
                            fieldLabel: 'desc',
                            hideLabel:false,
                            //hidden:true,
                            name:'desc',
                            id:'descid',
                            ref:'../desc',
                            anchor: '100%'
                            //allowBlank: 1
                        }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../exp',
                            fieldLabel: 'exp',
                            name:'exp',
                            id:'expid',
                            format: 'd M Y',
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../tdate',
                            fieldLabel: 'tdate',
                            name:'tdate',
                            id:'tdateid',
                            format: 'd M Y',
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'outlet_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'outlet_id',
                                    id:'outlet_idid',
                                    ref:'../outlet_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.NsccVoucherOnlineWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'NsccVoucherOnline/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'NsccVoucherOnline/create/';
                }
             
            Ext.getCmp('form-NsccVoucherOnline').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztNsccVoucherOnline.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-NsccVoucherOnline').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});