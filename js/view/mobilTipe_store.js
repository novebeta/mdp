jun.MobilTipestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilTipestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilTipeStoreId',
            url: 'MobilTipe',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_tipe_id'},
                {name: 'merk_id'},
                {name: 'nama'},
                {name: 'mobil_kategori_id'},
            ]
        }, cfg));
    }
});
jun.rztMobilTipe = new jun.MobilTipestore();
jun.rztMobilTipeCmp = new jun.MobilTipestore();
jun.rztMobilTipeLib = new jun.MobilTipestore();
jun.rztMobilTipeLib.load();
