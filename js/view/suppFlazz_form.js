jun.SuppFlazzWin = Ext.extend(Ext.Window, {
    title: 'Supplier',
    modez: 1,
    width: 400,
    height: 155,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SuppFlazz',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_supp',
                        id: 'nama_suppid',
                        ref: '../nama_supp',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'email',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'email',
                    //     id: 'emailid',
                    //     ref: '../email',
                    //     maxLength: 30,
                    //     //allowBlank: 1,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'telp',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'telp',
                    //     id: 'telpid',
                    //     ref: '../telp',
                    //     maxLength: 25,
                    //     //allowBlank: 1,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'alamat',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'alamat',
                    //     id: 'alamatid',
                    //     ref: '../alamat',
                    //     anchor: '100%'
                    //     //allowBlank: 1
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'city',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'city',
                    //     id: 'cityid',
                    //     ref: '../city',
                    //     maxLength: 100,
                    //     //allowBlank: 1,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'pt',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'pt',
                    //     id: 'ptid',
                    //     ref: '../pt',
                    //     maxLength: 255,
                    //     //allowBlank: 1,
                    //     anchor: '100%'
                    // },
                    {
                        xtype: 'combo',
                        lazyRender: true,
                        style: 'margin-bottom:2px',
                        mode: 'local',
                        enableKeyEvents: true,
                        forceSelection: true,
                        fieldLabel: 'Hutang',
                        store: jun.rztChartMasterCmp,
                        ref: '../account_code',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        anchor: '100%'
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SuppFlazzWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'SuppFlazz/update/id/' + this.id;
        } else {
            urlz = 'SuppFlazz/create/';
        }
        Ext.getCmp('form-SuppFlazz').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztSuppFlazz.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SuppFlazz').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});