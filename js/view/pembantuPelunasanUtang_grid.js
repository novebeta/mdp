jun.PembantuPelunasanUtangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PembantuPelunasanUtang",
    id: 'docs-jun.PembantuPelunasanUtangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            renderer: function (v, m, r, i) {
                if (r.get('void_user_id')){
                    m.style+="background-color:#ff9999;";
                }
                return v;
            }

        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer: function (v, m, r, i) {
                if (r.get('void_user_id')){
                    m.style+="background-color:#ff9999;";
                }
                return v;
            }
        },
//        {
//            header: 'Supplier',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'supplier_id',
//            width: 100,
//            renderer: jun.renderSupplier
//        },
//        {
//            header: 'Receipt No.',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'no_bukti',
//            width: 100
//        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: function (v, m, r, i) {
                if (r.get('void_user_id')){
                    m.style+="background-color:#ff9999;";
                }
                return Ext.util.Format.number(v, "0,0.00");
            }

        }
    ],
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0) {
            jun.rztSupplierLib.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztPembantuPelunasanUtang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tglPelunasanUtangid').getValue();
                    var tgl = Ext.getCmp('tglPembantuPelunasanUtangid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztPembantuPelunasanUtang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Debt Payment',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Debt Payment',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Void Debt Payment',
                    ref: '../btnVoid'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglPembantuPelunasanUtangid',
                    ref: '../tgl'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Update Tgl',
                    ref: '../btnUpdateTgl'
                }
            ]
        };
        jun.PembantuPelunasanUtangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnVoid.on('Click', this.loadVoidForm, this);
//                this.btnDelete.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.btnUpdateTgl.on('Click', this.loadEditTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    loadEditTgl: function (t, e) {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Pembantu Pelunasan Utang");
            return;
        }
        var idz = selectedz.json.pembantu_pelunasan_utang_id;
        var form = new jun.PembantuPelunasanUtangUpdateTgl({id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PembantuPelunasanUtangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pembantu_pelunasan_utang_id;
        var form = new jun.PembantuPelunasanUtangWin({
            modez: 1, id: idz,
            title: this.record.get("void_user_id")?'<span style="color: #ff1600; font-size: 12px">Pembantu Pelunasan Hutang</span>':'Pembantu Pelunasan Hutang'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPembantuPelunasanUtangDetil.baseParams = {
            pembantu_pelunasan_utang_id: idz
        };
        jun.rztPembantuPelunasanUtangDetil.load();
        jun.rztPembantuPelunasanUtangDetil.baseParams = {};
    },
    loadVoidForm: function(){
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Jenis Pelayanan");
            return;
        }
        else if (this.record.get('void_user_id')){
            Ext.MessageBox.alert("Warning", "Transaksi Ini Sudah Dibatalkan");
            return;
        }
        var idz = selectedz.json.pembantu_pelunasan_utang_id;
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah Anda Yakin Ingin Membatalkan Transaksi Ini ?', this.cancelYes, this);
    },

    cancelYes: function(btn){
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PembantuPelunasanUtang/Void/id/' + record.json.pembantu_pelunasan_utang_id,
            method: 'POST',
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    refreshTgl: function () {
        this.store.reload();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PembantuPelunasanUtang/delete/id/' + record.json.pembantu_pelunasan_utang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPembantuPelunasanUtang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
