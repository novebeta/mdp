jun.QuickSalesMenu = Ext.extend(Ext.Window, {
    id: 'form-QuickSalesMenu',
    width: 600,
    height: 450,
    layout: 'absolute',
    closable: false,
    resizable: false,
    draggable: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'panel',
                html: '<img src="css/silk_v013/icons/user_green.png" />',
                bodyStyle: 'background-color: #E8E8E8; border: none;',
                height: 16,
                width: 16,
                x: 10,
                y: 8,
                frame: false
            },
            {
                xtype: 'label',
                text: this.username,
                x: 30,
                y: 10
            },
            {
                xtype: 'panel',
                id: 'logomenu',
//                html: '<img src='+'"images/naavagreen-logo.png"'+' />',
                html: SYSTEM_LOGO,
                bodyStyle: 'background-color: #E8E8E8; border: none;',
                height: 80,
                width: 340,
                x: (this.width - 340) / 2,
                y: 100,
                frame: false
            },
            {
                xtype: 'button',
                text: 'Go to App',
                cls: 'big-text',
                width: 100,
                // height: 30,
                hidden: this.state == 1 ? true : false,
                x: (this.width - 100) / 2,
                y: 220,
                listeners: {
                    click: function () {
                        var form = document.createElement("form");
                        form.setAttribute("method", "POST");
                        form.setAttribute("action", "");
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", 'app');
                        hiddenField.setAttribute("value", true);
                        form.appendChild(hiddenField);
                        document.body.appendChild(form);
                        form.submit();
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Sales 1',
                width: 100,
                cls: 'big-text',
                x: (this.width - 100) / 2,
                y: 220 + 35 - (this.state == 1 ? 35 : 0),
                listeners: {
                    click: function () {
                        var form = new jun.SalestransWin({modez: 0});
                        form.show();
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Sales 2',
                width: 100,
                cls: 'big-text',
                x: (this.width - 100) / 2,
                y: 220 + 35 + 35 - (this.state == 1 ? 35 : 0),
                listeners: {
                    click: function () {
                        var form = new jun.SalestransWinTouch({modez: 0});
                        form.show();
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Logout',
                width: 100,
                cls: 'big-text',
                x: (this.width - 100) / 2,
                y: 220 + 35 + 35 + 35 - (this.state == 1 ? 35 : 0),
                listeners: {
                    click: function () {
                        Ext.MessageBox.confirm('Logout', 'Are you sure want to Logout?', function (btn) {
                                if (btn == 'yes') {
                                    location.href = "Site/Logout";
                                }
                            },
                            this);
                    }
                }
            }
        ];
        jun.QuickSalesMenu.superclass.initComponent.call(this);
    }
});
//======================================================================
jun.SalestransWinTouch = Ext.extend(Ext.Window, {
    title: 'Sales',
    modez: 1,
    width: '100%',
    height: '100%',
    layout: 'form',
    id: 'win-Salestrans-Touch',
    modal: true,
    padding: 5,
    resizable: !1,
    draggable: false,
    closeForm: false,
    statusActive: -1,
    maximized: true,
    onEsc: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
                if (btn == 'yes') {
                    this.close();
                }
            },
            this);
    },
    initComponent: function () {
        if (jun.rztKetTransCmp.getTotalCount() === 0) {
            jun.rztKetTransCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Salestrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'border',
                flex: 1,
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "panel",
                        region: 'west',
                        // flex: 1,
                        layout: {
                            type: 'vbox',
                            padding: '5',
                            align: 'stretch'
                        },
                        width: 430,
                        // defaults: {margins: '0 0 5 0'},
                        // margins: '0 0 0 5',
                        split: true,
                        // width: '25%',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        items: [
                            {
                                xtype: "panel",
                                frame: false,
                                layout: "form",
                                // width: '25%',
                                border: false,
                                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Receipt',
                                        hideLabel: false,
                                        hidden: true,
                                        name: 'doc_ref',
                                        id: 'doc_refid',
                                        ref: '../../../doc_ref',
                                        maxLength: 20,
                                        width: 175,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../../../tgl',
                                        fieldLabel: 'Date',
                                        name: 'tgl',
                                        id: 'tglid',
                                        format: 'd M Y',
                                        width: 200,
                                        readOnly: true,
                                        allowBlank: false,
                                        value: DATE_NOW
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Pasien',
                                        // msgTarget : 'side',
                                        anchor: '-20',
                                        defaults: {
                                            flex: 1
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Customer',
                                                emptyText: 'Input Customer',
                                                id: 'nama_customer_id',
                                                name: 'nama_customer',
                                                allowBlank: false,
                                                width: 200,
                                                x: 85,
                                                y: 32,
                                                style: {height: '32px'},
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        if (e.getKey() == e.ENTER) {
                                                            jun.Sales_getCustomer(f.getRawValue());
                                                        }
                                                    },
                                                    focus: function (f) {
                                                        f.selectText();
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'silk13-user',
                                                hidden: false,
                                                id: 'btnCustomer_id',
                                                ref: '../../../../btnCustomer',
                                                scale: 'large'
                                                // x: 290,
                                                // y: 34
                                            },
                                            {
                                                xtype: 'hidden',
                                                name: "customer_id",
                                                id: 'customer_id_id',
                                                ref: '../../../../customer',
                                                value: ""
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Doctor',
                                        emptyText: 'No Doctor',
                                        ref: '../../../dokter',
                                        id: 'doktersales_id',
                                        name: 'nama_dokter',
                                        allowBlank: true,
                                        width: 200,
                                        x: 85,
                                        y: 62 + 10,
                                        style: {height: '32px'},
                                        listeners: {
                                            focus: function (f) {
                                                f.selectText();
                                                var form = new jun.InputDoctorWinTouch();
                                                form.show();
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'hidden',
                                        name: "dokter_id",
                                        id: 'dokter_id_id',
                                        value: ""
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Keterangan',
                                        emptyText: 'Tidak ada keterangan',
                                        ref: '../../../ketTrans',
                                        id: 'KetTrans_nama_id',
                                        name: 'keterangan',
                                        allowBlank: true,
                                        width: 200,
                                        x: 85,
                                        y: 62 + 10,
                                        style: {height: '32px'},
                                        listeners: {
                                            focus: function (f) {
                                                f.selectText();
                                                var form = new jun.InputKetTransWinTouch();
                                                form.show();
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'hidden',
                                        name: "akronim",
                                        id: 'akronim_id',
                                        value: ""
                                    }
                                ]
                            },
                            {
                                xtype: 'spacer',
                                flex: 1
                            },
                            {
                                xtype: "panel",
                                frame: false,
                                layout: "fit",
                                border: false,
                                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                                items: [
                                    new jun.PaymentTouchGrid({
                                        'height': 250
                                    })
                                ]
                            },
                            {
                                xtype: "panel",
                                frame: false,
                                layout: "absolute",
                                // width: '25%',
                                border: false,
                                height: 230,
                                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                                // rowspan:2,
                                items: [
                                    {
                                        xtype: "label",
                                        text: "Sub Total",
                                        x: 9,
                                        y: 2,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'bruto',
                                        id: 'subtotalid',
                                        ref: '../../../subtotal',
                                        maxLength: 30,
                                        value: 0,
                                        readOnly: true,
                                        width: 295,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 2,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "VAT",
                                        x: 9,
                                        y: 32,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'vat',
                                        id: 'vatid',
                                        ref: '../../../vat',
                                        maxLength: 30,
                                        value: 0,
                                        readOnly: true,
                                        width: 295,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 32,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "Discount",
                                        x: 9,
                                        y: 62,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'disc',
                                        id: 'discid',
                                        ref: '../../../disc',
                                        maxLength: 5,
                                        value: 0,
                                        minValue: 0,
                                        maxValue: 100,
                                        width: 80,
                                        readOnly: true,
                                        enableKeyEvents: true,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 62,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "%",
                                        x: 5 + 90 + 80,
                                        y: 62,
                                        style: {
                                            'height': 26,
                                            'width': 15,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'discrp',
                                        id: 'discrpid',
                                        ref: '../../../discrp',
                                        value: 0,
                                        maxLength: 30,
                                        width: 199,
                                        readOnly: true,
                                        enableKeyEvents: true,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90 + 80 + 15,
                                        y: 62,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "Rounding",
                                        x: 9,
                                        y: 92,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'rounding',
                                        id: 'roundingid',
                                        ref: '../../../rounding',
                                        maxLength: 30,
                                        value: 0,
                                        readOnly: true,
                                        width: 295,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 92,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "Total",
                                        x: 9,
                                        y: 122,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'total',
                                        id: 'totalid',
                                        ref: '../../../total',
                                        value: 0,
                                        readOnly: true,
                                        maxLength: 30,
                                        width: 295,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 122,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "Collect",
                                        x: 9,
                                        y: 152,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'bayar',
                                        id: 'bayarid',
                                        ref: '../../../bayar',
                                        readOnly: true,
                                        value: 0,
                                        maxLength: 30,
                                        width: 295,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 152,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#AAD4FF',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: "label",
                                        text: "Change",
                                        x: 9,
                                        y: 182,
                                        style: {
                                            'height': 26,
                                            'width': 90,
                                            'font-size': '20px',
                                            'color': 'white',
                                            'background': 'grey',
                                            'padding': '2 0 0 5'
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'kembali',
                                        id: 'kembaliid',
                                        ref: '../../../kembali',
                                        value: 0,
                                        readOnly: true,
                                        maxLength: 30,
                                        width: 295,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2,
                                        x: 5 + 90,
                                        y: 182,
                                        style: {
                                            'height': 28,
                                            'font-size': '20px',
                                            'color': '#00FF00',
                                            'text-align': 'right',
                                            'background': 'grey',
                                            'border': 'none'
                                        }
                                    },
                                    {
                                        xtype: 'hidden',
                                        id: 'overrideid',
                                        ref: '../../../override',
                                        name: 'override'
                                    },
                                    {
                                        xtype: 'hidden',
                                        id: 'totalpotid',
                                        ref: '../../../totalpot',
                                        name: 'totalpot'
                                    },
                                    {
                                        xtype: 'hidden',
                                        id: 'total_discrp1id',
                                        ref: '../../../total_discrp1',
                                        name: 'total_discrp1'
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                frame: false,
                                layout: 'hbox',
                                layoutConfig: {
                                    padding: '5',
                                    align: 'middle'
                                },
                                border: false,
                                // height: 230,
                                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                                defaults: {
                                    margins: '0 5 0 0',
                                    flex: 1
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        text: 'Save',
                                        cls: 'big-text',
                                        ctCls: 'x-btn-over',
                                        hidden: false,
                                        id: 'btnsave-Touch',
                                        ref: '../../../btnSave',
                                        scale: 'large'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Payment',
                                        cls: 'big-text',
                                        ctCls: 'x-btn-over',
                                        iconCls: 'silk13-money',
                                        scale: 'large',
                                        ref: '../../../btnPayment'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Save & Close',
                                        hidden: true,
                                        ref: '../../../btnSaveClose',
                                        scale: 'large'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Cancel',
                                        cls: 'big-text',
                                        ctCls: 'x-btn-over',
                                        ref: '../../../btnCancel',
                                        scale: 'large'
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        region: 'center',
                        margins: '5 5 5 0',
                        layout: {
                            type: 'vbox',
                            padding: '5',
                            align: 'stretch'
                        },
                        defaults: {
                            margins: '0 0 5 0',
                            padding: '5'
                        },
                        items: [
                            {
                                title: "History",
                                //bodyStyle: "-webkit-transform:scale(1);-moz-transform-scale(1);",
                                // html: '<html><body><iframe width="100%" height="100%" src="http://localhost:81/posng/customers/testhistory/nobase/7559KDR01" style="-webkit-transform:scale(0.9);-moz-transform-scale(0.9);"></iframe></body></html>',
                                height: 500,
                                id: 'form-Salestrans-gridHistory',
                                autoScroll: true
                            },
                            new jun.SalestransDetailsGridTouch({
                                region: 'center',
                                title: "Product/Treatment Details",
                                // x: 5,
                                // y: 120,
                                ref: '../../gridDetails',
                                flex: 1,
                                frameHeader: !1,
                                header: !1,
                                margins: '0'
                            })
                        ]
                    }
                    // {
                    //     xtype: "panel",
                    //     flex: 1,
                    //     // frame: false,
                    //     // layout: "form",
                    //     layout: { type: 'vbox', align: 'stretch', padding: 5},
                    //     // width: 950,
                    //     // border: false,
                    //     bodyStyle: "background-color: #E4E4E4;padding: 10px",
                    //     rowspan:3,
                    //     items: [
                    //
                    //     ]
                    // },
                    // {
                    //     xtype: "panel",
                    //     // frame: false,
                    //     layout: "form",
                    //     // width: '25%',
                    //     // border: false,
                    //     bodyStyle: "background-color: #E4E4E4;padding: 10px",
                    //     // rowspan:2,
                    //     items: [
                    //
                    //     ]
                    // },
                ]
            }
        ];
        // this.fbar = {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'button',
        //             text: 'Save (F10)',
        //             hidden: false,
        //             id: 'btnsave-Touch',
        //             ref: '../btnSave',
        //             scale: 'large'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Save & Close',
        //             hidden: true,
        //             ref: '../btnSaveClose',
        //             scale: 'large'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Cancel',
        //             ref: '../btnCancel',
        //             scale: 'large'
        //         }
        //     ]
        // };
        jun.SalestransWinTouch.superclass.initComponent.call(this);
//        this.ketdisc.on('specialkey', function(t,e){
//            e.preventDefault();
//            if(e.getKey() == e.TAB){
//                var cmb = Ext.getCmp('barangdetailsid');
//                if (cmb) {
//                    cmb.focus();
//                }
//            }
//        }, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscrpChange, this);
        this.bayar.on('keyup', this.onBayarChange, this);
        this.btnPayment.on('Click', this.onbtnPaymentclick, this);
//        this.bank.on('select', this.onBankChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        //this.btnPayment.on('click', this.onbtnPaymentclick, this);
        //this.btnDelPayment.on('click', this.onbtnDelPaymentclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnCustomer.on('click', this.onbtnCustomerclick, this);
        this.gridDetails.on('rowclick', jun.Sales_barcodeSetFocus, this);
        this.gridDetails.on('headerclick', jun.Sales_barcodeSetFocus, this);
//        this.customer.on('change', this.onCustomerSelect, this);
//        this.logCheck.on('check', this.onlogCheck, this);
        this.on("close", this.onWinClose, this);
        this.on("activate", this.onActive, this);
//        this.card_number.on("specialkey", this.onCardnumber, this);
        var obj = localStorage.getItem("sales_dokter_id");
//        var obj = jun.globalStore.readLocalStorage();
        this.dokter.setValue(obj);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            //this.btnPayment.setVisible(false);
            //this.btnDelPayment.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(false);
            //this.btnPayment.setVisible(true);
            //this.btnDelPayment.setVisible(true);
            //this.setDateTime();
        }
        if (SALES_OVERRIDE == "1") {
            //this.disc.setReadOnly(false);
            this.discrp.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.discrp.setReadOnly(true);
            Ext.getCmp('discid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discrpid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onActive: function (t) {
        switch (this.statusActive) {
            case -1:
                this.customer.focus(false, 50);
                break;
            case 0:
                this.gridDetails.barang.focus(false, 50);
                break;
            case 1:
                this.gridDetails.focusGrid();
                break;
            case 2:
                this.btnSave.focus();
                break;
        }
        this.statusActive = -1;
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F3, Ext.EventObject.F4, Ext.EventObject.F10, Ext.EventObject.F11, Ext.EventObject.F12],
            //ctrl: true,
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F12:
                        if (this.btnPayment.isVisible()) {
                            var cmb = Ext.getCmp('barangdetailsid');
                            if (cmb) {
                                cmb.collapse();
                            }
                            this.onbtnPaymentclick();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.gridDetails.focusGrid();
                        break;
                    case Ext.EventObject.F10:
                        if (!this.btnSave.disabled) {
                            this.saveForm();
                        }
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onbtnPaymentclick: function () {
        var win = Ext.getCmp('form-PaymentWin-Touch');
        if (win) {
            win.show();
        } else {
            var form = new jun.PaymentWinTouch();
            form.show();
        }
    },
    onlogCheck: function (c, s) {
//        this.storeCode.reset()
        this.storeCode.setDisabled(!s);
    },
//    onCustomerSelect: function (combo, record, index) {
//        twoRows("Welcome To Naavagreen", record.json.nama_customer);
//    },
    onWinClose: function () {
        jun.rztSalestransDetails.removeAll();
        jun.rztPayment.removeAll();
        jun.rztPaketTrans.removeAll();
        this.customer.lastQuery = null;
    },
    onBayarChange: function () {
        jun.rztSalestransDetails.refreshData();
    },
    onDiscChange: function (a) {
        var disc1 = parseFloat(a.getValue());
        //if (disc1 == 0) return;
        var discrpf = parseFloat(this.discrp.getValue());
        if (discrpf != 0) {
            this.discrp.setValue(0);
        }
        jun.rztSalestransDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            var total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onDiscrpChange: function (a) {
        var discrpf = parseFloat(a.getValue());
        Ext.getCmp('btnsalesdetilid').setDisabled(discrpf != 0);
        var discf = parseFloat(this.disc.getValue());
        if (discf != 0) {
            this.disc.setValue(0);
            var disc1 = 0;
            var discrp1 = 0;
            jun.rztSalestransDetails.each(function (record) {
                var barang = jun.getBarang(record.data.barang_id);
                var price = parseFloat(record.data.price);
                var qty = parseFloat(record.data.qty);
                var disc = parseFloat(record.data.disc);
                var discrp = parseFloat(record.data.discrp);
                var bruto = round(price * qty, 2);
                var vat = jun.getTax(record.data.barang_id);
                discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
                var subtotal = bruto - discrp;
                var totalpot = discrp + discrp1;
                var total_with_disc = bruto - totalpot;
                var total = bruto - discrp;
                var vatrp = round(total_with_disc * vat, 2);
                record.set('discrp', discrp);
                record.set('vat', vat);
                record.set('vatrp', vatrp);
                record.set('total_pot', totalpot);
                record.set('total', total);
                record.set('bruto', bruto);
                record.set('disc1', disc1);
                record.set('discrp1', discrp1);
                record.commit();
            });
        }
        var subtotalf = jun.rztSalestransDetails.sum('total');
        if (subtotalf == 0)return;
        jun.rztSalestransDetails.each(function (record) {
            var total = parseFloat(record.data.total);
            var disc1 = round((total / subtotalf) * 100, 2);
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * discrpf, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var kembali = parseFloat(this.kembali.getValue());
        if (kembali < 0) {
            Ext.Msg.alert('Error', "Change can't less than 0");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (jun.rztSalestransDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        var urlz = 'Salestrans/create/';
        Ext.getCmp('form-Salestrans').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztSalestransDetails.data.items, "data")),
                payment: Ext.encode(Ext.pluck(
                    jun.rztPayment.data.items, "data")),
                paket: Ext.encode(Ext.pluck(
                    jun.rztPaketTrans.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                menuDetil("COLLECTED", parseFloat(Ext.getCmp("bayarid").getValue()), 'CHANGE', parseFloat(Ext.getCmp("kembaliid").getValue()));
//                jun.globalStore.set('sales_dokter_id', this.dokter.getValue());
                localStorage.setItem("sales_dokter_id", this.dokter.getValue());
                jun.rztSalestrans.reload();
                var response = Ext.decode(a.response.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    console.log("Printer not ready");
                } else {
                    // opencashdrawer();
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                    print(PRINTER_RECEIPT, __openCashDrawer);
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                    print(PRINTER_RECEIPT, printData);
                }
                if (this.closeForm) {
                    this.close();
                } else {
                    this.btnDisabled(false);
                    Ext.getCmp('form-Salestrans').getForm().reset();
                    //this.setDateTime();
                    this.onWinClose();
                    jun.rztSalestransDetails.refreshData();
                    jun.CustomerDisplay_Welcome();
                }
                this.customer.focus();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
                this.customer.focus();
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
                if (btn == 'yes') {
                    this.close();
                }
            },
            this);
    },
    onbtnCustomerclick: function () {
        var win = Ext.getCmp('form-InputCustomerWin-Touch');
        if (win) {
            win.show();
        } else {
            var form = new jun.InputCustomerWinTouch();
            form.show();
        }
    }
});
jun.SalestransDetailsGridTouch = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.SalestransDetailsGrid-Touch',
    //iconCls: "silk-grid",
    //cls: 'custom-sales-details',
    // cls: 'grid-cell-touch',
    enableHdMenu: false,
    stripeRows: true,
    viewConfig: {
        forceFit: true,
        scrollOffset: 5
        // getRowClass: function (record, rowIndex, rp, store) {
        //     // rp.tstyle += 'height: 40px;valign=middle;';
        //     rp.tstyle = 'custom-sales-details-touch';
        // }
    },
    enableKeyEvents: true,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Del',
            width: 40,
            align: 'center',
            sortable: false,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                //metaData.css += 'position: relative; top: -5px; overflow: visible;';
                var id = Ext.id();
                (function () {
                    var btn = new Ext.Button({
                        renderTo: id,
                        iconCls: 'silk13-cross', //silk13-bin,
                        scale: 'medium',
                        data: record,
                        listeners: {
                            click: function (b, e) {
                                jun.Sales_removeItem(this.data);
                            }
                        }
                    });
                }).defer(25);
                return '<span id="' + id + '"></span>';
            }
        },
        {
            header: 'paket_trans_id',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width: 40,
            hidden: true
        },
        // {
        //     header: 'Pkg',
        //     sortable: false,
        //     resizable: false,
        //     dataIndex: 'paket_trans_id',
        //     width: 30,
        //     align: "left",
        //     renderer: jun.renderPackageIco
        // },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderKodeBarang,
            tdCls: 'custom-column'
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 300,
            renderer: jun.renderBarang,
            tdCls: 'custom-column'
        },
        {
            header: '-',
            width: 40,
            align: 'center',
            sortable: false,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                //metaData.css += 'padding-top: 0;';
                var id = Ext.id();
                (function () {
                    var btn = new Ext.Button({
                        renderTo: id,
                        iconCls: 'silk13-delete',
                        scale: 'medium',
                        data: record,
                        listeners: {
                            click: function (b, e) {
                                jun.Sales_decreaseItem(this.data);
                            }
                        }
                    });
                }).defer(25);
                return '<span id="' + id + '"></span>';
            }
        },
        {
            header: 'Qty',
            sortable: false,
            resizable: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: '+',
            width: 40,
            align: 'center',
            sortable: false,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                //metaData.css += 'padding-top: 0; vertical-align: middle;';
                var id = Ext.id();
                (function () {
                    var btn = new Ext.Button({
                        renderTo: id,
                        iconCls: 'silk13-add',
                        scale: 'medium',
                        data: record,
                        listeners: {
                            click: function (b, e) {
                                jun.Sales_increaseItem(this.data);
                            }
                        }
                    });
                }).defer(25);
                return '<span id="' + id + '"></span>';
            }
        },
        {
            header: 'Price',
            sortable: false,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: false,
            resizable: true,
            dataIndex: 'total',
            width: 120,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztSalestransDetails;
        this.store.on('remove', jun.CustomerDisplay_Total, this);
        this.store.on('update', jun.CustomerDisplay_Total, this);
        this.store.on('add', jun.CustomerDisplay_Total, this);
        this.tbar = {
            xtype: 'toolbar',
            enableOverflow: true,
            items: [
                {
                    xtype: 'buttongroup',
                    // columns: 3,
                    frame: false,
                    border: false,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            id: 'sales_barcode_id',
                            ref: '../../barcode',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            emptyText: 'Barcode',
                            width: 300,
                            height: 32,
                            listeners: {
                                specialkey: function (f, e) {
                                    if (e.getKey() == e.ENTER) {
                                        jun.Sales_getProduct(f.getRawValue());
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            cls: 'big-text',
                            ctCls: 'x-btn-over',
                            text: 'Add Item',
                            iconCls: 'silk13-package_add',
                            ref: '../../btnAddItem'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../disc_name'
                        }
                    ]
                },
                {
                    xtype: 'tbfill'
                },
                {
                    xtype: 'buttongroup',
                    frame: false,
                    border: false,
                    defaults: {
                        scale: 'large'
                    },
                    items: []
                }
            ]
        };
        jun.SalestransDetailsGridTouch.superclass.initComponent.call(this);
        this.btnAddItem.on('Click', this.onbtnAddItemclick, this);
//        this.btnAdd.on('Click', this.loadForm, this);
//        this.btnEdit.on('Click', this.onClickbtnEdit, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
//        this.qty.on('keyup', this.onDiscChange, this);
//        this.price.on('keyup', this.onDiscChange, this);
//        this.disc.on('keyup', this.onDiscChange, this);
//        this.disca.on('keyup', this.onDiscChange, this);
//        this.barang.on('select', this.onChangeBarang, this);
        this.on("activate", this.onActive, this);
        this.on("keypress", this.onKeyPress, this);
//        if (SALES_OVERRIDE == "1") {
//            this.disc.setReadOnly(false);
//            this.disca.setReadOnly(false);
//            this.price.setReadOnly(false);
//        } else {
//            this.disc.setReadOnly(true);
//            this.disca.setReadOnly(true);
//            this.price.setReadOnly(true);
//            Ext.getCmp('priceid').on('render', function (c) {
//                c.getEl().on('dblclick', function () {
//                    if (this.readOnly) {
//                        var login = new jun.login();
//                        login.show();
//                    }
//                }, this);
//            });
//            Ext.getCmp('discdetilid').on('render', function (c) {
//                c.getEl().on('dblclick', function () {
//                    if (this.readOnly) {
//                        var login = new jun.login();
//                        login.show();
//                    }
//                }, this);
//            });
//            Ext.getCmp('discadetilid').on('render', function (c) {
//                c.getEl().on('dblclick', function () {
//                    if (this.readOnly) {
//                        var login = new jun.login();
//                        login.show();
//                    }
//                }, this);
//            });
//        }
        this.on('cellclick', this.editQty);
    },
    onKeyPress: function (e) {
        switch (e.getKey()) {
            case 46:
                if (!this.btnDelete.disabled) {
                    this.deleteRec();
                }
                break;
        }
    },
    onActive: function (t) {
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F1, Ext.EventObject.F2, Ext.EventObject.DELETE, Ext.EventObject.F3],
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F1:
                        if (!this.btnAdd.disabled) {
                            this.barang.focus();
                            this.loadForm();
                        }
                        break;
                    case Ext.EventObject.F2:
                        if (!this.btnEdit.disabled) {
                            this.onClickbtnEdit();
                        }
                        break;
                    case Ext.EventObject.DELETE:
                        if (!this.btnDelete.disabled) {
                            this.deleteRec();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.barang.collapse();
                        this.focusGrid()
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    onStoreChange: function () {
        jun.rztSalestransDetails.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSalestransDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already inputted");
//                return;
//            }
//        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var disc1 = parseFloat(Ext.getCmp('discid').getValue());
        var ketpot = this.ketpot.getValue();
        var beauty = this.beauty.getValue();
        var beauty2 = this.beauty2.getValue();
        var disc_name = this.disc_name.getValue();
        var bruto = round(price * qty, 2);
        var vat = jun.getTax(barang_id);
        var subtotal = bruto - disca;
        var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
        var totalpot = disca + discrp1;
        var total_with_disc = bruto - totalpot;
        var total = bruto - disca;
        var vatrp = round(total_with_disc * vat, 2);
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('beauty_id', beauty);
            record.set('beauty2_id', beauty2);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc_name', disc_name);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztSalestransDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    beauty_id: beauty,
                    beauty2_id: beauty2,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1,
                    paket_trans_id: '',
                    paket_details_id: ''
                });
            jun.rztSalestransDetails.add(d);
        }
//        clearText();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        this.disc.reset();
        this.disca.reset();
        this.beauty.reset();
        this.beauty2.reset();
        menuDetil(barang.data.kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
        this.barang.focus();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        var customer_id = Ext.getCmp("customersales_id").getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected.");
            return;
        }
        if (customer_id == "" || customer_id == undefined) {
            Ext.MessageBox.alert("Error", "Customer must selected.");
            this.barang.reset();
            return;
        }
        var barang = jun.getBarang(barang_id);
        //this.price.setValue(barang.data.price);
        if (jun.isCatJasa(barang_id)) {
            this.beauty.setDisabled(false);
            this.beauty2.setDisabled(false);
        } else {
            this.beauty.reset();
            this.beauty.setDisabled(true);
            this.beauty2.reset();
            this.beauty2.setDisabled(true);
        }
        this.price.setValue(barang.data.price);
        this.disc.setValue(0);
        //this.disc_name.setValue(response.msg.nama_status);
        //this.onDiscChange(this.disc);
        //Ext.Ajax.request({
        //    url: 'SalestransDetails/GetDiskon',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        customer_id: customer_id,
        //        barang_id: barang_id
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        this.price.setValue(response.msg.price);
        //        this.disc.setValue(response.msg.value);
        //        this.disc_name.setValue(response.msg.nama_status);
        //        this.onDiscChange(this.disc);
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    focusGrid: function () {
        var record = this.sm.getSelected();
        var idx = 0;
        if (record == undefined) {
            var total_store = this.store.data.length;
            idx = total_store - 1;
        } else {
            idx = this.store.indexOf(record);
        }
        this.getSelectionModel().selectRow(idx);
        this.getView().focusRow(idx);
    },
    onClickbtnEdit: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            Ext.getCmp('win-Salestrans').statusActive = 1;
            return;
        }
        if (record.data.paket_trans_id.trim().length > 0) {
            Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
            Ext.getCmp('win-Salestrans').statusActive = 1;
            return;
        }
        if (this.btnEdit.text == 'Edit (F2)') {
            this.barang.setValue(record.data.barang_id);
//            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.disc.setValue(record.data.disc);
            this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            this.beauty.setValue(record.data.beauty_id);
            this.beauty2.setValue(record.data.beauty2_id);
            this.price.setValue(record.data.price);
            this.disc_name.setValue(record.data.disc_name);
            this.btnEdit.setText("Save (F2)");
            this.btnDisable(true);
        } else {
            this.loadForm();
            this.btnEdit.setText("Edit (F2)");
            this.btnDisable(false);
        }
        this.barang.focus();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_details;
        var form = new jun.SalestransDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (record.data.paket_trans_id != '') {
            Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
        var barang = jun.getBarang(record.data.barang_id);
        menuDetil(barang.data.kode_barang, "-" + record.data.total, 'TOTAL',
            parseFloat(Ext.getCmp("totalid").getValue()));
        Ext.getCmp('win-Salestrans').statusActive = 1;
    },
    onbtnPaymentclick: function () {
        var win = Ext.getCmp('win-Salestrans-Touch');
        win.onbtnPaymentclick();
    },
    onbtnAddItemclick: function () {
//        var customer_id = Ext.getCmp("customersales_id").getValue();
//        if (customer_id == "" || customer_id == undefined) {
//            Ext.MessageBox.alert("Error", "Customer must selected.");
//            return;
//        }
        var form = new jun.AddItemWinTouch();
        form.show();
    },
    editQty: function (grid, rowIndex, columnIndex, e) {
        if (grid.getColumnModel().getDataIndex(columnIndex) != 'qty') return;
        var form = new jun.EditItemQtyWinTouch({record: this.record});
        form.show();
        //form.qty.setValue(this.record.get('qty'));
    }
});
//======================================================================
jun.InputCustomerWinTouch = Ext.extend(Ext.Window, {
    title: 'Add Item',
    id: 'form-InputCustomerWin-Touch',
    width: 600,
    height: 450,
    layout: 'form',
    modal: true,
    padding: 5,
    initComponent: function () {
        this.items = [
            new jun.InputCustomerGridTouch({
                anchor: '100% 100%',
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.InputCustomerWinTouch.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCloseclick, this);
    },
    onbtnCloseclick: function () {
        this.close();
        jun.Sales_barcodeSetFocus();
    }
});
jun.InputCustomerGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Customers",
    id: 'docs-jun.InputCustomerGrid-Touch',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 30px; padding-top: 4px';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 100,
            filter: {xtype: "textfield", height: 30}
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 250,
            filter: {xtype: "textfield", height: 30}
        },
        {
            header: 'Telephone',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield", height: 30}
        },
        {
            header: 'Date OF Birth',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_lahir',
            width: 100,
            filter: {xtype: "textfield", height: 30}
        }
    ],
    initComponent: function () {
        this.store = jun.rztCustomersSalesCmp;
        this.store.baseParams = {mode: "grid", limit: 20};
        this.store.reload();
        this.store.baseParams = {};
        jun.InputCustomerGridTouch.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowclick', function (grid, rowIndex, e) {
            this.inputCustomer();
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    inputCustomer: function () {
        var data = this.record.data;
        jun.Sales_setCustomer(data.customer_id, data.nama_customer);
        var win = Ext.getCmp('form-InputCustomerWin-Touch');
        var panel = Ext.getCmp('form-Salestrans-gridHistory');
        panel.load({
            url: 'customers/SimpleHistory/nobase/7559KDR01',
            scripts: true,
            params: {
                nobase: '7559KDR01',
                height: panel.getHeight() - 100
            }
        });
        win.onbtnCloseclick();
    }
});
jun.InputDoctorWinTouch = Ext.extend(Ext.Window, {
    title: 'Doctor',
    id: 'form-InputDoctorWin-Touch',
    width: 550,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    initComponent: function () {
        this.items = [
            new jun.InputDoctorGridTouch({
                anchor: '100% 100%',
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'No Doctor',
                    ref: '../btnClear'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.InputDoctorWinTouch.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCloseclick, this);
        this.btnClear.on('click', this.onbtnClearClick, this);
    },
    onbtnClearClick: function () {
        Ext.getCmp('doktersales_id').reset();
        Ext.getCmp('dokter_id_id').reset();
        this.onbtnCloseclick();
    },
    onbtnCloseclick: function () {
        this.close();
        jun.Sales_barcodeSetFocus();
    }
});
jun.InputDoctorGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Doctor",
    id: 'docs-jun.InputDoctorGridTouch-Touch',
    iconCls: "silk-grid",
    autoHeight: true,
    viewConfig: {
        forceFit: true,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 30px; padding-top: 4px';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doctor name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_dokter',
            width: 250
        }
    ],
    initComponent: function () {
        this.store = jun.rztDokterCmp;
        jun.InputDoctorGridTouch.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowclick', function (grid, rowIndex, e) {
            this.setComboDoctorValue();
            var win = Ext.getCmp('form-InputDoctorWin-Touch');
            win.onbtnCloseclick();
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    setComboDoctorValue: function () {
        Ext.getCmp('doktersales_id').setValue(this.record.get('nama_dokter'));
        Ext.getCmp('dokter_id_id').setRawValue(this.record.get('dokter_id'));
    }
});
jun.InputKetTransWinTouch = Ext.extend(Ext.Window, {
    title: 'Keterangan Transaksi',
    id: 'form-InputKetTransWin-Touch',
    width: 550,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    initComponent: function () {
        this.items = [
            new jun.InputKetTransGridTouch({
                anchor: '100% 100%',
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Tanpa Keterangan',
                    ref: '../btnClear'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.InputKetTransWinTouch.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCloseclick, this);
        this.btnClear.on('click', this.onbtnClearClick, this);
    },
    onbtnClearClick: function () {
        Ext.getCmp('KetTrans_nama_id').reset();
        Ext.getCmp('akronim_id').reset();
        this.onbtnCloseclick();
    },
    onbtnCloseclick: function () {
        this.close();
        jun.Sales_barcodeSetFocus();
    }
});
jun.InputKetTransGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Keterangan Transaksi",
    id: 'docs-jun.InputKetTransGridTouch-Touch',
    iconCls: "silk-grid",
    autoHeight: true,
    viewConfig: {
        forceFit: true,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 30px; padding-top: 4px';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Keterangan Transaksi',
            sortable: true,
            resizable: true,
            dataIndex: 'keterangan',
            width: 250
        }
    ],
    initComponent: function () {
        this.store = jun.rztKetTransCmp;
        jun.InputKetTransGridTouch.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowclick', function (grid, rowIndex, e) {
            this.setComboKetTransValue();
            var win = Ext.getCmp('form-InputKetTransWin-Touch');
            win.onbtnCloseclick();
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    setComboKetTransValue: function () {
        Ext.getCmp('KetTrans_nama_id').setValue(this.record.get('keterangan'));
        Ext.getCmp('akronim_id').setRawValue(this.record.get('akronim_id'));
    }
});
jun.AddItemWinTouch = Ext.extend(Ext.Window, {
    title: 'Add Item',
    id: 'form-AddItemWin-Touch',
    width: 550,
    height: 450,
    layout: 'form',
    modal: true,
    padding: 5,
    initComponent: function () {
        this.items = [
            new jun.AddItemGridTouch({
                anchor: '100% 100%',
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AddItemWinTouch.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCloseclick, this);
    },
    onbtnCloseclick: function () {
        this.close();
        jun.Sales_barcodeSetFocus();
    }
});
jun.AddItemGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Add Item",
    id: 'docs-jun.AddItemGridTouch-Touch',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true,
        scrollOffset: 5,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 30px; padding-top: 4px';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 100,
            filter: {xtype: "textfield", height: 30}
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 250,
            filter: {xtype: "textfield", height: 30}
        }
    ],
    initComponent: function () {
        this.store = jun.rztBarangCmp;
        jun.AddItemGridTouch.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowclick', function (grid, rowIndex, e) {
            this.addNewItem();
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    addNewItem: function () {
        var record = this.sm.getSelected();
        jun.Sales_addNewItem(record.data);
        var win = Ext.getCmp('form-AddItemWin-Touch');
        win.onbtnCloseclick();
    }
});
jun.EditItemQtyWinTouch = Ext.extend(Ext.Window, {
    title: 'Edit Quantity',
    id: 'form-EditItemQtyWin-Touch',
    width: 283,
    height: 400,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'numericfield',
                fieldLabel: 'Quantity',
                hideLabel: true,
                name: 'qty',
                ref: 'qty',
                anchor: '100%',
                height: 30,
                readOnly: true,
                value: 0
            },
            {
                xtype: 'buttongroup',
                columns: 3,
                defaults: {
                    xtype: 'button',
                    scale: 'large',
                    width: 80,
                    height: 60,
                    style: {
                        margin: '2px'
                    }
                },
                items: [
                    {
                        text: '7',
                        ref: '../btn7'
                    },
                    {
                        text: '8',
                        ref: '../btn8'
                    },
                    {
                        text: '9',
                        ref: '../btn9'
                    },
                    {
                        text: '4',
                        ref: '../btn4'
                    },
                    {
                        text: '5',
                        ref: '../btn5'
                    },
                    {
                        text: '6',
                        ref: '../btn6'
                    },
                    {
                        text: '1',
                        ref: '../btn1'
                    },
                    {
                        text: '2',
                        ref: '../btn2'
                    },
                    {
                        text: '3',
                        ref: '../btn3'
                    },
                    {
                        text: '0',
                        ref: '../btn0'
                    },
                    {
                        text: 'Clear',
                        ref: '../btnClear'
                    },
                    {
                        text: 'Backspace',
                        ref: '../btnBackspace'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EditItemQtyWinTouch.superclass.initComponent.call(this);
        this.btn0.on('click', this.inputAngka, this);
        this.btn1.on('click', this.inputAngka, this);
        this.btn2.on('click', this.inputAngka, this);
        this.btn3.on('click', this.inputAngka, this);
        this.btn4.on('click', this.inputAngka, this);
        this.btn5.on('click', this.inputAngka, this);
        this.btn6.on('click', this.inputAngka, this);
        this.btn7.on('click', this.inputAngka, this);
        this.btn8.on('click', this.inputAngka, this);
        this.btn9.on('click', this.inputAngka, this);
        this.btnClear.on('click', this.onbtnClearclick, this);
        this.btnBackspace.on('click', this.onbtnBackspaceclick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCloseclick, this);
    },
    inputAngka: function (b, e) { //console.log(b);
        var txt = String(this.qty.getValue());
        if (txt == '0') txt = '';
        this.qty.setValue(txt + b.text);
    },
    onbtnClearclick: function () {
        this.qty.setValue(0);
    },
    onbtnBackspaceclick: function () { //console.log('Backspace');
        var txt = String(this.qty.getValue());
        if (txt.length == 0) return;
        txt = txt.substr(0, txt.length - 1);
        if (txt.length == 0) txt = '0';
        this.qty.setValue(txt);
    },
    onbtnSaveclick: function () {
        var qty = this.qty.getValue();
        if (qty == 0) jun.removeItem(this.record);
        else jun.Sales_updateQtyItem(this.record, qty);
        this.close();
    },
    onClose: function () {
        jun.Sales_barcodeSetFocus();
        //Ext.getCmp('win-Salestrans-Touch').statusActive = 2;
    },
    onbtnCloseclick: function () {
        this.close();
    }
});
jun.PaymentWinTouch = Ext.extend(Ext.Window, {
    title: 'Payment',
    id: 'form-PaymentWin-Touch',
    width: 500,
    height: 300,
    layout: 'fit',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    closeAction: 'hide',
    initComponent: function () {
        this.items = [
            new jun.PaymentGridTouch({
                //x: 5,
                //y: 362+50,
//                height: 200,
                anchor: '100% 100%',
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PaymentWinTouch.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    onClose: function () {
        Ext.getCmp('win-Salestrans-Touch').statusActive = 2;
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.PaymentGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Payment",
    id: 'docs-jun.PaymentGrid-Touch',
    iconCls: "silk-grid",
    autoHeight: true,
    viewConfig: {
        forceFit: true,
        scrollOffset: 5,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 36px;';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Del',
            width: 40,
            align: 'center',
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                //metaData.css += 'position: relative; top: -5px; overflow: visible;';
                var id = Ext.id();
                (function () {
                    var btn = new Ext.Button({
                        renderTo: id,
                        iconCls: 'silk13-cross',
                        scale: 'medium',
                        data: record,
                        listeners: {
                            click: function (b, e) {
                                jun.rztPayment.remove(this.data);
                            }
                        }
                    });
                }).defer(25);
                return '<span id="' + id + '"></span>';
                //return '<img alt="" src="css/silk_v013/icons/bin.png" class="x-action-col-icon x-action-col-0  " ext:qtip="Remove">';
            }
        },
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 200,
            renderer: jun.renderBank
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPayment;
        this.store.on('remove', jun.CustomerDisplay_Change, this);
        this.store.on('update', jun.CustomerDisplay_Change, this);
        this.store.on('add', jun.CustomerDisplay_Change, this);
        this.tbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Add Payment',
                    ref: '../btnAdd'
                }
            ]
        };
        jun.PaymentGridTouch.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.addPaymentWin, this);
//        this.btnDelete.on('Click', this.deletePayment, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    addPaymentWin: function () {
        var win = Ext.getCmp('form-Add-PaymentWin-Touch');
        if (win) {
            win.show();
        } else {
            var form = new jun.AddPaymentWinTouch({modez: 0});
            form.show();
        }
    },
    deletePayment: function () {
        var record = this.sm.getSelected();
        // Check is list selected
//        if (record == undefined) {
//            Ext.MessageBox.alert("Warning", "You have not selected a payment");
//            return;
//        }
//        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this payment?', function (btn) {
//            if (btn == 'no') {
//                return;
//            }
//            jun.rztPayment.remove(record);
//        }, this);
    }
});
jun.NewAddPaymentWinTouch = Ext.extend(Ext.Window, {
    title: 'Add Payment',
    id: 'form-Add-PaymentWin-Touch',
    width: 500,
    height: 400,
    layout: 'fit',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    //defaultButton: 'paymentbankid',
    closeAction: 'hide',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;',
                id: 'form-Add-Payment-Touch',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        hidden: true,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmp,
                        ref: '../bank',
                        id: 'paymentbankid',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Payment Method',
                        hideLabel: false,
                        name: 'nama_bank',
                        id: 'nama_bankid',
                        ref: '../nama_bank',
                        enableKeyEvents: true,
                        maxLength: 16,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Card Number',
                        hideLabel: false,
                        name: 'card_number',
                        id: 'card_numberid',
                        ref: '../card_number',
                        enableKeyEvents: true,
                        maxLength: 16,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Card Type',
                        store: jun.rztCardCmp,
                        ref: '../card',
                        hiddenName: 'card_id',
                        valueField: 'card_id',
                        displayField: 'card_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Amount',
                        hideLabel: false,
                        name: 'amount',
                        id: 'amountid',
                        ref: '../amount',
                        maxLength: 30,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Payment',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AddPaymentWinTouch.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.bank.on('select', this.onBankChange, this);
        this.card_number.on("specialkey", this.onCardnumber, this);
        this.on("activate", this.onActive, this);
        this.on("hide", this.onClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onClose: function () {
        Ext.getCmp('form-PaymentWin-Touch').statusActive = 2;
    },
    onActive: function () {
        this.bank.focus(false, 50);
        this.btnDisabled(false);
    },
    onCardnumber: function (f, e) {
        if (e.getKey() == e.ENTER) {
            var val = this.card_number.getValue();
            if (val == "" || val == undefined) {
                return;
            }
            var p = new SwipeParserObj(val);
            this.card_number.setValue(p.account);
            Ext.getCmp('doc_refid').focus(false, 100);
        }
    },
    onBankChange: function () {
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        if (bank == SYSTEM_BANK_CASH) {
            this.card_number.reset();
            this.card.reset();
            this.card_number.setDisabled(true);
            this.card.setDisabled(true);
        } else {
            this.card_number.setDisabled(false);
            this.card.setDisabled(false);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        var card_number = this.card_number.getValue();
        var card_id = this.card.getValue();
        var amount = this.amount.getValue() == "" ? 0 : this.amount.getValue();
        var ulpt = 0;
        var c = jun.rztPayment.recordType,
            d = new c({
                bank_id: bank,
                amount: amount,
                ulpt: ulpt,
                card_number: card_number,
                card_id: card_id
            });
        jun.rztPayment.add(d);
        this.btnDisabled(false);
        Ext.getCmp('form-Add-Payment-Touch').getForm().reset();
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.AddPaymentWinTouch = Ext.extend(Ext.Window, {
    title: 'Add Payment',
    id: 'form-AddPaymentWin-Touch',
    width: 283,
    height: 420,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    step: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'panel',
                layout: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; border: none; padding-bottom: 10px;',
                items: [
                    {
                        xtype: 'label',
                        text: "Payment Method",
                        ref: '../labelPayment',
                        style: {
                            'font-size': '16px',
                            'font-weight': 'bold'
                        }
                    }
                ]
            },
            {
                xtype: 'hidden',
                name: "bank_id",
                ref: 'bank'
            },
            new jun.PaymentMethodGridTouch({
                //hidden: true,
                ref: 'methodGrid',
                anchor: '100%',
                height: 300,
                frameHeader: !1,
                header: !1
            }),
            jun.CardNumberPanel,
            {
                xtype: 'hidden',
                name: "card_id",
                ref: 'card'
            },
            new jun.CardTypeGridTouch({
                hidden: true,
                ref: 'cardTypeGrid',
                anchor: '100%',
                height: 300,
                frameHeader: !1,
                header: !1
            }),
            jun.AmountPanel
        ];
        this.fbar = {
            xtype: 'toolbar',
            defaults: {
                scale: 'large'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Next',
                    ref: '../btnNext'
                },
                {
                    xtype: 'button',
                    text: 'Save',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AddPaymentWinTouch.superclass.initComponent.call(this);
        this.bCard0.on('click', this.inputCardNumber, this);
        this.bCard1.on('click', this.inputCardNumber, this);
        this.bCard2.on('click', this.inputCardNumber, this);
        this.bCard3.on('click', this.inputCardNumber, this);
        this.bCard4.on('click', this.inputCardNumber, this);
        this.bCard5.on('click', this.inputCardNumber, this);
        this.bCard6.on('click', this.inputCardNumber, this);
        this.bCard7.on('click', this.inputCardNumber, this);
        this.bCard8.on('click', this.inputCardNumber, this);
        this.bCard9.on('click', this.inputCardNumber, this);
        this.bCardClear.on('click', this.clearCardNumber, this);
        this.bCardBackspace.on('click', this.backspaceCardNumber, this);
        this.btn0.on('click', this.inputAngka, this);
        this.btn1.on('click', this.inputAngka, this);
        this.btn2.on('click', this.inputAngka, this);
        this.btn3.on('click', this.inputAngka, this);
        this.btn4.on('click', this.inputAngka, this);
        this.btn5.on('click', this.inputAngka, this);
        this.btn6.on('click', this.inputAngka, this);
        this.btn7.on('click', this.inputAngka, this);
        this.btn8.on('click', this.inputAngka, this);
        this.btn9.on('click', this.inputAngka, this);
        this.btnClear.on('click', this.onbtnClearclick, this);
        this.btnBackspace.on('click', this.onbtnBackspaceclick, this);
        this.btnNext.on('click', this.onbtnNext, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.showStep();
    },
    showStep: function () {
        this.methodGrid.hide();
        this.cardNumberPanel.hide();
        this.cardTypeGrid.hide();
        this.amountPanel.hide();
        this.btnNext.setDisabled(true).hide();
        this.btnSaveClose.hide();
        switch (this.step) {
            case 1 :
                this.methodGrid.show();
                this.labelPayment.setText('Payment Method');
                break;
            case 2 :
                this.cardNumberPanel.show();
                this.btnNext.show();
                this.labelPayment.setText('Card Number');
                break;
            case 3 :
                this.cardTypeGrid.show();
                this.labelPayment.setText('Card Type');
                break;
            case 4 :
                this.amountPanel.show();
                this.btnSaveClose.show();
                this.labelPayment.setText('Amount');
                break;
        }
    },
    inputCardNumber: function (b, e) {
        var txt = String(this.card_number.getValue());
        txt = txt.split(" ").join("");
        txt += b.text;
        this.card_number.setValue(this.separateCardNumber(txt));
        this.cardNumberonChange();
    },
    clearCardNumber: function () {
        this.card_number.reset();
        this.cardNumberonChange();
    },
    backspaceCardNumber: function () {
        var txt = String(this.card_number.getValue());
        txt = txt.split(" ").join("");
        if (txt.length == 0) return;
        txt = txt.substr(0, txt.length - 1);
        this.card_number.setValue(this.separateCardNumber(txt));
        this.cardNumberonChange();
    },
    separateCardNumber: function (txt) {
        var newTxt = "", c = 0;
        for (i = 0; i < txt.length; i++) {
            c++;
            newTxt += txt.substr(i, 1);
            if (c == 4 && i < (txt.length - 1)) {
                newTxt += " ";
                c = 0;
            }
        }
        return newTxt;
    },
    cardNumberonChange: function (f) {
        if (this.card_number.getValue().trim() == "") this.btnNext.setDisabled(true);
        else this.btnNext.setDisabled(false);
    },
    onbtnNext: function () {
        this.step++;
        this.showStep();
    },
    inputAngka: function (b, e) {
        var txt = String(this.amount.getValue());
        if (txt == '0') txt = '';
        this.amount.setValue(txt + b.text);
    },
    onbtnClearclick: function () {
        this.amount.reset();
    },
    onbtnBackspaceclick: function () {
        var txt = String(this.amount.getValue());
        if (txt.length == 0) return;
        txt = txt.substr(0, txt.length - 1);
        if (txt.length == 0) txt = '0';
        this.amount.setValue(txt);
    },
    onClose: function () {
        jun.Sales_barcodeSetFocus();
        //Ext.getCmp('win-Salestrans-Touch').statusActive = 1;
    },
    onbtnSaveCloseclick: function () {
        var bank = this.bank.getValue();
        var card_number = this.card_number.getValue().split(" ").join("");
        var card_id = this.card.getValue();
        var amount = this.amount.getValue() == "" ? 0 : this.amount.getValue();
        var ulpt = 0;
        var c = jun.rztPayment.recordType,
            d = new c({
                bank_id: bank,
                amount: amount,
                ulpt: ulpt,
                card_number: card_number,
                card_id: card_id
            });
        jun.rztPayment.add(d);
        this.close();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.PaymentMethodGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Add Payment",
    id: 'docs-jun.PaymentMethodGrid-Touch',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true,
        scrollOffset: 5,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 30px; padding-top: 4px';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Payment Method',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_bank',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztBankCmp;
        jun.PaymentMethodGridTouch.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowclick', function (grid, rowIndex, e) {
            this.selectMethod();
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    selectMethod: function () {
        //jun.Sales_addNewItem(this.record.data);
        var win = Ext.getCmp('form-AddPaymentWin-Touch');
        win.bank.setValue(this.record.data.bank_id);
        if (win.bank.getValue() == SYSTEM_BANK_CASH) {
            win.step = 4;
        } else {
            win.step++;
        }
        win.showStep();
    }
});
jun.CardNumberPanel = {
    xtype: 'panel',
    hidden: true,
    layout: 'form',
    frame: false,
    bodyStyle: 'background-color: #E8E8E8; border: none;',
    ref: 'cardNumberPanel',
    items: [
        {
            xtype: 'textfield',
            hideLabel: true,
            ref: '../card_number',
            name: 'card_number',
            anchor: '100%',
            height: 30,
            readOnly: true
        },
        {
            xtype: 'buttongroup',
            columns: 3,
            defaults: {
                xtype: 'button',
                scale: 'large',
                width: 80,
                height: 60,
                style: {
                    margin: '2px'
                }
            },
            items: [
                {
                    text: '7',
                    ref: '../../bCard7'
                },
                {
                    text: '8',
                    ref: '../../bCard8'
                },
                {
                    text: '9',
                    ref: '../../bCard9'
                },
                {
                    text: '4',
                    ref: '../../bCard4'
                },
                {
                    text: '5',
                    ref: '../../bCard5'
                },
                {
                    text: '6',
                    ref: '../../bCard6'
                },
                {
                    text: '1',
                    ref: '../../bCard1'
                },
                {
                    text: '2',
                    ref: '../../bCard2'
                },
                {
                    text: '3',
                    ref: '../../bCard3'
                },
                {
                    text: '0',
                    ref: '../../bCard0'
                },
                {
                    text: 'Clear',
                    ref: '../../bCardClear'
                },
                {
                    text: 'Backspace',
                    ref: '../../bCardBackspace'
                }
            ]
        }
    ]
};
//jun.NARSgrid = Ext.extend(Ext.Panel, {
//    title: "NARS",
//    id: 'docs-jun.NARSgrid',
//    autoLoad: {
//        url: 'http://localhost:81/sync/',
//        scripts: true
//    }
//});
jun.PaymentTouchGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Payment",
    id: 'docs-jun.PaymentTouchGrid',
    viewConfig: {
        forceFit: true,
        scrollOffset: 5
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 100,
            renderer: jun.renderBank
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPayment;
        jun.PaymentTouchGrid.superclass.initComponent.call(this);
    }
});
jun.CardTypeGridTouch = Ext.extend(Ext.grid.GridPanel, {
    title: "Add Item",
    id: 'docs-jun.CardTypeGrid-Touch',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true,
        scrollOffset: 5,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 30px; padding-top: 4px';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Card Name',
            sortable: true,
            resizable: true,
            dataIndex: 'card_name',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztCardCmp;
        jun.CardTypeGridTouch.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowclick', function (grid, rowIndex, e) {
            this.selectCardType();
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    selectCardType: function () {
        //jun.Sales_addNewItem(this.record.data);
        var win = Ext.getCmp('form-AddPaymentWin-Touch');
        win.card.setValue(this.record.data.card_id);
        win.step++;
        win.showStep();
    }
});
jun.AmountPanel = {
    xtype: 'panel',
    hidden: true,
    layout: 'form',
    frame: false,
    bodyStyle: 'background-color: #E8E8E8; border: none;',
    ref: 'amountPanel',
    items: [
        {
            xtype: 'numericfield',
            hideLabel: true,
            ref: '../amount',
            name: 'amount',
            anchor: '100%',
            height: 30,
            value: 0,
            readOnly: true
        },
        {
            xtype: 'buttongroup',
            columns: 3,
            defaults: {
                xtype: 'button',
                scale: 'large',
                width: 80,
                height: 60,
                style: {
                    margin: '2px'
                }
            },
            items: [
                {
                    text: '7',
                    ref: '../../btn7'
                },
                {
                    text: '8',
                    ref: '../../btn8'
                },
                {
                    text: '9',
                    ref: '../../btn9'
                },
                {
                    text: '4',
                    ref: '../../btn4'
                },
                {
                    text: '5',
                    ref: '../../btn5'
                },
                {
                    text: '6',
                    ref: '../../btn6'
                },
                {
                    text: '1',
                    ref: '../../btn1'
                },
                {
                    text: '2',
                    ref: '../../btn2'
                },
                {
                    text: '3',
                    ref: '../../btn3'
                },
                {
                    text: '0',
                    ref: '../../btn0'
                },
                {
                    text: 'Clear',
                    ref: '../../btnClear'
                },
                {
                    text: 'Backspace',
                    ref: '../../btnBackspace'
                }
            ]
        }
    ]
};
//======================================================================
//fired when code customer is given
jun.Sales_getCustomer = function (c) {
    Ext.Ajax.request({
        url: 'Customers/Find',
        params: {customer: c},
        success: function (result, request) {
            var data = JSON.parse(result.responseText);
            if (data === null) { //no match
                Ext.getCmp('nama_customer_id').selectText();
                Ext.getCmp('nama_customer_id').markInvalid('Data Customer not found');
                return;
            }
            jun.Sales_setCustomer(data.customer_id, data.nama_customer);
        },
        failure: function () {
        }
    });
};
jun.Sales_setCustomer = function (id, name) {
    //set value
    Ext.getCmp('nama_customer_id').setValue(name);
    Ext.getCmp('customer_id_id').setValue(id);
    //show welcome
    jun.CustomerDisplay_WelcomeName(name);
    //field add item gets focus
    jun.Sales_barcodeSetFocus();
};
jun.Sales_getProduct = function (b) {
    Ext.Ajax.request({
        url: 'Barang/Find',
        params: {barang: b},
        success: function (result, request) {
            var data = JSON.parse(result.responseText);
            if (data === null) { //no match
                Ext.getCmp('sales_barcode_id').selectText();
                Ext.getCmp('sales_barcode_id').markInvalid('Data not found');
                return;
            }
            jun.Sales_addNewItem(data);
        },
        failure: function () {
        }
    });
};
jun.Sales_addNewItem = function (data) {
    //var barang_id = rcd.get('barang_id');
    var barang_id = data.barang_id;
    //kunci barang sama tidak boleh masuk dua kali
    // var a = jun.rztSalestransDetails.findExact("barang_id", barang_id);
    // if (a > -1) {
    //     Ext.MessageBox.alert("Error", "Item already inputted");
    //     return;
    // }
    var price = data.price;
    var qty = 1;
    var disc = 0;
    var disca = 0;
    var disc1 = 0;
    var ketpot = '';
    var beauty = '';
    var beauty2 = '';
    var disc_name = '';
    var bruto = round(price * qty, 2);
    var vat = jun.getTax(barang_id);
    var subtotal = bruto - disca;
    var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
    var totalpot = disca + discrp1;
    var total_with_disc = bruto - totalpot;
    var total = bruto - disca;
    var vatrp = round(total_with_disc * vat, 2);
    var c = jun.rztSalestransDetails.recordType,
        d = new c({
            barang_id: barang_id,
            qty: qty,
            price: price,
            disc: disc,
            discrp: disca,
            ketpot: ketpot,
            beauty_id: beauty,
            beauty2_id: beauty2,
            vat: vat,
            vatrp: vatrp,
            total_pot: totalpot,
            bruto: bruto,
            disc_name: disc_name,
            total: total,
            disc1: disc1,
            discrp1: discrp1,
            paket_trans_id: '',
            paket_details_id: ''
        });
    jun.rztSalestransDetails.add(d);
    jun.Sales_barcodeSetFocus();
};
jun.Sales_removeItem = function (record) {
    jun.rztSalestransDetails.remove(record);
    jun.Sales_barcodeSetFocus();
};
jun.Sales_increaseItem = function (record) {
    var qty = record.data.qty;
    ++qty;
    jun.Sales_updateQtyItem(record, qty);
    jun.Sales_barcodeSetFocus();
};
jun.Sales_decreaseItem = function (record) {
    var qty = record.data.qty;
    if (--qty < 1) qty = 1;
    jun.Sales_updateQtyItem(record, qty);
    jun.Sales_barcodeSetFocus();
};
jun.Sales_updateQtyItem = function (record, qty) {
    var price = record.data.price;
    var disca = record.data.discrp;
    var disc1 = record.data.discrp;
    var vat = record.data.vat;
    var bruto = round(price * qty, 2);
    var subtotal = bruto - disca;
    var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
    var totalpot = disca + discrp1;
    var total_with_disc = bruto - totalpot;
    var total = bruto - disca;
    var vatrp = round(total_with_disc * vat, 2);
    record.set('qty', qty);
    record.set('vatrp', vatrp);
    record.set('total_pot', totalpot);
    record.set('total', total);
    record.set('bruto', bruto);
    record.set('discrp1', discrp1);
    record.commit();
};
jun.Sales_barcodeSetFocus = function () {
    Ext.getCmp('sales_barcode_id').focus(false, 50);
    Ext.getCmp('sales_barcode_id').reset();
};
jun.CustomerDisplay_Welcome = function () {
    //console.log("Welcome To Naavagreen " +  name);
    twoRows("Welcome To", "Naavagreen");
};
jun.CustomerDisplay_WelcomeName = function (name) {
    //console.log("Welcome To Naavagreen " +  name);
    twoRows("Welcome To Naavagreen", name);
};
jun.CustomerDisplay_Total = function () {
    //console.log("Total " +  Ext.getCmp('totalid').getRawValue());
    twoRows("Total :", Ext.getCmp('totalid').getRawValue());
};
jun.CustomerDisplay_Change = function () {
    if (Ext.getCmp('kembaliid').getValue() < 0) {
        jun.CustomerDisplay_Total();
        return;
    }
    //console.log("Uang Kembali " +  Ext.getCmp('kembaliid').getRawValue());
    twoRows("Change :", Ext.getCmp('kembaliid').getRawValue());
};
jun.CustomerDisplay_Thank = function () {
    //console.log("Welcome To Naavagreen " +  name);
    twoRows("      Thankyou      ", "");
};