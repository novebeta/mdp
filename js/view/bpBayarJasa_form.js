jun.BpBayarJasaWin = Ext.extend(Ext.Window, {
    title: 'Pembayaran Jasa',
    modez: 1,
    width: 700,
    height: 440,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BpBayarJasa',
                labelWidth: 94,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    new jun.BpBayarJasaListGrid({
                        height: 247,
                        ref: '../BpBayarJasaListGrid',
                        frameHeader: !1,
                        header: !1,
                        style: 'margin-bottom:5px',
                    }),
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        maxLength: 600,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'hidden',
                        name: 'detil',
                        id: 'detilBpBayarJasaid',
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 0 0 5 0;',
                        margins: '5 0 5 0',
                        layout: 'column',
                        autoScroll: true,
                        border: false,
                        items: [
                            {
                                columnWidth: .15,
                                style: 'margin-top:2px;',
                                xtype: 'label',
                                text: 'Supplier :'
                            },
                            {
                                columnWidth: .35,
                                style: 'margin-bottom:5px;',
                                xtype: 'combo',
                                ref: '../../cmbsupp',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Supplier',
                                store: jun.rztBpSupp,
                                hiddenName: 'account_code',
                                valueField: 'account_code',
                                displayField: 'nama',
                                // width: 275
                            },
                            {
                                columnWidth: .15,
                                style: 'margin-left:5px;margin-top:2px;',
                                xtype: 'label',
                                text: 'Tanggal :'
                            },
                            {
                                columnWidth: .35,
                                style: 'margin-bottom:5px;',
                                xtype: 'xdatefield',
                                ref: '../../tgl',
                                fieldLabel: 'Tgl',
                                name: 'tgl',
                                id: 'tglid',
                                format: 'd M Y',
                                value: DATE_NOW
                                // width: 275
                            },
                            {
                                columnWidth: .15,
                                style: 'margin-top:2px;',
                                xtype: 'label',
                                text: 'Total :'
                            },
                            {
                                columnWidth: .35,
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Total',
                                //hidden:true,
                                id: 'totalBpBayarJasaid',
                                name: 'total',
                                ref: '../../total',
                                maxLength: 30,
                                readOnly: true,
                                // width: 275
                            },
                            {
                                columnWidth: .15,
                                style: 'margin-left:5px;margin-top:2px;',
                                xtype: 'label',
                                text: 'Kas/Bank :',
                            },
                            {
                                columnWidth: .35,
                                xtype: 'combo',
                                //typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kas/Bank',
                                store: jun.rztBankBp,
                                hiddenName: 'bank_id',
                                valueField: 'bank_id',
                                displayField: 'nama_bank',
                                anchor: '100%'
                            },
                            {
                                columnWidth: .15,
                                style: 'margin-top:5px;',
                                xtype: 'label',
                                text: 'Total Bayar:'
                            },
                            {
                                columnWidth: .35,
                                xtype: 'numericfield',
                                style: 'margin-top:5px;text-align:right;',
                                hideLabel: false,
                                fieldLabel: 'Total',
                                //hidden:true,
                                id: 'totalBpBayarUserJasaid',
                                name: 'total_bayar',
                                ref: '../../total_bayar',
                                maxLength: 30,
                                // width: 275
                            },
                            // {
                            //     columnWidth: .15,
                            //     style: 'margin-top:2px;',
                            //     xtype: 'label',
                            //     text: 'PPH 23 :',
                            // },
                            // {
                            //     columnWidth: .35,
                            //     xtype: 'radiogroup',
                            //     fieldLabel: 'PPH 23',
                            //     items: [
                            //         {boxLabel: 'Yes', name: 'pph23', inputValue: 1},
                            //         {boxLabel: 'No', name: 'pph23', inputValue: 0, checked: true},
                            //     ]
                            // }
                        ]
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BpBayarJasaWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.cmbsupp.on('select', this.onCmbSelect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onCmbSelect: function (c, r, i) {
        jun.rztBpBayarJasaList.load({params: {account_code: r.data.account_code}});
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'BpBayarJasa/create/';
        var total = parseFloat(this.total.getValue());
        var total_bayar = parseFloat(this.total_bayar.getValue());
        if(total_bayar > total){
            Ext.Msg.alert('Failure', 'Total bayar lebih besar dari total.');
            this.btnDisabled(false);
            return;
        }
        Ext.getCmp('form-BpBayarJasa').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            // params: {
            //     detil: Ext.encode(Ext.pluck(
            //         this.BpBayarJasaListGrid.store.data.items, "data")),
            //     id: this.idkas,
            //     mode: this.modez
            // },
            success: function (f, a) {
                jun.rztBpBayarJasa.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BpBayarJasa').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});