jun.CustomerBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CustomerBpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomerBpStoreId',
            url: 'CustomerBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_bp_id'},
                {name: 'nama_customer'},
                {name: 'ktp'},
                {name: 'npwp'},
                {name: 'email'},
                {name: 'telp'},
                {name: 'alamat'},
                {name: 'city'},
            ]
        }, cfg));
    }
});
jun.rztCustomerBp = new jun.CustomerBpstore();
jun.rztCustomerBpCmp = new jun.CustomerBpstore();
jun.rztCustomerBpLib = new jun.CustomerBpstore();
//jun.rztCustomerBp.load();
