jun.BpPartsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "BpParts",
    id: 'docs-jun.BpPartsGrid',
    // iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode',
            // sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 100
        },
        {
            header: 'Nama',
            // sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Qty',
            // sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Harga',
            // sortable: true,
            resizable: true,
            dataIndex: 'harga',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            // sortable: true,
            resizable: true,
            dataIndex: 'total_parts_line',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Posting',
            // sortable: true,
            // hidden: this.modez !== 3,
            resizable: true,
            dataIndex: 'posting',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'SUDAH'
                } else {
                    return 'BELUM'
                }
            }
        },
        {
            header: 'Hpp',
            // sortable: true,
            resizable: true,
            dataIndex: 'hpp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'PPN',
            // sortable: true,
            resizable: true,
            dataIndex: 'ppnout',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
    ],
    initComponent: function () {
        console.log(this.modez);
        this.store = jun.rztBpParts;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            hidden: (this.modez === 4 || this.modez === 5),
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Kode :'
                        },
                        // {
                        //     xtype: 'uctextfield',
                        //     ref: '../../kode',
                        //     width: 75,
                        //     style: "margin-bottom:2px"
                        // },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode'
                            ],
                            mode: 'local',
                            store: jun.rztBpSparepartsKode,
                            valueField: 'sparepart_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode}</td><td>{nama}</td>',
                                '</tr></tpl></tbody></table>'),
                            allowBlank: false,
                            listWidth: 500,
                            width: 250,
                            hideTrigger: true,
                            id: 'bp-parts-kode-id',
                            displayField: 'kode',
                            ref: '../../kode',
                            style: "margin-bottom:2px"
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            //id: 'totalid',
                            ref: '../../qty',
                            width: 100,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Part :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'nama'
                            ],
                            mode: 'local',
                            store: jun.rztBpSparepartsNama,
                            valueField: 'sparepart_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode}</td><td>{nama}</td>',
                                '</tr></tpl></tbody></table>'),
                            allowBlank: false,
                            listWidth: 500,
                            width: 250,
                            hideTrigger: true,
                            id: 'bp-parts-nama-id',
                            displayField: 'nama',
                            ref: '../../item',
                            style: "margin-bottom:2px"
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            //id: 'kreditid',
                            ref: '../../harga',
                            width: 100,
                            value: 0,
                            enableKeyEvents: true
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        },
                        {
                            xtype: 'button',
                            text: 'Post',
                            hidden: (this.modez !== 3),
                            height: 44,
                            ref: '../../btnPost'
                        },
                        {
                            xtype: 'button',
                            text: 'Un-Post',
                            hidden: (this.modez !== 3) || !UNPOST_PARTS,
                            height: 44,
                            ref: '../../btnUnPost'
                        }
                    ]
                }
            ]
        };
        jun.BpPartsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnPost.on('Click', this.postRec, this);
        this.btnUnPost.on('Click', function () {
            Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin unpost data ini?", this.unpostRec, this);
        }, this);
        this.kode.on('select', function (c, r, i) {
            this.item.setValue(r.data.sparepart_id);
        }, this);
        this.item.on('select', function (c, r, i) {
            this.kode.setValue(r.data.sparepart_id);
        }, this);
        this.on('afterrender', function (c, r, i) {
            if(parseFloat(this.modez) < 4) {
                Ext.getCmp('bp-parts-kode-id').wrap.dom.style.cssText = '';
                Ext.getCmp('bp-parts-nama-id').wrap.dom.style.cssText = '';
            }
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    postRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih part.");
            return;
        }
        if (record.data.posting != null) {
            Ext.MessageBox.alert("Warning", "Data sudah di posting.");
            return;
        }
        var form = new jun.PostingWin({data:record});
        form.show(this);
    },
    unpostRec: function (a) {
        if (a === "no") return;
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih part.");
            return;
        }
        if (record.data.posting === null) {
            Ext.MessageBox.alert("Warning", "Data belum di posting.");
            return;
        }
        Ext.Ajax.request({
            url: 'bpParts/UnPosting/id/' + record.data.bp_parts_id,
            method: 'POST',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    jun.rztBpParts.reload();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        this.btnPost.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function (s) {
        this.kode.setRawValue();
        this.item.setRawValue();
        this.qty.reset();
        this.harga.reset();
        this.btnDisable(false);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            if (record.data.posting != null) {
                Ext.MessageBox.alert("Warning", "Part tidak bisa diedit karena sudah di posting.");
                return;
            }
            this.kode.setRawValue(record.data.kode);
            this.item.setRawValue(record.data.nama);
            this.qty.setValue(record.data.qty);
            this.harga.setValue(record.data.harga);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        this.btnDisable(true);
        var kode = this.kode.getRawValue();
        var nama = this.item.getRawValue();
        if (!nama) {
            Ext.MessageBox.alert("Error", "Nama harus diisi.");
            // this.btnDisable(false);
            return;
        }
        var qty = parseFloat(this.qty.getValue());
        var harga = parseFloat(this.harga.getValue());
        var total = qty * harga;
        this.save({
            kode: kode,
            nama: nama,
            qty: qty,
            hpp: 0,
            harga: harga,
            total_parts_line: total
        });
    },
    save: function (data) {
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('kode', data.kode);
            record.set('nama', data.nama);
            record.set('qty', data.qty);
            record.set('harga', data.harga);
            record.set('total_parts_line', data.total_parts_line);
            record.commit();
            Ext.Ajax.request({
                url: 'bpParts/update/id/' + record.data.bp_parts_id,
                method: 'POST',
                params: data,
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        } else {
            var c = this.store.recordType,
                d = new c(data);
            this.store.add(d);
            if (this.modez == 3) {
                data.body_paint_id = this.body_paint_id;
                Ext.Ajax.request({
                    url: 'bpParts/create/',
                    method: 'POST',
                    params: data,
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        }
        this.resetForm();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin menghapus data ini?", this.deleteRecYes, this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
            return;
        }
        if (this.modez == 3) {
            Ext.Ajax.request({
                url: 'bpParts/delete/id/' + b.json.bp_parts_id,
                method: 'POST',
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    jun.rztBpParts.reload();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        } else {
            this.store.remove(b);
        }
    }
})
