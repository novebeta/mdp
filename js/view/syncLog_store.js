jun.SyncLogstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.SyncLogstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SyncLogStoreId',
            url: 'SyncLog',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'sync_id'},
                {name:'user'},
                {name:'store'},
                {name:'tdate'},
                {name:'status'},
                {name:'diff'},
                {name:'note'},
                {name:'action'},
                {name:'timestamps'}
            ]
        }, cfg));
    }
});
jun.rztSyncLog = new jun.SyncLogstore();
//jun.rztSyncLog.load();
