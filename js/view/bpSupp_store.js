jun.BpSuppstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpSuppstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpSuppStoreId',
            url: 'BpSupp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'account_code'},
                {name: 'nama'},
            ]
        }, cfg));
    }
});
jun.rztBpSupp = new jun.BpSuppstore();
jun.rztBpSupp.load();
