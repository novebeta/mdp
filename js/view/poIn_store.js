jun.PoInstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PoInstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PoInStoreId',
            url: 'PoIn',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_id'},
                {name: 'doc_ref'},
                {name: 'nama'},
                {name: 'id_user'},
                {name: 'tdate'},
                {name: 'tgl'},
                {name: 'note'},
                {name: 'total'},
                {name: 'supplier_id'},
                {name: 'status'},
                {name: 'tgl_delivery'},
                {name: 'termofpayment'},
                {name: 'sub_total'},
                {name: 'total_disc_rp'},
                {name: 'ship_to_company'},
                {name: 'ship_to_address'},
                {name: 'ship_to_city'},
                {name: 'ship_to_country'},
                {name: 'ship_to_phone'},
                {name: 'ship_to_fax'},
                {name: 'nama_rek'},
                {name: 'no_rek'},
                {name: 'bank_rek'},
                {name: 'divisi'},
                {name: 'doc_ref_pr'},
                {name: 'supp_nama'},
                {name: 'supp_company'},
                {name: 'supp_address'},
                {name: 'supp_city'},
                {name: 'supp_country'},
                {name: 'supp_phone'},
                {name: 'supp_fax'},
                {name: 'supp_email'},
                {name: 'ppn'},
                {name: 'acc_charge'},
                {name: 'total_pph_rp'},
                {name: 'disc', type: 'float'},
                {name: 'total_dpp', type: 'float'},
                {name: 'tax_rp'}
            ]
        }, cfg));
    }
});
jun.rztPoIn = new jun.PoInstore({baseParams: {mode: 'grid'}});
jun.rztPoInCmp = new jun.PoInstore({baseParams: {mode: 'grid'}});
jun.rztPoInReceiveCmp = new jun.PoInstore({baseParams: {mode: 'grid'}});
//jun.rztPoIn.load();
