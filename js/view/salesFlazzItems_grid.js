jun.SalesFlazzItemsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SalesFlazzItems",
    id: 'docs-jun.SalesFlazzItemsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header:'Nama',
            sortable:true,
            resizable:true,
            dataIndex:'barang_id',
            width:100,
            renderer: jun.renderBarangFlazz
        },
        {
            header:'Qty',
            sortable:true,
            resizable:true,
            dataIndex:'qty',
            width:100
        },
        {
            header:'Sebelum PPN',
            sortable:true,
            resizable:true,
            dataIndex:'harga',
            width:100
        },
        {
            header:'PPN',
            sortable:true,
            resizable:true,
            dataIndex:'vatrp',
            width:100
        },
        {
            header:'Total',
            sortable:true,
            resizable:true,
            dataIndex:'total',
            width:100
        },
        {
            header:'Harga Beli',
            sortable:true,
            resizable:true,
            dataIndex:'harga_beli',
            width:100
        },

        /*
             {
header:'total_pot',
sortable:true,
resizable:true,
dataIndex:'total_pot',
width:100
},
                {
header:'total_discrp1',
sortable:true,
resizable:true,
dataIndex:'total_discrp1',
width:100
},
                {
header:'vatrp',
sortable:true,
resizable:true,
dataIndex:'vatrp',
width:100
},
        */
    ],
    initComponent: function () {
        this.store = jun.rztSalesFlazzItems;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'Produk',
                            ref: '../../barang',
                            store: jun.rztBarangFlazzAll,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            displayField: 'nama_barang',
                            colspan: 3,
                            // readOnly: true,
                            width: 166,
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0Sbml PPN :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'sblmppnid',
                            ref: '../../sblmppn',
                            width: 95,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0Total :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'totallineid',
                            enableKeyEvents: true,
                            ref: '../../totalline',
                            width: 95,
                            value: 0,
                            minValue: 0
                        },

                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            enableKeyEvents: true,
                            width: 40,
                            value: 1,
                            minValue: 0
                        },

                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'hargaid',
                            ref: '../../harga',
                            width: 80,
                            value: 1,
                            minValue: 0
                        },

                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0PPN :'
                        },

                        {
                            xtype: 'numericfield',
                            id: 'ppnlineid',
                            ref: '../../ppnline',
                            width: 95,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0Harga Beli :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'hargabelilineid',
                            ref: '../../hargabeliline',
                            width: 95,
                            value: 0,
                            minValue: 0
                        },
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalesFlazzItemsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        // this.barang.on('Change', this.onItemChange, this);
        // this.harga.on('keyup', this.onDiscChange, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.totalline.on('keyup', this.onDiscChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onItemChange: function () {
        var barang_id = this.barang.getValue();
        var barang = jun.getBarang(barang_id);
        // this.sat.setText(barang.data.sat);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onDiscChange: function (a, e) {

        var total = parseFloat(this.totalline.getValue());
        // var price = parseFloat(this.harga.getValue());
        var qty = parseFloat(this.qty.getValue());
        var barang_id = this.barang.getValue();
        var barang = jun.getBarangFlazz(barang_id);
        var bruto = total;
        if (barang.data.ppn > 0) {
            bruto = round2(total / PEMBAGI_PPN);
        }
        var vatrp = total - bruto;
        var harga = round2(bruto / qty);

        // vatrp = parseFloat(this.vatrp.getValue());
        // var bruto = round(price * qty, 2);
        // // var vatrp = (vat/100) * bruto;
        // var total = bruto + vatrp;
        this.sblmppn.setValue(bruto);
        this.ppnline.setValue(vatrp);
        this.harga.setValue(harga);
        // this.total.setValue(total);
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztSalesFlazzItems.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return
            }
        }
        this.onDiscChange();
        var qty = parseFloat(this.qty.getValue());
        var harga = parseFloat(this.harga.getValue());
        var harga_beli = parseFloat(this.hargabeliline.getValue());
        var vatrp = parseFloat(this.ppnline.getValue());
        var total = parseFloat(this.totalline.getValue());
        var bruto = parseFloat(this.sblmppn.getValue());

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('harga', harga);
            record.set('bruto', bruto);
            record.set('vatrp', vatrp);
            record.set('total', total);
            record.set('harga_beli', harga_beli);
            record.commit();
        } else {
            var c = jun.rztSalesFlazzItems.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    harga: harga,
                    bruto: bruto,
                    vatrp: vatrp,
                    total: total,
                    harga_beli: harga_beli,
                });
            jun.rztSalesFlazzItems.add(d);
        }
        // this.store.reset();
        this.barang.reset();
        this.qty.reset();
        this.harga.reset();
        this.hargabeliline.reset();
        this.ppnline.reset();
        this.totalline.reset();
        this.sblmppn.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            this.harga.setValue(record.data.harga);
            this.ppnline.setValue(record.data.vatrp);
            this.totalline.setValue(record.data.total);
            this.sblmppn.setValue(record.data.bruto);
            this.hargabeliline.setValue(record.data.harga_beli);
            btn.setText("Save");
            this.btnDisable(true);
            this.onItemChange();
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
})
