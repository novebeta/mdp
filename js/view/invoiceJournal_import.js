jun.ImportInvoiceJournal = Ext.extend(Ext.Window, {
    title: "Import Invoice Journal",
    iconCls: "silk13-report",
    modez: 1,
    width: 500,
    height: 400,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
//        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
//            jun.rztChartMasterCmp.load();
//        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 250,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg',
                    style: {
                        'fontFamily': 'courier new',
                        'fontSize': '12px'
                    }
                }
            },
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportInvoiceJournal",
                border: !1
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportInvoiceJournal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
            return;
        }
        jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckImportInvoiceJournalWin({generateQuery: 0});
        win.show(this);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT INVOICE JOURNAL -----');
        this.rowLoopCounter = 0;
        this.headerCounter = 0;
        this.dataSubmit = null;
        this.arrData = jun.dataStockOpname[jun.namasheet];
        this.loopPost();
    },
    headerCounter: 0,
    dataSubmit: null,
    arrData: null,
    loopPost: function () {
        if (this.rowLoopCounter >= this.arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT INVOICE JOURNAL SELESAI -----'
                + '\nJumlah Baris : ' + this.rowLoopCounter
                + '\nJumlah data : ' + this.headerCounter
                + '\n-------------------------------------------\n');
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            this.arrData = null;
            jun.dataStockOpname = null;
            return;
        }
        var d = this.arrData[this.rowLoopCounter];
        if (d.jenis_baris == 'H') {
            d.supplier_name = d.supplier_name.trim();
            /*
             * Header
             */
            this.headerCounter++;
            this.dataSubmit = {};
            this.writeLog('\n============== Data ke- ' + this.headerCounter + ' ==============\n');
            this.loopLog('Data Header nomor : ' + this.headerCounter);
            //Supplier
            var supplier = jun.rztSupplierCmp.find('supplier_name', d.supplier_name);
            if (supplier < 0) {
                this.writeLog('\n***** GAGAL ****\nSupplier tidak ditemukan.');
                this.btnSave.setDisabled(false);
                //this.nextRow();
                return;
            }
            var supplier_id = jun.rztSupplierCmp.getAt(supplier).get('supplier_id');
            this.dataSubmit['mode'] = 0;
            this.dataSubmit['id'] = '';
            this.dataSubmit['doc_ref_other'] = d.no_Invoice;
            this.dataSubmit['supplier_id'] = supplier_id;
            this.dataSubmit['tgl'] = d.date;
            this.dataSubmit['tgl_jatuh_tempo'] = d.jatuh_tempo;
            this.dataSubmit['store_kode'] = d.store;
            this.dataSubmit['total'] = d.total_hutang;
            this.dataSubmit['posting'] = d.posting;
            this.dataSubmit['detil'] = [];
        } else {
            /*
             * Detail
             */
            if (!this.dataSubmit) {
                this.nextRow();
                return;
            }
            //COA
//            var coa = jun.rztChartMasterCmp.find('account_name', d.d_account_code);
//            if(coa<0){
//                this.loopLog('GAGAL\nAccount code tidak ditemukan.');
//                this.nextRow();
//                return;
//            }
//            var account_code = jun.rztChartMasterCmp.getAt(coa).get('account_code');
            //Data detail
            this.dataSubmit['detil'].push('{"account_code":"' + d.account_code + '","memo_":"' + d.note + '","store_kode":"' + d.branch + '","debit":' + d.debit + ',"kredit":' + d.kredit + ',"amount":' + (d.debit > 0 ? d.debit : -d.kredit) + '}');
            this.loopLog('Data detail dari Header nomor : ' + this.headerCounter);
        }
        var nextRound = this.arrData[this.rowLoopCounter + 1];
        if (!nextRound || nextRound.jenis_baris == 'H') {
            this.writeLog('>>> Submit : Data Header nomor : ' + this.headerCounter);
            if (this.dataSubmit['detil'].length > 0) {
                this.dataSubmit['detil'] = '[' + this.dataSubmit['detil'].join(',') + ']';
            }
            Ext.getCmp('form-ImportInvoiceJournal').getForm().submit({
                url: 'InvoiceJournal/Create',
                timeOut: 1000,
                scope: this,
                params: this.dataSubmit,
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    var msg = '';
                    if (response.success) {
                        msg = response.msg;
                    } else {
                        msg = '***** GAGAL ****\n' + response.msg;
                        this.btnSave.setDisabled(false);
                    }
                    this.writeLog('<<< Result : dari Header nomor : ' + this.headerCounter + '\n' + msg);
                    if (response.success) this.nextRow();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Connection failure has occured.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        } else {
            this.nextRow();
        }
    },
    nextRow: function () {
        this.rowLoopCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('Baris ke- ' + (this.rowLoopCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --> ' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    }
});
jun.importKasStore = new Ext.data.JsonStore({
    storeId: 'jun.importKasStore',
    idProperty: 'tgl',
    fields: [
        'tgl',
        'payment',
        'amount',
        'no_kwitansi',
        'store',
        'type',
        'note',
        'd_item_name',
        'd_debit',
        'd_kredit',
        'd_total',
        'd_account_code',
        'd_store'
    ]
});
jun.CheckImportInvoiceJournalWin = Ext.extend(Ext.Window, {
    title: "Data Stock Opname",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importKasStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode_barang', dataIndex: 'kode_barang', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztBarangLib.findExact('kode_barang', value) == -1)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: Ext.util.Format.numberRenderer("0,0")
                    }
                ]
            }
        ];
        jun.CheckImportInvoiceJournalWin.superclass.initComponent.call(this);
    }
});