jun.RangeDiskonstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.RangeDiskonstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RangeDiskonStoreId',
            url: 'RangeDiskon',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'range_diskon_id'},
                {name: 'nilai_awal'},
                {name: 'nilai_akhir'},
                {name: 'disc'},
                {name: 'store_kode'}
            ]
        }, cfg));
    }
});
jun.rztRangeDiskon = new jun.RangeDiskonstore();
//jun.rztRangeDiskon.load();
