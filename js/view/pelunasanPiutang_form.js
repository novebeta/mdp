jun.PelunasanPiutangWin = Ext.extend(Ext.Window, {
    title: 'Pelunasan Piutang',
    modez: 1,
    width: 590,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {

        jun.rztCustomersPiutangCmp.baseParams = {
            customer_id: POSCUSTOMERDEFAULT
        };
        jun.rztCustomersPiutangCmp.load();
        jun.rztCustomersPiutangCmp.baseParams = {};

        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PelunasanPiutang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'tgl',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        width: 175,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Receipt No:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_bukti',
                        ref: '../no_bukti',
                        maxLength: 50,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Suplier:",
                        x: 295,
                        y: 35
                    },
                    {
                        ref: '../customer',
                        xtype: 'combo',
//                        //typeAhead: true,
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersPiutangCmp,
                        id: "customer_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                            '{alamat}',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 350,
                        width: 175,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Payment:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmpPusat,
                        ref: '../id_bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    new jun.PelunasanPiutangDetilGrid({
                        x: 5,
                        y: 95,
                        height: 315,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        width: 175,
                        name: 'total',
                        id: 'totalPelunasanPiutangid',
                        ref: '../total',
                        maxLength: 30,
                        value: 0,
                        x: 375,
                        y: 422
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose',
                    id:'btnSaveClosePelunasanPiutang'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PelunasanPiutangWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('select', this.onCustomerChange, this);
        this.on("close", this.onWinClose, this);
        //this.setDateTime();

    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onActivate: function () {
        if (this.modez == 1 || this.modez == 2) {
            Ext.getCmp("btnSaveClosePelunasanPiutang").setVisible(false);
        } else {
            Ext.getCmp("btnSaveClosePelunasanPiutang").setVisible(true);
        }
    },
    onWinClose : function() {
        jun.rztPelunasanPiutangDetil.removeAll();
        jun.rztFakturPiutang.removeAll();
//        Ext.getCmp('form-PelunasanUtang').getForm().reset();
    },
    onCustomerChange: function () {
        //this.doc_ref.reset();
        this.id_bank.reset();
        //this.no_bg_cek.reset();
        var customer_id = this.customer.getValue();

//        jun.rztPelunasanUtang.load();
//        jun.rztPelunasanUtangDetil.removeAll();
//        jun.rztPelunasanUtangDetil.baseParams = {
//            supplier_id: supplier_id
//        };
//        jun.rztPelunasanUtangDetil.load();
//        jun.rztPelunasanUtangDetil.baseParams = {};
        jun.rztFakturPiutang.baseParams = {
            customer_id: customer_id
        };
        jun.rztFakturPiutang.load();
        jun.rztFakturPiutang.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'PelunasanPiutang/create';
        Ext.getCmp('form-PelunasanPiutang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPelunasanPiutangDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPelunasanPiutang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PelunasanPiutang').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});
