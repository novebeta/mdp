jun.InvoiceUgstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.InvoiceUgstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InvoiceUgStoreId',
            url: 'InvoiceUg',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'invoice_ug_id'},
{name:'doc_ref'},
{name:'tgl_bayar'},
{name:'total'},
{name:'bank_id'},
{name:'id_user'},
                
            ]
        }, cfg));
    }
});
jun.rztInvoiceUg = new jun.InvoiceUgstore();
//jun.rztInvoiceUg.load();
