

jun.CustomersLinkageWin = Ext.extend(Ext.Window, {
    title: 'CustomersLinkage',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-CustomersLinkage',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'customers_id',
                        hideLabel:false,
                        //hidden:true,
                        name:'customers_id',
                        id:'customers_idid',
                        ref:'../customers_id',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    }, 
                    {
                        xtype: 'textfield',
                        fieldLabel: 'online_id',
                        hideLabel:false,
                        //hidden:true,
                        name:'online_id',
                        id:'online_idid',
                        ref:'../online_id',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },                                
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.CustomersLinkageWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'CustomersLinkage/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'CustomersLinkage/create/';
                }
             
            Ext.getCmp('form-CustomersLinkage').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztCustomersLinkage.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-CustomersLinkage').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
    
   
});


jun.CustomersVoucherWin = Ext.extend(Ext.Window, {
    title: 'Customers Voucher',
    modez:1,
    width: 800,
    height: 550,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-CustomersVoucher',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Customer Point',
                        hideLabel:false,
                        //hidden:true,
                        name:'point',
                        id:'point',
                        ref:'../point',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    } ,
                   /* {
                        xtype: 'textfield',
                        fieldLabel: 'online_id',
                        hideLabel:false,
                        //hidden:true,
                        name:'online_id',
                        id:'online_idid',
                        ref:'../online_id',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },   */  
                    {
                        xtype: 'label',
                        text: 'Available Voucher'
                    },
                    new jun.PosPickProductDataView({
                        id: 'voucher-avail',
                        anchor: '100%',
                        height: 200,
                        online_id: this.online_id,
                        store: this.store,
                        ref:'../voucher',
                        parent: this
                    }),
                    {
                        xtype: 'label',
                        text: 'My Voucher'
                    },
                    new jun.PosPickMyProductDataView({
                        id: 'myvoucher-avail',
                        anchor: '100%',
                        height: 200,
                        online_id: this.online_id,
                        vid : '',
                        ref:'../myvoucher',
                        store: this.store,
                        parent: this
                    })                        
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                /*{
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },*/
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.CustomersLinkageWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        //this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        /*if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }*/

        // this.voucher.store.load({
        //     params: {
        //         id: this.customer_id
        //     }
        // });

        this.loadVocher();
    },

    loadVocher: function(){
        Ext.Ajax.request({
            url: 'Customerslinkage/Vouchers/',
            method: 'POST',
            scope: this,
            params: {
                        id: this.customer_id
                    },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.point.setValue(response.result['my-point']);
                this.voucher.store.loadData(response.result['available-voucher']);
                this.myvoucher.store.loadData(response.result['my-voucher']);
                jun.datavoucheravailable = response.result['available-voucher'];
                jun.datamyvoucheravailable = response.result['my-voucher'];

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'CustomersLinkage/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'CustomersLinkage/create/';
                }
             
            Ext.getCmp('form-CustomersLinkage').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztCustomersLinkage.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-CustomersLinkage').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
    
   
});

//DETAIL VOUCHER
jun.DetailVoucherWin = Ext.extend(Ext.Window, {
    title: 'Buy Voucher',
    modez:1,
    width: 403,
    height: 421,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-DetailVoucher',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        width: '100',
                        height: 100,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {

                                width: 100,
                                height: 100,
                                html: [
                                    '<html>',
                                    '<head>',
                                    '</head>',
                                    '<body>',
                                    '<div id="docs-jun.voucherPreview" class="voucherPreview" >',
                                    '<div class="product-img">',
                                    '<img id="detailgmbvoucher" name="detailgmbvoucher" src="" alt="VOUCHER" width="100" height="100">',
                                    /*'<span class="price-tag">Exp: {voucher_expired}</span>',*/
                                    '</div>',
                                    '</div>',
                                    '</body>',
                                    '</html>'
                                ],
                                ref: '../detailimgvoucher',
                                id: 'detailimgvoucher',
                            },
                        ]
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Point',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_voucher_point',
                        id:'voucher_expired',
                        ref:'../detail_voucher_point',
                        maxLength: 255,
                        value: this.vp,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Name',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_voucher_name',
                        id:'detail_voucher_name',
                        ref:'../detail_voucher_name',
                        maxLength: 255,
                        value: this.vn,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Voucher Description',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_voucher_description',
                        id:'detail_voucher_description',
                        ref:'../detail_voucher_description',
                        //maxLength: 255,
                        height:170,
                        value: this.vd,
                        //allowBlank: ,
                        anchor: '100%'
                    },

/*
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Outlet Redeem',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_redeemable',
                        id:'detail_redeemable',
                        ref:'../detail_redeemable',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    },*/

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                /*{
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                */
                {
                    xtype: 'button',
                    text: 'Buy',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.DetailVoucherWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            //this.btnSave.setVisible(true);
        }


    },



    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        var id = this.oid;
        var vid = this.vid;

        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membeli voucher ini?', function(btn)
                {
                   if(btn === 'yes')
                   {
                        Ext.Ajax.request({
                            url: 'Customerslinkage/Voucherbuy',
                            method: 'POST',
                            scope: this,
                            params: {
                                id: id,
                                vid: vid,
                                store: STORE
                            },
                            success: function (f, a) {
                                var response = Ext.decode(f.responseText);
                                //this.parent.reloadVoucher();

                                alert(response.status + ' - ' +response.result['0']);

                            },
                            failure: function (f, a) {
                                switch (a.failureType) {
                                    case Ext.form.Action.CLIENT_INVALID:
                                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                        break;
                                    case Ext.form.Action.CONNECT_FAILURE:
                                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                                        break;
                                    case Ext.form.Action.SERVER_INVALID:
                                        Ext.Msg.alert('Failure', a.result.msg);
                                }
                            }
                        });
                    }
                });
        this.close();
    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }


});

//DETAIL MYVOUCHER
jun.DetailMyVoucherWin = Ext.extend(Ext.Window, {
    title: 'Voucher',
    modez:1,
    width: 403,
    height: 332,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        //oid:oid, vid:vid, vp:vp, vn: vn, vd:vd, vexp:vexp, vrp:vrp, vidr:vidr
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-DetailMyVoucher',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        width: '100',
                        height: 100,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {

                                width: 100,
                                height: 100,
                                html: [
                                    '<html>',
                                    '<head>',
                                    '</head>',
                                    '<body>',
                                    '<div id="docs-jun.myvoucherPreview" class="myvoucherPreview" >',
                                    '<div class="product-img">',
                                    '<img id="detailgmbmyvoucher" name="detailgmbmyvoucher" src="" alt="VOUCHER" width="100" height="100">',
                                    /*'<span class="price-tag">Exp: {voucher_expired}</span>',*/
                                    '</div>',
                                    '</div>',
                                    '</body>',
                                    '</html>'
                                ],
                                ref: '../detailimgmyvoucher',
                                id: 'detailimgmyvoucher',
                            },
                        ]
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Point',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_myvoucher_point',
                        id:'voucher_expired',
                        ref:'../detail_myvoucher_point',
                        maxLength: 255,
                        value: this.vp,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Name',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_myvoucher_name',
                        id:'detail_myvoucher_name',
                        ref:'../detail_myvoucher_name',
                        maxLength: 255,
                        value: this.vn,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Voucher Description',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_myvoucher_description',
                        id:'detail_myvoucher_description',
                        ref:'../detail_myvoucher_description',
                        maxLength: 255,
                        value: this.vd,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Expired',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'detail_myvoucher_expired',
                        id:'detail_myvoucher_expired',
                        ref:'../detail_myvoucher_expired',
                        maxLength: 255,
                        value: this.vexp,
                        //allowBlank: ,
                        anchor: '100%'
                    },

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                /*{
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                */
                {
                    xtype: 'button',
                    text: 'Use',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.DetailMyVoucherWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            //this.btnSave.setVisible(true);
        }


    },



    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        //this.close();
        var id = this.oid;
        var vid = this.vidr;

        Ext.Ajax.request({
            url: 'Customerslinkage/Otp/',
            method: 'POST',
            scope: this,
            params: {
                id: id,
                vid: vid,
                store: STORE
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                alert(response.status + ' - ' +response.messages['0']);
                //this.parent.loadVocher();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        this.close();
        Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.sendotp, this, true);
        /*Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menggunakan voucher ini?', function(btn)
        {
            if(btn === 'yes')
            {
                Ext.Ajax.request({
                    url: 'Customerslinkage/Voucheruse',
                    method: 'POST',
                    scope: this,
                    params: {
                        id: id,
                        vid: vid,
                        store: STORE,
                        otp:vid
                    },
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);
                        //this.parent.reloadVoucher();

                        alert(response.status + ' - ' +response.result['0']);

                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        });*/
        //this.close();
    },

    sendotp:function(btn, txt){
        if (btn != 'ok') {
            return;
        }
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan OTP yang dikirimkan.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }

        Ext.Ajax.request({
            url: 'Customerslinkage/Voucheruse/',
            method: 'POST',
            scope: this,
            params: {
                id: this.oid,
                vid: this.vidr,//rec.get('id_voucher_redemption'),
                store: STORE,
                otp: txt
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //this.parent.loadVocher();
                if(response.status != 'fail')
                {
                    var form = new jun.UsedVoucherWin({modez:0});
                    form.show();
                    Ext.getCmp('trx_id').setValue(response.result[0]['trx_id']);
                    Ext.getCmp('voucher_used').setValue(response.result[0]['voucher_used']);
                    Ext.getCmp('voucher_expired').setValue(response.result[0]['voucher_expired']);
                    Ext.getCmp('voucher_name').setValue(response.result[0]['voucher']['voucher_name']);
                    Ext.getCmp('outlet_name_pos').setValue(response.result[0]['outlet_redeem']['outlet_name_pos']);
                    //Ext.getCmp('qr_code').setValue(response.result[0]['qr_code']);
                    document.getElementById("qr").src = response.result[0]['qr_code'];
                    document.getElementById("gmbvoucher").src = response.result[0]['voucher']['url_photo'];

                    //this.setCookie('trxid',response.result[0]['trx_id']);

                    Ext.Ajax.request({
                        url: 'NsccVoucherOnline/Create/',
                        method: 'POST',
                        scope: this,
                        params: {
                            id: response.result[0]['trx_id'],
                            vid:response.result[0]['voucher']['id_voucher'],
                            vnama:response.result[0]['voucher']['voucher_name'],
                            vdesc:response.result[0]['voucher']['voucher_name'],
                            exp: response.result[0]['voucher_expired'],
                            tdate:response.result[0]['voucher_used'],
                            oid:response.result[0]['outlet_redeem']['id_outlet'],
                            oname:response.result[0]['outlet_redeem']['outlet_name_pos'],
                            ocode:response.result[0]['outlet_redeem']['outlet_code'],
                            oaddress:response.result[0]['outlet_redeem']['outlet_address'],
                            uid:response.result[0]['id_user']
                        },
                        success: function (f, a) {
                            var response = Ext.decode(f.responseText);
                        },
                        failure: function (f, a) {
                            switch (a.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', a.result.msg);
                            }
                        }
                    });
                }
                else {
                    alert(response.status + ' - ' +response.messages['0']);
                }



                //document.getElementById('myImage').src= response.result[0]['qr_code'];
                ///this.loadVocher();
                //this.close();

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }


});


//USED VOUCHER
jun.UsedVoucherWin = Ext.extend(Ext.Window, {
    title: 'Voucher',
    modez:1,
    width: 403,
    height: 438,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-UsedVoucher',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        width: '100',
                        height: 100,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {

                                width: 100,
                                height: 100,
                                html: [
                                    '<html>',
                                    '<head>',
                                    '</head>',
                                    '<body>',
                                    '<div id="docs-jun.usevoucherPreview" class="usevoucherPreview" >',
                                    '<div class="product-img">',
                                    '<img id="gmbvoucher" name="gmbvoucher" src="" alt="VOUCHER" width="100" height="100">',
                                    /*'<span class="price-tag">Exp: {voucher_expired}</span>',*/
                                    '</div>',
                                    '</div>',
                                    '</body>',
                                    '</html>'
                                ],
                                ref: '../imgvoucher',
                                id: 'imgvoucher',
                            },
                        ]
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Transaction ID',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'trx_id',
                        id:'trx_id',
                        ref:'../trx_id',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Name',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'voucher_name',
                        id:'voucher_name',
                        ref:'../voucher_name',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'label',
                        text: 'Voucher:'
                    },*/

                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Used',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'voucher_used',
                        id:'voucher_used',
                        ref:'../voucher_used',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Voucher Exp',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'voucher_expired',
                        id:'voucher_expired',
                        ref:'../voucher_expired',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Outlet Redeem',
                        hideLabel:false,
                        readOnly:true,
                        //hidden:true,
                        name:'outlet_name_pos',
                        id:'outlet_name_pos',
                        ref:'../outlet_name_pos',
                        maxLength: 255,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    /* {
                         xtype: 'textfield',
                         fieldLabel: 'online_id',
                         hideLabel:false,
                         //hidden:true,
                         name:'online_id',
                         id:'online_idid',
                         ref:'../online_id',
                         maxLength: 11,
                         //allowBlank: ,
                         anchor: '100%'
                     },   */
                    {
                        xtype: 'label',
                        text: 'QR Code:'
                    },
                    {
                        width: '100',
                        height: 100,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {

                                width: 100,
                                height: 100,
                                html: [
                                    '<html>',
                                    '<head>',
                                    '</head>',
                                    '<body>',
                                    '<div id="docs-jun.qrPreview" class="qrPreview" >',
                                    '<div class="product-img">',
                                    '<img id="qr" name="qr" src="" alt="QR" width="100" height="100">',
                                    /*'<span class="price-tag">Exp: {voucher_expired}</span>',*/
                                    '</div>',
                                    '</div>',
                                    '</body>',
                                    '</html>'
                                ],
                                ref: '../qr_code',
                                id: 'qr_code',
                            },
                        ]
                    },
                    /*new jun.QRVoucherDataView({
                        id: 'qr_code',
                        anchor: '100%',
                        height: 200,
                        //online_id: this.online_id,
                        store: this.store,
                        ref:'../qr_code',
                        parent: this
                    })*/
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                /*{
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },*/
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.UsedVoucherWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        //this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            //this.btnSave.setVisible(true);
        }


    },

    /*loadVocher: function(){
        Ext.Ajax.request({
            url: 'Customerslinkage/Vouchers/',
            method: 'POST',
            scope: this,
            params: {
                id: this.customer_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.point.setValue(response.result['my-point']);
                this.voucher.store.loadData(response.result['available-voucher']);
                this.myvoucher.store.loadData(response.result['my-voucher']);

                /!* this.point.setValue(response['my-point'])
                this.voucher.store.loadData(response['available-voucher']);

                this.myvoucher.store.loadData(response['my-voucher']); *!/
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },*/

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz;
        if(this.modez == 1 || this.modez== 2) {

            urlz= 'CustomersLinkage/update/id/' + this.id;

        } else {

            urlz= 'CustomersLinkage/create/';
        }

        Ext.getCmp('form-CustomersLinkage').getForm().submit({
            url:urlz,
            timeOut: 1000,
            scope: this,
            success: function(f,a){
                jun.rztCustomersLinkage.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-CustomersLinkage').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }


});
/*jun.PosPickProductDataView = Ext.extend(Ext.DataView, {
    tpl: new Ext.XTemplate(
        '<ul>',
            '<tpl for=".">',
                '<li class="posDataViewItem product">',
                    '<div class="product-img">',
                        '<img src="{photo}">',
                        '<span class="price-tag">Rp {[Ext.util.Format.number(values.price, "0,0.00")]}</span>',
                    '</div>',
                    '<div class="product-name">{voucher_name}</div>',
                '</li>',
            '</tpl>',
        '</ul>'
    ),
    cls: 'posPickProductDataView',
    itemSelector: 'li.posDataViewItem',
    singleSelect: true,
    autoScroll: true,
    initComponent: function () {
        this.store = new jun.CustomersVoucherstore();
        
        jun.PosPickProductDataView.superclass.initComponent.call(this);
        this.on('selectionchange', this.onSelectionchange, this);
    },
    onSelectionchange: function(c, selections)
    {

        var r = c.getSelectedRecords();
        if(!r.length) return;
        var rec = r[0];
        if( this.getId() == 'voucher-avail')

        /!*Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menggunakan voucher ini?', this);

        Ext.MessageBox.confirm('Pertanyaan', 'Are you sure ?', function(btn){
            if(btn === 'yes'){
                //some code
            }
            else{
                //some code
            }
        });*!/

        Ext.Ajax.request({
            url: 'Customerslinkage/Voucherbuy',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: rec.get('id_voucher'),
                store: this.store
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.parent.loadVocher();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        else
        Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);

    },

    buyVocer : function(btn) {

    },

    otp:function(b,t){
        if (btn != 'ok') {
            return;
        }
    }
});*/


/////////////VIEW

var datavocer = null;


/*var tpl = new Ext.XTemplate(
    '<p>Name: {name}</p>',
    '<p>Kids: ',
    '<tpl for="kids">',
    '<tpl if="age > 1">',
    '<p>{name}</p>',
    '</tpl>',
    '</tpl></p>'
);
tpl.overwrite(panel.body, data);*/
/*var tplava = new Ext.XTemplate(
    '<ul>',
    '<tpl for=".">',
    '<li class="posDataViewItem product">',
    '<div class="product-img">',
    '<img src="{photo}" alt="Voucher" width="100" height="100">',
    '<span class="price-tag">Point {voucher_point}</span>',
    '</div>',
    '<div class="product-name"><center>{voucher_name}<center></span></span></div>',
    '</li>',
    '</tpl>',
    '</ul>'
);
tplava.overwrite(panel.body, ,true);*/

//AVAILABLE VOUCHER
jun.PosPickProductDataView = Ext.extend(Ext.DataView, {
    tpl: new Ext.XTemplate(
        '<ul>',
        '<tpl for=".">',
            '<li class="posDataViewItem product">',
                '<div class="product-img">',
                    '<img src="{photo}" alt="Voucher" width="100" height="100">',
                    '<span class="price-tag">Point {voucher_point}</span>',
                '</div>',
                '<div class="product-name"><center>{voucher_name}<center></span></span></div>',
            '</li>',
        '</tpl>',
        '</ul>'
    ),
    cls: 'posPickProductDataView',
    itemSelector: 'li.posDataViewItem',
    singleSelect: true,
    autoScroll: true,
    initComponent: function () {
        this.store = new jun.CustomersVoucherstore();

        jun.PosPickProductDataView.superclass.initComponent.call(this);
        this.on('selectionchange', this.onSelectionchange, this);
    },



    reloadVoucher:function(){
        this.parent.loadVocher();
    },

    onSelectionchange: function(c, selections)
    {

        var r = c.getSelectedRecords();
        if(!r.length) return;
        var rec = r[0];
        var vid = rec.get('id_voucher');
        var id = this.online_id;

        var data = getVocer(jun.datavoucheravailable,rec.get('id_voucher'));

        var form = new jun.DetailVoucherWin({modez:0, oid:id, vid:data['id_voucher'], vp:data['voucher_point'], vn: data['voucher_name'], vd:data['description']});
        form.show();
        document.getElementById("detailgmbvoucher").src = data['photo'];
        /*Ext.getCmp('detail_voucher_point').setValue(data['voucher_point']);
        Ext.getCmp('detail_voucher_name').setValue(data['voucher_name']);
        Ext.getCmp('detail_voucher_description').setValue(data['voucher_description']);
        */


        /*Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membeli voucher ini?', function(btn)
        {
           if(btn === 'yes')
           {
                Ext.Ajax.request({
                    url: 'Customerslinkage/Voucherbuy',
                    method: 'POST',
                    scope: this,
                    params: {
                        id: id,
                        vid: rec.get('id_voucher'),
                        store: STORE
                    },
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);
                        //this.reloadVoucher();
                        alert(response.status + ' - ' +response.result['0']);

                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        });*/

        /*if( this.getId() == 'voucher-avail')
        {

        }*/

    },
    /*loadVocher: function(){
        Ext.Ajax.request({
            url: 'Customerslinkage/Vouchers/',
            method: 'POST',
            scope: this,
            params: {
                id: this.customer_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.point.setValue(response.result['my-point']);
                this.voucher.store.loadData(response.result['available-voucher']);
                this.myvoucher.store.loadData(response.result['my-voucher']);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },*/

});

//MY VOUCHER
jun.PosPickMyProductDataView = Ext.extend(Ext.DataView, {
    tpl: new Ext.XTemplate(
        /*'<tpl for=".">',*/
        '<ul>',
            '<tpl for=".">',
                '<li class="posDataViewItem product">',
                //'<tpl for="voucher">',
                '<div class="product-img">',
                    '<img src="{photo}" alt="Voucher" width="100" height="100">',
                    '<span class="price-tag">Exp: {voucher_expired}</span>',
                '</div>',
                '<div class="product-name"><center>{voucher_name}<center></span></span></div>',
                //'</tpl>',
                '</li>',
            '</tpl>',
        '</ul>'
        /*'</tpl>'*/
    )/*.overwrite(Ext.getBody(), data)*/,

    cls: 'posPickProductDataView',
    itemSelector: 'li.posDataViewItem',
    singleSelect: true,
    autoScroll: true,
    initComponent: function () {

        this.store = new jun.CustomersMyVoucherstore();

        jun.PosPickMyProductDataView.superclass.initComponent.call(this);

        //this.tpl.overwrite(tpl., data.voucher,true);

        this.on('selectionchange', this.onSelectionchange, this);
    },

    onSelectionchange: function(c, selections)
    {

        var r = c.getSelectedRecords();
        if(!r.length) return;
        var rec = r[0];

        var vidr = rec.get('id_voucher_redemption');
        var oid = this.online_id;

        var data = getMyVocer(jun.datamyvoucheravailable,vidr);

        var vn = data['voucher']['voucher_name'];
        var vp = data['voucher']['voucher_point'];
        var vd = data['voucher']['description'];
        var vid = data['voucher']['id_voucher'];
        var vphoto = data['voucher']['photo'];//"https://api.natashaskin.net//img/voucher/8521547007879.png";//data['voucher']['photo'];
        var vexp = data['voucher_expired'];
        var vidr = data['id_voucher_redemption'];
        var vrp = data['redemption_point'];

        var form = new jun.DetailMyVoucherWin({modez:0, oid:oid, vid:vid, vp:vp, vn: vn, vd:vd, vexp:vexp, vrp:vrp, vidr:vidr});
        form.show();
        document.getElementById("detailgmbmyvoucher").src = vphoto;

        /*Ext.Ajax.request({
            url: 'Customerslinkage/Otp/',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: rec.get('id_voucher_redemption'),
                store: STORE
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                alert(response.status + ' - ' +response.messages['0']);
                //this.parent.loadVocher();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);*/

        /*if( this.getId() == 'dd-avail')

        /!*Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menggunakan voucher ini?', this);

        Ext.MessageBox.confirm('Pertanyaan', 'Are you sure ?', function(btn){
            if(btn === 'yes'){
                //some code
            }
            else{
                //some code
            }
        });*!/

            Ext.Ajax.request({
                url: 'Customerslinkage/Voucherbuy',
                method: 'POST',
                scope: this,
                params: {
                    id: this.online_id,
                    vid: rec.get('id_voucher_redemption'),
                    store: this.store
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    this.parent.loadVocher();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        else
            Ext.Ajax.request({
                url: 'Customerslinkage/Otp',
                method: 'POST',
                scope: this,
                params: {
                    id: this.online_id,
                    vid: rec.get('id_voucher_redemption'),
                    store: this.store
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    //this.parent.loadVocher();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
            Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);
*/
    },

    otp:function(btn, txt){
        if (btn != 'ok') {
            return;
        }
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan OTP yang dikirimkan.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }

        Ext.Ajax.request({
            url: 'Customerslinkage/Voucheruse/',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: this.vid,//rec.get('id_voucher_redemption'),
                store: STORE,
                otp: txt
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.parent.loadVocher();
                if(response.status != 'fail')
                {
                    var form = new jun.UsedVoucherWin({modez:0});
                    form.show();
                    Ext.getCmp('trx_id').setValue(response.result[0]['trx_id']);
                    Ext.getCmp('voucher_used').setValue(response.result[0]['voucher_used']);
                    Ext.getCmp('voucher_expired').setValue(response.result[0]['voucher_expired']);
                    Ext.getCmp('voucher_name').setValue(response.result[0]['voucher']['voucher_name']);
                    Ext.getCmp('outlet_name_pos').setValue(response.result[0]['outlet_redeem']['outlet_name_pos']);
                    //Ext.getCmp('qr_code').setValue(response.result[0]['qr_code']);
                    document.getElementById("qr").src = response.result[0]['qr_code'];
                    document.getElementById("gmbvoucher").src = response.result[0]['voucher']['url_photo'];

                    //this.setCookie('trxid',response.result[0]['trx_id']);

                    Ext.Ajax.request({
                        url: 'NsccVoucherOnline/Create/',
                        method: 'POST',
                        scope: this,
                        params: {
                            id: response.result[0]['trx_id'],
                            vid:response.result[0]['voucher']['id_voucher'],
                            vnama:response.result[0]['voucher']['voucher_name'],
                            vdesc:response.result[0]['voucher']['voucher_name'],
                            exp: response.result[0]['voucher_expired'],
                            tdate:response.result[0]['voucher_used'],
                            oid:response.result[0]['outlet_redeem']['id_outlet'],
                            oname:response.result[0]['outlet_redeem']['outlet_name_pos'],
                            ocode:response.result[0]['outlet_redeem']['outlet_code'],
                            oaddress:response.result[0]['outlet_redeem']['outlet_address'],
                            uid:response.result[0]['id_user']
                        },
                        success: function (f, a) {
                            var response = Ext.decode(f.responseText);
                        },
                        failure: function (f, a) {
                            switch (a.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', a.result.msg);
                            }
                        }
                    });
                }
                else {
                    alert(response.status + ' - ' +response.messages['0']);
                }



                //document.getElementById('myImage').src= response.result[0]['qr_code'];
                ///this.loadVocher();
                //this.close();

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },
    /*loadVocher: function(){
        Ext.Ajax.request({
            url: 'Customerslinkage/Vouchers/',
            method: 'POST',
            scope: this,
            params: {
                id: this.customer_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.point.setValue(response.result['my-point']);
                this.voucher.store.loadData(response.result['available-voucher']);
                this.myvoucher.store.loadData(response.result['my-voucher']);

                /!* this.point.setValue(response['my-point'])
                this.voucher.store.loadData(response['available-voucher']);

                this.myvoucher.store.loadData(response['my-voucher']); *!/
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },*/
});

jun.CustomersVoucherstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.CustomersVoucherstore.superclass.constructor.call(this, Ext.apply({
            // url: 'Customerslinkage/Vouchers',
            // root: 'available-voucher',
            fields: [
                {name:'voucher_name'},
                {name:'voucher_point'},
                {name:'photo'},
                {name:'id_voucher'},
                {name:'voucher'},
                {name:'customer'},
                {name:'redeemable_at'},
            ]
        }, cfg));
    }
});
jun.rztCustomersVoucherstore = new jun.CustomersVoucherstore();

jun.CustomersMyVoucherstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.CustomersMyVoucherstore.superclass.constructor.call(this, Ext.apply({
            // url: 'Customerslinkage/Vouchers',
            // root: 'available-voucher',
            fields: [
                {name:'voucher_expired'},
                {name:'voucher_name'},
                {name:'voucher_point'},
                {name:'photo'},
                {name:'id_voucher_redemption'}
            ]
        }, cfg));
    }
});
jun.rztCustomersMyVoucherstore = new jun.CustomersMyVoucherstore();






//SUCCESS VOUCHER
jun.SuccessVoucherDataView = Ext.extend(Ext.DataView, {
    tpl: new Ext.XTemplate(
        /*'<tpl for=".">',*/
        '<ul>',
        '<tpl for=".">',
        '<li class="posDataViewItem product">',
        //'<tpl for="voucher">',
        '<div class="product-img">',
        '<img src="{photo}" alt="Voucher" width="100" height="100">',
        /*'<span class="price-tag">Exp: {voucher_expired}</span>',*/
        '</div>',
        '<div class="product-name"><center>{voucher_name}<center></span></span></div>',
        //'</tpl>',
        '</li>',
        '</tpl>',
        '</ul>'
        /*'</tpl>'*/
    )/*.overwrite(Ext.getBody(), data)*/,

    cls: 'successVoucherDataView',
    itemSelector: 'li.posDataViewItem',
    singleSelect: true,
    autoScroll: true,
    initComponent: function () {

        this.store = new jun.SuccessVoucherstore();

        jun.SuccessVoucherDataView.superclass.initComponent.call(this);

        //this.tpl.overwrite(tpl., data.voucher,true);

        this.on('selectionchange', this.onSelectionchange, this);
    },

    onSelectionchange: function(c, selections)
    {

        var r = c.getSelectedRecords();
        if(!r.length) return;
        var rec = r[0];

        this.vid = rec.get('id_voucher_redemption');

        Ext.Ajax.request({
            url: 'Customerslinkage/Otp/',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: rec.get('id_voucher_redemption'),
                store: STORE
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                alert(response.status + ' - ' +response.messages['0']);
                //this.parent.loadVocher();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);

        /*if( this.getId() == 'dd-avail')

        /!*Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menggunakan voucher ini?', this);

        Ext.MessageBox.confirm('Pertanyaan', 'Are you sure ?', function(btn){
            if(btn === 'yes'){
                //some code
            }
            else{
                //some code
            }
        });*!/

            Ext.Ajax.request({
                url: 'Customerslinkage/Voucherbuy',
                method: 'POST',
                scope: this,
                params: {
                    id: this.online_id,
                    vid: rec.get('id_voucher_redemption'),
                    store: this.store
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    this.parent.loadVocher();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        else
            Ext.Ajax.request({
                url: 'Customerslinkage/Otp',
                method: 'POST',
                scope: this,
                params: {
                    id: this.online_id,
                    vid: rec.get('id_voucher_redemption'),
                    store: this.store
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    //this.parent.loadVocher();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
            Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);
*/
    },

    otp:function(btn, txt){
        if (btn != 'ok') {
            return;
        }
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan OTP yang dikirimkan.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }

        Ext.Ajax.request({
            url: 'Customerslinkage/Voucheruse/',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: this.vid,//rec.get('id_voucher_redemption'),
                store: STORE,
                otp: txt
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.parent.loadVocher();
                alert(response.status + ' - ' +response.result['0']);

                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.success(),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                ///this.loadVocher();
                //this.close();

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },
    /*loadVocher: function(){
        Ext.Ajax.request({
            url: 'Customerslinkage/Vouchers/',
            method: 'POST',
            scope: this,
            params: {
                id: this.customer_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.point.setValue(response.result['my-point']);
                this.voucher.store.loadData(response.result['available-voucher']);
                this.myvoucher.store.loadData(response.result['my-voucher']);

                /!* this.point.setValue(response['my-point'])
                this.voucher.store.loadData(response['available-voucher']);

                this.myvoucher.store.loadData(response['my-voucher']); *!/
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },*/
});
jun.SuccessVoucherstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.SuccessVoucherstore.superclass.constructor.call(this, Ext.apply({
            // url: 'Customerslinkage/Vouchers',
            // root: 'available-voucher',
            fields: [
                {name:'trx_id'},
                {name:'id_voucher_redemption'},
                {name:'qr_code'},
                {name:'id_user'},
                {name:'voucher_expired'},
                {name:'voucher_used'},
                {name:'id_outlet'}
            ]
        }, cfg));
    }
});
jun.rztSuccessVoucherstore = new jun.SuccessVoucherstore();

//QR VOUCHER
jun.QRVoucherDataView = Ext.extend(Ext.DataView, {
    tpl: new Ext.XTemplate(
        /*'<tpl for=".">',*/
        '<ul>',
        '<tpl for=".">',
        '<li class="posDataViewItem product">',
        //'<tpl for="voucher">',
        '<div class="product-img">',
        '<img src="{photo}" alt="Voucher" width="100" height="100">',
        /*'<span class="price-tag">Exp: {voucher_expired}</span>',*/
        '</div>',
        //'<div class="product-name"><center>{voucher_name}<center></span></span></div>',
        //'</tpl>',
        '</li>',
        '</tpl>',
        '</ul>'
        /*'</tpl>'*/
    )/*.overwrite(Ext.getBody(), data)*/,

    cls: 'QRVoucherDataView',
    itemSelector: 'li.posDataViewItem',
    singleSelect: true,
    autoScroll: true,
    initComponent: function () {

        this.store = new jun.QRVoucherstore();

        jun.SuccessVoucherDataView.superclass.initComponent.call(this);

        //this.tpl.overwrite(tpl., data.voucher,true);

        //this.on('selectionchange', this.onSelectionchange, this);
    },

    onSelectionchange: function(c, selections)
    {

        var r = c.getSelectedRecords();
        if(!r.length) return;
        var rec = r[0];

        this.vid = rec.get('id_voucher_redemption');

        Ext.Ajax.request({
            url: 'Customerslinkage/Otp/',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: rec.get('id_voucher_redemption'),
                store: STORE
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                alert(response.status + ' - ' +response.messages['0']);
                //this.parent.loadVocher();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);

        /*if( this.getId() == 'dd-avail')

        /!*Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menggunakan voucher ini?', this);

        Ext.MessageBox.confirm('Pertanyaan', 'Are you sure ?', function(btn){
            if(btn === 'yes'){
                //some code
            }
            else{
                //some code
            }
        });*!/

            Ext.Ajax.request({
                url: 'Customerslinkage/Voucherbuy',
                method: 'POST',
                scope: this,
                params: {
                    id: this.online_id,
                    vid: rec.get('id_voucher_redemption'),
                    store: this.store
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    this.parent.loadVocher();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        else
            Ext.Ajax.request({
                url: 'Customerslinkage/Otp',
                method: 'POST',
                scope: this,
                params: {
                    id: this.online_id,
                    vid: rec.get('id_voucher_redemption'),
                    store: this.store
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    //this.parent.loadVocher();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
            Ext.MessageBox.prompt("OTP " , "</br>Masukkan OTP :", this.otp, this, true);
*/
    },

    otp:function(btn, txt){
        if (btn != 'ok') {
            return;
        }
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan OTP yang dikirimkan.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }

        Ext.Ajax.request({
            url: 'Customerslinkage/Voucheruse/',
            method: 'POST',
            scope: this,
            params: {
                id: this.online_id,
                vid: this.vid,//rec.get('id_voucher_redemption'),
                store: STORE,
                otp: txt
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.parent.loadVocher();
                alert(response.status + ' - ' +response.result['0']);

                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.success(),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                ///this.loadVocher();
                //this.close();

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },
    /*loadVocher: function(){
        Ext.Ajax.request({
            url: 'Customerslinkage/Vouchers/',
            method: 'POST',
            scope: this,
            params: {
                id: this.customer_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.point.setValue(response.result['my-point']);
                this.voucher.store.loadData(response.result['available-voucher']);
                this.myvoucher.store.loadData(response.result['my-voucher']);

                /!* this.point.setValue(response['my-point'])
                this.voucher.store.loadData(response['available-voucher']);

                this.myvoucher.store.loadData(response['my-voucher']); *!/
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },*/
});

jun.QRVoucherstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.QRVoucherstore.superclass.constructor.call(this, Ext.apply({

            fields: [
                {name:'qr_code'}
            ]
        }, cfg));
    }
});
jun.rztQRVoucherstore = new jun.QRVoucherstore();