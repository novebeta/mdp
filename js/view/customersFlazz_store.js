jun.CustomersFlazzstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CustomersFlazzstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomersFlazzStoreId',
            url: 'CustomersFlazz',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_id'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'tempat_lahir'},
                {name: 'tgl_lahir'},
                {name: 'email'},
                {name: 'telp'},
                {name: 'alamat'},
                {name: 'city'},
                {name: 'pt'},
                {name: 'ktp'},
                {name: 'npwp'}
            ]
        }, cfg));
    }
});
jun.rztCustomersFlazz = new jun.CustomersFlazzstore();
jun.rztCustomersFlazzCmp = new jun.CustomersFlazzstore();
jun.rztCustomersFlazzLib = new jun.CustomersFlazzstore();
jun.rztCustomersFlazzCmp.load();
jun.rztCustomersFlazzLib.load();
