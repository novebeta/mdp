jun.SalesFlazzWin = Ext.extend(Ext.Window, {
    title: 'Penjualan Kartu',
    modez: 1,
    width: 690,
    height: 465,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SalesFlazz',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        // readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Customer',
                        store: jun.rztCustomersFlazzCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        width: 170,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. SO',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        anchor: '100%',
                        x: 400,
                        y: 2
                        //allowBlank: 1
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        ref: '../note',
                        width: 256,
                        height: 50,
                        x: 400,
                        y: 32,
                    },
                    new jun.SalesFlazzItemsGrid({
                        x: 5,
                        y: 95,
                        height: 230,
                        frameHeader: !1,
                        header: !1
                    }),
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     fieldLabel: 'Produk',
                    //     store: jun.rztBarangFlazz,
                    //     value: '0e812537-bcfd-11e9-a4ce-f8da0c234127',
                    //     hiddenName: 'barang_id',
                    //     valueField: 'barang_id',
                    //     displayField: 'nama_barang',
                    //     readOnly: true,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'numericfield',
                    //     fieldLabel: 'Qty',
                    //     hideLabel: false,
                    //     name: 'qty',
                    //     id: 'qtyid',
                    //     ref: '../qty',
                    //     maxLength: 11,
                    //     enableKeyEvents: true,
                    //     value: 1,
                    //     minValue: 0,
                    //     anchor: '100%'
                    // },
                    {
                        xtype: "label",
                        text: "Sblm PPN:",
                        x: 5,
                        y: 330
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Tot Sblm PPN',
                        hideLabel: false,
                        name: 'bruto',
                        id: 'brutoid',
                        ref: '../bruto',
                        maxLength: 15,
                        value: 0,
                        minValue: 0,
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 328
                    },
                    {
                        xtype: 'hidden',
                        ref: '../hargajualid',
                        id: 'hargajualid',
                        name: 'harga'
                    },
                    // {
                    //     xtype: 'hidden',
                    //     ref: '../disc',
                    //     name: 'disc',
                    //     value: 0
                    // },
                    // {
                    //     xtype: 'hidden',
                    //     ref: '../discrp',
                    //     name: 'discrp',
                    //     value: 0
                    // },
                    // {
                    //     xtype: 'hidden',
                    //     ref: '../vat',
                    //     name: 'vat',
                    //     value: 10
                    // },
                    {
                        xtype: "label",
                        text: "PPN:",
                        x: 5,
                        y: 360
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'PPN',
                        hideLabel: false,
                        value: 0,
                        minValue: 0,
                        name: 'vatrp',
                        id: 'vatrpid',
                        ref: '../vatrp',
                        maxLength: 15,
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 358
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 330
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total',
                        hideLabel: false,
                        value: 0,
                        minValue: 0,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        enableKeyEvents: true,
                        anchor: '100%',
                        x: 400,
                        y: 328
                    },
                    {
                        xtype: "label",
                        text: "Harga Beli:",
                        x: 295,
                        y: 360
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Harga Beli',
                        hideLabel: false,
                        name: 'harga_beli',
                        id: 'harga_beliid',
                        ref: '../harga_beli',
                        value: 0,
                        minValue: 0,
                        maxLength: 15,
                        anchor: '100%',
                        x: 400,
                        y: 358
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalesFlazzWin.superclass.initComponent.call(this);
        this.on("close", this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.harga.on('keyup', this.onDiscChange, this);
        // this.qty.on('keyup', this.onDiscChange, this);
        // this.total.on('keyup', this.onDiscChange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztSalesFlazzItems.removeAll();
    },
    onQtyChange: function (t, n, o) {
        var vatrp = parseFloat(this.vatrp.getValue());
        console.log("n " + n);
        console.log("o " + o);
        console.log(vatrp);
        let val = parseFloat(vatrp / o);
        console.log(val);
        console.log(val * n);
        this.vatrp.setValue(val * n);
    },
    onDiscChange: function (a, e) {
        var total = parseFloat(this.total.getValue());
        // var price = parseFloat(this.harga.getValue());
        var qty = parseFloat(this.qty.getValue());
        var bruto = round2(total / 1.1);
        var vatrp = total - bruto;
        var harga = round2(bruto / qty);
        // vatrp = parseFloat(this.vatrp.getValue());
        // var bruto = round(price * qty, 2);
        // // var vatrp = (vat/100) * bruto;
        // var total = bruto + vatrp;
        this.bruto.setValue(bruto);
        this.vatrp.setValue(vatrp);
        this.harga.setValue(harga);
        // this.total.setValue(total);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'SalesFlazz/create';
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'SalesFlazz/update/id/' + this.id;
        // } else {
        //     urlz = 'SalesFlazz/create/';
        // }
        Ext.getCmp('form-SalesFlazz').getForm().submit({
            url: urlz,
            // timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztSalesFlazzItems.data.items, "data")),
                mode: this.modez,
                id: this.id,
            },
            success: function (f, a) {
                jun.rztSalesFlazz.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SalesFlazz').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SalesFlazzTopUpWin = Ext.extend(Ext.Window, {
    title: 'Top Up Flazz',
    modez: 1,
    width: 355,
    height: 365,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SalesFlazzTopUp',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Customer',
                        store: jun.rztCustomersFlazzCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Produk',
                        store: jun.rztBarangFlazz,
                        value: '39c4fdb0-bcfd-11e9-a4ce-f8da0c234127',
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'nama_barang',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../bruto',
                        name: 'bruto'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../disc',
                        name: 'disc',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        ref: '../discrp',
                        name: 'discrp',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        ref: '../vat',
                        name: 'vat',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        ref: '../vatrp',
                        name: 'vatrp',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        ref: '../harga_beli',
                        name: 'harga_beli',
                        value: 0
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Qty',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty',
                        id: 'qtyid',
                        ref: '../qty',
                        maxLength: 11,
                        enableKeyEvents: true,
                        value: 1,
                        minValue: 0,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Harga',
                        hideLabel: false,
                        //hidden:true,
                        name: 'harga',
                        id: 'hargaid',
                        ref: '../harga',
                        value: 0,
                        minValue: 0,
                        enableKeyEvents: true,
                        maxLength: 15,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        value: 0,
                        minValue: 0,
                        maxLength: 15,
                        readOnly: true,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. SO',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        fieldLabel: 'Tgl SO',
                        name: 'tgl_jatuh_tempo',
                        id: 'tgl_jatuh_tempoid',
                        format: 'd M Y',
                        value: DATE_NOW,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalesFlazzTopUpWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.harga.on('keyup', this.onDiscChange, this);
        this.qty.on('keyup', this.onDiscChange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.harga.getValue());
        var qty = parseFloat(this.qty.getValue());
        var vat = parseFloat(this.vat.getValue());
        var bruto = round(price * qty, 2);
        var vatrp = (vat / 100) * bruto;
        var total = bruto + vatrp;
        this.bruto.setValue(bruto);
        this.vatrp.setValue(vatrp);
        this.total.setValue(total);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'SalesFlazz/create/';
        Ext.getCmp('form-SalesFlazzTopUp').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
            },
            success: function (f, a) {
                jun.rztSalesFlazz.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SalesFlazzTopUp').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});