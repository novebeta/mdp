jun.PelunasanPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanPiutangStoreId',
            url: 'PelunasanPiutang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_piutang_id'},
                {name: 'total'},
                {name: 'no_bg_cek'},
                {name: 'no_bukti'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'bank_id'},
                {name: 'customer_id'},
                {name: 'store'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'up'},

            ]
        }, cfg));
    }
});
jun.rztPelunasanPiutang = new jun.PelunasanPiutangstore();
//jun.rztPelunasanPiutang.load();
