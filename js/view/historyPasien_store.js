jun.HistoryPasienstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.HistoryPasienstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'HistoryPasienStoreId',
            url: 'history/HistoryPasien',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id_pasien'},
{name:'nokartu'},
{name:'nobase'},
{name:'noax'},
{name:'namacus'},
{name:'namadep'},
{name:'linedisc'},
{name:'alamat'},
{name:'email'},
{name:'kota'},
{name:'telp'},
{name:'jnscus'},
{name:'tgllh'},
{name:'sex'},
{name:'usia'},
{name:'kerja'},
{name:'awal'},
{name:'akhir'},
{name:'akhirke'},
{name:'saldo'},
{name:'dtgl'},
{name:'jmlcp'},
{name:'id_cabang'},
{name:'id_source'},
{name:'aktif'},
{name:'createddate'},
                
            ]
        }, cfg));
    }
});
jun.rztHistoryPasien = new jun.HistoryPasienstore();
//jun.rztHistoryPasien.load();
