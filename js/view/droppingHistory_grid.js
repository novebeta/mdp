jun.DroppingHistoryGrid=Ext.extend(Ext.grid.GridPanel ,{
	title:"DroppingHistory",
        id:'docs-jun.DroppingHistoryGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[

        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'tgl',
            width:50
        },
                                {
			header:'Docref',
			sortable:true,
			resizable:true,
            dataIndex:'doc_ref',
			width:70
		},

                                {
			header:'Origin',
			sortable:true,
			resizable:true,
            dataIndex:'store',
			width:30
		},
                {
			header:'Destination',
			sortable:true,
			resizable:true,
            dataIndex:'store_pengirim',
			width:30
		},
        {
            header:'User',
            sortable:true,
            resizable:true,
            dataIndex:'user',
            width:23
        },
                                {
			header:'Status',
			sortable:true,
			resizable:true,
            dataIndex:'status',
			width:50
		},
                                {
			header:'Description',
			sortable:true,
			resizable:true,
            dataIndex:'desc',
			width:120
		},

	],
	initComponent: function(){
	this.store = jun.rztDroppingHistory;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20,
			   ref:"../paging"
           }]
        };

		jun.DroppingHistoryGrid.superclass.initComponent.call(this);

        this.getSelectionModel().on('rowselect', this.getrow, this);
        //this.paging.doRefresh();
	},


        getrow: function(sm, idx, r){
            this.record = r;

            /*var selectedz = this.sm.getSelections();
            return '<div ext:qtitle="fsdfsdf" ext:qtip="sdfsdf"> sdfsdf</div>';*/

        },


})
