jun.MobilKategoriBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilKategoriBpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilKategoriBpStoreId',
            url: 'MobilKategoriBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_kategori_id'},
                {name: 'nama'},
            ]
        }, cfg));
    }
});
jun.rztMobilKategoriBp = new jun.MobilKategoriBpstore();
jun.rztMobilKategoriBpCmp = new jun.MobilKategoriBpstore();
jun.rztMobilKategoriBpLib = new jun.MobilKategoriBpstore();
jun.rztMobilKategoriBpLib.load();
