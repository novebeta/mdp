jun.SyncHistoryWin = Ext.extend(Ext.Window, {
    title: 'SyncHistory',
    modez:1,
    width: 752,
    height: 440,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-SyncHistory',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    new jun.SyncHistoryGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        ref: "../gridDetail",
                        x: 5,
                        y: 65 + 30
                    }),
                ]
            }];

        jun.SyncHistoryWin.superclass.initComponent.call(this);
        this.gridDetail.store.baseParams=this.storecode;
        this.gridDetail.paging.doRefresh();
//
    },

});