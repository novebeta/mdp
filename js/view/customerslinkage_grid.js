jun.CustomersLinkageGrid=Ext.extend(Ext.grid.GridPanel ,{        
	    title:"CustomersLinkage",
        id:'docs-jun.CustomersLinkageGrid',
        iconCls:"silk-grid",
        viewConfig:{
            forceFit:true
        },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	    columns:[
    /*     {
            header:'linkage_id',
            sortable:true,
            resizable:true,                        
            dataIndex:'linkage_id',
            width:100
        }, 
        {
			header:'customers_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'customers_id',
			width:100
        },
        */
            {
                header: 'Customers No.',
                sortable: true,
                resizable: true,
                dataIndex: 'no_customer',
                width: 100,
                filter: {xtype: "textfield"}
            },
            {
                header: 'Customer Name',
                sortable: true,
                resizable: true,
                dataIndex: 'nama_customer',
                width: 100,
                filter: {xtype: "textfield"}
            },
            {
                header: 'Phone',
                sortable: true,
                resizable: true,
                dataIndex: 'telp',
                width: 100,
                filter: {xtype: "textfield"}
            },
        
            {
                header:'Online ID',
                sortable:true,
                resizable:true,                        
                dataIndex:'online_id',
                width:100,
                filter: {xtype: "textfield"}
            },
                            
        ],
        initComponent: function(){
        this.store = jun.rztCustomersLinkage;
            this.bbar = {
                items: [
            {
                xtype: 'paging',
                store: this.store,
                displayInfo: true,
                pageSize: 20
            }]
            };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                   /*  {
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    }, 
                    {
                        xtype: 'button',
                        text: 'Ubah',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    },
                    {
                        xtype:'tbseparator'
                    },*/
                    {
                        xtype: 'button',
                        text: 'Linkage',
                        ref: '../btnLinkage'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Voucher',
                        ref: '../btnVoucher'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Online Reservation',
                        ref: '../btnReservation'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		    jun.CustomersLinkageGrid.superclass.initComponent.call(this);
	            //this.btnAdd.on('Click', this.loadForm, this);
                //this.btnEdit.on('Click', this.loadEditForm, this);
                //this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
                this.btnLinkage.on('Click', this.loadLinkageForm, this);
                this.btnVoucher.on('Click', this.loadVoucherForm, this);
                this.btnReservation.on("click", this.loadReservation, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
            this.btnLinkage.setDisabled(r.get('online_id'));
            this.btnVoucher.setDisabled(!r.get('online_id'));
        },
        
        loadForm: function(){
            var form = new jun.CustomersLinkageWin({modez:0});
            form.show();
        },
        
       /*  loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.linkage_id;
            var form = new jun.CustomersLinkageWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        }, */
        
        loadLinkageForm: function () {
            var selectedz = this.sm.getSelected();
            //var dodol = this.store.getAt(0);
            if (selectedz == undefined) {
                Ext.MessageBox.alert("Warning", "You have not selected a customer");
                return;
            }
            var idz = selectedz.json.customer_id;
            var form = new jun.CustomersWin({modez: 1, customer_id: idz});
            form.formz.getForm().loadRecord(this.record);
            form.nama_customer_cmb.setValue(this.record.json.nama_customer);
            form.show(this);
        },

        loadVoucherForm: function () {
            var selectedz = this.sm.getSelected();
            //var dodol = this.store.getAt(0);
            if (selectedz == undefined) {
                Ext.MessageBox.alert("Warning", "You have not selected a customer");
                return;
            }
            var idz = selectedz.json.customer_id;
            var form = new jun.CustomersVoucherWin({
                modez: 1, 
                customer_id: idz, 
                online_id: selectedz.json.online_id,
                store: selectedz.json.store
            });
            //form.formz.getForm().loadRecord(this.record);
            //form.nama_customer_cmb.setValue(this.record.json.nama_customer);
            form.show(this);
        },

        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'CustomersLinkage/delete/id/' + record.json.linkage_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztCustomersLinkage.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        },

        loadReservation: function(){
            //document.location.href = "online"; 
            var serverPath = window.location.href;
            var serverPathIndex = serverPath.lastIndexOf("/");
            window.open(serverPath.substring(0, serverPathIndex) + '/online','_blank','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,width=700,height=600');
        
        }
})
