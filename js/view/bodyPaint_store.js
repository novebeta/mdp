jun.BodyPaintstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BodyPaintstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BodyPaintStoreId',
            url: 'BodyPaint',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'body_paint_id'},
                {name: 'est_no'},
                {name: 'est_tgl'},
                {name: 'wo_no'},
                {name: 'wo_tgl'},
                {name: 'est_kerja'},
                {name: 'nama_sa'},
                {name: 'est_start'},
                {name: 'est_end'},
                {name: 'total_jasa'},
                {name: 'total_parts'},
                {name: 'grand_total'},
                {name: 'mobil_id'},
                {name: 'asuransi_bp_id'},
                {name: 'no_spk'},
                {name: 'asuransi_start'},
                {name: 'asuransi_end'},
                {name: 'total_jasa_line'},
                {name: 'total_jasa_discrp'},
                {name: 'biaya'},
                {name: 'mobil'},
                {name: 'tahun_km'},
                {name: 'inv_no'},
                {name: 'inv_tgl'},
                {name: 'lunas'},
                {name: 'b1_bank_id'},
                {name: 'no_polis'},
                {name: 'tgl_spk'},
                {name: 'qty_kejadian'},
                {name: 'own_risk'},
                {name: 'total_asuransi'},
                {name: 'vat'},
                {name: 'vatrp'},
                {name: 'total'},
                {name: 'tgl_spk_1'},
                {name: 'no_spk_1'},
                {name: 'b1_tgl'},
                {name: 'b1_total'},
                {name: 'b2_bank_id'},
                {name: 'b2_tgl'},
                {name: 'b2_total'},
                {name: 'bp_bayar_id'},
                {name: 'deleted'},
                {name: 'b3_tgl'},
                {name: 'note_'},
                {name: 'b1_qty'},
                {name: 'b4_tgl'},
                {name: 'b4_bank_id'},
                {name: 'b4_qty'},
                {name: 'b4_total'},
                {name: "no_faktur" },
                {name: 'npwp'}
            ]
        }, cfg));
    }
});
jun.rztBodyPaint = new jun.BodyPaintstore();
jun.rztWOBodyPaint = new jun.BodyPaintstore({
    'url': 'BodyPaint/IndexWO',
});
jun.rztINVBodyPaint = new jun.BodyPaintstore({
    'url': 'BodyPaint/IndexInv',
});
jun.rztCloseBodyPaint = new jun.BodyPaintstore({
    'url': 'BodyPaint/IndexClose',
});
//jun.rztBodyPaint.load();
jun.BpBayar2Liststore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayar2Liststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayar2ListStoreId',
            // url: 'BpBayar2',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'inv_no'},
                {name: 'no_pol'},
                {name: 'total', type: "float"},
                {name: 'body_paint_id'}
            ]
        }, cfg));
        // this.on('add', this.refreshData, this);
        // this.on('update', this.refreshData, this);
        // this.on('remove', this.refreshData, this);
    },
    // refreshData: function () {
    //     var total = this.sum("total");
    //     Ext.getCmp('totalBpBayar2id').setValue(total);
    // }
});
jun.rztBpBayar2List = new jun.BpBayar2Liststore();