jun.AsuransiBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AsuransiBpstore.superclass.constructor.call(this, Ext.apply({
            // storeId: 'AsuransiBpStoreId',
            url: 'AsuransiBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'asuransi_bp_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'address'},
                {name: 'city'},
                {name: 'npwp'}
            ]
        }, cfg));
    }
});
jun.rztAsuransiBp = new jun.AsuransiBpstore();
jun.rztAsuransiBpCmp = new jun.AsuransiBpstore();
jun.rztAsuransiBpLib = new jun.AsuransiBpstore();
//jun.rztAsuransiBp.load();
