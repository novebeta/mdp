jun.BodyPaintGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Estimasi",
    id: 'docs-jun.BodyPaintGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_no',
            width: 100
        },
        {
            header: 'Tgl Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_tgl',
            width: 100
        },
        {
            header: 'Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_kerja',
            width: 100
        },
        {
            header: 'Nama SA',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_sa',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'mobil',
            renderer: function (a) {
                return a.no_pol;
            },
            width: 100
        },
        {
            header: 'Total Jasa',
            sortable: true,
            resizable: true,
            dataIndex: 'total_jasa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total Parts',
            sortable: true,
            resizable: true,
            dataIndex: 'total_parts',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Grand Total',
            sortable: true,
            resizable: true,
            dataIndex: 'grand_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
    ],
    initComponent: function () {
        if (jun.rztMerkCmp.getTotalCount() === 0) {
            jun.rztMerkCmp.load();
        }
        if (jun.rztMobilTipeBpCmp.getTotalCount() === 0) {
            jun.rztMobilTipeBpCmp.load();
        }
        if (jun.rztMobilKategoriBpCmp.getTotalCount() === 0) {
            jun.rztMobilKategoriBpCmp.load();
        }
        if (jun.rztAsuransiBpCmp.getTotalCount() === 0) {
            jun.rztAsuransiBpCmp.load();
        }
        if (jun.rztJasaBpCmp.getTotalCount() === 0) {
            jun.rztJasaBpCmp.load();
        }
        this.store = jun.rztBodyPaint;
        jun.rztBodyPaint.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var no_pol = '';
                    if(Ext.getCmp('filter_nopol_body_paint_id') !== undefined){
                        no_pol = Ext.getCmp('filter_nopol_body_paint_id').getValue();
                    }
                    b.params.no_pol = no_pol;
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-',
                        {
                            xtype: 'combo',
                            fieldLabel: 'No. Polisi',
                            ref: '../../mobil_id',
                            triggerAction: 'query',
                            lazyRender: true,
                            mode: 'remote',
                            forceSelection: false,
                            autoSelect: false,
                            store: jun.rztMobilBpCmp,
                            id: "filter_nopol_body_paint_id",
                            // hiddenName: 'mobil_id',
                            valueField: 'no_pol',
                            displayField: 'no_pol',
                            hideTrigger: true,
                            minChars: 2,
                            matchFieldWidth: !1,
                            pageSize: 20,
                            itemSelector: 'div.search-item',
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                '{customer_id}',
                                '</div></tpl>'
                            ),
                            // allowBlank: false,
                            listWidth: 450,
                            lastQuery: "",
                            anchor: '100%'
                        },
                        '-'
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Estimasi',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Estimasi',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (STORE !== ''),
                },
                {
                    xtype: 'button',
                    text: 'Delete Estimasi',
                    ref: '../btnDelete',
                    hidden: (STORE !== ''),
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Estimasi',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Create Work Order',
                    ref: '../btnCreateWO'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.BodyPaintGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.btnCreateWO.on('Click', this.createWO, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        Ext.getCmp('filter_nopol_body_paint_id').on('select', function () {
            this.store.load();
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztMobilBpCmp.baseParams = {
            mobil_id: this.record.data.mobil_id
        };
        jun.rztMobilBpCmp.load();
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printEstimasi/id/' + idz, '_blank');
    },
    loadForm: function () {
        var form = new jun.BodyPaintWin({modez: 0, title: 'Estimasi'});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        var form = new jun.BodyPaintWin({modez: 1, title: 'Estimasi', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBpJasa.load({
            params: {
                body_paint_id: idz
            }
        });
        jun.rztBpParts.load({
            params: {
                body_paint_id: idz
            }
        });
        // setTimeout(function () {
        //     jun.rztBpJasa.fireEvent('add');
        //     jun.rztBpParts.fireEvent('add');
        // }, 1000);
        Ext.getCmp('radio-biaya').setValue(selectedz.json.biaya);
        let c = Ext.getCmp('mobil_body_paint_id');
        cIdx = jun.rztMobilBpCmp.findExact("mobil_id", selectedz.json.mobil_id);
        c.fireEvent('select', c, jun.rztMobilBpCmp.getAt(cIdx));
        if (selectedz.json.biaya == 'ASURANSI') {
            let d = Ext.getCmp('asuransi_bp_id');
            dIdx = jun.rztAsuransiBpCmp.findExact("asuransi_bp_id", selectedz.json.asuransi_bp_id);
            d.fireEvent('select', d, jun.rztAsuransiBpCmp.getAt(dIdx));
        }
    },
    createWO: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membuat WO data ini?', this.createWOYes, this);
    },
    createWOYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        var form = new jun.BodyPaintWin({modez: 2, title: 'Create Work Order', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBpJasa.load({
            params: {
                body_paint_id: idz
            }
        });
        jun.rztBpParts.load({
            params: {
                body_paint_id: idz
            }
        });
        // setTimeout(function () {
        //     jun.rztBpJasa.fireEvent('add');
        //     jun.rztBpParts.fireEvent('add');
        // }, 1000);
        Ext.getCmp('radio-biaya').setValue(selectedz.json.biaya);
        let c = Ext.getCmp('mobil_body_paint_id');
        cIdx = jun.rztMobilBpCmp.findExact("mobil_id", selectedz.json.mobil_id);
        c.fireEvent('select', c, jun.rztMobilBpCmp.getAt(cIdx));
        if (selectedz.json.biaya == 'ASURANSI') {
            let d = Ext.getCmp('asuransi_bp_id');
            dIdx = jun.rztAsuransiBpCmp.findExact("asuransi_bp_id", selectedz.json.asuransi_bp_id);
            d.fireEvent('select', d, jun.rztAsuransiBpCmp.getAt(dIdx));
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BodyPaint/delete/id/' + record.json.body_paint_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBodyPaint.reload();
                jun.rztWOBodyPaint.reload();
                jun.rztINVBodyPaint.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BodyPaintWOGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Work Order",
    id: 'docs-jun.BodyPaintWOGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_no',
            width: 100
        },
        {
            header: 'Tgl Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_tgl',
            width: 100
        },
        {
            header: 'No. WO',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_no',
            width: 100
        },
        {
            header: 'Tgl WO',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_tgl',
            width: 100
        },
        {
            header: 'Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_kerja',
            width: 100
        },
        {
            header: 'Nama SA',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_sa',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'mobil',
            renderer: function (a) {
                return a.no_pol;
            },
            width: 100
        },
        {
            header: 'Total Jasa',
            sortable: true,
            resizable: true,
            dataIndex: 'total_jasa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total Parts',
            sortable: true,
            resizable: true,
            dataIndex: 'total_parts',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Grand Total',
            sortable: true,
            resizable: true,
            dataIndex: 'grand_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
    ],
    initComponent: function () {
        if (jun.rztMerkCmp.getTotalCount() === 0) {
            jun.rztMerkCmp.load();
        }
        if (jun.rztMobilTipeBpCmp.getTotalCount() === 0) {
            jun.rztMobilTipeBpCmp.load();
        }
        if (jun.rztMobilKategoriBpCmp.getTotalCount() === 0) {
            jun.rztMobilKategoriBpCmp.load();
        }
        if (jun.rztAsuransiBpCmp.getTotalCount() === 0) {
            jun.rztAsuransiBpCmp.load();
        }
        if (jun.rztJasaBpCmp.getTotalCount() === 0) {
            jun.rztJasaBpCmp.load();
        }
        this.store = jun.rztWOBodyPaint;
        jun.rztWOBodyPaint.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var no_pol = Ext.getCmp('filter_nopol_wo_body_paint_id').getValue();
                    b.params.no_pol = no_pol;
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-',
                        {
                            xtype: 'combo',
                            fieldLabel: 'No. Polisi',
                            triggerAction: 'query',
                            lazyRender: true,
                            mode: 'remote',
                            forceSelection: false,
                            autoSelect: false,
                            store: jun.rztMobilBpCmp,
                            id: "filter_nopol_wo_body_paint_id",
                            valueField: 'no_pol',
                            displayField: 'no_pol',
                            hideTrigger: true,
                            minChars: 2,
                            matchFieldWidth: !1,
                            pageSize: 20,
                            itemSelector: 'div.search-item',
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                '{customer_id}',
                                '</div></tpl>'
                            ),
                            listWidth: 450,
                            lastQuery: "",
                            anchor: '100%'
                        },
                        '-'
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Invoice',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Work Order',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (STORE !== ''),
                },
                {
                    xtype: 'button',
                    text: 'Delete Work Order',
                    ref: '../btnDelete',
                    hidden: (STORE !== ''),
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Estimasi',
                    ref: '../btnPrintEst'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Work Order',
                    ref: '../btnPrint'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.BodyPaintWOGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrintEst.on('Click', this.printEstForm, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        Ext.getCmp('filter_nopol_wo_body_paint_id').on('select', function () {
            this.store.load();
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztMobilBpCmp.baseParams = {
            mobil_id: this.record.data.mobil_id
        };
        jun.rztMobilBpCmp.load();
    },
    printEstForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printEstimasi/id/' + idz, '_blank');
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printWorkOrder/id/' + idz, '_blank');
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        var form = new jun.BodyPaintWin({modez: 4, title: 'Work Order', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBpJasa.load({
            params: {
                body_paint_id: idz
            }
        });
        jun.rztBpParts.load({
            params: {
                body_paint_id: idz
            }
        });
        // setTimeout(function () {
        //     jun.rztBpJasa.fireEvent('add');
        //     jun.rztBpParts.fireEvent('add');
        // }, 1000);
        Ext.getCmp('radio-biaya').setValue(selectedz.json.biaya);
        let c = Ext.getCmp('mobil_body_paint_id');
        cIdx = jun.rztMobilBpCmp.findExact("mobil_id", selectedz.json.mobil_id);
        c.fireEvent('select', c, jun.rztMobilBpCmp.getAt(cIdx));
        if (selectedz.json.biaya == 'ASURANSI') {
            let d = Ext.getCmp('asuransi_bp_id');
            dIdx = jun.rztAsuransiBpCmp.findExact("asuransi_bp_id", selectedz.json.asuransi_bp_id);
            d.fireEvent('select', d, jun.rztAsuransiBpCmp.getAt(dIdx));
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        var form = new jun.BodyPaintWin({modez: 3, title: 'Work Order', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBpJasa.load({
            params: {
                body_paint_id: idz
            }
        });
        jun.rztBpParts.load({
            params: {
                body_paint_id: idz
            }
        });
        // setTimeout(function () {
        //     jun.rztBpJasa.fireEvent('add');
        //     jun.rztBpParts.fireEvent('add');
        // }, 1000);
        Ext.getCmp('radio-biaya').setValue(selectedz.json.biaya);
        let c = Ext.getCmp('mobil_body_paint_id');
        cIdx = jun.rztMobilBpCmp.findExact("mobil_id", selectedz.json.mobil_id);
        c.fireEvent('select', c, jun.rztMobilBpCmp.getAt(cIdx));
        if (selectedz.json.biaya == 'ASURANSI') {
            let d = Ext.getCmp('asuransi_bp_id');
            dIdx = jun.rztAsuransiBpCmp.findExact("asuransi_bp_id", selectedz.json.asuransi_bp_id);
            d.fireEvent('select', d, jun.rztAsuransiBpCmp.getAt(dIdx));
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BodyPaint/delete/id/' + record.json.body_paint_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBodyPaint.reload();
                jun.rztWOBodyPaint.reload();
                jun.rztINVBodyPaint.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BodyPaintINVGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice",
    id: 'docs-jun.BodyPaintINVGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: false
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: false}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_no',
            width: 100
        },
        {
            header: 'Tgl Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_tgl',
            width: 100
        },
        {
            header: 'No. WO',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_no',
            width: 100
        },
        {
            header: 'Tgl WO',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_tgl',
            width: 100
        },
        {
            header: 'No. INV',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_no',
            width: 100
        },
        {
            header: 'Tgl INV',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_tgl',
            width: 100
        },
        {
            header: 'Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_kerja',
            width: 100
        },
        {
            header: 'Nama SA',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_sa',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'mobil',
            renderer: function (a) {
                return a.no_pol;
            },
            width: 100
        },
        {
            header: 'Total Jasa',
            sortable: true,
            resizable: true,
            dataIndex: 'total_jasa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total Parts',
            sortable: true,
            resizable: true,
            dataIndex: 'total_parts',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Grand Total',
            sortable: true,
            resizable: true,
            dataIndex: 'grand_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Qty OR Cust',
            sortable: true,
            resizable: true,
            dataIndex: 'b1_qty',
            width: 50,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Bayar OR Cust',
            sortable: true,
            resizable: true,
            dataIndex: 'b1_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
            // renderer: function (value, metaData, record, rowIndex, colIndex, store) {
            //     // console.log(record);
            //     let tot = parseFloat(record.data.b1_total + record.data.b4_total);
            //     return Ext.util.Format.number(tot, "0,0");
            // }
        },
        {
            header: 'Qty OR HO',
            sortable: true,
            resizable: true,
            dataIndex: 'b4_qty',
            width: 50,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Bayar OR HO',
            sortable: true,
            resizable: true,
            dataIndex: 'b4_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Bayar Asuransi',
            sortable: true,
            resizable: true,
            dataIndex: 'b2_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Lunas',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'SUDAH'
                } else {
                    return 'BELUM'
                }
            }
        },
        {
            header: 'Tgl Lunas',
            resizable: true,
            dataIndex: 'lunas',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return value.substring(0,10);
                } else {
                    return ''
                }
            }
        },
    ],
    initComponent: function () {
        if (jun.rztMerkCmp.getTotalCount() === 0) {
            jun.rztMerkCmp.load();
        }
        if (jun.rztMobilTipeBpCmp.getTotalCount() === 0) {
            jun.rztMobilTipeBpCmp.load();
        }
        if (jun.rztMobilKategoriBpCmp.getTotalCount() === 0) {
            jun.rztMobilKategoriBpCmp.load();
        }
        if (jun.rztAsuransiBpCmp.getTotalCount() === 0) {
            jun.rztAsuransiBpCmp.load();
        }
        if (jun.rztJasaBpCmp.getTotalCount() === 0) {
            jun.rztJasaBpCmp.load();
        }
        this.store = jun.rztINVBodyPaint;
        jun.rztINVBodyPaint.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var no_pol = Ext.getCmp('filter_nopol_inv_body_paint_id').getValue();
                    b.params.no_pol = no_pol;
                    b.params.asuransi_bp_id = Ext.getCmp('grid_asuransi_bp_id').getValue();
                    b.params.status = Ext.getCmp('cmb_status_grid').getValue();
                    b.params.tglfrom = Ext.getCmp('tglgridinvbpfrom').getValue();
                    b.params.tglto = Ext.getCmp('tglgridinvbpto').getValue();
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridinvbpfrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridinvbpto'
                        }, '-',
                        'Nopol : ',
                        {
                            xtype: 'combo',
                            fieldLabel: 'No. Polisi',
                            ref: '../../mobil_id',
                            triggerAction: 'query',
                            lazyRender: true,
                            mode: 'remote',
                            forceSelection: false,
                            autoSelect: false,
                            store: jun.rztMobilBpCmp,
                            id: "filter_nopol_inv_body_paint_id",
                            // hiddenName: 'mobil_id',
                            valueField: 'no_pol',
                            displayField: 'no_pol',
                            hideTrigger: true,
                            minChars: 2,
                            matchFieldWidth: !1,
                            pageSize: 20,
                            itemSelector: 'div.search-item',
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                '{customer_id}',
                                '</div></tpl>'
                            ),
                            // allowBlank: false,
                            listWidth: 450,
                            lastQuery: "",
                            width: 100
                        }, '-', 'Asuransi : ',
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode'
                            ],
                            // readOnly: (this.modez !== 0 && this.modez !== 1),
                            fieldLabel: 'Kode Asuransi',
                            mode: 'local',
                            store: jun.rztAsuransiBpCmp,
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode}</td><td>{nama}</td>',
                                '</tr></tpl></tbody></table>'),
                            // allowBlank: false,
                            listWidth: 500,
                            hideTrigger: true,
                            hiddenName: 'asuransi_bp_id',
                            valueField: 'asuransi_bp_id',
                            displayField: 'kode',
                            id: 'grid_asuransi_bp_id',
                            width: 100
                        },
                        '-', 'STATUS : ',
                        new Ext.form.ComboBox({
                            displayField: "desc",
                            id: 'cmb_status_grid',
                            valueField: "code",
                            typeAhead: !0,
                            mode: "local",
                            forceSelection: !0,
                            triggerAction: "all",
                            selectOnFocus: !0,
                            width: 120,
                            store: new Ext.data.ArrayStore({
                                id: 'cmb_status_grid',
                                fields: ["code", "desc"],
                                data: [
                                    ["", "SEMUA STATUS"],
                                    ["BELUM", "BELUM LUNAS"],
                                    ["SUDAH", "SUDAH LUNAS"],
                                ]
                            })
                        }),
                        '-',
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Pembayaran Own Risk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Pembayaran Own Risk HO',
                    ref: '../btnAdd4',
                    hidden: !OWN_RISK_HO
                },
                {
                    xtype: 'tbseparator',
                    hidden: !OWN_RISK_HO
                },
                {
                    xtype: 'button',
                    text: 'Pembayaran Asuransi',
                    ref: '../btnAdd2'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Pembayaran Pribadi',
                    ref: '../btnPribadi'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Invoice',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Invoice',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kwitansi',
                    ref: '../btnPrintKwitansi'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Pelunasan Own Risk',
                    ref: '../btnPrintOwnRisk'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Pembayaran Asuransi',
                    ref: '../btnPrintPembayaranAsuransi'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print PASS',
                    ref: '../btnPrintPass'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.BodyPaintINVGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.deleteRec, this);
        this.btnAdd4.on('Click', this.bayar4, this);
        this.btnAdd2.on('Click', this.bayar2, this);
        this.btnPribadi.on('Click', this.pribadi, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.btnPrintKwitansi.on('Click', this.printKwitansi, this);
        this.btnPrintOwnRisk.on('Click', this.printOwnRisk, this);
        this.btnPrintPass.on('Click', this.printPass, this);
        this.btnPrintPembayaranAsuransi.on('Click', this.printPembayaranAsuransi, this);
        Ext.getCmp('filter_nopol_inv_body_paint_id').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('grid_asuransi_bp_id').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('cmb_status_grid').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridinvbpfrom').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridinvbpto').on('select', function () {
            this.store.load();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztMobilBpCmp.baseParams = {
            mobil_id: this.record.data.mobil_id
        };
        jun.rztMobilBpCmp.load();
    },
    printPembayaranAsuransi: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if (selectedz.json.biaya == 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan asuransi");
            return;
        }
        if (selectedz.json.b2_tgl == null) {
            Ext.MessageBox.alert("Warning", "Belum ada Pembayaran Asuransi.");
            return;
        }
        var idz = selectedz.json.bp_bayar_id;
        window.open('bodyPaint/PrintBuktiTerima/id/' + idz, '_blank');
    },
    printPass: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Invoice");
            return;
        }
        if (selectedz.data.biaya == 'PRIBADI' && selectedz.data.lunas == null) {
            Ext.MessageBox.alert("Warning", "Invoice pribadi belum lunas");
            return;
        }
        if (selectedz.data.biaya == 'ASURANSI' && selectedz.data.b1_tgl == null) {
            Ext.MessageBox.alert("Warning", "Invoice asuransi belum bayar Own Risk");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printPass/id/' + idz, '_blank');
    },
    printOwnRisk: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if (selectedz.json.biaya == 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan asuransi");
            return;
        }
        if (selectedz.json.b1_tgl == null) {
            Ext.MessageBox.alert("Warning", "Belum ada Pembayaran Own Risk.");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printOwnRisk/id/' + idz, '_blank');
    },
    printKwitansi: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if (selectedz.data.biaya == 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan asuransi");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printKwitansiAsuransi/id/' + idz, '_blank');
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printInvoice/id/' + idz, '_blank');
    },
    loadForm: function () {
        var form = new jun.BodyPaintWin({modez: 4});
        form.show();
    },
    bayar2: function () {
        var asuransi = Ext.getCmp('grid_asuransi_bp_id').getValue();
        if (asuransi == '') {
            Ext.MessageBox.alert("Warning", "Asuransi harus dipilih.");
            return;
        }
        var selectedz = this.sm.getSelections();
        if (selectedz.length < 1) {
            Ext.MessageBox.alert("Warning", "Minimal 1 Invoice yang harus di lunasi.");
            return;
        }
        jun.rztBpBayar2List.removeAll();
        var form = new jun.BpBayar2Win({modez: 0});
        selectedz.forEach(function (t, i) {
            if (t.json.lunas != null) {
                return;
            }
            if (t.json.bp_bayar_id != null) {
                return;
            }
            var c = jun.rztBpBayar2List.recordType;
            var d = new c({
                inv_no: t.json.inv_no,
                no_pol: t.json.mobil.no_pol,
                total: parseFloat(t.data.grand_total) - parseFloat(t.data.b1_total)- parseFloat(t.data.b4_total),
                body_paint_id: t.json.body_paint_id
            });
            jun.rztBpBayar2List.add(d);
            jun.rztBpBayar2List.commitChanges();
        });
        if (jun.rztBpBayar2List.getCount() > 0) {
            form.show();
            var total = jun.rztBpBayar2List.sum("total");
            total = Math.round(total);
            Ext.getCmp('totalBpTagih2id').setValue(total);
            Ext.getCmp('totalBpBayar2id').setValue(total);
        } else {
            Ext.MessageBox.alert("Warning", "Minimal 1 Invoice yang harus di lunasi.");
        }
    },
    pribadi: function () {
        // var asuransi = Ext.getCmp('grid_asuransi_bp_id').getValue();
        // if (asuransi == '') {
        //     Ext.MessageBox.alert("Warning", "Asuransi harus dipilih.");
        //     return;
        // }
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih invoice.");
            return;
        }
        if (selectedz.data.biaya != 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan own risk.");
            return;
        }
        this.record.data.total = 0;
        var form = new jun.BpBayar3Win({modez: 0});
        form.formz.getForm().loadRecord(this.record);
        form.show();
        jun.rztBpBayar3.load({params: {body_paint_id: this.record.data.body_paint_id}});
        // selectedz.forEach(function (t, i) {
        //     if (t.json.lunas != null) {
        //         return;
        //     }
        //     if (t.json.bp_bayar_id != null) {
        //         return;
        //     }
        //     var c = jun.rztBpBayar2List.recordType;
        //     var d = new c({
        //         inv_no: t.json.inv_no,
        //         no_pol: t.json.mobil.no_pol,
        //         total: parseFloat(t.data.grand_total) - parseFloat(t.data.b1_total),
        //         body_paint_id: t.json.body_paint_id
        //     });
        //     jun.rztBpBayar2List.add(d);
        //     jun.rztBpBayar2List.commitChanges();
        // });
        // if (jun.rztBpBayar2List.getCount() > 0) {
        //     form.show();
        //     var total = jun.rztBpBayar2List.sum("total");
        //     Ext.getCmp('totalBpBayar2id').setValue(total);
        // } else {
        //     Ext.MessageBox.alert("Warning", "Minimal 1 Invoice yang harus di lunasi.");
        // }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        var form = new jun.BodyPaintWin({modez: 5, title: 'Invoice', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBpJasa.load({
            params: {
                body_paint_id: idz
            }
        });
        jun.rztBpParts.load({
            params: {
                body_paint_id: idz
            }
        });
        // setTimeout(function () {
        //     jun.rztBpJasa.fireEvent('add');
        //     jun.rztBpParts.fireEvent('add');
        // }, 1000);
        Ext.getCmp('radio-biaya').setValue(selectedz.json.biaya);
        let c = Ext.getCmp('mobil_body_paint_id');
        cIdx = jun.rztMobilBpCmp.findExact("mobil_id", selectedz.json.mobil_id);
        c.fireEvent('select', c, jun.rztMobilBpCmp.getAt(cIdx));
        if (selectedz.json.biaya == 'ASURANSI') {
            let d = Ext.getCmp('asuransi_bp_id');
            dIdx = jun.rztAsuransiBpCmp.findExact("asuransi_bp_id", selectedz.json.asuransi_bp_id);
            d.fireEvent('select', d, jun.rztAsuransiBpCmp.getAt(dIdx));
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.json.biaya == 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan asuransi");
            return;
        }
        if (record.data.lunas != null) {
            Ext.MessageBox.alert("Warning", "Invoice sudah lunas");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membayar ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        var form = new jun.BayarInvBp({modez:1, record: record});
        form.show();
    },
    bayar4: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        if (record.json.biaya == 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan asuransi");
            return;
        }

        if (record.data.lunas != null) {
            Ext.MessageBox.alert("Warning", "Invoice sudah lunas");
            return;
        }
        var qty_sisa = parseFloat(record.data.qty_kejadian) - parseFloat(record.data.b1_qty);
        if ( qty_sisa <= 0 ) {
            Ext.MessageBox.alert("Warning", "Semua kejadian sudah terbayar.");
            return;
        }
        record.data.qty_kejadian = qty_sisa;
        record.data.total_asuransi = record.data.qty_kejadian * record.data.own_risk;
        var form = new jun.BayarInvBp({modez:4,record: record});
        form.show();
    }
});
jun.BodyPaintCloseGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice HO",
    id: 'docs-jun.BodyPaintCloseGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: false}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_no',
            width: 100
        },
        {
            header: 'Tgl Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_tgl',
            width: 100
        },
        {
            header: 'No. WO',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_no',
            width: 100
        },
        {
            header: 'Tgl WO',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_tgl',
            width: 100
        },
        {
            header: 'No. INV',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_no',
            width: 100
        },
        {
            header: 'Tgl INV',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_tgl',
            width: 100
        },
        {
            header: 'Estimasi',
            sortable: true,
            resizable: true,
            dataIndex: 'est_kerja',
            width: 100
        },
        {
            header: 'Nama SA',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_sa',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'mobil',
            renderer: function (a) {
                return a.no_pol;
            },
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztMerkCmp.getTotalCount() === 0) {
            jun.rztMerkCmp.load();
        }
        if (jun.rztMobilTipeBpCmp.getTotalCount() === 0) {
            jun.rztMobilTipeBpCmp.load();
        }
        if (jun.rztMobilKategoriBpCmp.getTotalCount() === 0) {
            jun.rztMobilKategoriBpCmp.load();
        }
        if (jun.rztAsuransiBpCmp.getTotalCount() === 0) {
            jun.rztAsuransiBpCmp.load();
        }
        if (jun.rztJasaBpCmp.getTotalCount() === 0) {
            jun.rztJasaBpCmp.load();
        }
        this.store = jun.rztCloseBodyPaint;
        jun.rztCloseBodyPaint.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var no_pol = Ext.getCmp('filter_nopol_close_body_paint_id').getValue();
                    b.params.no_pol = no_pol;
                    b.params.asuransi_bp_id = Ext.getCmp('grid_asuransi_bp_id').getValue();
                    b.params.status = Ext.getCmp('cmb_status_grid').getValue();
                    b.params.tglfrom = Ext.getCmp('tglgridclosebpfrom').getValue();
                    b.params.tglto = Ext.getCmp('tglgridclosebpto').getValue();
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridclosebpfrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridclosebpto'
                        }, '-',
                        'Nopol : ',
                        {
                            xtype: 'combo',
                            fieldLabel: 'No. Polisi',
                            ref: '../../mobil_id',
                            triggerAction: 'query',
                            lazyRender: true,
                            mode: 'remote',
                            forceSelection: false,
                            autoSelect: false,
                            store: jun.rztMobilBpCmp,
                            id: "filter_nopol_close_body_paint_id",
                            // hiddenName: 'mobil_id',
                            valueField: 'no_pol',
                            displayField: 'no_pol',
                            hideTrigger: true,
                            minChars: 2,
                            matchFieldWidth: !1,
                            pageSize: 20,
                            itemSelector: 'div.search-item',
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                '{customer_id}',
                                '</div></tpl>'
                            ),
                            // allowBlank: false,
                            listWidth: 450,
                            lastQuery: "",
                            width: 100
                        }, '-', 'Asuransi : ',
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode'
                            ],
                            // readOnly: (this.modez !== 0 && this.modez !== 1),
                            fieldLabel: 'Kode Asuransi',
                            mode: 'local',
                            store: jun.rztAsuransiBpCmp,
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode}</td><td>{nama}</td>',
                                '</tr></tpl></tbody></table>'),
                            // allowBlank: false,
                            listWidth: 500,
                            hideTrigger: true,
                            hiddenName: 'asuransi_bp_id',
                            valueField: 'asuransi_bp_id',
                            displayField: 'kode',
                            id: 'grid_asuransi_bp_id',
                            width: 100
                        },
                        '-', 'STATUS : ',
                        new Ext.form.ComboBox({
                            displayField: "desc",
                            id: 'cmb_status_grid',
                            valueField: "code",
                            typeAhead: !0,
                            mode: "local",
                            forceSelection: !0,
                            triggerAction: "all",
                            selectOnFocus: !0,
                            width: 120,
                            store: new Ext.data.ArrayStore({
                                id: 'cmb_status_grid',
                                fields: ["code", "desc"],
                                data: [
                                    ["", "SEMUA STATUS"],
                                    ["BELUM", "BELUM LUNAS"],
                                    ["SUDAH", "SUDAH LUNAS"],
                                ]
                            })
                        }),
                        '-',
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'UnClose',
                    ref: '../btnClose'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Generate Faktur',
                    ref: '../btnFaktur'
                },
            ]
        };
        jun.BodyPaintCloseGrid.superclass.initComponent.call(this);
        this.btnClose.on('Click', this.closeRec, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnFaktur.on('Click', this.generateFaktur, this);
        Ext.getCmp('filter_nopol_close_body_paint_id').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('grid_asuransi_bp_id').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('cmb_status_grid').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridclosebpfrom').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridclosebpto').on('select', function () {
            this.store.load();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        // jun.rztMobilBpCmp.baseParams = {
        //     mobil_id: this.record.data.mobil_id
        // };
        // jun.rztMobilBpCmp.load();
    },
    closeRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin close data ini?', this.closeRecYes, this);
    },
    closeRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BodyPaint/close/id/' + record.json.body_paint_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztCloseBodyPaint.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    generateFaktur: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin generete nomor faktur pajak?', function(btn){
            if (btn == 'no') {
                return;
            }
            Ext.Ajax.request({
                url: 'NoFaktur/generate',
                method: 'POST',
                params:{
                    tipe:2
                },
                success: function (f, a) {
                    jun.rztINVBodyPaint.reload();
                    jun.rztCloseBodyPaint.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }, this);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin hapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BodyPaint/delete/id/' + record.json.body_paint_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztINVBodyPaint.reload();
                jun.rztCloseBodyPaint.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BayarInvBp = Ext.extend(Ext.Window, {
    title: "Pembayaran Own Risk",
    // iconCls: "silk13-report",
    record: null,
    modez: 1,
    width: 350,
    height: 255,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-BayarInvBp",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'b1_tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        anchor: '100%',
                        allowBlank: false,
                        value: DATE_NOW
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        store: jun.rztBankBp,
                        hiddenName: 'b1_bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        hidden: (this.modez === 4),
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Banyak Kejadian',
                        hideLabel: false,
                        value: this.record.data.qty_kejadian,
                        readOnly: (this.modez === 4 ) ? true : !EDIT_QTY_KEJADIAN,
                        ref: '../qty_kejadian',
                        id: 'qty_kejadian_id',
                        name: 'qty_kejadian',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Nilai Own Risk',
                        hideLabel: false,
                        value: this.record.data.own_risk,
                        readOnly: true,
                        ref: '../own_risk',
                        id: 'own_risk_id',
                        name: 'own_risk',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total Own Risk',
                        hideLabel: false,
                        value: this.record.data.total_asuransi,
                        readOnly: true,
                        ref: '../total_asuransi',
                        name: 'b1_total',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'hidden',
                        name: 'modez',
                        value: this.modez
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-html",
                    text: "Save",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Cancel",
                    // hidden: (STORE != ''),
                    ref: "../btnCancel"
                },
            ]
        };
        this.height = (this.modez === 4 ) ? 195 : 225;
        jun.BayarInvBp.superclass.initComponent.call(this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.qty_kejadian.setValue(this.record.data.qty_kejadian);
        this.own_risk.setValue(this.record.data.own_risk);
        this.total_asuransi.setValue(this.record.data.total_asuransi);
        this.qty_kejadian.on("change", function(a,b,c){
            this.total_asuransi.setValue(parseFloat(b) * parseFloat(this.own_risk.getValue()));
        }, this);

    },
    onbtnCancelclick: function () {
        this.close();
    },
    onbtnSaveclick: function () {
        var grid = Ext.getCmp('docs-jun.BodyPaintINVGrid');
        var record = grid.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Invoice");
            return;
        }
        Ext.getCmp('form-BayarInvBp').getForm().submit({
            url: 'BodyPaint/bayar/id/' + record.json.body_paint_id,
            scope: this,
            success: function (f, a) {
                jun.rztINVBodyPaint.reload();
                // var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                // this.btnDisabled(false);
            }
        });
    }
});
jun.BpBayar2ListGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice",
    id: 'docs-jun.BpBayar2ListGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_no',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztBpBayar2List;
        jun.BpBayar2ListGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BpBayar2Win({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.BpBayar2Win({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BpBayar2/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBpBayar2.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
