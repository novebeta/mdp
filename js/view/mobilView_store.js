jun.MobilViewstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilViewstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilViewStoreId',
            url: 'MobilView',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_id'},
                {name: 'mobil_tipe_id'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'customer_id'},
                {name: 'nama_customer'},
                {name: 'alamat'},
                {name: 'tipe'},
                {name: 'merk'},
                {name: 'no_mesin'},
                {name: 'tahun_km'},
                {name: 'warna'}
            ]
        }, cfg));
    }
});
jun.rztMobilViewSalesCmp = new jun.MobilViewstore();
//jun.rztMobilView.load();
