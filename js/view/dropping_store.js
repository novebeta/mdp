jun.Droppingstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Droppingstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DroppingStoreId',
            url: 'Dropping',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'dropping_id'},
                {name: 'tgl', type: 'date'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'type_', type: 'int'},
                {name: 'store'},
                {name: 'order_dropping_id'},
                {name: 'store_penerima'},
                {name: 'approved', type: 'int'},
                {name: 'approved_user_id'},
                {name: 'approved_date', type: 'date'},
                {name: 'lunas', type: 'int'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztDropping = new jun.Droppingstore();
jun.rztDroppingCmp = new jun.Droppingstore();
//jun.rztDropping.load();
