jun.SuppFlazzstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SuppFlazzstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SuppFlazzStoreId',
            url: 'SuppFlazz',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'supp_id'},
                {name: 'nama_supp'},
                {name: 'email'},
                {name: 'telp'},
                {name: 'alamat'},
                {name: 'city'},
                {name: 'pt'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztSuppFlazz = new jun.SuppFlazzstore();
jun.rztSuppFlazzLib = new jun.SuppFlazzstore();
jun.rztSuppFlazzCmp = new jun.SuppFlazzstore();
jun.rztSuppFlazzLib.load();
