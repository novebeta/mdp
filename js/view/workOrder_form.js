jun.WorkOrderWin = Ext.extend(Ext.Window, {
    title: 'Pekerjaan Dalam Proses',
    modez: 1,
    width: 735,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-WorkOrder',
                layout: 'table',
                layoutConfig: {columns: 2},
                defaults: {width: 350},
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 45,
                        items: [
                            {
                                xtype: 'xdatefield',
                                ref: '../tgl_so',
                                fieldLabel: 'Tgl',
                                name: 'tgl_so',
                                id: 'tgl_soid',
                                format: 'd M Y',
                                allowBlank: 0,
                                anchor: '100%'
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 45,
                        items: [
                            {
                                xtype: 'combo',
                                fieldLabel: 'Mobil',
                                ref: '../mobil_id',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'remote',
                                forceSelection: true,
                                autoSelect: false,
                                store: jun.rztMobilViewSalesCmp,
                                id: "mobilsales_id",
                                hiddenName: 'mobil_id',
                                valueField: 'mobil_id',
                                displayField: 'no_pol',
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                itemSelector: 'div.search-item',
                                tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">',
                                    '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                    '{customer_id}',
                                    '</div></tpl>'
                                ),
                                allowBlank: false,
                                listWidth: 450,
                                lastQuery: "",
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        colspan: 2,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        // height: 220,
                        width: 700,
                        layout: 'form',
                        border: false,
                        items: [
                            new jun.SalesDetailMdpGrid({
                                hideToolbars: true,
                                // colspan: 2,
                                height: 150,
                                // width: 700,
                                frameHeader: !1,
                                header: !1,
                                ref: "../griddetil"
                            }),
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        frame: false,
                        border: false,
                        height: 120,
                        layout: {
                            type: 'vbox',
                            // padding: '5',
                            align: 'stretchmax'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                id: 'panel_mobil_info',
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;',
                                layout: {
                                    type: 'vbox',
                                    // padding: '5',
                                    align: 'stretchmax'
                                },
                                height: 100,
                                border: false,
                                ref: '../../mobil_info',
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Sub Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'sub_total',
                                id: 'sub_total_sales_id',
                                ref: '../sub_total',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Disc',
                                hideLabel: false,
                                //hidden:true,
                                name: 'totalpot',
                                id: 'total_pot_sales_id',
                                ref: '../totalpot',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'PPN',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total_vat',
                                id: 'total_vat_sales_id',
                                ref: '../total_vat',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total',
                                id: 'total_sales_id',
                                ref: '../total',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            }
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Simpan',
                //     hidden: false,
                //     ref: '../btnSave'
                // },
                {
                    xtype: 'button',
                    text: 'Finish Pekerjaan',
                    hidden: true,
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Revisi Pekerjaan',
                    hidden: true,
                    ref: '../btnRevisi'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.WorkOrderWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnRevisi.on('click', this.onbtnRevisiClick, this);
        this.on("close", this.onWinClose, this);
        // this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1) {
            this.btnSaveClose.setVisible(true);
        }
        if (this.modez == 2) {
            this.btnRevisi.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztSalesDetailMdp.removeAll();
        // jun.rztBarangView.clearFilter();
        // this.mobil_id.lastQuery = null;
    },
    btnDisabled: function (status) {
        this.btnRevisi.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        var form = new jun.UploadSalesOrderWin({modez: 0, workOrderId: this.id, id: 'woupload' + this.id});
        form.show();
        return;
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'WorkOrder/update/id/' + this.id;
        } else {
            urlz = 'WorkOrder/create/';
        }
        Ext.getCmp('form-WorkOrder').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztWorkOrder.reload();
                jun.rztWorkFinish.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-WorkOrder').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnRevisiClick: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin merevisi data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            url: 'WorkOrder/revisi/id/' + this.id,
            method: 'POST',
            scope: this,
            success: function (f, a) {
                jun.rztWorkOrderAll.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp(this.id).close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.UploadSalesOrderWin = new Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Upload Sales Order",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-UploadSalesOrderWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file import (*.jpeg)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                },
                // {
                //     xtype: "hidden",
                //     name: "sales_id",
                //     ref: "../sales_id"
                // },
                // {
                //     xtype: "hidden",
                //     name: "sales_id",
                // id: "workOrderIdUpload",
                // ref: "../workOrderId"
                // }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Upload",
                    hidden: !1,
                    ref: "../btnBackup"
                }
            ]
        };
        jun.UploadSalesOrderWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
    },
    onbtnBackupClick: function () {
        // Ext.getCmp(this.workOderId).close();
        // return;
        var fp = Ext.getCmp("form-UploadSalesOrderWin");
        if (fp.getForm().isValid()) {
            fp.getForm().submit({
                url: 'WorkOrder/Upload',
                params: {
                    sales_id: fp.refOwner.workOrderId
                },
                // waitMsg: 'Uploading your file...',
                // or using a progress bar
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    jun.rztWorkOrder.load();
                    Ext.getCmp(fp.refOwner.workOrderId).close();
                    Ext.getCmp('woupload' + fp.refOwner.workOrderId).close();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
        return;
        Ext.getCmp("form-UploadSalesOrderWin").getForm().reset();
        Ext.getCmp("form-UploadSalesOrderWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-UploadSalesOrderWin").getForm().url = "jual/export";
        var form = Ext.getCmp('form-UploadSalesOrderWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
});