jun.ReceiveDroppingstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReceiveDroppingstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReceiveDroppingStoreId',
            url: 'ReceiveDropping',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'receive_dropping_id'},
                {name: 'tgl', type: 'date'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'dropping_id'},
                {name: 'store_pengirim'},
                {name: 'lunas', type: 'int'},
                {name: 'up', type: 'int'}
            ]
        }, cfg));
    }
});
jun.rztReceiveDropping = new jun.ReceiveDroppingstore();
//jun.rztReceiveDropping.load();
