jun.DroppingRecallGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Recall",
    id: 'docs-jun.DroppingRecallGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 30,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case PR_OPEN :
                        return 'OPEN';
                    case PR_PROCESS :
                        metaData.style += "background-color: #FCFC98;";
                        return 'PROCESS';
                    case PR_CLOSED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['myId', 'displayText'],
                    data: [['all', 'ALL'], [0, 'OPEN'], [2, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Up',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 30,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case DR_PENDING :
                        metaData.style += "background-color: #FF1A1A;";
                        return 'PENDING';
                    case DR_SEND :
                        metaData.style += "background-color: #FFFF33;";
                        return 'SEND';
                    case DR_APPROVE :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'APPROVE';
                    case DR_PROCESS :
                        metaData.style += "background-color: #00FF00;";
                        return 'PROCESS';
                    case DR_RECEIVE :
                        metaData.style += "background-color: #FF4D88;";
                        return 'RECEIVE';
                    case DR_CLOSE :
                        metaData.style += "background-color: #ADAD85;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'PENDING'], [1, 'SEND'], [2, 'APPROVE'], [3, 'PROCESS'], [4, 'RECEIVE'], [5, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if(jun.rztStoreCmp.getTotalCount()==0) jun.rztStoreCmp.load();
        if(jun.rztBarangLib.getTotalCount()==0) jun.rztBarangLib.load();
        if(jun.rztBarangNonJasaAll.getTotalCount()==0) jun.rztBarangNonJasaAll.load();
        
        this.store = new jun.DroppingRecallStore({
            baseParams: {mode: "grid"}
        });
        this.store.reload();

        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit',
                    iconCls: 'silk13-pencil'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnClose',
                    iconCls: 'silk13-flag_blue'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Resend',
                    ref: '../btnResend',
                    iconCls: 'silk13-arrow_refresh'
                }
            ]
        };
        
        jun.DroppingRecallGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnClose.on('Click', this.btnCloseOnClick, this);
        this.btnResend.on('Click', this.loadResend, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;

        var filterEdit = this.record.get('store') == STORE && this.record.get('status') == PR_OPEN;
        this.btnEdit.setIconClass(filterEdit ?'silk13-pencil':'silk13-eye');
        this.btnEdit.setText(filterEdit ?"Edit":"View");

        this.btnClose.setDisabled(this.record.get('status') == PR_CLOSED);
    },
    loadForm: function () {
        var form = new jun.DroppingRecallWin({
            modez: 0,
            title: 'Create '+this.title
        });
        form.show();
        //form.card1.layout.setActiveItem(1);

        form.gridAllDetail.store.baseParams = { store: STORE };
        form.gridAllDetail.store.load();
    },
    loadEditForm: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data "+this.title+".");
            return;
        }

        var filter = this.record.get('store') == STORE && parseInt(this.record.get('status')) == PR_OPEN;
        var form = new jun.DroppingRecallWin({
            modez: (filter ? 1 : 2),
            dropping_recall_id: this.record.get('dropping_recall_id'),
            title: (filter ? "Edit " : "Show ")+this.title
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);

        form.gridDetail.store.baseParams = { dropping_recall_id: this.record.get('dropping_recall_id') };
        form.gridDetail.store.load();

        form.gridAllDetail.store.baseParams = { store: STORE, dropping_recall_id: this.record.get('dropping_recall_id') };
    },
    btnCloseOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Close "+this.title, "Anda belum memilih data "+this.title+".");
            return;
        }
        if (this.record.get('store') != STORE){
            Ext.MessageBox.alert("Close "+this.title, "Anda tidak diperbolehkan untuk menutup data ini.");
            return;
        }
        if (this.record.get('status') == PR_CLOSED){
            Ext.MessageBox.alert("Close "+this.title, "Data telah di-close.");
            return;
        }
        Ext.MessageBox.confirm("Close "+this.title, 'Apakah anda yakin ingin menutup data ini?', this.closeDroppingRecall, this);
    },
    closeDroppingRecall: function (btn) {
        if (btn == 'no') {
            return;
        }

        Ext.Ajax.request({
            url: 'DroppingRecall/Close',
            method: 'POST',
            params: {
                dropping_recall_id: this.record.get('dropping_recall_id')
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: "Close "+this.title,
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadResend: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data "+this.title+".");
            return;
        }

        Ext.Ajax.request({
            url: 'DroppingRecall/Resend/',
            method: 'POST',
            params: {
                dropping_recall_id: this.record.get('dropping_recall_id')
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: "Resend "+this.title,
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
});
