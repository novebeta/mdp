jun.SalesDetailMdpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesDetailMdpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesDetailMdpStoreId',
            url: 'SalesDetailMdp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_detail_id'},
                {name: 'sales_id'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'harga', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'tot_qty_harga', type: 'float'},
                {name: 'tot_after_disc', type: 'float'},
                {name: 'woug', type: 'float'},
                {name: 'woug_pot', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var tot_qty_harga = this.sum("tot_qty_harga");
        var discrp = this.sum("discrp");
        var vatrp = this.sum("vatrp");
        var total = this.sum("total");
        Ext.getCmp('sub_total_sales_id').setValue(tot_qty_harga);
        Ext.getCmp('total_pot_sales_id').setValue(discrp);
        Ext.getCmp('total_vat_sales_id').setValue(vatrp);
        Ext.getCmp('total_sales_id').setValue(total);
    }
});
jun.rztSalesDetailMdp = new jun.SalesDetailMdpstore();
