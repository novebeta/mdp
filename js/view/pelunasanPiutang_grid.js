jun.PelunasanPiutangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pelunasan Piutang",
    id: 'docs-jun.PelunasanPiutangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header:'Supplier',
            sortable:true,
            resizable:true,
            dataIndex:'supplier_id',
            width:100,
            renderer: jun.renderSupplier
        },
        {
            header: 'Receipt No.',
            sortable: true,
            resizable: true,
            dataIndex: 'no_bukti',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        if (jun.rztBankCmpPusat.getTotalCount() === 0) {
            jun.rztBankCmpPusat.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        jun.rztPelunasanPiutang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tglPelunasanUtangid').getValue();
                    var tgl = Ext.getCmp('tglPelunasanPiutangid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztPelunasanPiutang;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Account Receiveable',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Account Receiveable',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglPelunasanPiutangid',
                    ref: '../tgl'
                }
            ]
        };
        jun.PelunasanPiutangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PelunasanPiutangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pelunasan_piutang_id;
        var form = new jun.PelunasanPiutangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPelunasanPiutangDetil.baseParams = {
            pelunasan_piutang_id: idz
        };
        jun.rztPelunasanPiutangDetil.load();
        jun.rztPelunasanPiutangDetil.baseParams = {};
    }
});
