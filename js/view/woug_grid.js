jun.WougGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Work Order Ultra Grand",
    id: 'docs-jun.WougGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: false}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. WO',
            sortable: true,
            resizable: true,
            dataIndex: 'no_woug',
            width: 100
        },
        {
            header: 'Tgl WO',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_woug',
            width: 100
        },
        {
            header: 'Nama Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
            width: 100
        },
        {
            header: 'No. Rangka',
            sortable: true,
            resizable: true,
            dataIndex: 'no_rangka',
            width: 100
        },
        // {
        //     header:'warna',
        //     sortable:true,
        //     resizable:true,
        //     dataIndex:'warna',
        //     width:100
        // },
        {
            header: 'No. Mesin',
            sortable: true,
            resizable: true,
            dataIndex: 'no_mesin',
            width: 100
        },
        // {
        //     header: 'woug_persen',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'woug_persen',
        //     width: 100
        // },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'wo_hutang',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Reseller',
            sortable: true,
            resizable: true,
            dataIndex: 'store_kode',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'invoice_ug_id',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'LUNAS'
                } else {
                    return 'BELUM BAYAR'
                }
            }
        }
        // {
        //     header: 'mobil_km',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'mobil_km',
        //     width: 100
        // },
        /*
{
header:'tahun_pembuatan',
sortable:true,
resizable:true,
dataIndex:'tahun_pembuatan',
width:100
},
                {
header:'mulai_stnk',
sortable:true,
resizable:true,
dataIndex:'mulai_stnk',
width:100
},
                {
header:'no_rangka',
sortable:true,
resizable:true,
dataIndex:'no_rangka',
width:100
},
                {
header:'no_pol',
sortable:true,
resizable:true,
dataIndex:'no_pol',
width:100
},
                {
header:'warna',
sortable:true,
resizable:true,
dataIndex:'warna',
width:100
},
                {
header:'no_mesin',
sortable:true,
resizable:true,
dataIndex:'no_mesin',
width:100
},

        */
    ],
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() == 0) jun.rztBankTransCmpPusat.load();
        this.store = jun.rztWoug;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Pembayaran UG',
                    ref: '../btnAdd'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Ubah',
                //     ref: '../btnEdit'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.WougGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var selectedz = this.sm.getSelections();
        if (selectedz.length < 1) {
            Ext.MessageBox.alert("Warning", "Minimal 1 Work Order yang harus di lunasi.");
            return;
        }
        jun.rztWougList.removeAll();
        var form = new jun.WougWin({modez: 0});
        selectedz.forEach(function (t, i) {
            if (t.json.invoice_ug_id != null) {
                return;
            }
            var c = jun.rztWougList.recordType;
            var d = new c({
                no_woug: t.json.no_woug,
                nama_customer: t.json.nama_customer,
                woug_total: parseFloat(t.data.wo_hutang),
                sales_id: t.json.sales_id
            });
            jun.rztWougList.add(d);
            jun.rztWougList.commitChanges();
        });
        if (jun.rztWougList.getCount() > 0) {
            form.show();
        } else {
            Ext.MessageBox.alert("Warning", "Minimal 1 Work Order yang harus di lunasi.");
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelections();
        if (selectedz.length < 1) {
            Ext.MessageBox.alert("Warning", "Minimal 1 Work Order yang harus di lunasi.");
            return;
        }
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WougWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Woug/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztWoug.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.WougListGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Work Order Ultra Grand",
    id: 'docs-jun.WougListGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. WO',
            sortable: true,
            resizable: true,
            dataIndex: 'no_woug',
            width: 100
        },
        {
            header: 'Nama Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        // {
        //     header: 'No. Rangka',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'no_rangka',
        //     width: 100
        // },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'woug_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztWougList;
        jun.WougListGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.WougWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WougWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Woug/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztWoug.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

