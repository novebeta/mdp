jun.HistoryPasienGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"History",
        id:'docs-jun.HistoryPasienGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'id_pasien',
			sortable:true,
			resizable:true,                        
            dataIndex:'id_pasien',
			width:100
		},*/
/*                                {
			header:'nokartu',
			sortable:true,
			resizable:true,                        
            dataIndex:'nokartu',
			width:100,
                                    filter: {xtype: "textfield"}
		},*/
                                {
			header:'No. Customer',
			sortable:true,
			resizable:true,                        
            dataIndex:'nobase',
			width:100,
                                    filter: {xtype: "textfield"}
		},

                                {
			header:'Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'namacus',
			width:100,
                                    filter: {xtype: "textfield"}
		},

        {
            header:'Telepon',
            sortable:true,
            resizable:true,
            dataIndex:'telp',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Alamat',
            sortable:true,
            resizable:true,
            dataIndex:'alamat',
            width:100,
            filter: {xtype: "textfield"}
        },
        /*{
            header:'Email',
            sortable:true,
            resizable:true,
            dataIndex:'email',
            width:100,
            filter: {xtype: "textfield"}
        },*/
        {
            header:'Kota',
            sortable:true,
            resizable:true,
            dataIndex:'kota',
            width:100
        },
        {
            header:'Sex',
            sortable:true,
            resizable:true,
            dataIndex:'sex',
            width:100
        },
        {
            header:'Tgllh',
            sortable:true,
            resizable:true,
            dataIndex:'tgllh',
            width:100
        },
        {
            header:'Usia',
            sortable:true,
            resizable:true,
            dataIndex:'usia',
            width:100
        },
        {
            header:'Kerja',
            sortable:true,
            resizable:true,
            dataIndex:'kerja',
            width:100
        },
        {
            header:'Awal Transaksi',
            sortable:true,
            resizable:true,
            dataIndex:'awal',
            width:100
        },
                		/*
                {
			header:'linedisc',
			sortable:true,
			resizable:true,                        
            dataIndex:'linedisc',
			width:100
		},


                                {
			header:'kota',
			sortable:true,
			resizable:true,                        
            dataIndex:'kota',
			width:100
		},
                                {
			header:'telp',
			sortable:true,
			resizable:true,                        
            dataIndex:'telp',
			width:100,
			filter: {xtype: "textfield"}
		},
                                {
			header:'jnscus',
			sortable:true,
			resizable:true,                        
            dataIndex:'jnscus',
			width:100
		},
                                {
			header:'tgllh',
			sortable:true,
			resizable:true,                        
            dataIndex:'tgllh',
			width:100
		},
                                {
			header:'sex',
			sortable:true,
			resizable:true,                        
            dataIndex:'sex',
			width:100
		},
                                {
			header:'usia',
			sortable:true,
			resizable:true,                        
            dataIndex:'usia',
			width:100
		},
                                {
			header:'kerja',
			sortable:true,
			resizable:true,                        
            dataIndex:'kerja',
			width:100
		},
                                {
			header:'awal',
			sortable:true,
			resizable:true,                        
            dataIndex:'awal',
			width:100
		},
                                {
			header:'akhir',
			sortable:true,
			resizable:true,                        
            dataIndex:'akhir',
			width:100
		},
                                {
			header:'akhirke',
			sortable:true,
			resizable:true,                        
            dataIndex:'akhirke',
			width:100
		},
                                {
			header:'saldo',
			sortable:true,
			resizable:true,                        
            dataIndex:'saldo',
			width:100
		},
                                {
			header:'dtgl',
			sortable:true,
			resizable:true,                        
            dataIndex:'dtgl',
			width:100
		},
                                {
			header:'jmlcp',
			sortable:true,
			resizable:true,                        
            dataIndex:'jmlcp',
			width:100
		},
                                {
			header:'id_cabang',
			sortable:true,
			resizable:true,                        
            dataIndex:'id_cabang',
			width:100
		},
                                {
			header:'id_source',
			sortable:true,
			resizable:true,                        
            dataIndex:'id_source',
			width:100
		},
                                {
			header:'aktif',
			sortable:true,
			resizable:true,                        
            dataIndex:'aktif',
			width:100
		},
                                {
			header:'createddate',
			sortable:true,
			resizable:true,                        
            dataIndex:'createddate',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztHistoryPasien;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Show History',
                        ref: '../btnHistory',
                        iconCls: "silk13-status_away"
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Copy Customer',
                        ref: '../btnCopyCustomer',
                        iconCls: "silk13-database_save"
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: '************************* CUSTOMER ULANG TAHUN DI BULAN INI *************************',
                        ref: '../ultah',
                        hidden: true,
                        iconCls: "silk13-cake"
                    }

                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.HistoryPasienGrid.superclass.initComponent.call(this);
	        this.btnHistory.on('Click', this.showHistory, this);
                this.btnCopyCustomer.on('Click', this.copyCustomers, this);
                //this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
                this.on('rowdblclick', this.hisshow, this);
	},

    hisshow: function()
    {
        if(EMPATKASIR == '1')
        {
            var selectedz = this.sm.getSelected();
            var nobase = selectedz.data.nobase;

            Ext.getCmp('docs-jun.PasienHistoryWin').load({
                url: 'customers/SimpleHistory/nobase/' + nobase,
                scripts: true,
                params: {
                    nobase: nobase,
                    height: Ext.getCmp('docs-jun.PasienHistoryWin').getHeight() - 100
                }
            });
        }
        else {
            var selectedz = this.sm.getSelected();
            //var dodol = this.store.getAt(0);
            if (selectedz == undefined) {
                Ext.MessageBox.alert("Warning", "You have not selected a customer");
                return;
            }
            var tabpanel = this.findParentByType('tabpanel');
            var tab = tabpanel.getComponent('docs-jun.PasienHistoryWin');
            if (tab) {
                tabpanel.setActiveTab(tab);
            } else {
                var obj = eval('jun.PasienHistoryWin');
                var object = new obj({id: 'docs-jun.PasienHistoryWin', closable: !0});
                var p = tabpanel.add(object);
                tabpanel.setActiveTab(p);
                tab = p;
            }
            tab.no_customer.setValue(selectedz.data.nobase);
            tab.load({
                url: 'customers/SimpleHistory/nobase/' + selectedz.data.nobase,
                scripts: true,
                params: {
                    nobase: selectedz.data.nobase,
                    customer_id: selectedz.data.customer_id,
                    height: tab.getHeight() - 100
                }
            });
        }
    },

    getrow: function (sm, idx, r)
    {
        this.record = r;
        var selectedz = this.sm.getSelections();

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();


        var getTgl = r.get('tgllh');
        var tgl = new Date(getTgl);
        var getM = tgl.getMonth()+1;
        var filterSync = getM == mm;
        this.ultah.setVisible(filterSync);
        if(filterSync)
        {
            alert("CUSTOMER ULANG TAHUN DI BULAN INI !!!");
        }

    },


    showHistory: function () {

        if(EMPATKASIR == '1')
        {
            var selectedz = this.sm.getSelected();
            var nobase = selectedz.data.nobase;

            Ext.getCmp('docs-jun.PasienHistoryWin').load({
                url: 'customers/SimpleHistory/nobase/' + nobase,
                scripts: true,
                params: {
                    nobase: nobase,
                    height: Ext.getCmp('docs-jun.PasienHistoryWin').getHeight() - 100
                }
            });
        }
        else {
             var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var tabpanel = this.findParentByType('tabpanel');
        var tab = tabpanel.getComponent('docs-jun.PasienHistoryWin');
        if (tab) {
            tabpanel.setActiveTab(tab);
        } else {
            var obj = eval('jun.PasienHistoryWin');
            var object = new obj({id: 'docs-jun.PasienHistoryWin', closable: !0});
            var p = tabpanel.add(object);
            tabpanel.setActiveTab(p);
            tab = p;
        }
        tab.no_customer.setValue(selectedz.data.nobase);
        tab.load({
            url: 'customers/SimpleHistory/nobase/' + selectedz.data.nobase,
            scripts: true,
            params: {
                nobase: selectedz.data.nobase,
                customer_id: selectedz.data.customer_id,
                height: tab.getHeight() - 100
            }
        });
        }


    },

    copyCustomers: function () {
        Ext.MessageBox.prompt('Confirm', 'Masukkan No Base atau No Customers', this.prosesCopy);
    },
    prosesCopy: function (btn, txt) {
        if (btn != 'ok') {
            return;
        }
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan No Pasien atau No Base. Misal : 1JOG01',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/Soapcopy',
            method: 'POST',
            scope: this,
            params: {
                no_base: txt
            },
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

        loadForm: function(){
            var form = new jun.HistoryPasienWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.id_pasien;
            var form = new jun.HistoryPasienWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'HistoryPasien/delete/id/' + record.json.id_pasien,
                method: 'POST',
                success:function (f, a) {
                    jun.rztHistoryPasien.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
