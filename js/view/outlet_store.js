jun.Outletstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Outletstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'OutletStoreId',
            url: 'Outlet',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'outlet_id'},
{name:'code'},
{name:'name'},
{name:'address'},
{name:'city'},
{name:'postal_code'},
{name:'phone'},
{name:'email'},
{name:'open'},
{name:'close'},
{name:'status'},
                
            ]
        }, cfg));
    }
});
jun.rztOutlet = new jun.Outletstore();
//jun.rztOutlet.load();
