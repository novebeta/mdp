/*
 * 'Purchase Order'
 * - Menu untuk mengelola PO
 * - Menu untuk divisi Purchasing
 */
jun.PoGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Order",
    id: 'docs-jun.PoGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. PR',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_pr',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supp_company',
            width: 120,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama tujuan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Divisi',
            sortable: true,
            resizable: true,
            dataIndex: 'divisi',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 40,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Dibuat oleh',
            sortable: true,
            resizable: true,
            dataIndex: 'username',
            width: 60,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case PO_OPEN :
                        //metaData.style += "background-color: #AAFFD4;";
                        return 'OPEN';
                    case PO_PARTIALLY_RECEIVED :
                        metaData.style += "background-color: #FCFC98;";
                        return 'PARTIAL RCVD';
                    case PO_RECEIVED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'RECEIVED';
                    case PO_CLOSED :
                        metaData.style += "background-color: #AAD4FF;";
                        return 'CLOSED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['myId','displayText'],
                    data: [['all', 'ALL'], [0, 'OPEN'], [1, 'PARTIAL RCVD'], [2, 'RECEIVED'], [3, 'CLOSED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztPO;
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBarangPurchasable.getTotalCount() === 0) {
            jun.rztBarangPurchasable.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create PO',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Create Retur',
                    ref: '../btnAddRetur'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit PO',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print PO',
                    ref: '../btnPrintPO',
                    iconCls: 'silk13-page_white_excel'
                },
                {
                    xtype: 'button',
                    text: 'Print Rekap PO',
                    ref: '../btnPrintRekapPO',
                    iconCls: 'silk13-page_white_excel'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Close PO',
                    ref: '../btnClose',
                    iconCls: 'silk13-flag_blue'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Clone PO',
                    ref: '../btnClonePO',
                    iconCls: 'silk13-page_white_copy'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-PrintPO",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "po_id",
                            ref: "../../po_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };

        this.contextMenu = new Ext.menu.Menu({
            plain: true,
            items: [
                {
                    ref: 'menuClone',
                    text: 'Clone',
                    iconCls: 'silk13-page_white_copy'
                }
            ]
        });
        
        jun.PoGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnAddRetur.on('Click', this.loadFormRetur, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnClose.on('Click', this.btnCloseOnClick, this);
        this.btnPrintPO.on('Click', this.printPurchaseOrder, this);
        this.btnClonePO.on('Click', this.clonePo, this);
        this.btnPrintRekapPO.on('Click', this.printRekapPurchaseOrder, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);

        this.on('rowcontextmenu', this.showContextMenu, this);
        this.contextMenu.menuClone.on('click', this.clonePo, this);
        
        this.store.baseParams = {
            mode: "grid",
            viewAllData: 1
        };
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        
        this.btnEdit.setText(r.get('status') != PO_CLOSED ?"Edit PO":"View PO");
        this.btnClose.setDisabled(r.get('status') == PO_CLOSED);
        this.btnPrintPO.setDisabled(r.get('status') == PO_CLOSED);
    },
    loadForm: function () {
        var form = new jun.PoWin({
            modez: 0,
            title: "Create Purchase Order",
            storeDetail: jun.rztPoDetails,
            type_: 1
        });
        form.show();
    },
    loadFormRetur: function () {
        var form = new jun.PoWin({
            modez: 0,
            title: "Create Retur",
            storeDetail: jun.rztPoDetails,
            type_: -1
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Purchase Order.");
            return;
        }
        
        var ttl = "";
        switch(Number(selectedz.json.status)){
            case PO_OPEN : ttl = "OPEN"; break;
            case PO_PARTIALLY_RECEIVED : ttl = "PARTIALLY RECEIVED"; break;
            case PO_RECEIVED : ttl = "RECEIVED"; break;
            case PO_CLOSED : ttl = "CLOSED"; break;
        }
        
        var filter = (selectedz.json.status != PO_CLOSED);
        var form = new jun.PoWin({
            modez: (filter ? 1 : 2),
            title: ( filter ? "Edit" : "View")+" Purchase Order ["+ttl+"]",
            storeDetail: jun.rztPoDetails,
            pr_id: selectedz.json.pr_id,
            po_id: selectedz.json.po_id,
            type_: selectedz.json.type_
        });
        
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        
        jun.rztPoDetails.baseParams = {
            po_id: selectedz.json.po_id
        };
        jun.rztPoDetails.load();
        jun.rztPoDetails.baseParams = {};
    },
    clonePo: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Clone Purchase Order", "Anda belum memilih Data Purchase Order.");
            return;
        }

        var form = new jun.PoWin({
            modez: 0,
            title: "Create Purchase Order",
            storeDetail: jun.rztPoDetails,
            type_: selectedz.json.type_
        });

        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.doc_ref.reset();
        form.doc_ref_pr.reset();
        form.tgl.reset();
        form.tgl_delivery.reset();

        jun.rztPoDetails.baseParams = {
            po_id: selectedz.json.po_id
        };
        jun.rztPoDetails.load();
        jun.rztPoDetails.baseParams = {};
    },
    showContextMenu: function (c, idx, e) {
        e.stopEvent();
        this.sm.selectRow(idx);
        this.contextMenu.showAt(e.xy);
    },
    btnCloseOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Purchase Order.");
            return;
        }
        if (Number(this.sm.getSelected().json.status) == PO_CLOSED){
            Ext.MessageBox.alert("Warning", "Purchase Order telah berstatus CLOSED.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin akan menutup Purchase Order <b>'+this.sm.getSelected().json.doc_ref+'</b>?', this.closePO, this);
    },
    closePO: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'PoIn/Close',
            method: 'POST',
            params: {
                po_id: record.json.po_id
            },
            success: function (f, a) {
                jun.rztPO.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printPurchaseOrder: function(){
        var selectedz = this.sm.getSelected();
        
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Purchase Order");
            return;
        }
        if (Number(this.sm.getSelected().json.status) == PO_CLOSED){
            Ext.MessageBox.alert("Warning", "Purchase Order telah berstatus CLOSED.");
            return;
        }
        
        Ext.getCmp("form-PrintPO").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintPO").getForm().url = "Report/PrintPurchaseOrder";
        this.po_id.setValue(selectedz.json.po_id);
        var form = Ext.getCmp('form-PrintPO').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    printRekapPurchaseOrder: function(){
        var form = new jun.ReportRekapPurchaseOrder();
        form.show();
    }
});
