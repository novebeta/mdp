jun.GrupAttrstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.GrupAttrstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GrupAttrStoreId',
            url: 'GrupAttr',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name:'grup_attr_id'},
                {name:'vat'},
                {name:'tax'},
                {name:'coa_jual'},
                {name:'coa_sales_disc'},
                {name:'coa_sales_hpp'},
                {name:'coa_purchase'},
                {name:'coa_purchase_return'},
                {name:'coa_purchase_disc'},
                {name:'store'},
                {name:'up'},
                {name:'grup_id'},
                {name:'coa_jual_return'}
            ]
        }, cfg));
    }
});
jun.rztGrupAttr = new jun.GrupAttrstore();
jun.rztGrupAttrLib = new jun.GrupAttrstore();
jun.rztGrupAttrCmp = new jun.GrupAttrstore();
//jun.rztGrupAttr.load();
