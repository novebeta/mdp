jun.BarangFlazzWin = Ext.extend(Ext.Window, {
    title: 'Barang',
    modez: 1,
    width: 400,
    height: 260,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BarangFlazz',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Barang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang',
                        id: 'kode_barangid',
                        ref: '../kode_barang',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Barang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_barang',
                        id: 'nama_barangid',
                        ref: '../nama_barang',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Grup',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        //forceSelection: true,
                        store: jun.rztGrupLib,
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        ref: '../grup',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        boxLabel: "PPN",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "ppn"
                    },
                    {
                        xtype: 'combo',
                        lazyRender: true,
                        style: 'margin-bottom:2px',
                        mode: 'local',
                        enableKeyEvents: true,
                        forceSelection: true,
                        fieldLabel: 'Persediaan',
                        store: jun.rztChartMasterCmp,
                        ref: '../persediaan',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'persediaan',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        lazyRender: true,
                        style: 'margin-bottom:2px',
                        mode: 'local',
                        enableKeyEvents: true,
                        forceSelection: true,
                        fieldLabel: 'HPP',
                        store: jun.rztChartMasterCmp,
                        ref: '../hpp',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'hpp',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        lazyRender: true,
                        style: 'margin-bottom:2px',
                        mode: 'local',
                        enableKeyEvents: true,
                        forceSelection: true,
                        fieldLabel: 'Sales',
                        store: jun.rztChartMasterCmp,
                        ref: '../sales',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'sales',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        anchor: '100%'
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BarangFlazzWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'BarangFlazz/update/id/' + this.id;
        } else {
            urlz = 'BarangFlazz/create/';
        }
        Ext.getCmp('form-BarangFlazz').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBarangFlazz.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BarangFlazz').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});