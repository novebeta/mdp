jun.CustomersLinkagestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.CustomersLinkagestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomersLinkageStoreId',
            url: 'Customerslinkage',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'linkage_id'},
                {name:'customers_id'},
                {name:'online_id'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'telp'},
                {name: 'store'},
            ]
        }, cfg));
    }
});
jun.rztCustomersLinkage = new jun.CustomersLinkagestore();
//jun.rztCustomersLinkage.load();
