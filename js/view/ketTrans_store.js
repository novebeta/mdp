jun.KetTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KetTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KetTransStoreId',
            url: 'KetTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'akronim'},
                {name: 'keterangan'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztKetTrans = new jun.KetTransstore();
jun.rztKetTransLib = new jun.KetTransstore();
jun.rztKetTransCmp = new jun.KetTransstore();
