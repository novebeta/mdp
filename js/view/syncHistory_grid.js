jun.SyncHistoryGrid=Ext.extend(Ext.grid.GridPanel ,{
    title:"SyncHistory",
    id:'docs-jun.SyncHistoryGrid',
    iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    plugins:[new Ext.ux.grid.GridHeaderFilters],
    columns:[

        {
            header:'User',
            sortable:true,
            resizable:true,
            dataIndex:'username',
            width:100
        },
        {
            header:'Type',
            sortable:true,
            resizable:true,
            dataIndex:'type',
            width:100,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #33ff33;";
                        return 'SCAN';
                    case 1 :
                        metaData.style += "background-color: #66ccff;";
                        return 'SYNC';
                }
            },
        },
        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'tdate',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/M/Y H:i:s')
        },
        {
            header:'Start',
            sortable:true,
            resizable:true,
            dataIndex:'startdate',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        },
        {
            header:'End',
            sortable:true,
            resizable:true,
            dataIndex:'enddate',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/M/Y'),
        },
        {
            header:'Action',
            sortable:true,
            resizable:true,
            dataIndex:'action',
            width:100
        },
        /*                        {
			header:'note',
			sortable:true,
			resizable:true,
            dataIndex:'note',
			width:100
		},*/


    ],
    initComponent: function(){
        this.store = jun.rztSyncHistory;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    ref:"../paging"
                }]
        };

        jun.SyncHistoryGrid.superclass.initComponent.call(this);

    },

})
