jun.BpBayar2Win = Ext.extend(Ext.Window, {
    title: 'Pembayaran 2',
    modez: 1,
    width: 500,
    height: 460,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BpBayar2',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    new jun.BpBayar2ListGrid({
                        height: 247,
                        ref: '../BpBayar2ListGrid',
                        frameHeader: !1,
                        header: !1,
                    }),
                    {
                        style: 'margin-top:5px',
                        xtype: 'xdatefield',
                        ref: '../tgl_bayar',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tgl_bayarid',
                        format: 'd M Y',
                        width: 175,
                        // readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        ref: '../bank',
                        store: jun.rztBankBp,
                        allowBlank: false,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 275,
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode'
                        ],
                        readOnly:true,
                        fieldLabel: 'Kode Asuransi',
                        mode: 'local',
                        store: jun.rztAsuransiBpCmp,
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode</th><th>Nama</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode}</td><td>{nama}</td>',
                            '</tr></tpl></tbody></table>'),
                        // allowBlank: false,
                        listWidth: 500,
                        width: 275,
                        hideTrigger: true,
                        value: Ext.getCmp('grid_asuransi_bp_id').getValue(),
                        hiddenName: 'asuransi_bp_id',
                        valueField: 'asuransi_bp_id',
                        displayField: 'kode'
                    },
                    {

                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Total',
                        //hidden:true,
                        id: 'totalBpBayar2id',
                        name: 'total',
                        ref: '../total',
                        maxLength: 30,
                        // readOnly: true,
                        width: 275
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalBpTagih2id',
                        name: 'totalTagihan',
                        ref: '../totalTagih',
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BpBayar2Win.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on('close', this.onClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
        }
    },
    onClose: function () {
        jun.rztBpBayar2List.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'BodyPaint/bayar2/';
        if (parseFloat(this.total.getValue()) <= 0) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Total harus lebih besar dari nol.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            this.btnDisabled(true);
            return;
        }
        if (parseFloat(this.total.getValue()) > parseFloat(this.totalTagih.getValue())) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Total bayar tidak bisa lebih dari tagihan.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            this.btnDisabled(true);
            return;
        }
        if (this.bank.getValue() == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Kas/Bank untuk pembayaran ini harus dipilih.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            this.btnDisabled(true);
            return;
        }
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'BpBayar2/update/id/' + this.id;
        // } else {
        //     urlz = 'BpBayar2/create/';
        // }
        Ext.getCmp('form-BpBayar2').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztBpBayar2List.data.items, "data")),
            },
            success: function (f, a) {
                jun.rztINVBodyPaint.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BpBayar2').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
// jun.BpBayar2Win = Ext.extend(Ext.Window, {
//     title: 'BpBayar2',
//     modez: 1,
//     width: 400,
//     height: 300,
//     layout: 'form',
//     modal: true,
//     padding: 5,
//     closeForm: false,
//     initComponent: function () {
//         this.items = [
//             {
//                 xtype: 'form',
//                 frame: false,
//                 bodyStyle: 'background-color: #E4E4E4; padding: 10px',
//                 id: 'form-BpBayar2',
//                 labelWidth: 100,
//                 labelAlign: 'left',
//                 layout: 'form',
//                 ref: 'formz',
//                 border: false,
//                 items: [
//                     {
//                         xtype: 'xdatefield',
//                         ref: '../tgl',
//                         fieldLabel: 'tgl',
//                         name: 'tgl',
//                         id: 'tglid',
//                         format: 'd M Y',
//                         //allowBlank: 1,
//                         anchor: '100%'
//                     },
//                     {
//                         xtype: 'textfield',
//                         fieldLabel: 'total',
//                         hideLabel: false,
//                         //hidden:true,
//                         name: 'total',
//                         id: 'totalid',
//                         ref: '../total',
//                         maxLength: 15,
//                         //allowBlank: 1,
//                         anchor: '100%'
//                     },
//                     {
//                         xtype: 'textfield',
//                         fieldLabel: 'bank_id',
//                         hideLabel: false,
//                         //hidden:true,
//                         name: 'bank_id',
//                         id: 'bank_idid',
//                         ref: '../bank_id',
//                         maxLength: 36,
//                         //allowBlank: 1,
//                         anchor: '100%'
//                     },
//                     {
//                         xtype: 'textfield',
//                         fieldLabel: 'id_user',
//                         hideLabel: false,
//                         //hidden:true,
//                         name: 'id_user',
//                         id: 'id_userid',
//                         ref: '../id_user',
//                         maxLength: 50,
//                         //allowBlank: 1,
//                         anchor: '100%'
//                     },
//                 ]
//             }];
//         this.fbar = {
//             xtype: 'toolbar',
//             items: [
//                 {
//                     xtype: 'button',
//                     text: 'Simpan',
//                     hidden: false,
//                     ref: '../btnSave'
//                 },
//                 {
//                     xtype: 'button',
//                     text: 'Simpan & Tutup',
//                     ref: '../btnSaveClose'
//                 },
//                 {
//                     xtype: 'button',
//                     text: 'Batal',
//                     ref: '../btnCancel'
//                 }
//             ]
//         };
//         jun.BpBayar2Win.superclass.initComponent.call(this);
// //        this.on('activate', this.onActivate, this);
//         this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
//         this.btnSave.on('click', this.onbtnSaveclick, this);
//         this.btnCancel.on('click', this.onbtnCancelclick, this);
//         if (this.modez == 1 || this.modez == 2) {
//             this.btnSave.setVisible(false);
//         } else {
//             this.btnSave.setVisible(true);
//         }
//     },
//     btnDisabled: function (status) {
//         this.btnSave.setDisabled(status);
//         this.btnSaveClose.setDisabled(status);
//     },
//     saveForm: function () {
//         this.btnDisabled(true);
//         var urlz;
//         if (this.modez == 1 || this.modez == 2) {
//             urlz = 'BpBayar2/update/id/' + this.id;
//         } else {
//             urlz = 'BpBayar2/create/';
//         }
//         Ext.getCmp('form-BpBayar2').getForm().submit({
//             url: urlz,
//             timeOut: 1000,
//             scope: this,
//             success: function (f, a) {
//                 jun.rztBpBayar2.reload();
//                 var response = Ext.decode(a.response.responseText);
//                 Ext.MessageBox.show({
//                     title: 'Info',
//                     msg: response.msg,
//                     buttons: Ext.MessageBox.OK,
//                     icon: Ext.MessageBox.INFO
//                 });
//                 if (this.modez == 0) {
//                     Ext.getCmp('form-BpBayar2').getForm().reset();
//                     this.btnDisabled(false);
//                 }
//                 if (this.closeForm) {
//                     this.close();
//                 }
//             },
//             failure: function (f, a) {
//                 switch (a.failureType) {
//                     case Ext.form.Action.CLIENT_INVALID:
//                         Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                         break;
//                     case Ext.form.Action.CONNECT_FAILURE:
//                         Ext.Msg.alert('Failure', 'Ajax communication failed');
//                         break;
//                     case Ext.form.Action.SERVER_INVALID:
//                         Ext.Msg.alert('Failure', a.result.msg);
//                 }
//                 this.btnDisabled(false);
//             }
//         });
//     },
//     onbtnSaveCloseClick: function () {
//         this.closeForm = true;
//         this.saveForm(true);
//     },
//     onbtnSaveclick: function () {
//         this.closeForm = false;
//         this.saveForm(false);
//     },
//     onbtnCancelclick: function () {
//         this.close();
//     }
// });