jun.DroppingRecallStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DroppingRecallStore.superclass.constructor.call(this, Ext.apply({
            url: 'DroppingRecall',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'dropping_recall_id'},
                {name: 'tgl', type: 'date'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate', type: 'date'},
                {name: 'user_id'},
                {name: 'store'},
                {name: 'status', type: 'int'},
                {name: 'up'}
            ]
        }, cfg));
    }
});

jun.DroppingRecallDetailsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DroppingRecallDetailsStore.superclass.constructor.call(this, Ext.apply({
            url: 'DroppingRecall/Detail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'dropping_recall_details_id'},
                {name: 'dropping_recall_id'},
                {name: 'dropping_id'},
                {name: 'barang_id'},
                {name: 'seq', type: 'int'},
                {name: 'qty', type: 'int'},
                {name: 'price', type: 'float'},
                {name: 'visible', type: 'int'},

                /* cache */
                {name: 'qty_recall', type: 'float'},
                {name: 'qty_received', type: 'float'},
                {name: 'qty_recalled', type: 'float'},
                {name: 'qty_dropping', type: 'float'},

                /* view */
                {name: 'dropping_doc_ref'},
                {name: 'store_pengirim'},
                {name: 'store_penerima'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'sat'}
            ]
        }, cfg));
    },
    setSequence: function(){
        this.each(function (rec, idx) {
            rec.data.quality_inspection_detail_seq = idx;
        },this);
    }
});