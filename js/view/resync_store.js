jun.Resyncstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Resyncstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResyncStoreId',
            url: 'Resync',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id_resync'},
{name:'trans_id'},
{name:'type'},
{name:'action'},
{name:'startdate'},
{name:'enddate'},
{name:'status'},
                
            ]
        }, cfg));
    }
});
jun.rztResync = new jun.Resyncstore();
//jun.rztResync.load();
