jun.MemberCardstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.MemberCardstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MemberCardStoreId',
            url: 'MemberCard',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'membercard_id'},
                {name:'customer_id'},
                {name:'store'},
                {name:'no_card'},
                {name:'name'},
                {name:'since'},
                {name:'valid'},
                {name:'type'},
                {name:'category'},
                {name:'business_unit'},
                {name:'sub_business_unit'},
                {name:'active'},
                {name:'counter'},
                {name:'created_at'},
                {name:'update_at'},
                {name:'up'},
                
            ]
        }, cfg));
    }
});
jun.rztMemberCard = new jun.MemberCardstore();
//jun.rztMemberCard.load();
