jun.BpBayarPartsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayarPartsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayarPartsStoreId',
            url: 'BpBayarParts',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_bayar_parts_id'},
                {name: 'tgl'},
                {name: 'total'},
                {name: 'bank_id'},
                {name: 'id_user'},
                {name: 'tdate'},
                {name: 'note'},
                {name: 'ref'},
            ]
        }, cfg));
    }
});
jun.rztBpBayarParts = new jun.BpBayarPartsstore();
//jun.rztBpBayarParts.load();
jun.BpBayarPartsListstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayar2Liststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayarPartsListstoreId',
            url: 'BpBayarParts/list',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_parts_id'},
                {name: 'inv_no'},
                {name: 'inv_tgl'},
                {name: 'hpp', type: "float"},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'body_paint_id'}
            ]
        }, cfg));
    },
});
jun.rztBpBayarPartsList = new jun.BpBayarPartsListstore();