jun.OutletGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Outlet",
        id:'docs-jun.OutletGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
                        {
			header:'outlet_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'outlet_id',
			width:100
		},
                                {
			header:'code',
			sortable:true,
			resizable:true,                        
            dataIndex:'code',
			width:100
		},
                                {
			header:'name',
			sortable:true,
			resizable:true,                        
            dataIndex:'name',
			width:100
		},
                                {
			header:'address',
			sortable:true,
			resizable:true,                        
            dataIndex:'address',
			width:100
		},
                                {
			header:'city',
			sortable:true,
			resizable:true,                        
            dataIndex:'city',
			width:100
		},
                                {
			header:'postal_code',
			sortable:true,
			resizable:true,                        
            dataIndex:'postal_code',
			width:100
		},
                		/*
                {
			header:'phone',
			sortable:true,
			resizable:true,                        
            dataIndex:'phone',
			width:100
		},
                                {
			header:'email',
			sortable:true,
			resizable:true,                        
            dataIndex:'email',
			width:100
		},
                                {
			header:'open',
			sortable:true,
			resizable:true,                        
            dataIndex:'open',
			width:100
		},
                                {
			header:'close',
			sortable:true,
			resizable:true,                        
            dataIndex:'close',
			width:100
		},
                                {
			header:'status',
			sortable:true,
			resizable:true,                        
            dataIndex:'status',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztOutlet;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Ubah',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.OutletGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.OutletWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.outlet_id;
            var form = new jun.OutletWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'Outlet/delete/id/' + record.json.outlet_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztOutlet.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
