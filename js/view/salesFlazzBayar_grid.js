jun.SalesFlazzBayarGrid = Ext.extend(Ext.grid.GridPanel, {
  title: "Penjualan Bayar",
  id: "docs-jun.SalesFlazzBayarGrid",
  iconCls: "silk-grid",
  viewConfig: {
    forceFit: true,
  },
  sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),
  plugins: [new Ext.ux.grid.GridHeaderFilters()],
  columns: [
    {
      header: "Doc Ref",
      sortable: true,
      resizable: true,
      dataIndex: "doc_ref",
      width: 100,
    },
    {
      header: "Tgl",
      sortable: true,
      resizable: true,
      dataIndex: "tgl",
      width: 100,
    },
    {
      header: "Nama Bank",
      sortable: true,
      resizable: true,
      dataIndex: "nama_bank",
      width: 100,
    },
    {
      header: "Amount",
      sortable: true,
      resizable: true,
      dataIndex: "amount",
      width: 100,
    },
    {
      header: "Note",
      sortable: true,
      resizable: true,
      dataIndex: "note",
      width: 100,
    },
  ],
  initComponent: function () {
    this.store = jun.rztSalesFlazzBayar;
    this.bbar = {
      items: [
        {
          xtype: "paging",
          store: this.store,
          displayInfo: true,
          pageSize: 20,
        },
      ],
    };

    this.tbar = {
      xtype: "toolbar",
      items: [
        {
          xtype: "button",
          text: "Print",
          ref: "../btnPrint",
        },
        // {
        //   xtype: "tbseparator",
        // },
        // {
        //   xtype: "button",
        //   text: "Ubah",
        //   ref: "../btnEdit",
        // },
        // {
        //   xtype: "tbseparator",
        // },
        // {
        //   xtype: "button",
        //   text: "Hapus",
        //   ref: "../btnDelete",
        // },
      ],
    };
    this.store.baseParams = { mode: "grid" };
    this.store.reload();
    this.store.baseParams = {};
    jun.SalesFlazzBayarGrid.superclass.initComponent.call(this);
    this.btnPrint.on('Click', this.printForm, this);
    // this.btnEdit.on("Click", this.loadEditForm, this);
    // this.btnDelete.on("Click", this.deleteRec, this);
    this.getSelectionModel().on("rowselect", this.getrow, this);
  },

  
  printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        var idz = selectedz.json.sales_flazz_bayar_id;
        window.open('salesFlazzBayar/print/id/' + idz, '_blank');
    },
  getrow: function (sm, idx, r) {
    this.record = r;

    var selectedz = this.sm.getSelections();
  },

  loadForm: function () {
    var form = new jun.SalesFlazzBayarWin({ modez: 0 });
    form.show();
  },

  loadEditForm: function () {
    var selectedz = this.sm.getSelected();

    //var dodol = this.store.getAt(0);
    if (selectedz == undefined) {
      Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
      return;
    }
    var idz = selectedz.json.sales_flazz_bayar_id;
    var form = new jun.SalesFlazzBayarWin({ modez: 1, id: idz });
    form.show(this);
    form.formz.getForm().loadRecord(this.record);
  },

  deleteRec: function () {
    Ext.MessageBox.confirm(
      "Pertanyaan",
      "Apakah anda yakin ingin menghapus data ini?",
      this.deleteRecYes,
      this
    );
  },

  deleteRecYes: function (btn) {
    if (btn == "no") {
      return;
    }

    var record = this.sm.getSelected();

    // Check is list selected
    if (record == undefined) {
      Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
      return;
    }

    Ext.Ajax.request({
      url: "SalesFlazzBayar/delete/id/" + record.json.sales_flazz_bayar_id,
      method: "POST",
      success: function (f, a) {
        jun.rztSalesFlazzBayar.reload();
        var response = Ext.decode(f.responseText);
        Ext.MessageBox.show({
          title: "Info",
          msg: response.msg,
          buttons: Ext.MessageBox.OK,
          icon: Ext.MessageBox.INFO,
        });
      },
      failure: function (f, a) {
        switch (a.failureType) {
          case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert(
              "Failure",
              "Form fields may not be submitted with invalid values"
            );
            break;
          case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert("Failure", "Ajax communication failed");
            break;
          case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert("Failure", a.result.msg);
        }
      },
    });
  },
});
