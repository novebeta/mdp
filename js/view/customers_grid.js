jun.CustomersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Customers",
    id: 'docs-jun.CustomersGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        // {
        //     header: 'Customers No.',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'no_customer',
        //     width: 100,
        //     filter: {xtype: "textfield"}
        // },
        {
            header: 'Customer Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100,
            filter: {xtype: "textfield"}
        },
        // {
        //     header: 'Tgl Lahir',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_lahir',
        //     width: 100,
        //     renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        //     filter: {
        //         xtype: "xdatefield",
        //         format: 'd/m/Y'
        //     }
        // },
        {
            header: 'Phone',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Address',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100,
            filter: {xtype: "textfield"}
        }
        /*{
         header: 'Birthplace',
         sortable: true,
         resizable: true,
         dataIndex: 'tempat_lahir',
         width: 100,
         filter: {xtype: "textfield"}
         },
         {
         header: 'email',
         sortable: true,
         resizable: true,
         dataIndex: 'email',
         width: 100
         },
         {
         header:'telp',
         sortable:true,
         resizable:true,
         dataIndex:'telp',
         width:100
         },
         {
         header:'alamat',
         sortable:true,
         resizable:true,
         dataIndex:'alamat',
         width:100
         },
         {
         header:'kota',
         sortable:true,
         resizable:true,
         dataIndex:'kota',
         width:100
         },
         {
         header:'negara',
         sortable:true,
         resizable:true,
         dataIndex:'negara',
         width:100
         },
         {
         header:'awal',
         sortable:true,
         resizable:true,
         dataIndex:'awal',
         width:100
         },
         {
         header:'akhir',
         sortable:true,
         resizable:true,
         dataIndex:'akhir',
         width:100
         },
         */
    ],
    initComponent: function () {
        if (jun.rztNegaraCmp.getTotalCount() === 0) {
            jun.rztNegaraCmp.load();
        }
        if (jun.rztProvinsiCmp.getTotalCount() === 0) {
            jun.rztProvinsiCmp.load();
        }
        if (jun.rztKotaCmp.getTotalCount() === 0) {
            jun.rztKotaCmp.load();
        }
        if (jun.rztKecamatanCmp.getTotalCount() === 0) {
            jun.rztKecamatanCmp.load();
        }
        if (jun.rztStatusCustCmp.getTotalCount() === 0) {
            jun.rztStatusCustCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztInfoCmp.getTotalCount() === 0) {
            jun.rztInfoCmp.load();
        }
        this.store = jun.rztCustomers;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Customer',
                    ref: '../btnAdd',
                    // hidden: this.antrianform ? true : false
                },
                {
                    xtype: 'tbseparator',
                    // hidden: this.antrianform ? true : false
                },
                {
                    xtype: 'button',
                    text: 'Edit Customer',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Refresh Customer',
                    ref: '../btnRefresh'
                }
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Show History',
                //     ref: '../btnHistory',
                //     hidden: this.antrianform ? true : false
                // },
                // {
                //     xtype: 'tbseparator',
                //     hidden: this.antrianform ? true : false
                // },
                /*
                 * History local di customer grid
                //  */
                // {
                //     xtype: 'button',
                //     text: 'Show History Local',
                //     ref: '../btnHistoryLocal',
                //     hidden: this.antrianform ? true : false
                // },
                /*
                 * Untuk history pasien di dokter
                 */
                // {
                //     xtype: 'button',
                //     text: 'Show History Pasien',
                //     ref: '../btnHistoryForm',
                //     hidden: this.antrianform ? false : true
                // },
                // {
                //     xtype: 'tbseparator',
                //     hidden: this.antrianform ? false : true
                // },
                /*
                 * History local di dokter grid
                 */
                // {
                //     xtype: 'button',
                //     text: 'Show History Local Pasien',
                //     ref: '../btnHistoryFormLocal',
                //     hidden: this.antrianform ? false : true
                // },
                // {
                //     xtype: 'tbseparator',
                //     hidden: this.antrianform ? true : false
                // },
                // {
                //     xtype: 'button',
                //     text: 'Copy Customer',
                //     ref: '../btnCopy',
                //     hidden: this.antrianform ? true : false
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Print CARD',
                //     ref: '../btnPrinterCard'
                // },
                /* {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Linkage',
                    ref: '../btnLinkage'
                } */
            ]
        };
        jun.CustomersGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnRefresh.on('Click', function () {
            this.store.reload();
        }, this);
        // this.btnHistory.on('Click', this.showHistory, this);
        // this.btnHistoryLocal.on('Click', this.showHistoryLocal, this);
        // this.btnHistoryForm.on('Click', this.showHistoryForm, this);
        // this.btnHistoryFormLocal.on('Click', this.showHistoryFormLocal, this);
        // this.btnCopy.on('Click', this.copyCustomers, this);
        // this.btnPrinterCard.on('Click', this.onPrintCardnew, this);
//        this.btnPrinterCard.on('Click', this.onPrintCard, this);
//        this.btnHistory.on('Click', this.onbtnHistoryclick, this);
        this.on('rowdblclick', this.viewForm, this);
        this.on('bodyresize', this.bodyResize, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        /* button customer linkage */
        /* this.btnLinkage.on('Click', this.loadLinkageForm, this); */
    },
    onPrintCardnew: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var namecust = selectedz.json.nama_customer;
        var nocust = selectedz.json.no_customer;
        var sub = nocust.substring(0, nocust.length - 5);
        var nocst = sub;
        var record = this.sm.getSelected();
        record.set('customer_id', idz);
        record.set('name', namecust);
        record.set('no_card', nocst);
        record.commit();
        var form = new jun.MemberCardWin({modez: 0, customer_id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    },
    /*onPrintCardnew: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var printWindow = window.open('', '', 'height=1000px,width=1000px');
//        var printWindow = window.open('', '', 'height=384px ,width=645px');
        Ext.Ajax.request({
            url: 'Customers/PrintCard',
            method: 'POST',
            params: {
                customer_id: idz
            },
            success: function (f, a) {
//                jun.rztCustomers.reload();
//                jun.rztCustomersCmp.reload();
                var response = Ext.decode(f.responseText);
//                Ext.MessageBox.show({
//                    title: 'Info',
//                    msg: response.msg,
//                    buttons: Ext.MessageBox.OK,
//                    icon: Ext.MessageBox.INFO
//                });                
                printWindow.document.write('<html>');
                printWindow.document.write('<head>');

                printWindow.document.write('<script src="js/printcardlayout.js"></script>');
//                printWindow.document.write('<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/barcodes/JsBarcode.code128.min.js"></script>');


                if(PT_NEGARA == "AISHADERM")
                {
                    printWindow.document.write('<style>');

                     printWindow.document.write('@page { size: auto ; margin: 0;}');
                     printWindow.document.write('@media print { html,body { width: 6.7in; }}');
                     printWindow.document.write('html { background: #eee; }');
                     printWindow.document.write('body { height: 100%; width: 100%; margin: 0;}');
                     printWindow.document.write('.mycard { width: 6.7in; position: relative; background: #fff;}');
                     printWindow.document.write('.nocustomer { position: absolute; right: 180px; top: 177px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:25px;}');
                     printWindow.document.write('.nama {position: absolute; right: 180px; top: 211px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:25px;}');
                     printWindow.document.write('.since {position: absolute; right: 244px; top: 250px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:16px;}');
                     printWindow.document.write('.valid {position: absolute; right: 160px; top: 250px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:16px;}');
                     printWindow.document.write('.barcode{position: absolute;  left: 14px; top: 255px;}');

                     printWindow.document.write('</style>');
                }

                if(PT_NEGARA == "NATASHA")
                {
                    printWindow.document.write('<style>');

                    printWindow.document.write('@page { size: auto ; margin: 0;}');
                    printWindow.document.write('@media print { html,body { width: 6.7in; }}');

                    printWindow.document.write('html { background: #eee; }');
                    printWindow.document.write('body { height: 100%; width: 100%; margin: 0;}');
                    printWindow.document.write('.mycard {width: 6.7in; position: relative; background: #fff;}');


                    printWindow.document.write('.nocustomer { position: absolute; right: 165px; top: 152px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:25px;}');
                    printWindow.document.write('.nama {position: absolute; right: 165px; top: 180px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:25px;}');
                    printWindow.document.write('.since {position: absolute; right: 274px; top: 257px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:16px;}');
                    printWindow.document.write('.valid {position: absolute; right: 190px; top: 257px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:16px;}');
                    printWindow.document.write('.barcode{position: absolute;  right: 168px; top: 100px;}');

                    printWindow.document.write('</style>');
                }

                if(PT_NEGARA == "ENA")
                {
                    printWindow.document.write('<style>');

                    printWindow.document.write('@page { size: auto ; margin: 0;}');
                    printWindow.document.write('@media print { html,body { width: 6.7in; }}');

                    printWindow.document.write('html { background: #eee; }');
                    printWindow.document.write('body { height: 100%; width: 100%; margin: 0;}');
                    printWindow.document.write('.mycard {width: 6.7in; position: relative; background: #fff;}');

                    printWindow.document.write('.nama {position: absolute; left: 33px; top: 142px; width: 450px; padding: 0px; text-align:left; color:black; font-weight:bold; font-size:25px;}');
                    printWindow.document.write('.nocustomer { position: absolute; left: 33px; top: 170px; width: 400px; padding: 0px; text-align:left; color:black; font-weight:bold; font-size:25px;}');
                    //printWindow.document.write('.since {position: absolute; right: 274px; top: 257px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:16px;}');
                    //printWindow.document.write('.valid {position: absolute; right: 190px; top: 257px; width: 400px; padding: 0px; text-align:right; color:black; font-weight:bold; font-size:16px;}');
                    printWindow.document.write('.barcode{position: absolute;  left: 33px; top: 215px;}');

                    printWindow.document.write('</style>');
                }

                printWindow.document.write('</head>');
                printWindow.document.write('<body>');

                printWindow.document.write('<div class="mycard">');

                    printWindow.document.write('<div class="nocustomer">');
                        //printWindow.document.write(response.Customers.no_customer);
                        printWindow.document.write('2101100115');
                    printWindow.document.write('</div>');

                    printWindow.document.write('<div class="nama" style="">');
                        printWindow.document.write('ARMAN LAAMA');
                        //printWindow.document.write(response.Customers.nama_customer);
                    printWindow.document.write('</div>');

                    /!*printWindow.document.write('<div class="since">');
                        printWindow.document.write(response.yawal);
                    printWindow.document.write('</div>');

                    printWindow.document.write('<div class="valid">');
                        printWindow.document.write(response.mawal+"/"+response.yakhir);
                    printWindow.document.write('</div>');*!/

                //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="123JOG02" jsbarcode-textmargin="0" jsbarcode-fontoptions="bold" jsbarcode-displayValue="false" jsbarcode-height= "30" jsbarcode-width= "2"></svg>');

                //printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="'+response.Customers.no_customer+'"jsbarcode-margin="0" jsbarcode-height= "30" jsbarcode-width= "1" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                   printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="2101100115" jsbarcode-textmargin="0" jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "2" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');


                printWindow.document.write('</div>');

                printWindow.document.write('</body>');

                printWindow.document.write('<script>JsBarcode(".barcode").init();</script>');
                printWindow.document.write('</html>');

                setTimeout(function () {
                    printWindow.print();
                    printWindow.close();
                }, 500);
                return false;
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },*/
    bodyResize: function (p, w, h) {
        var bHeight = Ext.getCmp('docs-jun.CustomersGrid').body.getHeight() - 50;
        this.paging.pageSize = parseInt(bHeight / 21);
        this.store.baseParams = {
            mode: "grid",
            start: 0,
            limit: this.antrianform ? 20 : this.paging.pageSize
        };
        this.store.load();
    },
    copyCustomers: function () {
        Ext.MessageBox.prompt('Confirm', 'Masukkan No Base atau No Customers', this.prosesCopy);
    },
    prosesCopy: function (btn, txt) {
        if (btn != 'ok') {
            return;
        }
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan No Pasien atau No Base. Misal : 1JOG01',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/Soapcopy',
            method: 'POST',
            scope: this,
            params: {
                no_base: txt
            },
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    showHistory: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var tabpanel = this.findParentByType('tabpanel');
        var tab = tabpanel.getComponent('docs-jun.PasienHistoryWin');
        if (tab) {
            tabpanel.setActiveTab(tab);
        } else {
            var obj = eval('jun.PasienHistoryWin');
            var object = new obj({id: 'docs-jun.PasienHistoryWin', closable: !0});
            var p = tabpanel.add(object);
            tabpanel.setActiveTab(p);
            tab = p;
        }
        tab.no_customer.setValue(selectedz.data.no_customer);
        tab.load({
            url: 'customers/SimpleHistory/nobase/' + selectedz.data.no_customer,
            scripts: true,
            params: {
                nobase: selectedz.data.no_customer,
                customer_id: selectedz.data.customer_id,
                height: tab.getHeight() - 100
            }
        });
        // var win = window.open(NARSURL + "summary/detail/" + selectedz.data.no_customer, '_blank');
        // win.focus();
        //var form = new jun.CustomersHistoryWin();
        //form.formz.load({url:"https://www.google.com",scripts:true, params:{}});
        //form.show(this);
    },
    showHistoryLocal: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var tabpanel = this.findParentByType('tabpanel');
        var tab = tabpanel.getComponent('docs-jun.PasienHistoryWin');
        if (tab) {
            tabpanel.setActiveTab(tab);
        } else {
            var obj = eval('jun.PasienHistoryWin');
            var object = new obj({id: 'docs-jun.PasienHistoryWin', closable: !0});
            var p = tabpanel.add(object);
            tabpanel.setActiveTab(p);
            tab = p;
        }
        tab.no_customer.setValue(selectedz.data.no_customer);
        tab.load({
            url: 'customers/SimpleHistory/nobase/' + selectedz.data.no_customer,
            scripts: true,
            params: {
                nobase: selectedz.data.no_customer,
                customer_id: selectedz.data.customer_id,
                height: tab.getHeight() - 100,
                local: 1
            }
        });
        // var win = window.open(NARSURL + "summary/detail/" + selectedz.data.no_customer, '_blank');
        // win.focus();
        //var form = new jun.CustomersHistoryWin();
        //form.formz.load({url:"https://www.google.com",scripts:true, params:{}});
        //form.show(this);
    },
    onPrintCard: function () {
        var selectedz = this.sm.getSelected();
        var type;
        var cust_no;
        var cust_name = "";
        var since;
        var valid;
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var today = new Date();
        var validDt = today.add(Date.YEAR, VALID_CARD);
        var sinceDt = Date.parseDate(selectedz.json.awal, 'Y-m-d H:i:s');
        var tgl_lahir = Date.parseDate(selectedz.json.tgl_lahir, 'Y-m-d');
        var age = today.getFullYear() - tgl_lahir.getFullYear();
        cust_no = selectedz.json.store + "     " +
            selectedz.json.no_customer.replace(selectedz.json.store, "");
//        since = sinceDt.format('m Y');
//        valid = validDt.format('m Y');
        if (selectedz.json.sex == 'MALE') {
            type = 'MEN';
        } else {
            type = (age < 0) ? 'TEEN' : 'WOMEN';
        }
        if (selectedz.json.nama_customer.length > 21) {
            arr_name = selectedz.json.nama_customer.split(" ");
            for (index = 0; index < arr_name.length; index++) {
                if ((cust_name.length + arr_name[index].length) > 21) {
                    break;
                }
                cust_name += arr_name[index] + ' ';
            }
        } else {
            cust_name = selectedz.json.nama_customer;
        }
        var msg_ = //'Name : ' + cust_name +
            // '\nCust. No : ' + cust_no +
            'Card Type : ' +
            type + '<br/>Ribbon Colour : ' + (type == 'WOMEN' ? 'BLACK' : 'SILVER');
        //Ext.MessageBox.confirm('PLEASE, PREPARE PRINTER', msg_, this.confirmPrint, this);
        //Ext.Msg.alert('Please, Prepare Card', msg_);
        var strconfirm = Ext.MessageBox.confirm('Print Card', 'P L E A S E,  P R E P A R E  P R I N T E R' +
            "<br/>==================================<br/>" + msg_);
        if (strconfirm == false) {
            return;
        }
        //findPrinterCard();
        if (notReady()) {
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/SaveLogCard',
            method: 'POST',
            scope: this,
            params: {
                customer_id: selectedz.json.customer_id,
                cardtype: type,
                validcard: validDt.format('Y-m-d'),
                printcarddate: today.format('Y-m-d'),
                locprintcard: STORE
            },
            success: function (f, a) {
//                printCard(type, cust_no, cust_name, since, valid);
                var response = Ext.decode(f.responseText);
                if (notReady()) {
                    return;
                }
                if (response.success !== false) {
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_CARD, printData);
                } else {
                    Ext.Msg.alert('Info', "Tidak ada resep untuk transaksi ini.");
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    confirmPrint: function (btn) {
        if (btn == 'no') {
            return;
        }
    },
    viewForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({modez: 2, customer_id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    },
    onbtnHistoryclick: function () {
        Ext.Ajax.request({
            url: 'customers/createlog',
            method: 'POST',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                $.ajax({
                    url: response.url,
                    type: "POST",
                    crossDomain: true,
                    data: response,
                    dataType: "json",
                    success: function (result) {
                        alert(JSON.stringify(result));
                    },
                    error: function (xhr, status, error) {
                        alert(status);
                    }
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        return;
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Customer");
            return;
        }
        var idz = selectedz.json.customer_id;
//        var form = new jun.ReportHistoryCustomers({
//            customer_id: idz,
//            cust_label: "Customer Number : " + selectedz.json.no_customer +
//                "<br>Customer Name : " + selectedz.json.nama_customer +
//                "<br>Birthday : " + selectedz.json.tgl_lahir +
//                "<br>Phone : " + selectedz.json.telp +
//                "<br>Address : " + selectedz.json.alamat
//        });
//        form.show();
        Ext.getCmp("form-ReportViewHistoryCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportViewHistoryCustomers").getForm().url = "Report/ViewCustomerHistory";
        this.customer_id.setValue(idz);
        this.format.setValue('html');
        this.tglfrom.setValue(this.tgl.getValue().format('Y-m-d'));
        var form = Ext.getCmp('form-ReportViewHistoryCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    /*getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },*/
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var getTgl = r.get('tgl_lahir');
        var tgl = new Date(getTgl);
        var getM = tgl.getMonth() + 1;
        var filterSync = getM == mm;
        //this.ultah.setVisible(filterSync);
        if (getTgl != null) {
            if (filterSync) {
                alert("CUSTOMER ULANG TAHUN DI BULAN INI !!!");
            }
        }
    },
    loadForm: function () {
        var form = new jun.CustomersMdpWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersMdpWin({modez: 1, customer_id: idz});
        form.formz.getForm().loadRecord(this.record);
        // form.nama_customer_cmb.setValue(this.record.json.nama_customer);
        form.show(this);
    },
    loadLinkageForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({modez: 1, customer_id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.nama_customer_cmb.setValue(this.record.json.nama_customer);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/delete/id/' + record.json.customer_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    showHistoryForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var no_base = selectedz.data.no_customer;
        var panel = Ext.getCmp('form-dokter-gridHistoryForm');
        panel.load({
            url: 'customers/SimpleHistory/nobase/' + no_base,
            scripts: true,
            params: {
                nobase: no_base,
                customer_id: selectedz.data.customer_id,
                height: panel.getHeight() - 100
            }
        });
        panel.expand();
    },
    showHistoryFormLocal: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var no_base = selectedz.data.no_customer;
        var panel = Ext.getCmp('form-dokter-gridHistoryForm');
        panel.load({
            url: 'customers/SimpleHistory/nobase/' + no_base,
            scripts: true,
            params: {
                nobase: no_base,
                customer_id: selectedz.data.customer_id,
                height: panel.getHeight() - 100,
                local: 1
            }
        });
        panel.expand();
    }
});
