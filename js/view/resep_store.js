jun.Resepstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Resepstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResepStoreId',
            url: 'Resep',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'resep_id'},
                {name: 'nama_resep'},
                {name: 'barang_id'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztResep = new jun.Resepstore();
//jun.rztResep.load();


jun.Resepprintstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Resepprintstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResepStoreId',
            url: 'Resep/PrintResepList',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'pasien_name'},
                {name: 'pasien_code'},
                {name: 'doc_ref_receipt'},
                {name: 'barang'}
            ]
        }, cfg));
    }
});
jun.rztPrintResep = new jun.Resepprintstore();