jun.DroppingApprovalGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Approval",
    id: 'docs-jun.DroppingApprovalGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
       {
           header: 'Jenis',
           sortable: true,
           resizable: true,
           dataIndex: 'jenis',
           width: 50,
           renderer : function(value, metaData, record, rowIndex){
               switch(Number(value)){
                   case 0 : return 'ORDER DROPPING';
                   case 1 : return 'DROPPING';
               }
           },
           filter: {
               xtype: "combo",
               typeAhead: true,
               triggerAction: 'all',
               lazyRender:true,
               editable:false,
               mode: 'local',
               store: new Ext.data.ArrayStore({
                   id: 0,
                   fields: [
                       'myId',
                       'displayText'
                   ],
                   data: [['all', 'ALL'], [0, 'ORDER DROPPING'], [1, 'DROPPING']]
               }),
               value: 'all',
               valueField: 'myId',
               displayField: 'displayText'
           }
       },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Pemohon',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Pengirim',
            sortable: true,
            resizable: true,
            dataIndex: 'store_pengirim',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case PR_NEED_SHIPMENT :
                        metaData.style += "background-color: #FFAAFF;";
                        return 'NEED SHIPMENT';
                    case PR_OPEN :
                        return 'OPEN';
                    case PR_PROCESS :
                        metaData.style += "background-color: #FCFC98;";
                        return 'PROCESS';
                    case PR_CLOSED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [-1, 'DRAFT'], [0, 'OPEN'], [1, 'PROCESS'], [2, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Approved',
            sortable: true,
            resizable: true,
            dataIndex: 'approved',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                value && (metaData.css += ' silk13-tick ');
                return '';
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEEDS APPROVAL'], [1, 'APPROVED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Up',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case DR_PENDING :
                        metaData.style += "background-color: #FF1A1A;";
                        return 'PENDING';
                    case DR_SEND :
                        metaData.style += "background-color: #FFFF33;";
                        return 'SEND';
                    case DR_APPROVE :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'APPROVE';
                    case DR_PROCESS :
                        metaData.style += "background-color: #00FF00;";
                        return 'PROCESS';
                    case DR_RECEIVE :
                        metaData.style += "background-color: #FF4D88;";
                        return 'RECEIVE';
                    case DR_CLOSE :
                        metaData.style += "background-color: #ADAD85;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'PENDING'], [1, 'SEND'], [2, 'APPROVE'], [3, 'PROCESS'], [4, 'RECEIVE'], [5, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }

    ],
    initComponent: function () {
        if(jun.rztStoreCmp.getTotalCount()==0) jun.rztStoreCmp.load();
        if(jun.rztBarangLib.getTotalCount()==0) jun.rztBarangLib.load();
        if(jun.rztBarangNonJasaAll.getTotalCount()==0) jun.rztBarangNonJasaAll.load();

        this.store = jun.rztDroppingApproval;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Approve',
                    ref: '../btnApprove',
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Resend',
                    ref: '../btnResend',
                    iconCls: 'silk13-arrow_refresh'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'History',
                    ref: '../btnHistory',
                    iconCls: 'silk13-clock'
                },
            ]
        };

        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};

        jun.DroppingApprovalGrid.superclass.initComponent.call(this);
        this.btnApprove.on('Click', this.loadEditForm, this);
        this.btnResend.on('Click', this.loadResend, this);
        this.btnHistory.on('Click', this.loadHistory, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.DroppingApprovalWin({
            modez: 0,
            title: 'Create Transfer Approval'
        });
        form.show();
    },
    loadHistory: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }

        var idz = selectedz.json.id_order_dropping_id ? selectedz.json.id_order_dropping_id : selectedz.json.id_dropping_id;

        //Ext.MessageBox.alert("Warning", idz);

        var form = new jun.DroppingHistoryWin({
            modez: 0,
            historyid: {order_dropping_id :idz},
            title: 'History Dropping'
        });
        form.show();
    },
    loadResend: function () {

        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }

        var idz = selectedz.json.id_order_dropping_id ? selectedz.json.id_order_dropping_id : selectedz.json.id_dropping_id;
        var approve = selectedz.json.approved;
        var up = selectedz.json.up;

        /*if(up >= DR_APPROVE )
        {
            Ext.MessageBox.alert("Warning", "Transfer Approval ini telah di terkirim ditujuan.");
            return;
        }*/

        if(approve == 0)
        {
            Ext.MessageBox.alert("Warning", "Anda belum melakukan Approve.");
            return;
        }


        if(selectedz.json.id_order_dropping_id)
        {
            Ext.Ajax.request({
                url: 'OrderDropping/Reapprove/',
                method: 'POST',
                params: {
                    id: idz
                },
                scope: this,
                success: function (f, a) {
                    this.store.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
        else {
            Ext.Ajax.request({
                url: 'Dropping/Reapprove/',
                method: 'POST',
                params: {
                    id: idz
                },
                scope: this,
                success: function (f, a) {
                    this.store.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }

    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }

        if(this.record.get('id_dropping_id')){
            /*
             * Dropping
             */
            var idz = selectedz.json.id_dropping_id;
            jun.rztOrderDroppingCmp.baseParams = {};
            jun.rztOrderDroppingCmp.load({
                params: {
                    order_dropping_id: selectedz.json.order_dropping_id
                },
                callback: function(r){
                    var form = new jun.DroppingWin({
                        modez: 2,
                        id: this.record.get('id_dropping_id'),
                        title: "Transfer Approval",
                        approval: (this.record.get('approved')== 0?true:false)
                    });
                    form.show(this);
                    form.formz.getForm().loadRecord(this.record);
                },
                scope: this
            });

            jun.rztDroppingDetails.baseParams = { dropping_id: idz };
            jun.rztDroppingDetails.load();
            jun.rztDroppingDetails.baseParams = {};
        }
        else
        {
            /*
             * Order Dropping
             */
            var idz = selectedz.json.id_order_dropping_id;
            var approve = selectedz.json.approved;
            if(approve == 0)
            {
                var form = new jun.OrderDroppingWin({
                    modez: 1,
                    id: idz,
                    title: 'Order Transfer Approval',
                    approval: (this.record.get('approved')== 0?true:false)
                });
            }
            else
            {
                var form = new jun.OrderDroppingWin({
                    modez: 2,
                    id: idz,
                    title: 'Order Transfer Approval',
                    approval: (this.record.get('approved')== 0?true:false)
                });
            }

            form.show(this);
            form.formz.getForm().loadRecord(this.record);
            jun.rztOrderDroppingDetails.baseParams = {
                order_dropping_id: idz
            };
            jun.rztOrderDroppingDetails.load();
            jun.rztOrderDroppingDetails.baseParams = {};
        }
    }
});
