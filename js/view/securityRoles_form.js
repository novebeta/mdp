jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 1000,
    height: 640,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: 1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                enableKeyEvents: true,
                                style: {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Ultra Grand Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .23,
                                boxLabel: "Grup Kategori Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "155"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Kategori Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "156"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Merk Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "157"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Tipe Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "158"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "159"
                            },

                            {
                                columnWidth: .23,
                                boxLabel: "Produk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "107"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Harga Jual",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "160"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "108"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Reseller",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Range Diskon",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "407"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Disk Ultra Grand",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "408"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Sales Order",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "273"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pekerjaan Dalam Proses",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "274"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pekerjaan Dalam Proses (All)",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "277"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pekerjaan Selesai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "275"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pembayaran",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "276"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Work Order UG",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "278"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pekerjaan Dalam Proses",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "365"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pekerjaan Selesai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "366"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaksi Merdeka",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .23,
                                boxLabel: "Customer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "701"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "702"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "703"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Setting Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "704"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: 'Pembelian',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "705"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Penjualan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "706"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Penjualan Bayar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "708"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Flazz Top Up",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "707"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Laporan Trans Merdeka",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "730"
                            },
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Body Paint Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .23,
                                boxLabel: "Supplier Parts",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "295"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Customer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "279"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Merk Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "280"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Mobil Tipe",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "281"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "282"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Asuransi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "283"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Kategori Mobil",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "284"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Jasa",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "285"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Harga Jasa",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "286"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Estimasi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "287"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Work Order",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "288"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "UnPost Parts",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "296"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Invoice",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "289"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Edit Qty Kejadian",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "298"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Own Risk HO",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "299"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Pembayaran Asuransi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "297"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Invoice HO",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "294"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Bayar Jasa",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "290"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Bayar Parts",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "291"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Kas/Bank BP Masuk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "292"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Kas/Bank BP Keluar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "293"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Laporan Pekerjaan Luar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "367"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Laporan Analisa Faktur",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "368"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Laporan BP Trans",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "372"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Setting Disc",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "409"
                            },

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Accounting Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [

                            {
                                columnWidth: .23,
                                boxLabel: "Chart Of Account Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "131"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Chart Of Account Type",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "132"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Chart Of Account",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "211"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "General Journal Edit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "370"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "General Journal Delete",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "371"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: 'Kas/Bank Masuk',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "212"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: 'Kas/Bank Keluar',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "213"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Cash/Bank Transfer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "214"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Mutasi Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Kartu Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },

                            {
                                columnWidth: .23,
                                boxLabel: "Buku Besar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "314"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Jurnal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "315"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Laba Rugi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "318"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Buat Laba Tahun Berjalan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "215"
                            },

                            {
                                columnWidth: .23,
                                boxLabel: "Neraca",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "320"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Tutup Buku",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "369"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Rekening Koran",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "323"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Nomor Faktur",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "324"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Export CSV Faktur",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "325"
                            },
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .23,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: 'Edit Tanggal',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "246"
                            },

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .23,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "000"
                            },
                            {
                                columnWidth: .23,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/",
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                scope: this,
                success: function (a, b) {
//                    var a = BASE_URL;
                    window.location = BASE_URL;
//                    jun.rztSecurityRoles.reload();
//                    jun.sidebar.getRootNode().reload();
//                    var c = Ext.decode(b.response.responseText);
//                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SettingClientsWin = Ext.extend(Ext.Window, {
    title: "Setting Client",
    modez: 1,
    width: 350,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SettingClients",
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Setting Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'name_',
                        id: 'name_id',
                        ref: '../name_',
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Value',
                        hideLabel: false,
                        //hidden:true,
                        name: 'value_',
                        ref: '../value_',
                        maxLength: 50,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SettingClientsWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        if (this.modez == 0) {
            var c = jun.rztSettingClients.recordType,
                d = new c({
                    name_: this.name_.getValue(),
                    value_: this.value_.getValue()
                });
            jun.rztSettingClients.add(d);
        } else {
            this.r.set('name_', this.name_.getValue());
            this.r.set('value_', this.value_.getValue());
            this.r.commit();
        }
        var data = {};
        var arr = [];
        jun.rztSettingClients.each(function (rec) {
                arr.push(rec.data);
            }
        );
        data.data = arr;
        // Lockr.set('settingClient', data);
        localStorage.setItem("settingClient", JSON.stringify(data));
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});