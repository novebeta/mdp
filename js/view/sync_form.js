jun.SyncWin = Ext.extend(Ext.Window, {
    title: "Sync",
    iconCls: "asp-refresh",
    modez: 1,
    width: 425,
    height: 196,
    layout: "form",
    modal: true,
    resizable: !1,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-History",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        forceSelection: true,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        id: 'startdate',
                        ref: '../startdate',
                        fieldLabel: "Start Date",
                        hideLabel: !1,
                        name: "startdate",
                        maxLength: 100,
                        allowBlank: !1,
                        anchor: "100%",
                    },
                    {
                        xtype: 'xdatefield',
                        id: 'enddate',
                        ref: '../enddate',
                        fieldLabel: "End Date",
                        hideLabel: !1,
                        name: "enddate",
                        maxLength: 100,
                        allowBlank: !1,
                        anchor: "100%",
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Sync',
                        ref: '../action',
                        hiddenName: 'action',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['H', 'History'],
                                ['BTR', 'Bank Transfer'],
                                ['GLT', 'GL Trans'],
                                ['SLS', 'Sales'],
                                ['C', 'Customer'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Sync",
                        mode : 'local',
                        anchor: "100%",
                    },
                    /*{
                        width: '100%',
                        height: 245,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {
                                title: 'Logs',
                                width: 480,
                                height: 267,
                                html: [
                                    '<html>',
                                    '<body>',
                                    '<div id="docs-jun.MemberCardWinPreview" class="MemberCardForm_PhotoPreview" >',
                                    SHOWLOG,
                                    '</body>',
                                    '</html>'
                                ],
                                ref: '../../cardPreview',
                            },
                        ]
                    },*/
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                /*{
                    xtype: "button",
                    text: "Scan",
                    hidden: !1,
                    ref: "../btnCheck"
                },*/
                {
                    xtype: "button",
                    text: "Sync",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Cancel",
                    ref: "../btnCancel"
                }
            ]
        };

        jun.HistoryWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },

    saveForm: function () {

        var start = this.startdate.getValue();
        var end = this.enddate.getValue();
        var sync = this.action.getValue();

        if(start == "" || end == "")
        {
            Ext.MessageBox.alert("Warning", "Start dan End Date tidak boleh kosong.");
            return;
        }
        if(sync == "")
        {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Sync.");
            return;
        }

        //this.btnDisabled(true);
        var urlz = 'sync/Sync/';
        this.formz.getForm().submit({
            url: urlz,
            timeOut: 5000,
            scope: this,
            params: {
                mode: this.modez,
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                }
                if (this.closeForm) {
                    this.close();
                }

                this.close();

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });

        this.reload();

        //this.addHistory();

    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.saveForm();

    },
    onbtnCancelclick: function () {
        this.close();
    },

});/**
 * Created by wisnu on 27/01/2018.
 */


jun.ScanWin = Ext.extend(Ext.Window, {
    title: "Scan",
    iconCls: "asp-refresh",
    modez: 1,
    width: 425,
    height: 167,
    layout: "form",
    modal: true,
    resizable: !1,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-History",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        forceSelection: true,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        id: 'startdate',
                        ref: '../startdate',
                        fieldLabel: "Start Date",
                        hideLabel: !1,
                        name: "startdate",
                        maxLength: 100,
                        allowBlank: !1,
                        anchor: "100%",
                    },
                    {
                        xtype: 'xdatefield',
                        id: 'enddate',
                        ref: '../enddate',
                        fieldLabel: "End Date",
                        hideLabel: !1,
                        name: "enddate",
                        maxLength: 100,
                        allowBlank: !1,
                        anchor: "100%",
                    },
                    /*{
                        xtype: 'combo',
                        fieldLabel: 'Sync',
                        ref: '../action',
                        hiddenName: 'action',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['H', 'History'],
                                ['BTR', 'Bank Transfer'],
                                ['GLT', 'GL Trans'],
                                ['SLS', 'Sales'],
                                ['C', 'Customer'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Sync",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        width: '100%',
                        height: 245,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {
                                title: 'Logs',
                                width: 480,
                                height: 267,
                                html: [
                                    '<html>',
                                    '<body>',
                                    '<div id="docs-jun.MemberCardWinPreview" class="MemberCardForm_PhotoPreview" >',
                                    SHOWLOG,
                                    '</body>',
                                    '</html>'
                                ],
                                ref: '../../cardPreview',
                            },
                        ]
                    },*/
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Scan",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Cancel",
                    ref: "../btnCancel"
                }
            ]
        };

        jun.HistoryWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },

    saveForm: function () {

        var start = this.startdate.getValue();
        var end = this.enddate.getValue();
        //var sync = this.action.getValue();

        if(start == "" || end == "")
        {
            Ext.MessageBox.alert("Warning", "Start dan End Date tidak boleh kosong.");
            return;
        }

        //this.btnDisabled(true);
        var urlz = 'sync/Scan/';
        this.formz.getForm().submit({
            url: urlz,
            timeOut: 5000,
            scope: this,
            params: {
                mode: this.modez,
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                }
                if (this.closeForm) {
                    this.close();
                }
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
        this.reload();

    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.saveForm();
        this.close();
    },
    onbtnCancelclick: function () {
        this.close();
    }

});/**
 * Created by wisnu on 27/01/2018.
 */






