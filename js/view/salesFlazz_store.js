jun.SalesFlazzstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesFlazzstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesFlazzStoreId',
            url: 'SalesFlazz',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_flazz_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'up'},
                {name: 'total_pot'},
                {name: 'total_discrp1'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'lunas'},
                {name: 'bank_id'},
                {name: 'customer_id'},
                {name: 'barang_id'},
                {name: 'qty'},
                {name: 'harga'},
                {name: 'vatrp'},
                {name: 'lunas_bank_id'},
                {name: 'harga_beli'},
                {name: 'kurang_bayar'},
                {name: 'total_bayar'},
                {name: "no_faktur" },
                {name: 'npwp'}
            ]
        }, cfg));
    }
});
jun.rztSalesFlazz = new jun.SalesFlazzstore();
jun.rztSalesFlazzTopUp = new jun.SalesFlazzstore({url:'SalesFlazz/indextopup'});
//jun.rztSalesFlazz.load();
