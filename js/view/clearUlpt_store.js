jun.ClearUlptstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ClearUlptstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ClearUlptStoreId',
            url: 'ClearUlpt',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'clear_ulpt_id'},
                {name: 'tgl_dari'},
                {name: 'tgl_to'},
                {name: 'note_'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'store'},
                {name: 'up'},
                {name: 'user_id'},
                {name: 'total'},
            ]
        }, cfg));
    }
});
jun.rztClearUlpt = new jun.ClearUlptstore();
//jun.rztClearUlpt.load();
