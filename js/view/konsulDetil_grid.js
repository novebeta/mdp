jun.KonsulDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KonsulDetil",
    id: 'docs-jun.KonsulDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'konsul_detil_id',
            sortable: true,
            resizable: true,
            dataIndex: 'konsul_detil_id',
            width: 100
        },
        {
            header: 'barang_id',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100
        },
        {
            header: 'qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100
        },
        {
            header: 'disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100
        },
        {
            header: 'discrp',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 100
        },
        {
            header: 'ketpot',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        },
        /*
         {
         header:'vat',
         sortable:true,
         resizable:true,
         dataIndex:'vat',
         width:100
         },
         {
         header:'vatrp',
         sortable:true,
         resizable:true,
         dataIndex:'vatrp',
         width:100
         },
         {
         header:'bruto',
         sortable:true,
         resizable:true,
         dataIndex:'bruto',
         width:100
         },
         {
         header:'total',
         sortable:true,
         resizable:true,
         dataIndex:'total',
         width:100
         },
         {
         header:'total_pot',
         sortable:true,
         resizable:true,
         dataIndex:'total_pot',
         width:100
         },
         {
         header:'price',
         sortable:true,
         resizable:true,
         dataIndex:'price',
         width:100
         },
         {
         header:'disc_name',
         sortable:true,
         resizable:true,
         dataIndex:'disc_name',
         width:100
         },
         {
         header:'hpp',
         sortable:true,
         resizable:true,
         dataIndex:'hpp',
         width:100
         },
         {
         header:'cost',
         sortable:true,
         resizable:true,
         dataIndex:'cost',
         width:100
         },
         {
         header:'disc1',
         sortable:true,
         resizable:true,
         dataIndex:'disc1',
         width:100
         },
         {
         header:'discrp1',
         sortable:true,
         resizable:true,
         dataIndex:'discrp1',
         width:100
         },
         {
         header:'konsul_id',
         sortable:true,
         resizable:true,
         dataIndex:'konsul_id',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztKonsulDetil;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.KonsulDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KonsulDetilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.konsul_detil_id;
        var form = new jun.KonsulDetilWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'KonsulDetil/delete/id/' + record.json.konsul_detil_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKonsulDetil.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KonsulDetilCounterGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KonsulDetil",
    id: 'docs-jun.KonsulDetilCounterGrid',
    iconCls: "silk-grid",
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztKonsulDetilCounter;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Item',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add All Items',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.KonsulDetilCounterGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.addItemClick, this);
        this.btnEdit.on('Click', this.addAllItemsClick, this);
        this.on('disable', this.onDisable_, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onDisable_: function () {
        this.store.removeAll();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    addAllItems: function () {
        this.store.each(function (r) {
            var barang = jun.getBarang(r.data.barang_id);

            var b = jun.rztPaketPerawatan, c = b.findExact("barang_id", r.data.barang_id);
            if (c != -1)
            {
                Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').kmr_item.setValue(
                    Ext.getCmp('docs-SalestransCounterWin').dokter.getValue()
                );

                var customer_id = Ext.getCmp('docs-SalestransCounterWin').customer.getValue();
                Ext.Ajax.request({
                    url: 'SalestransDetails/GetDiskon',
                    method: 'POST',
                    scope: this,
                    params: {
                        customer_id: customer_id,
                        barang_id: r.data.barang_id
                    },
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);

                        if (response.msg.kmr != '')
                            Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').kmr_item.setValue(response.msg.kmr);

                        if (response.msg.awal == 1)
                        {
                            Ext.MessageBox.confirm('Questions', 'Apakah pasien akan membayar dengan harga penuh?', function(btn)
                            {
                                if (btn == 'no') {
                                    return;
                                }
                                response.msg.value = 0;
                            }, this);
                        }
                        else if (response.msg.awal == 2 && response.msg.value == 100)
                            Ext.Msg.alert('Warning', 'Pasien sudah membayar penuh pada transaksi sebelumnya');

                        //console.log(r.data);
                        var ket_pot = (r.data.ketpot === undefined || r.data.ketpot == "-") ? "" : r.data.ketpot;
                        var item_com = (response.msg.item_com === undefined) ? "" :" " + response.msg.item_com;
                        Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').addItem(
                            r.data.barang_id, barang.data.kode_barang, response.msg.price,
                            r.data.qty, response.msg.value, 0, response.msg.value, ket_pot+item_com, response.msg.kode,
                            barang.data.price_override
                        );

                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });

            }
            else
                Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').addItem(
                    r.data.barang_id, barang.data.kode_barang, barang.data.price,
                    r.data.qty, parseFloat(r.data.disc), parseFloat(r.data.discrp), parseFloat(r.data.disc), r.data.ketpot, '', barang.data.price_override
                );
        }, this);
    },
    addItemClick: function (t, e) {
        t.setDisabled(true);
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            t.setDisabled(false);
            return;
        }
        var barang = jun.getBarang(record.data.barang_id);
        Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').addItem(
            record.data.barang_id, barang.data.kode_barang, barang.data.price,
            record.data.qty, 0, 0, 0, record.data.ketpot, ''
        );
        t.setDisabled(false);
    },
    addAllItemsClick: function (t, e) {
        t.setDisabled(true);
        this.addAllItems();
        t.setDisabled(false);
    }
});
jun.KonsulDetilTambahanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KonsulDetil",
    storeDetils: null,
    id: 'docs-jun.KonsulDetilTambahanGrid',
    iconCls: "silk-grid",
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztKonsulDetilTambahan;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            ////typeAhead: true,
                            width: 100,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            //forceSelection: true,
                            store: this.storeDetils,
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../ketpot',
                            width: 190,
                            colspan: 4,
                            maxLength: 255
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            width: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            width: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.KonsulDetilTambahanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
        var a = jun.rztKonsulDetilTambahan.findExact("barang_id", barang_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Item already inputted");
            return;
        }
        var barang = jun.getBarang(barang_id);
        var qty = parseFloat(this.qty.getValue());
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
        var ketpot = this.ketpot.getValue();
        var c = jun.rztKonsulDetilTambahan.recordType,
            d = new c({
                barang_id: barang_id,
                qty: qty,
                ketpot: ketpot
            });
        jun.rztKonsulDetilTambahan.add(d);
        this.barang.reset();
        this.qty.reset();
        this.ketpot.reset();
        this.barang.focus();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
    }
});
