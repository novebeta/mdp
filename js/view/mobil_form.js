jun.MobilWin = Ext.extend(Ext.Window, {
    title: 'Mobil',
    modez: 1,
    width: 400,
    height: 330,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Mobil',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        ref: '../customer',
                        fieldLabel: 'Customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersSalesCmp,
                        id: "customermobil_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: 'div.search-item',
                        tpl: new Ext.XTemplate(
                            '<tpl for="."><div class="search-item">',
                            '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                            '{alamat}',
                            '</div></tpl>'
                        ),
                        allowBlank: false,
                        listWidth: 450,
                        lastQuery: "",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Tipe',
                        store: jun.rztMobilTipeCmp,
                        hiddenName: 'mobil_tipe_id',
                        valueField: 'mobil_tipe_id',
                        displayField: 'nama',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Tahun Pembuatan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tahun_pembuatan',
                        id: 'tahun_pembuatanid',
                        ref: '../tahun_pembuatan',
                        // decimalSeparator: '',
                        maxLength: 11,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../mulai_stnk',
                        fieldLabel: 'Mulai STNK',
                        name: 'mulai_stnk',
                        id: 'mulai_stnkid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Polisi',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_pol',
                        id: 'no_polid',
                        ref: '../no_pol',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Rangka',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_rangka',
                        id: 'no_rangkaid',
                        ref: '../no_rangka',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Mesin',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_mesin',
                        id: 'no_mesinid',
                        ref: '../no_mesin',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'KM',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tahun_km',
                        id: 'tahun_kmid',
                        ref: '../tahun_km',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Type Warna',
                        hideLabel: false,
                        //hidden:true,
                        name: 'warna',
                        id: 'warnaid',
                        ref: '../warna',
                        maxLength: 10,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     fieldLabel: 'customer_id',
                    //     store: jun.rztCustomers,
                    //     hiddenName: 'customer_id',
                    //     valueField: 'customer_id',
                    //     displayField: 'nama_customer',
                    //     anchor: '100%'
                    // },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.MobilWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Mobil/update/id/' + this.id;
        } else {
            urlz = 'Mobil/create/';
        }
        Ext.getCmp('form-Mobil').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztMobil.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Mobil').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});