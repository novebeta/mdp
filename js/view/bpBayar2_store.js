jun.BpBayar2store = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayar2store.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayar2StoreId',
            url: 'BpBayar2',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_bayar_id'},
                {name: 'tgl'},
                {name: 'total', type: "float"},
                {name: 'bank_id'},
                {name: 'id_user'},
                {name: 'ref'},
                {name: 'asuransi_bp_id'},
                {name: 'tagih'},
            ]
        }, cfg));
    }
});
jun.rztBpBayar2 = new jun.BpBayar2store();
//jun.rztBpBayar2.load();
