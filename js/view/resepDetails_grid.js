jun.ResepDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ResepDetails",
    id: 'docs-jun.ResepDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Bahan',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_bahan',
            width: 100
        },
        {
            header: 'Nama Bahan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_bahan',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100
        },
        {
            header: 'Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztResepDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Kode :'
                },
                {
                    xtype: 'textfield',
                    ref: '../kode_bahan',
                    maxLength: 20,
                    width: 50
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Nama :'
                },
                {
                    xtype: 'textfield',
                    ref: '../nama_bahan',
                    maxLength: 100,
                    width: 200
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Qty :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../qty',
                    width: 50,
                    value: 0,
                    minValue: 0
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Unit :'
                },
                {
                    xtype: 'textfield',
                    ref: '../sat',
                    width: 50,
                    maxLength: 20
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    width: 50,
                    ref: '../btnAdd'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    width: 50,
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    width: 50,
                    ref: '../btnDelete'
                }
            ]
        };
        jun.ResepDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadForm: function () {
        var kode_bahan = this.kode_bahan.getValue();
        var nama_bahan = this.nama_bahan.getValue();
        var sat = this.sat.getValue();
        var qty = parseFloat(this.qty.getValue());
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('kode_bahan', kode_bahan);
            record.set('nama_bahan', nama_bahan);
            record.set('sat', sat);
            record.set('qty', qty);
            record.commit();
        } else {
            var c = jun.rztResepDetails.recordType,
                d = new c({
                    kode_bahan: kode_bahan,
                    nama_bahan: nama_bahan,
                    sat: sat,
                    qty: qty
                });
            jun.rztResepDetails.add(d);
        }
        this.kode_bahan.reset();
        this.nama_bahan.reset();
        this.sat.reset();
        this.qty.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.kode_bahan.setValue(record.data.kode_bahan);
            this.nama_bahan.setValue(record.data.nama_bahan);
            this.qty.setValue(record.data.qty);
            this.sat.setValue(record.data.sat);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
})
