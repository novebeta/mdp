jun.NoFakturstore = Ext.extend(Ext.data.JsonStore, {
	constructor: function (cfg) {
		cfg = cfg || {};
		jun.NoFakturstore.superclass.constructor.call(
			this,
			Ext.apply(
				{
					storeId: "NoFakturStoreId",
					url: "NoFaktur",
					root: "results",
					totalProperty: "total",
					fields: [
						{ name: "no_faktur_id" },
						{ name: "no_faktur" },
						{ name: "mulai_berlaku" },
						{ name: "tipe_trans" },
						{ name: "id_trans" },
						{ name: "created_at" },
						{ name: "modified_at" },
						{ name: "created_user" },
						{ name: "modified_user" },
					],
				},
				cfg
			)
		);
	},
});
jun.rztNoFaktur = new jun.NoFakturstore();
//jun.rztNoFaktur.load();
