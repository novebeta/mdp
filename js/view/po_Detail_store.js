jun.PoDetailsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PoDetailsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseOrderDetailsStoreId',
            url: 'PoInDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_detail_id'},
                {name: 'po_id'},
                {name: 'seq', type: 'float'},
                {name: 'description'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'price', type: 'float'},
                {name: 'sub_total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'disc_rp', type: 'float'},
                {name: 'total_disc', type: 'float'},
                {name: 'total_dpp', type: 'float'},
                {name: 'ppn', type: 'float'},
                {name: 'ppn_rp', type: 'float'},
                {name: 'pph_id', type: 'int'},
                {name: 'pph', type: 'float'},
                {name: 'pph_rp', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'charge'},
                {name: 'item_id'},
                {name: 'qty_to_purchase'} //tidak disimpan di database, data sementara untuk mengetahui jumlah yg harus dipesan
            ]
        }, cfg));
    },
    refreshData: function () {
        var sub_total = this.sum("sub_total");
        var tax_rp = this.sum("ppn_rp");
        var total = this.sum("total");
        var disc_rp = this.sum("disc_rp");
        var total_pph_rp = this.sum("pph_rp");
        var total_dpp = this.sum("total_dpp");
        Ext.getCmp('total_dppid').setValue(total_dpp);
        Ext.getCmp('total_disc_rpid').setValue(disc_rp);
        Ext.getCmp('sub_totalid').setValue(sub_total);
        Ext.getCmp('totalprid').setValue(total);
        Ext.getCmp('total_pph_rpid').setValue(total_pph_rp);
        Ext.getCmp('tax_rpid').setValue(tax_rp);
        this.refreshSortData();
    },
    refreshSortData: function () {
        this.each(function (item, index, totalItems) {
            item.data.seq = index;
        },this);
    }
});
jun.rztPoDetails = new jun.PoDetailsStore();
jun.rztCreatePODetails = new jun.PoDetailsStore({url: 'PrDetail/GetItemsForPO'});

jun.HistoryHargaDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.HistoryHargaDetailsstore.superclass.constructor.call(this, Ext.apply({
            url: 'PoIn/HistoryHarga',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'price', type: 'float'},
                {name: 'supplier_name'},
                {name: 'tgl', type: 'date'}
            ]
        }, cfg));
    }
});
jun.rztHistoryHargaDetails = new jun.HistoryHargaDetailsstore();