jun.SalesDetailMdpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SalesDetailMdp",
    id: 'docs-jun.SalesDetailMdpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    hideToolbars: false,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztSalesDetailMdp;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Produk :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            colspan: 3,
                            forceSelection: true,
                            store: jun.rztBarangView,
                            // hiddenName: 'produk_id',
                            valueField: 'barang_id',
                            displayField: 'nama_barang',
                            ref: '../../produk_id',
                            allowBlank: false,
                            width: 175
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../harga',
                            id: 'priceid',
                            enableKeyEvents: true,
                            width: 75,
                            readOnly: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../disc',
                            id: 'discdetilid',
                            width: 50,
                            enableKeyEvents: true,
                            // readOnly: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Rp:'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../discrp',
                            id: 'discadetilid',
                            width: 75,
                            enableKeyEvents: true,
                            // readOnly: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPN:'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../vat',
                            // id: 'discadetilid',
                            width: 50,
                            // enableKeyEvents: true,
                            // readOnly: true,
                            value: 0,
                            minValue: 0
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large',
                        width: 40
                        //height: 44
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalesDetailMdpGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.produk_id.on('select', this.onChangeBarang, this);
        this.harga.on('keyup', this.onDiscChange, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscChange, this);
        // this.produk_id.on('expand', this.onExpandBarang, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.toolbars[0].hidden = this.hideToolbars;
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.harga.getValue());
        var qty = 1;
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.discrp.getValue());
        var bruto = Math.round(price * qty);
        if (disc > 100 || disca > bruto) {
            Ext.MessageBox.alert("Error", "Diskon tidak boleh melebihi harga jual");
            this.disc.setValue("0");
            this.discrp.setValue("0");
            this.onDiscChange();
            this.produk_id.focus();
            return;
        }
        if (a.id == "discdetilid" || a.id == "priceid") {
            this.discrp.setValue(Math.round(bruto * (disc / 100)));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                disc = round((disca / price) * 100, 2);
                this.disc.setValue(disc);
            }
        }
    },
    onChangeBarang: function (c, r, i) {
        this.harga.setValue(r.data.harga);
        this.disc.setValue(r.data.disc);
        this.discrp.setValue(r.data.discrp);
        this.vat.setValue(r.data.vat);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.produk_id.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Produk harus dipilih.");
            return
        }
        var harga = parseFloat(this.harga.getValue());
        var qty = 1;
        var tot_qty_harga = harga * qty;
        var disc = parseFloat(this.disc.getValue());
        var discrp = parseFloat(this.discrp.getValue());
        var vat = parseFloat(this.vat.getValue());
        var tot_after_disc = tot_qty_harga - discrp;
        var vatrp = Math.round(tot_after_disc * (vat / 100));
        var total = tot_after_disc + vatrp;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('harga', harga);
            record.set('discrp', discrp);
            record.set('disc', disc);
            record.set('tot_qty_harga', tot_qty_harga);
            record.set('vat', vat);
            record.set('tot_after_disc', tot_after_disc);
            record.set('vatrp', vatrp);
            record.set('total', total);
            record.commit();
        } else {
            var c = jun.rztSalesDetailMdp.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    harga: harga,
                    discrp: discrp,
                    disc: disc,
                    tot_qty_harga: tot_qty_harga,
                    tot_after_disc: tot_after_disc,
                    vat: vat,
                    vatrp: vatrp,
                    total: total
                });
            jun.rztSalesDetailMdp.add(d);
        }
        this.produk_id.reset();
        this.harga.reset();
        this.disc.reset();
        this.discrp.reset();
        this.vat.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
//        this.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.produk_id.setValue(record.data.barang_id);
            // this.onChangeBarang();
            // this.qty.setValue(record.data.qty);
            this.harga.setValue(record.data.harga);
            this.disc.setValue(record.data.disc);
            this.discrp.setValue(record.data.discrp);
            this.vat.setValue(record.data.vat);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
})
