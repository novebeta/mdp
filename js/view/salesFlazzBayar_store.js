jun.SalesFlazzBayarstore = Ext.extend(Ext.data.JsonStore, {
	constructor: function (cfg) {
		cfg = cfg || {};
		jun.SalesFlazzBayarstore.superclass.constructor.call(
			this,
			Ext.apply(
				{
					storeId: "SalesFlazzBayarStoreId",
					url: "SalesFlazzBayar",
					root: "results",
					totalProperty: "total",
					fields: [
						{ name: "sales_flazz_bayar_id" },
						{ name: "sales_flazz_id" },
						{ name: "doc_ref" },
						{ name: "tgl" },
						{ name: "bank_id" },
						{ name: "nama_bank" },
						{ name: "amount" },
						{ name: "note" },
						{ name: "tdate" },
						{ name: "user_id" },
					],
				},
				cfg
			)
		);
	},
});
jun.rztSalesFlazzBayar = new jun.SalesFlazzBayarstore();
//jun.rztSalesFlazzBayar.load();
