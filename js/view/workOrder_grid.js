jun.WorkOrderGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pekerjaan Dalam Proses",
    id: 'docs-jun.WorkOrderGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_so',
            renderer: Ext.util.Format.dateRenderer('d-M-Y')
        },
        {
            header: 'No. SO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
        },
        {
            header: 'No. Polisi',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
        },
        {
            header: 'Merk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_merk',
        },
        {
            header: 'Tipe Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_tipe',
        },
        {
            header: 'Kategori',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kategori',
        },
        {
            header: 'Usia Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'age',
        },
        {
            header: 'Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
        },
        {
            header: 'Harga Awal',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        },
        {
            header: 'Harga Akhir',
            sortable: true,
            resizable: true,
            dataIndex: 'tot_after_disc',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        }
    ],
    initComponent: function () {
        this.store = jun.rztWorkOrder;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Lihat Pekerjaan',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Finish Pekerjaan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Refresh Pekerjaan',
                    ref: '../btnRefresh'
                }
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.WorkOrderGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnRefresh.on('Click', function () {
            this.store.reload();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztMobilViewSalesCmp.load({
            params: {
                mobil_id: this.record.data.mobil_id
            }
        });
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WorkOrderWin({modez: 0, title: "Pekerjaan Dalam Proses - " + this.record.data.doc_ref});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Pekerjaan Dalam Proses.");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WorkOrderWin({
            modez: 1,
            id: idz,
            title: "Pekerjaan Dalam Proses - " + this.record.data.doc_ref
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'WorkOrder/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztWorkOrder.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.WorkOrderAllGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pekerjaan Dalam Proses (All)",
    id: 'docs-jun.WorkOrderAllGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Reseller',
            sortable: true,
            resizable: true,
            dataIndex: 'store_kode',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_so',
            renderer: Ext.util.Format.dateRenderer('d-M-Y')
        },
        {
            header: 'No. SO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
        },
        {
            header: 'No. Polisi',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
        },
        {
            header: 'Merk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_merk',
        },
        {
            header: 'Tipe Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_tipe',
        },
        {
            header: 'Kategori',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kategori',
        },
        {
            header: 'Usia Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'age',
        },
        {
            header: 'Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
        },
        {
            header: 'Harga Awal',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        },
        {
            header: 'Harga Akhir',
            sortable: true,
            resizable: true,
            dataIndex: 'tot_after_disc',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        }
    ],
    initComponent: function () {
        this.store = jun.rztWorkOrderAll;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Lihat Pekerjaan',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Revisi Pekerjaan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Refresh Pekerjaan',
                    ref: '../btnRefresh'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        this.store.reload();
        // this.store.baseParams = {};
        jun.WorkOrderAllGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnRefresh.on('Click', function () {
            this.store.reload();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztMobilViewSalesCmp.load({
            params: {
                mobil_id: this.record.data.mobil_id
            }
        });
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Pekerjaan Dalam Proses");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WorkOrderWin({modez: 0, title: "Pekerjaan Dalam Proses - " + this.record.data.doc_ref});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Pekerjaan Dalam Proses");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WorkOrderWin({
            modez: 2,
            id: idz,
            title: "Pekerjaan Dalam Proses - " + this.record.data.doc_ref
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    }
});
