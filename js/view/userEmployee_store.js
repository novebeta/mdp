jun.UserEmployeestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.UserEmployeestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'UserEmployeeStoreId',
            url: 'UserEmployee',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'user_employee_id'},
                {name: 'id'},
                {name: 'name'},
                {name: 'nama_employee'},
                {name: 'kode_employee'},
                {name: 'kode'}
            ]
        }, cfg));
    }
});
jun.rztUserEmployee = new jun.UserEmployeestore();
//jun.rztUserEmployee.load();
