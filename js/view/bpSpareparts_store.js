jun.BpSparepartsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpSparepartsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpSparepartsStoreId',
            url: 'BpSpareparts',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kode'},
                {name: 'nama'},
                {name: 'sparepart_id'},
            ]
        }, cfg));
    }
});
jun.rztBpSpareparts = new jun.BpSparepartsstore();
jun.rztBpSparepartsKode = new jun.BpSparepartsstore();
jun.rztBpSparepartsNama = new jun.BpSparepartsstore();
jun.rztBpSparepartsKode.load();
jun.rztBpSparepartsNama.load();
