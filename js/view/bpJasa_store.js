jun.BpJasastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpJasastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpJasaStoreId',
            url: 'BpJasa',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_jasa_id'},
                {name: 'tipe'},
                {name: 'body_paint_id'},
                {name: 'harga', type: "float"},
                {name: 'jasa_bp_id'},
                {name: 'disc', type: "float"},
                {name: 'discrp', type: "float"},
                {name: 'total_jasa_line', type: "float"},
                {name: 'dpp', type: "float"},
                {name: 'note'},
                {name: 'hpp1'},
                {name: 'hpp2'},
                {name: 'bp_bayar_jasa_id'},
                {name: 'ppn_ind'},
                {name: 'ppn', type: "float"},
                {name: 'ppn_out'},
            ]
        }, cfg));
        this.on('load', this.refreshData, this);
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        Ext.getCmp("tot_jasa_bp_id").setValue(this.sum("harga"));
        Ext.getCmp("tot_disc_bp_id").setValue(this.sum("discrp"));
        var dpp = this.sum("dpp");
        // var total_jasa_line = this.sum("total_jasa_line");
        var jasa_ppn = this.sum("ppn");
        var tot_parts = parseFloat(Ext.getCmp("total_partsid").getValue());
        var parts_ppn = (PENGALI_PPN * tot_parts);
        var total = dpp + tot_parts;
        Ext.getCmp("tot_jasa_id").setValue(dpp);
        Ext.getCmp("total_jasaid").setValue(dpp);
        Ext.getCmp("totalid").setValue(total);
        var vatrp = jasa_ppn + parts_ppn;
        var grand = parseFloat(total) + parseFloat(vatrp);
        Ext.getCmp("vatrpid").setValue(vatrp);
        Ext.getCmp("grand_totalid").setValue(grand);
    }
});
jun.rztBpJasa = new jun.BpJasastore();
//jun.rztBpJasa.load();
