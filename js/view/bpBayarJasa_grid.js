jun.BpBayarJasaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembayaran Jasa",
    id: 'docs-jun.BpBayarJasaGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 100
        },
        {
            header: 'Suplier',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 100
        },
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 100,
            renderer: jun.renderBank
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 300
        },
    ],
    initComponent: function () {
        this.store = jun.rztBpBayarJasa;
        jun.rztBpBayarJasa.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.tglfrom = Ext.getCmp('tglgridbyrjasafrom').getValue();
                    b.params.tglto = Ext.getCmp('tglgridbyrjasato').getValue();
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridbyrjasafrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridbyrjasato'
                        }]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Pembayaran',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Pembayaran',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Pembayaran',
                    ref: '../btnPrint'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        this.store.baseParams = {};
        jun.BpBayarJasaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printKas, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    printKas: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Pembayaran");
            return;
        }
        var idz = selectedz.json.bp_bayar_jasa_id;
        window.open('bpBayarJasa/print/id/' + idz, '_blank');
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BpBayarJasaWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.bp_bayar_jasa_id;
        var form = new jun.BpBayarJasaWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBpBayarJasaList.load({params: {bp_bayar_jasa_id: idz}});
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BpBayarJasa/delete/id/' + record.json.bp_bayar_jasa_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBpBayarJasa.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
var sm = new Ext.grid.CheckboxSelectionModel({'checkOnly': true});
jun.BpBayarJasaListGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice",
    id: 'docs-jun.BpBayarJasaListGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: sm,
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        sm,
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_tgl',
            // width: 100
        },
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'inv_no',
            // width: 100
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            // width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            // width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'hpp',
            // width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztBpBayarJasaList;
        jun.BpBayarJasaListGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.getSelectionModel().on('rowdeselect', this.getrow, this);
        // jun.rztBpBayarJasaList.load();
        this.store.removeAll();
    },
    getrow: async function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        // console.log(selectedz);
        var total = 0;
        await selectedz.forEach(function (element) {
            total += element.data.hpp;
        });
        Ext.getCmp('detilBpBayarJasaid').setValue(Ext.encode(Ext.pluck(
            selectedz, "data")));
        Ext.getCmp('totalBpBayarJasaid').setValue(total);
        Ext.getCmp('totalBpBayarUserJasaid').setValue(total);
    },
});

