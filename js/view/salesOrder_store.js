jun.SalesOrderstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesOrderstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesOrderStoreId',
            url: 'SalesOrder',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'tgl_so'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'sub_total'},
                {name: 'totalpot'},
                {name: 'total_vat'},
                {name: 'total'},
                {name: 'mobil_id'},
                {name: 'store_kode'},
                {name: 'tgl_finish'},
                {name: 'tgl_wo'},
                {name: 'upload'},
                {name: 'edited'},
                {name: 'invoice_id'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'nama_tipe'},
                {name: 'nama_merk'},
                {name: 'nama_customer'}
            ]
        }, cfg));
    }
});
jun.rztSalesOrder = new jun.SalesOrderstore();
jun.rztSalesMdp = new jun.SalesOrderstore({url: 'SalesMdp'});
//jun.rztSalesOrder.load();
