jun.BodyPaintWin = Ext.extend(Ext.Window, {
    title: 'Body Paint',
    modez: 1,
    width: 950,
    height: 580,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BodyPaint',
                layout: 'table',
                layoutConfig: {columns: 4},
                defaults: {height: 75, width: 250},
                // labelWidth: 100,
                // labelAlign: 'left',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        deferredRender: false,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 75,
                        width: 225,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No. Estimasi',
                                hideLabel: false,
                                //hidden:true,
                                name: 'est_no',
                                id: 'est_noid',
                                ref: '../est_no',
                                maxLength: 50,
                                //allowBlank: ,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../est_tgl',
                                fieldLabel: 'Tgl Estimasi',
                                name: 'est_tgl',
                                id: 'est_tglid',
                                format: 'd/m/Y',
                                value: DATE_NOW,
                                readOnly: true,
                                //allowBlank: ,
                                anchor: '100%'
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 50,
                        width: 200,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No. WO',
                                hideLabel: false,
                                //hidden:true,
                                name: 'wo_no',
                                id: 'wo_noid',
                                ref: '../wo_no',
                                maxLength: 50,
                                readOnly: true,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../wo_tgl',
                                fieldLabel: 'Tgl. WO',
                                name: 'wo_tgl',
                                id: 'wo_tglid',
                                format: 'd/m/Y',
                                readOnly: true,
                                value: null,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        id: 'main_panel_id',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numberfield',
                                fieldLabel: 'Estimasi Kerja',
                                hideLabel: false,
                                //hidden:true,
                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                name: 'est_kerja',
                                id: 'est_kerjaid',
                                ref: '../est_kerja',
                                maxLength: 11,
                                value: 0,
                                allowBlank: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama SA',
                                hideLabel: false,
                                // readOnly: (this.modez !== 0 && this.modez !== 1),
                                readOnly: 1,
                                name: 'nama_sa',
                                id: 'nama_said',
                                ref: '../nama_sa',
                                maxLength: 100,
                                allowBlank: 0,
                                value: USER,
                                anchor: '100%'
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 75,
                        width: 225,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'xdatefield',
                                ref: '../est_start',
                                fieldLabel: 'Est. Mulai',
                                name: 'est_start',
                                id: 'est_startid',
                                format: 'd/m/Y',
                                value: DATE_NOW,
                                readOnly: true,
                                allowBlank: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../est_end',
                                fieldLabel: 'Est. Selesai',
                                name: 'est_end',
                                id: 'est_endid',
                                readOnly: true,
                                format: 'd/m/Y',
                                allowBlank: 0,
                                anchor: '100%'
                            },
                        ]
                    },
                    {
                        xtype: 'tabpanel',
                        tabBarPosition: 'top',
                        deferredRender: false,
                        colspan: 4,
                        activeTab: 0,
                        resizeTabs: true,
                        width: 892,
                        height: 320,
                        tabWidth: 290,
                        tabBar: {
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            defaults: {flex: 1}
                        },
                        items: [
                            {
                                xtype: 'panel',
                                title: "Data Customer",
                                ref: '../../gridCustomer',
                                frameHeader: !1,
                                header: !1,
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                margins: '35 5 5 0',
                                layout: 'column',
                                border: false,
                                defaults: {columnWidth: .33},
                                items: [
                                    {
                                        xtype: 'panel',
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                        labelWidth: 75,
                                        width: 225,
                                        labelAlign: 'left',
                                        layout: 'form',
                                        border: false,
                                        items: [
                                            {
                                                xtype: 'combo',
                                                fieldLabel: 'No. Polisi',
                                                ref: '../../mobil_id',
                                                triggerAction: 'query',
                                                lazyRender: true,
                                                mode: 'remote',
                                                forceSelection: false,
                                                autoSelect: false,
                                                store: jun.rztMobilBpCmp,
                                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                                id: "mobil_body_paint_id",
                                                hiddenName: 'mobil_id',
                                                valueField: 'mobil_id',
                                                displayField: 'no_pol',
                                                hideTrigger: true,
                                                minChars: 2,
                                                matchFieldWidth: !1,
                                                pageSize: 20,
                                                itemSelector: 'div.search-item',
                                                tpl: new Ext.XTemplate(
                                                    '<tpl for="."><div class="search-item">',
                                                    '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                                    '{customer_id}',
                                                    '</div></tpl>'
                                                ),
                                                allowBlank: false,
                                                listWidth: 450,
                                                lastQuery: "",
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'combo',
                                                typeAhead: true,
                                                triggerAction: 'all',
                                                lazyRender: true,
                                                mode: 'local',
                                                forceSelection: true,
                                                fieldLabel: 'Merk Mobil',
                                                store: jun.rztMerkCmp,
                                                hiddenName: 'merk_id',
                                                valueField: 'merk_id',
                                                readOnly: true,
                                                id: 'merk_id',
                                                displayField: 'nama',
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'combo',
                                                typeAhead: true,
                                                triggerAction: 'all',
                                                lazyRender: true,
                                                mode: 'local',
                                                forceSelection: true,
                                                fieldLabel: 'Tipe Mobil',
                                                store: jun.rztMobilTipeBpCmp,
                                                hiddenName: 'mobil_tipe_id',
                                                valueField: 'mobil_tipe_id',
                                                readOnly: true,
                                                id: 'mobil_tipe_id',
                                                displayField: 'nama',
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'combo',
                                                typeAhead: true,
                                                triggerAction: 'all',
                                                lazyRender: true,
                                                mode: 'local',
                                                forceSelection: true,
                                                fieldLabel: 'Kategori',
                                                store: jun.rztMobilKategoriBpCmp,
                                                hiddenName: 'mobil_kategori_id',
                                                valueField: 'mobil_kategori_id',
                                                id: 'mobil_kategori_id',
                                                readOnly: true,
                                                displayField: 'nama',
                                                anchor: '100%'
                                            },
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                        labelWidth: 75,
                                        width: 225,
                                        labelAlign: 'left',
                                        layout: 'form',
                                        border: false,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Mesin',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'no_mesin',
                                                id: 'no_mesinid',
                                                ref: '../no_mesin',
                                                readOnly: true,
                                                maxLength: 50,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Rangka',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'no_rangka',
                                                id: 'no_rangkaid',
                                                ref: '../no_rangka',
                                                readOnly: true,
                                                maxLength: 50,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Warna',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'warna',
                                                id: 'warnaid',
                                                ref: '../warna',
                                                readOnly: true,
                                                maxLength: 50,
                                                //allowBlank: ,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Tahun',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'tahun_pembuatan',
                                                id: 'tahun_pembuatanid',
                                                ref: '../tahun_pembuatan',
                                                readOnly: true,
                                                // decimalSeparator: '',
                                                maxLength: 11,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'KM',
                                                hideLabel: false,
                                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                                name: 'tahun_km',
                                                id: 'tahun_kmid',
                                                ref: '../tahun_km',
                                                maxLength: 20,
                                                allowBlank: 0,
                                                anchor: '100%'
                                            },
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                        width: 225,
                                        height: 325,
                                        layout: {
                                            type: 'vbox',
                                            padding: '5',
                                            align: 'stretch'
                                        },
                                        border: false,
                                        items: [
                                            {
                                                xtype: "label",
                                                text: "Customer:",
                                                // flex: 1
                                            },
                                            {
                                                xtype: 'textarea',
                                                hideLabel: false,
                                                id: 'customer_id',
                                                readOnly: true,
                                                // width: 175,
                                                height: 105,
                                                // flex: 2
                                            },
                                            {
                                                xtype: "label",
                                                text: "Catatan:",
                                                hidden: (this.modez < 3),
                                                // flex: 1
                                            },
                                            {
                                                xtype: 'textarea',
                                                hideLabel: false,
                                                id: 'note_id',
                                                name: 'note_',
                                                hidden: (this.modez < 3),
                                                disabled: (this.modez !== 3 && this.modez !== 4),
                                                // width: 175,
                                                height: 105,
                                                // flex: 2
                                            },
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                title: "Data Pembiayaan",
                                ref: '../../gridPembiayaan',
                                frameHeader: !1,
                                header: !1,
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                margins: '35 5 5 0',
                                layout: 'column',
                                border: false,
                                defaults: {columnWidth: .33},
                                items: [
                                    {
                                        xtype: 'panel',
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                        labelWidth: 125,
                                        width: 225,
                                        labelAlign: 'left',
                                        layout: 'form',
                                        border: false,
                                        items: [
                                            {
                                                xtype: 'radiogroup',
                                                fieldLabel: 'Status Pembiayaan',
                                                id: 'radio-biaya',
                                                disabled: (this.modez !== 0 && this.modez !== 1),
                                                items: [
                                                    {
                                                        boxLabel: 'Pribadi',
                                                        name: 'biaya',
                                                        inputValue: 'PRIBADI',
                                                        id: 'biaya-pribadi',
                                                        // checked: true
                                                    },
                                                    {
                                                        boxLabel: 'Asuransi',
                                                        name: 'biaya',
                                                        inputValue: 'ASURANSI',
                                                        id: 'biaya-asuransi',
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                        labelWidth: 100,
                                        width: 225,
                                        id: 'frm-biaya-asurasi-1',
                                        labelAlign: 'left',
                                        layout: 'form',
                                        border: false,
                                        items: [
                                            {
                                                xtype: 'mfcombobox',
                                                searchFields: [
                                                    'kode'
                                                ],
                                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                                fieldLabel: 'Kode Asuransi',
                                                mode: 'local',
                                                store: jun.rztAsuransiBpCmp,
                                                itemSelector: 'tr.search-item',
                                                tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                                    '<th>Kode</th><th>Nama</th></tr></thead>',
                                                    '<tbody><tpl for="."><tr class="search-item">',
                                                    '<td>{kode}</td><td>{nama}</td>',
                                                    '</tr></tpl></tbody></table>'),
                                                // allowBlank: false,
                                                listWidth: 500,
                                                anchor: '100%',
                                                hideTrigger: true,
                                                hiddenName: 'asuransi_bp_id',
                                                valueField: 'asuransi_bp_id',
                                                displayField: 'kode',
                                                id: 'asuransi_bp_id',
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Nama',
                                                hideLabel: false,
                                                ref: '../asuransi_nama',
                                                id: 'asuransi_nama_id',
                                                maxLength: 50,
                                                readOnly: true,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Alamat',
                                                hideLabel: false,
                                                ref: '../asuransi_alamat',
                                                maxLength: 50,
                                                id: 'asuransi_alamat_id',
                                                readOnly: true,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kota',
                                                hideLabel: false,
                                                ref: '../asuransi_kota',
                                                id: 'asuransi_kota_id',
                                                readOnly: true,
                                                maxLength: 50,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Polis Asuransi',
                                                hideLabel: false,
                                                //hidden:true,
                                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                                name: 'no_polis',
                                                id: 'no_polisid',
                                                ref: '../no_polis',
                                                maxLength: 50,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'xdatefield',
                                                ref: '../asuransi_start',
                                                fieldLabel: 'Jangka Waktu',
                                                name: 'asuransi_start',
                                                id: 'asuransi_startid',
                                                format: 'd/m/Y',
                                                value: null,
                                                //allowBlank: 1,
                                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'xdatefield',
                                                ref: '../asuransi_end',
                                                fieldLabel: 'Sampai Dengan',
                                                name: 'asuransi_end',
                                                id: 'asuransi_endid',
                                                format: 'd/m/Y',
                                                value: null,
                                                //allowBlank: 1,
                                                readOnly: (this.modez !== 0 && this.modez !== 1),
                                                anchor: '100%'
                                            },
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                        width: 125,
                                        labelAlign: 'left',
                                        layout: 'form',
                                        border: false,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. SPK',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'no_spk',
                                                id: 'no_spkid',
                                                ref: '../no_spk',
                                                maxLength: 50,
                                                readOnly: true,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'xdatefield',
                                                ref: '../tgl_spk',
                                                fieldLabel: 'Tgl Spk',
                                                name: 'tgl_spk',
                                                id: 'tgl_spkid',
                                                format: 'd/m/Y',
                                                readOnly: true,
                                                value: null,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'Banyak Kejadian',
                                                hideLabel: false,
                                                value: 0,
                                                readOnly: true,
                                                ref: '../qty_kejadian',
                                                id: 'qty_kejadian_id',
                                                name: 'qty_kejadian',
                                                maxLength: 20,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'Nilai Own Risk',
                                                hideLabel: false,
                                                value: 0,
                                                readOnly: true,
                                                ref: '../own_risk',
                                                id: 'own_risk_id',
                                                name: 'own_risk',
                                                maxLength: 20,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'Total Own Risk',
                                                hideLabel: false,
                                                value: 0,
                                                readOnly: true,
                                                ref: '../total_asuransi',
                                                id: 'total_asuransi_id',
                                                name: 'total_asuransi',
                                                maxLength: 20,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'checkbox',
                                                boxLabel: "Update No SPK",
                                                hidden: (this.modez < 3),
                                                id: 'update_spkid',
                                                value: 0,
                                                inputValue: 1,
                                                uncheckedValue: 0
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. SPK',
                                                hideLabel: false,
                                                hidden: (this.modez < 3),
                                                name: 'no_spk_1',
                                                id: 'no_spk_1id',
                                                ref: '../no_spk_1',
                                                maxLength: 50,
                                                //allowBlank: 1,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'xdatefield',
                                                ref: '../tgl_spk_1',
                                                fieldLabel: 'Tgl Spk',
                                                name: 'tgl_spk_1',
                                                id: 'tgl_spk_1id',
                                                format: 'd/m/Y',
                                                value: null,
                                                hidden: (this.modez < 3),
                                                anchor: '100%'
                                            },
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                frame: false,
                                title: "Data Jasa",
                                bodyStyle: 'background-color: #E4E4E4',
                                width: 225,
                                height: 300,
                                layout: {
                                    type: 'vbox',
                                    // padding: '5',
                                    align: 'stretch'
                                },
                                border: false,
                                items: [
                                    new jun.BpJasaGrid({
                                        title: "Data Jasa",
                                        //x: 5,
                                        //y: 65,
                                        ref: '../../gridJasa',
                                        modez: this.modez,
                                        frameHeader: !1,
                                        header: !1,
                                        height: 245,
                                    }),
                                    {
                                        xtype: 'panel',
                                        // flex: 1,
                                        // height: 61,
                                        ref: '../../pnlJasa',
                                        frameHeader: !1,
                                        header: !1,
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4',
                                        // margins: '35 5 5 0',
                                        layout: 'column',
                                        border: false,
                                        defaults: {columnWidth: .33},
                                        items: [
                                            {
                                                xtype: 'panel',
                                                frame: false,
                                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                                labelWidth: 125,
                                                width: 225,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                border: false,
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        fieldLabel: 'Total Jasa BP',
                                                        hideLabel: false,
                                                        value: 0,
                                                        readOnly: true,
                                                        //hidden:true,
                                                        name: 'total_jasa_line',
                                                        ref: '../tot_jasa_bp',
                                                        id: 'tot_jasa_bp_id',
                                                        maxLength: 20,
                                                        //allowBlank: 1,
                                                        anchor: '100%'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'panel',
                                                frame: false,
                                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                                labelWidth: 100,
                                                width: 225,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                border: false,
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        fieldLabel: 'Total Disc',
                                                        hideLabel: false,
                                                        value: 0,
                                                        readOnly: true,
                                                        //hidden:true,
                                                        name: 'total_jasa_discrp',
                                                        ref: '../tot_disc_bp',
                                                        id: 'tot_disc_bp_id',
                                                        maxLength: 20,
                                                        //allowBlank: 1,
                                                        anchor: '100%'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'panel',
                                                frame: false,
                                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                                width: 125,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                border: false,
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        fieldLabel: 'Total Jasa',
                                                        hideLabel: false,
                                                        //hidden:true,
                                                        // name: 'tahun_km',
                                                        value: 0,
                                                        readOnly: true,
                                                        ref: '../tot_jasa',
                                                        id: 'tot_jasa_id',
                                                        maxLength: 20,
                                                        //allowBlank: 1,
                                                        anchor: '100%'
                                                    },
                                                ]
                                            }
                                        ]
                                    },
                                ]
                            },
                            {
                                xtype: 'panel',
                                frame: false,
                                title: "Data Parts",
                                bodyStyle: 'background-color: #E4E4E4',
                                width: 225,
                                height: 300,
                                layout: {
                                    type: 'vbox',
                                    // padding: '5',
                                    align: 'stretch'
                                },
                                border: false,
                                items: [
                                    new jun.BpPartsGrid({
                                        title: "Data Parts",
                                        modez: this.modez,
                                        body_paint_id: this.id,
                                        //x: 5,
                                        //y: 65,
                                        ref: '../../gridParts',
                                        frameHeader: !1,
                                        header: !1,
                                        height: 245,
                                    }),
                                    {
                                        xtype: 'panel',
                                        // flex: 1,
                                        // height: 61,
                                        ref: '../../pnlParts',
                                        frameHeader: !1,
                                        header: !1,
                                        frame: false,
                                        bodyStyle: 'background-color: #E4E4E4',
                                        // margins: '35 5 5 0',
                                        layout: 'column',
                                        border: false,
                                        defaults: {columnWidth: .33},
                                        items: [
                                            {
                                                xtype: 'panel',
                                                frame: false,
                                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                                labelWidth: 125,
                                                width: 225,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                border: false,
                                                items: []
                                            },
                                            {
                                                xtype: 'panel',
                                                frame: false,
                                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                                labelWidth: 100,
                                                width: 225,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                border: false,
                                                items: []
                                            },
                                            {
                                                xtype: 'panel',
                                                frame: false,
                                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                                width: 125,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                border: false,
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        fieldLabel: 'Total Parts BP',
                                                        hideLabel: false,
                                                        value: 0,
                                                        readOnly: true,
                                                        //hidden:true,
                                                        // name: 'tahun_km',
                                                        ref: '../tot_parts_bp',
                                                        id: 'tot_parts_bp',
                                                        maxLength: 20,
                                                        //allowBlank: 1,
                                                        anchor: '100%'
                                                    },
                                                ]
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        // flex: 1,
                        height: 85,
                        // ref: '../../pnlParts',
                        width: 892,
                        frameHeader: !1,
                        colspan: 4,
                        header: !1,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4',
                        // margins: '35 5 5 0',
                        layout: 'column',
                        border: false,
                        defaults: {columnWidth: .33},
                        items: [
                            {
                                xtype: 'panel',
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                labelWidth: 125,
                                width: 200,
                                labelAlign: 'left',
                                layout: 'form',
                                border: false,
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total Jasa',
                                        hideLabel: false,
                                        //hidden:true,
                                        value: 0,
                                        readOnly: true,
                                        name: 'total_jasa',
                                        id: 'total_jasaid',
                                        ref: '../total_jasa',
                                        maxLength: 15,
                                        //allowBlank: ,
                                        anchor: '100%'
                                    },
                                ]
                            },
                            {
                                xtype: 'panel',
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                labelWidth: 100,
                                // width: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                border: false,
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total Parts',
                                        hideLabel: false,
                                        value: 0,
                                        readOnly: true,
                                        //hidden:true,
                                        name: 'total_parts',
                                        id: 'total_partsid',
                                        ref: '../total_parts',
                                        maxLength: 15,
                                        //allowBlank: ,
                                        anchor: '100%'
                                    },
                                ]
                            },
                            {
                                xtype: 'panel',
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                labelWidth: 100,
                                width: 225,
                                height: 85,
                                labelAlign: 'left',
                                layout: 'form',
                                border: false,
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total',
                                        hideLabel: false,
                                        value: 0,
                                        readOnly: true,
                                        //hidden:true,
                                        name: 'total',
                                        id: 'totalid',
                                        ref: '../total',
                                        maxLength: 15,
                                        //allowBlank: ,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'PPN',
                                        hideLabel: false,
                                        value: 0,
                                        readOnly: true,
                                        //hidden:true,
                                        name: 'vatrp',
                                        id: 'vatrpid',
                                        ref: '../vatrp',
                                        maxLength: 15,
                                        //allowBlank: ,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'hidden',
                                        value: 10,
                                        name: 'vat',
                                        id: 'vatid',
                                        ref: '../vat',
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Grand Total',
                                        hideLabel: false,
                                        value: 0,
                                        readOnly: true,
                                        //hidden:true,
                                        name: 'grand_total',
                                        id: 'grand_totalid',
                                        ref: '../grand_total',
                                        maxLength: 15,
                                        //allowBlank: ,
                                        anchor: '100%'
                                    },
                                ]
                            },
                        ]
                    },
                    // {
                    //     xtype: 'panel',
                    //     frame: false,
                    //     bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                    //     labelWidth: 75,
                    //     width: 225,
                    //     labelAlign: 'left',
                    //     layout: 'form',
                    //     border: false,
                    //     items: []
                    // },
                    {
                        xtype: 'hidden',
                        id: 'syspref_disc1',
                    },
                    {
                        xtype: 'hidden',
                        id: 'syspref_disc2',
                    },
                    {
                        xtype: 'hidden',
                        id: 'syspref_mxdisc',
                    },
                    {
                        xtype: 'hidden',
                        id: 'syspref_mnhpp',
                    },
                    {
                        xtype: 'hidden',
                        id: 'syspref_mnhpp2',
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Simpan',
                //     hidden: true,
                //     ref: '../btnSave'
                // },
                {
                    xtype: 'button',
                    text: 'Create Work Order',
                    hidden: true,
                    ref: '../btnCreateWO'
                },
                {
                    xtype: 'button',
                    text: 'Create Invoice',
                    hidden: true,
                    ref: '../btnCreateInvoice'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                },
            ]
        };
        jun.BodyPaintWin.superclass.initComponent.call(this);
        this.on('close', this.onClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCreateWO.on('click', this.onbtnCreateWOClick, this);
        this.btnCreateInvoice.on('click', this.onbtnCreateInvoiceClick, this);
        // this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on('afterrender', this.onafterrender, this);
        Ext.getCmp('asuransi_bp_id').on('select', this.onAsuransiSelect, this);
        Ext.getCmp('mobil_body_paint_id').on('select', this.onMobilSelect, this);
        Ext.getCmp('radio-biaya').on('change', this.onRadioBiayaChange, this);
        Ext.getCmp('est_kerjaid').on('change', this.onEstChange, this);
        switch (this.modez) {
            case 0 :
                this.btnSaveClose.setVisible(true);
                this.btnCreateWO.setVisible(false);
                this.btnCreateInvoice.setVisible(false);
                break;
            case 1 :
                this.btnSaveClose.setVisible(true);
                this.btnCreateWO.setVisible(false);
                this.btnCreateInvoice.setVisible(false);
                break;
            case 2 :
                this.btnSaveClose.setVisible(false);
                this.btnCreateWO.setVisible(true);
                this.btnCreateInvoice.setVisible(false);
                break;
            case 3 :
                this.btnSaveClose.setVisible(true);
                this.btnCreateWO.setVisible(false);
                this.btnCreateInvoice.setVisible(false);
                break;
            case 4 :
                this.btnSaveClose.setVisible(false);
                this.btnCreateWO.setVisible(false);
                this.btnCreateInvoice.setVisible(true);
                break;
            case 5 :
                this.btnSaveClose.setVisible(false);
                this.btnCreateWO.setVisible(false);
                this.btnCreateInvoice.setVisible(false);
                break;
        }
    },
    onafterrender: function () {
        jun.rztBpSuppPartsCmp.load();
        Ext.Ajax.request({
            url: 'sysPrefs/loadBodyPaintDisc',
            method: 'GET',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.getCmp('syspref_disc1').setValue(response.msg.BPDISC1);
                Ext.getCmp('syspref_disc2').setValue(response.msg.BPDISC2);
                Ext.getCmp('syspref_mxdisc').setValue(response.msg.MXDISC);
                Ext.getCmp('syspref_mnhpp').setValue(response.msg.MNHPP);
                Ext.getCmp('syspref_mnhpp2').setValue(response.msg.MNHPP2);
                let nospk1 = Ext.getCmp('no_spk_1id').getValue();
                let tglspk1 = Ext.getCmp('tgl_spk_1id').getValue();
                if (nospk1 != '' || tglspk1 != '') {
                    Ext.getCmp('update_spkid').setValue(1);
                }
                document.getElementById("est_kerjaid").focus();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onClose: function () {
        jun.rztBpJasa.removeAll();
        jun.rztBpParts.removeAll();
    },
    onEstChange: function (t, n, o) {
        var s = Ext.getCmp('est_startid').getValue();
        let e = new Date(s.getTime() + (n * 24 * 60 * 60 * 1000));
        Ext.getCmp('est_endid').setValue(e);
    },
    onRadioBiayaChange: function (t, c) {
        if (c.inputValue == 'PRIBADI') {
            Ext.getCmp('asuransi_bp_id').setValue();
            Ext.getCmp('asuransi_nama_id').setValue();
            Ext.getCmp('asuransi_alamat_id').setValue();
            Ext.getCmp('asuransi_kota_id').setValue();
            Ext.getCmp('no_polisid').setValue();
            Ext.getCmp('asuransi_startid').setValue();
            Ext.getCmp('asuransi_endid').setValue();
            Ext.getCmp('frm-biaya-asurasi-1').setDisabled(true);
        } else {
            Ext.getCmp('frm-biaya-asurasi-1').setDisabled(false);
        }
    },
    onMobilSelect: function (c, r, i) {
        Ext.getCmp('mobil_tipe_id').setValue(r.data.mobil_tipe_id);
        Ext.getCmp('no_mesinid').setValue(r.data.no_mesin);
        Ext.getCmp('no_rangkaid').setValue(r.data.no_rangka);
        Ext.getCmp('warnaid').setValue(r.data.warna);
        Ext.getCmp('tahun_pembuatanid').setValue(r.data.tahun_pembuatan);
        // Ext.getCmp('tahun_kmid').setValue(r.data.tahun_km);
        Ext.getCmp('merk_id').setValue(r.data.mobilTipe.merk_id);
        Ext.getCmp('mobil_kategori_id').setValue(r.data.mobilTipe.mobil_kategori_id);
        Ext.getCmp('customer_id').setValue(r.data.customer.nama_customer + "\n" +
            r.data.customer.ktp + "\n" +
            r.data.customer.email + "\n" +
            r.data.customer.alamat + "\n" +
            r.data.customer.telp
        );
    },
    onAsuransiSelect: function (c, r, i) {
        Ext.getCmp('asuransi_nama_id').setValue(r.data.nama);
        Ext.getCmp('asuransi_alamat_id').setValue(r.data.address);
        Ext.getCmp('asuransi_kota_id').setValue(r.data.city);
    },
    btnDisabled: function (status) {
        this.btnCreateWO.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onbtnCreateWOClick: function () {
        this.btnDisabled(true);
        var modeBiaya;
        var rd = Ext.getCmp('radio-biaya').getValue();
        if (rd == null) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: "Status Pembiayaan harus diisi.",
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
        } else {
            modeBiaya = rd.inputValue;
            if (modeBiaya == "ASURANSI") {
                var f = new jun.AsuransiSPK({body_paint_id: this.id});
                f.show();
            } else if (modeBiaya == "PRIBADI") {
                Ext.Ajax.request({
                    url: 'BodyPaint/CreateWO/id/' + this.id,
                    method: 'POST',
                    scope: this,
                    success: function (f, a) {
                        jun.rztBodyPaint.reload();
                        jun.rztWOBodyPaint.reload();
                        var response = Ext.decode(f.responseText);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                        this.close();
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        }
        this.btnDisabled(false);
    },
    onbtnCreateInvoiceClick: async function () {
        this.btnDisabled(true);
        var status = true;
        await jun.rztBpParts.each(function (e, d) {
            if (e.data.posting == null) {
                status = false;
            }
        }, this);
        if (status) {
            Ext.Ajax.request({
                url: 'BodyPaint/CreateInv/id/' + this.id,
                method: 'POST',
                scope: this,
                success: function (f, a) {
                    jun.rztWOBodyPaint.reload();
                    jun.rztINVBodyPaint.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    this.close();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        } else {
            Ext.Msg.alert('Failure', "Parts harus diposting semua, sebelum membuat invoice.");
            this.btnDisabled(false);
        }
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (Ext.getCmp('est_kerjaid').getValue() <= 0) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Estimasi kerja harus lebih dari 0',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            Ext.getCmp('est_kerjaid').focus();
            this.btnDisabled(false);
            return;
        }
        if (Ext.getCmp('nama_said').getValue() == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Nama SA tidak boleh kosong.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            Ext.getCmp('nama_said').focus();
            this.btnDisabled(false);
            return;
        }
        if (Ext.getCmp('mobil_body_paint_id').getValue() == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Nopol tidak boleh kosong.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            Ext.getCmp('mobil_body_paint_id').focus();
            this.btnDisabled(false);
            return;
        }
        if (Ext.getCmp('tahun_kmid').getValue() == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'KM tidak boleh kosong.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            Ext.getCmp('tahun_kmid').focus();
            this.btnDisabled(false);
            return;
        }
        if (Ext.getCmp('radio-biaya').getValue() == null) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Status pembiayaan harus dipilih.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            Ext.getCmp('radio-biaya').focus();
            this.btnDisabled(false);
            return;
        }
        if (Ext.getCmp('radio-biaya').getValue().inputValue == 'ASURANSI') {
            if (Ext.getCmp('asuransi_bp_id').getValue() == '') {
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: 'Kode asuransi harus dipilih.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('asuransi_bp_id').focus();
                this.btnDisabled(false);
                return;
            }
            if (Ext.getCmp('no_polisid').getValue() == '') {
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: 'No Polis asuransi harus diisi.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('no_polisid').focus();
                this.btnDisabled(false);
                return;
            }
            // if (Ext.getCmp('asuransi_startid').getValue() == '') {
            //     Ext.MessageBox.show({
            //         title: 'Info',
            //         msg: 'Jangka waktu harus diisi.',
            //         buttons: Ext.MessageBox.OK,
            //         icon: Ext.MessageBox.INFO
            //     });
            //     Ext.getCmp('asuransi_startid').focus();
            //     this.btnDisabled(false);
            //     return;
            // }
            // if (Ext.getCmp('asuransi_endid').getValue() == '') {
            //     Ext.MessageBox.show({
            //         title: 'Info',
            //         msg: 'Sampai dengan harus diisi.',
            //         buttons: Ext.MessageBox.OK,
            //         icon: Ext.MessageBox.INFO
            //     });
            //     Ext.getCmp('asuransi_endid').focus();
            //     this.btnDisabled(false);
            //     return;
            // }
        }
        var urlz = 'BodyPaint/create/';
        Ext.getCmp('form-BodyPaint').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                jasaDetils: Ext.encode(Ext.pluck(
                    jun.rztBpJasa.data.items, "data")),
                partDetils: Ext.encode(Ext.pluck(
                    jun.rztBpParts.data.items, "data")),
            },
            success: function (f, a) {
                jun.rztBodyPaint.reload();
                jun.rztWOBodyPaint.reload();
                jun.rztINVBodyPaint.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BodyPaint').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.AsuransiSPK = Ext.extend(Ext.Window, {
    title: "Asuransi",
    // iconCls: "silk13-report",
    modez: 1,
    width: 350,
    height: 225,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-AsuransiSPK",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. SPK',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_spk',
                        ref: '../no_spk',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_spk',
                        fieldLabel: 'Tgl Spk',
                        name: 'tgl_spk',
                        format: 'd/m/Y',
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Banyak Kejadian',
                        hideLabel: false,
                        enableKeyEvents: true,
                        value: 0,
                        ref: '../qty_kejadian',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Nilai Own Risk',
                        hideLabel: false,
                        enableKeyEvents: true,
                        value: 0,
                        ref: '../own_risk',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total Own Risk',
                        hideLabel: false,
                        value: 0,
                        readOnly: true,
                        ref: '../total_asuransi',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-html",
                    text: "Save",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Cancel",
                    // hidden: (STORE != ''),
                    ref: "../btnCancel"
                },
            ]
        };
        jun.AsuransiSPK.superclass.initComponent.call(this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.qty_kejadian.on('keyup', this.calculate, this);
        this.own_risk.on('keyup', this.calculate, this);
    },
    calculate: function () {
        var qty_kejadian = parseInt(this.qty_kejadian.getValue());
        var own_risk = parseFloat(this.own_risk.getValue());
        this.total_asuransi.setValue(qty_kejadian * own_risk);
    },
    onbtnCancelclick: function () {
        this.close();
    },
    onbtnSaveclick: function () {
        var no_spk = this.no_spk.getValue();
        var tgl_spk = this.tgl_spk.getValue();
        var qty_kejadian = this.qty_kejadian.getValue();
        var own_risk = this.own_risk.getValue();
        var total_asuransi = this.total_asuransi.getValue();
        if (no_spk == '') {
            Ext.MessageBox.alert("Warning", "No SPK harus diisi.");
            return;
        }
        if (tgl_spk == '') {
            Ext.MessageBox.alert("Warning", "Tgl SPK harus diisi.");
            return;
        }
        // if (qty_kejadian <= 0) {
        //     Ext.MessageBox.alert("Warning", "Qty kejadian harus lebih dari 0.");
        //     return;
        // }
        // if (own_risk <= 0) {
        //     Ext.MessageBox.alert("Warning", "Nilai Own Risk harus lebih dari 0.");
        //     return;
        // }
        Ext.getCmp('no_spkid').setValue(no_spk);
        Ext.getCmp('tgl_spkid').setValue(tgl_spk);
        Ext.getCmp('qty_kejadian_id').setValue(qty_kejadian);
        Ext.getCmp('own_risk_id').setValue(own_risk);
        Ext.getCmp('total_asuransi_id').setValue(total_asuransi);
        var urlz = 'BodyPaint/create/';
        Ext.getCmp('form-BodyPaint').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: 2,
                id: this.body_paint_id,
                jasaDetils: Ext.encode(Ext.pluck(
                    jun.rztBpJasa.data.items, "data")),
                partDetils: Ext.encode(Ext.pluck(
                    jun.rztBpParts.data.items, "data")),
            },
            success: function (f, a) {
                jun.rztBodyPaint.reload();
                jun.rztWOBodyPaint.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp(this.body_paint_id).close();
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        this.close();
    }
});