jun.MobilTipeBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilTipeBpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilTipeBpStoreId',
            url: 'MobilTipeBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_tipe_id'},
                {name: 'merk_id'},
                {name: 'nama'},
                {name: 'mobil_kategori_id'},
            ]
        }, cfg));
    }
});
jun.rztMobilTipeBp = new jun.MobilTipeBpstore();
jun.rztMobilTipeBpCmp = new jun.MobilTipeBpstore();
jun.rztMobilTipeBpLib = new jun.MobilTipeBpstore();
jun.rztMobilTipeBpLib.load();
