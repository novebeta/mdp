jun.BpPartsWin = Ext.extend(Ext.Window, {
    title: 'BpParts',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-BpParts',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender:true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'body_paint_id',
                            store: jun.rztBodyPaint,
                            hiddenName:'body_paint_id',
                            valueField: 'body_paint_id',
                            displayField: 'est_no',
                            anchor: '100%'
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'nama',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'nama',
                                    id:'namaid',
                                    ref:'../nama',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'harga',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'harga',
                                    id:'hargaid',
                                    ref:'../harga',
                                    maxLength: 15,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'total_parts_line',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'total_parts_line',
                                    id:'total_parts_lineid',
                                    ref:'../total_parts_line',
                                    maxLength: 15,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.BpPartsWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'BpParts/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'BpParts/create/';
                }
             
            Ext.getCmp('form-BpParts').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztBpParts.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-BpParts').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});

jun.PostingWin = Ext.extend(Ext.Window, {
    title: 'Posting',
    modez:1,
    width: 400,
    height: 175,
    layout: 'form',
    data: null,
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-BpParts',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        ref: '../supplier',
                        store: jun.rztBpSuppPartsCmp,
                        hiddenName:'bp_supp_parts_id',
                        valueField: 'bp_supp_parts_id',
                        displayField: 'nama_supp',
                        anchor: '100%'
                    },
                    {

                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'HPP',
                        //hidden:true,
                        id: 'hpp_parts',
                        name: 'hpp',
                        ref: '../hpp',
                        maxLength: 30,
                        value: 0,
                        width: 100
                    },
                    {
                        xtype: 'checkbox',
                        boxLabel: "PPN",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        ref: '../ppn',
                        name: "ppn"
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.PostingWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()    {
        this.btnDisabled(true);
        var urlz;
        if (this.ppn.getValue() != 1){
            Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin parts ini tanpa PPN ?', function (result){
                if (result === 'no') {
                    this.btnDisabled(false);
                    return;
                }
                this.savePosting();
            }, this);
        }else{
            this.savePosting();
        }
    },

    savePosting: function (){
        record = this.data;
        var hpp = parseFloat(this.hpp.getValue());
        var ppn = this.ppn.getValue();
        var nilai = hpp;
        var ppnout = 0;
        var minhpp = parseFloat(Ext.getCmp('syspref_mnhpp').getValue());
        var minhpp2 = parseFloat(Ext.getCmp('syspref_mnhpp2').getValue());
        var supplier = this.supplier.getValue();
        let c = jun.rztBpSuppPartsLib.findExact("bp_supp_parts_id", supplier);
        let sup = jun.rztBpSuppPartsLib.getAt(c);
        if (sup == null){
            Ext.MessageBox.alert("Warning", "Supplier harus dipilih!");
            return;
        }
        if (sup.data.tipe === 'UNAUTHORIZED'){
            minhpp = minhpp2;
        }
        if (ppn){
            nilai = Math.floor(nilai / PEMBAGI_PPN);
            ppnout = hpp - nilai;
        }
        var range = (nilai / 100) * minhpp;
        var minmal_jual = nilai + range;
        if (minmal_jual > record.data.harga) {
            Ext.MessageBox.alert("Warning", "Harga jual terlalu rendah");
            return;
        }
        Ext.Ajax.request({
            url: 'bpParts/posting/id/' + record.data.bp_parts_id,
            method: 'POST',
            scope: this,
            params: {
                bp_supp_parts_id: supplier,
                hpp: nilai,
                ppn: this.ppn.getValue(),
                ppnout: ppnout,
                tipe: sup.data.tipe
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    jun.rztBpParts.reload();
                }
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });

                // this.btnDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }

                // this.btnDisabled(false);
            }
        });
        this.close();
    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});