jun.BpBayar2Grid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembayaran Asuransi",
    id: 'docs-jun.BpBayar2Grid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Tagih',
            sortable: true,
            resizable: true,
            dataIndex: 'tagih',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 100,
            renderer: jun.renderBank
        },
    ],
    initComponent: function () {
        this.store = jun.rztBpBayar2;
        jun.rztBpBayar2.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.tglfrom = Ext.getCmp('tglgridbayarasuransifrom').getValue();
                    b.params.tglto = Ext.getCmp('tglgridbayarasuransito').getValue();
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridbayarasuransifrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridbayarasuransito'
                        }]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Ganti Tgl Pembayaran Asuransi',
                    ref: '../btnEdit'
                },
            ]
        };
        this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        this.store.baseParams = {};
        jun.BpBayar2Grid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BpBayar2Win({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.bp_bayar_id;
        var form = new jun.BpBayar2TglWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BpBayar2/delete/id/' + record.json.bp_bayar_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBpBayar2.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BpBayar2TglWin = Ext.extend(Ext.Window, {
    title: 'Ganti Tgl Pembayaran Asuransi',
    modez: 1,
    width: 400,
    height: 140,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BpBayar2TglWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        format: 'd/m/Y',
                        // value: DATE_NOW,
                        // readOnly: true,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BpBayar2TglWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        var urlz  = 'bpBayar2/update/id/' + this.id;
        Ext.getCmp('form-BpBayar2TglWin').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                jun.rztBpBayar2.load();
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
