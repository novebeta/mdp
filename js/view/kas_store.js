jun.Kasstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kasstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KasStoreId',
            url: 'Kas',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kas_id'},
                {name: 'doc_ref'},
                {name: 'no_kwitansi'},
                {name: 'keperluan'},
                {name: 'total', type: 'float'},
                {name: 'total_debit', type: 'float'},
                {name: 'total_kredit', type: 'float'},
                {name: 'bank_id'},
                {name: 'amount', type: 'float'},
                {name: 'tgl'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'arus'},
                {name: 'visible'},
                {name: 'all_store'},
                {name: 'jml_cabang'},
                {name: 'subject'}
            ]
        }, cfg));
    }
});
//jun.rztKas = new jun.Kasstore();
jun.rztKas = new jun.Kasstore({baseParams: {mode: "masuk"}});
jun.rztKasKeluar = new jun.Kasstore({baseParams: {mode: "keluar"}});
jun.rztKasPusat = new jun.Kasstore({baseParams: {mode: "masuk_pusat"}});
jun.rztKasPusatKeluar = new jun.Kasstore({baseParams: {mode: "keluar_pusat"}});
jun.rztKasBp = new jun.Kasstore();
jun.rztKasKeluarBp = new jun.Kasstore();
//jun.rztKas.load();
