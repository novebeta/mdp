jun.UlptWin = Ext.extend(Ext.Window, {
    title: 'ULPT',
    modez: 1,
    width: 400,
    height: 280,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Ulpt',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. ULPT',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Sales Trans',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztSalestransULPTCmp,
                        hiddenName: 'salestrans_id',
                        valueField: 'salestrans_id',
                        displayField: 'doc_ref',
                        hideTrigger: true,
                        minChars: 1,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item-table",
                        tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                            '<div class="cell4" style="width: 150px;">{doc_ref}</div>',
                            '<div class="cell4" style="width: 100px;">{tgl:date("M j, Y")}</div>',
                            '<div class="cell4" style="width: 100px;">{no_customer}</div>',
                            '<div class="cell4" style="width: 350px;">{nama_customer}</div>',
                            '<div class="cell4" style="width: 50px;text-align: right;">{total:number("0,0.00")}</div>',
                            "</div></tpl>", '</div>'),
                        allowBlank: false,
                        listWidth: 750,
                        lastQuery: "",
                        name: 'salestrans_id',
                        ref: '../salestrans_id',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Amount',
                        hideLabel: false,
                        //hidden:true,
                        name: 'amount',
                        ref: '../amount',
                        maxLength: 30,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Pay Method',
                        store: jun.rztBankCmp,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        value: SYSTEM_BANK_CASH,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Card Type',
                        store: jun.rztCardCmp,
                        ref: '../card',
                        hiddenName: 'card_id',
                        valueField: 'card_id',
                        displayField: 'card_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        maxLength: 600,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.UlptWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onActivate: function (t) {
        this.tgl.setReadOnly(!EDIT_TGL);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Ulpt/update/id/' + this.id;
        } else {
            urlz = 'Ulpt/create/';
        }
        Ext.getCmp('form-Ulpt').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztUlpt.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Ulpt').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});