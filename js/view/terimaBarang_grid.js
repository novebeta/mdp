/*
 * 'Buat Penerima Barang'
 * - Menu untuk membuat penerimaan barang dari supplier
 * - Menu untuk Warehouse
 */
jun.CreateTerimaBarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Input Penerimaan Barang",
    id: 'docs-jun.CreateTerimaBarangGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 40,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_po',
            width: 60,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. Surat Jalan',
            sortable: true,
            resizable: true,
            dataIndex: 'no_sj',
            width: 60,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 140,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 30,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case TB_DRAFT :
                        return 'DRAFT';
                    case TB_OPEN :
                        //metaData.style += "background-color: #AAFFD4;";
                        return 'OPEN';
                    case TB_INVOICED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'INVOICED';
                    case TB_CLOSED :
                        metaData.style += "background-color: #AAD4FF;";
                        return 'CLOSED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['myId','displayText'],
                    data: [['all', 'ALL'], [-1, 'DRAFT'], [0, 'OPEN'], [1, 'INVOICED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Editable',
            sortable: true,
            resizable: true,
            dataIndex: 'lock_edit',
            width: 30,
            renderer : function(value, metaData, record, rowIndex){
                value && (metaData.css += ' silk13-lock ');
                return '';
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'UNLOCKED'], [1, 'LOCKED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangPurchasable.getTotalCount() === 0) {
            jun.rztBarangPurchasable.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        
        this.store = jun.rztCreateTerimaBarang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    ref: '../botbar',
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Penerimaan Barang',
                    ref: '../btnAdd'
                },
                { xtype: 'tbseparator' },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Open TB',
                    ref: '../btnOpen',
                    iconCls: 'silk13-flag_green',
                    hidden: NATASHA_CUSTOM
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Print Retur',
                    ref: '../btnPrintRetur',
                    iconCls: 'silk13-page_white_excel'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-PrintRetur",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "terima_barang_id",
                            ref: "../../terima_barang_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Create Transfer',
                    ref: '../btnCreateTransfer'
                }
            ]
        };
        
        jun.CreateTerimaBarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnOpen.on('Click', this.btnOpenOnClick, this);
        this.btnPrintRetur.on('Click', this.printRetur, this);
        this.btnCreateTransfer.on('Click', this.createtransfer, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        
        this.store.baseParams = { mode: "grid" };
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        
        var filter = (r.get('status') == TB_DRAFT || r.get('status') == TB_OPEN) && (r.get('store') == STORE || HEADOFFICE) && r.get('lock_edit') == 0;
        this.btnEdit.setText(filter ?"Edit":"View");
        
    },
    loadForm: function () {
        var form = new jun.TerimaBarangWin({
            modez: 0,
            title: "Input Penerimaan Barang",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Penerimaan Barang.");
            return;
        }
        
        jun.rztPoInReceiveCmp.baseParams = {};
        jun.rztPoInReceiveCmp.load({
            params: { po_id: this.record.data.po_id },
            callback: function(r){
              /*loadEditForm*/
                var ttl = "";
                switch(this.record.get('status')){
                    case TB_DRAFT : ttl = "DRAFT"; break;
                    case TB_OPEN : ttl = "OPEN"; break;
                    case TB_INVOICED : ttl = "INVOICED"; break;
                    case TB_CLOSED : ttl = "CLOSED"; break;
                }
                
                var filter = (this.record.get('status') == TB_DRAFT || this.record.get('status') == TB_OPEN) && (this.record.get('store') == STORE || HEADOFFICE) && this.record.get('lock_edit') == 0;
                
                var form = new jun.TerimaBarangWin({
                    modez: ( filter ? 1 : 2),
                    id: this.record.get('terima_barang_id'),
                    title: ( filter ? "Edit" : "View")+" Penerimaan Barang  ["+ttl+"]",
                    iconCls: filter?'silk13-pencil':'silk13-eye'
                });
                form.show(this);
                form.formz.getForm().loadRecord(this.record);
              /*End of - loadEditForm*/
            },
            scope: this
        });
        jun.rztPoInReceiveCmp.baseParams = {};
        
        jun.rztTerimaBarangDetails.baseParams = { terima_barang_id: selectedz.json.terima_barang_id };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    createtransfer: function() {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih data Penerimaan Barang.");
            return;
        }

        var form = new jun.DroppingWin({
            modez: 3,
            id: this.record.get('terima_barang_id'),
            sj: this.record.get('no_sj'),
            title: "Create Transfer"
        });
        form.show(this);
        jun.rztDroppingTerima.baseParams = {
            id_terima: this.record.get('terima_barang_id')
        };
        jun.rztDroppingTerima.load();
        jun.rztDroppingTerima.baseParams = {};
    },
    btnOpenOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih data Penerimaan Barang.");
            return;
        }
        if (!(this.record.get('store') == STORE || HEADOFFICE)){
            Ext.MessageBox.alert("Warning", "Anda tidak diperbolehkan untuk memposting data Penerimaan Barang ini.");
            return;
        }
        if (this.record.get('status') != PR_DRAFT){
            Ext.MessageBox.alert("Warning", "Data Penerimaan Barang telah diposting sebelumnya.");
            return;
        }
        Ext.MessageBox.confirm('Open PR', 'Apakah anda yakin ingin memposting data Penerimaan Barang ini?', this.openTB, this);
    },
    openTB: function (btn) {
        if (btn == 'no') {
            return;
        }
        
        Ext.Ajax.request({
            url: 'TerimaBarang/Open',
            method: 'POST',
            params: {
                id: this.record.get('terima_barang_id')
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printRetur: function(){
        var selectedz = this.sm.getSelected();
        
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        
        Ext.getCmp("form-PrintRetur").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintRetur").getForm().url = "Report/PrintReturBarang";
        this.terima_barang_id.setValue(selectedz.json.terima_barang_id);
        var form = Ext.getCmp('form-PrintRetur').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    ReturnSupplierItem: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Terima Barang");
            return;
        }
        if (this.record.get('status') == TB_DRAFT){
            Ext.MessageBox.alert("Warning", "Data Penerimaan Barang belum diposting.");
            return;
        }
        
        var dataPO = new jun.PoInstore();
        dataPO.load({
            params: { po_id: this.record.data.po_id },
            callback: function(r){
              /*ReturnSupplierItem*/
                var idz = selectedz.json.terima_barang_id;
                
                var form = new jun.ReturnTransferItemWin({
                    modez: 0,
                    terima_barang_id: idz,
                    title: "Return Supplier Item",
                    storeGridDetil: new jun.TransferItemDetailsstore({url: 'TerimaBarangDetails/ItemDetilReturn'})
                });
                form.show(this);
                form.tgl.setValue(DATE_NOW);
                form.disc.setValue(r[0].get('disc'));
                form.supplier.setValue(r[0].get('supplier_id'));
                //form.store.setValue(selectedz.json.store);
                
                form.storeGridDetil.load({
                    params: {terima_barang_id: idz}
                });
                
              /*End of - ReturnSupplierItem*/
            },
            scope: this
        });
    }
});

/*
 * 'Penerima Barang'
 * - Menu untuk invoicing
 * - Menu untuk divisi Purchasing
 */
jun.TerimaBarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Penerimaan Barang",
    id: 'docs-jun.TerimaBarangGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 40,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_po',
            width: 60,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. Surat Jalan',
            sortable: true,
            resizable: true,
            dataIndex: 'no_sj',
            width: 60,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 140,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 30,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case TB_DRAFT :
                        return 'DRAFT';
                    case TB_OPEN :
                        //metaData.style += "background-color: #AAFFD4;";
                        return 'OPEN';
                    case TB_PARTIALLY_INVOICED :
                        metaData.style += "background-color: #FCFC98;";
                        return 'PARTIALLY INVOICED';
                    case TB_INVOICED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'INVOICED';
                    case TB_CLOSED :
                        metaData.style += "background-color: #AAD4FF;";
                        return 'CLOSED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['myId','displayText'],
                    data: [['all', 'ALL'], [-1, 'DRAFT'], [0, 'OPEN'], [3, 'PARTIALLY INVOICED'], [1, 'INVOICED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Editable',
            sortable: true,
            resizable: true,
            dataIndex: 'lock_edit',
            width: 30,
            renderer : function(value, metaData, record, rowIndex){
                value && (metaData.css += ' silk13-lock ');
                return '';
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'UNLOCKED'], [1, 'LOCKED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        
        this.store = jun.rztTerimaBarang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    ref: '../botbar',
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'View',
                    ref: '../btnEdit'
                },
                { xtype: 'tbseparator' },
                {
                    xtype: 'button',
                    text: 'Unlock Edit',
                    ref: '../btnUnlock',
                    iconCls: 'silk13-lock_open'
                },
                { xtype: 'tbseparator' },
                {
                    xtype: 'button',
                    text: 'Input Invoice',
                    ref: '../btnInputInvoice'
                },
                {
                    xtype: "form",
                    frame: !1,
                    border: !1,
                    ref: '../formPrintInvoice',
                    items: [
                        {
                            xtype: "hidden",
                            name: "transfer_item_id",
                            ref: "../../transfer_item_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel"
                        }
                    ]
                }
            ]
        };
        
        jun.TerimaBarangGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnUnlock.on('Click', this.unlockEdit, this);
        this.btnInputInvoice.on('Click', this.inputInvoice, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        
        this.store.baseParams = {
            mode: "grid",
            viewForPurchasing: 1
        };
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        
        this.btnInputInvoice.setDisabled((r.get('status') == TB_INVOICED || r.get('status') == TB_CLOSED));
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Terima Barang");
            return;
        }
         
        jun.rztPoInReceiveCmp.baseParams = {};
        jun.rztPoInReceiveCmp.load({
            params: { po_id: this.record.data.po_id },
            callback: function(r){
              /*loadEditForm*/
                var ttl = "";
                switch(this.record.get('status')){
                    case TB_DRAFT : ttl = "DRAFT"; break;
                    case TB_OPEN : ttl = "OPEN"; break;
                    case TB_INVOICED : ttl = "INVOICED"; break;
                    case TB_CLOSED : ttl = "CLOSED"; break;
                }
                var idz = selectedz.json.terima_barang_id;
                
                var form = new jun.TerimaBarangWin({
                    modez: 2,
                    id: idz,
                    title: "View Penerimaan Barang  ["+ttl+"]",
                    iconCls: 'silk13-eye'
                });
                form.show(this);
                form.formz.getForm().loadRecord(this.record);
              /*End of - loadEditForm*/
            },
            scope: this
        });
        
        jun.rztTerimaBarangDetails.baseParams = { terima_barang_id: selectedz.json.terima_barang_id };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    inputInvoice: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Penerimaan Barang");
            return;
        }
        if (this.record.get('status') != TB_OPEN && this.record.get('status') != TB_PARTIALLY_INVOICED){
            Ext.MessageBox.alert("Warning", "Data Invoice untuk Penerimaan Barang ini telah diinput.");
            return;
        }
        var dataPO = new jun.PoInstore();
        dataPO.load({
            params: { po_id: this.record.data.po_id },
            callback: function(r){
              /*inputInvoice*/
                var idz = selectedz.json.terima_barang_id;
                
                var form = new jun.TransferItemWin({
                    modez: 0,
                    terima_barang_id: idz,
                    title: "Input Invoice",
                    parent: this,
                    storeGridDetil: new jun.TransferItemDetailsstore({url: 'TerimaBarangDetails/ItemDetilInvoice'})
                });
                form.show(this);
                
                form.tgl.setValue(DATE_NOW);
                form.disc.setValue(r[0].get('disc'));
                form.supplier.setValue(r[0].get('supplier_id'));
                //form.store.setValue(selectedz.json.store);
                
                /*hitung jatuh tempo*/
                var tgl_jthtmpo = new Date(selectedz.json.tgl);
                tgl_jthtmpo.setDate(tgl_jthtmpo.getDate() + parseInt(r[0].get('termofpayment')));
                form.tgl_jatuh_tempo.setValue(tgl_jthtmpo);
                
                form.griddetils.store.load({
                    params: {terima_barang_id: idz},
                    callback: function(){
                        this.calculateItem();
                    },
                    scope: form.griddetils.store
                });
                
              /*End of - inputInvoice*/
            },
            scope: this
        }); 
    },
    unlockEdit: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (this.record.get('status') != TB_OPEN) {
            Ext.MessageBox.alert("Warning", "Tidak dapat membuka Lock Edit.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin meng-unlock Penerimaan Barang <b>'+record.json.doc_ref+'</b>?', this.unlockEditYes, this);
    },
    unlockEditYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'TerimaBarang/UnlockEdit/id/' + record.json.terima_barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTerimaBarang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printInvoice: function(transfer_item_id){
        var form = this.formPrintInvoice.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintSupplierInvoice";
        this.transfer_item_id.setValue(transfer_item_id);
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
