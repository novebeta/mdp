jun.WorkOrderstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.WorkOrderstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WorkOrderStoreId',
            url: 'WorkOrder',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'tgl_so'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'sub_total'},
                {name: 'totalpot'},
                {name: 'total_vat'},
                {name: 'total'},
                {name: 'mobil_id'},
                {name: 'store_kode'},
                {name: 'tgl_finish'},
                {name: 'tgl_wo'},
                {name: 'upload'},
                {name: 'edited'},
                {name: 'invoice_id'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'nama_tipe'},
                {name: 'nama_merk'},
                {name: 'nama_customer'},
                {name: 'nama_barang'},
                {name: 'harga'},
                {name: 'discrp'},
                {name: 'tot_after_disc'},
                {name: 'age'},
                {name:'nama_kategori'}
            ]
        }, cfg));
    }
});
jun.rztWorkOrder = new jun.WorkOrderstore();
jun.rztWorkOrderAll = new jun.WorkOrderstore({baseParams: {store: "all", mode: "grid"}});
//jun.rztWorkOrder.load();
