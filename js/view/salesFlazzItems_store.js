jun.SalesFlazzItemsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesFlazzItemsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesFlazzItemsStoreId',
            url: 'SalesFlazzItems',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_flazz_item_id'},
                {name: 'note'},
                {name: 'total', type: 'float'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto', type: 'float'},
                {name: 'vat'},
                {name: 'up'},
                {name: 'total_pot'},
                {name: 'total_discrp1'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'harga', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'harga_beli', type: 'float'},
                {name: 'sales_flazz_id'},
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
        var harga_beli = this.sum("harga_beli");
        var harga = this.sum("harga");
        var vatrp = this.sum("vatrp");
        var bruto = this.sum("bruto");
        var total = this.sum("total");
        Ext.getCmp("harga_beliid").setValue(round(harga_beli,2));
        Ext.getCmp("hargajualid").setValue(round(harga,2));
        Ext.getCmp("brutoid").setValue(round(bruto,2));
        Ext.getCmp("vatrpid").setValue(round(vatrp,2));
        Ext.getCmp("totalid").setValue(round(total,2));
    }
});
jun.rztSalesFlazzItems = new jun.SalesFlazzItemsstore();
//jun.rztSalesFlazzItems.load();
