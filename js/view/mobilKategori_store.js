jun.MobilKategoristore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilKategoristore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilKategoriStoreId',
            url: 'MobilKategori',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_kategori_id'},
                {name: 'nama'},
                {name: 'mobil_grup_id'},
            ]
        }, cfg));
    }
});
jun.rztMobilKategori = new jun.MobilKategoristore();
jun.rztMobilKategoriCmp = new jun.MobilKategoristore();
jun.rztMobilKategoriLib = new jun.MobilKategoristore();
jun.rztMobilKategoriLib.load();
