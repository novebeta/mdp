jun.BonusJualstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BonusJualstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BonusJualStoreId',
            url: 'BonusJual',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bonus_jual_id'},
                {name: 'barang_id'},
                {name: 'bonus_name_id'},
                {name: 'persen_bonus'},
                {name: 'store'},
                {name: 'up'},
                {name: 'kode_barang'},
                {name: 'persen_bonus'},
                {name: 'bonus_name'}
            ]
        }, cfg));
    }
});
jun.rztBonusJual = new jun.BonusJualstore({ baseParams: { header: 1 }});
jun.rztBonusJualDetail = new jun.BonusJualstore();
//jun.rztBonusJualDetailCmp = new jun.BonusJualstore();
//jun.rztBonusJual.load();
