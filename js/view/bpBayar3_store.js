jun.BpBayar3store = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayar3store.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayar3StoreId',
            url: 'BpBayar3',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_bayar3_id'},
                {name: 'tgl'},
                {name: 'total', type: "float"},
                {name: 'bank_id'},
                {name: 'id_user'},
                {name: 'ref'},
                {name: 'body_paint_id'},
            ]
        }, cfg));
        this.on('load', this.refreshData, this);
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total = this.sum("total");
        Ext.getCmp("totalid").setValue(total);
    }
});
jun.rztBpBayar3 = new jun.BpBayar3store();
//jun.rztBpBayar3.load();
