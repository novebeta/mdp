jun.BarangFlazzstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangFlazzstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangFlazzStoreId',
            url: 'BarangFlazz',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'grup_id'},
                {name: 'active'},
                {name: 'sat'},
                {name: 'ppn'},
                {name: 'tipe_barang_id'},
                {name: 'barcode'},
                {name:'persediaan'},
                {name:'hpp'},
                {name:'sales'},
            ]
        }, cfg));
    }
});
jun.rztBarangFlazz = new jun.BarangFlazzstore();
jun.rztBarangFlazzAll = new jun.BarangFlazzstore();
jun.rztBarangFlazzCmp = new jun.BarangFlazzstore( {
    baseParams: {mode: "nonjasa", f: "cmp", tipe_barang_id: 0},
    method: 'POST'
});
jun.rztBarangFlazzLib = new jun.BarangFlazzstore();
jun.rztBarangFlazzLib.load();
jun.rztBarangFlazzCmp.load();
jun.rztBarangFlazzAll.load();
