jun.MobilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Mobil",
    id: 'docs-jun.MobilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tipe',
            sortable: true,
            resizable: true,
            dataIndex: 'mobil_tipe_id',
            width: 100,
            renderer: jun.renderMobilTipe
        },
        {
            header: 'Tahun Pembuatan',
            sortable: true,
            resizable: true,
            dataIndex: 'tahun_pembuatan',
            width: 100
        },
        {
            header: 'Mulai STNK',
            sortable: true,
            resizable: true,
            dataIndex: 'mulai_stnk',
            width: 100
        },
        {
            header: 'No. Rangka',
            sortable: true,
            resizable: true,
            dataIndex: 'no_rangka',
            width: 100
        },
        {
            header: 'Nopol',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
            width: 100
        },
        /*
{
header:'customer_id',
sortable:true,
resizable:true,
dataIndex:'customer_id',
width:100
},
        */
    ],
    initComponent: function () {
        this.store = jun.rztMobil;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Mobil',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Mobil',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete Mobil',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Refresh Mobil',
                    ref: '../btnRefresh'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.MobilGrid.superclass.initComponent.call(this);
        jun.rztMobilTipeCmp.load();
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnRefresh.on('Click', function () {
            this.store.reload();
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();

        jun.rztCustomersSalesCmp.load({
            params: {
                customer_id: this.record.data.customer_id
            }
        });
    },
    loadForm: function () {
        var form = new jun.MobilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.mobil_id;
        var form = new jun.MobilWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Mobil/delete/id/' + record.json.mobil_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztMobil.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
