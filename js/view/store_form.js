jun.StoreWin = Ext.extend(Ext.Window, {
    title: 'Reseller',
    modez: 1,
    width: 400,
    height: 380,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Store',
                fileUpload: true,
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode',
                        hideLabel: false,
                        //hidden:true,
                        name: 'store_kode',
                        id: 'store_kodeid',
                        ref: '../store_kode',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_store',
                        id: 'nama_storeid',
                        ref: '../nama_store',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Pemilik',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_pemilik',
                        id: 'nama_pemilikid',
                        ref: '../nama_pemilik',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Admin',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_admin',
                        id: 'nama_adminid',
                        ref: '../nama_admin',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NPWP',
                        hideLabel: false,
                        //hidden:true,
                        name: 'npwp',
                        id: 'npwpid',
                        ref: '../npwp',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Telp',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_telp',
                        id: 'no_telpid',
                        ref: '../no_telp',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: "fileuploadfield",
                        hideLabel: !1,
                        fieldLabel: "Logo",
                        emptyText: 'Select an file import (*.jpeg)',
                        ref: "../logo",
                        name: "logo",
                        anchor: "100%"
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Alamat',
                        hideLabel: false,
                        //hidden:true,
                        name: 'alamat',
                        id: 'alamatid',
                        ref: '../alamat',
                        // enableKeyEvents: true,
                        // style: {textTransform: "uppercase"},
                        // listeners: {
                        //     change: function (field, newValue, oldValue) {
                        //         field.setValue(newValue.toUpperCase());
                        //     }
                        // },
                        anchor: '100%'
                        //allowBlank:
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kota',
                        hideLabel: false,
                        //hidden:true,
                        name: 'city',
                        id: 'cityid',
                        ref: '../city',
                        // enableKeyEvents: true,
                        // style: {textTransform: "uppercase"},
                        // listeners: {
                        //     change: function (field, newValue, oldValue) {
                        //         field.setValue(newValue.toUpperCase());
                        //     }
                        // },
                        anchor: '100%'
                        //allowBlank:
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Tgl Maks Tagihan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tgl_jt_tagihan',
                        id: 'tgl_jt_tagihanid',
                        ref: '../tgl_jt_tagihan',
                        value: 1,
                        minValue: 1,
                        maxValue: 28,
                        width: 75
                    },
                    {
                        xtype: 'combo',
                        lazyRender: true,
                        // style: 'margin-bottom:2px',
                        mode: 'local',
                        enableKeyEvents: true,
                        forceSelection: true,
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterCmp,
                        ref: '../../account_code',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.StoreWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Store/update/id/' + this.id;
        } else {
            urlz = 'Store/create/';
        }
        Ext.getCmp('form-Store').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztStore.reload();
                // var response = Ext.decode(a.response.responseText);
                // Ext.MessageBox.show({
                //     title: 'Info',
                //     msg: response.msg,
                //     buttons: Ext.MessageBox.OK,
                //     icon: Ext.MessageBox.INFO
                // });
                // if (this.modez == 0) {
                //     Ext.getCmp('form-Store').getForm().reset();
                //     this.btnDisabled(false);
                // }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                // switch (a.failureType) {
                //     case Ext.form.Action.CLIENT_INVALID:
                //         Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                //         break;
                //     case Ext.form.Action.CONNECT_FAILURE:
                //         Ext.Msg.alert('Failure', 'Ajax communication failed');
                //         break;
                //     case Ext.form.Action.SERVER_INVALID:
                //         Ext.Msg.alert('Failure', a.result.msg);
                // }
                jun.rztStore.reload();
                this.close();
                // this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});