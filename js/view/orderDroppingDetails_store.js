jun.OrderDroppingDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.OrderDroppingDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'OrderDroppingDetailsStoreId',
            url: 'OrderDroppingDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'order_dropping_detail_id'},
                {name: 'qty'},
                {name: 'barang_id'},
                {name: 'visible'},
                {name: 'order_dropping_id'},
                {name: 'sat'},
            ]
        }, cfg));
    }
});
jun.rztOrderDroppingDetails = new jun.OrderDroppingDetailsstore();
//jun.rztOrderDroppingDetails.load();
