jun.bonusNamestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.bonusNamestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'bonusNameStoreId',
            url: 'BonusName',
            root: 'results',
            totalProperty: 'total',
            // autoLoad: METHODE_BONUS == 1 ? true : false,
            fields: [
                {name: 'bonus_name_id'},
                {name: 'bonus_name'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztBonusName = new jun.bonusNamestore();
jun.rztBonusNameLib = new jun.bonusNamestore();
jun.rztBonusNameCmp = new jun.bonusNamestore();
//jun.rztInfo.load();
