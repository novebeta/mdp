jun.PoInGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Order",
    id: 'docs-jun.PoInGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. PR',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_pr',
            width: 100
        },
        {
            header: 'Nama Tujuan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Divisi',
            sortable: true,
            resizable: true,
            dataIndex: 'divisi',
            width: 100
        }
        //{
        //    header: 'Status',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'status',
        //    width: 100,
        //    renderer: function (a, b, c) {
        //        switch (a) {
        //            case '0':
        //                return 'OPEN';
        //                break;
        //            case '1':
        //                return 'RECEVIED';
        //                break;
        //            case '2':
        //                return 'INVOICED';
        //                break;
        //            default:
        //                return '';
        //                break;
        //        }
        //    }
        //}
        /*
         {
         header:'ship_to_address',
         sortable:true,
         resizable:true,
         dataIndex:'ship_to_address',
         width:100
         },
         {
         header:'note',
         sortable:true,
         resizable:true,
         dataIndex:'note',
         width:100
         },
         {
         header:'total',
         sortable:true,
         resizable:true,
         dataIndex:'total',
         width:100
         },
         {
         header:'pr_id',
         sortable:true,
         resizable:true,
         dataIndex:'pr_id',
         width:100
         },
         {
         header:'supplier_id',
         sortable:true,
         resizable:true,
         dataIndex:'supplier_id',
         width:100
         },
         {
         header:'status',
         sortable:true,
         resizable:true,
         dataIndex:'status',
         width:100
         },
         {
         header:'tgl_tempo',
         sortable:true,
         resizable:true,
         dataIndex:'tgl_tempo',
         width:100
         },
         {
         header:'tgl_delivery',
         sortable:true,
         resizable:true,
         dataIndex:'tgl_delivery',
         width:100
         },
         {
         header:'termofpayment',
         sortable:true,
         resizable:true,
         dataIndex:'termofpayment',
         width:100
         },
         {
         header:'sub_total',
         sortable:true,
         resizable:true,
         dataIndex:'sub_total',
         width:100
         },
         {
         header:'total_disc_rp',
         sortable:true,
         resizable:true,
         dataIndex:'total_disc_rp',
         width:100
         },
         {
         header:'tax_rp',
         sortable:true,
         resizable:true,
         dataIndex:'tax_rp',
         width:100
         },
         {
         header:'ship_to_company',
         sortable:true,
         resizable:true,
         dataIndex:'ship_to_company',
         width:100
         },
         {
         header:'ship_to_city',
         sortable:true,
         resizable:true,
         dataIndex:'ship_to_city',
         width:100
         },
         {
         header:'ship_to_country',
         sortable:true,
         resizable:true,
         dataIndex:'ship_to_country',
         width:100
         },
         {
         header:'ship_to_phone',
         sortable:true,
         resizable:true,
         dataIndex:'ship_to_phone',
         width:100
         },
         {
         header:'ship_to_tax',
         sortable:true,
         resizable:true,
         dataIndex:'ship_to_tax',
         width:100
         },
         */
    ],
    initComponent: function () {
        //jun.rztPoIn.on({
        //    scope: this,
        //    beforeload: {
        //        fn: function (a, b) {
        //            b.params.tgl = Ext.getCmp('tglpoingridid').getValue();
        //            b.params.mode = "grid";
        //        }
        //    }
        //});
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.store = jun.rztPoIn;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create PO',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit PO',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View PO',
                    ref: '../btnView'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print PO',
                    ref: '../btnPrint'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportPurchaseOrder",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "po_id",
                            ref: "../../po_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "phpexcel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.PoInGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnView.on('Click', this.loadViewForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.btnPrint.on('Click', this.printPO, this);
        //this.btnAddTB.on('Click', this.addTB, this);
        //this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.reload();
    },
    printPO: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Delivery");
            return;
        }
        Ext.getCmp("form-ReportPurchaseOrder").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchaseOrder").getForm().url = "Report/PrintPurchaseOrder";
        this.po_id.setValue(selectedz.json.po_id);
        var form = Ext.getCmp('form-ReportPurchaseOrder').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        jun.rztBarangCmp.reload();
        var form = new jun.PoInWin({modez: 0});
        form.show();
    },
    loadViewForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.po_id;
        var form = new jun.PoInWin({modez: 2, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPoInDetails.baseParams = {
            po_id: idz
        };
        jun.rztPoInDetails.load();
        jun.rztPoInDetails.baseParams = {};
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.po_id;
        var form = new jun.PoInWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPoInDetails.baseParams = {
            po_id: idz
        };
        jun.rztPoInDetails.load();
        jun.rztPoInDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin Cancel PO ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PoIn/delete/id/' + record.json.po_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPoIn.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
