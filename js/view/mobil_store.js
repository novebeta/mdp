jun.Mobilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Mobilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilStoreId',
            url: 'Mobil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_id'},
                {name: 'mobil_tipe_id'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'customer_id'},
                {name: 'tahun_km'},
                {name: 'warna'}
            ]
        }, cfg));
    }
});
jun.rztMobil = new jun.Mobilstore();
jun.rztMobilCmp = new jun.Mobilstore();
jun.rztMobilSalesCmp = new jun.Mobilstore();
jun.rztMobilLib = new jun.Mobilstore();
jun.rztMobilLib.load();
