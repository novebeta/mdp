jun.BonusJualWin = Ext.extend(Ext.Window, {
    title: 'Bonus Jual',
    modez: 1,
    width: 600,
    height: 430,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BonusJual',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kode Barang',
                        store: jun.rztBarangBonusJual,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'kode_barang',
                        ref: '../barang',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        readOnly: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: jun.rztStoreCmp,
                        value:STORE,
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        anchor: '100%'
                    },
                    new jun.BonusJualDetailsGrid({
                        anchor: '100%',
                        height: 280,
                        frameHeader: !1,
                        header: !1,
                        layout: 'form',
                        ref: "../griddetils"
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BonusJualWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);        
        this.barang.on('select', this.barangOnSelect, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onWinClose: function () { 
        this.griddetils.store.removeAll();
    },
    barangOnSelect: function (combo, record, index) {
        var param = combo.getValue();
        this.loadStoreDetails(param);
    },
    loadStoreDetails: function (param) {
        this.griddetils.store.removeAll();
        this.griddetils.bonus_name_id.setValue('');
        this.griddetils.bonus.setValue('');
        Ext.Ajax.request({
            url: 'BonusJual/getBonus',
            method: 'POST',
            params:{
                barang_id : param
            },
            success: function (f, a) {
                jun.rztBonusJual.reload();
                var response = Ext.decode(f.responseText);
                if(response.status == 'false'){
//                    Ext.MessageBox.show({
//                        title: 'Info',
//                        msg: response.msg,
//                        buttons: Ext.MessageBox.OK,
//                        icon: Ext.MessageBox.INFO
//                    });
                } else {
                    for(var i = 0;i < response.count; i++){
                        var c = jun.rztBonusJualDetail.recordType,
                            d = new c({
                                bonus_name_id: response.tempbn[i],
                                persen_bonus: response.temppb[i]
                            });
                        jun.rztBonusJualDetail.add(d);
                    }
                }
                
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });     
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        Ext.getCmp('form-BonusJual').getForm().submit({
            url: 'BonusJual/create/',
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data"))
            },
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBonusJual.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BonusJual').getForm().reset();
                    this.griddetils.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});


jun.BonusJualDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.BonusJualDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'B',
            sortable: true,
            resizable: true,
            dataIndex: 'bonus_name_id',
            width: 10
        },
        {
            header: 'Bonus Name',
            sortable: true,
            resizable: true,
            dataIndex: 'bonus_name_id',
            width: 100,
            renderer: jun.renderBonusName
        },
        {
            header: 'Persen Bonus (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'persen_bonus',
            width: 30
        }
//        {
//            header: 'Persen Bonus',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'persen_bonus',
//            width: 30,
//            align: "right",
//            renderer: Ext.util.Format.numberRenderer("0,0.00")
//        }
    ],
    initComponent: function () {
        this.store = jun.rztBonusJualDetail;
       
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small',
                        minWidth: 40
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Nama Bonus :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBonusNameCmp,
                            valueField: 'bonus_name_id',
                            ref: '../../bonus_name_id',
                            displayField: 'bonus_name'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Bonus (%) :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../bonus',
                            width: 50,
                            value: 0,
                            minValue: 0
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        xtype: 'button',
                        scale: 'small'
                    },
                    items: [
                        {
                            text: 'add/update',
                            ref: '../../btnAdd'
                        },
                        {
                            text: 'add all',
                            ref: '../../btnAddAll'
                        }
                    ]
                }
            ]
        };
        jun.BonusJualDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnAddAll.on('Click', this.loadFormAll, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.bonus_name_id.on('select', this.bonus_name_idOnSelect, this);
        this.on('rowdblclick', this.loadEditForm, this);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Requests");
            return;
        }
//        console.log(selectedz);
        var idz = selectedz.data.bonus_name_id;
        var bonus = selectedz.data.persen_bonus;
//        console.log(idz);
        this.bonus_name_id.setValue(idz);
        this.bonus.setValue(bonus);
        
    },
    bonus_name_idOnSelect: function (c, r, i) {
        var a = this.store.findExact("bonus_name_id", c.getValue());
            if (a > -1) {
                var b = this.store.getAt(a);
                this.bonus.setValue(b.get('persen_bonus'));
                return;
            } else {
                return;
            }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var bonus_name_id = this.bonus_name_id.getValue();        
        if (bonus_name_id == "") {
            Ext.MessageBox.alert("Error", "No item is selected.");
            return;
        }
        if (this.bonus.getValue() == "") {
            Ext.MessageBox.alert("Error", "Persen Bonus cannot be null.");
            return;
        }
        
        var a = this.store.findExact("bonus_name_id", bonus_name_id);
        if (a > -1) {
            //if exists then update
            var r = this.store.getAt(a);
//            console.log(r);
            r.set('persen_bonus', this.bonus.getValue());
            r.commit();
        }else{
            //create
            var c = this.store.recordType,
                d = new c({
                    bonus_name_id: bonus_name_id,
                    persen_bonus: this.bonus.getValue()
                });
            this.store.add(d);
        }
        this.bonus_name_id.setValue('');
        this.bonus.setValue('');
    },
    loadFormAll: function () {
        for (var i = 0; i < jun.rztBonusNameCmp.getCount(); i++) {
            var r = jun.rztBonusNameCmp.getAt(i);
            var a = this.store.findExact("bonus_name_id", r.get('bonus_name_id'));
            if (a > -1) {
                continue;
            } else {
                var c = this.store.recordType,
                    d = new c({
                        bonus_name_id: r.get('bonus_name_id'),
                        persen_bonus: 0
                    });
                this.store.add(d);
            }
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Delete item', 'Do you want to delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Error", "No item is selected.");
            return;
        }
        this.store.remove(record);
    }
});