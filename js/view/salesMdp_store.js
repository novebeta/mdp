jun.SalesMdpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesMdpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesMdpStoreId',
            url: 'SalesMdp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'tgl_so'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'sub_total', type: 'float'},
                {name: 'totalpot', type: 'float'},
                {name: 'total_vat', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'mobil_id'},
                {name: 'store_kode'},
                {name: 'tgl_finish'},
                {name: 'tgl_wo'},
                {name: 'upload'},
                {name: 'edited'},
                {name: 'invoice_id'},
                {name: 'no_woug'},
                {name: 'tgl_woug'},
                {name: 'mobil_km', type: 'float'},
                {name: 'woug_persen', type: 'float'},
                {name: 'woug_total', type: 'float'},
                {name: 'wo_ppn', type: 'float'},
                {name: 'wo_hutang', type: 'float'},
                {name: 'invoice_ug_id'}
            ]
        }, cfg));
    }
});
//jun.rztSalesMdp.load();
