jun.BarangAttrstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.BarangAttrstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangAttrStoreId',
            url: 'BarangAttr',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'barang_attr_id'},
{name:'customer_id'},
{name:'barang_id'},
{name:'account_code'},
                
            ]
        }, cfg));
    }
});
jun.rztBarangAttr = new jun.BarangAttrstore();
//jun.rztBarangAttr.load();
