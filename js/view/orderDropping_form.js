jun.OrderDroppingWin = Ext.extend(Ext.Window, {
    title: 'Transfer Requests',
    modez: 1,
    width: 900,
    height: 420,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                id: 'form-OrderDropping',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        readOnly: true,
                        maxLength: 50,
                        width: 200,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        value: DATE_NOW,
                        readOnly: !EDIT_TGL,
                        width: 200,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        forceSelection: true,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        ref: '../store',
                        width: 200,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Pengirim:",
                        x: 340,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        forceSelection: true,
                        hiddenName: 'store_pengirim',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        allowBlank: false,
                        readOnly: !HEADOFFICE,
                        ref: '../store_pengirim',
                        width: 200,
                        x: 420,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 340,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        name: 'note',
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        maxLength: 600,
                        width: 300,
                        height: 50,
                        x: 420,
                        y: 32
                    },
                    new jun.OrderDroppingDetailsGrid({
                        x: 5,
                        y: 95,
                        anchor: '100% 100%',
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        readOnly: ((this.modez < 2)?false:true)
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Approve',
                    ref: '../btnApprove',
                    hidden: true,
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.OrderDroppingWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnApprove.on('click', this.onbtnApproveclick, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
        
        this.btnCancel.setText(this.modez < 2? 'Cancel':'Close');
        switch(this.modez) {
            case 0:
                this.btnSave.setVisible(true);
                this.btnSaveClose.setVisible(true);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            default :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(false);
                this.formz.getForm().applyToFields({ readOnly: true });
                break;
        }
        
        if(this.approval){
            this.btnApprove.setVisible(true);
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);

        var urlz = 'OrderDropping/create/';
        this.formz.getForm().submit({
            url: urlz,
            //timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztOrderDropping.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.griddetils.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnApproveclick: function () {
        this.formz.getForm().submit({
            url: 'OrderDropping/Approve/',
            //timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                jun.rztDroppingApproval.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {

        var fstore = this.store.getValue();
        var tstore = this.store_pengirim.getValue();


        if(fstore == tstore )
        {
            Ext.MessageBox.alert("Warning", "Pengirim tidak boleh sama.");
            return;
        }

        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {

        var fstore = this.store.getValue();
        var tstore = this.store_pengirim.getValue();


        if(fstore == tstore )
        {
            Ext.MessageBox.alert("Warning", "Pengirim tidak boleh sama.");
            return;
        }

        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});