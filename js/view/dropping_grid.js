jun.DroppingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer",
    id: 'docs-jun.DroppingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Pengirim',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Penerima',
            sortable: true,
            resizable: true,
            dataIndex: 'store_penerima',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case PR_DRAFT :
                        return 'DRAFT';
                    case PR_NEED_SHIPMENT :
                        metaData.style += "background-color: #FFD4D4;";
                        return 'NEED SHIPMENT';
                    case PR_OPEN :
                        return 'OPEN';
                    case PR_PROCESS :
                        metaData.style += "background-color: #FCFC98;";
                        return 'PROCESS';
                    case PR_CLOSED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [-2, 'NEED SHIPMENT'], [0, 'OPEN'], [1, 'PROCESS'], [2, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Approved',
            sortable: true,
            resizable: true,
            dataIndex: 'approved',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                value && (metaData.css += ' silk13-tick ');
                return '';
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEEDS APPROVAL'], [1, 'APPROVED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Up',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case DR_PENDING :
                        metaData.style += "background-color: #FF1A1A;";
                        return 'PENDING';
                    case DR_SEND :
                        metaData.style += "background-color: #FFFF33;";
                        return 'SEND';
                    case DR_APPROVE :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'APPROVE';
                    case DR_PROCESS :
                        metaData.style += "background-color: #00FF00;";
                        return 'PROCESS';
                    case DR_RECEIVE :
                        metaData.style += "background-color: #FF4D88;";
                        return 'RECEIVE';
                    case DR_CLOSE :
                        metaData.style += "background-color: #ADAD85;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'PENDING'], [1, 'SEND'], [2, 'APPROVE'], [3, 'PROCESS'], [4, 'RECEIVE'], [5, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if(jun.rztStoreCmp.getTotalCount()==0) jun.rztStoreCmp.load();
        if(jun.rztBarangNonJasaAll.getTotalCount()==0) jun.rztBarangNonJasaAll.load();
        if(jun.rztBarangLib.getTotalCount()==0) jun.rztBarangLib.load();
        
        this.store = new jun.Droppingstore({
            baseParams: {mode: "grid"}
        });
        this.store.reload();

        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit',
                    iconCls: 'silk13-pencil'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Shipping',
                    ref: '../btnOpen',
                    iconCls: 'silk13-flag_green'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Resend',
                    ref: '../btnResend',
                    iconCls: 'silk13-arrow_refresh'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Print Tanda Terima',
                    ref: '../btnPrintTandaTerima',
                    iconCls: 'silk13-page_white_excel'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'History',
                    ref: '../btnHistory',
                    iconCls: 'silk13-clock'
                },
                {
                    xtype: "form",
                    frame: !1,
                    border: !1,
                    ref: '../formz',
                    items: [
                        {
                            xtype: "hidden",
                            name: "dropping_id",
                            ref: "../../dropping_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };

        jun.DroppingGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnOpen.on('Click', this.confirmOpen, this);
        this.btnResend.on('Click', this.loadResend, this);
        this.btnHistory.on('Click', this.loadHistory, this);
        this.btnPrintTandaTerima.on('Click', this.PrintTandaTerima, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        
        var filterEdit = r.get('store') == STORE && (r.get('lunas') == PR_NEED_SHIPMENT || r.get('lunas') == PR_OPEN);
        this.btnEdit.setText(filterEdit ?"Edit":"Show");
        this.btnEdit.setIconClass(filterEdit ?'silk13-pencil':'silk13-eye');

        this.btnOpen.setDisabled(!(r.get('store') == STORE && r.get('lunas') == PR_NEED_SHIPMENT && r.get('approved')==1 ));
        this.btnPrintTandaTerima.setDisabled(!(r.get('store') == STORE && r.get('lunas') == PR_OPEN));
    },
    loadForm: function () {
        var form = new jun.DroppingWin({
            modez: 0,
            title: 'Create '+this.title
        });
        form.show();
    },
    confirmOpen: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer.");
            return;
        }
        if (this.record.get('store') != STORE){
            Ext.MessageBox.alert("Warning", "Anda tidak diperbolehkan untuk OPEN data Transfer ini.");
            return;
        }
        if (this.record.get('lunas') != PR_NEED_SHIPMENT){
            Ext.MessageBox.alert("Warning", "Transfer telah di-OPEN sebelumnya.");
            return;
        }
        Ext.MessageBox.confirm('Open Transfer', 'Apakah anda yakin ingin open transfer ini?', this.openOrder, this);
    },
    openOrder: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            url: 'Dropping/Open',
            method: 'POST',
            params: {
                dropping_id: this.record.get('dropping_id')
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Open Transfer',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadHistory: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data "+this.title+".");
            return;
        }

        var idz = selectedz.json.order_dropping_id ? selectedz.json.order_dropping_id : selectedz.json.dropping_id;

        var form = new jun.DroppingHistoryWin({
            modez: 0,
            historyid: {order_dropping_id :idz},
            title: 'History Dropping'
        });
        form.show();
    },
    loadResend: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data "+this.title);
            return;
        }

        var idz = selectedz.json.dropping_id;
        var cab = selectedz.json.store_penerima;

        if(cab == STORE)
        {
            Ext.MessageBox.alert("Warning", "Anda sebagai penerima tidak dapat melakukan resend.");
            return;
        }

        Ext.Ajax.request({
            url: 'Dropping/Resend/',
            method: 'POST',
            params: {
                id: idz
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data "+this.title+".");
            return;
        }
        
        jun.rztOrderDroppingCmp.baseParams = {};
        jun.rztOrderDroppingCmp.load({
            params: {
                order_dropping_id: this.record.get('order_dropping_id')
            },
            callback: function(r){
                var filter = this.record.get('store') == STORE && (this.record.get('lunas') == PR_NEED_SHIPMENT || this.record.get('lunas') == PR_OPEN);
                var form = new jun.DroppingWin({
                    modez: (filter ? 1 : 2),
                    id: this.record.get('dropping_id'),
                    title: (filter ? "Edit" : "Show")+" Transfer"
                });
                form.show(this);
                form.formz.getForm().loadRecord(this.record);
                
                if(this.record.get('order_dropping_id')){
                    form.store_penerima.setReadOnly(true);
                }else{
                    form.order_dropping.setReadOnly(true);
                }
            },
            scope: this
        });
        
        jun.rztDroppingDetails.baseParams = { dropping_id: this.record.get('dropping_id') };
        jun.rztDroppingDetails.load();
        jun.rztDroppingDetails.baseParams = {};
    },
    PrintTandaTerima: function (){
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data "+this.title+".");
            return;
        }
        if (this.record.get('store') != STORE) {
            Ext.MessageBox.alert("Warning", "Anda tidak dapat print Tanda Terima data ini.");
            return;
        }
        this.dropping_id.setValue(this.record.get('dropping_id'));
        var form = this.formz.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintTandaTerimaDropping";
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
