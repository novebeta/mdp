jun.BpBayar3Grid = Ext.extend(Ext.grid.GridPanel, {
    title: "BpBayar3",
    id: 'docs-jun.BpBayar3Grid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kas/Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 100,
            renderer: jun.renderBank
        },
        {
            header: 'Jumlah',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
    ],
    initComponent: function () {
        this.store = jun.rztBpBayar3;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Kas/Bank :'
                },
                {
                    xtype: 'combo',
                    //typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    ref: '../bank',
                    store: jun.rztBankBp,
                    allowBlank: false,
                    hiddenName: 'bank_id',
                    valueField: 'bank_id',
                    displayField: 'nama_bank',
                    width: 175,
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Jumlah :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../jumlah',
                    width: 75,
                    value: 0,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.BpBayar3Grid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if (selectedz.json.bp_bayar3_id == null) {
            Ext.MessageBox.alert("Warning", "Pembayaran harus disimpan terlebih dahulu.");
            return;
        }
        var idz = selectedz.json.bp_bayar3_id;
        window.open('bodyPaint/printBuktiTerimaPribadi/id/' + idz, '_blank');
    },
    loadForm: function () {
        this.btnDisable(true);
        var bank_id = this.bank.getValue();
        var total = parseFloat(this.jumlah.getValue());
        if (!bank_id) {
            Ext.MessageBox.alert("Error", "Bank harus dipilih.");
            // this.btnDisable(false);
            return;
        }
        this.save({
            bank_id: bank_id,
            total: total,
        });
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.bank.setValue(record.data.bank_id);
            this.jumlah.setValue(record.data.total);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    save: function (data) {
        var invoice = parseFloat(Ext.getCmp('grand_totalid').getValue());
        var total = parseFloat(Ext.getCmp('totalid').getValue());
        if (invoice < (total + data.total)) {
            data.total = invoice - total;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('bank_id', data.bank_id);
            record.set('total', data.total);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c(data);
            this.store.add(d);
        }
        this.resetForm();
    },
    resetForm: function (s) {
        this.bank.reset();
        this.jumlah.reset();
        this.btnDisable(false);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
})
