jun.TransferBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangStoreId',
            url: 'TransferBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_barang_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'up'},
                {name: 'total_pot'},
                {name: 'total_discrp1'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'lunas'},
                {name: 'bank_id'},
                {name: 'supp_id'},
            ]
        }, cfg));
    }
});
jun.rztTransferBarang = new jun.TransferBarangstore({url: 'TransferBarang/IndexIn'});
jun.rztTransferBarangOut = new jun.TransferBarangstore({url: 'TransferBarang/IndexOut'});
//jun.rztReturnTransferBarang = new jun.TransferBarangstore({url: 'TransferBarang/IndexOut'});
//jun.rztTransferBarang.load();
