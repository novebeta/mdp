jun.RacikanDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.RacikanDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RacikanDetailsStoreId',
            url: 'RacikanDetails',           
            root: 'results',
//            autoLoad: true,
            totalProperty: 'total',
            fields: [                
                {name:'racikan_detail_id'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'satuan'},
                {name: 'racikan_id'}
                
            ]
        }, cfg));
    }
});
jun.rztRacikanDetails = new jun.RacikanDetailsstore();
//jun.rztRacikanDetails.load();
