jun.BpPartsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpPartsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpPartsStoreId',
            url: 'BpParts',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_parts_id'},
                {name: 'body_paint_id'},
                {name: 'nama'},
                {name: 'harga', type: "float"},
                {name: 'total_parts_line', type: "float"},
                {name: 'qty', type: "float"},
                {name: 'kode'},
                {name: 'hpp'},
                {name: 'posting'},
                {name: 'bp_bayar_parts_id'},
                {name: 'vatrp'},
                {name: 'ppnout'},
                {name: 'tipe'},
            ]
        }, cfg));
        this.on('load', this.refreshData, this);
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total_parts_line = this.sum("total_parts_line");
        var parts_ppn = (PENGALI_PPN * total_parts_line);
        Ext.getCmp("tot_parts_bp").setValue(total_parts_line);
        var tot_jasa = parseFloat(Ext.getCmp("total_jasaid").getValue());
        var jasa_ppn = jun.rztBpJasa.sum("ppn");
        var total = total_parts_line + tot_jasa;
        Ext.getCmp("total_partsid").setValue(total_parts_line);
        Ext.getCmp("totalid").setValue(total);
        var vatrp = jasa_ppn + parts_ppn;
        var grand = parseFloat(total) + parseFloat(vatrp);
        Ext.getCmp("vatrpid").setValue(vatrp);
        Ext.getCmp("grand_totalid").setValue(grand);
    }
});
jun.rztBpParts = new jun.BpPartsstore();
//jun.rztBpParts.load();
