jun.BarangViewstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangViewstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangViewStoreId',
            url: 'BarangView',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'nama_barang'},
                {name: 'mobil_kategori_id'},
                {name: 'harga'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'vat'}
            ]
        }, cfg));
    }
});
jun.rztBarangView = new jun.BarangViewstore();
//jun.rztBarangView.load();
