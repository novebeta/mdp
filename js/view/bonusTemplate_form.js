jun.BonusTemplateWin = Ext.extend(Ext.Window, {
    title: 'Bonus Template',
    modez: 1,
    width: 400,
    height: 180,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BonusTemplate',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Grup',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztGrup,
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
//                        name: 'grup_id',
                        id: 'grup_idid',
                        ref: '../grup_id',
                        displayField: 'nama_grup'
                    },
//                    {
//                        xtype: 'textfield',
//                        fieldLabel: 'grup_id',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'grup_id',
//                        id: 'grup_idid',
//                        ref: '../grup_id',
//                        maxLength: 36,
//                        //allowBlank: ,
//                        anchor: '100%'
//                    },
                    {
                        xtype: 'hidden',
                        name: 'nama_grup',
                        id: 'nama_grupid',
                        ref: '../nama_grup'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Bonus Name',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBonusName,
                        hiddenName: 'bonus_name_id',
                        valueField: 'bonus_name_id',
//                        name: 'grup_id',
                        id: 'bonus_name_idid',
                        ref: '../bonus_name_id',
                        displayField: 'bonus_name'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Bonus (%)',
                        hideLabel: false,
                        //hidden:true,
                        name: 'persen_bonus',
                        id: 'persen_bonusid',
                        ref: '../persen_bonus',
                        maxLength: 10,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BonusTemplateWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.grup_id.on('select', this.grupOnSelect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    grupOnSelect: function (combo, record, index) {
        var namagrup = combo.store.getAt(combo.store.findExact("grup_id", combo.getValue())).get('nama_grup');
        this.nama_grup.setValue(namagrup);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {

            urlz = 'BonusTemplate/update/id/' + this.id;

        } else {

            urlz = 'BonusTemplate/create/';
        }

        Ext.getCmp('form-BonusTemplate').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBonusTemplate.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BonusTemplate').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});