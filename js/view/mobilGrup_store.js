jun.MobilGrupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MobilGrupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MobilGrupStoreId',
            url: 'MobilGrup',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mobil_grup_id'},
                {name: 'nama'},
            ]
        }, cfg));
    }
});
jun.rztMobilGrup = new jun.MobilGrupstore();
jun.rztMobilGrupCmp = new jun.MobilGrupstore();
jun.rztMobilGrupLib = new jun.MobilGrupstore();
jun.rztMobilGrupLib.load();
