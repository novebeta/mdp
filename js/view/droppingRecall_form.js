jun.DroppingRecallWin = Ext.extend(Ext.Window, {
    title: 'Transfer Recall',
    modez: 1,
    width: 1100,
    height: 500,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = {
            xtype: 'form',
            frame: false,
            border: false,
            bodyStyle: 'background: none;',
            layout: {
                type: 'vbox',
                padding: '5',
                align: 'stretch'
            },
            ref: 'formz',
            items: [
                {
                    xtype: 'container',
                    layout:'column',
                    height: 90,
                    defaults: {
                        xtype: 'container',
                        layout: 'form',
                        labelWidth: 70,
                        labelAlign: 'left'
                    },
                    items:[
                        {
                            columnWidth: .33,
                            defaults:{
                                width: 175
                            },
                            items:[
                                {
                                    xtype: 'textfield',
                                    fieldLabel: "Doc. Ref",
                                    name: 'doc_ref',
                                    readOnly: true
                                },
                                {
                                    xtype: 'xdatefield',
                                    fieldLabel: "Tgl",
                                    name: 'tgl',
                                    ref:"../../../tgl",
                                    format: 'd M Y',
                                    value: DATE_NOW,
                                    readOnly: !EDIT_TGL,
                                    allowBlank: false
                                },
                                {
                                    xtype: 'combo',
                                    fieldLabel: "Pengirim",
                                    typeAhead: true,
                                    triggerAction: 'all',
                                    lazyRender: true,
                                    mode: 'local',
                                    store: jun.rztStoreCmp,
                                    forceSelection: true,
                                    hiddenName: 'store',
                                    name: 'store',
                                    valueField: 'store_kode',
                                    displayField: 'store_kode',
                                    emptyText: "Select Branch",
                                    value: STORE,
                                    readOnly: !HEADOFFICE,
                                    ref:"../../../store_pengirim",
                                    allowBlank: false
                                }
                            ]
                        },
                        {
                            columnWidth: .67,
                            defaults:{
                                width: 300
                            },
                            items:[
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'note',
                                    name: 'note',
                                    maxLength: 45,
                                    height: 50
                                }
                            ]
                        }
                    ]
                },
                {
                    ref: '../card1',
                    layout: 'card',
                    activeItem: 0,
                    flex: 1,
                    border: false,
                    items: [
                        new jun.DroppingRecallAllDetailsGrid({
                            frameHeader: !1,
                            header: !1,
                            ref:"../../gridAllDetail",
                            parent: this,
                            readOnly: this.modez > 1
                        }),
                        new jun.DroppingRecallDetailsGrid({
                            frameHeader: !1,
                            header: !1,
                            ref:"../../gridDetail",
                            parent: this,
                            readOnly: this.modez > 1
                        })
                    ]
                }
            ]
        };
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Approve',
                    ref: '../btnApprove',
                    hidden: true,
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.DroppingRecallWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.store_pengirim.on('select', this.onSelectStorePengirim, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
        
        switch(this.modez){
            case 0:
                this.btnVisibled(false);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                this.card1.activeItem = 1;
                break;
            default :
                this.btnVisibled(false);
                this.card1.activeItem = 1;
                this.formz.getForm().applyToFields({ readOnly: true });
                break;
        }
    },
    onSelectStorePengirim: function (c, r, i) {
        this.onWinClose();
        this.gridAllDetail.store.setBaseParam('store_pengirim', c.getValue());
        this.gridAllDetail.store.reload();
    },
    onWinClose: function () {
        this.gridAllDetail.store.removeAll();
        this.gridDetail.store.removeAll();
    },
    btnVisibled: function (v) {
        this.btnSave.setVisible(v);
        this.btnSaveClose.setVisible(v);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        this.formz.getForm().submit({
            url: 'DroppingRecall/create/',
            scope: this,
            params: {
                mode: this.modez,
                dropping_recall_id: this.dropping_recall_id,
                detil: Ext.encode(Ext.pluck(this.gridDetail.store.data.items, "data"))
            },
            success: function (f, a) {
                if(Ext.getCmp('docs-jun.DroppingRecallGrid') !== undefined) Ext.getCmp('docs-jun.DroppingRecallGrid').store.reload();

                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: this.title,
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});