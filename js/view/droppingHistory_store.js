jun.DroppingHistorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.DroppingHistorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DroppingHistoryStoreId',
            url: 'DroppingHistory',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id_history'},
                {name:'order_dropping_id'},
                {name:'user_id'},
                {name:'doc_ref'},
                {name:'tgl'},
                {name:'store'},
                {name:'store_pengirim'},
                {name:'status'},
                {name:'desc'},
                {name:'user'},
            ]
        }, cfg));
    }
});
jun.rztDroppingHistory = new jun.DroppingHistorystore();
//jun.rztDroppingHistory.load();
