jun.InvoiceMdpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembayaran",
    id: 'docs-jun.InvoiceMdpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Reseller',
            sortable: true,
            resizable: true,
            dataIndex: 'store_kode',
            width: 100
        },
        {
            header: 'Periode',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_periode',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Cetak',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_cetak',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_bayar',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'LUNAS'
                } else {
                    return 'BELUM BAYAR'
                }
            }
        }
    ],
    initComponent: function () {
        jun.rztInvoiceMdp.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var seller = Ext.getCmp('storeinvoicegridid').getValue();
                    b.params.store = seller;
                    var blmlunas = Ext.getCmp('belumlunasinvoicegridid').getValue();
                    b.params.blmlunas = blmlunas;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztInvoiceMdp;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-',
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            // fieldLabel: 'Branch',
                            // ref: '../store',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            id: 'storeinvoicegridid',
                            store: jun.rztStoreCmp,
                            forceSelection: true,
                            hiddenName: 'store',
                            width: 100,
                            // name: 'store',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            emptyText: "All Reseller",
                            value: STORE,
                            readOnly: (STORE !== ''),
                        },
                        '-',
                        {
                            xtype: 'checkbox',
                            boxLabel: "Belum Lunas Saja",
                            id: 'belumlunasinvoicegridid',
                            value: 0,
                            inputValue: 1,
                            uncheckedValue: 0
                        },
                        '-',
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Print Invoice',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Pembayaran Sudah Diterima',
                    hidden: (STORE !== ''),
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (STORE !== ''),
                },
                {
                    xtype: 'button',
                    text: 'Refresh Invoice',
                    ref: '../btnRefresh'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-PrintInvoiceMdp",
                    layout: "form",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "invoice_id",
                            ref: "../../invoice_id"
                        }
                    ]
                }
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.InvoiceMdpGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.deleteRec, this);
        this.btnRefresh.on('Click', function () {
            this.store.reload();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        jun.rztStoreCmp.reload();
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih invoice.");
            return;
        }
        this.invoice_id.setValue(selectedz.json.invoice_id);
        Ext.getCmp("form-PrintInvoiceMdp").getForm().url = 'InvoiceMdp/PrintInvoice';
        Ext.getCmp("form-PrintInvoiceMdp").getForm().standardSubmit = !0;
        var form = Ext.getCmp('form-PrintInvoiceMdp').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        jun.rztInvoiceMdp.reload();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.invoice_id;
        var form = new jun.InvoiceMdpWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz.json.tgl_bayar != null) {
            Ext.MessageBox.alert("Warning", "Invoice sudah dibayar.");
            return;
        }
        var form = new  jun.PembayaranMdp();
        form.show();
        // return;
    },
});
jun.PembayaranMdp = Ext.extend(Ext.Window, {
    title: "Pembayaran",
    // iconCls: "silk13-report",
    modez: 1,
    width: 350,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-PembayaranMdp",
                labelWidth: 75,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        anchor: '100%',
                        allowBlank: false,
                        value: DATE_NOW
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-html",
                    text: "Save",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Cancel",
                    // hidden: (STORE != ''),
                    ref: "../btnCancel"
                },
            ]
        };
        jun.PembayaranMdp.superclass.initComponent.call(this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCancelclick: function () {
        this.close();
    },
    onbtnSaveclick: function () {
        var grid = Ext.getCmp('docs-jun.InvoiceMdpGrid');
        var record = grid.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Invoice");
            return;
        }
        Ext.getCmp('form-PembayaranMdp').getForm().submit({
            url: 'InvoiceMdp/bayar/id/' + record.json.invoice_id,
            scope: this,
            success: function (f, a) {
                jun.rztInvoiceMdp.reload();
                // var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                // this.btnDisabled(false);
            }
        });
    }
});