jun.Wougstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Wougstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WougStoreId',
            url: 'Woug',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'no_woug'},
                {name: 'tgl_woug'},
                {name: 'woug_persen', type: "float"},
                {name: 'woug_total', type: "float"},
                {name: 'wo_ppn', type: "float"},
                {name: 'wo_hutang', type: "float"},
                {name: 'sales_id'},
                {name: 'mobil_km'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'warna'},
                {name: 'no_mesin'},
                {name: 'nama_customer'},
                {name: 'store_kode'},
                {name: 'invoice_ug_id'}
            ]
        }, cfg));
    }
});
jun.rztWoug = new jun.Wougstore();
jun.WougListstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Wougstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WougListStoreId',
            // url: 'Woug',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'no_woug'},
                {name: 'nama_customer'},
                {name: 'woug_total', type: "float" },
                {name: 'sales_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total = this.sum("woug_total");
        Ext.getCmp('totalwougid').setValue(total);
    }
});
jun.rztWougList = new jun.WougListstore();
//jun.rztWoug.load();
