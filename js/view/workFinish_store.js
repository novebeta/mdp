jun.WorkFinishstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.WorkFinishstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WorkFinishStoreId',
            url: 'WorkFinish',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'tgl_so'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'sub_total'},
                {name: 'totalpot'},
                {name: 'total_vat'},
                {name: 'total'},
                {name: 'mobil_id'},
                {name: 'store_kode'},
                {name: 'tgl_finish'},
                {name: 'tgl_wo'},
                {name: 'upload'},
                {name: 'edited'},
                {name: 'invoice_id'},
                {name: 'tahun_pembuatan'},
                {name: 'mulai_stnk'},
                {name: 'no_rangka'},
                {name: 'no_pol'},
                {name: 'nama_tipe'},
                {name: 'nama_merk'},
                {name: 'nama_customer'},
                {name: 'nama_barang'},
                {name: 'harga'},
                {name: 'discrp'},
                {name: 'tot_after_disc'},
                {name: 'age'},
                {name: 'nama_kategori'}
            ]
        }, cfg));
    }
});
jun.rztWorkFinish = new jun.WorkFinishstore();
//jun.rztWorkFinish.load();
