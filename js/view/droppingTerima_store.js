jun.DroppingTerimastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.DroppingTerimastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DroppingTerimaStoreId',
            url: 'Dropping',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'dropping_detail_id'},
                {name: 'terima_barang_details_id'},
                {name: 'qty_diterima'},
                {name: 'qty_order'},
                {name: 'qty'},
                {name: 'barang_id'},
                {name: 'visible'},
                {name: 'dropping_id'},
                {name: 'price'}
            ]
        }, cfg));
    }
});
jun.rztDroppingTerima = new jun.DroppingTerimastore({
    url: 'Dropping/Getsisatransfer'
});
//jun.rztDroppingTerima.load();
