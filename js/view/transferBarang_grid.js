jun.TransferBarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembelian",
    id: 'docs-jun.TransferBarangGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Invoice No',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_other',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 200,
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'LUNAS'
                } else {
                    return 'BELUM BAYAR'
                }
            }
        },
        {
            header: 'Lunas',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            width: 100
        },
        //,
        // {
        //     header: 'Branch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'store',
        //     width: 100,
        //     renderer: jun.renderStore
        // }
    ],
    initComponent: function () {
        if (jun.rztBarangFlazzCmp.getTotalCount() === 0) {
            jun.rztBarangFlazzCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztSuppFlazzCmp.getTotalCount() === 0) {
            jun.rztSuppFlazzCmp.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        jun.rztTransferBarang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.tglfrom = Ext.getCmp('tglgridpembelianfrom').hiddenField.dom.value;
                    b.params.tglto = Ext.getCmp('tglgridpembelianto').hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridpembelianfrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridpembelianto'
                        }, '-',
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Pembelian',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Pembelian',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Invoice',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Bayar Pembelian',
                    ref: '../btnBayar'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     ref: '../tgl',
                //     id: 'tgltransferbaranggrid'
                // }
            ]
        };
        jun.TransferBarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnBayar.on('Click', this.deleteRec, this);
        this.btnPrint.on('Click', this.printForm, this);
        Ext.getCmp('tglgridpembelianfrom').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridpembelianto').on('select', function () {
            this.store.load();
        }, this);
        // this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pembelian");
            return;
        }
        var idz = selectedz.json.transfer_barang_id;
        window.open('TransferBarang/print/id/' + idz, '_blank');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TransferBarangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        if(selectedz.json.lunas != null){
            Ext.MessageBox.alert("Warning", "Pembelian sudah lunas tidak bisa diedit.");
            return;
        }
        var idz = selectedz.json.transfer_barang_id;
        var form = new jun.TransferBarangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferBarangDetails.proxy.setUrl('TransferBarangDetails/IndexIn');
        jun.rztTransferBarangDetails.baseParams = {
            transfer_barang_id: idz
        };
        jun.rztTransferBarangDetails.load();
        jun.rztTransferBarangDetails.baseParams = {};
    },
    deleteRec: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        if (selectedz.json.lunas != null) {
            Ext.MessageBox.alert("Warning", "Penjualan sudah lunas tidak bisa dibayar.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membayar ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        var form = new  jun.PembayaranBeliFlazz();
        form.show();

        // Ext.Msg.show({
        //     title: 'Pembayaran Flazz',
        //     msg: 'Kas/Bank',
        //     // value: 'choice 2',
        //     buttons: Ext.MessageBox.OKCANCEL,
        //     inputField: new Ext.form.ComboBox(
        //         {
        //             // xtype: 'combo',
        //             //typeAhead: true,
        //             triggerAction: 'all',
        //             lazyRender: true,
        //             mode: 'local',
        //             forceSelection: true,
        //             fieldLabel: 'bank_id',
        //             store: jun.rztBankTransCmpPusat,
        //             valueField: 'bank_id',
        //             displayField: 'nama_bank',
        //         }),
        //     fn: function (buttonId, text) {
        //         if (buttonId != 'ok')
        //             return;
        //         // Ext.Msg.alert('Your Choice', 'You chose: "' + text + '".');
        //         Ext.Ajax.request({
        //             url: 'TransferBarang/bayar/id/' + record.json.transfer_barang_id,
        //             method: 'POST',
        //             // scope: this,
        //             params: {
        //                 bank_id: text
        //             },
        //             success: function (f, a) {
        //                 jun.rztTransferBarang.reload();
        //                 var response = Ext.decode(f.responseText);
        //                 Ext.MessageBox.show({
        //                     title: 'Info',
        //                     msg: response.msg,
        //                     buttons: Ext.MessageBox.OK,
        //                     icon: Ext.MessageBox.INFO
        //                 });
        //             },
        //             failure: function (f, a) {
        //                 switch (a.failureType) {
        //                     case Ext.form.Action.CLIENT_INVALID:
        //                         Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                         break;
        //                     case Ext.form.Action.CONNECT_FAILURE:
        //                         Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                         break;
        //                     case Ext.form.Action.SERVER_INVALID:
        //                         Ext.Msg.alert('Failure', a.result.msg);
        //                 }
        //             }
        //         });
        //     }
        // });
    }
});

jun.PembayaranBeliFlazz = Ext.extend(Ext.Window, {
    title: "Bayar Pembelian Flazz",
    // iconCls: "silk13-report",
    modez: 1,
    width: 350,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-PembayaranBeliFlazz",
                labelWidth: 75,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        anchor: '100%',
                        allowBlank: false,
                        value: DATE_NOW
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-html",
                    text: "Save",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Cancel",
                    // hidden: (STORE != ''),
                    ref: "../btnCancel"
                },
            ]
        };
        jun.PembayaranBeliFlazz.superclass.initComponent.call(this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCancelclick: function () {
        this.close();
    },
    onbtnSaveclick: function () {
        var grid = Ext.getCmp('docs-jun.TransferBarangGrid');
        var record = grid.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Invoice");
            return;
        }
        Ext.getCmp('form-PembayaranBeliFlazz').getForm().submit({
            url: 'TransferBarang/bayar/id/' + record.json.transfer_barang_id,
            scope: this,
            success: function (f, a) {
                jun.rztTransferBarang.reload();
                // var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                // this.btnDisabled(false);
            }
        });
    }
});

//jun.ReturnTransferBarangGrid = Ext.extend(Ext.grid.GridPanel, {
//    title: "Return Supplier Item",
//    id: 'docs-jun.ReturnTransferBarangGrid',
//    iconCls: "silk-grid",
//    stripeRows: true,
//    viewConfig: {
//        forceFit: true
//    },
//    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
//    columns: [
//        {
//            header: 'Date',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'tgl',
//            width: 100
//        },
//        {
//            header: 'Doc. Ref',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'doc_ref',
//            width: 100
//        },
//        {
//            header: 'Note',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'note',
//            width: 100
//        },
//        {
//            header: 'Branch',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'store',
//            width: 100
//        }
//    ],
//    initComponent: function () {
//        if (jun.rztBarangLib.getTotalCount() === 0) {
//            jun.rztBarangLib.load();
//        }
//        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
//            jun.rztBarangNonJasa.load();
//        }
//        if (jun.rztSupplierCmp.getTotalCount() === 0) {
//            jun.rztSupplierCmp.load();
//        }
//        if (jun.rztGrup.getTotalCount() === 0) {
//            jun.rztGrup.load();
//        }
//        jun.rztReturnTransferBarang.on({
//            scope: this,
//            beforeload: {
//                fn: function (a, b) {
//                    //b.params.tgl = Ext.getCmp('tglreturntransferitemgrid').getValue();
//                    var tgl = Ext.getCmp('tglreturntransferbaranggrid');
//                    b.params.tgl = tgl.hiddenField.dom.value;
//                    b.params.mode = "grid";
//                }
//            }
//        });
//        this.store = jun.rztReturnTransferBarang;
//        this.tbar = {
//            xtype: 'toolbar',
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Add Return Item',
//                    ref: '../btnAdd'
//                },
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'button',
//                    text: 'View Return Item',
//                    ref: '../btnEdit'
//                },
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'label',
//                    style: 'margin:5px',
//                    text: '\xA0Date :'
//                },
//                {
//                    xtype: 'xdatefield',
//                    ref: '../tgl',
//                    id: 'tglreturntransfergrid'
//                }
//            ]
//        };
//        jun.ReturnTransferBarangGrid.superclass.initComponent.call(this);
//        this.btnAdd.on('Click', this.loadForm, this);
//        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.tgl.on('select', this.refreshTgl, this);
//        this.getSelectionModel().on('rowselect', this.getrow, this);
//        this.store.removeAll();
//    },
//    refreshTgl: function () {
//        this.store.reload();
//    },
//    getrow: function (sm, idx, r) {
//        this.record = r;
//        var selectedz = this.sm.getSelections();
//    },
//    loadForm: function () {
//        var form = new jun.ReturnTransferBarangWin({modez: 0});
//        form.show();
//    },
//    loadEditForm: function () {
//        var selectedz = this.sm.getSelected();
//        //var dodol = this.store.getAt(0);
//        if (selectedz == undefined) {
//            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
//            return;
//        }
//        var idz = selectedz.json.transfer_barang_id;
//        var form = new jun.ReturnTransferBarangWin({modez: 1, id: idz});
//        form.show(this);
//        form.formz.getForm().loadRecord(this.record);
//        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
//        jun.rztTransferBarangDetails.proxy.setUrl('TransferBarangDetails/IndexOut');
//        jun.rztTransferBarangDetails.baseParams = {
//            transfer_barang_id: idz
//        };
//        jun.rztTransferBarangDetails.load();
//        jun.rztTransferBarangDetails.baseParams = {};
//    },
//    deleteRec: function () {
//        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
//    },
//    deleteRecYes: function (btn) {
//        if (btn == 'no') {
//            return;
//        }
//        var record = this.sm.getSelected();
//        // Check is list selected
//        if (record == undefined) {
//            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
//            return;
//        }
//        Ext.Ajax.request({
//            url: 'ReturnTransferBarang/delete/id/' + record.json.transfer_barang_id,
//            method: 'POST',
//            success: function (f, a) {
//                jun.rztReturnTransferBarang.reload();
//                var response = Ext.decode(f.responseText);
//                Ext.MessageBox.show({
//                    title: 'Info',
//                    msg: response.msg,
//                    buttons: Ext.MessageBox.OK,
//                    icon: Ext.MessageBox.INFO
//                });
//            },
//            failure: function (f, a) {
//                switch (a.failureType) {
//                    case Ext.form.Action.CLIENT_INVALID:
//                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                        break;
//                    case Ext.form.Action.CONNECT_FAILURE:
//                        Ext.Msg.alert('Failure', 'Ajax communication failed');
//                        break;
//                    case Ext.form.Action.SERVER_INVALID:
//                        Ext.Msg.alert('Failure', a.result.msg);
//                }
//            }
//        });
//    }
//})
jun.TransferBarangOutGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Barang Keluar",
    id: 'docs-jun.TransferBarangOutGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasaTransfer.getTotalCount() === 0) {
            jun.rztBarangNonJasaTransfer.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztTransferBarangOut.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglTransferBarangOutGrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarangOut;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Barang Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tampil Barang Keluar',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglTransferBarangOutGrid'
                }
            ]
        };
        jun.TransferBarangOutGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TransferBarangOutWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_barang_id;
        var form = new jun.TransferBarangOutWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferBarangDetails.proxy.setUrl('TransferBarangDetails/IndexOut');            // <<<<<<indexout??
        jun.rztTransferBarangDetails.baseParams = {
            transfer_barang_id: idz
        };
        jun.rztTransferBarangDetails.load();
        jun.rztTransferBarangDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TransferBarangOut/delete/id/' + record.json.transfer_barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTransferBarangOut.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
