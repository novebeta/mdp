jun.KasGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Masuk",
    id: 'docs-jun.KasGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        // {
        //     header: 'Branch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'store',
        //     width: 100
        // }
    ],
    initComponent: function () {
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmpPendapatan.getTotalCount() === 0) {
            jun.rztChartMasterCmpPendapatan.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztKas.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglcashgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglcashgrid').getValue();
                    b.params.mode = "grid";
                    b.params.arus = "masuk";
                }
            }
        });
        this.store = jun.rztKas;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Kas/Bank Masuk',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Masuk',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglcashgrid'
                }
            ]
        };
        jun.KasGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnPrint.on('Click', this.printKas, this);
        this.store.removeAll();
    },
    printKas: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.kas_id;
        window.open('kas/print/id/' + idz, '_blank');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWin({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
//        form.modal.setValue(this.record.data.type_ == '1');
//        form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    }
});
jun.KasGridOut = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Keluar",
    id: 'docs-jun.KasGridOut',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'no_kwitansi',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        // {
        //     header: 'Branch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'store',
        //     width: 100
        // }
    ],
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmpBiaya.getTotalCount() === 0) {
            jun.rztChartMasterCmpBiaya.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztKasKeluar.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglcashgridout');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglcashgridout').getValue();
                    b.params.mode = "grid";
                    b.params.arus = "keluar";
                }
            }
        });
        this.store = jun.rztKasKeluar;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Kas/Bank Keluar',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Keluar',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglcashgridout'
                }
            ]
        };
        jun.KasGridOut.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printKas, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.store.removeAll();
    },
    printKas: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = record.json.kas_id;
        window.open('kas/print/id/' + idz, '_blank');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinOut({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinOut({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    }
});

jun.KasGridPusat = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Masuk",
    id: 'docs-jun.KasGridPusat',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield", filterName: "doc_ref"}
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            filter: {xtype: "textfield", filterName: "tgl"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100,
            filter: {xtype: "textfield", filterName: "keperluan"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "textfield", filterName: "total"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield", filterName: "store"}
        }
    ],
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztAreaCmp.getTotalCount() === 0) {
            jun.rztAreaCmp.load();
        }
        jun.rztKasPusat.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglcashgridpusat');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglcashgridpusat').getValue();
                    b.params.mode = "grid";
                    b.params.arus = "masuk_pusat";
                }
            }
        });
        this.store = jun.rztKasPusat;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Kas/Bank Masuk',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglcashgridpusat'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Import Kas/Bank Masuk',
                //     ref: '../btnImportMasuk'
                // }
            ]
        };
        jun.KasGridPusat.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDel.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        // this.btnImportMasuk.on('Click', this.importKasMasuk, this);
        this.store.removeAll();
    },
    importKasMasuk: function () {
        var form = new jun.ImportKas({arus: 1});
        form.show();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinPusat({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinPusat({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
//        form.modal.setValue(this.record.data.type_ == '1');
//        form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'Kas/delete',
            method: 'POST',
            params: {
                id: record.json.kas_id
            },
            success: function (f, a) {
                jun.rztKasPusat.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KasGridPusatOut = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Keluar",
    id: 'docs-jun.KasGridPusatOut',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield", filterName: "doc_ref"}
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            filter: {xtype: "textfield", filterName: "tgl"}
        },
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'no_kwitansi',
            width: 100,
            filter: {xtype: "textfield", filterName: "no_kwitansi"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100,
            filter: {xtype: "textfield", filterName: "keperluan"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "textfield", filterName: "total"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield", filterName: "store"}
        }
    ],
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztAreaCmp.getTotalCount() === 0) {
            jun.rztAreaCmp.load();
        }
        jun.rztKasPusatKeluar.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //var tgl = Ext.getCmp('tglcashgridpusatout').getValue();
                    var tgl = Ext.getCmp('tglcashgridpusatout');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = new Date(tgl).toISOString();
                    b.params.mode = "grid";
                    b.params.arus = "keluar_pusat";
                }
            }
        });
        this.store = jun.rztKasPusatKeluar;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Kas/Bank Keluar',
                    ref: '../btnEdit'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Delete Kas/Bank Keluar',
                //     ref: '../btnDel'
                // },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglcashgridpusatout'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Import Kas/Bank Keluar',
                //     ref: '../btnImport'//,
                //     // hidden: !(UID == '7b9053c1-36c4-11e6-85f5-00ff55a5a602')
                // }
            ]
        };
        jun.KasGridPusatOut.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDel.on('Click', this.deleteRec, this);
        // this.btnImport.on('Click', this.importKas, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinPusatOut({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinPusatOut({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    },
    importKas: function () {
        var form = new jun.ImportKas({arus: -1});
        form.show();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'Kas/delete',
            method: 'POST',
            params: {
                id: record.json.kas_id
            },
            success: function (f, a) {
                jun.rztKasPusatKeluar.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

jun.KasGridBp = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Masuk",
    id: 'docs-jun.KasGridBp',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Dari',
            sortable: true,
            resizable: true,
            dataIndex: 'subject',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        // {
        //     header: 'Branch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'store',
        //     width: 100
        // }
    ],
    initComponent: function () {
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztBankBp.getTotalCount() === 0) {
            jun.rztBankBp.load();
        }
        if (jun.rztChartMasterCmpPendapatan.getTotalCount() === 0) {
            jun.rztChartMasterCmpPendapatan.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztKasBp.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglcashbpgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    // b.params.tgl = Ext.getCmp('tglcashbpgrid').getValue();
                    b.params.mode = "bp";
                    b.params.arus = "masuk";
                }
            }
        });
        this.store = jun.rztKasBp;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Kas/Bank Masuk',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Masuk',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglcashbpgrid'
                }
            ]
        };
        jun.KasGridBp.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnPrint.on('Click', this.printKas, this);
        this.store.removeAll();
    },
    printKas: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.kas_id;
        window.open('kas/print/id/' + idz, '_blank');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinBp({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinBp({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
//        form.modal.setValue(this.record.data.type_ == '1');
//        form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    }
});
jun.KasGridBpOut = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Keluar",
    id: 'docs-jun.KasGridBpOut',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'no_kwitansi',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        // {
        //     header: 'Branch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'store',
        //     width: 100
        // }
    ],
    initComponent: function () {
        if (jun.rztBankBp.getTotalCount() === 0) {
            jun.rztBankBp.load();
        }
        if (jun.rztChartMasterCmpBiaya.getTotalCount() === 0) {
            jun.rztChartMasterCmpBiaya.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztKasKeluarBp.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglcashgridoutbp');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglcashgridout').getValue();
                    b.params.mode = "bp";
                    b.params.arus = "keluar";
                }
            }
        });
        this.store = jun.rztKasKeluarBp;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Kas/Bank Keluar',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Keluar',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglcashgridoutbp'
                }
            ]
        };
        jun.KasGridBpOut.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printKas, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.store.removeAll();
    },
    printKas: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = record.json.kas_id;
        window.open('kas/print/id/' + idz, '_blank');
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinBpOut({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinBpOut({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    }
});