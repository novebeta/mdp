jun.BpBayarJasastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayarJasastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayarJasaStoreId',
            url: 'BpBayarJasa',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_bayar_jasa_id'},
                {name: 'tgl'},
                {name: 'total'},
                {name: 'total_bayar'},
                {name: 'bank_id'},
                {name: 'id_user'},
                {name: 'tdate'},
                {name: 'note'},
                {name: 'account_code'},
                {name: 'ref'},
                {name: 'pph23'},
            ]
        }, cfg));
    }
});
jun.rztBpBayarJasa = new jun.BpBayarJasastore();
//jun.rztBpBayarJasa.load();
jun.BpBayarJasaListstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpBayar2Liststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpBayarJasaListstoreId',
            url: 'BpBayarJasa/list',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_jasa_id'},
                {name: 'inv_no'},
                {name: 'inv_tgl'},
                {name: 'hpp', type: "float"},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'account_code'},
                {name: 'body_paint_id'}
            ]
        }, cfg));
    },
});
jun.rztBpBayarJasaList = new jun.BpBayarJasaListstore();