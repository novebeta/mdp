jun.StoreGroupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.StoreGroupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StoreGroupStoreId',
            url: 'StoreGroup',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'nscc_store_group_id'},
{name:'nama_group'},
{name:'up'},
                
            ]
        }, cfg));
    }
});
jun.rztStoreGroup = new jun.StoreGroupstore();
//jun.rztStoreGroup.load();
