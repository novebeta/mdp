jun.BpBayar3Win = Ext.extend(Ext.Window, {
    title: 'Pembayaran Pribadi',
    modez: 1,
    width: 600,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BpBayar3',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    new jun.BpBayar3Grid({
                        height: 175,
                        ref: '../BpBayar3Grid',
                        frameHeader: !1,
                        header: !1,
                    }),
                    {
                        style: 'margin-top:5px',
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        value: DATE_NOW,
                        //allowBlank: 1,,
                        width: 175
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total Invoice',
                        hideLabel: false,
                        readOnly: true,
                        //hidden:true,
                        name: 'grand_total',
                        id: 'grand_totalid',
                        ref: '../grand_total',
                        maxLength: 15,
                        width: 175
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total Bayar',
                        hideLabel: false,
                        //hidden:true,
                        readOnly: true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 15,
                        width: 175
                    },
                    {
                        xtype: 'hidden',
                        name: 'body_paint_id',
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Print Kwitansi',
                    hidden: false,
                    ref: '../btnPrintKwitansi'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BpBayar3Win.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnPrintKwitansi.on('click', this.onPrintKwitansi, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(false);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
    },
    onPrintKwitansi: function () {
        var selectedz = Ext.getCmp('docs-jun.BodyPaintINVGrid').sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Invoice");
            return;
        }
        if (selectedz.data.biaya != 'PRIBADI') {
            Ext.MessageBox.alert("Warning", "Bukan pembiayaan own risk");
            return;
        }
        if (selectedz.data.b3_tgl == null) {
            Ext.MessageBox.alert("Warning", "Belum ada pembayaran.");
            return;
        }
        var idz = selectedz.json.body_paint_id;
        window.open('bodyPaint/printKwitansiPribadi/id/' + idz, '_blank');
    },
    btnDisabled: function (status) {
        // this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var total = parseFloat(Ext.getCmp("totalid").getValue());
        var grand_total = parseFloat(Ext.getCmp("grand_totalid").getValue());
        if (grand_total < total) {
            Ext.MessageBox.alert("Warning", "Jumlah yang di bayar tidak sesuai nilai invoice.");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'BpBayar3/create/';
        Ext.getCmp('form-BpBayar3').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztBpBayar3.data.items, "data")),
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztINVBodyPaint.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});