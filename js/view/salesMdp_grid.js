jun.SalesMdpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales Order",
    id: 'docs-jun.SalesMdpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. SO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Polisi',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        {
            header: 'Merk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_merk',
            width: 100
        },
        {
            header: 'Tipe Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_tipe',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
            width: 100
        },
        /*
{
header:'totalpot',
sortable:true,
resizable:true,
dataIndex:'totalpot',
width:100
},
                {
header:'total_vat',
sortable:true,
resizable:true,
dataIndex:'total_vat',
width:100
},
                {
header:'total',
sortable:true,
resizable:true,
dataIndex:'total',
width:100
},
                {
header:'mobil_id',
sortable:true,
resizable:true,
dataIndex:'mobil_id',
width:100
},
                {
header:'store_kode',
sortable:true,
resizable:true,
dataIndex:'store_kode',
width:100
},
                {
header:'tgl_finish',
sortable:true,
resizable:true,
dataIndex:'tgl_finish',
width:100
},
                {
header:'tgl_wo',
sortable:true,
resizable:true,
dataIndex:'tgl_wo',
width:100
},
                {
header:'upload',
sortable:true,
resizable:true,
dataIndex:'upload',
width:100
},
                {
header:'edited',
sortable:true,
resizable:true,
dataIndex:'edited',
width:100
},
        */
    ],
    initComponent: function () {
        this.store = jun.rztSalesMdp;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Sales Order',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Sales Order',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Instruksi',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Refresh Sales Order',
                    ref: '../btnRefresh'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.SalesMdpGrid.superclass.initComponent.call(this);
        jun.rztBarangView.load();
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.btnRefresh.on('Click', function () {
            this.store.reload();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztMobilViewSalesCmp.load({
            params: {
                mobil_id: this.record.data.mobil_id
            }
        });
    },
    loadForm: function () {
        if (STORE == '') {
            Ext.Msg.alert('Failure', 'Pusat tidak bisa membuat sales order.');
            return;
        }
        Ext.Ajax.request({
            url: 'SalesMdp/Check/',
            method: 'POST',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.msg != 'OK') {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                } else {
                    var form = new jun.SalesMdpWin({modez: 0});
                    form.show();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesMdpWin({modez: 1, id: idz, title: "Sales Order - " + this.record.data.doc_ref});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pekerjaan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesMdpWin({
            modez: 2,
            id: idz,
            title: "Print Instruksi Pekerjaan - " + this.record.data.doc_ref
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'SalesMdp/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSalesMdp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
