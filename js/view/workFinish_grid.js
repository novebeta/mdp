jun.WorkFinishGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pekerjaan Selesai",
    id: 'docs-jun.WorkFinishGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_so',
            renderer: Ext.util.Format.dateRenderer('d-M-Y')
        },
        {
            header: 'No. SO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
        },
        {
            header: 'No. Polisi',
            sortable: true,
            resizable: true,
            dataIndex: 'no_pol',
        },
        {
            header: 'Merk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_merk',
        },
        {
            header: 'Tipe Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_tipe',
        },
        {
            header: 'Kategori',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kategori',
        },
        {
            header: 'Usia Mobil',
            sortable: true,
            resizable: true,
            dataIndex: 'age',
        },
        {
            header: 'Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
        },
        {
            header: 'Harga Awal',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        },
        {
            header: 'Harga Akhir',
            sortable: true,
            resizable: true,
            dataIndex: 'tot_after_disc',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0"),
        }
        /*
{
header:'totalpot',
sortable:true,
resizable:true,
dataIndex:'totalpot',
width:100
},
                {
header:'total_vat',
sortable:true,
resizable:true,
dataIndex:'total_vat',
width:100
},
                {
header:'total',
sortable:true,
resizable:true,
dataIndex:'total',
width:100
},
                {
header:'mobil_id',
sortable:true,
resizable:true,
dataIndex:'mobil_id',
width:100
},
                {
header:'store_kode',
sortable:true,
resizable:true,
dataIndex:'store_kode',
width:100
},
                {
header:'tgl_finish',
sortable:true,
resizable:true,
dataIndex:'tgl_finish',
width:100
},
                {
header:'tgl_wo',
sortable:true,
resizable:true,
dataIndex:'tgl_wo',
width:100
},
                {
header:'upload',
sortable:true,
resizable:true,
dataIndex:'upload',
width:100
},
                {
header:'edited',
sortable:true,
resizable:true,
dataIndex:'edited',
width:100
},
        */
    ],
    initComponent: function () {
        jun.rztWorkFinish.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl_awal = Ext.getCmp('tglawalworkfinishgridid');
                    b.params.tgl_awal = tgl_awal.hiddenField.dom.value;
                    var tgl_akhir = Ext.getCmp('tglakhirworkfinishgridid');
                    b.params.tgl_akhir = tgl_akhir.hiddenField.dom.value;
                    var seller = Ext.getCmp('storeworkfinishgridid').getValue();
                    b.params.store = seller;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztWorkFinish;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    pageSize: 20,
                    displayInfo: true,
                    refreshText: 'Refresh Pekerjaan',
                    // displayMsg: 'Displaying topics {0} - {1} of {2}',
                    // emptyMsg: "No topics to display",
                    items: [
                        '-',
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            // fieldLabel: 'Branch',
                            // ref: '../store',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            id: 'storeworkfinishgridid',
                            store: jun.rztStoreCmp,
                            forceSelection: true,
                            hiddenName: 'store',
                            width: 100,
                            // name: 'store',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            emptyText: "All Reseller",
                            value: STORE,
                            readOnly: (STORE !== ''),
                        },
                        '-',
                        {
                            xtype: 'xdatefield',
                            // ref: '../tgl_awal',
                            id: 'tglawalworkfinishgridid',
                            fieldLabel: 'Date',
                            format: 'd M Y',
                            width: 125,
                            //readOnly: true,
                            allowBlank: false,
                            value: DATE_FIRST
                        },
                        {
                            xtype: 'label',
                            text: 's/d'
                        },
                        {
                            xtype: 'xdatefield',
                            // ref: '../tgl_akhir',
                            id: 'tglakhirworkfinishgridid',
                            fieldLabel: 'Date',
                            format: 'd M Y',
                            width: 125,
                            //readOnly: true,
                            allowBlank: false,
                            value: DATE_LAST
                        },
                        '-',
                    ]
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Tambah',
                //     ref: '../btnAdd'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Lihat Pekerjaan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Upload',
                    ref: '../btnLihatUpload'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Refresh Pekerjaan',
                //     ref: '../btnRefresh'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Print Sertifikat & Garansi Rush Protection',
                    disabled: true,
                    ref: '../btnPrintSertifikatGaransi'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Sertifikat Paint & Soundproof',
                    disabled: true,
                    ref: '../btnPrintSertifikat'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (STORE !== ''),
                },
                {
                    xtype: 'button',
                    text: 'Print Work Order',
                    hidden: (STORE !== ''),
                    ref: '../btnPrintWorkOrder'
                },
                // {
                //     xtype: "form",
                //     frame: !1,
                //     id: "form-PrintWorkOrder",
                //     labelWidth: 100,
                //     labelAlign: "left",
                //     layout: "form",
                //     ref: "formz",
                //     border: !1,
                //     items: [
                //         {
                //             xtype: "hidden",
                //             name: "format",
                //             ref: "../../sales_id"
                //         }
                //     ]
                // }
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.WorkFinishGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnLihatUpload.on('Click', this.lihatUpload, this);
        this.btnPrintSertifikatGaransi.on('Click', this.printSertifikatGaransi, this);
        this.btnPrintSertifikat.on('Click', this.printSertifikat, this);
        this.btnPrintWorkOrder.on('Click', this.printWorkOrder, this);
        // this.btnRefresh.on('Click', function () {
        //     this.store.reload();
        // }, this);
        jun.rztStoreCmp.reload();
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    lihatUpload: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pekerjaan");
            return;
        }
        javascript:window.open('upload/' + selectedz.json.sales_id + '.jpeg', '_blank');
    },
    printSertifikatGaransi: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pekerjaan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesMdpWin({
            modez: 3,
            id: idz,
            title: "Print Sertifikat dan Garansi - " + this.record.data.doc_ref
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    printSertifikat: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pekerjaan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesMdpWin({
            modez: 4,
            id: idz,
            title: "Print Sertifikat - " + this.record.data.doc_ref
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    printWorkOrder: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pekerjaan");
            return;
        }
        // if (selectedz.json.no_woug != '') {
        //     Ext.MessageBox.alert("Warning", "Work Order sudah di print.");
        //     return;
        // }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesMdpWin({
            modez: 5,
            id: idz,
            title: "Print Work Order - " + this.record.data.doc_ref
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.record.data.nama_barang.toLowerCase().includes('rust')) {
            if (this.record.data.age < (3 * 360)) {
                this.btnPrintSertifikatGaransi.setDisabled(false);
            } else {
                this.btnPrintSertifikatGaransi.setDisabled(true);
            }
        } else {
            this.btnPrintSertifikatGaransi.setDisabled(true);
        }
        if (this.record.data.nama_barang.toLowerCase().includes('paint') || this.record.data.nama_barang.toLowerCase().includes('soundproof')) {
            this.btnPrintSertifikat.setDisabled(false);
        } else {
            this.btnPrintSertifikat.setDisabled(true);
        }
        jun.rztMobilViewSalesCmp.load({
            params: {
                mobil_id: this.record.data.mobil_id
            }
        });
    },
    loadForm: function () {
        var form = new jun.WorkFinishWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Pekerjaan Selesai");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.WorkFinishWin({modez: 1, id: idz, title: "Pekerjaan Selesai - " + this.record.data.doc_ref});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetailMdp.load({
            params: {
                sales_id: idz
            }
        });
        jun.getMobilDeskripsi('panel_mobil_info', this.record.data.mobil_id);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'WorkFinish/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztWorkFinish.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
