jun.Ulptstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Ulptstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'UlptStoreId',
            url: 'Ulpt',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'ulpt_id'},
                {name: 'doc_ref'},
                {name: 'salestrans_id'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'tgl'},
                {name: 'amount'},
                {name: 'note'},
                {name: 'bank_id'},
                {name: 'account_code'},
                {name: 'customer_id'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'card_id'},
                {name: 'tipe_ulpt'},
                {name: 'lunas'}
            ]
        }, cfg));
    }
});
jun.rztUlpt = new jun.Ulptstore();
