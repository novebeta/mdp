jun.SyncLogWin = Ext.extend(Ext.Window, {
    title: 'Sync Logs',
    modez:1,
    width: 879,
    height: 482,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-SyncLog',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    new jun.SyncLogGrid({
                        height: 405 + 30 - 10,
                        frameHeader: !1,
                        header: !1,
                        ref: "../gridLog",
                        x: 5,
                        y: 65 + 30
                    })
                ]
            }];
        jun.SyncLogWin.superclass.initComponent.call(this);
        this.gridLog.store.baseParams=this.storecode;
        this.gridLog.paging.doRefresh();

    }
});