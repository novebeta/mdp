jun.Supplierstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Supplierstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SupplierStoreId',
            url: 'Supplier',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'supplier_id'},
                {name:'supplier_name'},
                {name:'account_code'},
                {name:'up'},
                {name:'supp_address'},
                {name:'supp_company'},
                {name:'supp_phone'},
                {name:'supp_fax'},
                {name:'supp_city'},
                {name:'supp_country'},
                {name:'supp_email'},
                {name:'bank_rek'},
                {name:'no_rek'},
                {name:'nama_rek'},
                {name:'termofpayment'},
                {name:'name_cp'},
                {name:'npwp'}
            ]
        }, cfg));
    }
});
jun.rztSupplier = new jun.Supplierstore();
jun.rztSupplierLib = new jun.Supplierstore();
jun.rztSupplierCmp = new jun.Supplierstore();
jun.rztSupplierLib.load();
