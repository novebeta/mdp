jun.InvoiceMdpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.InvoiceMdpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InvoiceMdpStoreId',
            url: 'InvoiceMdp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'invoice_id'},
                {name: 'doc_ref'},
                {name: 'tgl_cetak'},
                {name: 'total'},
                {name: 'store_kode'},
                {name: 'tgl_bayar'},
                {name: 'tgl_periode'},
                {name: 'sub_total'},
                {name: 'disc'},
                {name: 'disc_rp'},
                {name: 'ppn'},
                {name: 'jml'},
                {name: 'bank_id'}
            ]
        }, cfg));
    }
});
jun.rztInvoiceMdp = new jun.InvoiceMdpstore();
//jun.rztInvoiceMdp.load();
