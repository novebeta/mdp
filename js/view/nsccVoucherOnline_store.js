jun.NsccVoucherOnlinestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.NsccVoucherOnlinestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'NsccVoucherOnlineStoreId',
            url: 'NsccVoucherOnline',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'id'},
{name:'kode_voucher'},
{name:'nama_voucher'},
{name:'desc'},
{name:'exp'},
{name:'tdate'},
{name:'outlet_id'},
                
            ]
        }, cfg));
    }
});
jun.rztNsccVoucherOnline = new jun.NsccVoucherOnlinestore();
//jun.rztNsccVoucherOnline.load();
