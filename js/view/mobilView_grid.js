jun.MobilViewGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"MobilView",
        id:'docs-jun.MobilViewGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
                        {
			header:'mobil_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'mobil_id',
			width:100
		},
                                {
			header:'mobil_tipe_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'mobil_tipe_id',
			width:100
		},
                                {
			header:'tahun_pembuatan',
			sortable:true,
			resizable:true,                        
            dataIndex:'tahun_pembuatan',
			width:100
		},
                                {
			header:'mulai_stnk',
			sortable:true,
			resizable:true,                        
            dataIndex:'mulai_stnk',
			width:100
		},
                                {
			header:'no_rangka',
			sortable:true,
			resizable:true,                        
            dataIndex:'no_rangka',
			width:100
		},
                                {
			header:'no_pol',
			sortable:true,
			resizable:true,                        
            dataIndex:'no_pol',
			width:100
		},
                		/*
                {
			header:'customer_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'customer_id',
			width:100
		},
                                {
			header:'nama_customer',
			sortable:true,
			resizable:true,                        
            dataIndex:'nama_customer',
			width:100
		},
                                {
			header:'alamat',
			sortable:true,
			resizable:true,                        
            dataIndex:'alamat',
			width:100
		},
                                {
			header:'tipe',
			sortable:true,
			resizable:true,                        
            dataIndex:'tipe',
			width:100
		},
                                {
			header:'merk',
			sortable:true,
			resizable:true,                        
            dataIndex:'merk',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztMobilView;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Ubah',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.MobilViewGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.MobilViewWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.mobil_id;
            var form = new jun.MobilViewWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'MobilView/delete/id/' + record.json.mobil_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztMobilView.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
