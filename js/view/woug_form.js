jun.WougWin = Ext.extend(Ext.Window, {
    title: 'Pembayaran UG',
    modez: 1,
    width: 500,
    height: 430,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Woug',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    new jun.WougListGrid({
                        height: 247,
                        ref: '../wougListGrid',
                        frameHeader: !1,
                        header: !1,
                    }),
                    {
                        style: 'margin-top:5px',
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Total',
                        //hidden:true,
                        name: 'total',
                        id: 'totalwougid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        width: 275
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        ref: '../bank',
                        store: jun.rztBankTransCmpPusat,
                        allowBlank: false,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 275,
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_bayar',
                        fieldLabel: 'Tgl',
                        name: 'tgl_bayar',
                        id: 'tgl_bayarid',
                        format: 'd M Y',
                        width: 175,
                        // readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.WougWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on('close', this.onClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
        }
    },
    onClose: function () {
        jun.rztWougList.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'InvoiceUg/create/';
        if (parseFloat(this.total.getValue()) <= 0) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Total harus lebih besar dari nol.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
        }
        if (this.bank.getValue() == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Kas/Bank untuk pembayaran ini harus dipilih.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
        }
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'Woug/update/id/' + this.id;
        // } else {
        //     urlz = 'Woug/create/';
        // }
        Ext.getCmp('form-Woug').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztWougList.data.items, "data")),
            },
            success: function (f, a) {
                jun.rztWoug.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Woug').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});