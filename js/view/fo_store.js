jun.Fostore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Fostore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FoStoreId',
            url: 'Fo',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'fo_id'},
                {name: 'nama_fo'},
                {name: 'gol_id'},
                {name: 'kode_fo'},
                {name: 'active'},
                {name: 'store'},
                {name: 'up'},
                {name: 'tipe'}
            ]
        }, cfg));
    }
});
jun.rztFo = new jun.Fostore();
jun.rztFoCmp = new jun.Fostore();
jun.rztFoLib = new jun.Fostore();
//jun.rztFo.load();
