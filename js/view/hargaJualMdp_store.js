jun.HargaJualMdpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.HargaJualMdpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'HargaJualMdpStoreId',
            url: 'HargaJualMdp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'harga_id'},
                {name: 'harga'},
                {name: 'harga_ug'},
                {name: 'barang_id'},
                {name: 'mobil_kategori_id'},
                {name: 'discrp'},
                {name: 'vat'},
                {name: 'disc'}
            ]
        }, cfg));
    }
});
jun.rztHargaJualMdp = new jun.HargaJualMdpstore();
//jun.rztHargaJualMdp.load();
