jun.JasaBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JasaBpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JasaBpStoreId',
            url: 'JasaBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jasa_bp_id'},
                {name: 'nama'},
                {name: 'kode'},
                {name: 'account_code'},
                {name: 'overhead'},
            ]
        }, cfg));
    }
});
jun.rztJasaBp = new jun.JasaBpstore();
jun.rztJasaBpCmp = new jun.JasaBpstore();
jun.rztJasaBpLib = new jun.JasaBpstore();
jun.rztJasaBpLib.load();
