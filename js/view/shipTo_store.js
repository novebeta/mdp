jun.ShipTostore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ShipTostore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ShipToStoreId',
            url: 'ShipTo',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'ship_to_id'},
                {name: 'nama'},
                {name: 'ship_to_company'},
                {name: 'ship_to_address'},
                {name: 'ship_to_city'},
                {name: 'ship_to_country'},
                {name: 'ship_to_phone'},
                {name: 'ship_to_fax'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztShipTo = new jun.ShipTostore();
jun.rztShipToCmp = new jun.ShipTostore();

// jun.rztShipToCmp.load();
//jun.rztShipTo.load();
