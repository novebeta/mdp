jun.BarangPerlengkapanWin = Ext.extend(Ext.Window, {
    title: 'BarangPerlengkapan',
    modez: 1,
    width: 400,
    height: 190,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BarangPerlengkapan',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Barang Perlengkapan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_brg_perlengkapan',
                        id: 'kode_brg_perlengkapanid',
                        ref: '../kode_brg_perlengkapan',
                        maxLength: 20,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Barang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_brg_perlengkapan',
                        id: 'nama_brg_perlengkapanid',
                        ref: '../nama_brg_perlengkapan',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Keterangan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket_brg_perlengkapan',
                        id: 'ket_brg_perlengkapanid',
                        ref: '../ket_brg_perlengkapan',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                    //{
                    //    xtype: 'textfield',
                    //    fieldLabel: 'Satuan',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'satuan_perlengkapan',
                    //    id: 'satuan_perlengkapanid',
                    //    ref: '../satuan_perlengkapan',
                    //    maxLength: 20,
                    //    //allowBlank: ,
                    //    anchor: '100%'
                    //}
                    //{
                    //    xtype: 'textfield',
                    //    fieldLabel: 'Harga',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'harga_perlengkapan',
                    //    id: 'harga_perlengkapanid',
                    //    ref: '../harga_perlengkapan',
                    //    maxLength: 30,
                    //    //allowBlank: ,
                    //    anchor: '100%'
                    //},
                    //{
                    //    xtype: 'textfield',
                    //    fieldLabel: 'up',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'up',
                    //    id: 'upid',
                    //    ref: '../up',
                    //    maxLength: 3,
                    //    //allowBlank: ,
                    //    anchor: '100%'
                    //},

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BarangPerlengkapanWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {

            urlz = 'BarangPerlengkapan/update/id/' + this.id;

        } else {

            urlz = 'BarangPerlengkapan/create/';
        }

        Ext.getCmp('form-BarangPerlengkapan').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBarangPerlengkapan.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BarangPerlengkapan').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Semua kolom harus diisi!');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});