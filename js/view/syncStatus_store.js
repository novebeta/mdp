jun.SyncStatusstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.SyncStatusstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SyncStatusStoreId',
            url: 'SyncStatus',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'sync_id'},
                {name:'store_code'},
                {name:'ip_address'},
                {name:'glt_pusat'},
                {name:'btr_pusat'},
                {name:'glt_cabang'},
                {name:'btr_cabang'},
                {name:'diff_btr'},
                {name:'diff_glt'},
                {name:'updated_date'},
                {name:'startscan'},
                {name:'endscan'},
                {name:'status_scan'},
                {name:'sync_date'},
                {name:'startsync'},
                {name:'endsync'},
                {name:'status_sync'},
                {name:'note'},
            ]
        }, cfg));
    }
});
jun.rztSyncStatus = new jun.SyncStatusstore();
//jun.rztSyncStatus.load();
