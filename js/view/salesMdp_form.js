jun.SalesMdpWin = Ext.extend(Ext.Window, {
    title: 'Sales Order',
    modez: 1,
    width: 735,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SalesMdp',
                layout: 'table',
                layoutConfig: {columns: 2},
                defaults: {width: 350},
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 45,
                        items: [
                            {
                                xtype: 'xdatefield',
                                ref: '../../tgl_so',
                                fieldLabel: 'Tgl',
                                name: 'tgl_so',
                                id: 'tgl_soid',
                                format: 'd M Y',
                                value: DATE_NOW,
                                // readOnly: true,
                                allowBlank: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: "hidden",
                                name: "sales_id",
                                ref: "../../sales_id"
                            },
                            {
                                xtype: "hidden",
                                name: "mobil_km",
                                ref: "../../mobil_km"
                            },
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 45,
                        items: [
                            {
                                xtype: 'combo',
                                fieldLabel: 'Mobil',
                                ref: '../../mobil_id',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'remote',
                                forceSelection: true,
                                autoSelect: false,
                                store: jun.rztMobilViewSalesCmp,
                                id: "mobilsales_id",
                                hiddenName: 'mobil_id',
                                valueField: 'mobil_id',
                                displayField: 'no_pol',
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                itemSelector: 'div.search-item',
                                tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">',
                                    '<h3><span>{no_pol}<br />{tahun_pembuatan}</span>{no_rangka}</h3>',
                                    '{customer_id}',
                                    '</div></tpl>'
                                ),
                                allowBlank: false,
                                listWidth: 450,
                                lastQuery: "",
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        colspan: 2,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        // height: 220,
                        width: 700,
                        layout: 'form',
                        border: false,
                        items: [
                            new jun.SalesDetailMdpGrid({
                                hideToolbars: (this.modez != 1 && this.modez != 0),
                                // colspan: 2,
                                height: 150,
                                // width: 700,
                                frameHeader: !1,
                                header: !1,
                                ref: "../griddetil"
                            }),
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        frame: false,
                        border: false,
                        height: 120,
                        layout: {
                            type: 'vbox',
                            // padding: '5',
                            align: 'stretchmax'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                id: 'panel_mobil_info',
                                frame: false,
                                bodyStyle: 'background-color: #E4E4E4;',
                                layout: {
                                    type: 'vbox',
                                    // padding: '5',
                                    align: 'stretchmax'
                                },
                                height: 100,
                                border: false,
                                ref: '../../mobil_info',
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Sub Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'sub_total',
                                id: 'sub_total_sales_id',
                                ref: '../sub_total',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Disc',
                                hideLabel: false,
                                //hidden:true,
                                name: 'totalpot',
                                id: 'total_pot_sales_id',
                                ref: '../totalpot',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'PPN',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total_vat',
                                id: 'total_vat_sales_id',
                                ref: '../total_vat',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total',
                                id: 'total_sales_id',
                                ref: '../total',
                                maxLength: 15,
                                readOnly: true,
                                anchor: '100%'
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Print',
                    hidden: (this.modez != 2 && this.modez != 3 && this.modez != 4 && this.modez != 5),
                    ref: '../btnPrint'
                },
                {
                    xtype: 'button',
                    text: 'Simpan Sales Order',
                    hidden: (this.modez != 1 && this.modez != 0),
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalesMdpWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on("close", this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnPrint.on('click', this.onbtnSaveclick, this);
        this.mobil_id.on('select', this.onSelectMobil, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(false);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
    },
    onbtnSaveclick: function () {
        if (this.modez == 2) {
            Ext.getCmp("form-SalesMdp").getForm().url = 'salesMdp/PrintInstruksiKerja';
        }
        if (this.modez == 3) {
            Ext.getCmp("form-SalesMdp").getForm().url = 'salesMdp/PrintSertifikatRushProtection';
        }
        if (this.modez == 4) {
            Ext.getCmp("form-SalesMdp").getForm().url = 'salesMdp/PrintSertifikatPaintProtection';
        }
        if (this.modez == 5) {
            Ext.getCmp("form-SalesMdp").getForm().url = 'salesMdp/PrintWordOrderUltraGrand';
        }
        Ext.getCmp("form-SalesMdp").getForm().standardSubmit = !0;
        var form = Ext.getCmp('form-SalesMdp').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        setTimeout(function () {
            jun.rztSalesMdp.reload();
            jun.rztWorkOrder.reload();
        }, 3000);
        this.close();
    },
    onSelectMobil: function (c, r, i) {
        jun.getMobilDeskripsi('panel_mobil_info', r.data.mobil_id);
        var tgl_so = this.tgl_so.getValue();
        var mulai_stnk = Date.parseDate(r.data.mulai_stnk, 'Y-m-d');
        var diff = date_diff_indays(mulai_stnk, tgl_so);
        this.hideBarang = 'UC RUST PROTECTION';
        this.mobil_tipe_id = r.data.mobil_tipe_id;
        this.mobil_km.setValue(r.data.tahun_km);
        if (diff >= 60 || parseInt(r.data.tahun_km) >= 10000) {
            // console.log('under 30');
            this.hideBarang = 'NC RUST PROTECTION';
        }
        if (diff >= (360 * 3)) {
            Ext.Msg.alert('Error', "Sertifikat tidak akan di cetak karena mobil berusia lebih dari 3 tahun.");
        }
        // console.log(diff);
        jun.rztBarangView.load({
            params: {
                query_not: this.hideBarang,
                mobil_tipe_id: this.mobil_tipe_id
            }
        });
    },
    onWinClose: function () {
        jun.rztSalesDetailMdp.removeAll();
        jun.rztSalesMdp.reload();
        // jun.rztBarangView.clearFilter();
        // this.mobil_id.lastQuery = null;
    },
    btnDisabled: function (status) {
        // this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveConfirm: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah data yang Anda isikan yakin sudah benar?', this.saveForm, this);
    },
    saveForm: function (btn) {
        if (btn == 'no') {
            return;
        }
        if (jun.rztSalesDetailMdp.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            return;
        }
        this.btnDisabled(true);
        var urlz = 'SalesMdp/create/';
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'SalesMdp/update/id/' + this.id;
        // } else {
        //     urlz = 'SalesMdp/create/';
        // }
        Ext.getCmp('form-SalesMdp').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztSalesDetailMdp.data.items, "data")),
                mode: this.modez,
                id: this.id
            },
            success: function (f, a) {
                jun.rztSalesMdp.reload();
                jun.rztWorkOrder.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                // if (this.modez == 0) {
                //     Ext.getCmp('form-SalesMdp').getForm().reset();
                //     this.btnDisabled(false);
                // }
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveConfirm(true);
    },
    // onbtnSaveclick: function () {
    //     this.closeForm = false;
    //     this.saveConfirm(false);
    // },
    onbtnCancelclick: function () {
        this.close();
    }
});