jun.BpSuppPartsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BpSuppPartsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BpSuppPartsStoreId',
            url: 'BpSuppParts',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bp_supp_parts_id'},
                {name: 'kode_supp'},
                {name: 'nama_supp'},
                {name: 'tipe'},
            ]
        }, cfg));
    }
});
jun.rztBpSuppParts = new jun.BpSuppPartsstore();
jun.rztBpSuppPartsCmp = new jun.BpSuppPartsstore();
jun.rztBpSuppPartsLib = new jun.BpSuppPartsstore();
jun.rztBpSuppPartsLib.load();
