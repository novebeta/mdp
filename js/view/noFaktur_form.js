jun.NoFakturWin = Ext.extend(Ext.Window, {
	title: "Nomor Faktur",
	modez: 1,
	width: 325,
	height: 207,
	layout: "form",
	modal: true,
	padding: 5,
	closeForm: false,
	initComponent: function () {
		this.items = [
			{
				xtype: "form",
				frame: false,
				bodyStyle: "background-color: #E4E4E4; padding: 10px",
				id: "form-NoFaktur",
				labelWidth: 150,
				labelAlign: "left",
				layout: "form",
				ref: "formz",
				border: false,
				items: [
					{
						xtype: "textfield",
						fieldLabel: "Nomor Faktur Awal",
						hideLabel: false,
						//hidden:true,
						name: "no_faktur_awal",
						id: "no_faktur_awalid",
						ref: "../no_faktur_awal",
						maxLength: 20,
						allowBlank: 0,
						anchor: "100%",
					},
					{
						xtype: "numericfield",
						fieldLabel: "Jumlah",
						hideLabel: false,
						//hidden:true,
						name: "jml_nofaktur",
						id: "jml_nofakturid",
						ref: "../jml_nofaktur",
						maxLength: 20,
                        enableKeyEvents: true,
                        value: 1,
                        minValue: 1,
						allowBlank: 0,
						anchor: "100%",
					},
                    {
						xtype: "textfield",
						fieldLabel: "Nomor Faktur Akhir",
						hideLabel: false,
						//hidden:true,
						name: "no_faktur_akhir",
						id: "no_faktur_akhirid",
						ref: "../no_faktur_akhir",
						maxLength: 20,
						//allowBlank: ,
                        readOnly: true,
						anchor: "100%",
					},
					{
						xtype: "xdatefield",
						ref: "../mulai_berlaku",
						fieldLabel: "Mulai Berlaku",
						name: "mulai_berlaku",
						id: "mulai_berlakuid",
						format: "d M Y",
						allowBlank: 0,
						anchor: "100%",
					},
				],
			},
		];
		this.fbar = {
			xtype: "toolbar",
			items: [
				{
					xtype: "button",
					text: "Simpan",
					hidden: true,
					ref: "../btnSave",
				},
				{
					xtype: "button",
					text: "Simpan & Tutup",
					ref: "../btnSaveClose",
				},
				{
					xtype: "button",
					text: "Batal",
					ref: "../btnCancel",
				},
			],
		};
		jun.NoFakturWin.superclass.initComponent.call(this);
		//        this.on('activate', this.onActivate, this);
		this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
		this.btnSave.on("click", this.onbtnSaveclick, this);
		this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.jml_nofaktur.on('keyup', this.onJmlChange, this);
        this.no_faktur_awal.on('change', this.onJmlChange, this);
		// if (this.modez == 1 || this.modez == 2) {
		// 	this.btnSave.setVisible(false);
		// } else {
		// 	this.btnSave.setVisible(true);
		// }
	},

    onJmlChange: function (a, e) {
        var awal = this.no_faktur_awal.getValue();
        var jml = parseFloat(this.jml_nofaktur.getValue()) - 1;
        if(awal == '') return;
        var akhir = incrementLeadingZeroNumber(awal,jml);
        this.no_faktur_akhir.setValue(akhir);
    },

	btnDisabled: function (status) {
		this.btnSave.setDisabled(status);
		this.btnSaveClose.setDisabled(status);
	},

	saveForm: function () {
		this.btnDisabled(true);
		var urlz;
		if (this.modez == 1 || this.modez == 2) {
			urlz = "NoFaktur/update/id/" + this.id;
		} else {
			urlz = "NoFaktur/create/";
		}

		Ext.getCmp("form-NoFaktur")
			.getForm()
			.submit({
				url: urlz,
				timeOut: 1000,
				scope: this,
				success: function (f, a) {
					jun.rztNoFaktur.reload();
					var response = Ext.decode(a.response.responseText);
					Ext.MessageBox.show({
						title: "Info",
						msg: response.msg,
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.INFO,
					});
					if (this.modez == 0) {
						Ext.getCmp("form-NoFaktur").getForm().reset();
						this.btnDisabled(false);
					}
					if (this.closeForm) {
						this.close();
					}
				},
				failure: function (f, a) {
					switch (a.failureType) {
						case Ext.form.Action.CLIENT_INVALID:
							Ext.Msg.alert(
								"Failure",
								"Form fields may not be submitted with invalid values"
							);
							break;
						case Ext.form.Action.CONNECT_FAILURE:
							Ext.Msg.alert("Failure", "Ajax communication failed");
							break;
						case Ext.form.Action.SERVER_INVALID:
							Ext.Msg.alert("Failure", a.result.msg);
					}
					this.btnDisabled(false);
				},
			});
	},

	onbtnSaveCloseClick: function () {
		this.closeForm = true;
		this.saveForm(true);
	},

	onbtnSaveclick: function () {
		this.closeForm = false;
		this.saveForm(false);
	},
	onbtnCancelclick: function () {
		this.close();
	},
});
