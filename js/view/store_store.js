jun.Storestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Storestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StoreStoreId',
            url: 'Store',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'store_kode'},
                {name: 'nama_store'},
                {name: 'tipe'},
                {name: 'tanggal_backup'},
                {name: 'transaksi_flag'},
                {name: 'id_cabang'},
                {name: 'up'},
                {name: 'wilayah_id'},
                {name: 'nscc_store_group_id'},
                {name: 'beban_acc'},
                {name: 'conn'},
                {name: 'count'},
                {name: 'alamat'},
                {name: 'no_telp'},
                {name: 'npwp'},
                {name: 'nama_pemilik'},
                {name: 'nama_admin'},
                {name: 'logo'},
                {name: 'city'},
                {name: 'tgl_jt_tagihan'},
                {name: 'account_code'},
            ]
        }, cfg));
    },
    checkAll: function (store, column, checked) {
//      var dataIndex = column.dataIndex;
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('count', true);
        }
    },
    uncheckAll: function (store, column, checked) {
//      var dataIndex = column.dataIndex;
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('count', false);
        }
    }
});
jun.rztStore = new jun.Storestore();
jun.StoreReport = new jun.Storestore();
jun.rztStoreLib = new jun.Storestore();
jun.rztStoreCmp = new jun.Storestore();
//jun.rztStore.load();
