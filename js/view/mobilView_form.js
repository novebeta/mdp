jun.MobilViewWin = Ext.extend(Ext.Window, {
    title: 'MobilView',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-MobilView',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'mobil_tipe_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'mobil_tipe_id',
                                    id:'mobil_tipe_idid',
                                    ref:'../mobil_tipe_id',
                                    maxLength: 36,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'tahun_pembuatan',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'tahun_pembuatan',
                                    id:'tahun_pembuatanid',
                                    ref:'../tahun_pembuatan',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../mulai_stnk',
                            fieldLabel: 'mulai_stnk',
                            name:'mulai_stnk',
                            id:'mulai_stnkid',
                            format: 'd M Y',
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'no_rangka',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'no_rangka',
                                    id:'no_rangkaid',
                                    ref:'../no_rangka',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'no_pol',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'no_pol',
                                    id:'no_polid',
                                    ref:'../no_pol',
                                    maxLength: 10,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'customer_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'customer_id',
                                    id:'customer_idid',
                                    ref:'../customer_id',
                                    maxLength: 36,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'nama_customer',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'nama_customer',
                                    id:'nama_customerid',
                                    ref:'../nama_customer',
                                    maxLength: 100,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'textfield',
                            fieldLabel: 'alamat',
                            hideLabel:false,
                            //hidden:true,
                            name:'alamat',
                            id:'alamatid',
                            ref:'../alamat',
                            anchor: '100%'
                            //allowBlank: 1
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'tipe',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'tipe',
                                    id:'tipeid',
                                    ref:'../tipe',
                                    maxLength: 100,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'merk',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'merk',
                                    id:'merkid',
                                    ref:'../merk',
                                    maxLength: 100,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.MobilViewWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'MobilView/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'MobilView/create/';
                }
             
            Ext.getCmp('form-MobilView').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztMobilView.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-MobilView').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});