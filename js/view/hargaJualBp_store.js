jun.HargaJualBpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.HargaJualBpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'HargaJualBpStoreId',
            url: 'HargaJualBp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'harga_id'},
                {name: 'harga'},
                {name: 'jasa_bp_id'},
                {name: 'mobil_kategori_id'},
                {name: 'discrp'},
                {name: 'disc'},
                {name: 'tipe'},
                {name: 'hpp'},
            ]
        }, cfg));
    }
});
jun.rztHargaJualBp = new jun.HargaJualBpstore();
//jun.rztHargaJualBp.load();
