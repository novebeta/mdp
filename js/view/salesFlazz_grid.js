jun.SalesFlazzGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Penjualan",
    id: 'docs-jun.SalesFlazzGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        // {
        //     header: 'sales_flazz_id',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'sales_flazz_id',
        //     width: 100
        // },
        {
            header: 'tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'doc_ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'vatrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'No. SO',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Total Bayar',
            sortable: true,
            resizable: true,
            dataIndex: 'total_bayar',
            width: 100,
            align: "right",
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value == 0 && record.data.lunas != null) {
                    return new Intl.NumberFormat('en-US').format(record.data.total);
                } else {
                    return new Intl.NumberFormat('en-US').format(value);
                }
            }
        },
        {
            header: 'Kurang Bayar',
            sortable: true,
            resizable: true,
            dataIndex: 'total_bayar',
            width: 100,
            align: "right",
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value == 0 && record.data.lunas != null) {
                    return 0;
                } else {
                    return new Intl.NumberFormat('en-US').format(parseFloat(record.data.total) - parseFloat(record.data.total_bayar));
                }
            }
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'LUNAS'
                } else {
                    return 'BELUM BAYAR'
                }
            }
        },
        {
            header: 'Lunas',
            xtype: 'datecolumn',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 100
        },
        {
            header: 'Bank Lunas',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas_bank_id',
            renderer: jun.renderBank,
            width: 100
        },
        /*


                {
header:'total',
sortable:true,
resizable:true,
dataIndex:'total',
width:100
},
                {
header:'disc',
sortable:true,
resizable:true,
dataIndex:'disc',
width:100
},
                {
header:'discrp',
sortable:true,
resizable:true,
dataIndex:'discrp',
width:100
},
                {
header:'bruto',
sortable:true,
resizable:true,
dataIndex:'bruto',
width:100
},
                {
header:'vat',
sortable:true,
resizable:true,
dataIndex:'vat',
width:100
},
                {
header:'up',
sortable:true,
resizable:true,
dataIndex:'up',
width:100
},
                {
header:'total_pot',
sortable:true,
resizable:true,
dataIndex:'total_pot',
width:100
},
                {
header:'total_discrp1',
sortable:true,
resizable:true,
dataIndex:'total_discrp1',
width:100
},
                {
header:'tgl_jatuh_tempo',
sortable:true,
resizable:true,
dataIndex:'tgl_jatuh_tempo',
width:100
},
                {
header:'lunas',
sortable:true,
resizable:true,
dataIndex:'lunas',
width:100
},
                {
header:'bank_id',
sortable:true,
resizable:true,
dataIndex:'bank_id',
width:100
},
                {
header:'customer_id',
sortable:true,
resizable:true,
dataIndex:'customer_id',
width:100
},
                {
header:'barang_id',
sortable:true,
resizable:true,
dataIndex:'barang_id',
width:100
},
                {
header:'qty',
sortable:true,
resizable:true,
dataIndex:'qty',
width:100
},
                {
header:'harga',
sortable:true,
resizable:true,
dataIndex:'harga',
width:100
},
        */
    ],
    initComponent: function () {
        if (jun.rztBarangFlazz.getTotalCount() === 0) {
            jun.rztBarangFlazz.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        this.store = jun.rztSalesFlazz;
        jun.rztSalesFlazz.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.tglfrom = Ext.getCmp('tglgridflazzfrom').hiddenField.dom.value;
                    b.params.tglto = Ext.getCmp('tglgridflazzto').hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridflazzfrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridflazzto'
                        }, '-',
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Penjualan Kartu',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'TopUp Flazz',
                //     ref: '../btnTopUp'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Edit Penjualan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Invoice',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Bayar Penjualan',
                    ref: '../btnBayar'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Generate Faktur',
                    ref: '../btnFaktur'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        jun.SalesFlazzGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        // this.btnTopUp.on('Click', this.loadTuForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.btnBayar.on('Click', this.deleteRec, this);
        this.btnFaktur.on('Click', this.generateFaktur, this);
        Ext.getCmp('tglgridflazzfrom').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridflazzto').on('select', function () {
            this.store.load();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SalesFlazzWin({modez: 0});
        form.show();
    },
    loadTuForm: function () {
        var form = new jun.SalesFlazzTopUpWin({modez: 0});
        form.show();
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        var idz = selectedz.json.sales_flazz_id;
        window.open('salesFlazz/print/id/' + idz, '_blank');
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        if (selectedz.json.lunas != null) {
            Ext.MessageBox.alert("Warning", "Penjualan sudah lunas tidak bisa diedit.");
            return;
        }
        var form = null;
        var idz = selectedz.json.sales_flazz_id;
        if (selectedz.json.bank_id != null) {
            form = new jun.SalesFlazzTopUpWin({modez: 1, id: idz});
        } else {
            form = new jun.SalesFlazzWin({modez: 1, id: idz});
        }
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesFlazzItems.baseParams = {
            sales_flazz_id: idz
        };
        jun.rztSalesFlazzItems.load();
        jun.rztSalesFlazzItems.baseParams = {};
    },
    generateFaktur: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin generete nomor faktur pajak?', function(btn){
            if (btn == 'no') {
                return;
            }
            Ext.Ajax.request({
                url: 'NoFaktur/generate',
                method: 'POST',
                params:{
                    tipe:1
                },
                success: function (f, a) {
                    jun.rztSalesFlazz.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }, this);
    },
    deleteRec: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        if (selectedz.json.lunas != null) {
            Ext.MessageBox.alert("Warning", "Penjualan sudah lunas tidak bisa dibayar.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membayar ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        var form = new jun.PembayaranSalesFlazz({grid:this});
        form.show();
        // Ext.Msg.show({
        //     title: 'Pembayaran Flazz',
        //     msg: 'Kas/Bank',
        //     // value: 'choice 2',
        //     buttons: Ext.MessageBox.OKCANCEL,
        //     inputField: new Ext.form.ComboBox(
        //         {
        //             // xtype: 'combo',
        //             //typeAhead: true,
        //             triggerAction: 'all',
        //             lazyRender: true,
        //             mode: 'local',
        //             forceSelection: true,
        //             fieldLabel: 'bank_id',
        //             store: jun.rztBankTransCmpPusat,
        //             valueField: 'bank_id',
        //             displayField: 'nama_bank',
        //         }),
        //     fn: function (buttonId, text) {
        //         if (buttonId != 'ok')
        //             return;
        //         // Ext.Msg.alert('Your Choice', 'You chose: "' + text + '".');
        //         Ext.Ajax.request({
        //             url: 'salesFlazz/bayar/id/' + record.json.sales_flazz_id,
        //             method: 'POST',
        //             // scope: this,
        //             params: {
        //                 bank_id: text
        //             },
        //             success: function (f, a) {
        //                 jun.rztSalesFlazz.reload();
        //                 var response = Ext.decode(f.responseText);
        //                 Ext.MessageBox.show({
        //                     title: 'Info',
        //                     msg: response.msg,
        //                     buttons: Ext.MessageBox.OK,
        //                     icon: Ext.MessageBox.INFO
        //                 });
        //             },
        //             failure: function (f, a) {
        //                 switch (a.failureType) {
        //                     case Ext.form.Action.CLIENT_INVALID:
        //                         Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                         break;
        //                     case Ext.form.Action.CONNECT_FAILURE:
        //                         Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                         break;
        //                     case Ext.form.Action.SERVER_INVALID:
        //                         Ext.Msg.alert('Failure', a.result.msg);
        //                 }
        //             }
        //         });
        //     }
        // });
    }
});
jun.SalesFlazzTopupGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Flazz Top Up ",
    id: 'docs-jun.SalesFlazzTopupGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'doc_ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'vatrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'No. SO',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 100,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value != null) {
                    return 'LUNAS'
                } else {
                    return 'BELUM BAYAR'
                }
            }
        }
    ],
    initComponent: function () {
        if (jun.rztBarangFlazz.getTotalCount() === 0) {
            jun.rztBarangFlazz.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        this.store = jun.rztSalesFlazzTopUp;
        jun.rztSalesFlazzTopUp.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.tglfrom = Ext.getCmp('tglgridflazztopupfrom').hiddenField.dom.value;
                    b.params.tglto = Ext.getCmp('tglgridflazztopupto').hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    items: [
                        '-', 'Periode : ',
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            value: DATE_NOW,
                            ref: '../tglfrom',
                            id: 'tglgridflazztopupfrom'
                        },
                        {
                            xtype: 'xdatefield',
                            format: 'd M Y',
                            ref: '../tglto',
                            value: DATE_NOW,
                            id: 'tglgridflazztopupto'
                        }, '-',
                    ]
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Top Up',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'TopUp Flazz',
                //     ref: '../btnTopUp'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Edit Top Up',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Top Up',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Bayar Top Up',
                    ref: '../btnBayar'
                },
            ]
        };
        jun.SalesFlazzTopupGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printForm, this);
        this.btnBayar.on('Click', this.deleteRec, this);
        Ext.getCmp('tglgridflazztopupfrom').on('select', function () {
            this.store.load();
        }, this);
        Ext.getCmp('tglgridflazztopupto').on('select', function () {
            this.store.load();
        }, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SalesFlazzTopUpWin({modez: 0});
        form.show();
    },
    printForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        var idz = selectedz.json.sales_flazz_id;
        window.open('salesFlazz/print/id/' + idz, '_blank');
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih penjualan");
            return;
        }
        if (selectedz.json.lunas != null) {
            Ext.MessageBox.alert("Warning", "Penjualan sudah lunas tidak bisa diedit.");
            return;
        }
        var form = null;
        var idz = selectedz.json.sales_flazz_id;
        form = new jun.SalesFlazzTopUpWin({modez: 1, id: idz});
        form.show();
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membayar ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        var form = new jun.PembayaranSalesFlazz({grid:this});
        form.show();
    }
});
jun.PembayaranSalesFlazz = Ext.extend(Ext.Window, {
    title: "Bayar Penjualan Flazz",
    // iconCls: "silk13-report",
    modez: 1,
    grid: null,
    width: 350,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-PembayaranSalesFlazz",
                labelWidth: 75,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        anchor: '100%',
                        allowBlank: false,
                        value: DATE_NOW
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kas/Bank',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Jumlah',
                        hideLabel: false,
                        name: 'amount',
                        id: 'amountid',
                        ref: '../amount',
                        maxLength: 15,
                        value: 0,
                        minValue: 0,
                        anchor: '100%'
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-html",
                    text: "Save",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Cancel",
                    // hidden: (STORE != ''),
                    ref: "../btnCancel"
                },
            ]
        };
        jun.PembayaranSalesFlazz.superclass.initComponent.call(this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCancelclick: function () {
        this.close();
    },
    onbtnSaveclick: function () {
        var grid = this.grid;
        var record = grid.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Invoice");
            return;
        }
        Ext.getCmp('form-PembayaranSalesFlazz').getForm().submit({
            url: 'salesFlazz/bayar/id/' + record.json.sales_flazz_id,
            scope: this,
            success: function (f, a) {
                jun.rztSalesFlazz.reload();
                // var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if(a.result.id != ''){
                    window.open('salesFlazzBayar/print/id/' + a.result.id, '_blank');
                }
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                // this.btnDisabled(false);
            }
        });
    }
});
