jun.ResepWin = Ext.extend(Ext.Window, {
    title: 'Resep',
    modez: 1,
    width: 725,
    height: 300,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Resep',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                    border: false,
                    bodyStyle: 'background-color: #E4E4E4;'
                },
                ref: 'formz',
                border: false,
                items: [
                    {
                        width: '100%',
                        height: 30,
                        layout: 'hbox',
                        defaults: {
                            // margins: '0 0 5 0',
                            border: false,
                            bodyStyle: 'background-color: #E4E4E4;'
                        },
                        layoutConfig: {
                            // padding: 5,
                            align: 'stretch'
                        },
                        items: [
                            {
                                flex: 1,
                                layout: 'form',
                                margins: '0 5px 0 0',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Resep',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'nama_resep',
                                        ref: '../../../nama_resep',
                                        maxLength: 100,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                layout: 'form',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        fieldLabel: 'Item',
                                        store: jun.rztBarangCmp,
                                        hiddenName: 'barang_id',
                                        valueField: 'barang_id',
                                        displayField: 'kode_barang',
                                        ref: '../../../kode_barang',
                                        anchor: '100%'
                                    }
                                ]
                            }
                        ]
                    },
                    new jun.ResepDetailsGrid({
                        flex: 1,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ResepWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.kode_barang.on('beforeselect', this.onkodebarangSelect, this);
        this.on('close', this.onClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.kode_barang.setReadOnly(true);
        } else {
            this.btnSave.setVisible(true);
            this.kode_barang.setReadOnly(false);
        }
    },
    onClose: function () {
        jun.rztResepDetails.removeAll();
    },
    onkodebarangSelect: function (c, r, i) {
        if (jun.rztResep.findExact('barang_id', r.data.barang_id) > -1) {
            Ext.Msg.alert('Resep sudah ada!', 'Untuk edit resep mohon melalui menu Edit Resep!!!');
            // c.reset();
            return false;
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Resep/update/id/' + this.id;
        } else {
            urlz = 'Resep/create/';
        }
        Ext.getCmp('form-Resep').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this, params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztResepDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztResep.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Resep').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    }
    ,
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    }
    ,
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    }
    ,
    onbtnCancelclick: function () {
        this.close();
    }
});




jun.PrintResepApotik = Ext.extend(Ext.grid.GridPanel, {
    title: "Print Resep Apotik",
    id: 'docs-jun.PrintResepApotik',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    height: 1024,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_receipt'
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'pasien_code'
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'pasien_name'
        },
        {
            header: 'Item',
            sortable: true,
            resizable: true,
            dataIndex: 'barang'
        }
    ],
    initComponent: function (){

        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }

        jun.rztPrintResep.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsalesgridid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztPrintResep;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Tanggal Transaksi :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsalesgridid',
                    ref: '../tgl'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Tanggal dan Dokter yang akan dicetak :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglprint',
                    ref: '../tglprint',
                    submitFormat: 'Y-m-d H:i:s'
                },
                {
                    xtype: 'combo',
                    //typeAhead: true,
                    editable: false,
                    fieldLabel: 'Doctor',
                    ref: '../dokter',
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
//                        hidden:REFERRAL,
                    store: jun.rztDokterStatus,
                    hiddenName: 'dokter_id',
                    valueField: 'dokter_id',
                    displayField: 'nama_dokter',
                    allowBlank: true,
                    width: 200
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Resep',
                    ref: '../btnPrintStocker'
                }
            ]
        };
        jun.PrintResepApotik.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnPrint.on('Click', this.printSales, this);
        // this.btnResep.on('Click', this.printResep, this);
        this.btnPrintStocker.on('Click', this.printResep, this);
        // this.btnPrintBeauty.on('Click', this.printBeauty2, this);
        // this.btnRetur.on('Click', this.returAllRec, this);
        // this.btnComment.on('Click', this.editComment, this);
        this.tgl.on('select', this.refreshTgl, this);
        // this.on("activate", this.onActive, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    printResep: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var tglnya = this.tglprint.getValue();
        tglnya = (tglnya == "") ? "" : this.tglprint.getValue().format('Y-m-d').trim();
        Ext.Ajax.request({
            url: 'Salestrans/PrintResep',
            method: 'POST',
            scope: this,
            params: {
                dokter: this.dokter.getRawValue(),
                dokter_id: this.dokter.getValue(),
                tgl:tglnya,
                id: record.json.salestrans_id,
                mode:1
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (response.success !== false) {
                    if (notReady()) {
                        return;
                    }
                    var msg = [{type: 'raw', data: response.msg}];
                    var printDataResep = __printDataStocker.concat(msg, __feedPaper, __cutPaper);
                    if (PRINTER_STOCKER != 'default') {
                        print(PRINTER_STOCKER, printDataResep);
                    }
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztCustomersSalesCmp.baseParams = {
            customer_id: this.record.data.customer_id
        };
        jun.rztCustomersSalesCmp.load();
        jun.rztCustomersSalesCmp.baseParams = {};
    }
});