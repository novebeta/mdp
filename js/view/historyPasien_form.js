jun.HistoryPasienWin = Ext.extend(Ext.Window, {
    title: 'HistoryPasien',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-HistoryPasien',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'nokartu',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'nokartu',
                                    id:'nokartuid',
                                    ref:'../nokartu',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'nobase',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'nobase',
                                    id:'nobaseid',
                                    ref:'../nobase',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'noax',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'noax',
                                    id:'noaxid',
                                    ref:'../noax',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'namacus',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'namacus',
                                    id:'namacusid',
                                    ref:'../namacus',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'namadep',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'namadep',
                                    id:'namadepid',
                                    ref:'../namadep',
                                    maxLength: 5,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'linedisc',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'linedisc',
                                    id:'linediscid',
                                    ref:'../linedisc',
                                    maxLength: 30,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'alamat',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'alamat',
                                    id:'alamatid',
                                    ref:'../alamat',
                                    maxLength: 300,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'email',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'email',
                                    id:'emailid',
                                    ref:'../email',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'kota',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'kota',
                                    id:'kotaid',
                                    ref:'../kota',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'telp',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'telp',
                                    id:'telpid',
                                    ref:'../telp',
                                    maxLength: 80,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'jnscus',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'jnscus',
                                    id:'jnscusid',
                                    ref:'../jnscus',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../tgllh',
                            fieldLabel: 'tgllh',
                            name:'tgllh',
                            id:'tgllhid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'sex',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'sex',
                                    id:'sexid',
                                    ref:'../sex',
                                    maxLength: 6,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'usia',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'usia',
                                    id:'usiaid',
                                    ref:'../usia',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'kerja',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'kerja',
                                    id:'kerjaid',
                                    ref:'../kerja',
                                    maxLength: 20,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../awal',
                            fieldLabel: 'awal',
                            name:'awal',
                            id:'awalid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../akhir',
                            fieldLabel: 'akhir',
                            name:'akhir',
                            id:'akhirid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'akhirke',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'akhirke',
                                    id:'akhirkeid',
                                    ref:'../akhirke',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'saldo',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'saldo',
                                    id:'saldoid',
                                    ref:'../saldo',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../dtgl',
                            fieldLabel: 'dtgl',
                            name:'dtgl',
                            id:'dtglid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'jmlcp',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'jmlcp',
                                    id:'jmlcpid',
                                    ref:'../jmlcp',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'id_cabang',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'id_cabang',
                                    id:'id_cabangid',
                                    ref:'../id_cabang',
                                    maxLength: 11,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'id_source',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'id_source',
                                    id:'id_sourceid',
                                    ref:'../id_source',
                                    maxLength: 11,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'aktif',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'aktif',
                                    id:'aktifid',
                                    ref:'../aktif',
                                    maxLength: 1,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../createddate',
                            fieldLabel: 'createddate',
                            name:'createddate',
                            id:'createddateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.HistoryPasienWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'HistoryPasien/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'HistoryPasien/create/';
                }
             
            Ext.getCmp('form-HistoryPasien').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztHistoryPasien.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-HistoryPasien').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});