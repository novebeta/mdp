jun.BpJasaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "BpJasa",
    id: 'docs-jun.BpJasaGrid',
    // iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Jasa',
            sortable: true,
            resizable: true,
            dataIndex: 'jasa_bp_id',
            width: 100,
            renderer: jun.renderJasaBpKode
        },
        {
            header: 'Nama Jasa',
            sortable: true,
            resizable: true,
            dataIndex: 'jasa_bp_id',
            width: 400,
            renderer: function (v, m, r, rI, cI, s) {
                var label = '';
                var jb = jun.getJasaBp(v);
                if (jb != null) {
                    label = jb.data.nama;
                }
                if (r.data.note == null) {
                    r.data.note = "";
                }
                label = label + ' ' + r.data.note;
                return label;
            }
        },
        {
            header: 'Jenis',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe',
            width: 100
        },
        {
            header: 'HPP',
            sortable: true,
            resizable: true,
            dataIndex: 'hpp1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc Rp',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'dpp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
    ],
    initComponent: function () {
        this.store = jun.rztBpJasa;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            hidden: (this.modez === 4 || this.modez === 5),
            items: [
                {
                    xtype: 'panel',
                    width: 700,
                    defaults: {
                        border: false,
                    },
                    // height: 35,
                    // layout:'vbox',
                    // layoutConfig: {
                    //     align : 'stretch',
                    //     pack  : 'start',
                    // },
                    items: [
                        {
                            xtype: 'panel',
                            bodyStyle: 'background-color: #E4E4E4;',
                            layout: 'hbox',
                            layoutConfig: {
                                padding: '2 5 0 5',
                                align: 'middle'
                            },
                            defaults: {margins: '0 5 0 0'},
                            items: [
                                {
                                    xtype: 'label',
                                    text: 'Jasa :'
                                },
                                {
                                    xtype: 'mfcombobox',
                                    searchFields: [
                                        'kode',
                                        'nama'
                                    ],
                                    mode: 'local',
                                    store: jun.rztJasaBpCmp,
                                    valueField: 'jasa_bp_id',
                                    itemSelector: 'tr.search-item',
                                    tpl: new Ext.XTemplate('<table style="cellspacing=0;" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                        '<th>Kode</th><th>Nama</th><th>COA</th></tr></thead>',
                                        '<tbody><tpl for="."><tr class="search-item">',
                                        '<td>{kode}</td><td>{nama}</td><td>{account_code}</td>',
                                        '</tr></tpl></tbody></table>'),
                                    allowBlank: false,
                                    forceSelection: true,
                                    listWidth: 500,
                                    width: 250,
                                    displayField: 'nama',
                                    ref: '../../../jasa',
                                },
                                {
                                    xtype: 'label',
                                    text: 'Tipe :'
                                },
                                new jun.cmbTipeJasa({
                                    fieldLabel: 'Tipe',
                                    hiddenName: 'tipe',
                                    ref: '../../../tipe',
                                    width: 85
                                }),
                                {
                                    xtype: 'label',
                                    text: 'HPP :',
                                    width: 50
                                },
                                {
                                    xtype: 'numericfield',
                                    ref: '../../../hpp1',
                                    width: 85,
                                    value: 0,
                                    readOnly: true
                                    // enableKeyEvents: true
                                },
                                {
                                    xtype: 'hidden',
                                    ref: '../../../hpp2',
                                    value: 0
                                },
                                {
                                    xtype: 'label',
                                    text: 'Harga :'
                                },
                                {
                                    xtype: 'numericfield',
                                    //id: 'kreditid',
                                    ref: '../../../harga',
                                    width: 85,
                                    value: 0,
                                    enableKeyEvents: true
                                },
                            ]
                        },
                        {
                            bodyStyle: 'background-color: #E4E4E4;',
                            layout: 'hbox',
                            layoutConfig: {
                                padding: '2 5 2 5',
                                align: 'middle'
                            },
                            defaults: {margins: '0 5 0 0'},
                            items: [
                                {
                                    xtype: 'label',
                                    text: 'Note :'
                                },
                                {
                                    xtype: 'uctextfield',
                                    hideLabel: false,
                                    ref: '../../../note',
                                    // maxLength: 600,
                                    typeAhead: true,
                                    triggerAction: 'all',
                                    // lazyRender: true,
                                    mode: 'local',
                                    // colspan: 4,
                                    width: 250,
                                },
                                {
                                    xtype: 'label',
                                    text: 'Disc :'
                                },
                                {
                                    xtype: 'numericfield',
                                    ref: '../../../disc',
                                    width: 85,
                                    value: 0,
                                    enableKeyEvents: true
                                },
                                {
                                    xtype: 'label',
                                    text: 'Disc Rp:',
                                    width: 50
                                },
                                {
                                    xtype: 'numericfield',
                                    ref: '../../../discrp',
                                    width: 85,
                                    value: 0,
                                    enableKeyEvents: true
                                },
                                {
                                    xtype: 'label',
                                    text: 'PPN :',
                                },
                                {
                                    xtype: 'checkbox',
                                    // boxLabel: "HPP",
                                    ref: '../../../ppn_out',
                                    checked: true,
                                    value: 1,
                                    inputValue: 1,
                                    uncheckedValue: 0,
                                }
                            ]
                        },
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    // defaults: {
                    //     scale: 'large'
                    // },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.BpJasaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscrpChange, this);
        this.jasa.on('select', this.getPrice, this);
        this.tipe.on('select', this.getPrice, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onDiscChange: function () {
        var harga = parseFloat(this.harga.getValue());
        if (parseFloat(this.harga.getValue()) == 0) {
            return;
        }
        var rp = parseFloat(this.disc.getValue());
        this.discrp.setValue(round(harga * (rp / 100), 2));
    },
    onDiscrpChange: function () {
        var harga = parseFloat(this.harga.getValue());
        if (parseFloat(this.harga.getValue()) == 0) {
            return;
        }
        var rp = parseFloat(this.discrp.getValue());
        this.disc.setValue(round((rp / harga) * 100, 0));
    },
    getPrice: function () {
        this.hpp1.setReadOnly(true);
        this.harga.setReadOnly(true);
        var jasa_bp_id = this.jasa.getValue();
        var tipe = this.tipe.getValue();
        var mobil_kategori_id = Ext.getCmp('mobil_kategori_id').getValue();
        if (jasa_bp_id == '' || tipe == '' || mobil_kategori_id == '') {
            return;
        }
        var kode = jun.renderJasaBpKode(jasa_bp_id);
        var coa = jun.renderJasaCoa(jasa_bp_id);
        let nama = jun.renderJasaBp(jasa_bp_id);
        let specialKode = ['LL01','LL02','JS096'];
        this.hpp1.setReadOnly(!specialKode.includes(kode));
        this.harga.setReadOnly(!specialKode.includes(kode));
        // if (coa === '21-01-07' || coa === '21-01-04'|| coa === '21-01-11') {
        //     if (nama === 'JASA') {
        //         this.harga.setReadOnly(false);
        //     } else {
        //         this.harga.setReadOnly(true);
        //     }
        // } else {
        //     this.harga.setReadOnly(true);
        // }
        Ext.Ajax.request({
            url: 'HargaJualBp/GetPrice',
            method: 'POST',
            scope: this,
            params: {
                jasa_bp_id: jasa_bp_id,
                tipe: tipe,
                mobil_kategori_id: mobil_kategori_id,
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (kode !== 'JS096') {
                    this.hpp2.setValue(response.results.hpp);
                }else{
                    this.hpp2.setValue(0);
                }
                this.harga.setValue(response.results.harga);
                this.disc.setValue(response.results.disc);
                this.discrp.setValue(response.results.discrp);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function (s) {
        this.jasa.reset();
        this.tipe.reset();
        this.harga.reset();
        this.hpp1.reset();
        this.hpp2.reset();
        this.note.reset();
        this.disc.reset();
        this.discrp.reset();
        this.ppn_out.reset();
        this.btnDisable(false);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.jasa.setValue(record.data.jasa_bp_id);
            this.tipe.setValue(record.data.tipe);
            this.hpp1.setValue(record.data.hpp1);
            this.hpp2.setValue(record.data.hpp2);
            this.note.setValue(record.data.note);
            this.harga.setValue(record.data.harga);
            this.disc.setValue(record.data.disc);
            this.discrp.setValue(record.data.discrp);
            this.ppn_out.setValue(record.data.ppn_out == 1);
            btn.setText("Save");
            this.btnDisable(true);
            var coa = jun.renderJasaCoa(record.data.jasa_bp_id);
            let nama = jun.renderJasaBp(record.data.jasa_bp_id);
            this.hpp1.setReadOnly(coa !== '21-01-07');
            if (coa === '21-01-07' || coa === '21-01-04') {
                if (nama === 'JASA') {
                    this.harga.setReadOnly(false);
                } else {
                    this.harga.setReadOnly(true);
                }
            } else {
                this.harga.setReadOnly(true);
            }
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
        jun.rztBpSparepartsKode.load();
        jun.rztBpSparepartsNama.load();
    },
    loadForm: function () {
        this.btnDisable(true);
        var jasa = this.jasa.getValue();
        var tipe = this.tipe.getValue();
        var note = this.note.getValue();
        if (note == null) {
            note = "";
        }
        if (!jasa) {
            Ext.MessageBox.alert("Error", "Jasa harus dipilih.");
            // this.btnDisable(false);
            return;
        }
        if (!tipe) {
            Ext.MessageBox.alert("Error", "Tipe harus dipilih.");
            // this.btnDisable(false);
            return;
        }
        var harga = parseFloat(this.harga.getValue());
        var hpp1 = parseFloat(this.hpp1.getValue());
        var hpp2 = parseFloat(this.hpp2.getValue());
        var disc = parseFloat(this.disc.getValue());
        var discrp = parseFloat(this.discrp.getValue());
        var ppn_out = this.ppn_out.getValue() ? 1 : 0;
        var ppn_ind = 1;
        var maxdisc = parseFloat(Ext.getCmp('syspref_mxdisc').getValue());
        if (disc > maxdisc) {
            Ext.MessageBox.alert("Error", "Diskon terlalu besar.");
            // this.btnDisable(false);
            return;
        }
        var dpp = harga - discrp;
        var total_jasa_line = dpp;
        var ppn = 0;
        if(ppn_ind == 1){
            ppn = (PENGALI_PPN * dpp);
            total_jasa_line += ppn;
        }
        var coa = jun.renderJasaCoa(jasa);
        var kode = jun.renderJasaBpKode(jasa);
        if (kode.substring(0, 2) === 'JS' && kode !== 'JS096') {
            var disk1 = parseFloat(Ext.getCmp('syspref_disc1').getValue());
            var disk2 = parseFloat(Ext.getCmp('syspref_disc2').getValue());
            hpp2 = (disk2 / 100) * ((1 - (disk1 / 100)) * harga);
        }else{
            hpp2 = 0;
        }
        this.save({
            jasa_bp_id: jasa,
            tipe: tipe,
            harga: harga,
            hpp1: hpp1,
            hpp2: hpp2,
            note: note,
            disc: disc,
            dpp: dpp,
            ppn_ind: ppn_ind,
            ppn_out: ppn_out,
            ppn: ppn,
            discrp: discrp,
            total_jasa_line: total_jasa_line
        });
    },
    save: function (data) {
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('jasa_bp_id', data.jasa_bp_id);
            record.set('tipe', data.tipe);
            record.set('harga', data.harga);
            record.set('hpp1', data.hpp1);
            record.set('hpp2', data.hpp2);
            record.set('note', data.note);
            record.set('disc', data.disc);
            record.set('discrp', data.discrp);
            record.set('dpp', data.dpp);
            record.set('ppn_ind', data.ppn_ind);
            record.set('ppn_out', data.ppn_out);
            record.set('ppn', data.ppn);
            record.set('total_jasa_line', data.total_jasa_line);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c(data);
            this.store.add(d);
        }
        this.resetForm();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin menghapus data ini?", this.deleteRecYes, this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
            return;
        }
        this.store.remove(b);
    }
})
