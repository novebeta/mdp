jun.SyncLogGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Sync Logs",
        id:'docs-jun.SyncLogGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
/*                                {
			header:'User',
			sortable:true,
			resizable:true,                        
            dataIndex:'user',
			width:60
		},*/
                                {
			header:'Store',
			sortable:true,
			resizable:true,                        
            dataIndex:'store',
			width:100
		},
                                {
			header:'Date',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdate',
			width:100,
            renderer: Ext.util.Format.dateRenderer('d/M/Y')
		},
        /*
                                {
            header:'Status',
            sortable:true,
            resizable:true,
            dataIndex:'status',
            width:100
        },*/
        {
            header:'Status',
            sortable:true,
            resizable:true,
            dataIndex:'status',
            width:100,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #ffff99;";
                        return 'NO';
                    case 1 :
                        metaData.style += "background-color: #99ff99;";
                        return 'OK';
                    case 2 :
                        metaData.style += "background-color: #ff8080;";
                        return 'FAILED';
                }
            }
        },
                                {
			header:'Diff',
			sortable:true,
			resizable:true,                        
            dataIndex:'diff',
			width:100
		},
        {
            header:'Action',
            sortable:true,
            resizable:true,
            dataIndex:'action',
            width:100
        },
        {
			header:'Note',
			sortable:true,
			resizable:true,                        
            dataIndex:'note',
			width:100
		},

                                {
			header:'Timestamps',
			sortable:true,
			resizable:true,                        
            dataIndex:'timestamps',
			width:100
		}

	],
	initComponent: function(){
	this.store = jun.rztSyncLog;
        this.bbar = {
            items: [
           	{
				xtype: 'paging',
				store: this.store,
				displayInfo: true,
				pageSize: 20,
               	ref:"../paging"
           	}]
        };

		jun.SyncLogGrid.superclass.initComponent.call(this);

	}

})
