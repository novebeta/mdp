jun.login = new Ext.extend(Ext.Window, {
    width: 390,
    height: 200,
    layout: "form",
    modal: !0,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Login",
    padding: 5,
    initComponent: function () {
        this.items = [
            {
                xtype: "box",
                style: 'margin:5px',
                html: "Please, make sure that your computer's date and time are correct."
            },
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Login",
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                plain: !0,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date & Time',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y H:i:s',
                        readOnly: true,
                        value: DATE_NOW,
                        anchor: "100%"
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "usernameid",
                        ref: "../username",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Username",
                        name: "loginUsername",
                        allowBlank: !1
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Password",
                        name: "pass",
                        // name: "loginPassword",
                        inputType: "password",
                        allowBlank: !1
                    },
                    // {
                    //     xtype: "hidden",
                    //     name: "ip",
                    //     ref: "../ip"
                    // }
                    //{
                    //    xtype: "uctextfield",
                    //    hideLabel: !1,
                    //    id: "comid",
                    //    ref: "../com",
                    //    anchor: "100%",
                    //    fieldLabel: "Serial Port",
                    //    allowBlank: true
                    //},
                    //{
                    //    xtype: 'checkbox',
                    //    fieldLabel: 'Enable Tools',
                    //    boxLabel: "(Printer, Line Display, Cash Drawer)",
                    //    hideLabel: false,
                    //    ref: '../print'
                    //}
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                // {
                //     xtype: "button",
                //     text: "Exit",
                //     hidden: !1,
                //     ref: "../btnExit"
                // },
                {
                    xtype: "button",
                    text: "Login",
                    hidden: !1,
                    ref: "../btnLogin"
                }
            ]
        };
        jun.login.superclass.initComponent.call(this);
        this.btnLogin.on("click", this.onbtnLoginClick, this);
        // this.btnExit.on("click", this.onbtnExitClick, this);
        //this.setDateTime();
        //this.com.setValue(localStorage.getItem("serial_com"));
        //this.print.setValue(localStorage.getItem("enable_tools") != "false");
    },
    onbtnExitClick: function () {
        Ext.MessageBox.confirm('Exit', 'Are You sure want to exit', function (a) {
            if (a != "no") {
                try {
                    var gui = require('nw.gui');
                    gui.App.quit();
                } catch (err) {
                    var myWindow = window.open("", "_self");
                    myWindow.document.write("");
                    setTimeout(function () {
                        myWindow.close();
                    }, 1000);
                }
            }
        });
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onbtnLoginClick: function () {
        var username = this.username.getValue();
        var password = this.password.getValue();
        if (username.trim() == "" || password.trim() == "") {
            Ext.Msg.alert("Warning!", "Login Failed");
            return;
        }
        //if (this.print.getValue()) {
        //    if (this.com.getValue() == "" || this.com.getValue() == undefined) {
        //        Ext.Msg.alert("Warning!", "Serial port must define!");
        //        return;
        //    }
        //}
        // this.ip.setValue(IP);
        // var client = new ClientJS();
        var a = Ext.getCmp("passwordid").getValue();
        a = jun.EncryptPass(a);
        Ext.getCmp("passwordid").setValue(a);
        Ext.getCmp("form-Login").getForm().submit({
            scope: this,
            url: "login",
            params: {
                loginPassword: a,
                // ip_local: IP_LOCAL,
                // ip_public: IP_PUBLIC,
                // browserdata: BrowserData,
                // fingerprint: Fingerprint,
                // useragent: UserAgent,
                // browser: Browser,
                // browserversion: BrowserVersion,
                // browsermajorversion: BrowserMajorVersion,
                // ie: IE,
                // chrome: Chrome,
                // firefox: Firefox,
                // safari: Safari,
                // opera: Opera,
                // engine: Engine,
                // engineversion: EngineVersion,
                // os: OS,
                // osversion: OSVersion,
                // windows: Windows,
                // mac_os: MAC,
                // linux: Linux,
                // ubuntu: Ubuntu,
                // solaris: Solaris,
                // device: Device,
                // devicetype: DeviceType,
                // devicevendor: DeviceVendor,
                // cpu: CPU,
                // mobile: Mobile,
                // mobilemajor: MobileMajor,
                // mobileandroid: MobileAndroid,
                // mobileopera: MobileOpera,
                // mobilewindows: MobileWindows,
                // mobileblackberry: MobileBlackBerry,
                // mobileios: MobileIOS,
                // iphone: Iphone,
                // ipad: Ipad,
                // ipod: Ipod,
                // screenprint: ScreenPrint,
                // colordepth: ColorDepth,
                // currentresolution: CurrentResolution,
                // availableresolution: AvailableResolution,
                // devicexdpi: DeviceXDPI,
                // deviceydpi: DeviceYDPI,
                // plugins: Plugins,
                // java: Java,
                // javaversion: JavaVersion,
                // flash: Flash,
                // flashversion: FlashVersion,
                // silverlight: Silverlight,
                // silverlightversion: SilverlightVersion,
                // mimetypes: MimeTypes,
                // ismimetypes: IsMimeTypes,
                // font: Font,
                // fonts: Fonts,
                // localstorage: LocalStorage,
                // sessionstorage: SessionStorage,
                // cookie: Cookie,
                // timezone: TimeZone,
                // language: Language,
                // systemlanguage: SystemLanguage,
                // canvas: Canvas
                // CanvasPrint: client.getCanvasPrint()
            },
            waitTitle: "Connecting",
            waitMsg: "Sending data...",
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                //localStorage.setItem("serial_com", this.com.getValue());
                //localStorage.setItem("enable_tools", this.print.getValue());
                // var a = BASE_URL;
                if (response.msg == 'OK') {
                    window.location = BASE_URL;
                } else {
                    Ext.Msg.alert('Failure', response.msg);
                }
            },
            failure: function (f, a) {
                Ext.getCmp("form-Login").getForm().reset();
                //this.setDateTime();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.errors.reason);
                }
            }
        });
    }
});
jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border",
    initComponent: function () {
        this.items = [
            {
                xtype: "box",
                region: "north",
                applyTo: "header",
                height: 30
            },
            {
                xtype: "container",
                autoEl: "div",
                region: "west",
                height: 20
            },
            {
                xtype: "container",
                autoEl: "div",
                region: "east",
                height: 20
            },
            {
                xtype: "container",
                autoEl: "div",
                region: "south",
                height: 20
            }
        ];
        jun.ViewportUi.superclass.initComponent.call(this);
    }
});
jun.win = new Ext.extend(Ext.Window, {
    layout: "fit",
    width: 300,
    height: 150,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    initComponent: function () {
        this.items = [jun.login];
        jun.win.superclass.initComponent.call(this);
    }
});
Ext.onReady(function () {
    var a = function () {
        Ext.get("loading").remove();
        Ext.fly("loading-mask").fadeOut({
            remove: !0
        });
    };
    Ext.QuickTips.init();
    var b = convertDateToUTC(new Date());
    var diff = DATE_NOW_UTC.getTime() - b.getTime();
    var min = Math.ceil(diff / (1000 * 60));
    if (Math.abs(min) > 30) {
        // this.setDisabled(true);
        Ext.Msg.alert('ERROR', 'JAM KOMPUTER INI MEMILIKI SELISIH LEBIH DARI 30 MENIT DARI JAM SERVER!!!');
    } else {
        var b = new jun.login({});
        b.show();
    }
});